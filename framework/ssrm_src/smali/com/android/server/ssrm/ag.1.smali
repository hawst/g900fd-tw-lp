.class public final Lcom/android/server/ssrm/ag;
.super Ljava/lang/Thread;

# interfaces
.implements Landroid/speech/tts/TextToSpeech$OnInitListener;


# static fields
.field static final DEBUG:Z

.field static final MSG_SHUTDOWN:I = 0xa

.field static TAG:Ljava/lang/String; = null

.field private static aH:Lcom/android/server/ssrm/ag; = null

.field static bq:Ljava/util/Hashtable; = null

.field static final iA:I = 0x2

.field static final iB:I = 0x3

.field static final iC:I = 0x4

.field static final iD:I = 0x5

.field public static final iE:I = 0x6

.field static final iF:I = 0x7

.field static final iG:I = 0x8

.field static final iH:I = 0x9

.field public static iL:Z = false

.field static iO:Ljava/util/ArrayList; = null

.field static iP:Ljava/util/ArrayList; = null

.field public static iQ:Lf/m; = null

.field public static iR:Lf/a; = null

.field private static iS:Z = false

.field public static io:Z = false

.field static ip:I = 0x0

.field static iq:I = 0x0

.field static ir:Z = false

.field static final iz:I = 0x1

.field static final jD:Ljava/util/Hashtable;

.field public static jc:Ljava/lang/StringBuilder; = null

.field public static jd:Landroid/os/HandlerThread; = null

.field static ji:I = 0x0

.field static final jo:Ljava/lang/String; = "/sys/class/power_supply/battery/current_avg"

.field static js:Lcom/android/server/ssrm/aG;

.field public static jv:Z

.field public static jw:Z


# instance fields
.field final SSRM_NOTIFICATION_PERMISSION:Ljava/lang/String;

.field aK:Z

.field final iI:Ljava/lang/String;

.field final iJ:Ljava/lang/String;

.field final iK:Ljava/lang/String;

.field iM:Z

.field iN:Z

.field iT:Lc/a;

.field iU:Lc/o;

.field iV:Lb/M;

.field iW:Lb/L;

.field iX:Lb/N;

.field iY:Lb/P;

.field iZ:Lb/T;

.field ie:Landroid/app/ActivityManager;

.field if:Lcom/samsung/android/cover/CoverManager;

.field ig:Lcom/android/server/ssrm/X;

.field ih:Lcom/android/server/ssrm/F;

.field ii:Lcom/android/server/ssrm/aI;

.field ij:Lcom/android/server/ssrm/J;

.field ik:Lcom/android/server/ssrm/a;

.field il:Lcom/android/server/ssrm/aA;

.field im:Lg/a;

.field in:La/a;

.field final is:Ljava/lang/String;

.field final it:Ljava/lang/String;

.field final iu:Ljava/lang/String;

.field final iw:Ljava/lang/String;

.field final ix:Ljava/lang/String;

.field final iy:Ljava/lang/String;

.field jA:Landroid/os/DVFSHelper;

.field private jB:Ld/a;

.field jC:Lcom/android/server/ssrm/aC;

.field ja:Lb/R;

.field jb:Lb/ao;

.field final je:I

.field jf:Ljava/lang/Runnable;

.field jg:Lcom/android/server/ssrm/aD;

.field jh:Z

.field final jj:Ljava/lang/String;

.field final jk:Landroid/content/Intent;

.field final jl:Ljava/lang/String;

.field jm:I

.field jn:I

.field jp:Z

.field jq:Z

.field jr:Z

.field jt:Landroid/os/DVFSHelper;

.field ju:Lcom/android/server/ssrm/aH;

.field jx:I

.field jy:I

.field jz:Z

.field mContext:Landroid/content/Context;

.field mCurrentBatteryLevel:I

.field mHandler:Landroid/os/Handler;

.field mIntentReceiver:Landroid/content/BroadcastReceiver;

.field mPowerManager:Landroid/os/PowerManager;

.field mProcessObserver:Landroid/app/IProcessObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-class v0, Lcom/android/server/ssrm/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/ag;->DEBUG:Z

    sput-boolean v2, Lcom/android/server/ssrm/ag;->io:Z

    sput v2, Lcom/android/server/ssrm/ag;->ip:I

    sput v2, Lcom/android/server/ssrm/ag;->iq:I

    sput-boolean v2, Lcom/android/server/ssrm/ag;->ir:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/ag;->iO:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/ag;->iP:Ljava/util/ArrayList;

    sput-object v3, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    sput-object v3, Lcom/android/server/ssrm/ag;->iR:Lf/a;

    sput-boolean v2, Lcom/android/server/ssrm/ag;->iS:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/ag;->jc:Ljava/lang/StringBuilder;

    sput-object v3, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    const/4 v0, -0x1

    sput v0, Lcom/android/server/ssrm/ag;->ji:I

    sput-object v3, Lcom/android/server/ssrm/ag;->js:Lcom/android/server/ssrm/aG;

    sput-boolean v2, Lcom/android/server/ssrm/ag;->jv:Z

    sput-boolean v2, Lcom/android/server/ssrm/ag;->jw:Z

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lcom/android/server/ssrm/ag;->jD:Ljava/util/Hashtable;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/app/IActivityManager;)V
    .locals 6

    const/16 v1, 0x10

    const/4 v3, -0x1

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-string v0, "Monitor"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/android/server/ssrm/F;

    invoke-direct {v0}, Lcom/android/server/ssrm/F;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ih:Lcom/android/server/ssrm/F;

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->aK:Z

    iput v4, p0, Lcom/android/server/ssrm/ag;->mCurrentBatteryLevel:I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->is:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->it:Ljava/lang/String;

    const/16 v0, 0x2a

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    const/16 v0, 0x3a

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iw:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ix:Ljava/lang/String;

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iy:Ljava/lang/String;

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->SSRM_NOTIFICATION_PERMISSION:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iI:Ljava/lang/String;

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iJ:Ljava/lang/String;

    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iK:Ljava/lang/String;

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->iM:Z

    iput-boolean v5, p0, Lcom/android/server/ssrm/ag;->iN:Z

    const/16 v0, 0x2710

    iput v0, p0, Lcom/android/server/ssrm/ag;->je:I

    new-instance v0, Lcom/android/server/ssrm/aq;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/aq;-><init>(Lcom/android/server/ssrm/ag;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    iput-object v2, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->jh:Z

    const/16 v0, 0x2c

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jj:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jj:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jk:Landroid/content/Intent;

    const/16 v0, 0x20

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jl:Ljava/lang/String;

    iput v3, p0, Lcom/android/server/ssrm/ag;->jm:I

    iput v3, p0, Lcom/android/server/ssrm/ag;->jn:I

    new-instance v0, Ljava/io/File;

    const-string v1, "/sys/class/power_supply/battery/current_avg"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/server/ssrm/ag;->jp:Z

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->jq:Z

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->jr:Z

    new-instance v0, Lcom/android/server/ssrm/ar;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/ar;-><init>(Lcom/android/server/ssrm/ag;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/server/ssrm/as;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/as;-><init>(Lcom/android/server/ssrm/ag;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->mProcessObserver:Landroid/app/IProcessObserver;

    iput-object v2, p0, Lcom/android/server/ssrm/ag;->jt:Landroid/os/DVFSHelper;

    iput-object v2, p0, Lcom/android/server/ssrm/ag;->ju:Lcom/android/server/ssrm/aH;

    iput v4, p0, Lcom/android/server/ssrm/ag;->jx:I

    iput v4, p0, Lcom/android/server/ssrm/ag;->jy:I

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->jz:Z

    iput-object v2, p0, Lcom/android/server/ssrm/ag;->jA:Landroid/os/DVFSHelper;

    iput-object v2, p0, Lcom/android/server/ssrm/ag;->jB:Ld/a;

    new-instance v0, Lcom/android/server/ssrm/aC;

    invoke-direct {v0}, Lcom/android/server/ssrm/aC;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jC:Lcom/android/server/ssrm/aC;

    iput-object p1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->mPowerManager:Landroid/os/PowerManager;

    new-instance v0, Lcom/android/server/ssrm/aH;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/aH;-><init>(Lcom/android/server/ssrm/ag;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ju:Lcom/android/server/ssrm/aH;

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->ju:Lcom/android/server/ssrm/aH;

    const/16 v2, 0x120

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ie:Landroid/app/ActivityManager;

    new-instance v0, Lcom/android/server/ssrm/X;

    invoke-direct {v0, p1}, Lcom/android/server/ssrm/X;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ig:Lcom/android/server/ssrm/X;

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v0}, Lcom/android/server/ssrm/T;->a(Landroid/net/ConnectivityManager;)V

    invoke-virtual {p0}, Lcom/android/server/ssrm/ag;->bl()V

    invoke-virtual {p0}, Lcom/android/server/ssrm/ag;->start()V

    :try_start_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mProcessObserver:Landroid/app/IProcessObserver;

    invoke-interface {p2, v0}, Landroid/app/IActivityManager;->registerProcessObserver(Landroid/app/IProcessObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/android/server/ssrm/ag;->bu()V

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x19

    new-array v1, v1, [I

    fill-array-data v1, :array_c

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sput-boolean v4, Lcom/android/server/ssrm/ag;->iL:Z

    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to registerProcessObserver, e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    iget v0, v1, Landroid/graphics/Point;->x:I

    const/16 v2, 0xa00

    if-ge v0, v2, :cond_1

    iget v0, v1, Landroid/graphics/Point;->y:I

    const/16 v1, 0xa00

    if-lt v0, v1, :cond_2

    :cond_1
    sput-boolean v5, Lcom/android/server/ssrm/N;->eo:Z

    :cond_2
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    const-string v1, "CscFeature_Common_EnableLiveDemo"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/N;->eF:Z

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Feature.LIVE_DEMO = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lcom/android/server/ssrm/N;->eF:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_1
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/server/ssrm/aD;

    invoke-direct {v0, p0}, Lcom/android/server/ssrm/aD;-><init>(Lcom/android/server/ssrm/ag;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    invoke-static {}, Lf/m;->eN()Lf/m;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/server/ssrm/ag;->bd()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lf/m;->a(Landroid/content/Context;Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v0}, Lf/m;->a(Landroid/net/ConnectivityManager;)V

    :cond_3
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/server/ssrm/F;->d(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    invoke-static {}, Lf/a;->ew()Lf/a;

    move-result-object v0

    sput-object v0, Lcom/android/server/ssrm/ag;->iR:Lf/a;

    sget-object v0, Lcom/android/server/ssrm/ag;->iR:Lf/a;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/android/server/ssrm/ag;->iR:Lf/a;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/android/server/ssrm/ag;->be()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lf/a;->a(Landroid/content/Context;Ljava/io/InputStream;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_3
    :try_start_3
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const/16 v1, 0x28

    new-array v1, v1, [I

    fill-array-data v1, :array_d

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/G;->i(I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/container/ContainerAgent2/ContainerAgent2.apk"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    sput-boolean v0, Lcom/android/server/ssrm/ag;->iS:Z

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Knox version : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iS:Z

    if-eqz v0, :cond_5

    const-string v0, "2.0"

    :goto_5
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :catch_1
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v2, "failed to init ssrm setting engine"

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {v4}, Lcom/android/server/ssrm/F;->d(Z)V

    goto :goto_2

    :catch_2
    move-exception v0

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v2, "failed to init dvfs policy"

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_4

    :cond_5
    const-string v0, "1.0"

    goto :goto_5

    nop

    :array_0
    .array-data 4
        0x32
        0x1f
        0x1b
        0xc
        0x3
        0x2f
        0x9
        0x1f
        0x8
        0x29
        0x19
        0x1f
        0x14
        0x1b
        0x8
        0x13
        0x15
    .end array-data

    :array_1
    .array-data 4
        0x3d
        0x32
        0x1f
        0x1b
        0xc
        0x3
        0x2f
        0x9
        0x1f
        0x8
        0x29
        0x19
        0x1f
        0x14
        0x1b
        0x8
        0x13
        0x15
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_3
    .array-data 4
        0x9
        0x1f
        0x19
        0x25
        0x19
        0x15
        0x14
        0xe
        0x1b
        0x13
        0x14
        0x1f
        0x8
        0x25
        0x4b
        0x54
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_4
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0x25
        0x29
        0x2e
        0x3b
        0x2e
        0x2f
        0x29
        0x25
        0x34
        0x3b
        0x37
        0x3f
    .end array-data

    :array_5
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0x25
        0x29
        0x2e
        0x3b
        0x2e
        0x2f
        0x29
        0x25
        0x2c
        0x3b
        0x36
        0x2f
        0x3f
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xa
        0x1f
        0x8
        0x17
        0x13
        0x9
        0x9
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x34
        0x35
        0x2e
        0x33
        0x3c
        0x33
        0x39
        0x3b
        0x2e
        0x33
        0x35
        0x34
        0x25
        0x2a
        0x3f
        0x28
        0x37
        0x33
        0x29
        0x29
        0x33
        0x35
        0x34
    .end array-data

    :array_7
    .array-data 4
        0x28
        0x1f
        0x9
        0xa
        0x15
        0x14
        0x9
        0x1f
        0x3b
        0x2
        0x2e
        0x43
        0x33
        0x14
        0x1c
        0x15
    .end array-data

    :array_8
    .array-data 4
        0x3b
        0x2
        0x2e
        0x43
        0x33
        0x37
        0x3f
        0x54
        0x13
        0x9
        0x2c
        0x13
        0x9
        0x13
        0x18
        0x16
        0x1f
        0x2d
        0x13
        0x14
        0x1e
        0x15
        0xd
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0xa
        0x15
        0xd
        0x1f
        0x8
        0x55
        0xc
        0x15
        0x16
        0x11
        0x1f
        0x3
        0x25
        0xd
        0x1b
        0x11
        0x1f
        0xf
        0xa
    .end array-data

    :array_a
    .array-data 4
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x37
        0x3b
        0x22
        0x25
        0x38
        0x28
        0x33
        0x3d
        0x32
        0x2e
        0x34
        0x3f
        0x29
        0x29
        0x25
        0x39
        0x32
        0x3b
        0x34
        0x3d
        0x3f
        0x3e
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x16
        0x19
        0x1e
        0x55
        0xa
        0x1b
        0x14
        0x1f
        0x16
        0x55
        0x9
        0x13
        0x15
        0xa
        0x25
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x54
        0x1e
        0x13
        0x9
        0x1b
        0x18
        0x16
        0x1f
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0xa
        0x15
        0xd
        0x1f
        0x8
        0x25
        0x9
        0xf
        0xa
        0xa
        0x16
        0x3
        0x55
        0x18
        0x1b
        0xe
        0xe
        0x1f
        0x8
        0x3
        0x55
        0x19
        0x1b
        0xa
        0x1b
        0x19
        0x13
        0xe
        0x3
    .end array-data
.end method

.method private F(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "HotGameControl"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->D(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl0"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "HotGameControl0"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->r(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_2
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl1"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "HotGameControl1"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->s(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_3
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl2"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "HotGameControl2"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->t(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_4
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl3"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "HotGameControl3"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->u(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_5
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl4"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "HotGameControl4"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->v(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_6
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl5"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "HotGameControl5"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->w(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_7
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl6"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "HotGameControl6"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->x(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_8
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl7"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "HotGameControl7"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->y(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_9
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl8"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "HotGameControl8"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->z(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_a
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl9"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "HotGameControl9"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->A(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_b
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControl10"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "HotGameControl10"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->B(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_c
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControlExtreme0"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "HotGameControlExtreme0"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->l(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_d
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControlExtreme1"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string v0, "HotGameControlExtreme1"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->m(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_e
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControlExtreme2"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "HotGameControlExtreme2"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->n(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_f
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControlPerf"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    const-string v0, "HotGameControlPerf"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->o(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_10
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameControlForChina0"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string v0, "HotGameControlForChina0"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->q(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_11
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HotGameGroupForExceptionList"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "HotGameGroupForExceptionList"

    invoke-static {p1}, Lcom/android/server/ssrm/W;->p(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method static K(I)V
    .locals 0

    sput p0, Lcom/android/server/ssrm/ag;->ji:I

    return-void
.end method

.method public static M(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_0

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static T()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/ag;->ip:I

    return v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Landroid/app/IActivityManager;)Lcom/android/server/ssrm/ag;
    .locals 2

    const-class v1, Lcom/android/server/ssrm/ag;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/ssrm/ag;

    invoke-direct {v0, p0, p1}, Lcom/android/server/ssrm/ag;-><init>(Landroid/content/Context;Landroid/app/IActivityManager;)V

    sput-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/Class;)V
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    return-void
.end method

.method private static a(Ljava/lang/Class;Z)V
    .locals 3

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    const-class v0, Lc/a;

    invoke-virtual {p0, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lc/a;->do()Lc/a;

    move-result-object v0

    invoke-static {p0, v0}, Lb/p;->a(Ljava/lang/Class;Lb/p;)V

    :goto_1
    sget-object v0, Lcom/android/server/ssrm/ag;->jc:Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to register FgAppListener class c = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    invoke-static {p0, v0}, Lb/p;->a(Ljava/lang/Class;Lb/p;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;II)V
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/ay;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/android/server/ssrm/ay;->b(Ljava/lang/String;Ljava/lang/String;II)V

    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Z)V
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p0}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p0}, Lf/m;->aC(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p0}, Lf/m;->ay(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p0}, Lf/m;->aC(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p0}, Lf/m;->az(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static aZ()Z
    .locals 1

    sget-boolean v0, Lcom/android/server/ssrm/ag;->io:Z

    return v0
.end method

.method private b(Landroid/content/Context;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiApEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/android/server/ssrm/ag;->jr:Z

    :goto_0
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_1

    const-string v3, "ScreenOnAppStarted"

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->jq:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->jr:Z

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_1
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_1
    return-void

    :cond_2
    iput-boolean v1, p0, Lcom/android/server/ssrm/ag;->jr:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static ba()I
    .locals 1

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    return v0
.end method

.method private static bb()Ljava/io/InputStream;
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    const-string v3, "raw"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static bc()Ljava/io/InputStream;
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v2, Lcom/android/server/ssrm/N;->cy:Ljava/lang/String;

    const-string v3, "raw"

    const-string v4, "android"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static bd()Ljava/io/InputStream;
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x17

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    const/16 v1, 0x1a

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/io/File;

    const/16 v1, 0x15

    new-array v1, v1, [I

    fill-array-data v1, :array_2

    invoke-static {v1}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-boolean v1, Lcom/android/server/ssrm/ag;->DEBUG:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bb()Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/android/server/ssrm/ag;->bb()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v3, :cond_2

    const/16 v0, 0x400

    :try_start_2
    new-array v0, v0, [B

    :goto_1
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_5

    :cond_2
    if-eqz v3, :cond_3

    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_4
    :goto_2
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_9

    :try_start_4
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bb()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v4, 0x0

    :try_start_5
    invoke-virtual {v1, v0, v4, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v3

    :goto_3
    :try_start_6
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    if-eqz v2, :cond_6

    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_6
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_4
    if-eqz v3, :cond_7

    :try_start_8
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_7
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_4

    :cond_8
    :goto_5
    throw v0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_5

    :cond_9
    invoke-static {}, Lcom/android/server/ssrm/ag;->bb()Ljava/io/InputStream;

    move-result-object v0

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_4

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_6
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_3

    nop

    :array_0
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x54
        0x1e
        0x1f
        0x18
        0xf
        0x1d
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x54
        0x15
        0x8
        0x13
        0x1d
        0x13
        0x14
        0x1b
        0x16
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x54
        0x14
        0x1f
        0xd
    .end array-data
.end method

.method private static be()Ljava/io/InputStream;
    .locals 3

    new-instance v1, Ljava/io/File;

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->DEBUG:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/android/server/ssrm/ag;->bc()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/server/ssrm/ag;->bc()Ljava/io/InputStream;

    move-result-object v0

    goto :goto_0

    :array_0
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x1e
        0xc
        0x1c
        0x9
        0x25
        0xa
        0x15
        0x16
        0x13
        0x19
        0x3
        0x54
        0x2
        0x17
        0x16
    .end array-data
.end method

.method private bf()V
    .locals 4

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-class v0, Lb/ao;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lb/aB;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lb/g;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lb/K;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cI:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/L;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cP:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/M;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cM:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/N;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cT:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/P;

    sget-boolean v3, Lcom/android/server/ssrm/N;->dd:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/T;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cU:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/R;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cX:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/W;

    sget-boolean v3, Lcom/android/server/ssrm/N;->dL:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/X;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cF:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/Y;

    sget-boolean v3, Lcom/android/server/ssrm/N;->do:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/an;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cR:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dO:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dI:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dG:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dJ:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dK:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dp:Z

    if-eqz v0, :cond_9

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/Z;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cJ:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ac;

    sget-boolean v3, Lcom/android/server/ssrm/N;->ec:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ae;

    sget-boolean v3, Lcom/android/server/ssrm/N;->dn:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/af;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dl:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/ssrm/N;->dk:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/ssrm/N;->dm:Z

    if-eqz v0, :cond_a

    :cond_1
    move v0, v2

    :goto_1
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ag;

    sget-boolean v3, Lcom/android/server/ssrm/N;->dQ:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ah;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cN:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/aj;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cV:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/al;

    sget-boolean v3, Lcom/android/server/ssrm/N;->de:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/aa;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cY:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/V;

    sget-boolean v3, Lcom/android/server/ssrm/N;->dV:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/ar;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_b

    :cond_2
    move v0, v2

    :goto_2
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/h;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cI:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/aA;

    sget-boolean v3, Lcom/android/server/ssrm/N;->ey:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/at;

    sget-boolean v3, Lcom/android/server/ssrm/N;->eE:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/aw;

    sget-boolean v3, Lcom/android/server/ssrm/N;->ew:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/J;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-nez v0, :cond_4

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->dS:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_c

    :cond_4
    move v0, v2

    :goto_3
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/I;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dZ:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_d

    :cond_5
    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_4
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/ax;

    sget-boolean v0, Lcom/android/server/ssrm/N;->ex:Z

    if-eqz v0, :cond_e

    sget-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    if-eqz v0, :cond_e

    move v0, v2

    :goto_5
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/aq;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v3, Lb/au;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dh:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->cK:Z

    if-eqz v0, :cond_f

    :cond_6
    move v0, v2

    :goto_6
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/i;

    invoke-static {}, Lb/i;->cj()Z

    move-result v3

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ay;

    sget-boolean v3, Lcom/android/server/ssrm/N;->eA:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/d;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cI:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/l;

    sget-boolean v3, Lcom/android/server/ssrm/N;->ek:Z

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/ap;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v3, Lb/J;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dt:Z

    if-eqz v0, :cond_10

    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-eqz v0, :cond_10

    move v0, v2

    :goto_7
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/as;

    sget-boolean v3, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v3, :cond_7

    sget-boolean v3, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v3, :cond_7

    sget-boolean v3, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v3, :cond_7

    sget-boolean v3, Lcom/android/server/ssrm/N;->dx:Z

    if-nez v3, :cond_7

    sget-boolean v3, Lcom/android/server/ssrm/N;->dA:Z

    if-eqz v3, :cond_8

    :cond_7
    move v1, v2

    :cond_8
    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lc/a;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lc/k;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lc/m;

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;)V

    const-class v0, Lc/o;

    sget-boolean v1, Lcom/android/server/ssrm/N;->cW:Z

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/o;

    sget-boolean v1, Lcom/android/server/ssrm/N;->dd:Z

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lb/e;

    sget-boolean v1, Lcom/android/server/ssrm/N;->dd:Z

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/Class;Z)V

    const-class v0, Lc/a;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lc/a;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iT:Lc/a;

    const-class v0, Lc/o;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lc/o;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iU:Lc/o;

    const-class v0, Lb/M;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/M;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iV:Lb/M;

    const-class v0, Lb/L;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/L;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iW:Lb/L;

    const-class v0, Lb/N;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/N;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iX:Lb/N;

    const-class v0, Lb/P;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/P;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iY:Lb/P;

    const-class v0, Lb/T;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/T;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->iZ:Lb/T;

    const-class v0, Lb/R;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/R;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ja:Lb/R;

    const-class v0, Lb/ao;

    invoke-static {v0}, Lb/p;->c(Ljava/lang/Class;)Lb/p;

    move-result-object v0

    check-cast v0, Lb/ao;

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jb:Lb/ao;

    return-void

    :cond_9
    move v0, v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_2

    :cond_c
    move v0, v1

    goto/16 :goto_3

    :cond_d
    move v0, v1

    goto/16 :goto_4

    :cond_e
    move v0, v1

    goto/16 :goto_5

    :cond_f
    move v0, v1

    goto/16 :goto_6

    :cond_10
    move v0, v1

    goto/16 :goto_7
.end method

.method private bh()V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dW:Z

    if-eqz v0, :cond_0

    const-string v0, "BMW"

    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iO:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/server/ssrm/az;

    invoke-direct {v1, p0}, Lcom/android/server/ssrm/az;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private bi()V
    .locals 2

    sget-object v0, Lcom/android/server/ssrm/ag;->iP:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jC:Lcom/android/server/ssrm/aC;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public static bj()Lcom/android/server/ssrm/ag;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    return-object v0
.end method

.method private bm()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/android/server/ssrm/aT;->bH()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.camera"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    const-string v0, "CameraRecording"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    const-string v0, "CameraShooting"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method private bp()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->ix:Ljava/lang/String;

    sget-object v2, Lb/at;->tn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iy:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PackageName"

    const-string v2, "android.av.media.libstagefright"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PID"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method private bq()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->ix:Ljava/lang/String;

    sget-object v2, Lb/at;->tn:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iy:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PackageName"

    const-string v2, "android.av.media.libstagefright"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PID"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method private br()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/android/server/ssrm/G;->i(Z)V

    invoke-static {v2}, Lcom/android/server/ssrm/T;->i(Z)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, v2}, Lf/m;->ak(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->io:Z

    if-eqz v0, :cond_2

    const-string v0, "ScreenOffInCall"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "ScreenOff"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method private static bs()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/server/ssrm/G;->g(Z)V

    return-void
.end method

.method private static bt()V
    .locals 2

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/server/ssrm/G;->g(Z)V

    const-string v0, "ro.product.name"

    const-string v1, "default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ms013gzm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ms013gzn"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/X;->aV()V

    :cond_0
    return-void
.end method

.method private d(Landroid/content/Intent;)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPackageReplaced: packageName = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/server/ssrm/ag;->bn()V

    :cond_0
    return-void
.end method

.method protected static dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 9

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_7

    invoke-static {}, Lcom/android/server/ssrm/G;->isPowerSavingMode()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "O"

    :goto_0
    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "O"

    :goto_1
    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "O"

    :goto_2
    invoke-static {}, Lcom/android/server/ssrm/G;->ag()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "O"

    :goto_3
    invoke-static {}, Lcom/android/server/ssrm/G;->ah()Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "O"

    :goto_4
    invoke-static {}, Lcom/android/server/ssrm/G;->ai()Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v5, "O"

    :goto_5
    invoke-static {}, Lcom/android/server/ssrm/G;->aj()Z

    move-result v6

    if-eqz v6, :cond_6

    const-string v6, "O"

    :goto_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Basic Informations : PS:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, " / UPS:"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / EM:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / UC:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / PC:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / AC:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / KX:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, p1}, Lf/m;->dump(Ljava/io/PrintWriter;)V

    :goto_7
    return-void

    :cond_0
    const-string v0, "X"

    goto/16 :goto_0

    :cond_1
    const-string v1, "X"

    goto/16 :goto_1

    :cond_2
    const-string v2, "X"

    goto/16 :goto_2

    :cond_3
    const-string v3, "X"

    goto/16 :goto_3

    :cond_4
    const-string v4, "X"

    goto :goto_4

    :cond_5
    const-string v5, "X"

    goto :goto_5

    :cond_6
    const-string v6, "X"

    goto :goto_6

    :cond_7
    invoke-static {p1}, Lcom/android/server/ssrm/T;->d(Ljava/io/PrintWriter;)V

    goto :goto_7
.end method

.method private e(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "GMS_INTENT_EXIST"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    const-string v1, "GMS_BUNDLING_BLACK_LIST"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->jb:Lb/ao;

    invoke-virtual {v2, v0, v1}, Lb/ao;->a(ZLjava/lang/String;)V

    return-void
.end method

.method private f(Landroid/content/Intent;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->ix:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->iy:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    const-string v3, "PackageName"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    if-eqz v1, :cond_4

    sget-object v3, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BroadcastReceiver::onReceive SSRM_STATUS_NOTIFY with statusName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "  statusValue ="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v3, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v3, v1, v2}, Lf/m;->b(Ljava/lang/String;Z)V

    :cond_0
    sget-object v3, Lb/ar;->tf:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iput-boolean v2, p0, Lcom/android/server/ssrm/ag;->jq:Z

    :cond_1
    sget-boolean v1, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v1, :cond_4

    const-string v1, "ScreenOnAppStarted"

    iget-boolean v2, p0, Lcom/android/server/ssrm/ag;->jq:Z

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/android/server/ssrm/ag;->jr:Z

    if-eqz v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    :cond_3
    invoke-static {v1, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_4
    return-void
.end method

.method private g(Landroid/content/Intent;)V
    .locals 4

    const-string v0, "com.samsung.ssrm.COVER_OPEN"

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.ssrm.COVER_OPEN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "coverOpen"

    const-string v2, "coverOpen"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method public static getMainLooper()Landroid/os/Looper;
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    goto :goto_0
.end method

.method private static h(Landroid/content/Intent;)V
    .locals 2

    const-string v0, "state"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "enable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/android/server/ssrm/X;->r(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "disable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/android/server/ssrm/X;->r(Z)V

    goto :goto_0
.end method

.method private i(Landroid/content/Intent;)V
    .locals 8

    const/16 v7, -0x3e7

    const/4 v6, -0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "level"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Lcom/android/server/ssrm/G;->i(I)V

    invoke-static {}, Lcom/android/server/ssrm/F;->getBatteryLevel()I

    move-result v0

    if-eq v0, v7, :cond_7

    :goto_0
    iget v1, p0, Lcom/android/server/ssrm/ag;->jy:I

    if-eq v1, v0, :cond_0

    const-string v1, "battery"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/android/server/ssrm/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/android/server/ssrm/T;->az()V

    iput v0, p0, Lcom/android/server/ssrm/ag;->jy:I

    iget v0, p0, Lcom/android/server/ssrm/ag;->jy:I

    const/16 v1, 0x55

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/X;->q(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    const-string v0, "dev.ssrm.live_thumbnail"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_1
    const-string v0, "plugged"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const/4 v0, 0x2

    if-ne v1, v0, :cond_4

    invoke-static {v2}, Lcom/android/server/ssrm/G;->f(Z)V

    invoke-static {v2}, Lb/p;->H(Z)V

    :goto_2
    if-ne v1, v2, :cond_5

    move v0, v2

    :goto_3
    invoke-static {v0}, Lcom/android/server/ssrm/G;->h(Z)V

    if-ne v1, v2, :cond_6

    :goto_4
    invoke-static {v2}, Lcom/android/server/ssrm/T;->k(Z)V

    const-string v0, "temperature"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "temperature"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    sput v0, Lcom/android/server/ssrm/ag;->iq:I

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    if-eq v0, v6, :cond_2

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    invoke-static {v0}, Lcom/android/server/ssrm/T;->t(I)V

    invoke-static {}, Lcom/android/server/ssrm/F;->U()I

    move-result v0

    if-eq v0, v7, :cond_1

    sput v0, Lcom/android/server/ssrm/ag;->iq:I

    :cond_1
    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    invoke-static {v0}, Lcom/android/server/ssrm/G;->j(I)V

    :cond_2
    const-string v0, "status"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/G;->k(I)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0, v2}, Lcom/android/server/ssrm/X;->q(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->ig:Lcom/android/server/ssrm/X;

    invoke-virtual {v0}, Lcom/android/server/ssrm/X;->aW()V

    const-string v0, "dev.ssrm.live_thumbnail"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-static {v3}, Lcom/android/server/ssrm/G;->f(Z)V

    invoke-static {v3}, Lb/p;->H(Z)V

    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_4

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method private onBootCompleted()V
    .locals 5

    const v3, 0x927c0

    const/4 v4, 0x1

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->aK:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/aU;->bN()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "onReceive:: ACTION_BOOT_COMPLETED is received."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->aK:Z

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iK:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/android/server/ssrm/ag;->iM:Z

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/aU;->bC()V

    new-instance v0, Lcom/android/server/ssrm/aI;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/ssrm/aI;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ii:Lcom/android/server/ssrm/aI;

    new-instance v0, Lcom/samsung/android/cover/CoverManager;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/cover/CoverManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->if:Lcom/samsung/android/cover/CoverManager;

    new-instance v0, Lcom/android/server/ssrm/J;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/ssrm/J;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ij:Lcom/android/server/ssrm/J;

    sget-boolean v0, Lcom/android/server/ssrm/N;->eD:Z

    if-eqz v0, :cond_3

    new-instance v0, Lcom/android/server/ssrm/a;

    invoke-direct {v0}, Lcom/android/server/ssrm/a;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->ik:Lcom/android/server/ssrm/a;

    :cond_3
    new-instance v0, Lcom/android/server/ssrm/aA;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, v2}, Lcom/android/server/ssrm/aA;-><init>(Lcom/android/server/ssrm/ag;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->il:Lcom/android/server/ssrm/aA;

    invoke-static {}, Lcom/android/server/ssrm/aL;->bz()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lcom/android/server/ssrm/ag;->c(IZ)V

    :cond_4
    const-string v0, "dev.knoxapp.running"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/server/ssrm/ag;->bn()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->eo:Z

    if-nez v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->ec:Z

    if-eqz v0, :cond_6

    :cond_5
    invoke-static {}, Lcom/android/server/ssrm/y;->K()V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->DEBUG:Z

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/android/server/ssrm/F;->Z()V

    :cond_6
    sget-boolean v0, Lcom/android/server/ssrm/N;->cD:Z

    if-nez v0, :cond_8

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-nez v0, :cond_7

    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-eqz v0, :cond_8

    :cond_7
    new-instance v0, La/a;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, La/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->in:La/a;

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->in:La/a;

    invoke-virtual {v0}, La/a;->start()V

    :cond_8
    const-string v0, "FINISH"

    const-string v1, "persist.sys.setupwizard"

    const-string v2, "FINISH"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "Unboxing overheat proteciton will be held for 10 min."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNBOX_CPU_MAX"

    const v1, 0x155cc0

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/d;->acquire(I)V

    :cond_9
    :goto_1
    const-string v0, "/data/system/hgl_test.xml"

    new-instance v0, Ljava/io/File;

    const-string v1, "/data/system/hgl_test.xml"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_a

    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/data/system/hgl_test.xml"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_a
    :goto_2
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0x9

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/DataTputMonitor;->init(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Le/a;->a(Landroid/content/Context;Landroid/os/Handler;)V

    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v0

    if-eq v0, v4, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/z;->create()V

    goto/16 :goto_0

    :cond_b
    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v0, :cond_c

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "Unboxing overheat proteciton will be held for 10 min."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNBOX_CPU_MAX"

    const v1, 0x180600

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/d;->acquire(I)V

    goto :goto_1

    :cond_c
    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_d

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "Unboxing overheat proteciton will be held for 10 min."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNBOX_CPU_MAX"

    const v1, 0x19f0a0

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/d;->acquire(I)V

    goto :goto_1

    :cond_d
    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_9

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "Unboxing overheat proteciton will be held for 10 min."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "UNBOX_CPU_MAX"

    const v1, 0x16da00

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->b(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/d;->acquire(I)V

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_2
.end method

.method private onScreenOn()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-static {v2}, Lcom/android/server/ssrm/G;->i(Z)V

    invoke-static {v2}, Lcom/android/server/ssrm/T;->i(Z)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, v2}, Lf/m;->ak(Z)V

    const-string v0, "ScreenOffInCall"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    const-string v0, "ScreenOff"

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->eu:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/Q;->a(Landroid/content/Context;)Lcom/android/server/ssrm/Q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/ssrm/Q;->onScreenOn()V

    :cond_2
    const-string v0, "com.samsung.ssrm.SCREEN_ON"

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.ssrm.SCREEN_ON"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void
.end method

.method public static s(Z)V
    .locals 3

    sput-boolean p0, Lcom/android/server/ssrm/ag;->ir:Z

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sSetDisableBrightnessControlForOctaTest :: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static sendMessage(Landroid/os/Message;)V
    .locals 1

    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    iget-boolean v0, v0, Lcom/android/server/ssrm/ag;->aK:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/server/ssrm/ag;->aH:Lcom/android/server/ssrm/ag;

    iget-object v0, v0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method static u(Z)V
    .locals 2

    const-string v0, "hf"

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "jf"

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "jm"

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p0, :cond_2

    const-string v0, "debug.mdpcomp.idletime"

    const-string v1, "5"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "debug.mdpcomp.idletime"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static w(Z)V
    .locals 3

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onNetworkBoosterEnabled:: enabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "NetworkBooster"

    invoke-virtual {v0, v1, p0}, Lf/m;->b(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method G(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    sget-boolean v1, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/android/server/ssrm/aU;->a(ILjava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    sput-boolean v3, Lcom/android/server/ssrm/ag;->jw:Z

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->it:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    :goto_0
    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sput-boolean v3, Lcom/android/server/ssrm/ag;->jv:Z

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->is:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    sget-boolean v1, Lcom/android/server/ssrm/ag;->jw:Z

    if-eqz v1, :cond_0

    sput-boolean v2, Lcom/android/server/ssrm/ag;->jw:Z

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->it:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/ag;->jv:Z

    if-eqz v0, :cond_1

    sput-boolean v2, Lcom/android/server/ssrm/ag;->jv:Z

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->is:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method public J(I)V
    .locals 1

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->jh:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    iput p1, v0, Lcom/android/server/ssrm/aD;->jW:I

    :cond_0
    return-void
.end method

.method L(I)V
    .locals 3

    iget v0, p0, Lcom/android/server/ssrm/ag;->jm:I

    if-eq p1, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/android/server/ssrm/aT;->bH()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".POWER_STRETCH"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "value"

    iget v2, p0, Lcom/android/server/ssrm/ag;->jn:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    iput p1, p0, Lcom/android/server/ssrm/ag;->jm:I

    return-void
.end method

.method a(Ljava/io/InputStream;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Ld/c;->b(Ljava/io/InputStream;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Ld/a;->b(Ljava/util/ArrayList;)V

    new-instance v0, Ld/a;

    invoke-direct {v0}, Ld/a;-><init>()V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->jB:Ld/a;

    sget-object v0, Lcom/android/server/ssrm/ag;->iO:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jB:Ld/a;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method a(Ljava/lang/String;II)V
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/aT;->bH()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.android.phone"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v2, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onForegroundAppChanged:: packageName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pid = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v2, Lcom/android/server/ssrm/ag;->iS:Z

    if-eqz v2, :cond_2

    invoke-static {p3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    const/16 v3, 0x64

    if-lt v2, v3, :cond_3

    sget-object v2, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v3, "onForegroundAppChanged:: dev.knoxapp.running set true "

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "dev.knoxapp.running"

    const-string v3, "true"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v1, Lcom/android/server/ssrm/G;->bX:Z

    :cond_2
    :goto_1
    invoke-static {p1}, Lcom/android/server/ssrm/aT;->M(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/android/server/ssrm/aT;->N(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/android/server/ssrm/aT;->bH()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onForegroundAppChanged::ignored packageName = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v3, "onForegroundAppChanged:: dev.knoxapp.running set false "

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "dev.knoxapp.running"

    const-string v3, "false"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    sput-boolean v0, Lcom/android/server/ssrm/G;->bX:Z

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bm()V

    invoke-static {v2}, Lcom/android/server/ssrm/aT;->L(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/server/ssrm/aT;->T(I)V

    const-string v3, "package"

    invoke-static {v3, v2}, Lcom/android/server/ssrm/T;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v3, v2, p2}, Lb/p;->a(Landroid/content/Context;Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v3, v2, p2}, Lcom/android/server/ssrm/be;->a(Landroid/content/Context;Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->il:Lcom/android/server/ssrm/aA;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->il:Lcom/android/server/ssrm/aA;

    invoke-virtual {v3, v2}, Lcom/android/server/ssrm/aA;->J(Ljava/lang/String;)V

    :cond_5
    iget-object v3, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/server/ssrm/ag;->iN:Z

    sget-boolean v3, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v3, :cond_8

    sget-object v3, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v3, :cond_8

    const-string v3, "PreloadedApps"

    iget-boolean v4, p0, Lcom/android/server/ssrm/ag;->iN:Z

    if-nez v4, :cond_6

    sget-object v4, Lb/l;->oE:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    :cond_6
    move v0, v1

    :cond_7
    invoke-static {v3, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    invoke-direct {p0, v2}, Lcom/android/server/ssrm/ag;->F(Ljava/lang/String;)V

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0, v2}, Lf/m;->aA(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p0, v2}, Lcom/android/server/ssrm/ag;->G(Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_b

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v3, "AppStart"

    invoke-virtual {v0, v3}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jB:Ld/a;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jB:Ld/a;

    invoke-virtual {v0, v2}, Ld/a;->aj(Ljava/lang/String;)V

    :cond_a
    sget-object v0, Lcom/android/server/ssrm/ag;->iP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/ssrm/aF;

    invoke-interface {v0, v2, p2, p3}, Lcom/android/server/ssrm/aF;->b(Ljava/lang/String;II)V

    goto :goto_3

    :cond_b
    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-eqz v0, :cond_c

    const-string v0, "com.sec.android.app.camera"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_c
    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    if-lez v0, :cond_d

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    const/16 v3, 0x1f4

    if-ge v0, v3, :cond_d

    invoke-static {}, Lcom/android/server/ssrm/G;->ad()I

    move-result v0

    const/16 v3, 0x14

    if-lt v0, v3, :cond_d

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_d

    const-string v0, "AppStart"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_d
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x5

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget v3, v0, Landroid/os/Message;->what:I

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x1770

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    :cond_e
    invoke-static {v2}, Le/a;->J(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public b(IZ)V
    .locals 1

    iput-boolean p2, p0, Lcom/android/server/ssrm/ag;->jh:Z

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->jh:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    iput p1, v0, Lcom/android/server/ssrm/aD;->jW:I

    :cond_0
    return-void
.end method

.method b(Landroid/content/Intent;)V
    .locals 3

    invoke-static {}, Lcom/android/server/ssrm/aG;->bw()Lcom/android/server/ssrm/aG;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->iJ:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v1, Lcom/android/server/ssrm/aG;->kc:Z

    iget-boolean v0, v1, Lcom/android/server/ssrm/aG;->ka:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v2, "onSIPIntentReceived:: SIP visility was updated more than two times in 100ms."

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, v1, Lcom/android/server/ssrm/aG;->ka:Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v2, Lcom/android/server/ssrm/at;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/at;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-direct {v0, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public bg()V
    .locals 4

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "SSRM Handler Thread"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    sget-object v0, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v0, Lcom/android/server/ssrm/ah;

    sget-object v1, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/server/ssrm/ah;-><init>(Lcom/android/server/ssrm/ag;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bf()V

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bh()V

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bi()V

    invoke-static {}, Lcom/android/server/ssrm/F;->W()V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->DEBUG:Z

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/F;->X()V

    invoke-static {}, Lcom/android/server/ssrm/F;->Y()V

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/aL;->bz()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/android/server/ssrm/aL;->bA()I

    move-result v0

    sput v0, Lcom/android/server/ssrm/ag;->ip:I

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PreMonitor.getLastBatteryTemperature:: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/android/server/ssrm/ag;->ip:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jf:Ljava/lang/Runnable;

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->jg:Lcom/android/server/ssrm/aD;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->eG:Z

    if-eqz v0, :cond_0

    new-instance v0, Lg/a;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, v1, v2}, Lg/a;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/android/server/ssrm/ag;->im:Lg/a;

    goto/16 :goto_0
.end method

.method declared-synchronized bk()Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->ei:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    :cond_1
    :try_start_1
    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    sget-object v3, Lcom/android/server/ssrm/N;->eh:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/T;->s(I)V

    rem-int/lit8 v3, v0, 0xa

    sub-int v3, v0, v3

    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->iN:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/ag;->jv:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/ag;->jw:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_7

    add-int/lit8 v3, v3, 0x28

    :cond_2
    :goto_1
    invoke-static {}, Lcom/android/server/ssrm/F;->T()I

    move-result v0

    const/16 v4, -0x3e7

    if-eq v0, v4, :cond_3

    move v3, v0

    :cond_3
    sget v4, Lcom/android/server/ssrm/ag;->ip:I

    sput v3, Lcom/android/server/ssrm/ag;->ip:I

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    if-gez v0, :cond_4

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    sput v0, Lcom/android/server/ssrm/ag;->ip:I

    :cond_4
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_5

    const-string v5, "LowTemp"

    sget v0, Lcom/android/server/ssrm/ag;->iq:I

    if-gez v0, :cond_9

    move v0, v2

    :goto_2
    invoke-static {v5, v0}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_5
    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_6

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_6

    :try_start_2
    iget-boolean v0, p0, Lcom/android/server/ssrm/ag;->jp:Z

    if-eqz v0, :cond_a

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v5, "/sys/class/power_supply/battery/current_avg"

    invoke-static {v0, v5}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    :goto_3
    :try_start_3
    sget-object v5, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    sget v6, Lcom/android/server/ssrm/ag;->iq:I

    invoke-virtual {v5, v3, v6, v0}, Lf/m;->a(III)I

    move-result v0

    sput v0, Lcom/android/server/ssrm/ag;->ip:I

    :cond_6
    sget v0, Lcom/android/server/ssrm/ag;->ip:I

    if-eq v4, v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_7
    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-eqz v0, :cond_8

    add-int/lit8 v3, v3, 0x14

    goto :goto_1

    :cond_8
    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->if:Lcom/samsung/android/cover/CoverManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->if:Lcom/samsung/android/cover/CoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/cover/CoverManager;->getCoverState()Lcom/samsung/android/cover/CoverState;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/cover/CoverState;->getType()I

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->M(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/aT;->bH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/W;->o(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v3, v3, -0xa

    goto :goto_1

    :cond_9
    move v0, v1

    goto :goto_2

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_3

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_a
    move v0, v1

    goto :goto_3
.end method

.method public bl()V
    .locals 11

    const/4 v4, 0x0

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_PRESENT"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USB_CHARGING"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->iI:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.android.systemui.statusbar.EXPANDED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.android.systemui.statusbar.COLLAPSED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.samsung.cover.OPEN"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SECURE_PLAYBACK_START"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.SECURE_PLAYBACK_STOP"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.samsung.pen.INSERT"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.START_NETWORK_BOOSTER"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.STOP_NETWORK_BOOSTER"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.sec.android.intent.action.EMERGENCY_MODE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.sec.android.intent.action.POWER_SAVING_MODE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.sec.android.intent.action.ULTRA_POWER_SAVING_MODE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.samsung.check.update.schedule"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.android.systemui.power.action.ACTION_REQUEST_SHUTDOWN"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.android.systemui.power.action.ACTION_CLEAR_SHUTDOWN"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.BMW_SHUTDOWN_START"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->iw:Ljava/lang/String;

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    iget-object v9, p0, Lcom/android/server/ssrm/ag;->SSRM_NOTIFICATION_PERMISSION:Ljava/lang/String;

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.sec.intent.ACTION.SSRM_POLICY_RESULT"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v9, "android.permission.DEVICE_POWER"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v8, Landroid/content/IntentFilter;

    invoke-direct {v8}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android_secret_code"

    invoke-virtual {v8, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v7, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v9, "com.sec.factory.permission.KEYSTRING"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "sec_container_1.com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    return-void
.end method

.method bn()V
    .locals 3

    sget-boolean v0, Lcom/android/server/ssrm/N;->et:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "requestWlucPolicyUpdate::"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.intent.ACTION.SSRM_POLICY_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "VERSION"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.DEVICE_POWER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method bo()V
    .locals 3

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "requestHglPolicyUpdate::"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.intent.ACTION.SSRM_HGL_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "CHIPNAME"

    const-string v2, "ro.chipname"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PRODUCT_MODEL"

    const-string v2, "ro.product.model"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "PRODUCT_DEVICE"

    const-string v2, "ro.product.device"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SSRM_FILE_NAME"

    sget-object v2, Lcom/android/server/ssrm/N;->cx:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.DEVICE_POWER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    return-void
.end method

.method bu()V
    .locals 4

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "TYPE_SCROLL"

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->jC:Lcom/android/server/ssrm/aC;

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "TYPE_WINDOW_ORIENTATION"

    new-instance v2, Lcom/android/server/ssrm/au;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/au;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "FINGER_HOVER_ON"

    new-instance v2, Lcom/android/server/ssrm/av;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/av;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "FINGER_HOVER_OFF"

    new-instance v2, Lcom/android/server/ssrm/aw;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/aw;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "MULTIVERSE_FINGER_HOVER"

    new-instance v2, Lcom/android/server/ssrm/ax;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/ax;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "MULTIVERSE_SIDE_KEY"

    new-instance v2, Lcom/android/server/ssrm/ai;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/ai;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "CAMERA_SIDE_KEY"

    new-instance v2, Lcom/android/server/ssrm/aj;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/aj;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "SBROWSER_PAGE_LOADING"

    new-instance v2, Lcom/android/server/ssrm/ak;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/ak;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "SBROWSER_DASH_MODE"

    new-instance v2, Lcom/android/server/ssrm/al;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/al;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x2d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v2, "REQ_DROP_CACHE"

    new-instance v3, Lcom/android/server/ssrm/an;

    invoke-direct {v3, p0, v0}, Lcom/android/server/ssrm/an;-><init>(Lcom/android/server/ssrm/ag;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v0, 0x28

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v2, "BUS_DCVS_GOVERNOR"

    new-instance v3, Lcom/android/server/ssrm/ao;

    invoke-direct {v3, p0, v0}, Lcom/android/server/ssrm/ao;-><init>(Lcom/android/server/ssrm/ag;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/android/server/ssrm/ag;->bq:Ljava/util/Hashtable;

    const-string v1, "GENERAL_SHELL"

    new-instance v2, Lcom/android/server/ssrm/ap;

    invoke-direct {v2, p0}, Lcom/android/server/ssrm/ap;-><init>(Lcom/android/server/ssrm/ag;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0xc
        0x13
        0x8
        0xe
        0xf
        0x1b
        0x16
        0x55
        0x9
        0x1f
        0x19
        0x55
        0x9
        0x1f
        0x19
        0x25
        0x17
        0x13
        0x9
        0x19
        0x55
        0x1e
        0x8
        0x15
        0xa
        0x25
        0x19
        0x1b
        0x19
        0x12
        0x1f
        0x9
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x4a
        0x54
        0xb
        0x19
        0x15
        0x17
        0x56
        0x19
        0xa
        0xf
        0x18
        0xd
        0x55
        0x1d
        0x15
        0xc
        0x1f
        0x8
        0x14
        0x15
        0x8
    .end array-data
.end method

.method final c(IZ)V
    .locals 3

    const-string v0, "max_brightness"

    const-string v0, "change_type"

    sget v0, Lcom/android/server/ssrm/ag;->ji:I

    if-ne v0, p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    sput p1, Lcom/android/server/ssrm/ag;->ji:I

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "notifyLimitBrightness() : brightness : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isBootCompleted = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/server/ssrm/ag;->aK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jk:Landroid/content/Intent;

    const-string v1, "max_brightness"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    sget v0, Lcom/android/server/ssrm/ag;->ip:I

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jk:Landroid/content/Intent;

    const-string v1, "change_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_0
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->jk:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/android/server/ssrm/ag;->jk:Landroid/content/Intent;

    const-string v1, "change_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method c(Landroid/content/Intent;)V
    .locals 8

    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, p1}, Lb/p;->b(Landroid/content/Context;Landroid/content/Intent;)V

    sget-boolean v3, Lcom/android/server/ssrm/N;->ez:Z

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/be;->bW()Lcom/android/server/ssrm/be;

    move-result-object v3

    invoke-virtual {v3, v1, p1}, Lcom/android/server/ssrm/be;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_0
    sget-boolean v3, Lcom/android/server/ssrm/N;->eD:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->ik:Lcom/android/server/ssrm/a;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->ik:Lcom/android/server/ssrm/a;

    invoke-virtual {v3, v1, p1}, Lcom/android/server/ssrm/a;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    :cond_1
    iget-object v3, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v3, p1}, Lcom/android/server/ssrm/aU;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    const-string v3, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->onBootCompleted()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/ag;->i(Landroid/content/Intent;)V

    goto :goto_0

    :cond_4
    const-string v3, "android.intent.action.USB_CHARGING"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {p1}, Lcom/android/server/ssrm/ag;->h(Landroid/content/Intent;)V

    goto :goto_0

    :cond_5
    const-string v3, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Lcom/android/server/ssrm/ag;->bt()V

    goto :goto_0

    :cond_6
    const-string v3, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-static {}, Lcom/android/server/ssrm/ag;->bs()V

    goto :goto_0

    :cond_7
    const-string v3, "android.intent.action.SCREEN_ON"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->onScreenOn()V

    goto :goto_0

    :cond_8
    const-string v3, "com.samsung.cover.OPEN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/ag;->g(Landroid/content/Intent;)V

    goto :goto_0

    :cond_9
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->br()V

    goto :goto_0

    :cond_a
    iget-object v3, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    iget-object v3, p0, Lcom/android/server/ssrm/ag;->iw:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    :cond_b
    invoke-direct {p0, p1}, Lcom/android/server/ssrm/ag;->f(Landroid/content/Intent;)V

    goto :goto_0

    :cond_c
    iget-object v3, p0, Lcom/android/server/ssrm/ag;->iI:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {p0, p1}, Lcom/android/server/ssrm/ag;->b(Landroid/content/Intent;)V

    goto :goto_0

    :cond_d
    const-string v3, "android.intent.action.SECURE_PLAYBACK_START"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bq()V

    goto :goto_0

    :cond_e
    const-string v3, "android.intent.action.SECURE_PLAYBACK_STOP"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-direct {p0}, Lcom/android/server/ssrm/ag;->bp()V

    goto/16 :goto_0

    :cond_f
    const-string v3, "android.net.conn.TETHER_STATE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-direct {p0, v1}, Lcom/android/server/ssrm/ag;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_10
    const-string v1, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    sget-boolean v0, Lcom/android/server/ssrm/N;->eu:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/Q;->a(Landroid/content/Context;)Lcom/android/server/ssrm/Q;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/server/ssrm/Q;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_11
    const-string v1, "android.intent.action.START_NETWORK_BOOSTER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, "android.intent.action.STOP_NETWORK_BOOSTER"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_12
    const-string v0, "START"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->w(Z)V

    goto/16 :goto_0

    :cond_13
    const-string v1, "android.intent.action.USER_FOREGROUND"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.sec.intent.ACTION.SSRM_POLICY_RESULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/ag;->e(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_14
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-direct {p0, p1}, Lcom/android/server/ssrm/ag;->d(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_15
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, "wifi_state"

    const/4 v2, -0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sget-object v2, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "WLUC wifi conntection state = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v2, Lcom/android/server/ssrm/N;->eG:Z

    if-eqz v2, :cond_2

    if-ne v1, v5, :cond_2

    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/ag;->x(Z)V

    goto/16 :goto_0

    :cond_16
    const-string v1, "com.samsung.check.update.schedule"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    sget-boolean v0, Lcom/android/server/ssrm/N;->eG:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x62

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_17
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_18

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    if-eqz v0, :cond_18

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    invoke-virtual {v0}, Lf/m;->aC()V

    goto/16 :goto_0

    :cond_18
    invoke-static {}, Lcom/android/server/ssrm/T;->aC()V

    goto/16 :goto_0

    :cond_19
    const-string v1, "android.intent.action.BMW_SHUTDOWN_START"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "DegasBmwShutdown: device will be shutdown now"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :cond_1a
    const-string v1, "android.intent.action.EMERGENCY_STATE_CHANGED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    const-string v1, "reason"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v4, :cond_1b

    if-eq v1, v5, :cond_1b

    if-eq v1, v6, :cond_1b

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    :cond_1b
    if-eq v1, v4, :cond_1c

    if-ne v1, v6, :cond_1d

    :cond_1c
    const/4 v0, 0x1

    :cond_1d
    invoke-virtual {p0, v0}, Lcom/android/server/ssrm/ag;->v(Z)V

    goto/16 :goto_0

    :cond_1e
    const-string v1, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    const-string v1, "com.sec.android.extra.MULTIWINDOW_RUNNING"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v7}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v7, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0

    :cond_1f
    const-string v0, "com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_20

    const-string v0, "sec_container_1.com.sec.android.intent.action.DVFS_POLARIS_PDF_VIEW"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_20
    invoke-virtual {p0, p1}, Lcom/android/server/ssrm/ag;->j(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method j(Landroid/content/Intent;)V
    .locals 3

    const-string v0, "ENABLE"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/server/ssrm/ag;->jz:Z

    :goto_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->ix:Ljava/lang/String;

    const-string v2, "Polaris_cpuLock"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->iy:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/android/server/ssrm/ag;->jz:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PackageName"

    const-string v2, "null"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/ssrm/ag;->jz:Z

    goto :goto_0
.end method

.method public onInit(I)V
    .locals 0

    return-void
.end method

.method t(Z)V
    .locals 4

    sget-object v1, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    iget-object v2, p0, Lcom/android/server/ssrm/ag;->jl:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method v(Z)V
    .locals 3

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onEmergencySettingChanged = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lcom/android/server/ssrm/ag;->iL:Z

    if-eqz v0, :cond_0

    const-string v0, "EmergencyChange"

    invoke-static {v0, p1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method x(Z)V
    .locals 4

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "WLUC trial success."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput v2, p0, Lcom/android/server/ssrm/ag;->jx:I

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    const/16 v1, 0x62

    iput v1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/android/server/ssrm/ag;->jx:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/server/ssrm/ag;->jx:I

    iget v0, p0, Lcom/android/server/ssrm/ag;->jx:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "WLUC connection try..... "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/server/ssrm/ag;->jx:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x7

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/android/server/ssrm/ag;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/android/server/ssrm/ag;->jx:I

    sget-object v0, Lcom/android/server/ssrm/ag;->TAG:Ljava/lang/String;

    const-string v1, "WLUC failed."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
