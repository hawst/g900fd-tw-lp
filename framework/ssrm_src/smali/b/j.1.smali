.class public abstract Lb/j;
.super Ljava/lang/Object;


# instance fields
.field protected oh:I

.field final synthetic oi:Lb/i;


# direct methods
.method protected constructor <init>(Lb/i;)V
    .locals 0

    iput-object p1, p0, Lb/j;->oi:Lb/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Lb/j;->cm()V

    return-void
.end method


# virtual methods
.method protected aa(I)V
    .locals 4

    sget-object v0, Lb/i;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSysFSFile: fps = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/p;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lb/j;->co()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/j;->oi:Lb/i;

    invoke-virtual {v0, p1}, Lb/i;->Z(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lb/i;->TAG:Ljava/lang/String;

    invoke-virtual {p0}, Lb/j;->co()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lc/a;->do()Lc/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lc/a;->do()Lc/a;

    move-result-object v0

    invoke-virtual {p0}, Lb/j;->co()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lc/a;->d(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lb/i;->TAG:Ljava/lang/String;

    const-string v1, "Sys FS file wasn\'t updated!!!"

    invoke-static {v0, v1}, Lb/p;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public cl()V
    .locals 0

    invoke-virtual {p0}, Lb/j;->cn()V

    return-void
.end method

.method public cm()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lb/j;->oh:I

    return-void
.end method

.method protected cn()V
    .locals 2

    invoke-virtual {p0}, Lb/j;->cp()I

    move-result v0

    iget v1, p0, Lb/j;->oh:I

    if-eq v1, v0, :cond_0

    invoke-virtual {p0, v0}, Lb/j;->aa(I)V

    iput v0, p0, Lb/j;->oh:I

    :cond_0
    return-void
.end method

.method public abstract co()Ljava/lang/String;
.end method

.method protected cp()I
    .locals 3

    const/4 v0, 0x0

    const/4 v1, 0x1

    sget-boolean v2, Lb/i;->oa:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget v2, Lb/i;->nO:I

    if-lez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lb/j;->oi:Lb/i;

    iget-boolean v2, v2, Lb/i;->nP:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lb/j;->oi:Lb/i;

    iget-boolean v2, v2, Lb/i;->nQ:Z

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lb/j;->oi:Lb/i;

    iget-boolean v2, v2, Lb/i;->nR:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb/j;->oi:Lb/i;

    iget-boolean v2, v2, Lb/i;->nS:Z

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public initialize()V
    .locals 0

    invoke-virtual {p0}, Lb/j;->cn()V

    return-void
.end method
