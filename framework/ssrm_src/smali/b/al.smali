.class public final Lb/al;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/E;
.implements Lb/t;


# static fields
.field static sF:Ljava/util/HashMap;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field pE:Z

.field final pH:I

.field final pI:I

.field final pJ:I

.field pL:Z

.field final pP:I

.field final pQ:I

.field final pR:I

.field final pS:I

.field final pT:I

.field final pU:I

.field pY:Z

.field final pw:I

.field final qM:I

.field final qs:I

.field final qt:I

.field final qw:I

.field rD:Z

.field rE:Z

.field final ri:I

.field final rj:I

.field final rl:I

.field final rm:I

.field final rn:I

.field ru:Z

.field rv:Z

.field rw:Z

.field rx:Z

.field rz:Z

.field sC:Z

.field sD:Z

.field sE:Z

.field final sG:Ljava/lang/String;

.field final sH:Ljava/lang/String;

.field final sI:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lb/al;->sF:Ljava/util/HashMap;

    sget-object v1, Lcom/android/server/ssrm/W;->go:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lb/al;->sF:Ljava/util/HashMap;

    invoke-virtual {v4, v3, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x3c

    const/16 v4, 0x3b

    const/16 v3, 0x36

    const/16 v2, 0x14

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/al;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/al;->pL:Z

    iput-boolean v1, p0, Lb/al;->pY:Z

    iput-boolean v1, p0, Lb/al;->pE:Z

    iput-boolean v1, p0, Lb/al;->ru:Z

    iput-boolean v1, p0, Lb/al;->rv:Z

    iput-boolean v1, p0, Lb/al;->rw:Z

    iput-boolean v1, p0, Lb/al;->rx:Z

    iput-boolean v1, p0, Lb/al;->rz:Z

    iput-boolean v1, p0, Lb/al;->sC:Z

    iput-boolean v1, p0, Lb/al;->sD:Z

    iput-boolean v1, p0, Lb/al;->sE:Z

    const/4 v0, 0x1

    iput v0, p0, Lb/al;->qw:I

    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/al;->sG:Ljava/lang/String;

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/al;->sH:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/al;->sI:Ljava/lang/String;

    iput-boolean v1, p0, Lb/al;->rD:Z

    iput-boolean v1, p0, Lb/al;->rE:Z

    iget-object v0, p0, Lb/al;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "85 1400000:90 1700000:95"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pH:I

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000 1400000:80000 1500000:40000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pI:I

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1190400"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pJ:I

    new-array v0, v4, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pP:I

    new-array v0, v4, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pQ:I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pR:I

    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "100000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pw:I

    const/16 v0, 0x2f

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "883200"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->ri:I

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "10"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->rj:I

    const/16 v0, 0x39

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pS:I

    new-array v0, v5, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "340"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pT:I

    new-array v0, v5, [I

    fill-array-data v0, :array_e

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "99000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->rm:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->pU:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "199000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->rl:I

    new-array v0, v4, [I

    fill-array-data v0, :array_11

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->qs:I

    new-array v0, v5, [I

    fill-array-data v0, :array_12

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "60"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->qt:I

    new-array v0, v3, [I

    fill-array-data v0, :array_13

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->qM:I

    new-array v0, v3, [I

    fill-array-data v0, :array_14

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "16"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/al;->rn:I

    new-array v0, v2, [I

    fill-array-data v0, :array_15

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_16

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_19

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_1a

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1f

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_21

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_22

    invoke-static {v0}, Lb/al;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const-string v0, "dm"

    invoke-static {v0}, Lcom/android/server/ssrm/F;->h(Ljava/lang/String;)V

    new-instance v0, Lb/am;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/am;-><init>(Lb/al;Landroid/os/Looper;)V

    iput-object v0, p0, Lb/al;->mHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xd
        0x15
        0x8
        0x1e
        0x9
        0x17
        0x15
        0x18
        0x13
        0x16
        0x1f
        0x54
        0x17
        0xf
        0x9
        0x13
        0x19
        0x12
        0x1f
        0x8
        0x15
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x19
        0x10
        0x1f
        0x14
        0x17
        0x54
        0x17
        0x15
        0x14
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xf
        0x17
        0x15
        0x14
        0x13
        0x9
        0xe
        0xf
        0x1e
        0x13
        0x15
        0x54
        0xe
        0x13
        0x16
        0x1f
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1c
        0x1b
        0x19
        0xe
        0x15
        0x8
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x9
        0x3
        0x14
        0x19
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
        0x25
        0x17
        0x9
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1f
        0x14
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_f
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_10
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_11
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_12
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_13
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x13
        0x15
        0x25
        0x13
        0x9
        0x25
        0x18
        0xf
        0x9
        0x3
    .end array-data

    :array_14
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x4a
        0x54
        0xb
        0x19
        0x15
        0x17
        0x56
        0x19
        0xa
        0xf
        0x18
        0xd
        0x55
        0x19
        0xa
        0xf
        0x18
        0xd
        0x25
        0x12
        0xd
        0x17
        0x15
        0x14
        0x55
        0x13
        0x15
        0x25
        0xa
        0x1f
        0x8
        0x19
        0x1f
        0x14
        0xe
    .end array-data

    :array_15
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_16
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_17
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4c
        0x32
        0x2a
    .end array-data

    :array_18
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4d
        0x32
        0x37
    .end array-data

    :array_19
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x8
        0x15
        0xc
        0x13
        0x15
        0x54
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_1a
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_1b
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x25
        0x1c
        0x8
        0x1f
        0x1f
    .end array-data

    :array_1c
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x48
    .end array-data

    :array_1d
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_1e
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1f
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x1b
        0x11
        0x1b
        0x15
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_20
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xd
        0x12
        0x1b
        0xe
        0x9
        0x1b
        0xa
        0xa
    .end array-data

    :array_21
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0xd
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_22
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x17
    .end array-data
.end method


# virtual methods
.method public E(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/al;->pY:Z

    invoke-virtual {p0}, Lb/al;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    const-string v0, "FullScreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/al;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusNotiReceived:: FullScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/al;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Lb/al;->rD:Z

    invoke-virtual {p0}, Lb/al;->update()V

    :cond_0
    return-void
.end method

.method ai(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lb/al;->sF:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public br()V
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/ag;->aZ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/al;->rE:Z

    invoke-virtual {p0}, Lb/al;->update()V

    :cond_0
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/al;->pE:Z

    if-eqz v0, :cond_6

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lb/al;->sC:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/al;->pI:I

    const-string v1, "40000 800000:60000 1000000:100000 1700000:80000"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/al;->sD:Z

    if-eqz v0, :cond_e

    iget v0, p0, Lb/al;->pJ:I

    const-string v1, "1497600"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    :cond_2
    :goto_1
    iget-boolean v0, p0, Lb/al;->pY:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/al;->pS:I

    const-string v1, "3"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/al;->rj:I

    const-string v1, "100"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    :cond_3
    iget-boolean v0, p0, Lb/al;->rz:Z

    if-nez v0, :cond_f

    iget v0, p0, Lb/al;->qM:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/al;->rn:I

    const-string v1, "10"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    :cond_4
    :goto_2
    iget-boolean v0, p0, Lb/al;->rw:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lb/al;->pT:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/al;->pU:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/al;->qs:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/al;->qt:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    :cond_5
    return-void

    :cond_6
    iget-boolean v0, p0, Lb/al;->pY:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lb/al;->ri:I

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_7
    iget-boolean v0, p0, Lb/al;->pL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/al;->sD:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lb/al;->ri:I

    const-string v1, "1497600"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_8
    iget-boolean v0, p0, Lb/al;->sE:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lb/al;->ri:I

    const-string v1, "1574400"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-boolean v0, p0, Lb/al;->rD:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lb/al;->ru:Z

    if-eqz v0, :cond_a

    iget v0, p0, Lb/al;->ri:I

    const-string v1, "422400"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget v0, p0, Lb/al;->ri:I

    const-string v1, "960000"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    iget-boolean v0, p0, Lb/al;->rE:Z

    if-eqz v0, :cond_c

    iget v0, p0, Lb/al;->ri:I

    const-string v1, "422400"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-boolean v0, p0, Lb/al;->ru:Z

    if-nez v0, :cond_d

    iget-boolean v0, p0, Lb/al;->rv:Z

    if-eqz v0, :cond_0

    :cond_d
    iget v0, p0, Lb/al;->ri:I

    const-string v1, "883200"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    iget-boolean v0, p0, Lb/al;->sE:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/al;->pJ:I

    const-string v1, "1574400"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_1

    :cond_f
    iget v0, p0, Lb/al;->rl:I

    const-string v1, "499000"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    iget-boolean v0, p0, Lb/al;->pL:Z

    if-nez v0, :cond_4

    iget v0, p0, Lb/al;->rm:I

    const-string v1, "199000"

    invoke-virtual {p0, v0, v1}, Lb/al;->b(ILjava/lang/String;)V

    goto/16 :goto_2
.end method

.method public cb()V
    .locals 8

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/al;->bH()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lb/al;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lb/al;->pE:Z

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lb/al;->pL:Z

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/al;->ru:Z

    iput-boolean v1, p0, Lb/al;->rx:Z

    iput-boolean v1, p0, Lb/al;->sC:Z

    iput-boolean v1, p0, Lb/al;->sD:Z

    iput-boolean v1, p0, Lb/al;->sE:Z

    invoke-static {v3}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lb/al;->rx:Z

    invoke-virtual {p0, v3}, Lb/al;->ai(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/al;->sC:Z

    iget-object v0, p0, Lb/al;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lb/al;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v2, v0, Landroid/os/Message;->what:I

    iget-object v4, p0, Lb/al;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x2710

    invoke-virtual {v4, v0, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_1
    iget-object v0, p0, Lb/al;->sG:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/al;->sD:Z

    iget-object v0, p0, Lb/al;->sH:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/al;->sI:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lb/al;->sE:Z

    invoke-static {v3}, Lcom/android/server/ssrm/aU;->T(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lb/al;->ru:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lb/al;->rx:Z

    if-eqz v0, :cond_6

    :cond_2
    iput-boolean v2, p0, Lb/al;->rz:Z

    :goto_3
    invoke-virtual {p0}, Lb/al;->update()V

    goto :goto_0

    :cond_3
    const-string v0, "com.android.email"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lb/al;->rw:Z

    goto :goto_1

    :cond_4
    iput-boolean v1, p0, Lb/al;->rw:Z

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    iput-boolean v1, p0, Lb/al;->rz:Z

    goto :goto_3
.end method

.method public onScreenOn()V
    .locals 1

    iget-boolean v0, p0, Lb/al;->rE:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/al;->rE:Z

    invoke-virtual {p0}, Lb/al;->update()V

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/al;->rv:Z

    invoke-virtual {p0}, Lb/al;->update()V

    return-void
.end method
