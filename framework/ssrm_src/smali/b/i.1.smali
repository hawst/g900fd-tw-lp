.class public final Lb/i;
.super Lb/p;

# interfaces
.implements Lb/E;
.implements Lb/r;
.implements Lb/t;


# static fields
.field static TAG:Ljava/lang/String;

.field public static nN:Lb/j;

.field public static nO:I

.field static oa:Z

.field private static final ob:Ljava/lang/String;

.field private static final oc:Ljava/lang/String;

.field static og:Landroid/os/CustomFrequencyManager;


# instance fields
.field final iu:Ljava/lang/String;

.field final iw:Ljava/lang/String;

.field nP:Z

.field nQ:Z

.field nR:Z

.field nS:Z

.field nT:Z

.field nU:Z

.field nV:Z

.field final nW:I

.field final nX:I

.field final nY:I

.field final nZ:I

.field final od:Ljava/lang/String;

.field final oe:Ljava/lang/String;

.field final of:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    const-class v0, Lb/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/i;->TAG:Ljava/lang/String;

    sput-object v2, Lb/i;->nN:Lb/j;

    const/4 v0, 0x0

    sput v0, Lb/i;->nO:I

    const-string v0, "1"

    const-string v1, "sys.multiwindow.running"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lb/i;->oa:Z

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/i;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/i;->ob:Ljava/lang/String;

    const/16 v0, 0x25

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/i;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/i;->oc:Ljava/lang/String;

    sput-object v2, Lb/i;->og:Landroid/os/CustomFrequencyManager;

    return-void

    nop

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x16
        0x19
        0x1e
        0x55
        0xa
        0x1b
        0x14
        0x1f
        0x16
        0x55
        0x1c
        0xa
        0x9
        0x25
        0x19
        0x12
        0x1b
        0x14
        0x1d
        0x1f
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1d
        0x8
        0x1b
        0xa
        0x12
        0x13
        0x19
        0x9
        0x55
        0x1c
        0x18
        0x4a
        0x55
        0x16
        0x19
        0x1e
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x16
        0x1f
        0xc
        0x1f
        0x16
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v0, p0, Lb/i;->nP:Z

    iput-boolean v0, p0, Lb/i;->nQ:Z

    iput-boolean v0, p0, Lb/i;->nR:Z

    iput-boolean v0, p0, Lb/i;->nS:Z

    iput-boolean v0, p0, Lb/i;->nT:Z

    iput-boolean v0, p0, Lb/i;->nU:Z

    iput-boolean v0, p0, Lb/i;->nV:Z

    iput v0, p0, Lb/i;->nW:I

    const/4 v0, 0x1

    iput v0, p0, Lb/i;->nX:I

    const/4 v0, 0x2

    iput v0, p0, Lb/i;->nY:I

    const/4 v0, 0x3

    iput v0, p0, Lb/i;->nZ:I

    const/16 v0, 0x1a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/i;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/i;->od:Ljava/lang/String;

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/i;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/i;->oe:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/i;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/i;->of:Ljava/lang/String;

    const-string v0, "com.sec.android.intent.action.SSRM_REQUEST"

    iput-object v0, p0, Lb/i;->iu:Ljava/lang/String;

    const-string v0, "sec_container_1.com.sec.android.intent.action.SSRM_REQUEST"

    iput-object v0, p0, Lb/i;->iw:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3
        0x15
        0xf
        0xe
        0xf
        0x18
        0x1f
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x3
        0x15
        0xf
        0x11
        0xf
        0x54
        0xa
        0x12
        0x15
        0x14
        0x1f
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x19
        0x12
        0x8
        0x15
        0x17
        0x1f
    .end array-data
.end method

.method static A(Z)V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dQ:Z

    if-eqz v0, :cond_1

    sget-object v0, Lb/i;->og:Landroid/os/CustomFrequencyManager;

    if-nez v0, :cond_0

    sget-object v0, Lb/i;->mContext:Landroid/content/Context;

    const-string v1, "CustomFrequencyManagerService"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/CustomFrequencyManager;

    sput-object v0, Lb/i;->og:Landroid/os/CustomFrequencyManager;

    :cond_0
    sget-object v0, Lb/i;->og:Landroid/os/CustomFrequencyManager;

    if-eqz v0, :cond_1

    sget-object v0, Lb/i;->og:Landroid/os/CustomFrequencyManager;

    invoke-virtual {v0, p0}, Landroid/os/CustomFrequencyManager;->onTopAppChanged(Z)V

    :cond_1
    return-void
.end method

.method public static cj()Z
    .locals 2

    new-instance v0, Ljava/io/File;

    sget-object v1, Lb/i;->ob:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/File;

    sget-object v1, Lb/i;->oc:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method Z(I)Z
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V
    .locals 5

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3}, Lb/p;->a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cm()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "1"

    const-string v1, "sys.multiwindow.running"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lb/i;->oa:Z

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    goto :goto_0

    :cond_2
    const-string v2, "com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "sec_container_1.com.sec.android.intent.action.SSRM_REQUEST"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    sget-object v0, Lb/i;->ix:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lb/i;->iy:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    const-string v0, "PID"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const-string v4, "Camera_preview"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "MoviePlayer_play"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    sget-object v4, Lb/ar;->tf:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "ChatOnV_vtCall"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "GroupPlay_fpsChange"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "Phone_vtCall"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    if-eqz v3, :cond_5

    :goto_1
    sput v0, Lb/i;->nO:I

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    goto/16 :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    sget-object v0, Lb/i;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "statusName = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", statusValue = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/i;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "FullScreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean p2, p0, Lb/i;->nP:Z

    sget-object v0, Lb/i;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FullScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/i;->nP:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/i;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lb/i;->nP:Z

    iget-boolean v1, p0, Lb/i;->nU:Z

    if-eq v0, v1, :cond_1

    iget-boolean v0, p0, Lb/i;->nP:Z

    invoke-static {v0}, Lb/i;->A(Z)V

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    :cond_0
    iget-boolean v0, p0, Lb/i;->nP:Z

    iput-boolean v0, p0, Lb/i;->nU:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "BrowserLowFps"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean p2, p0, Lb/i;->nS:Z

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_1

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    goto :goto_0
.end method

.method public cb()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lb/i;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lb/i;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    iget-object v2, p0, Lb/i;->od:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lb/i;->oe:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lb/i;->of:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    iput-boolean v4, p0, Lb/i;->nQ:Z

    iput-boolean v3, p0, Lb/i;->nR:Z

    iput-boolean v3, p0, Lb/i;->nS:Z

    :goto_1
    iget-boolean v0, p0, Lb/i;->nQ:Z

    iget-boolean v1, p0, Lb/i;->nV:Z

    if-eq v0, v1, :cond_6

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_3

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    :cond_3
    iget-boolean v0, p0, Lb/i;->nQ:Z

    iput-boolean v0, p0, Lb/i;->nV:Z

    goto :goto_0

    :cond_4
    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput-boolean v4, p0, Lb/i;->nR:Z

    iput-boolean v3, p0, Lb/i;->nQ:Z

    goto :goto_1

    :cond_5
    iput-boolean v3, p0, Lb/i;->nQ:Z

    iput-boolean v3, p0, Lb/i;->nR:Z

    iput-boolean v3, p0, Lb/i;->nS:Z

    goto :goto_1

    :cond_6
    iget-boolean v0, p0, Lb/i;->nR:Z

    iget-boolean v1, p0, Lb/i;->nT:Z

    if-eq v0, v1, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_7

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->cl()V

    :cond_7
    iget-boolean v0, p0, Lb/i;->nR:Z

    iput-boolean v0, p0, Lb/i;->nT:Z

    goto :goto_0
.end method

.method public ci()V
    .locals 1

    invoke-virtual {p0}, Lb/i;->ck()Lb/j;

    move-result-object v0

    sput-object v0, Lb/i;->nN:Lb/j;

    sget-object v0, Lb/i;->nN:Lb/j;

    if-eqz v0, :cond_0

    sget-object v0, Lb/i;->nN:Lb/j;

    invoke-virtual {v0}, Lb/j;->initialize()V

    :cond_0
    return-void
.end method

.method ck()Lb/j;
    .locals 2

    new-instance v0, Ljava/io/File;

    sget-object v1, Lb/i;->ob:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lb/k;

    invoke-direct {v0, p0}, Lb/k;-><init>(Lb/i;)V

    sget-object v1, Lb/i;->ob:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lb/k;->ah(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/io/File;

    sget-object v1, Lb/i;->oc:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lb/k;

    invoke-direct {v0, p0}, Lb/k;-><init>(Lb/i;)V

    sget-object v1, Lb/i;->oc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lb/k;->ah(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
