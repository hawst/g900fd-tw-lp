.class public Lb/ay;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/D;
.implements Lb/G;
.implements Lb/t;
.implements Lb/w;
.implements Lb/y;


# instance fields
.field final TAG:Ljava/lang/String;

.field ct:Z

.field mHandler:Landroid/os/Handler;

.field mUserId:I

.field oT:Z

.field pL:Z

.field final tK:I

.field tL:I

.field tM:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/ay;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ay;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/ay;->pL:Z

    iput-boolean v1, p0, Lb/ay;->oT:Z

    iput v1, p0, Lb/ay;->tK:I

    iput v1, p0, Lb/ay;->tL:I

    iput v1, p0, Lb/ay;->mUserId:I

    iput-boolean v1, p0, Lb/ay;->ct:Z

    new-instance v0, Lb/az;

    invoke-direct {v0, p0}, Lb/az;-><init>(Lb/ay;)V

    iput-object v0, p0, Lb/ay;->tM:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lb/ay;->mHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public J(Z)V
    .locals 1

    iput-boolean p1, p0, Lb/ay;->oT:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method ad(I)V
    .locals 4

    iget-boolean v0, p0, Lb/ay;->ct:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ay;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lb/ay;->tM:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/ay;->ct:Z

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/ay;->ct:Z

    iget-object v0, p0, Lb/ay;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lb/ay;->tM:Ljava/lang/Runnable;

    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public br()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lb/ay;->tL:I

    return-void
.end method

.method public cE()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method public cF()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method public cG()V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method public cb()V
    .locals 2

    sget-object v0, Lb/ay;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {p0}, Lb/ay;->bH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/ay;->pL:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method public onScreenOn()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/ay;->ad(I)V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
