.class public Lb/l;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/t;


# static fields
.field static mHandler:Landroid/os/Handler; = null

.field public static oE:Ljava/lang/String; = null

.field static final ol:I = 0x1

.field static final om:I = 0x2

.field static final on:I = 0x3

.field static final oo:I = 0x4


# instance fields
.field final DEBUG:Z

.field final TAG:Ljava/lang/String;

.field final TYPE_CPU:I

.field final TYPE_NONE:I

.field final is:Ljava/lang/String;

.field final it:Ljava/lang/String;

.field oA:Z

.field final oB:I

.field oC:I

.field oD:Z

.field oj:Z

.field ok:Lcom/android/server/ssrm/LoadDetectMonitor;

.field final op:I

.field final oq:I

.field final or:I

.field os:J

.field ot:J

.field ou:Ljava/lang/String;

.field ov:J

.field ow:J

.field ox:Z

.field oy:Z

.field oz:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lb/l;->oE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const-wide/32 v6, 0x927c0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/l;->DEBUG:Z

    const/16 v0, 0xbb8

    iput v0, p0, Lb/l;->op:I

    const v0, 0x249f0

    iput v0, p0, Lb/l;->oq:I

    const v0, 0xea60

    iput v0, p0, Lb/l;->or:I

    iput-wide v6, p0, Lb/l;->os:J

    iput-wide v6, p0, Lb/l;->ot:J

    const-string v0, "null"

    iput-object v0, p0, Lb/l;->ou:Ljava/lang/String;

    iput-wide v4, p0, Lb/l;->ov:J

    iput-wide v4, p0, Lb/l;->ow:J

    iput-boolean v2, p0, Lb/l;->ox:Z

    iput-boolean v2, p0, Lb/l;->oy:Z

    new-instance v0, Lb/n;

    invoke-direct {v0, p0}, Lb/n;-><init>(Lb/l;)V

    iput-object v0, p0, Lb/l;->oz:Ljava/lang/Runnable;

    iput-boolean v2, p0, Lb/l;->oA:Z

    iput v2, p0, Lb/l;->TYPE_NONE:I

    const/4 v0, 0x1

    iput v0, p0, Lb/l;->TYPE_CPU:I

    const/4 v0, 0x2

    iput v0, p0, Lb/l;->oB:I

    iput v2, p0, Lb/l;->oC:I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/l;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/l;->is:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/l;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/l;->it:Ljava/lang/String;

    iput-boolean v2, p0, Lb/l;->oD:Z

    new-instance v0, Lcom/android/server/ssrm/LoadDetectMonitor;

    invoke-direct {v0}, Lcom/android/server/ssrm/LoadDetectMonitor;-><init>()V

    iput-object v0, p0, Lb/l;->ok:Lcom/android/server/ssrm/LoadDetectMonitor;

    new-instance v0, Lb/m;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/m;-><init>(Lb/l;Landroid/os/Looper;)V

    sput-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-direct {p0}, Lb/l;->cv()V

    return-void

    :array_0
    .array-data 4
        0x32
        0x1f
        0x1b
        0xc
        0x3
        0x2f
        0x9
        0x1f
        0x8
        0x29
        0x19
        0x1f
        0x14
        0x1b
        0x8
        0x13
        0x15
    .end array-data

    :array_1
    .array-data 4
        0x3d
        0x32
        0x1f
        0x1b
        0xc
        0x3
        0x2f
        0x9
        0x1f
        0x8
        0x29
        0x19
        0x1f
        0x14
        0x1b
        0x8
        0x13
        0x15
    .end array-data
.end method

.method private cr()V
    .locals 2

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lb/l;->oj:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lb/l;->cu()V

    :cond_0
    invoke-virtual {p0}, Lb/l;->cq()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/l;->B(Z)V

    :cond_1
    return-void
.end method

.method private cs()V
    .locals 4

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    sget-object v1, Lb/l;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private cu()V
    .locals 2

    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    const-string v1, "!@# Monitoring OFF"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/l;->oj:Z

    iget-object v0, p0, Lb/l;->ok:Lcom/android/server/ssrm/LoadDetectMonitor;

    invoke-virtual {v0}, Lcom/android/server/ssrm/LoadDetectMonitor;->aY()V

    return-void
.end method

.method private cv()V
    .locals 4

    const-wide/32 v2, 0x83d60

    sget-boolean v0, Lcom/android/server/ssrm/N;->ea:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->cZ:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-eqz v0, :cond_1

    :cond_0
    iput-wide v2, p0, Lb/l;->os:J

    iput-wide v2, p0, Lb/l;->ot:J

    :cond_1
    return-void
.end method

.method public static reportLoadState(I)V
    .locals 2

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    iput p0, v0, Landroid/os/Message;->arg1:I

    sget-object v1, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method B(Z)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lb/l;->a(ZI)V

    return-void
.end method

.method C(Z)V
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/l;->bH()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lb/l;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v2

    invoke-static {v0}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, v0}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, La/a;->aa(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/G;->bY:Z

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFgAppChanged:: needToMonitorLoad = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lb/l;->oj:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lb/l;->ct()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lb/l;->cs()V

    goto :goto_1

    :cond_4
    iget-boolean v0, p0, Lb/l;->oj:Z

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lb/l;->cq()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    invoke-direct {p0}, Lb/l;->cr()V

    :cond_6
    iput-boolean v1, p0, Lb/l;->ox:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lb/l;->ow:J

    goto :goto_1
.end method

.method a(ZI)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shiftSiopTable:: enable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", type = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p1, p0, Lb/l;->oA:Z

    iget-boolean v0, p0, Lb/l;->oA:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lb/l;->oD:Z

    if-nez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lb/l;->ov:J

    iput-boolean v7, p0, Lb/l;->oD:Z

    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shiftSiopTable:: mShiftStartTime = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lb/l;->ov:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-boolean v0, p0, Lb/l;->oy:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    iget-wide v0, p0, Lb/l;->ow:J

    iget-wide v2, p0, Lb/l;->ot:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    :cond_0
    iget-wide v0, p0, Lb/l;->ow:J

    iget-wide v2, p0, Lb/l;->os:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_4

    :cond_1
    iput-boolean v7, p0, Lb/l;->ox:Z

    invoke-virtual {p0, v6, p2}, Lb/l;->b(ZI)V

    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "shiftSiopTable:: mIsTimeout = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/l;->ox:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lb/l;->ow:J

    iget-wide v4, p0, Lb/l;->ov:J

    sub-long v4, v0, v4

    add-long/2addr v2, v4

    iput-wide v2, p0, Lb/l;->ow:J

    iget-object v2, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "shiftSiopTable:: mShiftTotalTime = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lb/l;->ow:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mIsTimeout = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lb/l;->ox:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-wide v0, p0, Lb/l;->ov:J

    goto :goto_0

    :cond_3
    iput-boolean v6, p0, Lb/l;->oD:Z

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lb/l;->oA:Z

    invoke-virtual {p0, v0, p2}, Lb/l;->b(ZI)V

    goto :goto_1
.end method

.method protected ab(I)V
    .locals 6

    const/4 v1, 0x1

    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onLoadDetected:: state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    if-ne p1, v1, :cond_0

    move v0, v1

    :goto_1
    invoke-virtual {p0, v1, v0}, Lb/l;->a(ZI)V

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lb/l;->oz:Ljava/lang/Runnable;

    const-wide/32 v4, 0x249f0

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x3

    iput v0, v2, Landroid/os/Message;->what:I

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    const-wide/32 v4, 0xea60

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    invoke-direct {p0}, Lb/l;->cu()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method b(ZI)V
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez p1, :cond_1

    iget-object v0, p0, Lb/l;->it:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lb/l;->is:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    iput p2, p0, Lb/l;->oC:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lb/l;->ow:J

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lb/l;->oC:I

    if-eq v0, p2, :cond_0

    iput p2, p0, Lb/l;->oC:I

    iget v0, p0, Lb/l;->oC:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lb/l;->bH()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/l;->oE:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HeavyUserScenarioCover"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "HeavyUserScenarioCover"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :goto_1
    iget-object v0, p0, Lb/l;->is:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    iget-object v0, p0, Lb/l;->it:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const-string v0, "PreloadedApps"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lb/l;->oC:I

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lb/l;->bH()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/l;->oE:Ljava/lang/String;

    sget-object v0, Lcom/android/server/ssrm/ag;->iQ:Lf/m;

    const-string v1, "HeavyUserScenarioCover"

    invoke-virtual {v0, v1}, Lf/m;->aB(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "HeavyUserScenarioCover"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    :goto_2
    iget-object v0, p0, Lb/l;->is:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    const-string v0, "PreloadedApps"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/ag;->a(Ljava/lang/String;Z)V

    goto :goto_2
.end method

.method public br()V
    .locals 0

    invoke-direct {p0}, Lb/l;->cr()V

    return-void
.end method

.method public cb()V
    .locals 4

    invoke-virtual {p0}, Lb/l;->bH()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.sec.android.app.launcher"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lb/l;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x4

    iput v1, v0, Landroid/os/Message;->what:I

    sget-object v1, Lb/l;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/l;->C(Z)V

    goto :goto_0
.end method

.method cq()Z
    .locals 1

    iget-boolean v0, p0, Lb/l;->oA:Z

    return v0
.end method

.method ct()V
    .locals 2

    iget-boolean v0, p0, Lb/l;->ox:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/l;->TAG:Ljava/lang/String;

    const-string v1, "!@# Monitoring ON"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/l;->oj:Z

    iget-object v0, p0, Lb/l;->ok:Lcom/android/server/ssrm/LoadDetectMonitor;

    invoke-virtual {v0}, Lcom/android/server/ssrm/LoadDetectMonitor;->aX()V

    goto :goto_0
.end method

.method public onScreenOn()V
    .locals 2

    iget-boolean v0, p0, Lb/l;->ox:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lb/l;->oA:Z

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/ag;->T()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/l;->ox:Z

    :cond_0
    invoke-virtual {p0}, Lb/l;->cb()V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
