.class public final Lb/Y;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field pA:I

.field qM:I

.field re:Z

.field rf:I

.field rg:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/Y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/Y;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/Y;->rg:Z

    iput-boolean v1, p0, Lb/Y;->re:Z

    iget-object v0, p0, Lb/Y;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/Y;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x34

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/Y;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/Y;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/Y;->qM:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/Y;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "95"

    invoke-virtual {p0, v0, v1}, Lb/Y;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/Y;->pA:I

    const/16 v0, 0x25

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/Y;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/Y;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/Y;->rf:I

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/Y;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/Y;->addPackage(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x9
        0xa
        0x8
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x13
        0x15
        0x25
        0x13
        0x9
        0x25
        0x18
        0xf
        0x9
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x9
        0xa
        0x8
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0xa
        0x15
        0xd
        0x1f
        0x8
        0x25
        0x9
        0xf
        0xa
        0xa
        0x16
        0x3
        0x55
        0x18
        0x1b
        0xe
        0xe
        0x1f
        0x8
        0x3
        0x55
        0x1f
        0xc
        0x1f
        0x14
        0xe
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/Y;->re:Z

    invoke-virtual {p0}, Lb/Y;->update()V

    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/Y;->rg:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/Y;->qM:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/Y;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/Y;->pA:I

    const-string v1, "75"

    invoke-virtual {p0, v0, v1}, Lb/Y;->b(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lb/Y;->re:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/Y;->rf:I

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/Y;->b(ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lb/Y;->rf:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/Y;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 0

    return-void
.end method

.method public onScreenOn()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/Y;->re:Z

    invoke-virtual {p0}, Lb/Y;->update()V

    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/Y;->rg:Z

    invoke-virtual {p0}, Lb/Y;->update()V

    return-void
.end method
