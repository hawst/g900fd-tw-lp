.class public Lb/au;
.super Lb/p;

# interfaces
.implements Lb/t;
.implements Lb/x;


# instance fields
.field final TAG:Ljava/lang/String;

.field oO:Z

.field pL:Z

.field tp:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/au;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/au;->pL:Z

    iput-boolean v1, p0, Lb/au;->oO:Z

    iput-boolean v1, p0, Lb/au;->tp:Z

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/au;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/au;->addPackage(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x9
        0x18
        0x8
        0x15
        0xd
        0x9
        0x1f
        0x8
    .end array-data
.end method


# virtual methods
.method public I(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/au;->oO:Z

    invoke-virtual {p0}, Lb/au;->cg()V

    return-void
.end method

.method public cb()V
    .locals 0

    return-void
.end method

.method protected cg()V
    .locals 2

    iget-boolean v0, p0, Lb/au;->pL:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lb/au;->oO:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/au;->TAG:Ljava/lang/String;

    const-string v1, "service call SurfaceFlinger 1051 i32 1"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/au;->tp:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lb/au;->tp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/au;->TAG:Ljava/lang/String;

    const-string v1, "service call SurfaceFlinger 1051 i32 0"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->f(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/au;->tp:Z

    goto :goto_0
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/au;->pL:Z

    invoke-virtual {p0}, Lb/au;->cg()V

    return-void
.end method
