.class public Lb/M;
.super Lb/a;

# interfaces
.implements Lb/E;
.implements Lb/t;


# static fields
.field static pW:[Ljava/lang/String;

.field static pX:[Ljava/lang/String;


# instance fields
.field final TAG:Ljava/lang/String;

.field pD:Z

.field pE:Z

.field pH:I

.field pI:I

.field pJ:I

.field pL:Z

.field pM:Z

.field pN:Z

.field pO:Z

.field pP:I

.field pQ:I

.field pR:I

.field pS:I

.field pT:I

.field pU:I

.field pV:Z

.field pY:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x13

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x17

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    new-array v1, v5, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const/16 v2, 0x14

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x1b

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0x20

    new-array v2, v2, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v5, [I

    fill-array-data v2, :array_5

    invoke-static {v2}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x19

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    invoke-static {v2}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lb/M;->pW:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const/16 v1, 0xa

    new-array v1, v1, [I

    fill-array-data v1, :array_7

    invoke-static {v1}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sput-object v0, Lb/M;->pX:[Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
        0x11
        0x1b
        0x11
        0x1b
        0x15
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0x1b
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x8
        0x49
        0x25
        0x8
        0x15
        0xd
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0xa
        0x13
        0x19
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x3f
        0xa
        0x13
        0x19
        0x39
        0x13
        0xe
        0x1b
        0x1e
        0x1f
        0x16
    .end array-data

    :array_7
    .array-data 4
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x3b

    const/16 v2, 0x38

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/M;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/M;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/M;->pV:Z

    iput-boolean v1, p0, Lb/M;->pL:Z

    iput-boolean v1, p0, Lb/M;->pE:Z

    iput-boolean v1, p0, Lb/M;->pD:Z

    iput-boolean v1, p0, Lb/M;->pO:Z

    iput-boolean v1, p0, Lb/M;->pM:Z

    iput-boolean v1, p0, Lb/M;->pN:Z

    iput-boolean v1, p0, Lb/M;->pY:Z

    iget-object v0, p0, Lb/M;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "70 600000:70 800000:75 1500000:80 1700000:90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pH:I

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000 1000000:80000 1200000:100000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pI:I

    new-array v0, v2, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "600000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pJ:I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pP:I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pQ:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pR:I

    const/16 v0, 0x39

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pS:I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "360"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pT:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/M;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "95"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/M;->pU:I

    const-string v0, "dm"

    invoke-static {v0}, Lcom/android/server/ssrm/F;->h(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1f
        0x14
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data
.end method


# virtual methods
.method protected E(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/M;->pY:Z

    invoke-virtual {p0}, Lb/M;->update()V

    return-void
.end method

.method public L(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/M;->pO:Z

    invoke-virtual {p0}, Lb/M;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/M;->pM:Z

    invoke-virtual {p0}, Lb/M;->update()V

    :cond_0
    const-string v0, "ChatOnV_vtCall"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean p2, p0, Lb/M;->pN:Z

    invoke-virtual {p0}, Lb/M;->update()V

    :cond_1
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/M;->pL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/M;->pV:Z

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lb/M;->pT:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pU:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/M;->pE:Z

    if-eqz v0, :cond_3

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-nez v0, :cond_2

    iget v0, p0, Lb/M;->pH:I

    const-string v1, "60 800000:65 1400000:70"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "19000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pJ:I

    const-string v1, "650000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pP:I

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pQ:I

    const-string v1, "79000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-boolean v0, p0, Lb/M;->pY:Z

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lb/M;->pS:I

    const-string v1, "3"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lb/M;->pH:I

    const-string v1, "60 800000:65 1400000:70"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "19000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pJ:I

    const-string v1, "650000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pP:I

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pQ:I

    const-string v1, "79000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lb/M;->pL:Z

    if-eqz v0, :cond_7

    sget-boolean v0, Lcom/android/server/ssrm/N;->eo:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lb/M;->pH:I

    const-string v1, "80 1000000:99"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "20000 1000000:100000 1200000:400000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_6
    iget v0, p0, Lb/M;->pH:I

    const-string v1, "70 600000:99 1700000:80"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "20000 650000:100000 1200000:400000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_7
    iget-boolean v0, p0, Lb/M;->pD:Z

    if-eqz v0, :cond_8

    iget v0, p0, Lb/M;->pH:I

    const-string v1, "70 650000:95 800000:99"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "20000 1000000:140000 1200000:500000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_8
    iget-boolean v0, p0, Lb/M;->pM:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lb/M;->pH:I

    const-string v1, "70 650000:95 800000:99"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/M;->pI:I

    const-string v1, "20000 1000000:140000 1200000:500000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_9
    iget-boolean v0, p0, Lb/M;->pN:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/M;->pH:I

    const-string v1, "80 900000:99"

    invoke-virtual {p0, v0, v1}, Lb/M;->b(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public cb()V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/M;->bH()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/M;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lb/M;->pL:Z

    invoke-virtual {v1, v2}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lb/M;->pE:Z

    iput-boolean v0, p0, Lb/M;->pD:Z

    sget-object v3, Lb/M;->pW:[Ljava/lang/String;

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iput-boolean v6, p0, Lb/M;->pD:Z

    :cond_1
    sget-object v1, Lb/M;->pX:[Ljava/lang/String;

    array-length v3, v1

    :goto_2
    if-ge v0, v3, :cond_2

    aget-object v4, v1, v0

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    iput-boolean v6, p0, Lb/M;->pD:Z

    :cond_2
    const-string v0, "com.sec.android.app.camera"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/M;->pV:Z

    invoke-virtual {p0}, Lb/M;->update()V

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
