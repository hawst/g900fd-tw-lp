.class public Lb/g;
.super Lb/a;

# interfaces
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field final nA:I

.field final nB:I

.field nC:Ljava/util/Hashtable;

.field nD:Ljava/util/Hashtable;

.field nE:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    const/16 v4, 0x17

    const/16 v3, 0x15

    const/16 v2, 0x14

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/g;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lb/g;->nC:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lb/g;->nD:Ljava/util/Hashtable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/g;->nE:Z

    iget-object v0, p0, Lb/g;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->l(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/g;->nA:I

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->l(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/g;->nB:I

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ad(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/g;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lb/g;->ac(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x9
        0x3
        0x9
        0x54
        0x19
        0x1b
        0x17
        0x1f
        0x8
        0x1b
        0x17
        0x15
        0x1e
        0x1f
        0x54
        0x18
        0x16
        0x1b
        0x19
        0x11
        0x18
        0x15
        0x2
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x3
        0x9
        0x54
        0x19
        0x1b
        0x17
        0x1f
        0x8
        0x1b
        0x17
        0x15
        0x1e
        0x1f
        0x54
        0xc
        0xe
        0x19
        0x1b
        0x16
        0x16
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x16
        0x15
        0x19
        0x14
        0x1b
        0x16
        0x16
        0x54
        0x31
        0x13
        0x17
        0x3d
        0x13
        0x29
        0x1b
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x17
        0x14
        0x9
        0x15
        0x1c
        0xe
        0x54
        0x16
        0x1d
        0xf
        0x14
        0x1b
        0xc
        0x13
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0x12
        0x13
        0x14
        0x11
        0xd
        0x1b
        0x8
        0x1f
        0x54
        0x13
        0x14
        0x1b
        0xc
        0x13
        0x1b
        0x13
        0x8
    .end array-data

    :array_5
    .array-data 4
        0x11
        0x8
        0x54
        0x17
        0x1b
        0xa
        0xa
        0x1f
        0x8
        0x9
        0x54
        0x3b
        0xe
        0x16
        0x1b
        0x14
        0x29
        0x17
        0x1b
        0x8
        0xe
    .end array-data

    :array_6
    .array-data 4
        0x11
        0xe
        0x54
        0x14
        0x1b
        0xc
        0x13
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x35
        0x34
        0x29
        0x54
        0x37
        0x1b
        0xa
        0xa
        0x16
        0x1f
    .end array-data

    :array_8
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x14
        0x12
        0x14
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x14
        0x17
        0x1b
        0xa
    .end array-data

    :array_9
    .array-data 4
        0x11
        0x8
        0x54
        0x19
        0x15
        0x54
        0x1e
        0x48
        0x54
        0x11
        0x13
        0x9
        0xe
        0x13
        0x8
        0x14
        0x1e
    .end array-data

    :array_a
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x12
        0x15
        0xc
        0x1b
        0x14
        0x9
        0x54
        0x1b
        0xf
        0xe
        0x15
        0x1d
        0xf
        0x1b
        0x8
        0x1e
    .end array-data

    :array_b
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xa
        0x15
        0x11
        0x1f
        0xc
        0x13
        0x1b
        0x14
        0x54
        0x15
        0xa
        0xe
        0x13
        0x17
        0xf
        0x9
    .end array-data

    :array_c
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0xe
        0x54
        0x15
        0x16
        0x16
        0x1f
        0x12
        0x17
        0x1b
        0xa
    .end array-data

    :array_d
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data
.end method

.method private ac(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/g;->nD:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private ad(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/g;->nC:Ljava/util/Hashtable;

    invoke-virtual {v0, p1, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private ae(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lb/g;->nC:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private af(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lb/g;->nD:Ljava/util/Hashtable;

    invoke-virtual {v0, p1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "ChatOnV_vtCall"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/g;->nE:Z

    invoke-virtual {p0}, Lb/g;->update()V

    :cond_0
    return-void
.end method

.method ca()V
    .locals 3

    invoke-virtual {p0}, Lb/g;->bH()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lb/g;->ae(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "blackbox"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "BlackBox"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "l001mtm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1}, Lb/g;->af(Ljava/lang/String;)Z

    move-result v1

    if-eqz v0, :cond_1

    iget v0, p0, Lb/g;->nA:I

    const-string v2, "1"

    invoke-virtual {p0, v0, v2}, Lb/g;->c(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/g;->nE:Z

    if-nez v0, :cond_2

    if-eqz v1, :cond_3

    :cond_2
    iget v0, p0, Lb/g;->nB:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/g;->c(ILjava/lang/String;)V

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public cb()V
    .locals 0

    invoke-virtual {p0}, Lb/g;->update()V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
