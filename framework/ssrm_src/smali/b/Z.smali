.class public final Lb/Z;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/t;
.implements Lb/w;


# instance fields
.field final TAG:Ljava/lang/String;

.field pE:Z

.field pF:Z

.field final rh:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/Z;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/Z;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/Z;->pF:Z

    iput-boolean v1, p0, Lb/Z;->pE:Z

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/Z;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/Z;->rh:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/Z;->pF:Z

    invoke-virtual {p0}, Lb/Z;->cK()V

    return-void
.end method

.method public cE()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/Z;->pF:Z

    invoke-virtual {p0}, Lb/Z;->cK()V

    return-void
.end method

.method public cK()V
    .locals 2

    iget-boolean v0, p0, Lb/Z;->pE:Z

    if-eqz v0, :cond_0

    const-string v0, "up_threshold"

    const/16 v1, 0x46

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sampling_down_factor"

    const/16 v1, 0xf

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sampling_rate"

    const/16 v1, 0x4e20

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sync_freq"

    const v1, 0x17e530

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lb/Z;->pF:Z

    if-eqz v0, :cond_1

    const-string v0, "sampling_rate"

    const v1, 0x7a120

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const-string v0, "up_threshold"

    const/16 v1, 0x5a

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sampling_down_factor"

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sampling_rate"

    const v1, 0xc350

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sync_freq"

    const v1, 0xe01f0

    invoke-virtual {p0, v0, v1}, Lb/Z;->i(Ljava/lang/String;I)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 3

    invoke-virtual {p0}, Lb/Z;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    sget-object v2, Lb/Z;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v0

    iget-boolean v2, p0, Lb/Z;->pE:Z

    if-ne v2, v0, :cond_2

    iget-boolean v2, p0, Lb/Z;->pF:Z

    if-eq v2, v1, :cond_0

    :cond_2
    iput-boolean v0, p0, Lb/Z;->pE:Z

    iput-boolean v1, p0, Lb/Z;->pF:Z

    invoke-virtual {p0}, Lb/Z;->cK()V

    goto :goto_0
.end method

.method public i(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lb/Z;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lb/Z;->rh:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lb/Z;->b(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
