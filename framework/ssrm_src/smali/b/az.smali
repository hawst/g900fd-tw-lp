.class Lb/az;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic tN:Lb/ay;


# direct methods
.method constructor <init>(Lb/ay;)V
    .locals 0

    iput-object p1, p0, Lb/az;->tN:Lb/ay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method dg()I
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->isPowerSavingMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lb/az;->tN:Lb/ay;

    iget-boolean v1, v1, Lb/ay;->pL:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method dh()I
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->isPowerSavingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public run()V
    .locals 4

    sget-boolean v0, Lcom/android/server/ssrm/N;->cW:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lb/az;->dg()I

    move-result v0

    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lb/az;->tN:Lb/ay;

    iget v1, v1, Lb/ay;->tL:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lb/az;->tN:Lb/ay;

    iput v0, v1, Lb/ay;->tL:I

    iget-object v1, p0, Lb/az;->tN:Lb/ay;

    iget-object v1, v1, Lb/ay;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "report_rate,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v1, Lcom/android/server/ssrm/aT;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "dev.ssrm.report_rate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lb/az;->tN:Lb/ay;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lb/ay;->ct:Z

    return-void

    :cond_1
    invoke-virtual {p0}, Lb/az;->dh()I

    move-result v0

    goto :goto_0
.end method
