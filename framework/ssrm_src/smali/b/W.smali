.class public Lb/W;
.super Lb/a;

# interfaces
.implements Lb/D;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field oT:Z

.field final pH:I

.field final pI:I

.field final pJ:I

.field pL:Z

.field final pQ:I

.field rc:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x38

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/W;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/W;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/W;->pL:Z

    iput-boolean v1, p0, Lb/W;->rc:Z

    iput-boolean v1, p0, Lb/W;->oT:Z

    iget-object v0, p0, Lb/W;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/W;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/W;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "19000 1000000:39000 1200000:79000 1300000:19000"

    invoke-virtual {p0, v0, v1}, Lb/W;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/W;->pI:I

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/W;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "800000"

    invoke-virtual {p0, v0, v1}, Lb/W;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/W;->pJ:I

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/W;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "19000"

    invoke-virtual {p0, v0, v1}, Lb/W;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/W;->pQ:I

    new-array v0, v2, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/W;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "86 800000:90 1000000:95"

    invoke-virtual {p0, v0, v1}, Lb/W;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/W;->pH:I

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data
.end method


# virtual methods
.method public J(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/W;->oT:Z

    invoke-virtual {p0}, Lb/W;->update()V

    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/W;->pL:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/W;->pI:I

    const-string v1, "19000 1000000:59000 1200000:139000 1300000:59000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pJ:I

    const-string v1, "600000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pH:I

    const-string v1, "85 600000:90 1000000:95 1400000:99"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lb/W;->rc:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/W;->pI:I

    const-string v1, "19000 1000000:59000 1200000:99000 1300000:19000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pJ:I

    const-string v1, "600000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pQ:I

    const-string v1, "39000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pH:I

    const-string v1, "85 600000:90 1000000:95"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lb/W;->oT:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/W;->pI:I

    const-string v1, "19000 1000000:59000 1200000:99000 1300000:19000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pJ:I

    const-string v1, "600000"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/W;->pH:I

    const-string v1, "85 600000:90 1000000:95"

    invoke-virtual {p0, v0, v1}, Lb/W;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 2

    invoke-virtual {p0}, Lb/W;->bH()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lb/W;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lb/W;->pL:Z

    const/16 v1, 0x1f

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/W;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/W;->rc:Z

    invoke-virtual {p0}, Lb/W;->update()V

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
        0xa
        0x16
        0x1b
        0x3
        0x1f
        0x8
    .end array-data
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
