.class public abstract Lb/a;
.super Lb/p;


# instance fields
.field protected TAG:Ljava/lang/String;

.field nk:Ljava/util/ArrayList;

.field nl:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/a;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lb/a;->nl:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method final b(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/b;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p2, v0, Lb/b;->no:Ljava/lang/String;

    goto :goto_0
.end method

.method final c(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/a;->nl:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p2, v0, Lb/c;->no:Ljava/lang/String;

    goto :goto_0
.end method

.method abstract ca()V
.end method

.method k(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v2, Lb/b;

    invoke-direct {v2}, Lb/b;-><init>()V

    iput-object p1, v2, Lb/b;->mPath:Ljava/lang/String;

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lb/b;->np:Z

    iget-boolean v0, v2, Lb/b;->np:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/a;->TAG:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lb/b;->nn:Ljava/lang/String;

    iput-object v0, v2, Lb/b;->nm:Ljava/lang/String;

    :goto_2
    iget-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iput-object p2, v2, Lb/b;->no:Ljava/lang/String;

    iput-object p2, v2, Lb/b;->nn:Ljava/lang/String;

    iput-object p2, v2, Lb/b;->nm:Ljava/lang/String;

    goto :goto_2
.end method

.method l(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lb/a;->nl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v1, Lb/c;

    invoke-direct {v1}, Lb/c;-><init>()V

    iput-object p1, v1, Lb/c;->nq:Ljava/lang/String;

    iput-object p2, v1, Lb/c;->no:Ljava/lang/String;

    iput-object p2, v1, Lb/c;->nm:Ljava/lang/String;

    iput-object p2, v1, Lb/c;->nn:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lb/a;->nl:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method setTag(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lb/a;->TAG:Ljava/lang/String;

    return-void
.end method

.method update()V
    .locals 5

    iget-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/b;

    iget-object v2, v0, Lb/b;->nm:Ljava/lang/String;

    iput-object v2, v0, Lb/b;->no:Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lb/a;->nl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    iget-object v2, v0, Lb/c;->nm:Ljava/lang/String;

    iput-object v2, v0, Lb/c;->no:Ljava/lang/String;

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lb/a;->ca()V

    iget-object v0, p0, Lb/a;->nk:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/b;

    iget-object v2, v0, Lb/b;->nn:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lb/b;->nn:Ljava/lang/String;

    iget-object v3, v0, Lb/b;->no:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_3
    iget-boolean v2, v0, Lb/b;->np:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lb/a;->TAG:Ljava/lang/String;

    iget-object v3, v0, Lb/b;->mPath:Ljava/lang/String;

    iget-object v4, v0, Lb/b;->no:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_3
    iget-object v2, v0, Lb/b;->no:Ljava/lang/String;

    iput-object v2, v0, Lb/b;->nn:Ljava/lang/String;

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lb/a;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lb/b;->mPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lb/b;->no:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " => permission denied."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lb/a;->nl:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/c;

    iget-object v2, v0, Lb/c;->nn:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lb/c;->nn:Ljava/lang/String;

    iget-object v3, v0, Lb/c;->no:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_7
    iget-object v2, v0, Lb/c;->nq:Ljava/lang/String;

    iget-object v3, v0, Lb/c;->no:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v0, Lb/c;->no:Ljava/lang/String;

    iput-object v2, v0, Lb/c;->nn:Ljava/lang/String;

    goto :goto_4

    :cond_8
    return-void
.end method
