.class public Lb/I;
.super Lb/p;

# interfaces
.implements Lb/t;
.implements Lb/w;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field oW:Landroid/sec/performance/AffinityControl;

.field private final oX:I

.field private final oY:[I

.field private oZ:I

.field private pa:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/I;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/I;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lb/I;->oW:Landroid/sec/performance/AffinityControl;

    const/4 v0, 0x4

    iput v0, p0, Lb/I;->oX:I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lb/I;->oY:[I

    iput v1, p0, Lb/I;->oZ:I

    iput-boolean v1, p0, Lb/I;->pa:Z

    const-string v0, "com.ftat"

    invoke-virtual {p0, v0}, Lb/I;->addPackage(Ljava/lang/String;)V

    new-instance v0, Landroid/sec/performance/AffinityControl;

    invoke-direct {v0}, Landroid/sec/performance/AffinityControl;-><init>()V

    iput-object v0, p0, Lb/I;->oW:Landroid/sec/performance/AffinityControl;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
    .end array-data
.end method


# virtual methods
.method public cE()V
    .locals 1

    iget-boolean v0, p0, Lb/I;->pa:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/I;->cH()V

    :cond_0
    return-void
.end method

.method protected declared-synchronized cH()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb/I;->oW:Landroid/sec/performance/AffinityControl;

    if-eqz v0, :cond_0

    iget v0, p0, Lb/I;->oZ:I

    if-lez v0, :cond_0

    sget-boolean v0, Lcom/android/server/ssrm/N;->dZ:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/I;->oW:Landroid/sec/performance/AffinityControl;

    iget v1, p0, Lb/I;->oZ:I

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x4

    aput v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/sec/performance/AffinityControl;->SetAffinity(I[I)I

    :goto_0
    sget-object v0, Lb/I;->TAG:Ljava/lang/String;

    const-string v1, "SetAffinity"

    invoke-static {v0, v1}, Lb/I;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v0, p0, Lb/I;->oW:Landroid/sec/performance/AffinityControl;

    iget v1, p0, Lb/I;->oZ:I

    iget-object v2, p0, Lb/I;->oY:[I

    invoke-virtual {v0, v1, v2}, Landroid/sec/performance/AffinityControl;->SetAffinity(I[I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cb()V
    .locals 0

    return-void
.end method

.method public z(Z)V
    .locals 1

    iput-boolean p1, p0, Lb/I;->pa:Z

    iget-boolean v0, p0, Lb/I;->pa:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/I;->bI()I

    move-result v0

    iput v0, p0, Lb/I;->oZ:I

    invoke-virtual {p0}, Lb/I;->cH()V

    :cond_0
    return-void
.end method
