.class public Lb/aA;
.super Lb/p;

# interfaces
.implements Lb/v;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static final tO:Ljava/lang/String;


# instance fields
.field private tP:Z

.field private tQ:Z

.field private tR:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/aA;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/aA;->TAG:Ljava/lang/String;

    const/16 v0, 0x3a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/aA;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/aA;->tO:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
        0x25
        0xe
        0xf
        0x8
        0x18
        0x15
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x1c

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/aA;->tP:Z

    iput-boolean v1, p0, Lb/aA;->tQ:Z

    iput-boolean v1, p0, Lb/aA;->tR:Z

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/aA;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aA;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/aA;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aA;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/aA;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aA;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/aA;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aA;->addPackage(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x9
        0x18
        0x8
        0x15
        0xd
        0x9
        0x1f
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x18
        0x8
        0x15
        0xd
        0x9
        0x1f
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x19
        0x12
        0x8
        0x15
        0x17
        0x1f
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x1f
        0xc
        0x1f
        0x8
        0x1d
        0x16
        0x1b
        0x1e
        0x1f
        0x9
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
    .end array-data
.end method

.method private di()V
    .locals 3

    const-string v0, "jf"

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lb/aA;->dj()Z

    move-result v0

    iget-boolean v1, p0, Lb/aA;->tP:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lb/aA;->tP:Z

    if-eqz v0, :cond_2

    sget-object v0, Lb/aA;->TAG:Ljava/lang/String;

    sget-object v1, Lb/aA;->tO:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lb/aA;->b(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    sget-object v0, Lb/aA;->TAG:Ljava/lang/String;

    sget-object v1, Lb/aA;->tO:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lb/aA;->b(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private dj()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lb/aA;->tQ:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lb/aA;->tR:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/aA;->tR:Z

    invoke-direct {p0}, Lb/aA;->di()V

    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/aA;->tQ:Z

    invoke-direct {p0}, Lb/aA;->di()V

    return-void
.end method
