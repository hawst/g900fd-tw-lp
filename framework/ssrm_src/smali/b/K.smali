.class public Lb/K;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/t;
.implements Lb/w;


# instance fields
.field final TAG:Ljava/lang/String;

.field pA:I

.field pB:I

.field pC:I

.field pD:Z

.field pE:Z

.field pF:Z

.field pG:[Ljava/lang/String;

.field pw:I

.field px:I

.field py:I

.field pz:I


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x38

    const/16 v4, 0x37

    const/16 v3, 0x13

    const/4 v2, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/K;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/K;->TAG:Ljava/lang/String;

    iput-boolean v2, p0, Lb/K;->pD:Z

    iput-boolean v2, p0, Lb/K;->pE:Z

    iput-boolean v2, p0, Lb/K;->pF:Z

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x17

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x9

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xc

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0xb

    new-array v2, v2, [I

    fill-array-data v2, :array_5

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xa

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    invoke-static {v2}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/K;->pG:[Ljava/lang/String;

    iget-object v0, p0, Lb/K;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->pw:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "100000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->px:I

    new-array v0, v5, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1200000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->py:I

    new-array v0, v5, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "600000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->pz:I

    const/16 v0, 0x35

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->pA:I

    new-array v0, v4, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->pB:I

    new-array v0, v4, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/K;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "60"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/K;->pC:I

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_2
    .array-data 4
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0x1b
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x8
        0x49
        0x25
        0x8
        0x15
        0xd
    .end array-data

    :array_5
    .array-data 4
        0x3f
        0xa
        0x13
        0x19
        0x39
        0x13
        0xe
        0x1b
        0x1e
        0x1f
        0x16
    .end array-data

    :array_6
    .array-data 4
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1c
        0x1b
        0x19
        0xe
        0x15
        0x8
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0x9
        0xe
        0x1f
        0xa
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x25
        0x18
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0x9
        0xe
        0x1f
        0xa
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
        0x25
        0x16
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
        0x25
        0x12
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
        0x25
        0x16
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/K;->pF:Z

    invoke-virtual {p0}, Lb/K;->update()V

    return-void
.end method

.method public cE()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/K;->pF:Z

    invoke-virtual {p0}, Lb/K;->update()V

    return-void
.end method

.method ca()V
    .locals 3

    iget-object v0, p0, Lb/K;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateParameters:: mIsTargetAppInForeground = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/K;->pD:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lb/K;->pE:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/K;->pw:I

    const-string v1, "4"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->px:I

    const-string v1, "80000"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->py:I

    const-string v1, "1400000"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->pz:I

    const-string v1, "1200000"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->pA:I

    const-string v1, "70"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->pB:I

    const-string v1, "80"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/K;->pC:I

    const-string v1, "60"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lb/K;->pD:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/K;->pB:I

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lb/K;->pF:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/K;->pC:I

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/K;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/K;->bH()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v0, p0, Lb/K;->pD:Z

    iget-object v2, p0, Lb/K;->pG:[Ljava/lang/String;

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    iput-boolean v6, p0, Lb/K;->pD:Z

    :cond_2
    sget-object v0, Lb/K;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    iget-boolean v2, p0, Lb/K;->pD:Z

    if-nez v2, :cond_3

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    iput-boolean v6, p0, Lb/K;->pD:Z

    :cond_3
    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/K;->pE:Z

    invoke-virtual {p0}, Lb/K;->update()V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
