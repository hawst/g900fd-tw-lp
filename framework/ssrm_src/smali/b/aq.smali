.class public final Lb/aq;
.super Lb/p;

# interfaces
.implements Lb/E;
.implements Lb/H;
.implements Lb/r;
.implements Lb/t;


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field sX:Z

.field sY:Z

.field sZ:Z

.field ta:Landroid/os/DVFSHelper;

.field tb:Landroid/os/DVFSHelper;

.field tc:Landroid/os/DVFSHelper;

.field td:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/aq;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v0, p0, Lb/aq;->sX:Z

    iput-boolean v0, p0, Lb/aq;->sY:Z

    iput-boolean v0, p0, Lb/aq;->sZ:Z

    iput-boolean v0, p0, Lb/aq;->td:Z

    return-void
.end method

.method static N(Z)V
    .locals 3

    sget-object v0, Lb/aq;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableDBurstMode:: enable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/aq;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/aq;->a([I)Ljava/lang/String;

    move-result-object v0

    if-eqz p0, :cond_0

    sget-object v1, Lb/aq;->TAG:Ljava/lang/String;

    const-string v2, "64"

    invoke-static {v1, v0, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/aq;->TAG:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v1, v0, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x18
        0x16
        0x15
        0x19
        0x11
        0x55
        0x17
        0x17
        0x19
        0x18
        0x16
        0x11
        0x4a
        0x55
        0x18
        0x11
        0x15
        0xa
        0x9
        0x25
        0x1f
        0x14
    .end array-data
.end method


# virtual methods
.method public K(Z)V
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lb/aq;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUsbConnectionStatusChanged:: connected = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/aq;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_0

    iput-boolean v3, p0, Lb/aq;->sX:Z

    iput-boolean v3, p0, Lb/aq;->sY:Z

    invoke-virtual {p0}, Lb/aq;->cS()V

    :cond_0
    return-void
.end method

.method O(Z)V
    .locals 1

    iget-boolean v0, p0, Lb/aq;->td:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lb/aq;->td:Z

    invoke-virtual {p0}, Lb/aq;->cS()V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lb/p;->a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.hardware.usb.action.USB_STATE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "connected"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "configured"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "mtp"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    sget-object v3, Lb/aq;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "USB_STATE:: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lb/aq;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lb/aq;->mContext:Landroid/content/Context;

    const-string v3, "usb"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbManager;

    if-eqz v1, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->isUsb30Enabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lb/aq;->O(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lb/aq;->O(Z)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "MTP_fileTransfer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lb/aq;->sX:Z

    if-eq v0, p2, :cond_0

    iput-boolean p2, p0, Lb/aq;->sX:Z

    invoke-virtual {p0}, Lb/aq;->cS()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "OBEX_dataTransfer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lb/aq;->sY:Z

    if-eq v0, p2, :cond_0

    iput-boolean p2, p0, Lb/aq;->sY:Z

    invoke-virtual {p0}, Lb/aq;->cS()V

    goto :goto_0
.end method

.method cS()V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lb/aq;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onMTPStatusChanged:: mMtpTransferOn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lb/aq;->sX:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mObexTransferOn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lb/aq;->sY:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lb/aq;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v2, p0, Lb/aq;->sX:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lb/aq;->sY:Z

    if-eqz v2, :cond_2

    :cond_0
    iget-boolean v2, p0, Lb/aq;->sZ:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lb/aq;->ta:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v2, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v2, p0, Lb/aq;->tc:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->acquire()V

    iput-boolean v0, p0, Lb/aq;->sZ:Z

    :cond_1
    :goto_0
    iget-boolean v2, p0, Lb/aq;->sZ:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lb/aq;->td:Z

    if-eqz v2, :cond_3

    :goto_1
    invoke-static {v0}, Lb/aq;->N(Z)V

    return-void

    :cond_2
    iget-boolean v2, p0, Lb/aq;->sZ:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lb/aq;->ta:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->release()V

    iget-object v2, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->release()V

    iget-object v2, p0, Lb/aq;->tc:Landroid/os/DVFSHelper;

    invoke-virtual {v2}, Landroid/os/DVFSHelper;->release()V

    iput-boolean v1, p0, Lb/aq;->sZ:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public cb()V
    .locals 0

    return-void
.end method

.method public ci()V
    .locals 9

    const/4 v8, 0x0

    const-wide/16 v4, 0x0

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/aq;->mContext:Landroid/content/Context;

    const/16 v2, 0xb

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Lb/aq;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/aq;->ta:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/aq;->ta:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/aq;->ta:Landroid/os/DVFSHelper;

    const v3, 0x13d620

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/aq;->mContext:Landroid/content/Context;

    const/16 v2, 0x10

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/aq;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    sget-boolean v1, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v1, :cond_2

    iget-object v0, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_0
    :goto_0
    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/aq;->mContext:Landroid/content/Context;

    const/16 v2, 0xf

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/aq;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x13

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/aq;->tc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/aq;->tc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedBUSFrequency()[I

    move-result-object v0

    if-eqz v0, :cond_1

    array-length v1, v0

    if-lez v1, :cond_1

    iget-object v1, p0, Lb/aq;->tc:Landroid/os/DVFSHelper;

    const-string v2, "BUS"

    aget v0, v0, v8

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lb/aq;->tb:Landroid/os/DVFSHelper;

    const-string v2, "CORE_NUM"

    aget v0, v0, v8

    int-to-long v6, v0

    invoke-virtual {v1, v2, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x37
        0x2e
        0x2a
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
    .end array-data

    :array_1
    .array-data 4
        0x37
        0x2e
        0x2a
        0x25
        0x39
        0x35
        0x28
        0x3f
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
    .end array-data

    :array_2
    .array-data 4
        0x37
        0x2e
        0x2a
        0x25
        0x38
        0x2f
        0x29
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
    .end array-data
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
