.class public Lb/L;
.super Lb/a;

# interfaces
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field pD:Z

.field pG:[Ljava/lang/String;

.field pH:I

.field pI:I

.field pJ:I

.field pK:I

.field pL:Z

.field pM:Z

.field pN:Z

.field pO:Z


# direct methods
.method public constructor <init>()V
    .locals 5

    const/16 v4, 0x3e

    const/4 v3, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/L;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/L;->TAG:Ljava/lang/String;

    iput-boolean v3, p0, Lb/L;->pL:Z

    iput-boolean v3, p0, Lb/L;->pM:Z

    iput-boolean v3, p0, Lb/L;->pN:Z

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x17

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const/16 v2, 0x13

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0xc

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xa

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/L;->pG:[Ljava/lang/String;

    iput-boolean v3, p0, Lb/L;->pO:Z

    iget-object v0, p0, Lb/L;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "79 1100000:83 1500000:90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/L;->pH:I

    const/16 v0, 0x45

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "39000 1000000:59000 1200000:99000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/L;->pI:I

    new-array v0, v4, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1100000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/L;->pJ:I

    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/L;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "32"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/L;->pK:I

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x25
        0x1f
        0x1b
        0x1d
        0x16
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x25
        0x1f
        0x1b
        0x1d
        0x16
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x25
        0x1f
        0x1b
        0x1d
        0x16
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x11
        0x1f
        0x8
        0x14
        0x1f
        0x16
        0x55
        0x12
        0x17
        0xa
        0x55
        0x16
        0x15
        0x1b
        0x1e
        0x25
        0x1b
        0xc
        0x1d
        0x25
        0xa
        0x1f
        0x8
        0x13
        0x15
        0x1e
        0x25
        0x17
        0x9
    .end array-data
.end method


# virtual methods
.method public L(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/L;->pO:Z

    invoke-virtual {p0}, Lb/L;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/L;->pM:Z

    invoke-virtual {p0}, Lb/L;->update()V

    :cond_0
    const-string v0, "ChatOnV_vtCall"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean p2, p0, Lb/L;->pN:Z

    invoke-virtual {p0}, Lb/L;->update()V

    :cond_1
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/L;->pD:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/L;->pH:I

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/L;->pK:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget-boolean v0, p0, Lb/L;->pL:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/L;->pI:I

    const-string v1, "99000 1200000:399000"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget-boolean v0, p0, Lb/L;->pO:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/L;->pJ:I

    const-string v1, "800000"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lb/L;->pI:I

    const-string v1, "59000 1000000:139000 1200000:499000"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lb/L;->pM:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/L;->pH:I

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/L;->pI:I

    const-string v1, "19000 1000000:139000 1200000:499000"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/L;->pK:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lb/L;->pN:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/L;->pH:I

    const-string v1, "80 900000:99"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/L;->pK:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/L;->b(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 7

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/L;->bH()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v0, p0, Lb/L;->pD:Z

    iget-object v2, p0, Lb/L;->pG:[Ljava/lang/String;

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_2
    iput-boolean v6, p0, Lb/L;->pD:Z

    :cond_3
    sget-object v0, Lb/L;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    iget-boolean v2, p0, Lb/L;->pD:Z

    if-nez v2, :cond_4

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iput-boolean v6, p0, Lb/L;->pD:Z

    :cond_4
    invoke-virtual {p0}, Lb/L;->update()V

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
