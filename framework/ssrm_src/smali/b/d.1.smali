.class public Lb/d;
.super Lb/p;

# interfaces
.implements Lb/t;


# static fields
.field static final TAG:Ljava/lang/String;

.field private static final nt:Ljava/lang/String;

.field private static final nu:Ljava/lang/String;


# instance fields
.field nr:I

.field ns:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x34

    const-class v0, Lb/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/d;->TAG:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/d;->nt:Ljava/lang/String;

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/d;->nu:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x1f
        0x2
        0x3
        0x14
        0x15
        0x9
        0x4f
        0x57
        0x18
        0xf
        0x9
        0x1c
        0x8
        0x1f
        0xb
        0x57
        0x17
        0x13
        0x1c
        0x55
        0x1f
        0x14
        0x25
        0x17
        0x15
        0x14
        0x13
        0xe
        0x15
        0x8
        0x13
        0x14
        0x1d
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x1f
        0x2
        0x3
        0x14
        0x15
        0x9
        0x4f
        0x57
        0x18
        0xf
        0x9
        0x1c
        0x8
        0x1f
        0xb
        0x57
        0x13
        0x14
        0xe
        0x55
        0x1f
        0x14
        0x25
        0x17
        0x15
        0x14
        0x13
        0xe
        0x15
        0x8
        0x13
        0x14
        0x1d
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Lb/p;-><init>()V

    iput v0, p0, Lb/d;->nr:I

    iput v0, p0, Lb/d;->ns:I

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/d;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/d;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/d;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/d;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/d;->addPackage(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x1b
        0x11
        0x1b
        0x15
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xd
        0x12
        0x1b
        0xe
        0x9
        0x1b
        0xa
        0xa
    .end array-data
.end method


# virtual methods
.method public cb()V
    .locals 2

    invoke-virtual {p0}, Lb/d;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/d;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lb/d;->isPackageExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_1
    iput v0, p0, Lb/d;->ns:I

    invoke-virtual {p0}, Lb/d;->cc()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method cc()V
    .locals 4

    iget v0, p0, Lb/d;->nr:I

    iget v1, p0, Lb/d;->ns:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lb/d;->ns:I

    iput v0, p0, Lb/d;->nr:I

    sget-object v0, Lb/d;->TAG:Ljava/lang/String;

    sget-object v1, Lb/d;->nt:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lb/d;->ns:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lb/d;->TAG:Ljava/lang/String;

    sget-object v1, Lb/d;->nu:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lb/d;->ns:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
