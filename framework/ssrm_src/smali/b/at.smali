.class public Lb/at;
.super Lb/p;

# interfaces
.implements Lb/E;


# static fields
.field private static TAG:Ljava/lang/String;

.field private static ti:Z

.field private static tj:Z

.field private static tk:Z

.field public static final tn:Ljava/lang/String;

.field public static final to:Ljava/lang/String;


# instance fields
.field mZ:Z

.field private pl:Landroid/os/DVFSHelper;

.field private tl:Landroid/os/DVFSHelper;

.field private tm:Landroid/os/DVFSHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const-class v0, Lb/at;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/at;->TAG:Ljava/lang/String;

    sput-boolean v1, Lb/at;->ti:Z

    sput-boolean v1, Lb/at;->tj:Z

    sput-boolean v1, Lb/at;->tk:Z

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/at;->tn:Ljava/lang/String;

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/at;->to:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x29
        0x1f
        0x19
        0xf
        0x8
        0x1f
        0x2a
        0x16
        0x1b
        0x3
        0x18
        0x1b
        0x19
        0x11
        0x25
        0xa
        0x16
        0x1b
        0x3
    .end array-data

    :array_1
    .array-data 4
        0x29
        0x19
        0x8
        0x1f
        0x1f
        0x14
        0x37
        0x13
        0x8
        0x8
        0x15
        0x8
        0x13
        0x14
        0x1d
        0x25
        0x1f
        0x14
        0x1b
        0x18
        0x16
        0x1f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/at;->mZ:Z

    iput-object v1, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    iput-object v1, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    iput-object v1, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    return-void
.end method

.method private static P(Z)V
    .locals 3

    sget-boolean v0, Lb/at;->tk:Z

    if-ne v0, p0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-boolean p0, Lb/at;->tk:Z

    sget-object v0, Lb/at;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sendSIOPExceptionIntent:: mExceptionEnabled = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lb/at;->tk:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/at;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const/16 v1, 0x2a

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SSRM_STATUS_NAME"

    const/16 v2, 0x16

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "SSRM_STATUS_VALUE"

    sget-boolean v2, Lb/at;->tk:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PID"

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "PackageName"

    sget-object v2, Lb/at;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lb/at;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_0

    sget-object v1, Lb/at;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_1
    .array-data 4
        0x29
        0x19
        0x8
        0x1f
        0x1f
        0x14
        0x37
        0x13
        0x8
        0x8
        0x15
        0x8
        0x13
        0x14
        0x1d
        0x38
        0x15
        0x15
        0x9
        0xe
        0x1f
        0x8
    .end array-data
.end method

.method private cT()V
    .locals 11

    const/16 v3, 0xc

    const/4 v10, 0x1

    const-wide/16 v4, 0x0

    const/16 v9, 0x1a

    const/4 v8, 0x0

    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-boolean v0, p0, Lb/at;->mZ:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_1
    invoke-static {v8}, Lb/at;->P(Z)V

    iput-boolean v10, p0, Lb/at;->mZ:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    iput-boolean v8, p0, Lb/at;->mZ:Z

    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    if-nez v0, :cond_4

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/at;->mContext:Landroid/content/Context;

    new-array v2, v9, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    const v6, 0x7a120

    invoke-virtual {v2, v6}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_4
    :goto_1
    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    if-nez v0, :cond_5

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/at;->mContext:Landroid/content/Context;

    new-array v2, v9, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    const v3, 0x927c0

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_5
    :goto_2
    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    if-nez v0, :cond_7

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/at;->mContext:Landroid/content/Context;

    new-array v2, v9, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/at;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_d

    :cond_6
    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_7
    :goto_3
    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    invoke-static {v8}, Lb/at;->P(Z)V

    sget-object v0, Lb/at;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateStatus:: mSecurePlayBack = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lb/at;->ti:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", mScreenMirroring = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-boolean v2, Lb/at;->tj:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/at;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-boolean v0, Lb/at;->ti:Z

    if-eqz v0, :cond_8

    sget-boolean v0, Lb/at;->tj:Z

    if-eqz v0, :cond_9

    :cond_8
    sget-boolean v0, Lb/at;->ti:Z

    if-nez v0, :cond_f

    sget-boolean v0, Lb/at;->tj:Z

    if-eqz v0, :cond_f

    :cond_9
    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_a
    :goto_4
    sget-boolean v0, Lb/at;->tj:Z

    if-eqz v0, :cond_2

    invoke-static {v10}, Lb/at;->P(Z)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/at;->tl:Landroid/os/DVFSHelper;

    const v6, 0xf4240

    invoke-virtual {v2, v6}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto/16 :goto_1

    :cond_c
    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    const v3, 0x124f80

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto/16 :goto_2

    :cond_d
    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUCoreNum()[I

    move-result-object v0

    aget v0, v0, v8

    const/4 v1, 0x2

    if-le v0, v1, :cond_e

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    const-wide/16 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto/16 :goto_3

    :cond_e
    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto/16 :goto_3

    :cond_f
    sget-boolean v0, Lb/at;->ti:Z

    if-eqz v0, :cond_a

    sget-boolean v0, Lb/at;->tj:Z

    if-eqz v0, :cond_a

    iget-object v0, p0, Lb/at;->tm:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v0, p0, Lb/at;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    goto :goto_4

    nop

    :array_0
    .array-data 4
        0x29
        0x39
        0x28
        0x3f
        0x3f
        0x34
        0x25
        0x37
        0x33
        0x28
        0x28
        0x35
        0x28
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
        0x25
        0x4b
        0x25
        0x4a
        0x3d
    .end array-data

    :array_1
    .array-data 4
        0x29
        0x39
        0x28
        0x3f
        0x3f
        0x34
        0x25
        0x37
        0x33
        0x28
        0x28
        0x35
        0x28
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
        0x25
        0x4b
        0x25
        0x48
        0x3d
    .end array-data

    :array_2
    .array-data 4
        0x29
        0x39
        0x28
        0x3f
        0x3f
        0x34
        0x25
        0x37
        0x33
        0x28
        0x28
        0x35
        0x28
        0x25
        0x38
        0x35
        0x35
        0x29
        0x2e
        0x3f
        0x28
        0x25
        0x39
        0x35
        0x28
        0x3f
    .end array-data
.end method


# virtual methods
.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    sget-object v0, Lb/at;->tn:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sput-boolean p2, Lb/at;->ti:Z

    invoke-direct {p0}, Lb/at;->cT()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lb/at;->to:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-boolean p2, Lb/at;->tj:Z

    invoke-direct {p0}, Lb/at;->cT()V

    goto :goto_0
.end method
