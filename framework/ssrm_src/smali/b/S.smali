.class Lb/S;
.super Landroid/os/Handler;


# instance fields
.field final synthetic qG:Lb/R;


# direct methods
.method constructor <init>(Lb/R;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lb/S;->qG:Lb/R;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lb/S;->qG:Lb/R;

    iget-object v0, v0, Lb/R;->TAG:Ljava/lang/String;

    const/16 v1, 0x19

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/S;->qG:Lb/R;

    invoke-virtual {v0}, Lb/R;->bH()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lb/S;->qG:Lb/R;

    invoke-static {v0}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v1, Lb/R;->qv:Z

    iget-object v0, p0, Lb/S;->qG:Lb/R;

    invoke-virtual {v0}, Lb/R;->update()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 4
        0x37
        0x29
        0x3d
        0x25
        0x31
        0x34
        0x35
        0x2d
        0x34
        0x25
        0x3d
        0x3b
        0x37
        0x3f
        0x25
        0x3c
        0x35
        0x28
        0x3f
        0x3d
        0x28
        0x35
        0x2f
        0x34
        0x3e
    .end array-data
.end method
