.class public final Lb/ax;
.super Lb/p;

# interfaces
.implements Lb/D;
.implements Lb/t;


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field tG:Z

.field tH:Z

.field tI:Z

.field tJ:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/ax;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/ax;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v0, p0, Lb/ax;->tG:Z

    iput-boolean v0, p0, Lb/ax;->tH:Z

    iput-boolean v0, p0, Lb/ax;->tI:Z

    const/4 v0, 0x2

    iput v0, p0, Lb/ax;->tJ:I

    return-void
.end method


# virtual methods
.method public J(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/ax;->tG:Z

    invoke-virtual {p0}, Lb/ax;->df()V

    return-void
.end method

.method public cb()V
    .locals 3

    invoke-virtual {p0}, Lb/ax;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/ax;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lb/ax;->tI:Z

    const-string v2, "com.facebook.katana"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.google.android.talk"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.kakao.talk"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.whatsapp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.sina.weibo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "com.tencent.mobileqq"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/ax;->tH:Z

    :goto_1
    invoke-virtual {p0}, Lb/ax;->df()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/ax;->tH:Z

    goto :goto_1
.end method

.method public de()I
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lb/ax;->tI:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/android/server/ssrm/N;->el:Z

    if-eqz v1, :cond_1

    const/16 v0, 0x9

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lb/ax;->tH:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lb/ax;->tG:Z

    if-nez v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public df()V
    .locals 4

    invoke-virtual {p0}, Lb/ax;->de()I

    move-result v0

    iget v1, p0, Lb/ax;->tJ:I

    if-ne v1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput v0, p0, Lb/ax;->tJ:I

    sget-object v1, Lb/ax;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "boost_level,"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
