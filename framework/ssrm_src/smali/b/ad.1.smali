.class Lb/ad;
.super Landroid/os/Handler;


# instance fields
.field final synthetic rP:Lb/ac;


# direct methods
.method constructor <init>(Lb/ac;Landroid/os/Looper;)V
    .locals 0

    iput-object p1, p0, Lb/ad;->rP:Lb/ac;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lb/ad;->rP:Lb/ac;

    iget-boolean v0, v0, Lb/ac;->rM:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ad;->rP:Lb/ac;

    iget-object v0, v0, Lb/ac;->rL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/ad;->rP:Lb/ac;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lb/ac;->rM:Z

    iget-object v0, p0, Lb/ad;->rP:Lb/ac;

    iget-object v0, v0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v1, "Power collapse is enabled."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
