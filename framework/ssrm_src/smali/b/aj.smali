.class public final Lb/aj;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field pE:Z

.field final pH:I

.field final pI:I

.field final pJ:I

.field pL:Z

.field final pP:I

.field final pQ:I

.field final pR:I

.field final pS:I

.field final pT:I

.field final pU:I

.field pY:Z

.field final pw:I

.field final qM:I

.field final qs:I

.field final qt:I

.field final qw:I

.field rD:Z

.field rE:Z

.field final ri:I

.field final rj:I

.field final rl:I

.field final rn:I

.field ru:Z

.field rv:Z

.field rw:Z

.field rx:Z

.field rz:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x14

    const/16 v4, 0x13

    const/16 v3, 0x3b

    const/16 v2, 0x36

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/aj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/aj;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/aj;->pL:Z

    iput-boolean v1, p0, Lb/aj;->pY:Z

    iput-boolean v1, p0, Lb/aj;->pE:Z

    iput-boolean v1, p0, Lb/aj;->ru:Z

    iput-boolean v1, p0, Lb/aj;->rv:Z

    iput-boolean v1, p0, Lb/aj;->rw:Z

    iput-boolean v1, p0, Lb/aj;->rx:Z

    iput-boolean v1, p0, Lb/aj;->rz:Z

    const/4 v0, 0x1

    iput v0, p0, Lb/aj;->qw:I

    iput-boolean v1, p0, Lb/aj;->rD:Z

    iput-boolean v1, p0, Lb/aj;->rE:Z

    iget-object v0, p0, Lb/aj;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/aj;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "85 1400000:90 1700000:95"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pH:I

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000 1400000:80000 1500000:40000 1700000:20000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pI:I

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1190400"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pJ:I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pP:I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pQ:I

    new-array v0, v2, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pR:I

    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "100000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pw:I

    const/16 v0, 0x2f

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v1

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v0, :cond_0

    const-string v0, "1574400"

    :goto_0
    invoke-virtual {p0, v1, v0}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->ri:I

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "10"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->rj:I

    const/16 v0, 0x39

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pS:I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "340"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pT:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->pU:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "199000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->rl:I

    new-array v0, v3, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->qs:I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "60"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->qt:I

    new-array v0, v2, [I

    fill-array-data v0, :array_f

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->qM:I

    new-array v0, v2, [I

    fill-array-data v0, :array_10

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "16"

    invoke-virtual {p0, v0, v1}, Lb/aj;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/aj;->rn:I

    new-array v0, v5, [I

    fill-array-data v0, :array_11

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x23

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    new-array v0, v5, [I

    fill-array-data v0, :array_15

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_16

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_19

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_1a

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_1d

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    invoke-static {v0}, Lb/aj;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/aj;->addPackage(Ljava/lang/String;)V

    const-string v0, "dm"

    invoke-static {v0}, Lcom/android/server/ssrm/F;->h(Ljava/lang/String;)V

    new-instance v0, Lb/ak;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/ak;-><init>(Lb/aj;Landroid/os/Looper;)V

    iput-object v0, p0, Lb/aj;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    const-string v0, "1190400"

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1c
        0x1b
        0x19
        0xe
        0x15
        0x8
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x9
        0x3
        0x14
        0x19
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
        0x25
        0x17
        0x9
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1f
        0x14
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_f
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x13
        0x15
        0x25
        0x13
        0x9
        0x25
        0x18
        0xf
        0x9
        0x3
    .end array-data

    :array_10
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x4a
        0x54
        0xb
        0x19
        0x15
        0x17
        0x56
        0x19
        0xa
        0xf
        0x18
        0xd
        0x55
        0x19
        0xa
        0xf
        0x18
        0xd
        0x25
        0x12
        0xd
        0x17
        0x15
        0x14
        0x55
        0x13
        0x15
        0x25
        0xa
        0x1f
        0x8
        0x19
        0x1f
        0x14
        0xe
    .end array-data

    :array_11
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_12
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_13
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4c
        0x32
        0x2a
    .end array-data

    :array_14
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4d
        0x32
        0x37
    .end array-data

    :array_15
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x8
        0x15
        0xc
        0x13
        0x15
        0x54
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_16
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_17
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x25
        0x1c
        0x8
        0x1f
        0x1f
    .end array-data

    :array_18
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x48
    .end array-data

    :array_19
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_1a
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1b
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x1b
        0x11
        0x1b
        0x15
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1c
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xd
        0x12
        0x1b
        0xe
        0x9
        0x1b
        0xa
        0xa
    .end array-data

    :array_1d
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0xd
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_1e
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x17
    .end array-data
.end method


# virtual methods
.method public E(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/aj;->pY:Z

    invoke-virtual {p0}, Lb/aj;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    const-string v0, "FullScreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/aj;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusNotiReceived:: FullScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/aj;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Lb/aj;->rD:Z

    invoke-virtual {p0}, Lb/aj;->update()V

    :cond_0
    return-void
.end method

.method public br()V
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/ag;->aZ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/aj;->rE:Z

    invoke-virtual {p0}, Lb/aj;->update()V

    :cond_0
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/aj;->pE:Z

    if-eqz v0, :cond_5

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lb/aj;->pY:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/aj;->pS:I

    const-string v1, "3"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->rj:I

    const-string v1, "100"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/aj;->rz:Z

    if-nez v0, :cond_2

    iget v0, p0, Lb/aj;->qM:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->rn:I

    const-string v1, "10"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Lb/aj;->rw:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/aj;->pT:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->pU:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->qs:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->qt:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lb/aj;->bH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Le/r;->al(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lb/aj;->pI:I

    const-string v1, "9000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->pH:I

    const-string v1, "50 883200:60 1497600:65"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->pP:I

    const-string v1, "65"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->pJ:I

    const-string v1, "1728000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/aj;->pR:I

    const-string v1, "10000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    :cond_4
    return-void

    :cond_5
    iget-boolean v0, p0, Lb/aj;->pY:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lb/aj;->ri:I

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lb/aj;->pL:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/aj;->rD:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lb/aj;->ru:Z

    if-eqz v0, :cond_7

    iget v0, p0, Lb/aj;->ri:I

    const-string v1, "652800"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget v0, p0, Lb/aj;->ri:I

    const-string v1, "1190400"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget-boolean v0, p0, Lb/aj;->rE:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lb/aj;->ri:I

    const-string v1, "652800"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget-boolean v0, p0, Lb/aj;->ru:Z

    if-nez v0, :cond_a

    iget-boolean v0, p0, Lb/aj;->rv:Z

    if-eqz v0, :cond_0

    :cond_a
    iget v0, p0, Lb/aj;->ri:I

    const-string v1, "960000"

    invoke-virtual {p0, v0, v1}, Lb/aj;->b(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public cb()V
    .locals 7

    const/4 v6, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0}, Lb/aj;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/aj;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lb/aj;->pE:Z

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lb/aj;->pL:Z

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lb/aj;->ru:Z

    iput-boolean v6, p0, Lb/aj;->rx:Z

    invoke-static {v0}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-boolean v3, p0, Lb/aj;->rx:Z

    iget-object v1, p0, Lb/aj;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lb/aj;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput v3, v1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lb/aj;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_1
    invoke-static {v0}, Lcom/android/server/ssrm/aU;->T(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb/aj;->ru:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lb/aj;->rx:Z

    if-eqz v0, :cond_4

    :cond_1
    iput-boolean v3, p0, Lb/aj;->rz:Z

    :goto_2
    invoke-virtual {p0}, Lb/aj;->update()V

    goto :goto_0

    :cond_2
    const-string v1, "com.android.email"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iput-boolean v3, p0, Lb/aj;->rw:Z

    goto :goto_1

    :cond_3
    iput-boolean v6, p0, Lb/aj;->rw:Z

    goto :goto_1

    :cond_4
    iput-boolean v6, p0, Lb/aj;->rz:Z

    goto :goto_2
.end method

.method public onScreenOn()V
    .locals 1

    iget-boolean v0, p0, Lb/aj;->rE:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/aj;->rE:Z

    invoke-virtual {p0}, Lb/aj;->update()V

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/aj;->rv:Z

    invoke-virtual {p0}, Lb/aj;->update()V

    return-void
.end method
