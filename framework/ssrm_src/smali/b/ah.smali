.class public final Lb/ah;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field pE:Z

.field pL:Z

.field pY:Z

.field final qw:I

.field rD:Z

.field rE:Z

.field final rR:Ljava/lang/String;

.field final rS:Ljava/lang/String;

.field final rT:Ljava/lang/String;

.field final rU:Ljava/lang/String;

.field final rV:Ljava/lang/String;

.field final rW:Ljava/lang/String;

.field final rX:Ljava/lang/String;

.field final rY:Ljava/lang/String;

.field final rZ:Ljava/lang/String;

.field ra:Z

.field ru:Z

.field rv:Z

.field rz:Z

.field final sa:Ljava/lang/String;

.field final sb:Ljava/lang/String;

.field final sc:Ljava/lang/String;

.field final sd:Ljava/lang/String;

.field final se:Ljava/lang/String;

.field final sf:Ljava/lang/String;

.field sg:Ljava/lang/String;

.field sh:Ljava/lang/String;

.field si:Ljava/lang/String;

.field sj:Ljava/lang/String;

.field sk:Ljava/lang/String;

.field sl:Ljava/lang/String;

.field sm:Ljava/lang/String;

.field sn:Ljava/lang/String;

.field so:Ljava/lang/String;

.field sp:Ljava/lang/String;

.field sq:Ljava/lang/String;

.field sr:Ljava/lang/String;

.field ss:Ljava/lang/String;

.field st:Ljava/lang/String;

.field su:Z

.field sv:Z

.field final sw:Ljava/lang/String;

.field final sx:Ljava/lang/String;

.field final sy:Ljava/lang/String;

.field final sz:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x14

    const/16 v4, 0x13

    const/16 v3, 0x3b

    const/16 v2, 0x23

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const/16 v0, 0x38

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    const/16 v0, 0x40

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    const/16 v0, 0x39

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rY:Ljava/lang/String;

    const/16 v0, 0x2f

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->rZ:Ljava/lang/String;

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sb:Ljava/lang/String;

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sc:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sd:Ljava/lang/String;

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->se:Ljava/lang/String;

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sf:Ljava/lang/String;

    iput-boolean v1, p0, Lb/ah;->pL:Z

    iput-boolean v1, p0, Lb/ah;->su:Z

    iput-boolean v1, p0, Lb/ah;->ra:Z

    iput-boolean v1, p0, Lb/ah;->pY:Z

    iput-boolean v1, p0, Lb/ah;->pE:Z

    iput-boolean v1, p0, Lb/ah;->ru:Z

    iput-boolean v1, p0, Lb/ah;->rv:Z

    iput-boolean v1, p0, Lb/ah;->rz:Z

    const/4 v0, 0x1

    iput v0, p0, Lb/ah;->qw:I

    iput-boolean v1, p0, Lb/ah;->sv:Z

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sw:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_10

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sx:Ljava/lang/String;

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sy:Ljava/lang/String;

    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sz:Ljava/lang/String;

    iput-boolean v1, p0, Lb/ah;->rD:Z

    iput-boolean v1, p0, Lb/ah;->rE:Z

    new-array v0, v5, [I

    fill-array-data v0, :array_13

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_14

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_15

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    new-array v0, v5, [I

    fill-array-data v0, :array_16

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_1a

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    new-array v0, v4, [I

    fill-array-data v0, :array_1e

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_1f

    invoke-static {v0}, Lb/ah;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ah;->addPackage(Ljava/lang/String;)V

    :try_start_0
    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rS:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sg:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rU:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sh:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rT:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->si:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rV:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sj:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rR:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sk:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rW:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sl:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rX:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sm:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->rZ:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sn:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sa:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->so:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sb:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sp:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sc:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sq:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sd:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->sr:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->se:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->ss:Ljava/lang/String;

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sf:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lb/ah;->o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ah;->st:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_ABOVE_HISPEED_DELAY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_GO_HISPEED_LOAD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_HISPEED_FREQ = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_MIN_SAMPLE_TIME = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_TARGET_LOADS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_TIMER_RATE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_SAMPLING_DOWN_FACTOR = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_SYNC_THRESHOLD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_BOOST_MS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_IO_IS_BUSY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->st:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string v0, "dm"

    invoke-static {v0}, Lcom/android/server/ssrm/F;->h(Ljava/lang/String;)V

    new-instance v0, Lb/ai;

    invoke-direct {v0, p0}, Lb/ai;-><init>(Lb/ah;)V

    iput-object v0, p0, Lb/ah;->mHandler:Landroid/os/Handler;

    return-void

    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "20000 1400000:80000 1500000:40000 1700000:20000"

    iput-object v0, p0, Lb/ah;->sg:Ljava/lang/String;

    const-string v0, "99"

    iput-object v0, p0, Lb/ah;->sh:Ljava/lang/String;

    const-string v0, "1190400"

    iput-object v0, p0, Lb/ah;->si:Ljava/lang/String;

    const-string v0, "40000"

    iput-object v0, p0, Lb/ah;->sj:Ljava/lang/String;

    const-string v0, "85 1400000:90 1700000:95"

    iput-object v0, p0, Lb/ah;->sk:Ljava/lang/String;

    const-string v0, "20000"

    iput-object v0, p0, Lb/ah;->sl:Ljava/lang/String;

    const-string v0, "100000"

    iput-object v0, p0, Lb/ah;->sm:Ljava/lang/String;

    const-string v0, "1190400"

    iput-object v0, p0, Lb/ah;->sn:Ljava/lang/String;

    const-string v0, "20"

    iput-object v0, p0, Lb/ah;->so:Ljava/lang/String;

    const-string v0, "340"

    iput-object v0, p0, Lb/ah;->sp:Ljava/lang/String;

    const-string v0, "90"

    iput-object v0, p0, Lb/ah;->sq:Ljava/lang/String;

    const-string v0, "90"

    iput-object v0, p0, Lb/ah;->sr:Ljava/lang/String;

    const-string v0, "60"

    iput-object v0, p0, Lb/ah;->ss:Ljava/lang/String;

    const-string v0, "1"

    iput-object v0, p0, Lb/ah;->st:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_ABOVE_HISPEED_DELAY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_GO_HISPEED_LOAD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_HISPEED_FREQ = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_MIN_SAMPLE_TIME = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_TARGET_LOADS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_TIMER_RATE = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_SAMPLING_DOWN_FACTOR = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_SYNC_THRESHOLD = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->sn:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_BOOST_MS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DEFAULT_IO_IS_BUSY = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lb/ah;->st:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_ABOVE_HISPEED_DELAY = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_GO_HISPEED_LOAD = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_HISPEED_FREQ = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_MIN_SAMPLE_TIME = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_TARGET_LOADS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_TIMER_RATE = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_SAMPLING_DOWN_FACTOR = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_SYNC_THRESHOLD = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->sn:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_BOOST_MS = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DEFAULT_IO_IS_BUSY = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lb/ah;->st:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    throw v0

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1c
        0x1b
        0x19
        0xe
        0x15
        0x8
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1f
        0x14
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x9
        0x3
        0x14
        0x19
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x25
        0x18
        0x15
        0x15
        0x9
        0xe
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
        0x25
        0x17
        0x9
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x13
        0x15
        0x25
        0x13
        0x9
        0x25
        0x18
        0xf
        0x9
        0x3
    .end array-data

    :array_f
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_10
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3e
        0x37
        0x32
        0x37
    .end array-data

    :array_11
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x12
        0x1b
        0x16
        0x1c
        0x18
        0x8
        0x13
        0x19
        0x11
        0x54
        0x1c
        0x8
        0xf
        0x13
        0xe
        0x14
        0x13
        0x14
        0x10
        0x1b
        0x1c
        0x8
        0x1f
        0x1f
    .end array-data

    :array_12
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x12
        0x1b
        0x16
        0x1c
        0x18
        0x8
        0x13
        0x19
        0x11
        0x54
        0x1c
        0x8
        0xf
        0x13
        0xe
        0x14
        0x13
        0x14
        0x10
        0x1b
    .end array-data

    :array_13
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_14
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4c
        0x32
        0x2a
    .end array-data

    :array_15
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4d
        0x32
        0x37
    .end array-data

    :array_16
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x8
        0x15
        0xc
        0x13
        0x15
        0x54
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_17
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_18
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x25
        0x1c
        0x8
        0x1f
        0x1f
    .end array-data

    :array_19
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x16
        0xf
        0x54
        0x1d
        0xf
        0x14
        0x18
        0x8
        0x15
        0x9
        0x48
    .end array-data

    :array_1a
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_1b
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1c
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x1b
        0x11
        0x1b
        0x15
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data

    :array_1d
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xd
        0x12
        0x1b
        0xe
        0x9
        0x1b
        0xa
        0xa
    .end array-data

    :array_1e
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0xd
        0x13
        0xe
        0xe
        0x1f
        0x8
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_1f
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x17
    .end array-data
.end method


# virtual methods
.method public E(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/ah;->pY:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    const-string v0, "FullScreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusNotiReceived:: FullScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ah;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Lb/ah;->rD:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    :cond_0
    return-void
.end method

.method public br()V
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/ag;->aZ()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/ah;->rE:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    :cond_0
    return-void
.end method

.method public cM()V
    .locals 2

    iget-object v0, p0, Lb/ah;->sn:Ljava/lang/String;

    sget-boolean v1, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lb/ah;->pE:Z

    if-eqz v1, :cond_1

    iget-object v0, p0, Lb/ah;->sn:Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v1, p0, Lb/ah;->rZ:Ljava/lang/String;

    invoke-static {v1, v0}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    iget-boolean v1, p0, Lb/ah;->su:Z

    if-eqz v1, :cond_2

    const-string v0, "1728000"

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lb/ah;->pE:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lb/ah;->pY:Z

    if-eqz v1, :cond_4

    :cond_3
    const-string v0, "0"

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lb/ah;->pL:Z

    if-eqz v1, :cond_5

    iget-object v0, p0, Lb/ah;->sn:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget-boolean v1, p0, Lb/ah;->ra:Z

    if-eqz v1, :cond_6

    const-string v0, "1190400"

    goto :goto_0

    :cond_6
    iget-boolean v1, p0, Lb/ah;->rD:Z

    if-nez v1, :cond_7

    iget-boolean v1, p0, Lb/ah;->rE:Z

    if-eqz v1, :cond_8

    :cond_7
    const-string v0, "652800"

    goto :goto_0

    :cond_8
    iget-boolean v1, p0, Lb/ah;->ru:Z

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lb/ah;->rv:Z

    if-eqz v1, :cond_0

    :cond_9
    const-string v0, "960000"

    goto :goto_0
.end method

.method public cN()V
    .locals 2

    iget-boolean v0, p0, Lb/ah;->pY:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ah;->rY:Ljava/lang/String;

    const-string v1, "3"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-boolean v0, p0, Lb/ah;->pE:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lb/ah;->rY:Ljava/lang/String;

    const-string v1, "0"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lb/ah;->su:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const-string v1, "20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const-string v1, "80000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const-string v1, "65 1400000:70 1700000:75"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "40"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lb/ah;->pY:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "100"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-boolean v0, p0, Lb/ah;->rD:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const-string v1, "20000 1400000:80000 1500000:40000 1700000:20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    const-string v1, "99"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    const-string v1, "1190400"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const-string v1, "40000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const-string v1, "85 1400000:90 1700000:95"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    const-string v1, "20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    const-string v1, "100000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "20"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public cO()V
    .locals 2

    iget-boolean v0, p0, Lb/ah;->pE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const-string v1, "19000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    const-string v1, "90"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    const-string v1, "1497600"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const-string v1, "79000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const-string v1, "60 800000:65 1400000:70"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    const-string v1, "20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    const-string v1, "300000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "100"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lb/ah;->pY:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const-string v1, "19000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    const-string v1, "90"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    const-string v1, "1497600"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const-string v1, "79000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const-string v1, "60 800000:65 1400000:70"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    const-string v1, "20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    const-string v1, "300000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "100"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lb/ah;->rD:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    const-string v1, "20000 1400000:80000 1500000:40000 1700000:20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    const-string v1, "99"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    const-string v1, "1190400"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    const-string v1, "40000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    const-string v1, "85 1400000:90 1700000:95"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    const-string v1, "20000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    const-string v1, "100000"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    const-string v1, "20"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lb/ah;->rS:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sg:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rU:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sh:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rT:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->si:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rV:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sj:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rR:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sk:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rW:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sl:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->rX:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sm:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sa:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->so:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public cP()V
    .locals 2

    iget-boolean v0, p0, Lb/ah;->sv:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    const-string v1, "Mode change is disabled."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sb:Ljava/lang/String;

    const-string v1, "800"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sc:Ljava/lang/String;

    const-string v1, "200"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sd:Ljava/lang/String;

    const-string v1, "800"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->se:Ljava/lang/String;

    const-string v1, "200"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/ah;->TAG:Ljava/lang/String;

    const-string v1, "Mode change is enabled."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sb:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sp:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sc:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sq:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->sd:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->sr:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ah;->se:Ljava/lang/String;

    iget-object v1, p0, Lb/ah;->ss:Ljava/lang/String;

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 10

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lb/ah;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v1, Lb/ah;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v6

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v0}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v8

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->T(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez v7, :cond_2

    if-eqz v8, :cond_6

    :cond_2
    move v1, v3

    :goto_1
    sget-boolean v4, Lcom/android/server/ssrm/N;->eo:Z

    if-eqz v4, :cond_9

    iget-object v4, p0, Lb/ah;->sw:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lb/ah;->sx:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lb/ah;->sy:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lb/ah;->sz:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_3
    move v4, v3

    :goto_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->dC:Z

    if-eqz v0, :cond_8

    if-eqz v6, :cond_8

    invoke-static {}, Lcom/android/server/ssrm/G;->aj()Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lb/ah;->mContext:Landroid/content/Context;

    const-string v9, "wifi"

    invoke-virtual {v0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getLinkSpeed()I

    move-result v0

    const/16 v9, 0xea

    if-lt v0, v9, :cond_8

    move v0, v3

    :goto_3
    iget-boolean v9, p0, Lb/ah;->sv:Z

    if-eqz v9, :cond_7

    if-nez v8, :cond_4

    iput-boolean v2, p0, Lb/ah;->sv:Z

    invoke-virtual {p0}, Lb/ah;->cP()V

    :cond_4
    :goto_4
    iget-boolean v2, p0, Lb/ah;->pL:Z

    if-ne v2, v6, :cond_5

    iget-boolean v2, p0, Lb/ah;->ra:Z

    if-ne v2, v4, :cond_5

    iget-boolean v2, p0, Lb/ah;->pE:Z

    if-ne v2, v5, :cond_5

    iget-boolean v2, p0, Lb/ah;->ru:Z

    if-ne v2, v7, :cond_5

    iget-boolean v2, p0, Lb/ah;->su:Z

    if-ne v2, v0, :cond_5

    iget-boolean v2, p0, Lb/ah;->rz:Z

    if-eq v2, v1, :cond_0

    :cond_5
    iput-boolean v6, p0, Lb/ah;->pL:Z

    iput-boolean v4, p0, Lb/ah;->ra:Z

    iput-boolean v5, p0, Lb/ah;->pE:Z

    iput-boolean v7, p0, Lb/ah;->ru:Z

    iput-boolean v0, p0, Lb/ah;->su:Z

    iput-boolean v1, p0, Lb/ah;->rz:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    if-eqz v8, :cond_4

    iget-object v2, p0, Lb/ah;->mHandler:Landroid/os/Handler;

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v2, p0, Lb/ah;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    iput v3, v2, Landroid/os/Message;->what:I

    iget-object v3, p0, Lb/ah;->mHandler:Landroid/os/Handler;

    const-wide/16 v8, 0x2710

    invoke-virtual {v3, v2, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_4

    :cond_8
    move v0, v2

    goto :goto_3

    :cond_9
    move v4, v2

    goto :goto_2
.end method

.method public cg()V
    .locals 2

    invoke-virtual {p0}, Lb/ah;->cM()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/ah;->cN()V

    :goto_0
    iget-boolean v0, p0, Lb/ah;->rz:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/ah;->sf:Ljava/lang/String;

    const-string v1, "0"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lb/ah;->cO()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lb/ah;->sf:Ljava/lang/String;

    const-string v1, "1"

    invoke-static {v0, v1}, Lb/ah;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onScreenOn()V
    .locals 1

    iget-boolean v0, p0, Lb/ah;->rE:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/ah;->rE:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/ah;->rv:Z

    invoke-virtual {p0}, Lb/ah;->cg()V

    return-void
.end method
