.class public Lb/J;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/t;
.implements Lb/w;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field J:I

.field mPackageName:Ljava/lang/String;

.field final pb:Ljava/lang/String;

.field final pc:Ljava/lang/String;

.field final pd:Ljava/lang/String;

.field final pe:Ljava/lang/String;

.field final pf:Ljava/lang/String;

.field final pg:I

.field final ph:I

.field pi:Landroid/os/DVFSHelper;

.field pj:Landroid/os/DVFSHelper;

.field pk:Landroid/os/DVFSHelper;

.field pl:Landroid/os/DVFSHelper;

.field final pm:I

.field final pn:I

.field private po:Z

.field private pp:Z

.field private pq:Z

.field private pr:Z

.field private ps:Z

.field private pt:Z

.field private pu:Z

.field private pv:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/J;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/J;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x1c

    const/16 v4, 0x15

    const/16 v3, 0x14

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/J;->pb:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/J;->pc:Ljava/lang/String;

    new-array v0, v5, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/J;->pd:Ljava/lang/String;

    new-array v0, v5, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/J;->pe:Ljava/lang/String;

    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/J;->pf:Ljava/lang/String;

    iput v1, p0, Lb/J;->pg:I

    const/4 v0, 0x1

    iput v0, p0, Lb/J;->ph:I

    iput-object v2, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    iput-object v2, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    iput-object v2, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    iput-object v2, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    const/16 v0, 0x3a98

    iput v0, p0, Lb/J;->pm:I

    const/16 v0, 0x2710

    iput v0, p0, Lb/J;->pn:I

    const/4 v0, -0x1

    iput v0, p0, Lb/J;->J:I

    iput-boolean v1, p0, Lb/J;->po:Z

    iput-boolean v1, p0, Lb/J;->pp:Z

    iput-boolean v1, p0, Lb/J;->pq:Z

    iput-boolean v1, p0, Lb/J;->pr:Z

    iput-boolean v1, p0, Lb/J;->ps:Z

    iput-boolean v1, p0, Lb/J;->pt:Z

    iput-boolean v1, p0, Lb/J;->pu:Z

    iput-boolean v1, p0, Lb/J;->pv:Z

    const-string v0, ""

    iput-object v0, p0, Lb/J;->mPackageName:Ljava/lang/String;

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/J;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x20

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/J;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/J;->addPackage(Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-array v0, v3, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/J;->addPackage(Ljava/lang/String;)V

    new-array v0, v3, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/J;->addPackage(Ljava/lang/String;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x13
        0x16
        0x15
        0x15
        0x54
        0x9
        0xf
        0x18
        0xd
        0x1b
        0x3
        0x9
        0xf
        0x8
        0x1c
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0x48
        0x11
        0x9
        0xa
        0x15
        0x8
        0xe
        0x9
        0x54
        0x14
        0x18
        0x1b
        0x48
        0x11
        0x4b
        0x49
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0x48
        0x11
        0x9
        0xa
        0x15
        0x8
        0xe
        0x9
        0x54
        0x14
        0x18
        0x1b
        0x48
        0x11
        0x4b
        0x4e
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x17
        0x15
        0x1e
        0xf
        0x16
        0x1f
        0x55
        0x19
        0xa
        0xf
        0x13
        0x1e
        0x16
        0x1f
        0x55
        0xa
        0x1b
        0x8
        0x1b
        0x17
        0x1f
        0xe
        0x1f
        0x8
        0x9
        0x55
        0x15
        0x1c
        0x1c
    .end array-data

    :array_5
    .array-data 4
        0x1e
        0x11
        0x54
        0x16
        0x15
        0x1d
        0x13
        0x9
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x13
        0x8
        0x1b
        0xe
        0xe
        0x1b
        0x19
        0x11
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1e
        0x1f
        0xc
        0x9
        0x13
        0x9
        0xe
        0x1f
        0x8
        0x9
        0x54
        0x39
        0x15
        0x15
        0x11
        0x13
        0x1f
        0x28
        0xf
        0x14
        0x3c
        0x15
        0x8
        0x31
        0x1b
        0x11
        0x1b
        0x15
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x16
        0x13
        0x14
        0x11
        0xe
        0x15
        0x17
        0x15
        0x8
        0x8
        0x15
        0xd
        0x54
        0xd
        0x13
        0x14
        0x1e
        0x8
        0xf
        0x14
        0x14
        0x1f
        0x8
    .end array-data

    :array_8
    .array-data 4
        0x12
        0x1f
        0x1b
        0xe
        0x49
        0x1d
    .end array-data

    :array_9
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_a
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x13
        0x16
        0x15
        0x15
        0x54
        0x9
        0xf
        0x18
        0xd
        0x1b
        0x3
        0x9
        0xf
        0x8
        0x1c
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/J;->pp:Z

    invoke-virtual {p0}, Lb/J;->cI()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/J;->update()V

    :cond_0
    return-void
.end method

.method public cE()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/J;->pp:Z

    invoke-virtual {p0}, Lb/J;->cI()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/J;->update()V

    :cond_0
    return-void
.end method

.method protected declared-synchronized cI()V
    .locals 6

    monitor-enter p0

    :try_start_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lb/J;->pq:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lb/J;->po:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lb/J;->pp:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    if-nez v0, :cond_2

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/J;->mContext:Landroid/content/Context;

    const/16 v2, 0xc

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xc

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/android/server/ssrm/N;->cw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const v3, 0x124f80

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    if-nez v0, :cond_3

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/J;->mContext:Landroid/content/Context;

    const/16 v2, 0xd

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/J;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xe

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_3
    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v0, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/J;->pq:Z

    sget-object v0, Lb/J;->TAG:Ljava/lang/String;

    const-string v1, "acquire lock"

    invoke-static {v0, v1}, Lb/J;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const v3, 0xea600

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lb/J;->pq:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/J;->pq:Z

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iget-object v0, p0, Lb/J;->pl:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    sget-object v0, Lb/J;->TAG:Ljava/lang/String;

    const-string v1, "release lock"

    invoke-static {v0, v1}, Lb/J;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :array_0
    .array-data 4
        0x3d
        0x3b
        0x37
        0x3f
        0x25
        0x39
        0x2a
        0x2f
        0x25
        0x37
        0x33
        0x34
    .end array-data

    :array_1
    .array-data 4
        0x12
        0x1f
        0x1b
        0xe
        0x49
        0x1d
    .end array-data

    :array_2
    .array-data 4
        0x3d
        0x3b
        0x37
        0x3f
        0x25
        0x39
        0x35
        0x28
        0x3f
        0x25
        0x37
        0x33
        0x34
    .end array-data
.end method

.method protected declared-synchronized cJ()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lb/J;->ps:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/J;->pt:Z

    if-eqz v0, :cond_2

    :cond_0
    iget v0, p0, Lb/J;->J:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    iget v1, p0, Lb/J;->J:I

    invoke-virtual {v0, v1}, Landroid/os/DVFSHelper;->acquire(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public cb()V
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lb/J;->bH()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lb/J;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lb/J;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lb/J;->pb:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lb/J;->ps:Z

    iget-object v1, p0, Lb/J;->pc:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lb/J;->pt:Z

    iget-object v1, p0, Lb/J;->pd:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lb/J;->pu:Z

    iget-object v1, p0, Lb/J;->pe:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/J;->pv:Z

    iget-boolean v0, p0, Lb/J;->ps:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x3a98

    iput v0, p0, Lb/J;->J:I

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lb/J;->update()V

    invoke-virtual {p0}, Lb/J;->cJ()V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lb/J;->pt:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x2710

    iput v0, p0, Lb/J;->J:I

    goto :goto_1
.end method

.method protected declared-synchronized update()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/J;->mContext:Landroid/content/Context;

    const-string v2, "GAME_CPU_MIN"

    const/16 v3, 0xc

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    const-wide/32 v2, 0x927c0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_0
    iget-object v0, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/J;->mContext:Landroid/content/Context;

    const-string v2, "GAME_CPU_MIN_TIMEOUT"

    const/16 v3, 0xc

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/J;->pk:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    const-wide/32 v2, 0x1b7740

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_1
    iget-boolean v0, p0, Lb/J;->pq:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lb/J;->ps:Z

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lb/J;->pp:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/J;->pq:Z

    :cond_2
    :goto_0
    iget-object v0, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    if-nez v0, :cond_3

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/J;->mContext:Landroid/content/Context;

    const-string v2, "GAMENBA_BUS_MIN"

    const/16 v3, 0x13

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    const-wide/32 v2, 0xc96a8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_3
    iget-boolean v0, p0, Lb/J;->pr:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lb/J;->pp:Z

    if-nez v0, :cond_7

    iget-boolean v0, p0, Lb/J;->pu:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lb/J;->pv:Z

    if-eqz v0, :cond_7

    :cond_4
    iget-object v0, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    sget-object v0, Lb/J;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/J;->pf:Ljava/lang/String;

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/J;->pr:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    :goto_1
    monitor-exit p0

    return-void

    :cond_6
    :try_start_1
    iget-boolean v0, p0, Lb/J;->pq:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/J;->pj:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/J;->pq:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    :try_start_2
    iget-boolean v0, p0, Lb/J;->pr:Z

    if-eqz v0, :cond_5

    sget-object v0, Lb/J;->TAG:Ljava/lang/String;

    iget-object v1, p0, Lb/J;->pf:Ljava/lang/String;

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/J;->pi:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/J;->pr:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/J;->po:Z

    invoke-virtual {p0}, Lb/J;->cI()V

    return-void
.end method
