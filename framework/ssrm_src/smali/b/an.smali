.class public final Lb/an;
.super Lb/p;

# interfaces
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field pG:[Ljava/lang/String;

.field pL:Z

.field pY:Z

.field final rh:Ljava/lang/String;

.field rz:Z

.field sK:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/an;->TAG:Ljava/lang/String;

    iput-boolean v2, p0, Lb/an;->pL:Z

    iput-boolean v2, p0, Lb/an;->sK:Z

    iput-boolean v2, p0, Lb/an;->pY:Z

    iput-boolean v2, p0, Lb/an;->rz:Z

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x1b

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/an;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const/16 v2, 0x14

    new-array v2, v2, [I

    fill-array-data v2, :array_1

    invoke-static {v2}, Lb/an;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x1a

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/an;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/an;->pG:[Ljava/lang/String;

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/an;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/an;->rh:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x19
        0x1b
        0x17
        0x1f
        0x8
        0x1b
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
    .end array-data
.end method


# virtual methods
.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Camera_recordingDual"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iput-boolean p2, p0, Lb/an;->sK:Z

    invoke-virtual {p0}, Lb/an;->cQ()V

    :cond_1
    return-void
.end method

.method public cQ()V
    .locals 3

    const/4 v2, 0x2

    const/4 v1, 0x1

    iget-boolean v0, p0, Lb/an;->rz:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lb/an;->sK:Z

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "sampling_down_factor"

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_early_factor"

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_interim_factor"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lb/an;->pL:Z

    if-eqz v0, :cond_2

    const-string v0, "sampling_down_factor"

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_early_factor"

    invoke-virtual {p0, v0, v2}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_interim_factor"

    const/16 v1, 0x14

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    const-string v0, "sampling_down_factor"

    invoke-virtual {p0, v0, v2}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_early_factor"

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    const-string v0, "sampling_interim_factor"

    invoke-virtual {p0, v0, v1}, Lb/an;->j(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public cb()V
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lb/an;->bH()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lb/an;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lb/an;->pL:Z

    iput-boolean v0, p0, Lb/an;->rz:Z

    iget-object v2, p0, Lb/an;->pG:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/an;->rz:Z

    :cond_1
    invoke-virtual {p0}, Lb/an;->cQ()V

    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public j(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lb/an;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lb/an;->rh:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lb/an;->b(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
