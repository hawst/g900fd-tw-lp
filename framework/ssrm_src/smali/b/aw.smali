.class public final Lb/aw;
.super Lb/p;

# interfaces
.implements Lb/D;
.implements Lb/G;
.implements Lb/s;
.implements Lb/t;
.implements Lb/y;


# static fields
.field static final tv:Ljava/util/Hashtable;

.field static final tw:Ljava/util/Hashtable;


# instance fields
.field final TAG:Ljava/lang/String;

.field mZ:Z

.field oT:Z

.field pE:Z

.field tA:I

.field tB:Z

.field tC:Z

.field tD:Z

.field tE:Z

.field tF:Z

.field final tu:[Ljava/lang/String;

.field final tx:I

.field final ty:I

.field tz:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v4, 0x15

    const/16 v3, 0x13

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lb/aw;->tv:Ljava/util/Hashtable;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    sget-object v0, Lb/aw;->tv:Ljava/util/Hashtable;

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tv:Ljava/util/Hashtable;

    const/16 v1, 0x20

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    const/16 v1, 0x1c

    new-array v1, v1, [I

    fill-array-data v1, :array_3

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    const/16 v1, 0x1b

    new-array v1, v1, [I

    fill-array-data v1, :array_4

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    new-array v1, v4, [I

    fill-array-data v1, :array_5

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    const/16 v1, 0x1a

    new-array v1, v1, [I

    fill-array-data v1, :array_6

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    new-array v1, v4, [I

    fill-array-data v1, :array_7

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x19
        0x16
        0x15
        0x19
        0x11
        0xa
        0x1b
        0x19
        0x11
        0x1b
        0x1d
        0x1f
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x9
        0x54
        0x17
        0x1b
        0xa
        0x9
    .end array-data

    :array_4
    .array-data 4
        0x10
        0xa
        0x54
        0x19
        0x15
        0x54
        0x14
        0xe
        0xe
        0x1e
        0x15
        0x19
        0x15
        0x17
        0x15
        0x54
        0x19
        0x1b
        0x8
        0x8
        0x13
        0x1f
        0x8
        0x17
        0x1b
        0x13
        0x16
    .end array-data

    :array_5
    .array-data 4
        0x10
        0xa
        0x54
        0x14
        0x1b
        0xc
        0x1f
        0x8
        0x54
        0x16
        0x13
        0x14
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3
        0x15
        0xf
        0xe
        0xf
        0x18
        0x1f
    .end array-data

    :array_7
    .array-data 4
        0x10
        0xa
        0x54
        0x19
        0x15
        0x54
        0x10
        0x15
        0x12
        0x15
        0x9
        0xa
        0x1b
        0x19
        0x1f
        0x54
        0x10
        0x15
        0x8
        0xe
        0x1f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/aw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/aw;->TAG:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x17

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/aw;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, Lb/aw;->tu:[Ljava/lang/String;

    iput v3, p0, Lb/aw;->tx:I

    iput v3, p0, Lb/aw;->ty:I

    iput v3, p0, Lb/aw;->tz:I

    iput v3, p0, Lb/aw;->tA:I

    iput-boolean v2, p0, Lb/aw;->mZ:Z

    iput-boolean v2, p0, Lb/aw;->tB:Z

    iput-boolean v2, p0, Lb/aw;->tC:Z

    iput-boolean v2, p0, Lb/aw;->tD:Z

    iput-boolean v2, p0, Lb/aw;->tE:Z

    iput-boolean v2, p0, Lb/aw;->oT:Z

    iput-boolean v2, p0, Lb/aw;->pE:Z

    iput-boolean v2, p0, Lb/aw;->tF:Z

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data
.end method

.method private cZ()I
    .locals 4

    const/4 v1, 0x3

    const/4 v0, 0x2

    iget-boolean v2, p0, Lb/aw;->oT:Z

    if-eqz v2, :cond_5

    const-string v2, "com.android.mms"

    invoke-virtual {p0}, Lb/aw;->bH()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lb/aw;->tE:Z

    if-eqz v2, :cond_4

    :cond_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v1

    :cond_2
    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x5

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lb/aw;->tC:Z

    if-nez v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lb/aw;->tB:Z

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v1, p0, Lb/aw;->tD:Z

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private da()I
    .locals 2

    const/4 v0, 0x2

    iget-boolean v1, p0, Lb/aw;->oT:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lb/aw;->tC:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v0, p0, Lb/aw;->tB:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lb/aw;->bH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->S(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    const/4 v0, 0x5

    goto :goto_0

    :cond_5
    iget-boolean v1, p0, Lb/aw;->tB:Z

    if-eqz v1, :cond_6

    const/4 v0, 0x4

    goto :goto_0

    :cond_6
    iget-boolean v1, p0, Lb/aw;->tF:Z

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private db()I
    .locals 2

    const/4 v0, 0x2

    iget-boolean v1, p0, Lb/aw;->oT:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lb/aw;->tC:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private dc()I
    .locals 2

    const/4 v0, 0x2

    iget-boolean v1, p0, Lb/aw;->oT:Z

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lb/aw;->tC:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-boolean v1, p0, Lb/aw;->tB:Z

    if-eqz v1, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->cP:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    goto :goto_0

    :cond_4
    iget-boolean v1, p0, Lb/aw;->pE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/server/ssrm/N;->em:Z

    if-nez v1, :cond_5

    sget-boolean v1, Lcom/android/server/ssrm/N;->dR:Z

    if-eqz v1, :cond_0

    :cond_5
    const/16 v0, 0x9

    goto :goto_0
.end method


# virtual methods
.method public J(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/aw;->oT:Z

    invoke-virtual {p0}, Lb/aw;->cX()V

    return-void
.end method

.method public cB()V
    .locals 0

    invoke-virtual {p0}, Lb/aw;->cX()V

    return-void
.end method

.method public cF()V
    .locals 0

    invoke-virtual {p0}, Lb/aw;->cX()V

    return-void
.end method

.method public cG()V
    .locals 0

    invoke-virtual {p0}, Lb/aw;->cX()V

    return-void
.end method

.method cX()V
    .locals 3

    iget v0, p0, Lb/aw;->tz:I

    invoke-virtual {p0}, Lb/aw;->cY()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lb/aw;->cY()I

    move-result v0

    iput v0, p0, Lb/aw;->tz:I

    iget-object v0, p0, Lb/aw;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "boost_level,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lb/aw;->tz:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->d(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lb/aw;->tA:I

    invoke-virtual {p0}, Lb/aw;->dd()I

    move-result v1

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lb/aw;->dd()I

    move-result v0

    iput v0, p0, Lb/aw;->tA:I

    iget-object v0, p0, Lb/aw;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lb/aw;->tA:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method cY()I
    .locals 1

    invoke-static {}, Lcom/android/server/ssrm/aT;->bG()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->aa()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->isEmergencyMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/android/server/ssrm/G;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_3

    :cond_2
    invoke-direct {p0}, Lb/aw;->da()I

    move-result v0

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->de:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_5

    :cond_4
    invoke-direct {p0}, Lb/aw;->cZ()I

    move-result v0

    goto :goto_0

    :cond_5
    sget-boolean v0, Lcom/android/server/ssrm/N;->ec:Z

    if-nez v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->ed:Z

    if-eqz v0, :cond_7

    :cond_6
    invoke-direct {p0}, Lb/aw;->db()I

    move-result v0

    goto :goto_0

    :cond_7
    invoke-direct {p0}, Lb/aw;->dc()I

    move-result v0

    goto :goto_0
.end method

.method public cb()V
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/aw;->tB:Z

    iput-boolean v0, p0, Lb/aw;->tC:Z

    iput-boolean v0, p0, Lb/aw;->tF:Z

    invoke-virtual {p0}, Lb/aw;->bH()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v2, Lb/aw;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput-boolean v5, p0, Lb/aw;->tB:Z

    :cond_1
    invoke-virtual {v2, v1}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lb/aw;->pE:Z

    iget-object v2, p0, Lb/aw;->tu:[Ljava/lang/String;

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iput-boolean v5, p0, Lb/aw;->tC:Z

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->df:Z

    if-eqz v0, :cond_3

    invoke-static {v1}, Lcom/android/server/ssrm/W;->m(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v5, p0, Lb/aw;->tF:Z

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->cE:Z

    if-eqz v0, :cond_4

    sget-object v0, Lb/aw;->tv:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/aw;->tD:Z

    sget-object v0, Lb/aw;->tw:Ljava/util/Hashtable;

    invoke-virtual {v0, v1}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/aw;->tE:Z

    :cond_4
    invoke-virtual {p0}, Lb/aw;->cX()V

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method dd()I
    .locals 1

    iget-boolean v0, p0, Lb/aw;->tB:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
