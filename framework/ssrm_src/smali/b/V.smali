.class public final Lb/V;
.super Lb/a;

# interfaces
.implements Lb/E;
.implements Lb/r;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field fJ:Landroid/os/DVFSHelper;

.field pE:Z

.field pH:I

.field pI:I

.field pJ:I

.field pM:Z

.field pP:I

.field pQ:I

.field pR:I

.field qI:Landroid/os/DVFSHelper;

.field qJ:Landroid/os/DVFSHelper;

.field qK:I

.field qL:I

.field qM:I

.field qN:I

.field qO:I

.field qP:Z

.field qQ:Z

.field qR:Z

.field qS:Z

.field final qT:Ljava/lang/String;

.field final qU:Ljava/lang/String;

.field final qV:Ljava/lang/String;

.field final qW:Ljava/lang/String;

.field final qX:Ljava/lang/String;

.field final qY:Ljava/lang/String;

.field final qZ:Ljava/lang/String;

.field ra:Z

.field rb:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x38

    const/16 v4, 0x36

    const/16 v3, 0x23

    const/16 v2, 0x14

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/V;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/V;->pM:Z

    iput-boolean v1, p0, Lb/V;->pE:Z

    iput-boolean v1, p0, Lb/V;->qP:Z

    iput-boolean v1, p0, Lb/V;->qQ:Z

    iput-boolean v1, p0, Lb/V;->qR:Z

    iput-boolean v1, p0, Lb/V;->qS:Z

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qT:Ljava/lang/String;

    new-array v0, v2, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qU:Ljava/lang/String;

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qV:Ljava/lang/String;

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qW:Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qX:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qY:Ljava/lang/String;

    new-array v0, v3, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/V;->qZ:Ljava/lang/String;

    iput-boolean v1, p0, Lb/V;->ra:Z

    iput-boolean v1, p0, Lb/V;->rb:Z

    iget-object v0, p0, Lb/V;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-1"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->qK:I

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pI:I

    const/16 v0, 0x31

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->qL:I

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pP:I

    new-array v0, v5, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "624000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pJ:I

    new-array v0, v4, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->qM:I

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "80000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pQ:I

    new-array v0, v4, [I

    fill-array-data v0, :array_e

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pR:I

    const/16 v0, 0x37

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "80000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->qN:I

    new-array v0, v5, [I

    fill-array-data v0, :array_10

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->pH:I

    const/16 v0, 0x33

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "416000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/V;->qO:I

    new-array v0, v2, [I

    fill-array-data v0, :array_12

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_13

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_15

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    new-array v0, v3, [I

    fill-array-data v0, :array_16

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x3f

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    invoke-static {v0}, Lb/V;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
        0xa
        0x16
        0x1b
        0x3
        0x1f
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0x1b
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x8
        0x49
        0x25
        0x8
        0x15
        0xd
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0x1b
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x8
        0x49
        0x25
        0x14
        0x1b
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4c
        0x32
        0x2a
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3d
        0x3b
        0x34
        0x3e
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3b
        0x4c
        0x32
        0x2a
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0xa
        0x15
        0xd
        0x1f
        0x8
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x25
        0x17
        0x1b
        0x2
        0x25
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x13
        0x15
        0x25
        0x13
        0x9
        0x25
        0x18
        0xf
        0x9
        0x3
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_f
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x9
        0x16
        0x1b
        0x19
        0x11
    .end array-data

    :array_10
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_11
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x19
        0x16
        0x1b
        0x9
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x1e
        0x1f
        0xc
        0x1c
        0x8
        0x1f
        0xb
        0x57
        0xc
        0xa
        0xf
        0x54
        0x4a
        0x55
        0xf
        0x9
        0x1f
        0x8
        0x9
        0xa
        0x1b
        0x19
        0x1f
        0x55
        0x9
        0x1f
        0xe
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_12
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_13
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x8
        0x15
        0xc
        0x13
        0x15
        0x54
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_14
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_15
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x13
        0x16
        0x15
        0x15
        0x54
        0x9
        0xf
        0x18
        0xd
        0x1b
        0x3
        0x9
        0xf
        0x8
        0x1c
    .end array-data

    :array_16
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x16
        0x15
        0x1c
        0xe
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x3b
        0x34
        0x37
        0x2a
        0x54
        0x3d
        0x16
        0x15
        0x1c
        0xe
        0x3e
        0x37
        0x32
        0x37
    .end array-data

    :array_17
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x19
        0x15
        0x17
        0x48
        0xf
        0x9
        0x54
        0x12
        0x15
        0x17
        0x1f
        0x8
        0xf
        0x14
        0x18
        0x1b
        0xe
        0xe
        0x16
        0x1f
        0x48
        0x54
        0x14
        0x15
        0x8
        0x17
        0x1b
        0x16
        0x54
        0x1c
        0x8
        0x1f
        0x1f
        0x1c
        0xf
        0x16
        0x16
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1d
        0x16
        0x15
        0x18
        0x1b
        0x16
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data
.end method


# virtual methods
.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/V;->pM:Z

    invoke-virtual {p0}, Lb/V;->update()V

    :cond_0
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/V;->qP:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/V;->qK:I

    const-string v1, "624000"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lb/V;->pE:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/V;->pJ:I

    const-string v1, "1066000"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/V;->qL:I

    const-string v1, "100"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/V;->qM:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/V;->pQ:I

    const-string v1, "50000"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/V;->pP:I

    const-string v1, "80"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lb/V;->qQ:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lb/V;->qO:I

    const-string v1, "208000"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-boolean v0, p0, Lb/V;->ra:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/V;->qL:I

    const-string v1, "100"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lb/V;->pM:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/V;->qO:I

    const-string v1, "312000"

    invoke-virtual {p0, v0, v1}, Lb/V;->b(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public cb()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/V;->bH()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lb/V;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/V;->pE:Z

    iget-object v0, p0, Lb/V;->qT:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/V;->qU:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    sget-boolean v0, Lcom/android/server/ssrm/N;->dX:Z

    if-eqz v0, :cond_6

    :cond_1
    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lb/V;->qP:Z

    iget-object v0, p0, Lb/V;->qV:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/V;->qQ:Z

    sget-boolean v0, Lcom/android/server/ssrm/N;->dW:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lb/V;->qW:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/V;->qX:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_2
    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lb/V;->qR:Z

    :cond_3
    :goto_3
    iget-boolean v0, p0, Lb/V;->qR:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lb/V;->qP:Z

    if-eqz v0, :cond_b

    :cond_4
    iget-boolean v0, p0, Lb/V;->qS:Z

    if-nez v0, :cond_b

    iget-object v0, p0, Lb/V;->qI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iput-boolean v2, p0, Lb/V;->qS:Z

    :cond_5
    :goto_4
    invoke-virtual {p0}, Lb/V;->update()V

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    sget-boolean v0, Lcom/android/server/ssrm/N;->dX:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/V;->qY:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lb/V;->qZ:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lb/V;->qR:Z

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_5

    :cond_b
    iget-boolean v0, p0, Lb/V;->qS:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lb/V;->qR:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lb/V;->qP:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lb/V;->qI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    iput-boolean v1, p0, Lb/V;->qS:Z

    goto :goto_4
.end method

.method public ci()V
    .locals 8

    const-wide/32 v6, 0x4c2c0

    const-wide/16 v4, 0x0

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/V;->mContext:Landroid/content/Context;

    const-string v2, "MRVL_GPU_MIN"

    const/16 v3, 0x10

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/V;->mContext:Landroid/content/Context;

    const-string v2, "MRVL_DDR_MAX"

    const/16 v3, 0x14

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/V;->qI:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/V;->qI:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/V;->mContext:Landroid/content/Context;

    const-string v2, "MRVL_DDR_MIN"

    const/16 v3, 0x13

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    return-void
.end method

.method public z(Z)V
    .locals 4

    const-wide/32 v2, 0x4c2c0

    iput-boolean p1, p0, Lb/V;->ra:Z

    iget-boolean v0, p0, Lb/V;->ra:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lb/V;->rb:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->cancelExtraOptions()V

    iget-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iget-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->cancelExtraOptions()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    const-string v1, "GPU"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/V;->rb:Z

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lb/V;->update()V

    return-void

    :cond_2
    iget-boolean v0, p0, Lb/V;->rb:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lb/V;->ra:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lb/V;->qJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    sget-boolean v0, Lcom/android/server/ssrm/N;->dM:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/V;->fJ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/V;->rb:Z

    goto :goto_0
.end method
