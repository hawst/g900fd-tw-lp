.class public Lb/e;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/B;


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field final bb:Z

.field final mHandler:Landroid/os/Handler;

.field mNetworkType:I

.field mScreenOn:Z

.field nv:Z

.field nw:J

.field final nx:I

.field final ny:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/e;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v1, p0, Lb/e;->bb:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/e;->mScreenOn:Z

    iput-boolean v1, p0, Lb/e;->nv:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lb/e;->nw:J

    sget v0, Lcom/android/server/ssrm/DataTputMonitor;->bg:I

    iput v0, p0, Lb/e;->mNetworkType:I

    const v0, 0x1312d0

    iput v0, p0, Lb/e;->nx:I

    const v0, 0xbebc20

    iput v0, p0, Lb/e;->ny:I

    new-instance v0, Lb/f;

    invoke-direct {v0, p0}, Lb/f;-><init>(Lb/e;)V

    iput-object v0, p0, Lb/e;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Lb/e;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/android/server/ssrm/DataTputMonitor;->a(Landroid/os/Handler;)V

    return-void
.end method

.method private static cf()I
    .locals 6

    const/4 v1, 0x0

    const/4 v3, 0x0

    :try_start_0
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    const-string v4, "/proc/net/arp"

    invoke-direct {v0, v4}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v3, " +"

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    const/4 v4, 0x4

    if-lt v3, v4, :cond_0

    const/4 v3, 0x3

    aget-object v3, v1, v3

    const-string v4, "..:..:..:..:..:.."

    invoke-virtual {v3, v4}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    aget-object v1, v1, v3

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    const/16 v3, 0x12c

    invoke-virtual {v1, v3}, Ljava/net/InetAddress;->isReachable(I)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    :goto_1
    return v0

    :catch_0
    move-exception v1

    sget-object v2, Lb/e;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lb/e;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catch_1
    move-exception v0

    move-object v2, v3

    move v5, v1

    move-object v1, v0

    move v0, v5

    :goto_2
    :try_start_3
    sget-object v3, Lb/e;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lb/e;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_2

    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v1

    sget-object v2, Lb/e;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lb/e;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_3

    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    sget-object v2, Lb/e;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lb/e;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/e;->mScreenOn:Z

    invoke-virtual {p0}, Lb/e;->ce()V

    return-void
.end method

.method public cd()Z
    .locals 4

    iget-boolean v0, p0, Lb/e;->mScreenOn:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lb/e;->nw:J

    const-wide/32 v2, 0x1312d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lb/e;->nw:J

    const-wide/32 v2, 0xbebc20

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget v0, p0, Lb/e;->mNetworkType:I

    sget v1, Lcom/android/server/ssrm/DataTputMonitor;->TYPE_LTE:I

    if-ne v0, v1, :cond_0

    invoke-static {}, Lb/e;->cf()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized ce()V
    .locals 3

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lb/e;->cd()Z

    move-result v0

    iget-boolean v1, p0, Lb/e;->nv:Z

    if-nez v1, :cond_1

    if-eqz v0, :cond_1

    sget-object v0, Lb/e;->TAG:Ljava/lang/String;

    const-string v1, "/sys/kernel/hmp/up_threshold"

    const-string v2, "1024"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lb/e;->TAG:Ljava/lang/String;

    const-string v1, "/sys/power/enable_dm_hotplug"

    const-string v2, "0"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/e;->nv:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v1, p0, Lb/e;->nv:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    sget-object v0, Lb/e;->TAG:Ljava/lang/String;

    const-string v1, "/sys/kernel/hmp/up_threshold"

    const-string v2, "700"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lb/e;->TAG:Ljava/lang/String;

    const-string v1, "/sys/power/enable_dm_hotplug"

    const-string v2, "1"

    invoke-static {v0, v1, v2}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/e;->nv:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onScreenOn()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/e;->mScreenOn:Z

    invoke-virtual {p0}, Lb/e;->ce()V

    return-void
.end method
