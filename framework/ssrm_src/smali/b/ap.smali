.class public final Lb/ap;
.super Lb/p;

# interfaces
.implements Lb/q;
.implements Lb/r;


# static fields
.field static final DEBUG:Z

.field private static TAG:Ljava/lang/String;


# instance fields
.field fI:Landroid/os/DVFSHelper;

.field mEnabled:Z

.field sP:I

.field sQ:I

.field sR:I

.field sS:I

.field sT:Z

.field sU:I

.field sV:I

.field final sW:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lb/ap;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/ap;->TAG:Ljava/lang/String;

    const-string v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lb/ap;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    const/16 v1, 0x14

    const/16 v3, 0xa

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Lb/p;-><init>()V

    iput v0, p0, Lb/ap;->sU:I

    iput v0, p0, Lb/ap;->sV:I

    iput-boolean v6, p0, Lb/ap;->mEnabled:Z

    const/16 v0, 0x25

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ap;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ap;->sW:Ljava/lang/String;

    sget-boolean v0, Lcom/android/server/ssrm/N;->ds:Z

    if-eqz v0, :cond_1

    const v2, 0x19f0a0

    const v4, 0x16e360

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lb/ap;->a(IIIIZ)V

    :goto_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lb/ap;->sW:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lb/ap;->cR()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v0, :cond_3

    :cond_2
    const v2, 0x155cc0

    const v4, 0x155cc0

    move-object v0, p0

    move v5, v6

    invoke-virtual/range {v0 .. v5}, Lb/ap;->a(IIIIZ)V

    goto :goto_0

    :cond_3
    iput-boolean v5, p0, Lb/ap;->mEnabled:Z

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x25
        0xc
        0x48
        0x54
        0x16
        0x15
        0xd
        0x25
        0x18
        0x1b
        0xe
        0xe
        0x1f
        0x8
        0x3
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data
.end method

.method static getBatteryLevel()I
    .locals 3

    const-string v0, "dev.ssrm.bat_level"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-boolean v1, Lb/ap;->DEBUG:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    :cond_0
    const/16 v0, -0x3e7

    :goto_0
    return v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method final a(IIIIZ)V
    .locals 3

    sget-object v0, Lb/ap;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Init value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lb/ap;->sP:I

    iput p2, p0, Lb/ap;->sR:I

    iput p3, p0, Lb/ap;->sQ:I

    iput p4, p0, Lb/ap;->sS:I

    iput-boolean p5, p0, Lb/ap;->sT:Z

    return-void
.end method

.method ac(I)V
    .locals 5

    const/4 v1, -0x1

    iget v0, p0, Lb/ap;->sQ:I

    if-gt p1, v0, :cond_1

    iget v0, p0, Lb/ap;->sS:I

    :goto_0
    iget v2, p0, Lb/ap;->sU:I

    if-eq v0, v2, :cond_0

    iput v0, p0, Lb/ap;->sU:I

    sget-object v2, Lb/ap;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateLowBatteryMode:: curFreq = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lb/ap;->sU:I

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lb/ap;->fI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget v2, p0, Lb/ap;->sU:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    iget-object v0, p0, Lb/ap;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lb/ap;->sP:I

    if-gt p1, v0, :cond_2

    iget v0, p0, Lb/ap;->sR:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lb/ap;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_1
.end method

.method cR()V
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/FileReader;

    iget-object v3, p0, Lb/ap;->sW:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_0
    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v2, "1st_low_batt_step"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ap;->sP:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :goto_1
    :try_start_2
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :cond_1
    :goto_2
    return-void

    :cond_2
    :try_start_4
    const-string v2, "2nd_low_batt_step"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ap;->sQ:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_3
    if-eqz v1, :cond_3

    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_4
    throw v0

    :cond_4
    :try_start_6
    const-string v2, "1st_low_batt_cpu_max"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ap;->sR:I

    goto :goto_0

    :cond_5
    const-string v2, "2nd_low_batt_cpu_max"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ap;->sS:I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :cond_6
    if-eqz v1, :cond_1

    :try_start_7
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public ci()V
    .locals 6

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/ap;->mContext:Landroid/content/Context;

    const/16 v2, 0xc

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-static {v2}, Lb/ap;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0xd

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/ap;->fI:Landroid/os/DVFSHelper;

    return-void

    nop

    :array_0
    .array-data 4
        0x38
        0x3b
        0x2e
        0x2e
        0x25
        0x39
        0x2a
        0x2f
        0x25
        0x37
        0x3b
        0x22
    .end array-data
.end method

.method public k(Landroid/content/Intent;)V
    .locals 4

    const/4 v3, -0x1

    iget-boolean v0, p0, Lb/ap;->mEnabled:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "level"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-static {}, Lb/ap;->getBatteryLevel()I

    move-result v0

    const/16 v2, -0x3e7

    if-eq v0, v2, :cond_3

    :goto_1
    iget-boolean v1, p0, Lb/ap;->sT:Z

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/android/server/ssrm/G;->U()I

    move-result v1

    if-lez v1, :cond_2

    iget v0, p0, Lb/ap;->sU:I

    if-eq v0, v3, :cond_0

    iput v3, p0, Lb/ap;->sV:I

    iput v3, p0, Lb/ap;->sU:I

    iget-object v0, p0, Lb/ap;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_0

    :cond_2
    sget-object v1, Lb/ap;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onBatteryChange:: mCurrentBatteryLevel = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget v1, p0, Lb/ap;->sV:I

    if-eq v0, v1, :cond_0

    iput v0, p0, Lb/ap;->sV:I

    iget v0, p0, Lb/ap;->sV:I

    invoke-virtual {p0, v0}, Lb/ap;->ac(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
