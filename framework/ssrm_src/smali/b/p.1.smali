.class public abstract Lb/p;
.super Ljava/lang/Object;


# static fields
.field public static final iu:Ljava/lang/String;

.field public static final iw:Ljava/lang/String;

.field public static final ix:Ljava/lang/String;

.field public static final iy:Ljava/lang/String;

.field public static mContext:Landroid/content/Context;

.field private static final oL:Ljava/util/Map;

.field static oR:Lb/z;

.field static oS:Lb/C;


# instance fields
.field final ACTION_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String;

.field final EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String;

.field private aK:Z

.field private mPid:I

.field private oM:Z

.field private oN:Ljava/lang/String;

.field private oO:Z

.field protected final oP:Ljava/util/concurrent/ConcurrentHashMap;

.field oQ:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lb/p;->oL:Ljava/util/Map;

    const/16 v0, 0x2a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/p;->iu:Ljava/lang/String;

    const/16 v0, 0x3a

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/p;->iw:Ljava/lang/String;

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/p;->ix:Ljava/lang/String;

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/p;->iy:Ljava/lang/String;

    new-instance v0, Lb/z;

    invoke-direct {v0}, Lb/z;-><init>()V

    sput-object v0, Lb/p;->oR:Lb/z;

    new-instance v0, Lb/C;

    invoke-direct {v0}, Lb/C;-><init>()V

    sput-object v0, Lb/p;->oS:Lb/C;

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_1
    .array-data 4
        0x9
        0x1f
        0x19
        0x25
        0x19
        0x15
        0x14
        0xe
        0x1b
        0x13
        0x14
        0x1f
        0x8
        0x25
        0x4b
        0x54
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x29
        0x29
        0x28
        0x37
        0x25
        0x28
        0x3f
        0x2b
        0x2f
        0x3f
        0x29
        0x2e
    .end array-data

    :array_2
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0x25
        0x29
        0x2e
        0x3b
        0x2e
        0x2f
        0x29
        0x25
        0x34
        0x3b
        0x37
        0x3f
    .end array-data

    :array_3
    .array-data 4
        0x29
        0x29
        0x28
        0x37
        0x25
        0x29
        0x2e
        0x3b
        0x2e
        0x2f
        0x29
        0x25
        0x2c
        0x3b
        0x36
        0x2f
        0x3f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x30

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/p;->ACTION_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String;

    const/16 v0, 0x29

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/p;->EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String;

    iput-boolean v1, p0, Lb/p;->oM:Z

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/p;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/p;->oN:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lb/p;->mPid:I

    iput-boolean v1, p0, Lb/p;->oO:Z

    iput-boolean v1, p0, Lb/p;->aK:Z

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    iput-boolean v1, p0, Lb/p;->oQ:Z

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x34
        0x35
        0x2e
        0x33
        0x3c
        0x23
        0x25
        0x37
        0x2f
        0x36
        0x2e
        0x33
        0x2d
        0x33
        0x34
        0x3e
        0x35
        0x2d
        0x25
        0x29
        0x2e
        0x3b
        0x2e
        0x2f
        0x29
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1f
        0x2
        0xe
        0x8
        0x1b
        0x54
        0x37
        0x2f
        0x36
        0x2e
        0x33
        0x2d
        0x33
        0x34
        0x3e
        0x35
        0x2d
        0x25
        0x28
        0x2f
        0x34
        0x34
        0x33
        0x34
        0x3d
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x16
        0x1b
        0xf
        0x14
        0x19
        0x12
        0x1f
        0x8
    .end array-data
.end method

.method public static D(Z)V
    .locals 3

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p0}, Lb/p;->E(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static F(Z)V
    .locals 3

    sget-object v0, Lb/p;->oR:Lb/z;

    iput-boolean p0, v0, Lb/z;->oT:Z

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, v0, Lb/D;

    if-eqz v2, :cond_0

    check-cast v0, Lb/D;

    invoke-interface {v0, p0}, Lb/D;->J(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static G(Z)V
    .locals 3

    sget-object v0, Lb/p;->oR:Lb/z;

    iput-boolean p0, v0, Lb/z;->oU:Z

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_0

    instance-of v2, v0, Lb/v;

    if-eqz v2, :cond_0

    check-cast v0, Lb/v;

    invoke-interface {v0, p0}, Lb/v;->a(Z)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static H(Z)V
    .locals 3

    sget-object v0, Lb/p;->oR:Lb/z;

    iget-boolean v0, v0, Lb/z;->oV:Z

    if-ne v0, p0, :cond_1

    :cond_0
    return-void

    :cond_1
    sget-object v0, Lb/p;->oR:Lb/z;

    iput-boolean p0, v0, Lb/z;->oV:Z

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_2

    instance-of v2, v0, Lb/H;

    if-eqz v2, :cond_2

    check-cast v0, Lb/H;

    invoke-interface {v0, p0}, Lb/H;->K(Z)V

    goto :goto_0
.end method

.method public static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 3

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {v0, p0, p1, p2}, Lb/p;->c(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/Class;Lb/p;)V
    .locals 1

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lb/p;->cA()V

    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/Class;)Lb/p;
    .locals 2

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    invoke-virtual {v0}, Lb/p;->cA()V

    sget-object v1, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lb/p;->isRegistered()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p0, p1, v0}, Lb/p;->a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected static b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/android/server/ssrm/aT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static c(Ljava/lang/Class;)Lb/p;
    .locals 1

    sget-object v0, Lb/p;->oL:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lb/p;

    return-object v0
.end method

.method private c(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    sget-object v0, Lb/p;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-object p1, Lb/p;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lb/p;->oN:Ljava/lang/String;

    iput p3, p0, Lb/p;->mPid:I

    instance-of v0, p0, Lb/t;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lb/t;

    invoke-interface {v0}, Lb/t;->cb()V

    iget-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lb/p;->oN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    iget-boolean v1, p0, Lb/p;->oM:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lb/p;->oM:Z

    move-object v0, p0

    check-cast v0, Lb/t;

    iget-boolean v1, p0, Lb/p;->oM:Z

    invoke-interface {v0, v1}, Lb/t;->z(Z)V

    goto :goto_0
.end method

.method protected static logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected static n(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    const-string v0, "Monitor"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "writeStringToSysfs:: path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/p;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v1, "UTF-8"

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_0

    :try_start_2
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_2
    :goto_3
    throw v0

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method protected E(Z)V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V
    .locals 4

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-object p1, Lb/p;->mContext:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lb/p;->ACTION_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lb/p;->EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String;

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lb/p;->oO:Z

    instance-of v0, p3, Lb/x;

    if-eqz v0, :cond_0

    check-cast p3, Lb/x;

    iget-boolean v0, p0, Lb/p;->oO:Z

    invoke-interface {p3, v0}, Lb/x;->I(Z)V

    goto :goto_0

    :cond_2
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/p;->aK:Z

    instance-of v0, p3, Lb/r;

    if-eqz v0, :cond_0

    check-cast p3, Lb/r;

    invoke-interface {p3}, Lb/r;->ci()V

    goto :goto_0

    :cond_3
    sget-object v1, Lb/p;->iu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    sget-object v1, Lb/p;->iw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    sget-object v0, Lb/p;->ix:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lb/p;->iy:Ljava/lang/String;

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "PackageName"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    instance-of v3, p3, Lb/E;

    if-eqz v3, :cond_0

    check-cast p3, Lb/E;

    invoke-interface {p3, v0, v1, v2}, Lb/E;->a(Ljava/lang/String;ZLjava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    instance-of v0, p3, Lb/w;

    if-eqz v0, :cond_0

    check-cast p3, Lb/w;

    invoke-interface {p3}, Lb/w;->cE()V

    goto :goto_0

    :cond_6
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-nez v0, :cond_7

    instance-of v0, p3, Lb/w;

    if-eqz v0, :cond_7

    move-object v0, p3

    check-cast v0, Lb/w;

    invoke-interface {v0}, Lb/w;->cE()V

    :cond_7
    instance-of v0, p3, Lb/B;

    if-eqz v0, :cond_0

    check-cast p3, Lb/B;

    invoke-interface {p3}, Lb/B;->onScreenOn()V

    goto/16 :goto_0

    :cond_8
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    instance-of v0, p3, Lb/A;

    if-eqz v0, :cond_0

    check-cast p3, Lb/A;

    invoke-interface {p3}, Lb/A;->br()V

    goto/16 :goto_0

    :cond_9
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    instance-of v0, p3, Lb/q;

    if-eqz v0, :cond_0

    check-cast p3, Lb/q;

    invoke-interface {p3, p2}, Lb/q;->k(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const-string v1, "android.intent.action.TIME_SET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    :cond_b
    instance-of v0, p3, Lb/F;

    if-eqz v0, :cond_0

    check-cast p3, Lb/F;

    invoke-interface {p3, p2}, Lb/F;->l(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    const-string v1, "com.sec.android.intent.action.EMERGENCY_MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    instance-of v0, p3, Lb/s;

    if-eqz v0, :cond_0

    check-cast p3, Lb/s;

    invoke-interface {p3}, Lb/s;->cB()V

    goto/16 :goto_0

    :cond_d
    const-string v1, "com.sec.android.intent.action.ULTRA_POWER_SAVING_MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    instance-of v0, p3, Lb/G;

    if-eqz v0, :cond_0

    check-cast p3, Lb/G;

    invoke-interface {p3}, Lb/G;->cG()V

    goto/16 :goto_0

    :cond_e
    const-string v1, "com.sec.android.intent.action.POWER_SAVING_MODE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    instance-of v0, p3, Lb/y;

    if-eqz v0, :cond_0

    check-cast p3, Lb/y;

    invoke-interface {p3}, Lb/y;->cF()V

    goto/16 :goto_0

    :cond_f
    const-string v1, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_10

    instance-of v0, p3, Lb/u;

    if-eqz v0, :cond_0

    check-cast p3, Lb/u;

    invoke-interface {p3}, Lb/u;->cC()V

    goto/16 :goto_0

    :cond_10
    if-nez v0, :cond_0

    instance-of v0, p3, Lb/u;

    if-eqz v0, :cond_0

    check-cast p3, Lb/u;

    invoke-interface {p3}, Lb/u;->cD()V

    goto/16 :goto_0
.end method

.method protected addPackage(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method protected bH()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lb/p;->oN:Ljava/lang/String;

    return-object v0
.end method

.method protected bI()I
    .locals 1

    iget v0, p0, Lb/p;->mPid:I

    return v0
.end method

.method cA()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/p;->oQ:Z

    return-void
.end method

.method protected cw()V
    .locals 2

    iget-object v0, p0, Lb/p;->oN:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Lb/t;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, Lb/t;

    invoke-interface {v0}, Lb/t;->cb()V

    iget-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lb/p;->oN:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    iget-boolean v1, p0, Lb/p;->oM:Z

    if-eq v1, v0, :cond_0

    iput-boolean v0, p0, Lb/p;->oM:Z

    move-object v0, p0

    check-cast v0, Lb/t;

    iget-boolean v1, p0, Lb/p;->oM:Z

    invoke-interface {v0, v1}, Lb/t;->z(Z)V

    goto :goto_0
.end method

.method protected cx()Z
    .locals 1

    iget-boolean v0, p0, Lb/p;->oO:Z

    return v0
.end method

.method protected cy()Z
    .locals 1

    iget-boolean v0, p0, Lb/p;->aK:Z

    return v0
.end method

.method protected cz()V
    .locals 1

    iget-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    return-void
.end method

.method protected isPackageExist(Ljava/lang/String;)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lb/p;->oP:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method isRegistered()Z
    .locals 1

    iget-boolean v0, p0, Lb/p;->oQ:Z

    return v0
.end method

.method o(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p1, p2}, Lcom/android/server/ssrm/aT;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
