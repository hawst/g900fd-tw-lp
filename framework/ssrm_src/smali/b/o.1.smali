.class public Lb/o;
.super Lb/p;

# interfaces
.implements Lb/t;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field mPackageName:Ljava/lang/String;

.field final oG:Ljava/lang/String;

.field final oH:Ljava/lang/String;

.field oI:Landroid/os/DVFSHelper;

.field oJ:Z

.field oK:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/o;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/o;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/o;->oG:Ljava/lang/String;

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/o;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/o;->oH:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    iput-boolean v1, p0, Lb/o;->oJ:Z

    iput-boolean v1, p0, Lb/o;->oK:Z

    const-string v0, ""

    iput-object v0, p0, Lb/o;->mPackageName:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x13
        0x17
        0x9
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x19
        0x15
        0x17
        0x17
        0x15
        0x14
        0x13
        0x17
        0x9
        0x9
        0x1f
        0x8
        0xc
        0x13
        0x19
        0x1f
    .end array-data
.end method


# virtual methods
.method public cb()V
    .locals 2

    invoke-virtual {p0}, Lb/o;->bH()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lb/o;->mPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object v0, p0, Lb/o;->mPackageName:Ljava/lang/String;

    iget-object v1, p0, Lb/o;->oG:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lb/o;->oH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/o;->oK:Z

    :goto_1
    invoke-virtual {p0}, Lb/o;->update()V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/o;->oK:Z

    goto :goto_1
.end method

.method protected declared-synchronized update()V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/o;->mContext:Landroid/content/Context;

    const-string v2, "FGAPP_BUS_MIN"

    const/16 v3, 0x13

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    const-wide/32 v2, 0xc96a8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_0
    iget-boolean v0, p0, Lb/o;->oJ:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lb/o;->oK:Z

    if-eqz v0, :cond_2

    sget-object v0, Lb/o;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FgAppBooster : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/o;->oK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/o;->oJ:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-boolean v0, p0, Lb/o;->oJ:Z

    if-eqz v0, :cond_1

    sget-object v0, Lb/o;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FgAppBooster : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/o;->oK:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/o;->oI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/o;->oJ:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
