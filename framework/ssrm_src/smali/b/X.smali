.class public final Lb/X;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/B;


# instance fields
.field final TAG:Ljava/lang/String;

.field rd:I

.field re:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/X;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/X;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/X;->re:Z

    iget-object v0, p0, Lb/X;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/X;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x35

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/X;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "performance"

    invoke-virtual {p0, v0, v1}, Lb/X;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/X;->rd:I

    return-void

    nop

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x9
        0x19
        0x1b
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1d
        0x15
        0xc
        0x1f
        0x8
        0x14
        0x15
        0x8
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/X;->re:Z

    invoke-virtual {p0}, Lb/X;->update()V

    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/X;->re:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/X;->rd:I

    const-string v1, "ondemand"

    invoke-virtual {p0, v0, v1}, Lb/X;->b(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onScreenOn()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/X;->re:Z

    invoke-virtual {p0}, Lb/X;->update()V

    return-void
.end method
