.class public final Lb/ag;
.super Lb/a;

# interfaces
.implements Lb/A;
.implements Lb/B;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field pA:I

.field pE:Z

.field pw:I

.field px:I

.field rQ:I

.field re:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ag;->TAG:Ljava/lang/String;

    iput-boolean v1, p0, Lb/ag;->pE:Z

    iput-boolean v1, p0, Lb/ag;->re:Z

    iget-object v0, p0, Lb/ag;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/ag;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x35

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/ag;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ag;->pA:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/ag;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ag;->pw:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "50000"

    invoke-virtual {p0, v0, v1}, Lb/ag;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ag;->px:I

    const/16 v0, 0x32

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/ag;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "918000"

    invoke-virtual {p0, v0, v1}, Lb/ag;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ag;->rQ:I

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1c
        0x1b
        0x19
        0xe
        0x15
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x13
        0x14
        0x1d
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_3
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x9
        0x3
        0x14
        0x19
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data
.end method


# virtual methods
.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/ag;->re:Z

    invoke-virtual {p0}, Lb/ag;->update()V

    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/ag;->pE:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/ag;->pA:I

    const-string v1, "70"

    invoke-virtual {p0, v0, v1}, Lb/ag;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/ag;->pw:I

    const-string v1, "15"

    invoke-virtual {p0, v0, v1}, Lb/ag;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/ag;->px:I

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/ag;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/ag;->rQ:I

    const-string v1, "1242000"

    invoke-virtual {p0, v0, v1}, Lb/ag;->b(ILjava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lb/ag;->re:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lb/ag;->px:I

    const-string v1, "500000"

    invoke-virtual {p0, v0, v1}, Lb/ag;->b(ILjava/lang/String;)V

    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v0, "sys.pwrcntl.decision.change"

    const-string v1, "0"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public cb()V
    .locals 2

    invoke-virtual {p0}, Lb/ag;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lb/ag;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/server/ssrm/aU;->Q(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/ag;->pE:Z

    invoke-virtual {p0}, Lb/ag;->update()V

    goto :goto_0
.end method

.method public onScreenOn()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/ag;->re:Z

    invoke-virtual {p0}, Lb/ag;->update()V

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
