.class Lb/aD;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic uj:Lb/aB;


# direct methods
.method constructor <init>(Lb/aB;)V
    .locals 0

    iput-object p1, p0, Lb/aD;->uj:Lb/aB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :goto_0
    sget-object v1, Lb/aB;->tZ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lb/aB;->tZ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    sget-wide v2, Lb/aB;->ue:J

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    # getter for: Lb/aB;->tY:I
    invoke-static {}, Lb/aB;->access$100()I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    :cond_0
    iget-object v0, p0, Lb/aD;->uj:Lb/aB;

    iget-object v0, v0, Lb/aB;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "call updatemDNIe with delay "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lb/p;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/aD;->uj:Lb/aB;

    invoke-virtual {v0}, Lb/aB;->dl()V

    const/4 v0, 0x0

    # setter for: Lb/aB;->tY:I
    invoke-static {v0}, Lb/aB;->access$102(I)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    :try_start_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
