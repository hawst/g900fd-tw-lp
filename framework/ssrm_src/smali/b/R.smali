.class public Lb/R;
.super Lb/a;

# interfaces
.implements Lb/D;
.implements Lb/E;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field oT:Z

.field pG:[Ljava/lang/String;

.field pL:Z

.field final pS:I

.field final pT:I

.field final pU:I

.field pY:Z

.field final pZ:I

.field qA:Z

.field qB:Z

.field qD:Z

.field qE:[Ljava/lang/String;

.field final qa:I

.field final qb:I

.field final qc:I

.field final qd:I

.field final qe:I

.field final qf:I

.field final qg:I

.field final qh:I

.field final qi:I

.field final qj:I

.field final qk:I

.field final ql:I

.field final qm:I

.field final qn:I

.field final qo:I

.field final qp:I

.field final qq:I

.field final qr:I

.field final qs:I

.field final qt:I

.field final qu:I

.field qv:Z

.field final qw:I

.field qx:Z

.field qy:Z

.field qz:Z


# direct methods
.method public constructor <init>()V
    .locals 8

    const/4 v7, 0x1

    const/16 v6, 0x44

    const/16 v5, 0x3d

    const/16 v4, 0x40

    const/4 v3, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/R;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/R;->TAG:Ljava/lang/String;

    iput-boolean v3, p0, Lb/R;->qv:Z

    iput v7, p0, Lb/R;->qw:I

    iput-boolean v3, p0, Lb/R;->pL:Z

    iput-boolean v3, p0, Lb/R;->qx:Z

    iput-boolean v3, p0, Lb/R;->qD:Z

    iput-boolean v3, p0, Lb/R;->qy:Z

    iput-boolean v3, p0, Lb/R;->qz:Z

    iput-boolean v3, p0, Lb/R;->oT:Z

    iput-boolean v3, p0, Lb/R;->qA:Z

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x9

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xc

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x13

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0xb

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/16 v2, 0xa

    new-array v2, v2, [I

    fill-array-data v2, :array_4

    invoke-static {v2}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/R;->pG:[Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/16 v1, 0x13

    new-array v1, v1, [I

    fill-array-data v1, :array_5

    invoke-static {v1}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xf

    new-array v1, v1, [I

    fill-array-data v1, :array_6

    invoke-static {v1}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x11

    new-array v2, v2, [I

    fill-array-data v2, :array_7

    invoke-static {v2}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x15

    new-array v2, v2, [I

    fill-array-data v2, :array_8

    invoke-static {v2}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/R;->qE:[Ljava/lang/String;

    iput-boolean v3, p0, Lb/R;->pY:Z

    iput-boolean v3, p0, Lb/R;->qB:Z

    iget-object v0, p0, Lb/R;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/a;->setTag(Ljava/lang/String;)V

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->pZ:I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qa:I

    new-array v0, v4, [I

    fill-array-data v0, :array_b

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qb:I

    new-array v0, v5, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "900000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qc:I

    new-array v0, v5, [I

    fill-array-data v0, :array_d

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "75"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qd:I

    new-array v0, v4, [I

    fill-array-data v0, :array_e

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "85"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qe:I

    new-array v0, v6, [I

    fill-array-data v0, :array_f

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "19000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qf:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qg:I

    new-array v0, v6, [I

    fill-array-data v0, :array_11

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qh:I

    const/16 v0, 0x3b

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qi:I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "20000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qj:I

    new-array v0, v4, [I

    fill-array-data v0, :array_14

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qk:I

    new-array v0, v5, [I

    fill-array-data v0, :array_15

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "1000000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->ql:I

    new-array v0, v5, [I

    fill-array-data v0, :array_16

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "80 1000000:81 1400000:87 1700000:90"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qm:I

    new-array v0, v4, [I

    fill-array-data v0, :array_17

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "89"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qn:I

    new-array v0, v6, [I

    fill-array-data v0, :array_18

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "59000 1200000:119000 1700000:19000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qo:I

    const/16 v0, 0x36

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qp:I

    new-array v0, v6, [I

    fill-array-data v0, :array_1a

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "40000"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qq:I

    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "32"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qr:I

    const/16 v0, 0x3e

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->pS:I

    const/16 v0, 0x41

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "360"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->pT:I

    const/16 v0, 0x42

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "95"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->pU:I

    new-array v0, v4, [I

    fill-array-data v0, :array_1f

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "240"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qs:I

    const/16 v0, 0x41

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "60"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qt:I

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_21

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "700"

    invoke-virtual {p0, v0, v1}, Lb/a;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/R;->qu:I

    const-string v0, "dm"

    invoke-static {v0}, Lcom/android/server/ssrm/F;->h(Ljava/lang/String;)V

    new-instance v0, Lb/S;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/S;-><init>(Lb/R;Landroid/os/Looper;)V

    iput-object v0, p0, Lb/R;->mHandler:Landroid/os/Handler;

    return-void

    :array_0
    .array-data 4
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1f
        0x1b
        0x54
        0x1d
        0x1b
        0x17
        0x1f
        0x9
        0x54
        0x8
        0x49
        0x25
        0x8
        0x15
        0xd
    .end array-data

    :array_3
    .array-data 4
        0x3f
        0xa
        0x13
        0x19
        0x39
        0x13
        0xe
        0x1b
        0x1e
        0x1f
        0x16
    .end array-data

    :array_4
    .array-data 4
        0x1b
        0x14
        0x1d
        0x8
        0x3
        0x18
        0x13
        0x8
        0x1e
        0x9
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1c
        0x1b
        0x19
        0x1f
        0x18
        0x15
        0x15
        0x11
        0x54
        0x11
        0x1b
        0xe
        0x1b
        0x14
        0x1b
    .end array-data

    :array_6
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x17
        0x17
        0x9
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1f
        0x17
        0x1b
        0x13
        0x16
    .end array-data

    :array_8
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x17
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x9
        0x16
        0x1b
        0x19
        0x11
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_f
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_10
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
    .end array-data

    :array_11
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4a
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
        0xa
        0xf
        0x16
        0x9
        0x1f
        0x25
        0x1e
        0xf
        0x8
        0x1b
        0xe
        0x13
        0x15
        0x14
    .end array-data

    :array_12
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x8
        0x1b
        0xe
        0x1f
    .end array-data

    :array_13
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x13
        0x17
        0x1f
        0x8
        0x25
        0x9
        0x16
        0x1b
        0x19
        0x11
    .end array-data

    :array_14
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0x13
        0x14
        0x25
        0x9
        0x1b
        0x17
        0xa
        0x16
        0x1f
        0x25
        0xe
        0x13
        0x17
        0x1f
    .end array-data

    :array_15
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1c
        0x8
        0x1f
        0xb
    .end array-data

    :array_16
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0xe
        0x1b
        0x8
        0x1d
        0x1f
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
        0x9
    .end array-data

    :array_17
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1d
        0x15
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_18
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1b
        0x18
        0x15
        0xc
        0x1f
        0x25
        0x12
        0x13
        0x9
        0xa
        0x1f
        0x1f
        0x1e
        0x25
        0x1e
        0x1f
        0x16
        0x1b
        0x3
    .end array-data

    :array_19
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
    .end array-data

    :array_1a
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x18
        0x15
        0x15
        0x9
        0xe
        0xa
        0xf
        0x16
        0x9
        0x1f
        0x25
        0x1e
        0xf
        0x8
        0x1b
        0xe
        0x13
        0x15
        0x14
    .end array-data

    :array_1b
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x11
        0x1f
        0x8
        0x14
        0x1f
        0x16
        0x55
        0x12
        0x17
        0xa
        0x55
        0x16
        0x15
        0x1b
        0x1e
        0x25
        0x1b
        0xc
        0x1d
        0x25
        0xa
        0x1f
        0x8
        0x13
        0x15
        0x1e
        0x25
        0x17
        0x9
    .end array-data

    :array_1c
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x1f
        0x14
        0x1c
        0x15
        0x8
        0x19
        0x1f
        0x1e
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_1d
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_1e
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x14
        0xe
        0x1f
        0x8
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_1f
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x17
        0xf
        0x16
        0xe
        0x13
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_20
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x4e
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x13
        0x14
        0xe
        0x1f
        0x8
        0x1b
        0x19
        0xe
        0x13
        0xc
        0x1f
        0x55
        0x9
        0x13
        0x14
        0x1d
        0x16
        0x1f
        0x25
        0x1f
        0x2
        0x13
        0xe
        0x25
        0x16
        0x15
        0x1b
        0x1e
    .end array-data

    :array_21
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x11
        0x1f
        0x8
        0x14
        0x1f
        0x16
        0x55
        0x12
        0x17
        0xa
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
    .end array-data
.end method


# virtual methods
.method protected E(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/R;->pY:Z

    invoke-virtual {p0}, Lb/R;->update()V

    return-void
.end method

.method public J(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/R;->oT:Z

    invoke-virtual {p0}, Lb/R;->update()V

    return-void
.end method

.method public M(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/R;->qB:Z

    invoke-virtual {p0}, Lb/R;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/R;->qy:Z

    invoke-virtual {p0}, Lb/R;->update()V

    :cond_0
    const-string v0, "Camera_recordingDual"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean p2, p0, Lb/R;->qz:Z

    invoke-virtual {p0}, Lb/R;->update()V

    :cond_1
    return-void
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/R;->pY:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lb/R;->pS:I

    const-string v1, "3"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lb/R;->qA:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lb/R;->qB:Z

    if-nez v0, :cond_1

    iget v0, p0, Lb/R;->qu:I

    const-string v1, "1024"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/R;->qv:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/R;->pT:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->pU:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qs:I

    const-string v1, "800"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qt:I

    const-string v1, "200"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    iget-boolean v0, p0, Lb/R;->pL:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lb/R;->qd:I

    const-string v1, "75 900000:85 1100000:99"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qm:I

    const-string v1, "98"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qo:I

    const-string v1, "99000 1200000:399000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qr:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-boolean v0, p0, Lb/R;->qx:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lb/R;->qf:I

    const-string v1, "39000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->ql:I

    const-string v1, "800000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qm:I

    const-string v1, "98"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qo:I

    const-string v1, "139000 1200000:499000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qr:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lb/R;->qy:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lb/R;->qz:Z

    if-eqz v0, :cond_7

    :cond_6
    iget v0, p0, Lb/R;->qm:I

    const-string v1, "98"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qo:I

    const-string v1, "139000 1200000:499000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qr:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget-boolean v0, p0, Lb/R;->qA:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lb/R;->qD:Z

    if-eqz v0, :cond_0

    :cond_8
    iget v0, p0, Lb/R;->qd:I

    const-string v1, "80"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qc:I

    const-string v1, "800000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qf:I

    const-string v1, "39000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qm:I

    const-string v1, "98"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qo:I

    const-string v1, "139000 1200000:499000"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/R;->qr:I

    const-string v1, "64"

    invoke-virtual {p0, v0, v1}, Lb/R;->b(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method public cb()V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/R;->bH()Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lb/R;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/server/ssrm/aU;->P(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/R;->pL:Z

    iput-boolean v1, p0, Lb/R;->qx:Z

    iget-object v3, p0, Lb/R;->pG:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_0
    iput-boolean v7, p0, Lb/R;->qx:Z

    :cond_1
    iput-boolean v1, p0, Lb/R;->qD:Z

    iget-object v3, p0, Lb/R;->qE:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_2
    iput-boolean v7, p0, Lb/R;->qD:Z

    :cond_3
    invoke-static {v2}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lb/R;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lb/R;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v7, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lb/R;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_2
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/R;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lb/R;->bH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/R;->qA:Z

    invoke-virtual {p0}, Lb/R;->update()V

    return-void

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    iput-boolean v1, p0, Lb/R;->qv:Z

    goto :goto_2

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0xe
        0x1b
        0x16
        0x11
    .end array-data
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
