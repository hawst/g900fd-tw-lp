.class public final Lb/ac;
.super Lb/a;

# interfaces
.implements Lb/D;
.implements Lb/E;
.implements Lb/r;
.implements Lb/t;


# instance fields
.field final TAG:Ljava/lang/String;

.field mHandler:Landroid/os/Handler;

.field oT:Z

.field pG:[Ljava/lang/String;

.field final pH:I

.field final pI:I

.field final pJ:I

.field final pP:I

.field final qM:I

.field qx:Z

.field rI:Z

.field rJ:Z

.field final rK:Ljava/lang/String;

.field rL:Landroid/os/DVFSHelper;

.field rM:Z

.field final rN:I

.field final rO:I

.field ru:Z

.field rv:Z

.field rz:Z


# direct methods
.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x14

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Lb/a;-><init>()V

    const-class v0, Lb/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    iput-boolean v3, p0, Lb/ac;->oT:Z

    iput-boolean v3, p0, Lb/ac;->qx:Z

    iput-boolean v3, p0, Lb/ac;->rI:Z

    iput-boolean v3, p0, Lb/ac;->ru:Z

    iput-boolean v3, p0, Lb/ac;->rz:Z

    iput-boolean v3, p0, Lb/ac;->rv:Z

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    new-array v1, v5, [I

    fill-array-data v1, :array_0

    invoke-static {v1}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xd

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    invoke-static {v1}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const/16 v2, 0x1c

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    invoke-static {v2}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x12

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lb/ac;->pG:[Ljava/lang/String;

    iput-boolean v3, p0, Lb/ac;->rJ:Z

    new-array v0, v5, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ac;->rK:Ljava/lang/String;

    iput-boolean v3, p0, Lb/ac;->rM:Z

    iput v4, p0, Lb/ac;->rN:I

    const/16 v0, 0x1770

    iput v0, p0, Lb/ac;->rO:I

    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/ac;->setTag(Ljava/lang/String;)V

    const-string v0, "/sys/devices/system/cpu/cpufreq/interactive/above_hispeed_delay"

    const-string v1, "25000 1094400:50000"

    invoke-virtual {p0, v0, v1}, Lb/ac;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ac;->pI:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/interactive/go_hispeed_load"

    const-string v1, "90"

    invoke-virtual {p0, v0, v1}, Lb/ac;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ac;->pP:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/interactive/hispeed_freq"

    const-string v1, "998400"

    invoke-virtual {p0, v0, v1}, Lb/ac;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ac;->pJ:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/interactive/target_loads"

    const-string v1, "85 998400:90 1094400:80"

    invoke-virtual {p0, v0, v1}, Lb/ac;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ac;->pH:I

    const-string v0, "/sys/devices/system/cpu/cpufreq/interactive/io_is_busy"

    const-string v1, "0"

    invoke-virtual {p0, v0, v1}, Lb/ac;->k(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lb/ac;->qM:I

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/ac;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/ac;->addPackage(Ljava/lang/String;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0xe
        0x1f
        0x14
        0x19
        0x1f
        0x14
        0xe
        0x54
        0x17
        0x15
        0x18
        0x13
        0x16
        0x1f
        0xb
        0xb
    .end array-data

    :array_1
    .array-data 4
        0xe
        0xf
        0x14
        0x1f
        0x13
        0x14
        0x54
        0xa
        0x16
        0x1b
        0x3
        0x1f
        0x8
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x9
        0x54
        0x17
        0x1b
        0xa
        0x9
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x18
        0x1b
        0x13
        0x1e
        0xf
        0x54
        0x38
        0x1b
        0x13
        0x1e
        0xf
        0x37
        0x1b
        0xa
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data
.end method


# virtual methods
.method public J(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/ac;->oT:Z

    invoke-virtual {p0}, Lb/ac;->update()V

    return-void
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    const-string v0, "Camera_recording"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p0, Lb/ac;->rI:Z

    invoke-virtual {p0}, Lb/ac;->update()V

    :cond_0
    return-void
.end method

.method cL()V
    .locals 6

    const-wide/16 v4, 0x1770

    const/4 v1, 0x1

    iget-boolean v2, p0, Lb/ac;->rM:Z

    if-nez v2, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-boolean v3, p0, Lb/ac;->rz:Z

    if-eqz v3, :cond_1

    iget-boolean v3, p0, Lb/ac;->rJ:Z

    if-nez v3, :cond_1

    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v2, "Preload -> Preload"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ac;->rL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iput-boolean v1, p0, Lb/ac;->rM:Z

    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v2, "Power collapse is disabled."

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lb/ac;->rz:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lb/ac;->rJ:Z

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v2, "Preload -> 3rd party"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ac;->rL:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    iput-boolean v1, p0, Lb/ac;->rM:Z

    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v1, "Power collapse is disabled."

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    iget-boolean v0, p0, Lb/ac;->rz:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v2, "3rd party -> Preload"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lb/ac;->TAG:Ljava/lang/String;

    const-string v2, "3rd party -> 3rd party"

    invoke-static {v0, v2}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method ca()V
    .locals 2

    iget-boolean v0, p0, Lb/ac;->rI:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lb/ac;->pP:I

    const-string v1, "99"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/ac;->pJ:I

    const-string v1, "800000"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    iget v0, p0, Lb/ac;->pH:I

    const-string v1, "85 800000:90 1094400:95"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lb/ac;->rz:Z

    if-nez v0, :cond_1

    iget v0, p0, Lb/ac;->qM:I

    const-string v1, "1"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    :cond_1
    iget-boolean v0, p0, Lb/ac;->rJ:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lb/ac;->pH:I

    const-string v1, "90 800000:95 1094400:99"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    :cond_2
    iget-boolean v0, p0, Lb/ac;->rv:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lb/ac;->oT:Z

    if-nez v0, :cond_3

    iget v0, p0, Lb/ac;->pI:I

    const-string v1, "25000 1094400:500000"

    invoke-virtual {p0, v0, v1}, Lb/ac;->b(ILjava/lang/String;)V

    :cond_3
    return-void
.end method

.method public cb()V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lb/ac;->bH()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean v1, p0, Lb/ac;->qx:Z

    iget-object v3, p0, Lb/ac;->pG:[Ljava/lang/String;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_1
    iput-boolean v7, p0, Lb/ac;->qx:Z

    :cond_2
    sget-object v0, Lb/ac;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/server/ssrm/aU;->c(Landroid/content/Context;)Lcom/android/server/ssrm/aU;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/android/server/ssrm/aU;->R(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lb/ac;->ru:Z

    invoke-static {v2}, Lcom/android/server/ssrm/aU;->T(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lb/ac;->ru:Z

    if-nez v0, :cond_3

    invoke-static {v2}, Lcom/android/server/ssrm/W;->E(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lb/ac;->qx:Z

    if-eqz v0, :cond_5

    :cond_3
    iput-boolean v7, p0, Lb/ac;->rz:Z

    :goto_2
    iget-object v0, p0, Lb/ac;->rK:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lb/ac;->rJ:Z

    invoke-virtual {p0}, Lb/ac;->update()V

    invoke-virtual {p0}, Lb/ac;->cL()V

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    iput-boolean v1, p0, Lb/ac;->rz:Z

    goto :goto_2
.end method

.method public ci()V
    .locals 6

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/ac;->mContext:Landroid/content/Context;

    const-string v2, "SD8916_PCD"

    const/16 v3, 0x16

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/ac;->rL:Landroid/os/DVFSHelper;

    new-instance v0, Lb/ad;

    invoke-static {}, Lcom/android/server/ssrm/ag;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/ad;-><init>(Lb/ac;Landroid/os/Looper;)V

    iput-object v0, p0, Lb/ac;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method public z(Z)V
    .locals 0

    iput-boolean p1, p0, Lb/ac;->rv:Z

    invoke-virtual {p0}, Lb/ac;->update()V

    return-void
.end method
