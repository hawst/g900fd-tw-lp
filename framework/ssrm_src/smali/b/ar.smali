.class public Lb/ar;
.super Lb/p;

# interfaces
.implements Lb/t;


# static fields
.field public static final tf:Ljava/lang/String;


# instance fields
.field final TAG:Ljava/lang/String;

.field fI:Landroid/os/DVFSHelper;

.field final te:Ljava/lang/String;

.field tg:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ar;->a([I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/ar;->tf:Ljava/lang/String;

    return-void

    :array_0
    .array-data 4
        0x34
        0x1b
        0xc
        0x13
        0x1d
        0x1b
        0xe
        0x13
        0x15
        0x14
        0x25
        0x9
        0x12
        0x15
        0xd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ar;->TAG:Ljava/lang/String;

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ar;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ar;->te:Ljava/lang/String;

    iput-object v1, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    iput-object v1, p0, Lb/ar;->tg:[I

    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/ar;->a([I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [I

    fill-array-data v1, :array_2

    invoke-static {v1}, Lb/ar;->a([I)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x12

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    invoke-static {v2}, Lb/ar;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0}, Lb/ar;->addPackage(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lb/ar;->addPackage(Ljava/lang/String;)V

    invoke-virtual {p0, v2}, Lb/ar;->addPackage(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lb/ar;->te:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lb/ar;->addPackage(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1d
        0x15
        0x15
        0x1d
        0x16
        0x1f
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x9
        0x54
        0x17
        0x1b
        0xa
        0x9
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x11
        0xe
        0x54
        0x9
        0x11
        0x1b
        0x1c
        0x54
        0x16
        0x4a
        0x4a
        0x4b
        0x17
        0xe
        0x17
        0x4a
        0x43
        0x4b
    .end array-data

    :array_2
    .array-data 4
        0x11
        0xe
        0x54
        0x14
        0x1b
        0xc
        0x13
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x17
        0x14
        0x9
        0x15
        0x1c
        0xe
        0x54
        0x16
        0x1d
        0xf
        0x14
        0x1b
        0xc
        0x13
    .end array-data
.end method


# virtual methods
.method public cb()V
    .locals 0

    return-void
.end method

.method public z(Z)V
    .locals 7

    const v6, 0x13d620

    iget-object v0, p0, Lb/ar;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onFgAppInPackageList:: isForeground = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/ar;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/ar;->mContext:Landroid/content/Context;

    const-string v2, "NAVI_ARM_CONTROL"

    const/16 v3, 0xd

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->getSupportedCPUFrequency()[I

    move-result-object v0

    iput-object v0, p0, Lb/ar;->tg:[I

    iget-object v0, p0, Lb/ar;->tg:[I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->cI:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    const v3, 0x927c0

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_1
    :goto_1
    if-eqz p1, :cond_4

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :goto_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, Lb/ar;->iu:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lb/ar;->ix:Ljava/lang/String;

    sget-object v2, Lb/ar;->tf:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Lb/ar;->iy:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "PackageName"

    const-string v2, "com.android.server.ssrm"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lb/ar;->te:Ljava/lang/String;

    invoke-virtual {p0}, Lb/ar;->bH()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "PID"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_3
    sget-object v1, Lb/ar;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_0

    :cond_2
    sget-boolean v0, Lcom/android/server/ssrm/N;->cJ:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v2, v6}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto :goto_1

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->cN:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v2, v6}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lb/ar;->fI:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_2

    :cond_5
    const-string v1, "PID"

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_3
.end method
