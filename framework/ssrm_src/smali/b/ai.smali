.class Lb/ai;
.super Landroid/os/Handler;


# instance fields
.field final synthetic sA:Lb/ah;


# direct methods
.method constructor <init>(Lb/ah;)V
    .locals 0

    iput-object p1, p0, Lb/ai;->sA:Lb/ah;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lb/ai;->sA:Lb/ah;

    iget-object v0, v0, Lb/ah;->TAG:Ljava/lang/String;

    const-string v1, "MSG_KNOWN_GAME_FOREGROUND"

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/ai;->sA:Lb/ah;

    invoke-virtual {v0}, Lb/ah;->bH()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/android/server/ssrm/W;->C(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lb/ai;->sA:Lb/ah;

    iget-boolean v1, v1, Lb/ah;->sv:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/ai;->sA:Lb/ah;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lb/ah;->sv:Z

    iget-object v0, p0, Lb/ai;->sA:Lb/ah;

    invoke-virtual {v0}, Lb/ah;->cP()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
