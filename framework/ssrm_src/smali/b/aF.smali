.class Lb/aF;
.super Landroid/database/ContentObserver;


# instance fields
.field final synthetic uj:Lb/aB;


# direct methods
.method constructor <init>(Lb/aB;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lb/aF;->uj:Lb/aB;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method dn()V
    .locals 1

    sget-object v0, Lb/p;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onChange(Z)V
    .locals 1

    iget-object v0, p0, Lb/aF;->uj:Lb/aB;

    invoke-static {v0}, Lb/aB;->a(Lb/aB;)V

    iget-object v0, p0, Lb/aF;->uj:Lb/aB;

    invoke-virtual {v0}, Lb/aB;->dl()V

    return-void
.end method

.method register()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x2

    sget-object v0, Lb/p;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "reading_mode_app_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    const-string v1, "e_reading_display_mode"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3, p0, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    return-void
.end method
