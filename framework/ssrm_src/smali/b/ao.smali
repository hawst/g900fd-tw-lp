.class public Lb/ao;
.super Lb/p;

# interfaces
.implements Lb/r;
.implements Lb/t;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private sL:Z

.field private final sM:Ljava/util/HashMap;

.field private sN:Z

.field final sO:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/ao;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v1, p0, Lb/ao;->sL:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/ao;->sM:Ljava/util/HashMap;

    iput-boolean v1, p0, Lb/ao;->sN:Z

    const/16 v0, 0x35

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/ao;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/ao;->sO:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x38
        0x2f
        0x34
        0x3e
        0x36
        0x33
        0x34
        0x3d
        0x25
        0x2a
        0x35
        0x36
        0x33
        0x39
        0x23
        0x25
        0x2f
        0x2a
        0x3e
        0x3b
        0x2e
        0x3f
        0x3e
    .end array-data
.end method


# virtual methods
.method public a(ZLjava/lang/String;)V
    .locals 7

    iput-boolean p1, p0, Lb/ao;->sL:Z

    iget-object v0, p0, Lb/ao;->sM:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    if-eqz v3, :cond_0

    sget-object v4, Lb/ao;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "pkg : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lb/ao;->sM:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lb/ao;->cb()V

    return-void
.end method

.method public cb()V
    .locals 3

    sget-boolean v0, Lcom/android/server/ssrm/N;->et:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lb/ao;->sL:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lb/ao;->sM:Ljava/util/HashMap;

    invoke-virtual {p0}, Lb/ao;->bH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lb/ao;->sN:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lb/ao;->sN:Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lb/ao;->sO:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "GMS_LOCATION_BUNDLING"

    iget-boolean v2, p0, Lb/ao;->sN:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lb/ao;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.DEVICE_POWER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    sget-object v0, Lb/ao;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUNDLING_POLICY_UPDATED: GMS_LOCATION_BUNDLING = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/ao;->sN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ci()V
    .locals 3

    sget-boolean v0, Lcom/android/server/ssrm/N;->et:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/ao;->sN:Z

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lb/ao;->sO:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "GMS_LOCATION_BUNDLING"

    iget-boolean v2, p0, Lb/ao;->sN:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    sget-object v1, Lb/ao;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.DEVICE_POWER"

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    sget-object v0, Lb/ao;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BUNDLING_POLICY_UPDATED: GMS_LOCATION_BUNDLING = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/ao;->sN:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/server/ssrm/aT;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
