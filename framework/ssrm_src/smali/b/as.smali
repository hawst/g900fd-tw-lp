.class public Lb/as;
.super Lb/p;

# interfaces
.implements Lb/r;
.implements Lb/t;


# instance fields
.field th:Landroid/os/DVFSHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lb/p;-><init>()V

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/as;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/as;->addPackage(Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x1f
        0xc
        0x1f
        0x8
        0x1d
        0x16
        0x1b
        0x1e
        0x1f
        0x9
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
    .end array-data
.end method


# virtual methods
.method public cb()V
    .locals 0

    return-void
.end method

.method public ci()V
    .locals 6

    new-instance v0, Landroid/os/DVFSHelper;

    sget-object v1, Lb/as;->mContext:Landroid/content/Context;

    const-string v2, "PSM_FPS_MAX_59"

    const/16 v3, 0x15

    const-wide/16 v4, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lb/as;->th:Landroid/os/DVFSHelper;

    iget-object v0, p0, Lb/as;->th:Landroid/os/DVFSHelper;

    const-string v1, "FPS"

    const-wide/16 v2, 0x62

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    return-void
.end method

.method public z(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lb/as;->th:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lb/as;->th:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_0
.end method
