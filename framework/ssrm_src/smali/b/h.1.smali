.class public Lb/h;
.super Lb/p;

# interfaces
.implements Lb/E;
.implements Lb/t;


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private nF:Z

.field private nG:Z

.field private final nH:I

.field private final nI:I

.field private nJ:I

.field final nK:Ljava/lang/String;

.field final nL:Ljava/lang/String;

.field nM:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lb/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lb/h;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/16 v1, 0x5a

    const/16 v2, 0x1c

    const/4 v0, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    iput-boolean v0, p0, Lb/h;->nF:Z

    iput-boolean v0, p0, Lb/h;->nG:Z

    iput v1, p0, Lb/h;->nH:I

    const/16 v0, 0x63

    iput v0, p0, Lb/h;->nI:I

    iput v1, p0, Lb/h;->nJ:I

    const/16 v0, 0x37

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/h;->nK:Ljava/lang/String;

    const/16 v0, 0x32

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/h;->nL:Ljava/lang/String;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lb/h;->nM:Ljava/util/HashMap;

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    new-array v0, v2, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lb/p;->addPackage(Ljava/lang/String;)V

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x19

    new-array v1, v1, [I

    fill-array-data v1, :array_7

    invoke-static {v1}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lb/h;->m(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    invoke-static {v1}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lb/h;->m(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1f

    new-array v1, v1, [I

    fill-array-data v1, :array_b

    invoke-static {v1}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lb/h;->m(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    invoke-static {v0}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x1a

    new-array v1, v1, [I

    fill-array-data v1, :array_d

    invoke-static {v1}, Lb/h;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lb/h;->m(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0xf
        0xa
        0x25
        0xe
        0x12
        0x8
        0x1f
        0x9
        0x12
        0x15
        0x16
        0x1e
        0x25
        0x12
    .end array-data

    :array_1
    .array-data 4
        0x55
        0x9
        0x3
        0x9
        0x55
        0x1e
        0x1f
        0xc
        0x13
        0x19
        0x1f
        0x9
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x19
        0xa
        0xf
        0x55
        0x19
        0xa
        0xf
        0x1c
        0x8
        0x1f
        0xb
        0x55
        0x15
        0x14
        0x1e
        0x1f
        0x17
        0x1b
        0x14
        0x1e
        0x55
        0x1d
        0x1b
        0x17
        0x1f
        0x25
        0x17
        0x15
        0x1e
        0x1f
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x16
        0xa
        0x12
        0x1b
        0x19
        0x16
        0x15
        0xf
        0x1e
        0x54
        0x19
        0x1b
        0x9
        0xe
        0x16
        0x1f
        0x17
        0x1b
        0x9
        0xe
        0x1f
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x9
        0x18
        0x8
        0x15
        0xd
        0x9
        0x1f
        0x8
    .end array-data

    :array_4
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x18
        0x8
        0x15
        0xd
        0x9
        0x1f
        0x8
    .end array-data

    :array_5
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x19
        0x12
        0x8
        0x15
        0x17
        0x1f
    .end array-data

    :array_6
    .array-data 4
        0x3d
        0x1b
        0x16
        0x16
        0x1f
        0x8
        0x3
    .end array-data

    :array_7
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1d
        0x1b
        0x16
        0x16
        0x1f
        0x8
        0x3
        0x49
        0x1e
    .end array-data

    :array_8
    .array-data 4
        0x2c
        0x13
        0x1e
        0x1f
        0x15
        0x2a
        0x16
        0x1b
        0x3
        0x1f
        0x8
    .end array-data

    :array_9
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1b
        0x17
        0x9
        0xf
        0x14
        0x1d
        0x54
        0x1f
        0xc
        0x1f
        0x8
        0x1d
        0x16
        0x1b
        0x1e
        0x1f
        0x9
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
    .end array-data

    :array_a
    .array-data 4
        0x2c
        0x13
        0x1e
        0x1f
        0x15
        0x52
        0x12
        0x13
        0x1e
        0x1e
        0x1f
        0x14
        0x53
    .end array-data

    :array_b
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0xc
        0x13
        0x1e
        0x1f
        0x15
        0xa
        0x16
        0x1b
        0x3
        0x1f
        0x8
    .end array-data

    :array_c
    .array-data 4
        0x39
        0x1b
        0x17
        0x1f
        0x8
        0x1b
    .end array-data

    :array_d
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x9
        0x1f
        0x19
        0x54
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x1b
        0xa
        0xa
        0x54
        0x19
        0x1b
        0x17
        0x1f
        0x8
        0x1b
    .end array-data
.end method


# virtual methods
.method protected Y(I)V
    .locals 3

    iget v0, p0, Lb/h;->nJ:I

    if-eq v0, p1, :cond_0

    sget-object v0, Lb/h;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateThresholdSysFS :: new value = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/h;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/h;->nK:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/h;->n(Ljava/lang/String;Ljava/lang/String;)V

    iput p1, p0, Lb/h;->nJ:I

    sget-boolean v0, Lcom/android/server/ssrm/N;->cC:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lb/h;->nL:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lb/h;->nJ:I

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lb/h;->nL:Ljava/lang/String;

    const-string v1, "0"

    invoke-static {v0, v1}, Lb/h;->n(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lb/h;->nL:Ljava/lang/String;

    const-string v1, "1"

    invoke-static {v0, v1}, Lb/h;->n(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    const-string v0, "FullScreen"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lb/h;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStatusNotiReceived:: FullScreen = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/h;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean p2, p0, Lb/h;->nG:Z

    invoke-virtual {p0}, Lb/h;->cg()V

    :cond_0
    return-void
.end method

.method ag(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lb/h;->nM:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cb()V
    .locals 1

    invoke-virtual {p0}, Lb/h;->bH()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lb/h;->isPackageExist(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/h;->nF:Z

    :goto_1
    invoke-virtual {p0}, Lb/h;->cg()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/h;->nF:Z

    goto :goto_1
.end method

.method protected cg()V
    .locals 1

    invoke-virtual {p0}, Lb/h;->ch()I

    move-result v0

    invoke-virtual {p0, v0}, Lb/h;->Y(I)V

    return-void
.end method

.method protected ch()I
    .locals 4

    const/16 v0, 0x63

    const/16 v1, 0x5a

    iget-boolean v2, p0, Lb/h;->nF:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lb/h;->bH()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lb/h;->nG:Z

    if-eqz v3, :cond_2

    invoke-virtual {p0, v2}, Lb/h;->ag(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method m(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lb/h;->nM:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public z(Z)V
    .locals 0

    return-void
.end method
