.class public Lb/aB;
.super Lb/p;

# interfaces
.implements Lb/A;
.implements Lb/r;
.implements Lb/t;
.implements Lb/w;
.implements Lb/x;


# static fields
.field private static tY:I = 0x0

.field static tZ:Ljava/lang/Object; = null

.field static ua:Ljava/lang/Object; = null

.field static final ud:I = 0x1

.field public static ue:J

.field public static uf:J

.field public static ug:I

.field public static uh:J

.field public static ui:J


# instance fields
.field final TAG:Ljava/lang/String;

.field private mHandler:Landroid/os/Handler;

.field mPowerManager:Landroid/os/PowerManager;

.field final tS:Ljava/lang/String;

.field tT:Z

.field tU:Z

.field tV:Z

.field tW:I

.field tX:Lb/aF;

.field ub:Ljava/lang/Thread;

.field uc:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    sput v1, Lb/aB;->tY:I

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lb/aB;->tZ:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lb/aB;->ua:Ljava/lang/Object;

    sput-wide v2, Lb/aB;->ue:J

    sput-wide v2, Lb/aB;->uf:J

    sput v1, Lb/aB;->ug:I

    sput-wide v2, Lb/aB;->uh:J

    sput-wide v2, Lb/aB;->ui:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lb/p;-><init>()V

    const-class v0, Lb/aB;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lb/aB;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lb/aB;->tS:Ljava/lang/String;

    iput-boolean v1, p0, Lb/aB;->tT:Z

    iput-boolean v1, p0, Lb/aB;->tU:Z

    iput-boolean v1, p0, Lb/aB;->tV:Z

    iput v1, p0, Lb/aB;->tW:I

    const/4 v0, 0x0

    iput-object v0, p0, Lb/aB;->tX:Lb/aF;

    iget-object v0, p0, Lb/aB;->tS:Ljava/lang/String;

    const-string v1, "-1"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :array_0
    .array-data 4
        0x9
        0x3
        0x9
        0x54
        0x9
        0x9
        0x8
        0x17
        0x54
        0x17
        0x1e
        0x14
        0x13
        0x1f
    .end array-data
.end method

.method static synthetic a(Lb/aB;)V
    .locals 0

    invoke-direct {p0}, Lb/aB;->dk()V

    return-void
.end method

.method static synthetic access$100()I
    .locals 1

    sget v0, Lb/aB;->tY:I

    return v0
.end method

.method static synthetic access$102(I)I
    .locals 0

    sput p0, Lb/aB;->tY:I

    return p0
.end method

.method private dk()V
    .locals 1

    invoke-virtual {p0}, Lb/aB;->cz()V

    const-string v0, "com.google.android.apps.books"

    invoke-virtual {p0, v0}, Lb/aB;->addPackage(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public I(Z)V
    .locals 3

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isMultiWindowActivated = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lb/aB;->cx()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lb/aB;->ua:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lb/aB;->ua:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 3

    sget-wide v0, Lb/aB;->uh:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-wide p1, Lb/aB;->uh:J

    iget-object v0, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lb/p;->a(Landroid/content/Context;Landroid/content/Intent;Lb/p;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.USER_SWITCHED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object p1, Lb/aB;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const-string v1, "onReceive:: ACTION_USER_FOREGROUND"

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/aB;->tX:Lb/aF;

    invoke-virtual {v0}, Lb/aF;->dn()V

    iget-object v0, p0, Lb/aB;->tX:Lb/aF;

    invoke-virtual {v0}, Lb/aF;->register()V

    invoke-direct {p0}, Lb/aB;->dk()V

    :cond_0
    return-void
.end method

.method protected ae(I)V
    .locals 3

    iget-object v0, p0, Lb/aB;->mPowerManager:Landroid/os/PowerManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lb/aB;->tW:I

    if-eq p1, v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeReadingMode(I)Z

    move-result v0

    iput-boolean v0, p0, Lb/aB;->tV:Z

    iput p1, p0, Lb/aB;->tW:I

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setmDNIeUIMode => "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lb/aB;->tV:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lb/aB;->tS:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_2

    iget-object v0, p0, Lb/aB;->mPowerManager:Landroid/os/PowerManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->setAutoBrightnessForEbookOnly(Z)V

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const-string v1, "setAutoBrightnessForEbookOnly = true"

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const-string v1, "IllegalAccessError occured!"

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lb/aB;->mPowerManager:Landroid/os/PowerManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/PowerManager;->setAutoBrightnessForEbookOnly(Z)V

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const-string v1, "setAutoBrightnessForEbookOnly = false"

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalAccessError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public b(J)V
    .locals 7

    sget-wide v0, Lb/aB;->ui:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sput-wide p1, Lb/aB;->ui:J

    sget v0, Lb/aB;->ug:I

    const/16 v1, 0x19

    if-gt v0, v1, :cond_0

    sget v0, Lb/aB;->ug:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lb/aB;->ug:I

    sget-wide v0, Lb/aB;->uf:J

    sget-wide v2, Lb/aB;->ui:J

    sget-wide v4, Lb/aB;->uh:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    sput-wide v0, Lb/aB;->uf:J

    sget-wide v0, Lb/aB;->uf:J

    sget v2, Lb/aB;->ug:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    sput-wide v0, Lb/aB;->ue:J

    goto :goto_0
.end method

.method public br()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lb/aB;->tU:Z

    invoke-virtual {p0}, Lb/aB;->dl()V

    return-void
.end method

.method public cE()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lb/aB;->tU:Z

    invoke-virtual {p0}, Lb/aB;->dl()V

    return-void
.end method

.method public cb()V
    .locals 2

    sget-object v1, Lb/aB;->ua:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lb/aB;->ua:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public ci()V
    .locals 2

    sget-object v0, Lb/aB;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    iget-object v0, p0, Lb/aB;->TAG:Ljava/lang/String;

    const-string v1, "onBootComplete : mContext == null"

    invoke-static {v0, v1}, Lb/aB;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lb/aB;->mContext:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lb/aB;->mPowerManager:Landroid/os/PowerManager;

    new-instance v0, Lb/aC;

    sget-object v1, Lcom/android/server/ssrm/ag;->jd:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lb/aC;-><init>(Lb/aB;Landroid/os/Looper;)V

    iput-object v0, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    new-instance v0, Lb/aF;

    iget-object v1, p0, Lb/aB;->mHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lb/aF;-><init>(Lb/aB;Landroid/os/Handler;)V

    iput-object v0, p0, Lb/aB;->tX:Lb/aF;

    iget-object v0, p0, Lb/aB;->tX:Lb/aF;

    invoke-virtual {v0}, Lb/aF;->register()V

    invoke-direct {p0}, Lb/aB;->dk()V

    invoke-virtual {p0}, Lb/aB;->dl()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lb/aD;

    invoke-direct {v1, p0}, Lb/aD;-><init>(Lb/aB;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lb/aB;->ub:Ljava/lang/Thread;

    iget-object v0, p0, Lb/aB;->ub:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lb/aE;

    invoke-direct {v1, p0}, Lb/aE;-><init>(Lb/aB;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lb/aB;->uc:Ljava/lang/Thread;

    iget-object v0, p0, Lb/aB;->uc:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected declared-synchronized dl()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lb/aB;->dm()I

    move-result v0

    invoke-virtual {p0, v0}, Lb/aB;->ae(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected dm()I
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lb/aB;->tT:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lb/aB;->cx()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lb/aB;->tU:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v0, "com.android.email"

    invoke-virtual {p0}, Lb/aB;->bH()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    goto :goto_0

    :cond_2
    const/16 v0, 0x9

    goto :goto_0
.end method

.method public z(Z)V
    .locals 2

    invoke-virtual {p0}, Lb/aB;->bH()Ljava/lang/String;

    move-result-object v0

    iput-boolean p1, p0, Lb/aB;->tT:Z

    const-string v1, "launcher"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v1, Lb/aB;->tZ:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->cO:Z

    if-eqz v0, :cond_0

    const/16 v0, -0x64

    sput v0, Lb/aB;->tY:I

    :cond_0
    sget-object v0, Lb/aB;->tZ:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
