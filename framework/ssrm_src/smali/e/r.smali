.class public Le/r;
.super Ljava/lang/Object;

# interfaces
.implements Le/c;


# static fields
.field protected static final TAG:Ljava/lang/String;

.field private static mIntentReceiver:Landroid/content/BroadcastReceiver;

.field static wC:Ljava/util/Hashtable;


# instance fields
.field wD:Lcom/android/server/ssrm/d;

.field wE:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Le/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Le/r;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Le/r;->wC:Ljava/util/Hashtable;

    new-instance v0, Le/s;

    invoke-direct {v0}, Le/s;-><init>()V

    sput-object v0, Le/r;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 6

    const/4 v4, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Le/r;->wE:Z

    new-instance v3, Landroid/content/IntentFilter;

    const-string v0, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v3, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v0, "package"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    sget-object v1, Le/r;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v0, p1

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    sget-object v0, Le/r;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, p1, v4}, Landroid/content/BroadcastReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_1

    const-string v0, "MUSIC_STUDIO_BOOST"

    const v1, 0xb2200

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/android/server/ssrm/N;->cX:Z

    if-eqz v0, :cond_0

    const-string v0, "MUSIC_STUDIO_BOOST"

    const v1, 0x9eb10

    invoke-static {v0, v1}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;I)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    goto :goto_0
.end method

.method public static al(Ljava/lang/String;)Z
    .locals 2

    sget-object v1, Le/r;->wC:Ljava/util/Hashtable;

    monitor-enter v1

    :try_start_0
    sget-object v0, Le/r;->wC:Ljava/util/Hashtable;

    invoke-virtual {v0, p0}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public J(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    if-eqz v0, :cond_0

    invoke-static {p1}, Le/r;->al(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Le/r;->wE:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/r;->wE:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Le/r;->al(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Le/r;->wE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/r;->wD:Lcom/android/server/ssrm/d;

    invoke-virtual {v0}, Lcom/android/server/ssrm/d;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Le/r;->wE:Z

    goto :goto_0
.end method
