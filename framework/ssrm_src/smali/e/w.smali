.class public Le/w;
.super Ljava/lang/Object;

# interfaces
.implements Le/b;


# static fields
.field static TAG:Ljava/lang/String;


# instance fields
.field final i:Ljava/lang/String;

.field ie:Landroid/app/ActivityManager;

.field final j:Landroid/content/Intent;

.field final m:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field final n:Ljava/lang/String;

.field q:[Ljava/lang/String;

.field r:Z

.field u:Ljava/lang/Runnable;

.field private final v:Ljava/io/File;

.field wM:I

.field wN:I

.field wO:J

.field wP:I

.field wQ:I

.field wR:I

.field wS:I

.field wT:I

.field wU:I

.field final wV:I

.field final wW:I

.field final wX:I

.field wY:Z

.field wZ:Z

.field final xa:Ljava/lang/String;

.field final xb:Ljava/io/File;

.field xc:Ljava/lang/Runnable;

.field xd:[Ljava/lang/String;

.field xe:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Le/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Le/w;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 8

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Le/w;->wM:I

    iput v3, p0, Le/w;->wN:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Le/w;->wO:J

    const v0, 0xea60

    iput v0, p0, Le/w;->wP:I

    const v0, 0xea60

    iput v0, p0, Le/w;->wQ:I

    const/16 v0, 0x262

    iput v0, p0, Le/w;->wR:I

    const/16 v0, 0x2a8

    iput v0, p0, Le/w;->wS:I

    const/16 v0, 0x2bc

    iput v0, p0, Le/w;->wT:I

    const/16 v0, 0x320

    iput v0, p0, Le/w;->wU:I

    iput v3, p0, Le/w;->wV:I

    iput v7, p0, Le/w;->wW:I

    const/16 v0, 0x8

    iput v0, p0, Le/w;->wX:I

    iput-boolean v3, p0, Le/w;->wY:Z

    iput-boolean v3, p0, Le/w;->wZ:Z

    const/16 v0, 0x2a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/w;->i:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Le/w;->i:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Le/w;->j:Landroid/content/Intent;

    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/w;->xa:Ljava/lang/String;

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/w;->n:Ljava/lang/String;

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    invoke-static {v0}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/w;->m:Ljava/lang/String;

    iput-boolean v3, p0, Le/w;->r:Z

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x25

    new-array v1, v1, [I

    fill-array-data v1, :array_4

    invoke-static {v1}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Le/w;->xb:Ljava/io/File;

    new-instance v0, Le/x;

    invoke-direct {v0, p0}, Le/x;-><init>(Le/w;)V

    iput-object v0, p0, Le/w;->xc:Ljava/lang/Runnable;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "com.sec.knox.containeragent"

    aput-object v1, v0, v3

    const-string v1, "com.android.contacts"

    aput-object v1, v0, v4

    const-string v1, "com.android.phone"

    aput-object v1, v0, v5

    const-string v1, "com.android.incallui"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "com.google.android.apps.maps"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.vznavigator"

    aput-object v2, v0, v1

    const-string v1, "com.vznavigator.Generic"

    aput-object v1, v0, v7

    iput-object v0, p0, Le/w;->q:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "com.android.settings"

    aput-object v1, v0, v3

    const-string v1, "com.sec.allsharecastplayer"

    aput-object v1, v0, v4

    const-string v1, "com.sec.android.app.wfdbroker"

    aput-object v1, v0, v5

    iput-object v0, p0, Le/w;->xd:[Ljava/lang/String;

    new-instance v0, Le/z;

    invoke-direct {v0, p0}, Le/z;-><init>(Le/w;)V

    iput-object v0, p0, Le/w;->u:Ljava/lang/Runnable;

    new-instance v0, Ljava/io/File;

    const/16 v1, 0x1a

    new-array v1, v1, [I

    fill-array-data v1, :array_5

    invoke-static {v1}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Le/w;->v:Ljava/io/File;

    new-instance v0, Le/A;

    invoke-direct {v0, p0}, Le/A;-><init>(Le/w;)V

    iput-object v0, p0, Le/w;->xe:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Le/w;->mContext:Landroid/content/Context;

    iput-object p2, p0, Le/w;->mHandler:Landroid/os/Handler;

    iget-object v0, p0, Le/w;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Le/w;->ie:Landroid/app/ActivityManager;

    sget-boolean v0, Lcom/android/server/ssrm/N;->ee:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/w;->xb:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iput-boolean v4, p0, Le/w;->r:Z

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Le/w;->r:Z

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->s(Z)V

    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x258

    iput v0, p0, Le/w;->wR:I

    const/16 v0, 0x29e

    iput v0, p0, Le/w;->wS:I

    :cond_2
    :goto_1
    invoke-static {}, Le/w;->ev()Z

    move-result v0

    iput-boolean v0, p0, Le/w;->wY:Z

    goto :goto_0

    :cond_3
    sget-boolean v0, Lcom/android/server/ssrm/N;->dI:Z

    if-nez v0, :cond_4

    sget-boolean v0, Lcom/android/server/ssrm/N;->dG:Z

    if-eqz v0, :cond_2

    :cond_4
    const/16 v0, 0x276

    iput v0, p0, Le/w;->wR:I

    const/16 v0, 0x2a8

    iput v0, p0, Le/w;->wS:I

    goto :goto_1

    nop

    :array_0
    .array-data 4
        0x1b
        0x14
        0x1e
        0x8
        0x15
        0x13
        0x1e
        0x54
        0x13
        0x14
        0xe
        0x1f
        0x14
        0xe
        0x54
        0x1b
        0x19
        0xe
        0x13
        0x15
        0x14
        0x54
        0x39
        0x32
        0x3f
        0x39
        0x31
        0x25
        0x39
        0x35
        0x35
        0x36
        0x3e
        0x35
        0x2d
        0x34
        0x25
        0x36
        0x3f
        0x2c
        0x3f
        0x36
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x12
        0x1f
        0x19
        0x11
        0x25
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x16
        0x1f
        0xc
        0x1f
        0x16
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x12
        0x1f
        0x19
        0x11
        0x25
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x16
        0x13
        0x9
        0xe
    .end array-data

    :array_3
    .array-data 4
        0x15
        0xc
        0x1f
        0x8
        0x12
        0x1f
        0x1b
        0xe
        0x25
        0x13
        0x1e
    .end array-data

    :array_4
    .array-data 4
        0x55
        0x1e
        0x1b
        0xe
        0x1b
        0x55
        0x9
        0x3
        0x9
        0xe
        0x1f
        0x17
        0x55
        0x9
        0x9
        0x8
        0x17
        0x25
        0xc
        0x48
        0x54
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x25
        0x1e
        0x13
        0x9
        0x1b
        0x18
        0x16
        0x1f
    .end array-data

    :array_5
    .array-data 4
        0x55
        0x19
        0x1b
        0x8
        0x8
        0x13
        0x1f
        0x8
        0x55
        0x19
        0x15
        0x15
        0x16
        0x1e
        0x15
        0xd
        0x14
        0x54
        0x9
        0x12
        0xf
        0xe
        0x1e
        0x15
        0xd
        0x14
    .end array-data
.end method

.method public static a(Ljava/io/File;I)I
    .locals 2

    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, v1, v1}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    move-result v0

    return v0
.end method

.method private static a([I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/android/server/ssrm/aT;->a([I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/app/ActivityManager;)Ljava/util/ArrayList;
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/16 v0, 0x3e8

    invoke-virtual {p0, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->numRunning:I

    if-lez v3, :cond_0

    iget-object v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v3, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method static ev()Z
    .locals 2

    const-string v0, "persist.sys.setupwizard"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "FINISH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(I)I
    .locals 10

    const/4 v1, 0x0

    const-string v0, "checkingSIOP"

    const-string v2, "killActiveApplications start"

    invoke-static {v0, v2}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/w;->ie:Landroid/app/ActivityManager;

    invoke-static {v0}, Le/w;->a(Landroid/app/ActivityManager;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Le/w;->ie:Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v4

    const/16 v0, 0x8

    if-ne p1, v0, :cond_b

    invoke-virtual {p0}, Le/w;->a()Ljava/util/HashMap;

    move-result-object v2

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iget-object v6, p0, Le/w;->q:[Ljava/lang/String;

    array-length v7, v6

    move v0, v1

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lcom/android/server/ssrm/ag;->aZ()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v6, p0, Le/w;->xd:[Ljava/lang/String;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    sget-object v9, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v5, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v2, v1

    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "com.vznavigator"

    invoke-virtual {v0, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v5, Le/w;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " killActiveApplications : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Le/w;->ie:Landroid/app/ActivityManager;

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    goto :goto_4

    :cond_7
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_8
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    const-string v4, "com.sec.android.app.videoplayer"

    iget-object v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    sget-object v4, Le/w;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " killActiveApplications : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Le/w;->ie:Landroid/app/ActivityManager;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_9
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int v2, v0, v1

    :cond_a
    :goto_6
    return v2

    :cond_b
    const/4 v0, 0x6

    if-lt p1, v0, :cond_c

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Le/w;->a([I)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v2, v1

    :goto_7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v5, p0, Le/w;->ie:Landroid/app/ActivityManager;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/app/ActivityManager;->forceStopPackage(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    :goto_8
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_7

    :cond_c
    const/4 v2, -0x1

    goto :goto_6

    :cond_d
    move v0, v2

    goto :goto_8

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x14
        0xe
        0x15
        0x54
        0x9
        0xe
        0x1b
        0x18
        0x13
        0x16
        0x13
        0xe
        0x3
    .end array-data
.end method

.method a()Ljava/util/HashMap;
    .locals 4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Le/w;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    :goto_0
    const-string v1, "26653696"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/w;->xb:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_2

    :try_start_0
    const-string v0, "Cooldown mode : [OFF]"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Le/w;->xb:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/w;->r:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v0, p0, Le/w;->r:Z

    invoke-static {v0}, Lcom/android/server/ssrm/ag;->s(Z)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_2
    const-string v0, "Cooldown mode : [ON]"

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Le/w;->xb:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    iput-boolean v2, p0, Le/w;->r:Z

    goto :goto_1

    :cond_3
    const-string v1, "com.android.systemui.power.action.ACTION_REQUEST_SHUTDOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v0, p0, Le/w;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_4

    :try_start_1
    sget-object v0, Le/w;->TAG:Ljava/lang/String;

    const-string v1, " onReceive: create cooldown shutdown file"

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/w;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :try_start_2
    iget-object v0, p0, Le/w;->v:Ljava/io/File;

    const/16 v1, 0x1a4

    invoke-static {v0, v1}, Le/w;->a(Ljava/io/File;I)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_4
    :goto_3
    if-eqz p1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.KEY_CONFIRM"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v1, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_2

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :cond_5
    const-string v1, "com.android.systemui.power.action.ACTION_CLEAR_SHUTDOWN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/w;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Le/w;->TAG:Ljava/lang/String;

    const-string v1, " onReceive: delete cooldown shutdown file."

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/w;->v:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2
.end method

.method public am(Ljava/lang/String;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Le/w;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Le/w;->mContext:Landroid/content/Context;

    const v2, 0x10407a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Le/w;->mContext:Landroid/content/Context;

    const v2, 0x104079f

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Le/w;->mContext:Landroid/content/Context;

    const v2, 0x10407a1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Le/y;

    invoke-direct {v2, p0}, Le/y;-><init>(Le/w;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d9

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public ec()V
    .locals 7

    const/4 v5, 0x0

    iget-boolean v0, p0, Le/w;->r:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Le/w;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Le/w;->xc:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "android.provider.Telephony.SECRET_CODE"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "android_secret_code"

    invoke-virtual {v3, v0}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v0, p0, Le/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Le/w;->xe:Landroid/content/BroadcastReceiver;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const-string v4, "com.sec.factory.permission.KEYSTRING"

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    new-instance v4, Landroid/content/IntentFilter;

    invoke-direct {v4}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.android.systemui.power.action.ACTION_REQUEST_SHUTDOWN"

    invoke-virtual {v4, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v0, "com.android.systemui.power.action.ACTION_CLEAR_SHUTDOWN"

    invoke-virtual {v4, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Le/w;->mContext:Landroid/content/Context;

    iget-object v2, p0, Le/w;->xe:Landroid/content/BroadcastReceiver;

    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    move-object v6, v5

    invoke-virtual/range {v1 .. v6}, Landroid/content/Context;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public g(II)V
    .locals 7

    const v6, 0x104079e

    const/16 v5, 0x8

    const/4 v3, 0x6

    const/4 v4, 0x0

    iget-boolean v0, p0, Le/w;->wY:Z

    if-nez v0, :cond_0

    invoke-static {}, Le/w;->ev()Z

    move-result v0

    iput-boolean v0, p0, Le/w;->wY:Z

    iget-boolean v0, p0, Le/w;->wY:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Le/w;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "apTemp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", batTemp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Le/w;->wS:I

    if-ge p1, v0, :cond_1

    iget v0, p0, Le/w;->wU:I

    if-lt p2, v0, :cond_4

    :cond_1
    iput v5, p0, Le/w;->wN:I

    :goto_1
    iget v0, p0, Le/w;->wN:I

    iget v1, p0, Le/w;->wM:I

    if-eq v0, v1, :cond_3

    sget-object v0, Le/w;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CoolDownLevel is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Le/w;->wN:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Le/w;->wN:I

    invoke-static {v0}, Lcom/android/server/ssrm/G;->l(I)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget v1, p0, Le/w;->wN:I

    if-ne v1, v5, :cond_7

    iget v0, p0, Le/w;->wQ:I

    int-to-long v0, v0

    iput-wide v0, p0, Le/w;->wO:J

    :goto_2
    iget-boolean v0, p0, Le/w;->wZ:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Le/w;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Le/w;->u:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v4, p0, Le/w;->wZ:Z

    :cond_2
    iget v0, p0, Le/w;->wN:I

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/w;->wZ:Z

    iget-object v0, p0, Le/w;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Le/w;->u:Ljava/lang/Runnable;

    iget-wide v2, p0, Le/w;->wO:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_3
    iget v0, p0, Le/w;->wN:I

    iput v0, p0, Le/w;->wM:I

    goto/16 :goto_0

    :cond_4
    iget v0, p0, Le/w;->wR:I

    if-ge p1, v0, :cond_5

    iget v0, p0, Le/w;->wT:I

    if-lt p2, v0, :cond_6

    :cond_5
    iput v3, p0, Le/w;->wN:I

    goto :goto_1

    :cond_6
    iput v4, p0, Le/w;->wN:I

    goto :goto_1

    :cond_7
    iget v1, p0, Le/w;->wN:I

    if-ne v1, v3, :cond_8

    iget v1, p0, Le/w;->wP:I

    int-to-long v2, v1

    iput-wide v2, p0, Le/w;->wO:J

    const-string v1, "com.into.stability"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v2, p0, Le/w;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/w;->xa:Ljava/lang/String;

    iget v2, p0, Le/w;->wN:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/w;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Le/w;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto :goto_2

    :cond_8
    iget-object v1, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v2, p0, Le/w;->n:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/w;->xa:Ljava/lang/String;

    iget v2, p0, Le/w;->wN:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/w;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Le/w;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    goto/16 :goto_2
.end method
