.class public Le/h;
.super Ljava/lang/Object;


# instance fields
.field final TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mNetworkInfo:Landroid/net/NetworkInfo;

.field final vV:I

.field final vW:I

.field final vX:I

.field final vY:I

.field vZ:Landroid/os/DVFSHelper;

.field wa:Landroid/os/DVFSHelper;

.field wb:Landroid/os/DVFSHelper;

.field wc:Landroid/os/DVFSHelper;

.field wd:Landroid/os/DVFSHelper;

.field we:J

.field wf:J

.field wg:I

.field wh:Z

.field wi:Z

.field wj:I

.field wk:Le/q;

.field final wl:Ljava/util/List;

.field wm:Le/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 4

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Le/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/h;->TAG:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Le/h;->vV:I

    const/4 v0, 0x2

    iput v0, p0, Le/h;->vW:I

    const/4 v0, 0x3

    iput v0, p0, Le/h;->vX:I

    const/4 v0, 0x4

    iput v0, p0, Le/h;->vY:I

    iput-wide v2, p0, Le/h;->we:J

    iput-wide v2, p0, Le/h;->wf:J

    iput v1, p0, Le/h;->wg:I

    iput-boolean v1, p0, Le/h;->wh:Z

    iput-boolean v1, p0, Le/h;->wi:Z

    iput v1, p0, Le/h;->wj:I

    new-instance v0, Le/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Le/q;-><init>(Le/h;Le/i;)V

    iput-object v0, p0, Le/h;->wk:Le/q;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le/h;->wl:Ljava/util/List;

    iput-object p1, p0, Le/h;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    invoke-direct {p0}, Le/h;->eg()Le/k;

    move-result-object v1

    iput-object v1, p0, Le/h;->wm:Le/k;

    iget-object v1, p0, Le/h;->wm:Le/k;

    if-nez v1, :cond_0

    iget-object v0, p0, Le/h;->TAG:Ljava/lang/String;

    const-string v1, "No model"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Le/i;

    invoke-direct {v1, p0}, Le/i;-><init>(Le/h;)V

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method private eg()Le/k;
    .locals 2

    sget-boolean v0, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v0, :cond_0

    new-instance v0, Le/p;

    invoke-direct {v0, p0}, Le/p;-><init>(Le/h;)V

    :goto_0
    return-object v0

    :cond_0
    sget-boolean v0, Lcom/android/server/ssrm/N;->cV:Z

    if-eqz v0, :cond_1

    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Le/m;

    invoke-direct {v0, p0}, Le/m;-><init>(Le/h;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public am(I)V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Le/h;->wg:I

    if-eq v0, p1, :cond_4

    iget-object v0, p0, Le/h;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Le/h;->wg:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lt p1, v3, :cond_5

    iget v0, p0, Le/h;->wg:I

    if-gtz v0, :cond_5

    iget-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_0
    :goto_0
    const/4 v0, 0x2

    if-lt p1, v0, :cond_6

    iget-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_1
    iget-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_2

    iget-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_2
    iget-object v0, p0, Le/h;->wm:Le/k;

    iget-object v1, p0, Le/h;->wm:Le/k;

    invoke-virtual {v1}, Le/k;->ek()I

    move-result v1

    invoke-virtual {v0, v1}, Le/k;->an(I)V

    :goto_1
    iget v0, p0, Le/h;->wg:I

    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_2
    iput p1, p0, Le/h;->wg:I

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_0

    :cond_6
    if-lt p1, v3, :cond_9

    iget-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_7

    iget-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_7
    iget-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_8

    iget-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    :cond_8
    iget-object v0, p0, Le/h;->wm:Le/k;

    iget-object v1, p0, Le/h;->wm:Le/k;

    invoke-virtual {v1}, Le/k;->ej()I

    move-result v1

    invoke-virtual {v0, v1}, Le/k;->an(I)V

    goto :goto_1

    :cond_9
    iget-object v0, p0, Le/h;->wm:Le/k;

    iget-object v1, p0, Le/h;->wm:Le/k;

    invoke-virtual {v1}, Le/k;->ei()I

    move-result v1

    invoke-virtual {v0, v1}, Le/k;->an(I)V

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_a

    iget-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_a
    iget-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_3

    iget-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_b

    iget-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    :cond_b
    iget-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    if-eqz v0, :cond_3

    iget-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public ee()V
    .locals 9

    const/16 v8, 0x13

    const/16 v3, 0xc

    const-wide/16 v4, 0x0

    iget-boolean v0, p0, Le/h;->wh:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->ep()I

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Le/h;->mContext:Landroid/content/Context;

    const-string v2, "LTE_TP_CPU1"

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    iget-object v0, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Le/h;->vZ:Landroid/os/DVFSHelper;

    iget-object v6, p0, Le/h;->wm:Le/k;

    invoke-virtual {v6}, Le/k;->ep()I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v6, v2

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_1
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->eq()I

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Le/h;->mContext:Landroid/content/Context;

    const-string v2, "LTE_TP_CPU2"

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    iget-object v0, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    const-string v1, "CPU"

    iget-object v2, p0, Le/h;->wa:Landroid/os/DVFSHelper;

    iget-object v3, p0, Le/h;->wm:Le/k;

    invoke-virtual {v3}, Le/k;->eq()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/os/DVFSHelper;->getApproximateCPUFrequency(I)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_2
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->er()I

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Le/h;->mContext:Landroid/content/Context;

    const-string v2, "LTE_TP_CORE"

    const/16 v3, 0xe

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    iget-object v0, p0, Le/h;->wb:Landroid/os/DVFSHelper;

    const-string v1, "CORE_NUM"

    iget-object v2, p0, Le/h;->wm:Le/k;

    invoke-virtual {v2}, Le/k;->er()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_3
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->es()I

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Le/h;->mContext:Landroid/content/Context;

    const-string v2, "LTE_TP_BUS1"

    move v3, v8

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    iget-object v0, p0, Le/h;->wc:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    iget-object v2, p0, Le/h;->wm:Le/k;

    invoke-virtual {v2}, Le/k;->es()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_4
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->et()I

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/os/DVFSHelper;

    iget-object v1, p0, Le/h;->mContext:Landroid/content/Context;

    const-string v2, "LTE_TP_BUS2"

    move v3, v8

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    iget-object v0, p0, Le/h;->wd:Landroid/os/DVFSHelper;

    const-string v1, "BUS"

    iget-object v2, p0, Le/h;->wm:Le/k;

    invoke-virtual {v2}, Le/k;->et()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/DVFSHelper;->addExtraOption(Ljava/lang/String;J)V

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/h;->wh:Z

    goto/16 :goto_0
.end method

.method public ef()V
    .locals 8

    const-wide/16 v6, 0x8

    iget-boolean v0, p0, Le/h;->wh:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Le/h;->ee()V

    :cond_0
    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->eu()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Le/h;->we:J

    iget-wide v2, p0, Le/h;->wf:J

    invoke-static {}, Landroid/net/TrafficStats;->getMobileTxBytes()J

    move-result-wide v4

    iput-wide v4, p0, Le/h;->we:J

    invoke-static {}, Landroid/net/TrafficStats;->getMobileRxBytes()J

    move-result-wide v4

    iput-wide v4, p0, Le/h;->wf:J

    iget-wide v4, p0, Le/h;->we:J

    sub-long v0, v4, v0

    mul-long/2addr v0, v6

    iget-object v4, p0, Le/h;->wm:Le/k;

    invoke-virtual {v4}, Le/k;->eh()I

    move-result v4

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    div-long/2addr v0, v4

    iget-wide v4, p0, Le/h;->wf:J

    sub-long v2, v4, v2

    mul-long/2addr v2, v6

    iget-object v4, p0, Le/h;->wm:Le/k;

    invoke-virtual {v4}, Le/k;->eh()I

    move-result v4

    div-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    div-long/2addr v2, v4

    iget-object v4, p0, Le/h;->wm:Le/k;

    invoke-virtual {v4}, Le/k;->en()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gtz v4, :cond_1

    iget-object v4, p0, Le/h;->wm:Le/k;

    invoke-virtual {v4}, Le/k;->eo()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_3

    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Le/h;->am(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v4, p0, Le/h;->wm:Le/k;

    invoke-virtual {v4}, Le/k;->el()J

    move-result-wide v4

    cmp-long v0, v0, v4

    if-gtz v0, :cond_4

    iget-object v0, p0, Le/h;->wm:Le/k;

    invoke-virtual {v0}, Le/k;->em()J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Le/h;->am(I)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Le/h;->am(I)V

    goto :goto_0
.end method
