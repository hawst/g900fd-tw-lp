.class public Le/t;
.super Landroid/os/UEventObserver;

# interfaces
.implements Le/b;


# instance fields
.field final TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mTelephonyManager:Landroid/telephony/TelephonyManager;

.field final wF:Ljava/lang/String;

.field final wG:Ljava/lang/String;

.field wH:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/os/UEventObserver;-><init>()V

    const-class v0, Le/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v0, "TMUSTATE"

    iput-object v0, p0, Le/t;->wF:Ljava/lang/String;

    const-string v0, "SWITCH_NAME"

    iput-object v0, p0, Le/t;->wG:Ljava/lang/String;

    iput-boolean v2, p0, Le/t;->wH:Z

    iput-object p1, p0, Le/t;->mContext:Landroid/content/Context;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Le/t;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    iget-object v0, p0, Le/t;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    invoke-static {p1, v0}, Landroid/os/FactoryTest;->isFactoryMode(Landroid/content/Context;Landroid/telephony/TelephonyManager;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Le/t;->wH:Z

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isUserBuild = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Le/t;->wH:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Le/t;->wH:Z

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/android/server/ssrm/aT;->y(Z)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public ec()V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-boolean v2, Lcom/android/server/ssrm/N;->cI:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/ssrm/N;->cM:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/ssrm/N;->cT:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/ssrm/N;->cU:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/android/server/ssrm/N;->dd:Z

    if-eqz v2, :cond_5

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    const-string v2, "TMUSTATE"

    invoke-virtual {p0, v2}, Le/t;->startObserving(Ljava/lang/String;)V

    :cond_1
    sget-boolean v2, Lcom/android/server/ssrm/N;->cN:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/android/server/ssrm/N;->cV:Z

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/android/server/ssrm/N;->cY:Z

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    iget-boolean v1, p0, Le/t;->wH:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    const-string v0, "SWITCH_NAME"

    invoke-virtual {p0, v0}, Le/t;->startObserving(Ljava/lang/String;)V

    :cond_4
    return-void

    :cond_5
    move v2, v0

    goto :goto_0
.end method

.method public onUEvent(Landroid/os/UEventObserver$UEvent;)V
    .locals 4

    const/4 v3, -0x1

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "UEvent : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "TMUSTATE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eq v0, v3, :cond_1

    :try_start_1
    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "TMU_UEVENT_MATCH matched"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TMUSTATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "Tmu state Tripping"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Le/t;->systemShutdown()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not parse TMUSTATE from event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    :try_start_3
    invoke-virtual {p1}, Landroid/os/UEventObserver$UEvent;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SWITCH_NAME"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v0

    if-eq v0, v3, :cond_0

    :try_start_4
    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "SWITCH_NAME_MATCH matched"

    invoke-static {v0, v1}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SWITCH_NAME"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "uart3"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SWITCH_STATE"

    invoke-virtual {p1, v0}, Landroid/os/UEventObserver$UEvent;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Le/t;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "switchState = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "at_distributor is stopped by SSRM."

    invoke-static {v0, v1}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "stop at_distributor"

    invoke-static {v0, v1}, Le/v;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :try_start_6
    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "at_distributor is restarted by SSRM."

    invoke-static {v0, v1}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "start at_distributor"

    invoke-static {v0, v1}, Le/v;->f(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0
.end method

.method public systemShutdown()V
    .locals 3

    iget-object v0, p0, Le/t;->TAG:Ljava/lang/String;

    const-string v1, "systemShutdown"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Le/t;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.ACTION_REQUEST_SHUTDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.KEY_CONFIRM"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "android.intent.extra.SYSTEM_REQUEST"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Le/t;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
