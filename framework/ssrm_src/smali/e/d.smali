.class public final Le/d;
.super Ljava/lang/Object;


# static fields
.field private static final vI:Ljava/lang/String; = "/sys/class/power_supply/battery/temp"

.field private static final vJ:Ljava/lang/String; = "/sys/class/lcd/panel/temperature"

.field private static vL:I

.field private static vM:I

.field private static vN:I


# instance fields
.field final TAG:Ljava/lang/String;

.field mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field vG:Z

.field vH:Z

.field vK:[I

.field vO:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xc8

    sput v0, Le/d;->vL:I

    sput v0, Le/d;->vM:I

    const/16 v0, 0x64

    sput v0, Le/d;->vN:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-class v0, Le/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    iput-boolean v2, p0, Le/d;->vG:Z

    iput-boolean v2, p0, Le/d;->vH:Z

    const/4 v0, 0x0

    iput-object v0, p0, Le/d;->vK:[I

    new-instance v0, Le/e;

    invoke-direct {v0, p0}, Le/e;-><init>(Le/d;)V

    iput-object v0, p0, Le/d;->vO:Ljava/lang/Runnable;

    iput-object p1, p0, Le/d;->mContext:Landroid/content/Context;

    iput-object p2, p0, Le/d;->mHandler:Landroid/os/Handler;

    new-instance v0, Ljava/io/File;

    const-string v3, "/sys/class/power_supply/battery/temp"

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    const-string v4, "/sys/class/lcd/panel/temperature"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Le/d;->vG:Z

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->canWrite()Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Le/d;->vH:Z

    iget-boolean v0, p0, Le/d;->vG:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Le/d;->vH:Z

    if-nez v0, :cond_3

    :cond_0
    iget-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mInputPathExist = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Le/d;->vG:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOutputPathExist = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Le/d;->vH:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/lcd/panel/temperature"

    invoke-static {v0, v1}, Le/v;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-gtz v0, :cond_5

    :cond_4
    iget-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    const-string v1, "failed to read /sys/class/lcd/panel/temperature"

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v2, p0, Le/d;->vH:Z

    goto :goto_2

    :cond_5
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v0, ", "

    invoke-direct {v3, v1, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    :goto_3
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v4

    if-eqz v4, :cond_6

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    goto :goto_3

    :cond_6
    new-array v0, v0, [I

    iput-object v0, p0, Le/d;->vK:[I

    new-instance v0, Ljava/util/StringTokenizer;

    const-string v3, ", "

    invoke-direct {v0, v1, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v0}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, p0, Le/d;->vK:[I

    invoke-virtual {v0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v1, v2

    iget-object v1, p0, Le/d;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mAmoledTempTable["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Le/d;->vK:[I

    aget v4, v4, v2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Le/d;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Le/d;->vO:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_2
.end method

.method private ed()I
    .locals 2

    :try_start_0
    iget-object v0, p0, Le/d;->TAG:Ljava/lang/String;

    const-string v1, "/sys/class/power_supply/battery/temp"

    invoke-static {v0, v1}, Le/v;->readSysfs(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method aj(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Le/d;->vK:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Le/d;->vK:[I

    aget v1, v1, v0

    mul-int/lit8 v1, v1, 0xa

    if-ge p1, v1, :cond_1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method ak(I)I
    .locals 2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Le/d;->vK:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Le/d;->vK:[I

    aget v1, v1, v0

    mul-int/lit8 v1, v1, 0xa

    if-gt p1, v1, :cond_1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method al(I)I
    .locals 1

    iget-object v0, p0, Le/d;->vK:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Le/d;->vK:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public update()V
    .locals 5

    iget-boolean v0, p0, Le/d;->vG:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Le/d;->vH:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/d;->vK:[I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Le/d;->ed()I

    move-result v0

    sput v0, Le/d;->vM:I

    sget v0, Le/d;->vL:I

    invoke-virtual {p0, v0}, Le/d;->aj(I)I

    move-result v0

    sget v1, Le/d;->vM:I

    invoke-virtual {p0, v1}, Le/d;->aj(I)I

    move-result v1

    iget-object v2, p0, Le/d;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "prevTemp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Le/d;->vL:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", currTemp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Le/d;->vM:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", prevStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", currStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    if-le v0, v1, :cond_4

    sget v0, Le/d;->vM:I

    invoke-virtual {p0, v0}, Le/d;->ak(I)I

    move-result v0

    invoke-virtual {p0, v0}, Le/d;->al(I)I

    move-result v0

    sget v1, Le/d;->vN:I

    if-eq v1, v0, :cond_2

    iget-object v1, p0, Le/d;->TAG:Ljava/lang/String;

    const-string v2, "/sys/class/lcd/panel/temperature"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    sput v0, Le/d;->vN:I

    :cond_3
    :goto_1
    sget v0, Le/d;->vM:I

    sput v0, Le/d;->vL:I

    goto :goto_0

    :cond_4
    if-ge v0, v1, :cond_3

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p0, v0}, Le/d;->al(I)I

    move-result v0

    sget v1, Le/d;->vN:I

    if-eq v1, v0, :cond_5

    iget-object v1, p0, Le/d;->TAG:Ljava/lang/String;

    const-string v2, "/sys/class/lcd/panel/temperature"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Le/v;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    sput v0, Le/d;->vN:I

    goto :goto_1
.end method
