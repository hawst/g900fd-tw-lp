.class public Le/g;
.super Ljava/lang/Object;

# interfaces
.implements Le/c;


# instance fields
.field final pb:Ljava/lang/String;

.field final pc:Ljava/lang/String;

.field final pm:I

.field final pn:I

.field final vR:Ljava/lang/String;

.field vS:Lcom/android/server/ssrm/d;

.field vT:Landroid/os/DVFSHelper;

.field vU:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 6

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Le/v;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/g;->pb:Ljava/lang/String;

    const/16 v0, 0x14

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    invoke-static {v0}, Le/v;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/g;->pc:Ljava/lang/String;

    const/16 v0, 0x20

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    invoke-static {v0}, Le/v;->a([I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Le/g;->vR:Ljava/lang/String;

    iput-object v1, p0, Le/g;->vS:Lcom/android/server/ssrm/d;

    iput-object v1, p0, Le/g;->vT:Landroid/os/DVFSHelper;

    const/16 v0, 0x3a98

    iput v0, p0, Le/g;->pm:I

    const/16 v0, 0x2710

    iput v0, p0, Le/g;->pn:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Le/g;->vU:Z

    const-string v0, "GAME_LAUNCH_BOOST"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v2, v3}, Lcom/android/server/ssrm/d;->a(Ljava/lang/String;D)Lcom/android/server/ssrm/d;

    move-result-object v0

    iput-object v0, p0, Le/g;->vS:Lcom/android/server/ssrm/d;

    new-instance v0, Landroid/os/DVFSHelper;

    const-string v2, "POWER_COLLAPSE_DISABLE"

    const/16 v3, 0x16

    const-wide/16 v4, 0x0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Landroid/os/DVFSHelper;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Le/g;->vT:Landroid/os/DVFSHelper;

    return-void

    nop

    :array_0
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x13
        0x17
        0x1b
        0x14
        0x1d
        0x13
        0x54
        0xe
        0x1f
        0x17
        0xa
        0x16
        0x1f
        0x8
        0xf
        0x14
        0x48
    .end array-data

    :array_1
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x11
        0x13
        0x16
        0x15
        0x15
        0x54
        0x9
        0xf
        0x18
        0xd
        0x1b
        0x3
        0x9
        0xf
        0x8
        0x1c
    .end array-data

    :array_2
    .array-data 4
        0x19
        0x15
        0x17
        0x54
        0x8
        0x1b
        0xe
        0x8
        0x15
        0x1e
        0x9
        0xe
        0xf
        0x1e
        0x13
        0x15
        0x54
        0x1e
        0x8
        0x13
        0x1c
        0xe
        0x17
        0x1b
        0x14
        0x13
        0x1b
        0x48
        0x16
        0x13
        0xe
        0x1f
    .end array-data
.end method


# virtual methods
.method public J(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Le/g;->pb:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Le/g;->vS:Lcom/android/server/ssrm/d;

    const/16 v1, 0x3a98

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/d;->acquire(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Le/g;->pc:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Le/g;->vS:Lcom/android/server/ssrm/d;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/android/server/ssrm/d;->acquire(I)V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Le/g;->vU:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Le/g;->vR:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "GAMEBooster"

    const-string v1, "ACQUIRE!!"

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/g;->vT:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->acquire()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Le/g;->vU:Z

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Le/g;->vU:Z

    if-eqz v0, :cond_0

    const-string v0, "GAMEBooster"

    const-string v1, "RELEASE!!"

    invoke-static {v0, v1}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Le/g;->vT:Landroid/os/DVFSHelper;

    invoke-virtual {v0}, Landroid/os/DVFSHelper;->release()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Le/g;->vU:Z

    goto :goto_0
.end method
