.class Le/i;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic wn:Le/h;


# direct methods
.method constructor <init>(Le/h;)V
    .locals 0

    iput-object p1, p0, Le/i;->wn:Le/h;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Le/i;->wn:Le/h;

    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iput-object v0, v1, Le/h;->mNetworkInfo:Landroid/net/NetworkInfo;

    iget-object v0, p0, Le/i;->wn:Le/h;

    iput-object p1, v0, Le/h;->mContext:Landroid/content/Context;

    iget-object v0, p0, Le/i;->wn:Le/h;

    iget-object v0, v0, Le/h;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Le/j;->$SwitchMap$android$net$NetworkInfo$DetailedState:[I

    iget-object v1, p0, Le/i;->wn:Le/h;

    iget-object v1, v1, Le/h;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Le/i;->wn:Le/h;

    iget-object v0, v0, Le/h;->TAG:Ljava/lang/String;

    const-string v1, "MOBILE CONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Le/i;->wn:Le/h;

    iget-object v0, v0, Le/h;->wk:Le/q;

    invoke-static {v0, v3, v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Le/i;->wn:Le/h;

    iget-object v0, v0, Le/h;->TAG:Ljava/lang/String;

    const-string v1, "MOBILE DISCONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Le/i;->wn:Le/h;

    iget-object v0, v0, Le/h;->wk:Le/q;

    invoke-static {v0, v3, v2, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
