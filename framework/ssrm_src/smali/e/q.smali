.class Le/q;
.super Landroid/os/Handler;


# instance fields
.field final synthetic wn:Le/h;


# direct methods
.method private constructor <init>(Le/h;)V
    .locals 0

    iput-object p1, p0, Le/q;->wn:Le/h;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Le/h;Le/i;)V
    .locals 0

    invoke-direct {p0, p1}, Le/q;-><init>(Le/h;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v2, p0, Le/q;->wn:Le/h;

    iget v3, p1, Landroid/os/Message;->arg1:I

    if-ne v3, v0, :cond_1

    :goto_1
    iput-boolean v0, v2, Le/h;->wi:Z

    iget-object v0, p0, Le/q;->wn:Le/h;

    iget v2, v0, Le/h;->wj:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Le/h;->wj:I

    iget-object v0, p0, Le/q;->wn:Le/h;

    iget-boolean v0, v0, Le/h;->wi:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Le/q;->wn:Le/h;

    invoke-virtual {v0}, Le/h;->ef()V

    iget-object v0, p0, Le/q;->wn:Le/h;

    iget v0, v0, Le/h;->wj:I

    invoke-static {p0, v4, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Le/q;->wn:Le/h;

    iget-object v1, v1, Le/h;->wm:Le/k;

    invoke-virtual {v1}, Le/k;->eh()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Le/q;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v2, p0, Le/q;->wn:Le/h;

    iget v2, v2, Le/h;->wj:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Le/q;->wn:Le/h;

    invoke-virtual {v0}, Le/h;->ef()V

    iget-object v0, p0, Le/q;->wn:Le/h;

    iget v0, v0, Le/h;->wj:I

    invoke-static {p0, v4, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Le/q;->wn:Le/h;

    iget-object v1, v1, Le/h;->wm:Le/k;

    invoke-virtual {v1}, Le/k;->eh()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Le/q;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Le/q;->wn:Le/h;

    iget-object v1, v0, Le/h;->wl:Ljava/util/List;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Messenger;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Le/q;->wn:Le/h;

    iget-object v0, v0, Le/h;->wl:Ljava/util/List;

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
