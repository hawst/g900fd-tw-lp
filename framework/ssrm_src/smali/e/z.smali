.class Le/z;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic xf:Le/w;


# direct methods
.method constructor <init>(Le/w;)V
    .locals 0

    iput-object p1, p0, Le/z;->xf:Le/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Le/z;->xf:Le/w;

    iget-boolean v1, v1, Le/w;->wZ:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Le/z;->xf:Le/w;

    iget v1, v1, Le/w;->wN:I

    const/4 v2, 0x6

    if-lt v1, v2, :cond_3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iget-object v2, p0, Le/z;->xf:Le/w;

    iget v2, v2, Le/w;->wN:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Le/z;->xf:Le/w;

    iget-object v2, v2, Le/w;->q:[Ljava/lang/String;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    sget-object v5, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le/z;->xf:Le/w;

    iget-object v0, v0, Le/w;->j:Landroid/content/Intent;

    iget-object v2, p0, Le/z;->xf:Le/w;

    iget-object v2, v2, Le/w;->n:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Le/z;->xf:Le/w;

    iget-object v0, v0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/z;->xf:Le/w;

    iget-object v1, v1, Le/w;->xa:Ljava/lang/String;

    iget-object v2, p0, Le/z;->xf:Le/w;

    iget v2, v2, Le/w;->wN:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/z;->xf:Le/w;

    iget-object v0, v0, Le/w;->j:Landroid/content/Intent;

    iget-object v1, p0, Le/z;->xf:Le/w;

    iget-object v1, v1, Le/w;->m:Ljava/lang/String;

    const v2, 0x104079e

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Le/z;->xf:Le/w;

    iget-object v0, v0, Le/w;->mContext:Landroid/content/Context;

    iget-object v1, p0, Le/z;->xf:Le/w;

    iget-object v1, v1, Le/w;->j:Landroid/content/Intent;

    sget-object v2, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_1
    iget-object v0, p0, Le/z;->xf:Le/w;

    iget-object v1, p0, Le/z;->xf:Le/w;

    iget v1, v1, Le/w;->wN:I

    invoke-virtual {v0, v1}, Le/w;->a(I)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v1, p0, Le/z;->xf:Le/w;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Le/w;->am(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Le/z;->xf:Le/w;

    const/4 v1, 0x0

    iput-boolean v1, v0, Le/w;->wZ:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Le/w;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mKillActiveApplicationsRunnable:: e = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Le/v;->logOnEng(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
