.class public Lcom/sec/android/org/bouncycastle/ocsp/OCSPRespGenerator;
.super Ljava/lang/Object;
.source "OCSPRespGenerator.java"


# static fields
.field public static final INTERNAL_ERROR:I = 0x2

.field public static final MALFORMED_REQUEST:I = 0x1

.field public static final SIG_REQUIRED:I = 0x5

.field public static final SUCCESSFUL:I = 0x0

.field public static final TRY_LATER:I = 0x3

.field public static final UNAUTHORIZED:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public generate(ILjava/lang/Object;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;
    .locals 8
    .param p1, "status"    # I
    .param p2, "response"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 33
    if-nez p2, :cond_0

    .line 35
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;

    new-instance v5, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;

    new-instance v6, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;

    invoke-direct {v6, p1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;-><init>(I)V

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;)V

    invoke-direct {v4, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;)V

    .line 54
    :goto_0
    return-object v4

    .line 37
    :cond_0
    instance-of v4, p2, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;

    if-eqz v4, :cond_1

    move-object v2, p2

    .line 39
    check-cast v2, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;

    .line 44
    .local v2, "r":Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    :try_start_0
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DEROctetString;

    invoke-virtual {v2}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->getEncoded()[B

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/android/org/bouncycastle/asn1/DEROctetString;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .local v1, "octs":Lcom/android/org/bouncycastle/asn1/ASN1OctetString;
    new-instance v3, Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;

    sget-object v4, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPObjectIdentifiers;->id_pkix_ocsp_basic:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v3, v4, v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/org/bouncycastle/asn1/ASN1OctetString;)V

    .line 54
    .local v3, "rb":Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;

    new-instance v5, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;

    new-instance v6, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;

    invoke-direct {v6, p1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;-><init>(I)V

    invoke-direct {v5, v6, v3}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponseStatus;Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;)V

    invoke-direct {v4, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPResponse;)V

    goto :goto_0

    .line 46
    .end local v1    # "octs":Lcom/android/org/bouncycastle/asn1/ASN1OctetString;
    .end local v3    # "rb":Lcom/sec/android/org/bouncycastle/asn1/ocsp/ResponseBytes;
    :catch_0
    move-exception v0

    .line 48
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "can\'t encode object."

    invoke-direct {v4, v5, v0}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 58
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "r":Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    :cond_1
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "unknown response object"

    invoke-direct {v4, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v4
.end method
