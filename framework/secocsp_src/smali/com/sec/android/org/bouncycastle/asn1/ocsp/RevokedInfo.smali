.class public Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;
.super Lcom/android/org/bouncycastle/asn1/ASN1Object;
.source "RevokedInfo.java"


# instance fields
.field private revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

.field private revocationTime:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;


# direct methods
.method private constructor <init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 2
    .param p1, "seq"    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    const/4 v1, 0x1

    .line 30
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationTime:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 33
    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 35
    invoke-virtual {p1, v1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v0, v1}, Lcom/android/org/bouncycastle/asn1/DEREnumerated;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Enumerated;

    move-result-object v0

    invoke-static {v0}, Lcom/android/org/bouncycastle/asn1/x509/CRLReason;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    .line 38
    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/CRLReason;)V
    .locals 0
    .param p1, "revocationTime"    # Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .param p2, "revocationReason"    # Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationTime:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 25
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    .line 26
    return-void
.end method

.method public static getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;
    .locals 1
    .param p0, "obj"    # Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 44
    invoke-static {p0, p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 50
    instance-of v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    if-eqz v0, :cond_0

    .line 52
    check-cast p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    .line 59
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 54
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 56
    new-instance v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    invoke-static {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 59
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getRevocationReason()Lcom/android/org/bouncycastle/asn1/x509/CRLReason;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    return-object v0
.end method

.method public getRevocationTime()Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationTime:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5

    .prologue
    .line 82
    new-instance v0, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 84
    .local v0, "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationTime:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 85
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    if-eqz v1, :cond_0

    .line 87
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;->revocationReason:Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    invoke-direct {v1, v2, v3, v4}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 90
    :cond_0
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
