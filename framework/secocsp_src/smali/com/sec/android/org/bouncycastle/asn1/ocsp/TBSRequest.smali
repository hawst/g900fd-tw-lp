.class public Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;
.super Lcom/android/org/bouncycastle/asn1/ASN1Object;
.source "TBSRequest.java"


# static fields
.field private static final V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;


# instance fields
.field requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

.field requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

.field requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

.field version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

.field versionSet:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 18
    new-instance v0, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(J)V

    sput-object v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    return-void
.end method

.method private constructor <init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 6
    .param p1, "seq"    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 57
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    .line 60
    .local v0, "index":I
    invoke-virtual {p1, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    instance-of v3, v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v3, :cond_3

    .line 62
    invoke-virtual {p1, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v2

    check-cast v2, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    .line 64
    .local v2, "o":Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    invoke-virtual {v2}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v3

    if-nez v3, :cond_2

    .line 66
    iput-boolean v5, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->versionSet:Z

    .line 67
    invoke-virtual {p1, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    check-cast v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v3, v5}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    .line 68
    add-int/lit8 v0, v0, 0x1

    .line 80
    .end local v2    # "o":Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    :goto_0
    invoke-virtual {p1, v0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    instance-of v3, v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v3, :cond_0

    .line 82
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .local v1, "index":I
    invoke-virtual {p1, v0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    check-cast v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v3, v5}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    move v0, v1

    .line 85
    .end local v1    # "index":I
    .restart local v0    # "index":I
    :cond_0
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "index":I
    .restart local v1    # "index":I
    invoke-virtual {p1, v0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    check-cast v3, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .line 87
    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v3

    add-int/lit8 v4, v1, 0x1

    if-ne v3, v4, :cond_1

    .line 89
    invoke-virtual {p1, v1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v3

    check-cast v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v3, v5}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .line 91
    :cond_1
    return-void

    .line 72
    .end local v1    # "index":I
    .restart local v0    # "index":I
    .restart local v2    # "o":Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    :cond_2
    sget-object v3, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_0

    .line 77
    .end local v2    # "o":Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    :cond_3
    sget-object v3, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    iput-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/android/org/bouncycastle/asn1/x509/GeneralName;Lcom/android/org/bouncycastle/asn1/ASN1Sequence;Lcom/android/org/bouncycastle/asn1/x509/Extensions;)V
    .locals 1
    .param p1, "requestorName"    # Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    .param p2, "requestList"    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;
    .param p3, "requestExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 49
    sget-object v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    .line 50
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    .line 51
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .line 52
    iput-object p3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/android/org/bouncycastle/asn1/x509/GeneralName;Lcom/android/org/bouncycastle/asn1/ASN1Sequence;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 1
    .param p1, "requestorName"    # Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    .param p2, "requestList"    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;
    .param p3, "requestExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 38
    sget-object v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    .line 39
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    .line 40
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .line 41
    invoke-static {p3}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .line 42
    return-void
.end method

.method public static getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;
    .locals 1
    .param p0, "obj"    # Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 97
    invoke-static {p0, p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 103
    instance-of v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;

    if-eqz v0, :cond_0

    .line 105
    check-cast p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;

    .line 112
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 107
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 109
    new-instance v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;

    invoke-static {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 112
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getRequestExtensions()Lcom/android/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    return-object v0
.end method

.method public getRequestList()Lcom/android/org/bouncycastle/asn1/ASN1Sequence;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    return-object v0
.end method

.method public getRequestorName()Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    return-object v0
.end method

.method public getVersion()Lcom/android/org/bouncycastle/asn1/ASN1Integer;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 147
    new-instance v0, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 153
    .local v0, "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    sget-object v2, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->V1:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v1, v2}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->versionSet:Z

    if-eqz v1, :cond_1

    .line 155
    :cond_0
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    invoke-direct {v1, v4, v2, v3}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 158
    :cond_1
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    if-eqz v1, :cond_2

    .line 160
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    invoke-direct {v1, v4, v4, v2}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestList:Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 165
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v1, :cond_3

    .line 167
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    invoke-direct {v1, v4, v2, v3}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 170
    :cond_3
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
