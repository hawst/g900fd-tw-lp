.class Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;
.super Ljava/lang/Object;
.source "BasicOCSPRespGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ResponseObject"
.end annotation


# instance fields
.field certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

.field certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

.field extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

.field nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

.field final synthetic this$0:Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator;

.field thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;


# direct methods
.method public constructor <init>(Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator;Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;Lcom/sec/android/org/bouncycastle/ocsp/CertificateStatus;Ljava/util/Date;Ljava/util/Date;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 6
    .param p2, "certId"    # Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    .param p3, "certStatus"    # Lcom/sec/android/org/bouncycastle/ocsp/CertificateStatus;
    .param p4, "thisUpdate"    # Ljava/util/Date;
    .param p5, "nextUpdate"    # Ljava/util/Date;
    .param p6, "extensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    const/4 v5, 0x0

    .line 61
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->this$0:Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    .line 64
    if-nez p3, :cond_0

    .line 66
    new-instance v1, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    invoke-direct {v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;-><init>()V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    .line 88
    :goto_0
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-direct {v1, p4}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;-><init>(Ljava/util/Date;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 90
    if-eqz p5, :cond_3

    .line 92
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-direct {v1, p5}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;-><init>(Ljava/util/Date;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 99
    :goto_1
    iput-object p6, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .line 100
    return-void

    .line 68
    :cond_0
    instance-of v1, p3, Lcom/sec/android/org/bouncycastle/ocsp/UnknownStatus;

    if-eqz v1, :cond_1

    .line 70
    new-instance v1, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    const/4 v2, 0x2

    sget-object v3, Lcom/android/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/org/bouncycastle/asn1/DERNull;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;-><init>(ILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    goto :goto_0

    :cond_1
    move-object v0, p3

    .line 74
    check-cast v0, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;

    .line 76
    .local v0, "rs":Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;
    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;->hasRevocationReason()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    new-instance v1, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    new-instance v2, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    new-instance v3, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;->getRevocationTime()Ljava/util/Date;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;-><init>(Ljava/util/Date;)V

    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;->getRevocationReason()I

    move-result v4

    invoke-static {v4}, Lcom/android/org/bouncycastle/asn1/x509/CRLReason;->lookup(I)Lcom/android/org/bouncycastle/asn1/x509/CRLReason;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;-><init>(Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/CRLReason;)V

    invoke-direct {v1, v2}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    goto :goto_0

    .line 83
    :cond_2
    new-instance v1, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    new-instance v2, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;

    new-instance v3, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;->getRevocationTime()Ljava/util/Date;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;-><init>(Ljava/util/Date;)V

    invoke-direct {v2, v3, v5}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;-><init>(Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/CRLReason;)V

    invoke-direct {v1, v2}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/RevokedInfo;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    goto :goto_0

    .line 96
    .end local v0    # "rs":Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;
    :cond_3
    iput-object v5, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    goto :goto_1
.end method


# virtual methods
.method public toResponse()Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;

    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    invoke-virtual {v1}, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;->toASN1Object()Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    iget-object v3, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    iget-object v4, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    iget-object v5, p0, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPRespGenerator$ResponseObject;->extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V

    return-object v0
.end method
