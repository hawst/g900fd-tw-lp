.class Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;
.super Ljava/lang/Object;
.source "OCSPReqGenerator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RequestObject"
.end annotation


# instance fields
.field certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

.field extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

.field final synthetic this$0:Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;


# direct methods
.method public constructor <init>(Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 0
    .param p2, "certId"    # Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    .param p3, "extensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->this$0:Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    .line 55
    iput-object p3, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .line 56
    return-void
.end method


# virtual methods
.method public toRequest()Lcom/sec/android/org/bouncycastle/asn1/ocsp/Request;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Request;

    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->certId:Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    invoke-virtual {v1}, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;->toASN1Object()Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->extensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    invoke-static {v2}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Request;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;Lcom/android/org/bouncycastle/asn1/x509/Extensions;)V

    return-object v0
.end method
