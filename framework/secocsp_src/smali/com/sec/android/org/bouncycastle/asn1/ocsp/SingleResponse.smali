.class public Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;
.super Lcom/android/org/bouncycastle/asn1/ASN1Object;
.source "SingleResponse.java"


# instance fields
.field private certID:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

.field private certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

.field private nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

.field private singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

.field private thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;


# direct methods
.method private constructor <init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 5
    .param p1, "seq"    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 57
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 58
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certID:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    .line 59
    invoke-virtual {p1, v2}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    .line 60
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    check-cast v1, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 62
    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v1

    if-le v1, v4, :cond_1

    .line 64
    invoke-virtual {p1, v3}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    check-cast v1, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v1, v2}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 66
    invoke-virtual {p1, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v1

    check-cast v1, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-static {v1, v2}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->size()I

    move-result v1

    if-le v1, v3, :cond_0

    .line 71
    invoke-virtual {p1, v3}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjectAt(I)Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v0

    check-cast v0, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    .line 73
    .local v0, "o":Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    invoke-virtual {v0}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v1

    if-nez v1, :cond_2

    .line 75
    invoke-static {v0, v2}, Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1GeneralizedTime;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    goto :goto_0

    .line 79
    :cond_2
    invoke-static {v0, v2}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/Extensions;)V
    .locals 0
    .param p1, "certID"    # Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;
    .param p2, "certStatus"    # Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;
    .param p3, "thisUpdate"    # Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .param p4, "nextUpdate"    # Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .param p5, "singleExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certID:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    .line 49
    iput-object p2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    .line 50
    iput-object p3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 51
    iput-object p4, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    .line 52
    iput-object p5, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 6
    .param p1, "certID"    # Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;
    .param p2, "certStatus"    # Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;
    .param p3, "thisUpdate"    # Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .param p4, "nextUpdate"    # Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .param p5, "singleExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    .line 38
    invoke-static {p5}, Lcom/android/org/bouncycastle/asn1/x509/Extensions;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;Lcom/android/org/bouncycastle/asn1/x509/Extensions;)V

    .line 39
    return-void
.end method

.method public static getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;
    .locals 1
    .param p0, "obj"    # Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;
    .param p1, "explicit"    # Z

    .prologue
    .line 88
    invoke-static {p0, p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;
    .locals 2
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 94
    instance-of v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;

    if-eqz v0, :cond_0

    .line 96
    check-cast p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;

    .line 103
    .end local p0    # "obj":Ljava/lang/Object;
    :goto_0
    return-object p0

    .line 98
    .restart local p0    # "obj":Ljava/lang/Object;
    :cond_0
    if-eqz p0, :cond_1

    .line 100
    new-instance v0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;

    invoke-static {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    .line 103
    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCertID()Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certID:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    return-object v0
.end method

.method public getCertStatus()Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    return-object v0
.end method

.method public getNextUpdate()Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    return-object v0
.end method

.method public getSingleExtensions()Lcom/android/org/bouncycastle/asn1/x509/Extensions;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    return-object v0
.end method

.method public getThisUpdate()Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 144
    new-instance v0, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 146
    .local v0, "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certID:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertID;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 147
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->certStatus:Lcom/sec/android/org/bouncycastle/asn1/ocsp/CertStatus;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 148
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->thisUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 150
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    if-eqz v1, :cond_0

    .line 152
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->nextUpdate:Lcom/android/org/bouncycastle/asn1/DERGeneralizedTime;

    invoke-direct {v1, v4, v2, v3}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    if-eqz v1, :cond_1

    .line 157
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/sec/android/org/bouncycastle/asn1/ocsp/SingleResponse;->singleExtensions:Lcom/android/org/bouncycastle/asn1/x509/Extensions;

    invoke-direct {v1, v4, v4, v2}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 160
    :cond_1
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v1, v0}, Lcom/android/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1
.end method
