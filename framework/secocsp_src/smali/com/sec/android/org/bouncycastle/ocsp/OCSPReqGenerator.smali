.class public Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;
.super Ljava/lang/Object;
.source "OCSPReqGenerator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;
    }
.end annotation


# instance fields
.field private list:Ljava/util/List;

.field private requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

.field private requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->list:Ljava/util/List;

    .line 42
    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    .line 43
    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .line 45
    return-void
.end method

.method private generateRequest(Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/security/SecureRandom;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    .locals 17
    .param p1, "signingAlgorithm"    # Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;
    .param p2, "key"    # Ljava/security/PrivateKey;
    .param p3, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p4, "provider"    # Ljava/lang/String;
    .param p5, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;,
            Ljava/security/NoSuchProviderException;
        }
    .end annotation

    .prologue
    .line 127
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->list:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 129
    .local v7, "it":Ljava/util/Iterator;
    new-instance v8, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v8}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 131
    .local v8, "requests":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 135
    :try_start_0
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;

    invoke-virtual {v14}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;->toRequest()Lcom/sec/android/org/bouncycastle/asn1/ocsp/Request;

    move-result-object v14

    invoke-virtual {v8, v14}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 137
    :catch_0
    move-exception v5

    .line 139
    .local v5, "e":Ljava/lang/Exception;
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v15, "exception creating Request"

    invoke-direct {v14, v15, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v14

    .line 143
    .end local v5    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v12, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    new-instance v15, Lcom/android/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v15, v8}, Lcom/android/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-direct {v12, v14, v15, v0}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;-><init>(Lcom/android/org/bouncycastle/asn1/x509/GeneralName;Lcom/android/org/bouncycastle/asn1/ASN1Sequence;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V

    .line 145
    .local v12, "tbsReq":Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;
    const/4 v9, 0x0

    .line 146
    .local v9, "sig":Ljava/security/Signature;
    const/4 v11, 0x0

    .line 148
    .local v11, "signature":Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;
    if-eqz p1, :cond_4

    .line 150
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    if-nez v14, :cond_1

    .line 152
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v15, "requestorName must be specified if request is signed."

    invoke-direct {v14, v15}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 157
    :cond_1
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p4

    invoke-static {v14, v0}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPUtil;->createSignatureInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Signature;

    move-result-object v9

    .line 158
    if-eqz p5, :cond_2

    .line 160
    move-object/from16 v0, p2

    move-object/from16 v1, p5

    invoke-virtual {v9, v0, v1}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;Ljava/security/SecureRandom;)V
    :try_end_1
    .catch Ljava/security/NoSuchProviderException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_2

    .line 177
    :goto_1
    const/4 v4, 0x0

    .line 181
    .local v4, "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    :try_start_2
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 182
    .local v3, "bOut":Ljava/io/ByteArrayOutputStream;
    new-instance v2, Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;

    invoke-direct {v2, v3}, Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 184
    .local v2, "aOut":Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;
    invoke-virtual {v2, v12}, Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;->writeObject(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 186
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/security/Signature;->update([B)V

    .line 188
    new-instance v4, Lcom/android/org/bouncycastle/asn1/DERBitString;

    .end local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    invoke-virtual {v9}, Ljava/security/Signature;->sign()[B

    move-result-object v14

    invoke-direct {v4, v14}, Lcom/android/org/bouncycastle/asn1/DERBitString;-><init>([B)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    .line 195
    .restart local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    new-instance v10, Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;

    sget-object v14, Lcom/android/org/bouncycastle/asn1/DERNull;->INSTANCE:Lcom/android/org/bouncycastle/asn1/DERNull;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v14}, Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    .line 197
    .local v10, "sigAlgId":Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    if-eqz p3, :cond_5

    move-object/from16 v0, p3

    array-length v14, v0

    if-lez v14, :cond_5

    .line 199
    new-instance v13, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v13}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    .line 202
    .local v13, "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_2
    :try_start_3
    move-object/from16 v0, p3

    array-length v14, v0

    if-eq v6, v14, :cond_3

    .line 204
    new-instance v15, Lcom/android/org/bouncycastle/asn1/x509/X509CertificateStructure;

    aget-object v14, p3, v6

    invoke-virtual {v14}, Ljava/security/cert/X509Certificate;->getEncoded()[B

    move-result-object v14

    invoke-static {v14}, Lcom/android/org/bouncycastle/asn1/ASN1Primitive;->fromByteArray([B)Lcom/android/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v14

    check-cast v14, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    invoke-direct {v15, v14}, Lcom/android/org/bouncycastle/asn1/x509/X509CertificateStructure;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    invoke-virtual {v13, v15}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3 .. :try_end_3} :catch_5

    .line 202
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 164
    .end local v2    # "aOut":Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;
    .end local v3    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    .end local v6    # "i":I
    .end local v10    # "sigAlgId":Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .end local v13    # "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    :cond_2
    :try_start_4
    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Ljava/security/Signature;->initSign(Ljava/security/PrivateKey;)V
    :try_end_4
    .catch Ljava/security/NoSuchProviderException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    .line 167
    :catch_1
    move-exception v5

    .line 170
    .local v5, "e":Ljava/security/NoSuchProviderException;
    throw v5

    .line 172
    .end local v5    # "e":Ljava/security/NoSuchProviderException;
    :catch_2
    move-exception v5

    .line 174
    .local v5, "e":Ljava/security/GeneralSecurityException;
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "exception creating signature: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v14

    .line 190
    .end local v5    # "e":Ljava/security/GeneralSecurityException;
    :catch_3
    move-exception v5

    .line 192
    .local v5, "e":Ljava/lang/Exception;
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "exception processing TBSRequest: "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v14

    .line 208
    .end local v5    # "e":Ljava/lang/Exception;
    .restart local v2    # "aOut":Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;
    .restart local v3    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    .restart local v6    # "i":I
    .restart local v10    # "sigAlgId":Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .restart local v13    # "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    :catch_4
    move-exception v5

    .line 210
    .local v5, "e":Ljava/io/IOException;
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v15, "error processing certs"

    invoke-direct {v14, v15, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v14

    .line 212
    .end local v5    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v5

    .line 214
    .local v5, "e":Ljava/security/cert/CertificateEncodingException;
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v15, "error encoding certs"

    invoke-direct {v14, v15, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v14

    .line 217
    .end local v5    # "e":Ljava/security/cert/CertificateEncodingException;
    :cond_3
    new-instance v11, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;

    .end local v11    # "signature":Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;
    new-instance v14, Lcom/android/org/bouncycastle/asn1/DERSequence;

    invoke-direct {v14, v13}, Lcom/android/org/bouncycastle/asn1/DERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    invoke-direct {v11, v10, v4, v14}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;-><init>(Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/org/bouncycastle/asn1/DERBitString;Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    .line 225
    .end local v2    # "aOut":Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;
    .end local v3    # "bOut":Ljava/io/ByteArrayOutputStream;
    .end local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    .end local v6    # "i":I
    .end local v10    # "sigAlgId":Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    .end local v13    # "v":Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;
    .restart local v11    # "signature":Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;
    :cond_4
    :goto_3
    new-instance v14, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;

    new-instance v15, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPRequest;

    invoke-direct {v15, v12, v11}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPRequest;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/TBSRequest;Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;)V

    invoke-direct {v14, v15}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;-><init>(Lcom/sec/android/org/bouncycastle/asn1/ocsp/OCSPRequest;)V

    return-object v14

    .line 221
    .restart local v2    # "aOut":Lcom/android/org/bouncycastle/asn1/ASN1OutputStream;
    .restart local v3    # "bOut":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "bitSig":Lcom/android/org/bouncycastle/asn1/DERBitString;
    .restart local v10    # "sigAlgId":Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;
    :cond_5
    new-instance v11, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;

    .end local v11    # "signature":Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;
    invoke-direct {v11, v10, v4}, Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;-><init>(Lcom/android/org/bouncycastle/asn1/x509/AlgorithmIdentifier;Lcom/android/org/bouncycastle/asn1/DERBitString;)V

    .restart local v11    # "signature":Lcom/sec/android/org/bouncycastle/asn1/ocsp/Signature;
    goto :goto_3
.end method


# virtual methods
.method public addRequest(Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;)V
    .locals 3
    .param p1, "certId"    # Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;-><init>(Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public addRequest(Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 2
    .param p1, "certId"    # Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    .param p2, "singleRequestExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->list:Ljava/util/List;

    new-instance v1, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator$RequestObject;-><init>(Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    return-void
.end method

.method public generate()Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 239
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->generateRequest(Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/security/SecureRandom;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 241
    :catch_0
    move-exception v6

    .line 246
    .local v6, "e":Ljava/security/NoSuchProviderException;
    new-instance v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no provider! - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v0
.end method

.method public generate(Ljava/lang/String;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    .locals 6
    .param p1, "signingAlgorithm"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/security/PrivateKey;
    .param p3, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p4, "provider"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 257
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->generate(Ljava/lang/String;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/security/SecureRandom;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;

    move-result-object v0

    return-object v0
.end method

.method public generate(Ljava/lang/String;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/security/SecureRandom;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    .locals 7
    .param p1, "signingAlgorithm"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/security/PrivateKey;
    .param p3, "chain"    # [Ljava/security/cert/X509Certificate;
    .param p4, "provider"    # Ljava/lang/String;
    .param p5, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;,
            Ljava/security/NoSuchProviderException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 268
    if-nez p1, :cond_0

    .line 270
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "no signing algorithm specified"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPUtil;->getAlgorithmOID(Ljava/lang/String;)Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;

    move-result-object v1

    .local v1, "oid":Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;
    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 277
    invoke-direct/range {v0 .. v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->generateRequest(Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;Ljava/security/PrivateKey;[Ljava/security/cert/X509Certificate;Ljava/lang/String;Ljava/security/SecureRandom;)Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 279
    .end local v1    # "oid":Lcom/android/org/bouncycastle/asn1/DERObjectIdentifier;
    :catch_0
    move-exception v6

    .line 281
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unknown signing algorithm specified: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSignatureAlgNames()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 292
    invoke-static {}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPUtil;->getAlgNames()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public setRequestExtensions(Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;)V
    .locals 0
    .param p1, "requestExtensions"    # Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestExtensions:Lcom/android/org/bouncycastle/asn1/x509/X509Extensions;

    .line 117
    return-void
.end method

.method public setRequestorName(Lcom/android/org/bouncycastle/asn1/x509/GeneralName;)V
    .locals 0
    .param p1, "requestorName"    # Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    .line 111
    return-void
.end method

.method public setRequestorName(Ljavax/security/auth/x500/X500Principal;)V
    .locals 5
    .param p1, "requestorName"    # Ljavax/security/auth/x500/X500Principal;

    .prologue
    .line 99
    :try_start_0
    new-instance v1, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    const/4 v2, 0x4

    new-instance v3, Lcom/android/org/bouncycastle/jce/X509Principal;

    invoke-virtual {p1}, Ljavax/security/auth/x500/X500Principal;->getEncoded()[B

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/android/org/bouncycastle/jce/X509Principal;-><init>([B)V

    invoke-direct {v1, v2, v3}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;-><init>(ILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    iput-object v1, p0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->requestorName:Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot encode principal: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
