.class public abstract Lcom/sec/android/security/ocsp/OCSPClient;
.super Ljava/lang/Object;
.source "OCSPClient.java"


# static fields
.field private static final OCSP_SIGNING_KEY_USAGE:Ljava/lang/String; = "1.3.6.1.5.5.7.3.9"


# instance fields
.field private defaultResponderURI:Ljava/lang/String;

.field private issuer:Ljava/security/cert/X509Certificate;


# direct methods
.method public constructor <init>(Ljava/security/cert/X509Certificate;)V
    .locals 1
    .param p1, "issuer"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-nez p1, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 46
    :cond_0
    iput-object p1, p0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    .line 47
    return-void
.end method

.method public constructor <init>(Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .param p1, "issuer"    # Ljava/security/cert/X509Certificate;
    .param p2, "defaultResponderURI"    # Ljava/lang/String;

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/sec/android/security/ocsp/OCSPClient;-><init>(Ljava/security/cert/X509Certificate;)V

    .line 52
    if-nez p2, :cond_0

    .line 53
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 54
    :cond_0
    iput-object p2, p0, Lcom/sec/android/security/ocsp/OCSPClient;->defaultResponderURI:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private generateOCSPRequest(Ljava/util/Collection;Ljava/lang/String;)[B
    .locals 9
    .param p2, "responderURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;",
            "Ljava/lang/String;",
            ")[B"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "certificates":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/security/cert/X509Certificate;>;"
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;

    invoke-direct {v4}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;-><init>()V

    .line 86
    .local v4, "requestGenerator":Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/X509Certificate;

    .line 88
    .local v0, "certificate":Ljava/security/cert/X509Certificate;
    iget-object v6, p0, Lcom/sec/android/security/ocsp/OCSPClient;->defaultResponderURI:Ljava/lang/String;

    if-nez v6, :cond_1

    .line 92
    :try_start_0
    invoke-static {v0}, Lcom/sec/android/security/ocsp/OCSPClient;->getResponderURI(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v5

    .line 93
    .local v5, "uri":Ljava/lang/String;
    invoke-virtual {p2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 94
    new-instance v6, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v7, "Responder URIs do not match"

    invoke-direct {v6, v7}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    .end local v5    # "uri":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 98
    .local v2, "e":Ljava/io/IOException;
    new-instance v6, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v7, "No responder URI"

    invoke-direct {v6, v7, v2}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v6

    .line 101
    .end local v2    # "e":Ljava/io/IOException;
    .restart local v5    # "uri":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    invoke-virtual {v7}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 102
    new-instance v6, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v7, "Invalid issuer for certificate"

    invoke-direct {v6, v7}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 105
    .end local v5    # "uri":Ljava/lang/String;
    :cond_1
    new-instance v1, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    const-string v6, "1.3.14.3.2.26"

    iget-object v7, p0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v8

    invoke-direct {v1, v6, v7, v8}, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/math/BigInteger;)V

    .line 106
    .local v1, "certificateId":Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    invoke-virtual {v4, v1}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->addRequest(Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;)V

    goto :goto_0

    .line 111
    .end local v0    # "certificate":Ljava/security/cert/X509Certificate;
    .end local v1    # "certificateId":Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    :cond_2
    :try_start_1
    invoke-virtual {v4}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReqGenerator;->generate()Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPReq;->getEncoded()[B
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v6

    return-object v6

    .line 113
    :catch_1
    move-exception v2

    .line 114
    .restart local v2    # "e":Ljava/io/IOException;
    new-instance v6, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v7, "Error generating request"

    invoke-direct {v6, v7, v2}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v6
.end method

.method private static getResponderURI(Ljava/security/cert/X509Certificate;)Ljava/lang/String;
    .locals 10
    .param p0, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 59
    sget-object v8, Lcom/android/org/bouncycastle/asn1/x509/X509Extension;->authorityInfoAccess:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v8}, Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Ljava/security/cert/X509Certificate;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v3

    .line 60
    .local v3, "authorityInfoAccessEncoded":[B
    if-nez v3, :cond_1

    .line 79
    :cond_0
    :goto_0
    return-object v7

    .line 63
    :cond_1
    invoke-static {v3}, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1OctetString;

    move-result-object v8

    invoke-virtual {v8}, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v8

    invoke-static {v8}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v8

    invoke-static {v8}, Lcom/sec/android/org/bouncycastle/asn1/x509/AuthorityInformationAccess;->getInstance(Ljava/lang/Object;)Lcom/sec/android/org/bouncycastle/asn1/x509/AuthorityInformationAccess;

    move-result-object v2

    .line 64
    .local v2, "authorityInfoAccess":Lcom/sec/android/org/bouncycastle/asn1/x509/AuthorityInformationAccess;
    if-eqz v2, :cond_0

    .line 67
    invoke-virtual {v2}, Lcom/sec/android/org/bouncycastle/asn1/x509/AuthorityInformationAccess;->getAccessDescriptions()[Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;

    move-result-object v1

    .local v1, "arr$":[Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;
    array-length v5, v1

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v5, :cond_0

    aget-object v0, v1, v4

    .line 69
    .local v0, "ad":Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;
    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;->getAccessMethod()Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v8

    sget-object v9, Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;->id_ad_ocsp:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v8, v9}, Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 71
    invoke-virtual {v0}, Lcom/sec/android/org/bouncycastle/asn1/x509/AccessDescription;->getAccessLocation()Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v6

    .line 72
    .local v6, "name":Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    invoke-virtual {v6}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;->getTagNo()I

    move-result v8

    const/4 v9, 0x6

    if-ne v8, v9, :cond_2

    .line 74
    invoke-virtual {v6}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;->getName()Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v7

    check-cast v7, Lcom/android/org/bouncycastle/asn1/DERIA5String;

    invoke-virtual {v7}, Lcom/android/org/bouncycastle/asn1/DERIA5String;->getString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 67
    .end local v6    # "name":Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private processResponse([BLjava/util/Collection;)Lcom/sec/android/security/ocsp/ResponseInfo;
    .locals 27
    .param p1, "responseData"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/Collection",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)",
            "Lcom/sec/android/security/ocsp/ResponseInfo;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 123
    .local p2, "requestedCertificates":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/security/cert/X509Certificate;>;"
    :try_start_0
    new-instance v17, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;-><init>([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    .local v17, "response":Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;->getStatus()I

    move-result v19

    .line 131
    .local v19, "responseStatus":I
    packed-switch v19, :pswitch_data_0

    .line 146
    :pswitch_0
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Unknown OCSPResponse status code!"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 125
    .end local v17    # "response":Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;
    .end local v19    # "responseStatus":I
    :catch_0
    move-exception v8

    .line 127
    .local v8, "e":Ljava/io/IOException;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Unable to parse response"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 134
    .end local v8    # "e":Ljava/io/IOException;
    .restart local v17    # "response":Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;
    .restart local v19    # "responseStatus":I
    :pswitch_1
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "An internal error occured in the OCSP Server"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 136
    :pswitch_2
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "The server did not understand the request"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 138
    :pswitch_3
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Server accepts only signed requests"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 140
    :pswitch_4
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Server is too busy"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 142
    :pswitch_5
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Authorization failed"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 152
    :pswitch_6
    :try_start_1
    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPResp;->getResponseObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    :try_end_1
    .catch Lcom/sec/android/org/bouncycastle/ocsp/OCSPException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 163
    .local v4, "basicResponse":Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    if-nez v4, :cond_0

    .line 164
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Unrecognized response type"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 154
    .end local v4    # "basicResponse":Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    :catch_1
    move-exception v8

    .line 156
    .local v8, "e":Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
    throw v8

    .line 158
    .end local v8    # "e":Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
    :catch_2
    move-exception v8

    .line 160
    .local v8, "e":Ljava/lang/Exception;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Unrecognized response type"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 168
    .end local v8    # "e":Ljava/lang/Exception;
    .restart local v4    # "basicResponse":Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;
    :cond_0
    :try_start_2
    const-string v24, "BC"

    move-object/from16 v0, v24

    invoke-virtual {v4, v0}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->getCerts(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    :try_end_2
    .catch Ljava/security/NoSuchProviderException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v18

    .line 175
    .local v18, "responseCertificates":[Ljava/security/cert/X509Certificate;
    if-eqz v18, :cond_1

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v24, v0

    if-nez v24, :cond_2

    .line 176
    :cond_1
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 170
    .end local v18    # "responseCertificates":[Ljava/security/cert/X509Certificate;
    :catch_3
    move-exception v8

    .line 172
    .local v8, "e":Ljava/security/NoSuchProviderException;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "This code requires BouncyCastle provider"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 178
    .end local v8    # "e":Ljava/security/NoSuchProviderException;
    .restart local v18    # "responseCertificates":[Ljava/security/cert/X509Certificate;
    :cond_2
    new-instance v13, Ljava/util/Date;

    invoke-direct {v13}, Ljava/util/Date;-><init>()V

    .line 179
    .local v13, "now":Ljava/util/Date;
    invoke-virtual {v4}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->getProducedAt()Ljava/util/Date;

    move-result-object v14

    .line 180
    .local v14, "producedAt":Ljava/util/Date;
    invoke-virtual {v13, v14}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 181
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Invalid time in response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 183
    :cond_3
    invoke-virtual {v4}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->getResponderId()Lcom/sec/android/org/bouncycastle/ocsp/RespID;

    move-result-object v16

    .line 184
    .local v16, "responderId":Lcom/sec/android/org/bouncycastle/ocsp/RespID;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/RespID;

    const/16 v25, 0x0

    aget-object v25, v18, v25

    invoke-virtual/range {v25 .. v25}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/RespID;-><init>(Ljava/security/PublicKey;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/org/bouncycastle/ocsp/RespID;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_4

    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/RespID;

    const/16 v25, 0x0

    aget-object v25, v18, v25

    invoke-virtual/range {v25 .. v25}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v25

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/RespID;-><init>(Ljavax/security/auth/x500/X500Principal;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/sec/android/org/bouncycastle/ocsp/RespID;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_4

    .line 187
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Responder ID does not match the responder\'s certificate"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 190
    :cond_4
    const/4 v15, 0x0

    .line 191
    .local v15, "responderCert":Ljava/security/cert/X509Certificate;
    const/16 v24, 0x0

    aget-object v24, v18, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    move-object/from16 v25, v0

    invoke-virtual/range {v24 .. v25}, Ljava/security/cert/X509Certificate;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_7

    .line 193
    const/16 v24, 0x0

    aget-object v24, v18, v24

    invoke-virtual/range {v24 .. v24}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljavax/security/auth/x500/X500Principal;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_5

    .line 194
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 198
    :cond_5
    const/16 v24, 0x0

    :try_start_3
    aget-object v24, v18, v24

    invoke-virtual/range {v24 .. v24}, Ljava/security/cert/X509Certificate;->checkValidity()V
    :try_end_3
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_5

    .line 207
    const/16 v24, 0x0

    :try_start_4
    aget-object v24, v18, v24

    invoke-virtual/range {v24 .. v24}, Ljava/security/cert/X509Certificate;->getExtendedKeyUsage()Ljava/util/List;

    move-result-object v24

    const-string v25, "1.3.6.1.5.5.7.3.9"

    invoke-interface/range {v24 .. v25}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_6

    .line 208
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24
    :try_end_4
    .catch Ljava/security/cert/CertificateParsingException; {:try_start_4 .. :try_end_4} :catch_4

    .line 210
    :catch_4
    move-exception v8

    .line 212
    .local v8, "e":Ljava/security/cert/CertificateParsingException;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 200
    .end local v8    # "e":Ljava/security/cert/CertificateParsingException;
    :catch_5
    move-exception v8

    .line 202
    .local v8, "e":Ljava/security/cert/CertificateException;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 217
    .end local v8    # "e":Ljava/security/cert/CertificateException;
    :cond_6
    const/16 v24, 0x0

    :try_start_5
    aget-object v24, v18, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v25

    const-string v26, "BC"

    invoke-virtual/range {v24 .. v26}, Ljava/security/cert/X509Certificate;->verify(Ljava/security/PublicKey;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_7

    .line 223
    const/16 v24, 0x0

    aget-object v15, v18, v24

    .line 228
    :cond_7
    const/16 v24, 0x0

    :try_start_6
    aget-object v24, v18, v24

    invoke-virtual/range {v24 .. v24}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v24

    const-string v25, "BC"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->verify(Ljava/security/PublicKey;Ljava/lang/String;)Z

    move-result v24

    if-nez v24, :cond_8

    .line 229
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24
    :try_end_6
    .catch Ljava/security/NoSuchProviderException; {:try_start_6 .. :try_end_6} :catch_6

    .line 231
    :catch_6
    move-exception v8

    .line 233
    .local v8, "e":Ljava/security/NoSuchProviderException;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "This code requires BouncyCastle provider"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 219
    .end local v8    # "e":Ljava/security/NoSuchProviderException;
    :catch_7
    move-exception v8

    .line 221
    .local v8, "e":Ljava/lang/Exception;
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Cannot validate the response"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-direct {v0, v1, v8}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v24

    .line 236
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_8
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 237
    .local v22, "stats":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/security/cert/X509Certificate;Lcom/sec/android/security/ocsp/CertificateStatus;>;"
    invoke-virtual {v4}, Lcom/sec/android/org/bouncycastle/ocsp/BasicOCSPResp;->getResponses()[Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;

    move-result-object v20

    .line 238
    .local v20, "responses":[Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v24

    if-eqz v24, :cond_f

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/security/cert/X509Certificate;

    .line 240
    .local v5, "certificate":Ljava/security/cert/X509Certificate;
    new-instance v6, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    const-string v24, "1.3.14.3.2.26"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/security/ocsp/OCSPClient;->issuer:Ljava/security/cert/X509Certificate;

    move-object/from16 v25, v0

    invoke-virtual {v5}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v26

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    invoke-direct {v6, v0, v1, v2}, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;-><init>(Ljava/lang/String;Ljava/security/cert/X509Certificate;Ljava/math/BigInteger;)V

    .line 241
    .local v6, "certificateId":Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    move-object/from16 v3, v20

    .local v3, "arr$":[Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;
    array-length v11, v3

    .local v11, "len$":I
    const/4 v10, 0x0

    .local v10, "i$":I
    :goto_0
    if-ge v10, v11, :cond_9

    aget-object v21, v3, v10

    .line 243
    .local v21, "singleResponse":Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;->getCertID()Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 245
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;->getThisUpdate()Ljava/util/Date;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v24

    if-eqz v24, :cond_a

    .line 246
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Invalid time in response"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 248
    :cond_a
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;->getNextUpdate()Ljava/util/Date;

    move-result-object v12

    .line 249
    .local v12, "nextUpdate":Ljava/util/Date;
    if-eqz v12, :cond_b

    .line 251
    invoke-virtual {v13, v12}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 252
    new-instance v24, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v25, "Response is too old"

    invoke-direct/range {v24 .. v25}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v24

    .line 255
    :cond_b
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;->getCertStatus()Ljava/lang/Object;

    move-result-object v23

    .line 256
    .local v23, "status":Ljava/lang/Object;
    sget-object v7, Lcom/sec/android/security/ocsp/CertificateStatus;->UNKNOWN:Lcom/sec/android/security/ocsp/CertificateStatus;

    .line 257
    .local v7, "certificateStatus":Lcom/sec/android/security/ocsp/CertificateStatus;
    if-nez v23, :cond_c

    .line 258
    sget-object v7, Lcom/sec/android/security/ocsp/CertificateStatus;->GOOD:Lcom/sec/android/security/ocsp/CertificateStatus;

    .line 259
    :cond_c
    move-object/from16 v0, v23

    instance-of v0, v0, Lcom/sec/android/org/bouncycastle/ocsp/RevokedStatus;

    move/from16 v24, v0

    if-eqz v24, :cond_d

    .line 260
    sget-object v7, Lcom/sec/android/security/ocsp/CertificateStatus;->REVOKED:Lcom/sec/android/security/ocsp/CertificateStatus;

    .line 262
    :cond_d
    move-object/from16 v0, v22

    invoke-virtual {v0, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    .end local v7    # "certificateStatus":Lcom/sec/android/security/ocsp/CertificateStatus;
    .end local v12    # "nextUpdate":Ljava/util/Date;
    .end local v23    # "status":Ljava/lang/Object;
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 267
    .end local v3    # "arr$":[Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;
    .end local v5    # "certificate":Ljava/security/cert/X509Certificate;
    .end local v6    # "certificateId":Lcom/sec/android/org/bouncycastle/ocsp/CertificateID;
    .end local v10    # "i$":I
    .end local v11    # "len$":I
    .end local v21    # "singleResponse":Lcom/sec/android/org/bouncycastle/ocsp/SingleResp;
    :cond_f
    new-instance v24, Lcom/sec/android/security/ocsp/ResponseInfo;

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    invoke-direct {v0, v1, v14, v15}, Lcom/sec/android/security/ocsp/ResponseInfo;-><init>(Ljava/util/Map;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V

    return-object v24

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method public checkCertificate(Ljava/security/cert/X509Certificate;)Lcom/sec/android/security/ocsp/ResponseInfo;
    .locals 2
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    new-instance v0, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v1, "No certificates to check"

    invoke-direct {v0, v1}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/security/ocsp/OCSPClient;->checkCertificates(Ljava/util/Collection;)Lcom/sec/android/security/ocsp/ResponseInfo;

    move-result-object v0

    return-object v0
.end method

.method public checkCertificates(Ljava/util/Collection;)Lcom/sec/android/security/ocsp/ResponseInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/security/cert/X509Certificate;",
            ">;)",
            "Lcom/sec/android/security/ocsp/ResponseInfo;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
        }
    .end annotation

    .prologue
    .line 280
    .local p1, "certificates":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/security/cert/X509Certificate;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    if-nez v4, :cond_1

    .line 281
    :cond_0
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "No certificates to check"

    invoke-direct {v4, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 283
    :cond_1
    iget-object v3, p0, Lcom/sec/android/security/ocsp/OCSPClient;->defaultResponderURI:Ljava/lang/String;

    .line 284
    .local v3, "uri":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 288
    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/security/cert/X509Certificate;

    invoke-static {v4}, Lcom/sec/android/security/ocsp/OCSPClient;->getResponderURI(Ljava/security/cert/X509Certificate;)Ljava/lang/String;

    move-result-object v3

    .line 289
    if-nez v3, :cond_2

    .line 290
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "No responder URI"

    invoke-direct {v4, v5}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Ljava/io/IOException;
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "No responder URI"

    invoke-direct {v4, v5, v0}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4

    .line 298
    .end local v0    # "e":Ljava/io/IOException;
    :cond_2
    invoke-direct {p0, p1, v3}, Lcom/sec/android/security/ocsp/OCSPClient;->generateOCSPRequest(Ljava/util/Collection;Ljava/lang/String;)[B

    move-result-object v1

    .line 302
    .local v1, "request":[B
    :try_start_1
    invoke-virtual {p0, v3, v1}, Lcom/sec/android/security/ocsp/OCSPClient;->queryResponder(Ljava/lang/String;[B)[B

    move-result-object v2

    .line 304
    .local v2, "response":[B
    invoke-direct {p0, v2, p1}, Lcom/sec/android/security/ocsp/OCSPClient;->processResponse([BLjava/util/Collection;)Lcom/sec/android/security/ocsp/ResponseInfo;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    return-object v4

    .line 306
    .end local v2    # "response":[B
    :catch_1
    move-exception v0

    .line 308
    .restart local v0    # "e":Ljava/io/IOException;
    new-instance v4, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v5, "Error communicating with the responder"

    invoke-direct {v4, v5, v0}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v4
.end method

.method protected abstract queryResponder(Ljava/lang/String;[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
