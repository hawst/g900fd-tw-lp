.class public Lcom/sec/android/security/ocsp/ResponseInfo;
.super Ljava/lang/Object;
.source "ResponseInfo.java"


# instance fields
.field private producedAt:Ljava/util/Date;

.field private requestedCertInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/security/cert/X509Certificate;",
            "Lcom/sec/android/security/ocsp/CertificateStatus;",
            ">;"
        }
    .end annotation
.end field

.field private responderCert:Ljava/security/cert/X509Certificate;


# direct methods
.method constructor <init>(Ljava/util/Map;Ljava/util/Date;Ljava/security/cert/X509Certificate;)V
    .locals 0
    .param p2, "producedAt"    # Ljava/util/Date;
    .param p3, "responderCert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/security/cert/X509Certificate;",
            "Lcom/sec/android/security/ocsp/CertificateStatus;",
            ">;",
            "Ljava/util/Date;",
            "Ljava/security/cert/X509Certificate;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    .local p1, "requestedCertInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/security/cert/X509Certificate;Lcom/sec/android/security/ocsp/CertificateStatus;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->requestedCertInfo:Ljava/util/Map;

    .line 20
    iput-object p2, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->producedAt:Ljava/util/Date;

    .line 21
    iput-object p3, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->responderCert:Ljava/security/cert/X509Certificate;

    .line 22
    return-void
.end method


# virtual methods
.method public getRequestedCertInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/security/cert/X509Certificate;",
            "Lcom/sec/android/security/ocsp/CertificateStatus;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->requestedCertInfo:Ljava/util/Map;

    return-object v0
.end method

.method public getResponderCert()Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->responderCert:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public getResponseGenerationTime()Ljava/util/Date;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/security/ocsp/ResponseInfo;->producedAt:Ljava/util/Date;

    return-object v0
.end method
