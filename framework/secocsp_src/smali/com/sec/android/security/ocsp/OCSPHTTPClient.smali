.class public Lcom/sec/android/security/ocsp/OCSPHTTPClient;
.super Lcom/sec/android/security/ocsp/OCSPClient;
.source "OCSPHTTPClient.java"


# instance fields
.field private final REQUEST_CONTENT_TYPE:Ljava/lang/String;

.field private final RESPONSE_CONTENT_TYPE:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/security/cert/X509Certificate;)V
    .locals 1
    .param p1, "issuer"    # Ljava/security/cert/X509Certificate;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/android/security/ocsp/OCSPClient;-><init>(Ljava/security/cert/X509Certificate;)V

    .line 17
    const-string v0, "application/ocsp-request"

    iput-object v0, p0, Lcom/sec/android/security/ocsp/OCSPHTTPClient;->REQUEST_CONTENT_TYPE:Ljava/lang/String;

    .line 18
    const-string v0, "application/ocsp-response"

    iput-object v0, p0, Lcom/sec/android/security/ocsp/OCSPHTTPClient;->RESPONSE_CONTENT_TYPE:Ljava/lang/String;

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/security/cert/X509Certificate;Ljava/lang/String;)V
    .locals 1
    .param p1, "issuer"    # Ljava/security/cert/X509Certificate;
    .param p2, "defaultResponderURL"    # Ljava/lang/String;

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lcom/sec/android/security/ocsp/OCSPClient;-><init>(Ljava/security/cert/X509Certificate;Ljava/lang/String;)V

    .line 17
    const-string v0, "application/ocsp-request"

    iput-object v0, p0, Lcom/sec/android/security/ocsp/OCSPHTTPClient;->REQUEST_CONTENT_TYPE:Ljava/lang/String;

    .line 18
    const-string v0, "application/ocsp-response"

    iput-object v0, p0, Lcom/sec/android/security/ocsp/OCSPHTTPClient;->RESPONSE_CONTENT_TYPE:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method protected queryResponder(Ljava/lang/String;[B)[B
    .locals 11
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "request"    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 43
    const-string v8, "http"

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 44
    new-instance v8, Ljava/io/IOException;

    const-string v9, "The protocol should be http or https"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 46
    :cond_0
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 47
    .local v5, "responderUrl":Ljava/net/URL;
    invoke-virtual {v5}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 48
    .local v2, "connection":Ljava/net/HttpURLConnection;
    array-length v8, p2

    invoke-virtual {v2, v8}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 49
    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setAllowUserInteraction(Z)V

    .line 50
    invoke-virtual {v2, v10}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 51
    invoke-virtual {v2, v10}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 52
    invoke-virtual {v2, v9}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 53
    invoke-static {v9}, Ljava/net/HttpURLConnection;->setFollowRedirects(Z)V

    .line 54
    const-string v8, "POST"

    invoke-virtual {v2, v8}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 55
    const-string v8, "Content-Length"

    array-length v9, p2

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v8, "Content-Type"

    const-string v9, "application/ocsp-request"

    invoke-virtual {v2, v8, v9}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :try_start_0
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 61
    .local v4, "outputStream":Ljava/io/OutputStream;
    invoke-virtual {v4, p2}, Ljava/io/OutputStream;->write([B)V

    .line 62
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 64
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v8

    const/16 v9, 0xc8

    if-eq v8, v9, :cond_1

    .line 66
    new-instance v8, Ljava/io/IOException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Request was not accepted! Returned code: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    .end local v4    # "outputStream":Ljava/io/OutputStream;
    :catchall_0
    move-exception v8

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v8

    .line 69
    .restart local v4    # "outputStream":Ljava/io/OutputStream;
    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v8

    const-string v9, "application/ocsp-response"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 71
    :cond_2
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Wrong response type"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 74
    :cond_3
    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 75
    .local v3, "inputStream":Ljava/io/BufferedInputStream;
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v7

    .line 76
    .local v7, "responseLength":I
    new-array v6, v7, [B

    .line 77
    .local v6, "response":[B
    const/4 v0, 0x0

    .line 78
    .local v0, "bytesRead":I
    const/4 v1, 0x0

    .line 79
    .local v1, "bytesReadTotal":I
    :goto_0
    if-ge v1, v7, :cond_5

    .line 81
    sub-int v8, v7, v1

    invoke-virtual {v3, v6, v1, v8}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v0

    .line 82
    if-gez v0, :cond_4

    .line 83
    new-instance v8, Ljava/io/IOException;

    const-string v9, "Can not retrieve the response"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 84
    :cond_4
    add-int/2addr v1, v0

    goto :goto_0

    .line 91
    :cond_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    return-object v6
.end method
