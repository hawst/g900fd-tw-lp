.class public Lcom/sec/android/security/crl/CRLCache;
.super Ljava/lang/Object;
.source "CRLCache.java"


# static fields
.field private static final CACHE_SIZE:I = 0x40

.field private static final crlDownloaders:[Lcom/sec/android/security/crl/CRLDownloader;

.field static final httpDownloader:Lcom/sec/android/security/crl/CRLDownloader;


# instance fields
.field cache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/security/cert/X509CRL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 29
    new-instance v0, Lcom/sec/android/security/crl/CRLCache$1;

    invoke-direct {v0}, Lcom/sec/android/security/crl/CRLCache$1;-><init>()V

    sput-object v0, Lcom/sec/android/security/crl/CRLCache;->httpDownloader:Lcom/sec/android/security/crl/CRLDownloader;

    .line 66
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/sec/android/security/crl/CRLDownloader;

    const/4 v1, 0x0

    sget-object v2, Lcom/sec/android/security/crl/CRLCache;->httpDownloader:Lcom/sec/android/security/crl/CRLDownloader;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/security/crl/CRLCache;->crlDownloaders:[Lcom/sec/android/security/crl/CRLDownloader;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/security/crl/CRLCache;->cache:Ljava/util/HashMap;

    .line 70
    return-void
.end method

.method private static getCrlDistributionPoints(Ljava/security/cert/X509Extension;)Ljava/util/Collection;
    .locals 15
    .param p0, "cert"    # Ljava/security/cert/X509Extension;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/cert/X509Extension;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    const-string v13, "2.5.29.31"

    invoke-interface {p0, v13}, Ljava/security/cert/X509Extension;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object v2

    .line 74
    .local v2, "crlDistributionPointsEncoded":[B
    if-nez v2, :cond_0

    .line 75
    new-instance v13, Ljava/io/IOException;

    const-string v14, "No distribution points in certificate"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 77
    :cond_0
    invoke-static {v2}, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;->fromByteArray([B)Lcom/android/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v13

    check-cast v13, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;

    check-cast v13, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v13}, Lcom/android/org/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v2

    .line 78
    invoke-static {v2}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->fromByteArray([B)Lcom/android/org/bouncycastle/asn1/ASN1Primitive;

    move-result-object v13

    invoke-static {v13}, Lcom/android/org/bouncycastle/asn1/x509/CRLDistPoint;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/x509/CRLDistPoint;

    move-result-object v3

    .line 80
    .local v3, "distPoints":Lcom/android/org/bouncycastle/asn1/x509/CRLDistPoint;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v12, "uris":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v3}, Lcom/android/org/bouncycastle/asn1/x509/CRLDistPoint;->getDistributionPoints()[Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;

    move-result-object v0

    .local v0, "arr$":[Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;
    array-length v8, v0

    .local v8, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    move v7, v6

    .end local v0    # "arr$":[Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;
    .end local v6    # "i$":I
    .end local v8    # "len$":I
    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_3

    aget-object v4, v0, v7

    .line 82
    .local v4, "dp":Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;
    invoke-virtual {v4}, Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;->getDistributionPoint()Lcom/android/org/bouncycastle/asn1/x509/DistributionPointName;

    move-result-object v5

    .line 83
    .local v5, "dpn":Lcom/android/org/bouncycastle/asn1/x509/DistributionPointName;
    if-eqz v5, :cond_2

    .line 84
    invoke-virtual {v5}, Lcom/android/org/bouncycastle/asn1/x509/DistributionPointName;->getType()I

    move-result v13

    if-nez v13, :cond_2

    .line 85
    invoke-virtual {v5}, Lcom/android/org/bouncycastle/asn1/x509/DistributionPointName;->getName()Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v11

    check-cast v11, Lcom/android/org/bouncycastle/asn1/x509/GeneralNames;

    .line 86
    .local v11, "names":Lcom/android/org/bouncycastle/asn1/x509/GeneralNames;
    invoke-virtual {v11}, Lcom/android/org/bouncycastle/asn1/x509/GeneralNames;->getNames()[Lcom/android/org/bouncycastle/asn1/x509/GeneralName;

    move-result-object v1

    .local v1, "arr$":[Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    array-length v9, v1

    .local v9, "len$":I
    const/4 v6, 0x0

    .end local v7    # "i$":I
    .restart local v6    # "i$":I
    :goto_1
    if-ge v6, v9, :cond_2

    aget-object v10, v1, v6

    .line 87
    .local v10, "n":Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    const/4 v13, 0x6

    invoke-virtual {v10}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;->getTagNo()I

    move-result v14

    if-ne v13, v14, :cond_1

    .line 88
    invoke-virtual {v10}, Lcom/android/org/bouncycastle/asn1/x509/GeneralName;->getName()Lcom/android/org/bouncycastle/asn1/ASN1Encodable;

    move-result-object v13

    check-cast v13, Lcom/android/org/bouncycastle/asn1/ASN1String;

    invoke-interface {v13}, Lcom/android/org/bouncycastle/asn1/ASN1String;->getString()Ljava/lang/String;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 81
    .end local v1    # "arr$":[Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    .end local v6    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "n":Lcom/android/org/bouncycastle/asn1/x509/GeneralName;
    .end local v11    # "names":Lcom/android/org/bouncycastle/asn1/x509/GeneralNames;
    :cond_2
    add-int/lit8 v6, v7, 0x1

    .restart local v6    # "i$":I
    move v7, v6

    .end local v6    # "i$":I
    .restart local v7    # "i$":I
    goto :goto_0

    .line 94
    .end local v4    # "dp":Lcom/android/org/bouncycastle/asn1/x509/DistributionPoint;
    .end local v5    # "dpn":Lcom/android/org/bouncycastle/asn1/x509/DistributionPointName;
    :cond_3
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_4

    .line 95
    new-instance v13, Ljava/io/IOException;

    const-string v14, "No distribution points in certificate"

    invoke-direct {v13, v14}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 97
    :cond_4
    return-object v12
.end method

.method private isCrlTooOld(Ljava/security/cert/X509CRL;)Z
    .locals 3
    .param p1, "crl"    # Ljava/security/cert/X509CRL;

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/security/cert/X509CRL;->getNextUpdate()Ljava/util/Date;

    move-result-object v0

    .line 134
    .local v0, "nextUpdate":Ljava/util/Date;
    if-nez v0, :cond_0

    .line 135
    const/4 v2, 0x1

    .line 138
    :goto_0
    return v2

    .line 137
    :cond_0
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 138
    .local v1, "now":Ljava/util/Date;
    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v2

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized get(Ljava/security/cert/X509Certificate;)Ljava/security/cert/X509CRL;
    .locals 10
    .param p1, "cert"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/sec/android/security/crl/CRLCache;->getCrlDistributionPoints(Ljava/security/cert/X509Extension;)Ljava/util/Collection;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 104
    .local v7, "uri":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/security/crl/CRLCache;->cache:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/security/cert/X509CRL;

    .line 105
    .local v1, "crl":Ljava/security/cert/X509CRL;
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/sec/android/security/crl/CRLCache;->isCrlTooOld(Ljava/security/cert/X509CRL;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 106
    :cond_1
    iget-object v8, p0, Lcom/sec/android/security/crl/CRLCache;->cache:Ljava/util/HashMap;

    invoke-virtual {v8, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/sec/android/security/crl/CRLCache;->crlDownloaders:[Lcom/sec/android/security/crl/CRLDownloader;

    .local v0, "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v3, v0, v5

    .line 108
    .local v3, "d":Lcom/sec/android/security/crl/CRLDownloader;
    invoke-interface {v3, v7}, Lcom/sec/android/security/crl/CRLDownloader;->match(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 109
    invoke-interface {v3, v7}, Lcom/sec/android/security/crl/CRLDownloader;->downloadCrl(Ljava/lang/String;)Ljava/security/cert/X509CRL;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_2

    invoke-direct {p0, v1}, Lcom/sec/android/security/crl/CRLCache;->isCrlTooOld(Ljava/security/cert/X509CRL;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 111
    :cond_2
    new-instance v8, Ljava/io/IOException;

    const-string v9, "No fresh CRL available"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    .end local v0    # "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    .end local v1    # "crl":Ljava/security/cert/X509CRL;
    .end local v3    # "d":Lcom/sec/android/security/crl/CRLDownloader;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v7    # "uri":Ljava/lang/String;
    :catchall_0
    move-exception v8

    monitor-exit p0

    throw v8

    .line 113
    .restart local v0    # "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    .restart local v1    # "crl":Ljava/security/cert/X509CRL;
    .restart local v3    # "d":Lcom/sec/android/security/crl/CRLDownloader;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v7    # "uri":Ljava/lang/String;
    :cond_3
    :try_start_1
    iget-object v8, p0, Lcom/sec/android/security/crl/CRLCache;->cache:Ljava/util/HashMap;

    invoke-virtual {v8, v7, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v2, v1

    .line 120
    .end local v0    # "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    .end local v1    # "crl":Ljava/security/cert/X509CRL;
    .end local v3    # "d":Lcom/sec/android/security/crl/CRLDownloader;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .local v2, "crl":Ljava/security/cert/X509CRL;
    :goto_1
    monitor-exit p0

    return-object v2

    .line 107
    .end local v2    # "crl":Ljava/security/cert/X509CRL;
    .restart local v0    # "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    .restart local v1    # "crl":Ljava/security/cert/X509CRL;
    .restart local v3    # "d":Lcom/sec/android/security/crl/CRLDownloader;
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .end local v0    # "arr$":[Lcom/sec/android/security/crl/CRLDownloader;
    .end local v3    # "d":Lcom/sec/android/security/crl/CRLDownloader;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_5
    move-object v2, v1

    .line 120
    .end local v1    # "crl":Ljava/security/cert/X509CRL;
    .restart local v2    # "crl":Ljava/security/cert/X509CRL;
    goto :goto_1

    .line 122
    .end local v2    # "crl":Ljava/security/cert/X509CRL;
    .end local v7    # "uri":Ljava/lang/String;
    :cond_6
    :try_start_2
    new-instance v8, Ljava/io/IOException;

    const-string v9, "No distribution points in certificate"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method
