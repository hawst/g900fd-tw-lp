.class public Lcom/sec/android/security/crl/CRLUtils;
.super Lcom/android/org/bouncycastle/jce/provider/RFC3280CertPathUtilities;
.source "CRLUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/android/org/bouncycastle/jce/provider/RFC3280CertPathUtilities;-><init>()V

    return-void
.end method

.method public static checkCRLs(Ljava/security/cert/PKIXParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/util/List;)V
    .locals 6
    .param p0, "params"    # Ljava/security/cert/PKIXParameters;
    .param p1, "cert"    # Ljava/security/cert/X509Certificate;
    .param p2, "validDate"    # Ljava/util/Date;
    .param p3, "sign"    # Ljava/security/cert/X509Certificate;
    .param p4, "workingPublicKey"    # Ljava/security/PublicKey;
    .param p5, "certPathCerts"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/android/org/bouncycastle/jce/provider/AnnotatedException;
        }
    .end annotation

    .prologue
    .line 39
    instance-of v1, p0, Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;

    if-eqz v1, :cond_0

    move-object v0, p0

    .line 41
    check-cast v0, Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;

    .local v0, "paramsPKIX":Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;
    :goto_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 47
    invoke-static/range {v0 .. v5}, Lcom/android/org/bouncycastle/jce/provider/RFC3280CertPathUtilities;->checkCRLs(Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/util/List;)V

    .line 48
    return-void

    .line 45
    .end local v0    # "paramsPKIX":Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;
    :cond_0
    invoke-static {p0}, Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;->getInstance(Ljava/security/cert/PKIXParameters;)Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;

    move-result-object v0

    .restart local v0    # "paramsPKIX":Lcom/android/org/bouncycastle/x509/ExtendedPKIXParameters;
    goto :goto_0
.end method
