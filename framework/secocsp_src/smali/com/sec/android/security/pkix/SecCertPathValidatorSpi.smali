.class public Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;
.super Lcom/android/org/bouncycastle/jce/provider/PKIXCertPathValidatorSpi;
.source "SecCertPathValidatorSpi.java"


# static fields
.field private static final crlCache:Lcom/sec/android/security/crl/CRLCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/sec/android/security/crl/CRLCache;

    invoke-direct {v0}, Lcom/sec/android/security/crl/CRLCache;-><init>()V

    sput-object v0, Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;->crlCache:Lcom/sec/android/security/crl/CRLCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/android/org/bouncycastle/jce/provider/PKIXCertPathValidatorSpi;-><init>()V

    return-void
.end method

.method private isOcspEnabled()Z
    .locals 1

    .prologue
    .line 112
    invoke-static {}, Lcom/sec/android/security/pkix/EdmPolicyUtil;->isOcspCheckEnabled()Z

    move-result v0

    return v0
.end method

.method private isRevocationEnabled()Z
    .locals 1

    .prologue
    .line 108
    invoke-static {}, Lcom/sec/android/security/pkix/EdmPolicyUtil;->isRevocationCheckEnabled()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public engineValidate(Ljava/security/cert/CertPath;Ljava/security/cert/CertPathParameters;)Ljava/security/cert/CertPathValidatorResult;
    .locals 11
    .param p1, "certPath"    # Ljava/security/cert/CertPath;
    .param p2, "params"    # Ljava/security/cert/CertPathParameters;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertPathValidatorException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 56
    instance-of v8, p2, Ljava/security/cert/PKIXParameters;

    if-nez v8, :cond_0

    .line 58
    new-instance v8, Ljava/security/InvalidAlgorithmParameterException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Parameters must be a "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-class v10, Ljava/security/cert/PKIXParameters;

    invoke-virtual {v10}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " instance."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v8

    :cond_0
    move-object v5, p2

    .line 62
    check-cast v5, Ljava/security/cert/PKIXParameters;

    .line 63
    .local v5, "pkixParameters":Ljava/security/cert/PKIXParameters;
    invoke-virtual {v5}, Ljava/security/cert/PKIXParameters;->isRevocationEnabled()Z

    move-result v7

    .line 64
    .local v7, "wasRevocationEnabled":Z
    invoke-direct {p0}, Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;->isRevocationEnabled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 66
    invoke-virtual {p1}, Ljava/security/cert/CertPath;->getCertificates()Ljava/util/List;

    move-result-object v1

    .line 70
    .local v1, "certs":Ljava/util/List;, "Ljava/util/List<+Ljava/security/cert/Certificate;>;"
    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/security/cert/X509Certificate;

    invoke-virtual {v5}, Ljava/security/cert/PKIXParameters;->getTrustAnchors()Ljava/util/Set;

    move-result-object v9

    invoke-virtual {v5}, Ljava/security/cert/PKIXParameters;->getSigProvider()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v9, v10}, Lcom/sec/android/security/pkix/CertPathValidatorUtilities;->findTrustAnchor(Ljava/security/cert/X509Certificate;Ljava/util/Set;Ljava/lang/String;)Ljava/security/cert/TrustAnchor;

    move-result-object v6

    .line 72
    .local v6, "trust":Ljava/security/cert/TrustAnchor;
    if-nez v6, :cond_1

    .line 73
    new-instance v9, Ljava/security/cert/CertPathValidatorException;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "No trustanchor for certificate "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/security/cert/X509Certificate;

    invoke-virtual {v8}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v8

    invoke-virtual {v8}, Ljavax/security/auth/x500/X500Principal;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v9, v8}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_0
    .catch Lcom/android/org/bouncycastle/jce/provider/AnnotatedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    .end local v6    # "trust":Ljava/security/cert/TrustAnchor;
    :catch_0
    move-exception v3

    .line 77
    .local v3, "e":Lcom/android/org/bouncycastle/jce/provider/AnnotatedException;
    new-instance v8, Ljava/security/cert/CertPathValidatorException;

    invoke-virtual {v3}, Lcom/android/org/bouncycastle/jce/provider/AnnotatedException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-direct {v8, v9, v3, p1, v10}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Ljava/security/cert/CertPath;I)V

    throw v8

    .line 81
    .end local v3    # "e":Lcom/android/org/bouncycastle/jce/provider/AnnotatedException;
    .restart local v6    # "trust":Ljava/security/cert/TrustAnchor;
    :cond_1
    invoke-virtual {v5}, Ljava/security/cert/PKIXParameters;->getCertPathCheckers()Ljava/util/List;

    move-result-object v2

    .line 82
    .local v2, "checkers":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/PKIXCertPathChecker;>;"
    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 94
    :cond_2
    new-instance v8, Lcom/sec/android/security/pkix/PKIXRevocationChecker;

    sget-object v9, Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;->crlCache:Lcom/sec/android/security/crl/CRLCache;

    invoke-direct {p0}, Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;->isOcspEnabled()Z

    move-result v10

    invoke-direct {v8, v6, v9, v10}, Lcom/sec/android/security/pkix/PKIXRevocationChecker;-><init>(Ljava/security/cert/TrustAnchor;Lcom/sec/android/security/crl/CRLCache;Z)V

    invoke-virtual {v5, v8}, Ljava/security/cert/PKIXParameters;->addCertPathChecker(Ljava/security/cert/PKIXCertPathChecker;)V

    .line 95
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/security/cert/PKIXParameters;->setRevocationEnabled(Z)V

    .line 100
    .end local v1    # "certs":Ljava/util/List;, "Ljava/util/List<+Ljava/security/cert/Certificate;>;"
    .end local v2    # "checkers":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/PKIXCertPathChecker;>;"
    .end local v6    # "trust":Ljava/security/cert/TrustAnchor;
    :cond_3
    :try_start_1
    invoke-super {p0, p1, p2}, Lcom/android/org/bouncycastle/jce/provider/PKIXCertPathValidatorSpi;->engineValidate(Ljava/security/cert/CertPath;Ljava/security/cert/CertPathParameters;)Ljava/security/cert/CertPathValidatorResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 103
    invoke-virtual {v5, v7}, Ljava/security/cert/PKIXParameters;->setRevocationEnabled(Z)V

    return-object v8

    .line 86
    .restart local v1    # "certs":Ljava/util/List;, "Ljava/util/List<+Ljava/security/cert/Certificate;>;"
    .restart local v2    # "checkers":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/PKIXCertPathChecker;>;"
    .restart local v6    # "trust":Ljava/security/cert/TrustAnchor;
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Ljava/security/cert/PKIXParameters;->setCertPathCheckers(Ljava/util/List;)V

    .line 88
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/cert/PKIXCertPathChecker;

    .line 89
    .local v0, "c":Ljava/security/cert/PKIXCertPathChecker;
    instance-of v8, v0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;

    if-nez v8, :cond_5

    .line 90
    invoke-virtual {v5, v0}, Ljava/security/cert/PKIXParameters;->addCertPathChecker(Ljava/security/cert/PKIXCertPathChecker;)V

    goto :goto_0

    .line 103
    .end local v0    # "c":Ljava/security/cert/PKIXCertPathChecker;
    .end local v1    # "certs":Ljava/util/List;, "Ljava/util/List<+Ljava/security/cert/Certificate;>;"
    .end local v2    # "checkers":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/PKIXCertPathChecker;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "trust":Ljava/security/cert/TrustAnchor;
    :catchall_0
    move-exception v8

    invoke-virtual {v5, v7}, Ljava/security/cert/PKIXParameters;->setRevocationEnabled(Z)V

    throw v8
.end method
