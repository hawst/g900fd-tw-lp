.class public Lcom/sec/android/security/pkix/PKIXProvider;
.super Ljava/security/Provider;
.source "PKIXProvider.java"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 11
    const-string v0, "SRP"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-string v1, "Samsung PKIX provider with mandatory certificate revocation check"

    invoke-direct {p0, v0, v2, v3, v1}, Ljava/security/Provider;-><init>(Ljava/lang/String;DLjava/lang/String;)V

    .line 13
    new-instance v0, Lcom/sec/android/security/pkix/PKIXProvider$1;

    invoke-direct {v0, p0}, Lcom/sec/android/security/pkix/PKIXProvider$1;-><init>(Lcom/sec/android/security/pkix/PKIXProvider;)V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    .line 21
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/security/pkix/PKIXProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/security/pkix/PKIXProvider;

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/sec/android/security/pkix/PKIXProvider;->setup()V

    return-void
.end method

.method private setup()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "Alg.Alias.CertPathValidator.RFC3280"

    const-string v1, "PKIX"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/security/pkix/PKIXProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 25
    const-string v0, "CertPathValidator.PKIX"

    const-class v1, Lcom/sec/android/security/pkix/SecCertPathValidatorSpi;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/security/pkix/PKIXProvider;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    return-void
.end method
