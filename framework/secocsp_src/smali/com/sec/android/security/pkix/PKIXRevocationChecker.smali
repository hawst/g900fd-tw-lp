.class Lcom/sec/android/security/pkix/PKIXRevocationChecker;
.super Ljava/security/cert/PKIXCertPathChecker;
.source "PKIXRevocationChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/security/pkix/PKIXRevocationChecker$1;
    }
.end annotation


# instance fields
.field crlCache:Lcom/sec/android/security/crl/CRLCache;

.field currentIssuer:Ljava/security/cert/X509Certificate;

.field isOcspEnabled:Z


# direct methods
.method public constructor <init>(Ljava/security/cert/TrustAnchor;Lcom/sec/android/security/crl/CRLCache;Z)V
    .locals 1
    .param p1, "anchor"    # Ljava/security/cert/TrustAnchor;
    .param p2, "cache"    # Lcom/sec/android/security/crl/CRLCache;
    .param p3, "isOcspEnabled"    # Z

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/security/cert/PKIXCertPathChecker;-><init>()V

    .line 35
    invoke-virtual {p1}, Ljava/security/cert/TrustAnchor;->getTrustedCert()Ljava/security/cert/X509Certificate;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    .line 36
    iput-object p2, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->crlCache:Lcom/sec/android/security/crl/CRLCache;

    .line 37
    iput-boolean p3, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->isOcspEnabled:Z

    .line 38
    return-void
.end method

.method private checkStatusWithCRL(Ljava/security/cert/X509Certificate;)V
    .locals 12
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertPathValidatorException;
        }
    .end annotation

    .prologue
    .line 64
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->crlCache:Lcom/sec/android/security/crl/CRLCache;

    invoke-virtual {v1, p1}, Lcom/sec/android/security/crl/CRLCache;->get(Ljava/security/cert/X509Certificate;)Ljava/security/cert/X509CRL;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 88
    .local v9, "crl":Ljava/security/cert/X509CRL;
    :try_start_1
    const-string v1, "X.509"

    invoke-static {v1}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v7

    .line 89
    .local v7, "cf":Ljava/security/cert/CertificateFactory;
    const-string v1, "Collection"

    new-instance v2, Ljava/security/cert/CollectionCertStoreParameters;

    invoke-static {v9}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/cert/CollectionCertStoreParameters;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v2}, Ljava/security/cert/CertStore;->getInstance(Ljava/lang/String;Ljava/security/cert/CertStoreParameters;)Ljava/security/cert/CertStore;

    move-result-object v11

    .line 90
    .local v11, "store":Ljava/security/cert/CertStore;
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/security/cert/CertificateFactory;->generateCertPath(Ljava/util/List;)Ljava/security/cert/CertPath;

    move-result-object v8

    .line 91
    .local v8, "cp":Ljava/security/cert/CertPath;
    new-instance v6, Ljava/security/cert/TrustAnchor;

    iget-object v1, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    const/4 v2, 0x0

    invoke-direct {v6, v1, v2}, Ljava/security/cert/TrustAnchor;-><init>(Ljava/security/cert/X509Certificate;[B)V

    .line 92
    .local v6, "anchor":Ljava/security/cert/TrustAnchor;
    new-instance v0, Ljava/security/cert/PKIXParameters;

    invoke-static {v6}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/cert/PKIXParameters;-><init>(Ljava/util/Set;)V

    .line 93
    .local v0, "params":Ljava/security/cert/PKIXParameters;
    invoke-virtual {v0, v11}, Ljava/security/cert/PKIXParameters;->addCertStore(Ljava/security/cert/CertStore;)V

    .line 94
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    iget-object v3, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    iget-object v1, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    invoke-virtual {v1}, Ljava/security/cert/X509Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v4

    invoke-virtual {v8}, Ljava/security/cert/CertPath;->getCertificates()Ljava/util/List;

    move-result-object v5

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/sec/android/security/crl/CRLUtils;->checkCRLs(Ljava/security/cert/PKIXParameters;Ljava/security/cert/X509Certificate;Ljava/util/Date;Ljava/security/cert/X509Certificate;Ljava/security/PublicKey;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 99
    return-void

    .line 65
    .end local v0    # "params":Ljava/security/cert/PKIXParameters;
    .end local v6    # "anchor":Ljava/security/cert/TrustAnchor;
    .end local v7    # "cf":Ljava/security/cert/CertificateFactory;
    .end local v8    # "cp":Ljava/security/cert/CertPath;
    .end local v9    # "crl":Ljava/security/cert/X509CRL;
    .end local v11    # "store":Ljava/security/cert/CertStore;
    :catch_0
    move-exception v10

    .line 66
    .local v10, "e":Ljava/io/IOException;
    new-instance v1, Ljava/security/cert/CertPathValidatorException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to get CRL for certificate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/security/auth/x500/X500Principal;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v10}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 96
    .end local v10    # "e":Ljava/io/IOException;
    .restart local v9    # "crl":Ljava/security/cert/X509CRL;
    :catch_1
    move-exception v10

    .line 97
    .local v10, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/security/cert/CertPathValidatorException;

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v10}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private checkStatusWithOCSP(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;)V
    .locals 6
    .param p1, "certificate"    # Ljava/security/cert/X509Certificate;
    .param p2, "issuer"    # Ljava/security/cert/X509Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;,
            Ljava/security/cert/CertPathValidatorException;
        }
    .end annotation

    .prologue
    .line 102
    new-instance v1, Lcom/sec/android/security/ocsp/OCSPHTTPClient;

    invoke-direct {v1, p2}, Lcom/sec/android/security/ocsp/OCSPHTTPClient;-><init>(Ljava/security/cert/X509Certificate;)V

    .line 103
    .local v1, "ocspClient":Lcom/sec/android/security/ocsp/OCSPHTTPClient;
    invoke-virtual {v1, p1}, Lcom/sec/android/security/ocsp/OCSPHTTPClient;->checkCertificate(Ljava/security/cert/X509Certificate;)Lcom/sec/android/security/ocsp/ResponseInfo;

    move-result-object v2

    .line 105
    .local v2, "ocspResponseInfo":Lcom/sec/android/security/ocsp/ResponseInfo;
    invoke-virtual {v2}, Lcom/sec/android/security/ocsp/ResponseInfo;->getRequestedCertInfo()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/security/ocsp/CertificateStatus;

    .line 106
    .local v0, "certStatus":Lcom/sec/android/security/ocsp/CertificateStatus;
    sget-object v3, Lcom/sec/android/security/pkix/PKIXRevocationChecker$1;->$SwitchMap$com$sec$android$security$ocsp$CertificateStatus:[I

    invoke-virtual {v0}, Lcom/sec/android/security/ocsp/CertificateStatus;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 115
    new-instance v3, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;

    const-string v4, "Need to use CRLs"

    invoke-direct {v3, v4}, Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 112
    :pswitch_0
    new-instance v3, Ljava/security/cert/CertPathValidatorException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Certificate "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getSubjectX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object v5

    invoke-virtual {v5}, Ljavax/security/auth/x500/X500Principal;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " is revoked"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 117
    :pswitch_1
    return-void

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public check(Ljava/security/cert/Certificate;Ljava/util/Collection;)V
    .locals 3
    .param p1, "cert"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/security/cert/Certificate;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertPathValidatorException;
        }
    .end annotation

    .prologue
    .line 43
    .local p2, "unresolvedCritExts":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    move-object v1, p1

    check-cast v1, Ljava/security/cert/X509Certificate;

    .line 45
    .local v1, "x509Cert":Ljava/security/cert/X509Certificate;
    iget-boolean v2, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->isOcspEnabled:Z

    if-eqz v2, :cond_0

    .line 48
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    invoke-direct {p0, v1, v2}, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->checkStatusWithOCSP(Ljava/security/cert/X509Certificate;Ljava/security/cert/X509Certificate;)V
    :try_end_0
    .catch Lcom/sec/android/org/bouncycastle/ocsp/OCSPException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    iput-object v1, p0, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->currentIssuer:Ljava/security/cert/X509Certificate;

    .line 59
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 51
    .local v0, "e":Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
    invoke-direct {p0, v1}, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->checkStatusWithCRL(Ljava/security/cert/X509Certificate;)V

    goto :goto_0

    .line 54
    .end local v0    # "e":Lcom/sec/android/org/bouncycastle/ocsp/OCSPException;
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/android/security/pkix/PKIXRevocationChecker;->checkStatusWithCRL(Ljava/security/cert/X509Certificate;)V

    goto :goto_0
.end method

.method public getSupportedExtensions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public init(Z)V
    .locals 2
    .param p1, "forward"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertPathValidatorException;
        }
    .end annotation

    .prologue
    .line 127
    if-eqz p1, :cond_0

    .line 128
    new-instance v0, Ljava/security/cert/CertPathValidatorException;

    const-string v1, "Forward checking is not supported"

    invoke-direct {v0, v1}, Ljava/security/cert/CertPathValidatorException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 129
    :cond_0
    return-void
.end method

.method public isForwardCheckingSupported()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method
