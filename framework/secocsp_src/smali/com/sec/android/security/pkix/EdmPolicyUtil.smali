.class public Lcom/sec/android/security/pkix/EdmPolicyUtil;
.super Ljava/lang/Object;
.source "EdmPolicyUtil.java"


# static fields
.field private static final TYPE_OCSP:I = 0x1

.field private static final TYPE_REVOCATION:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getPolicyValue(I)Z
    .locals 11
    .param p0, "type"    # I

    .prologue
    const/4 v9, 0x0

    .line 18
    :try_start_0
    const-string v8, "android.sec.enterprise.EnterpriseDeviceManager"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 19
    .local v1, "clsEdm":Ljava/lang/Class;
    const-string v8, "android.sec.enterprise.certificate.CertificatePolicy"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 21
    .local v0, "clsCertPolicy":Ljava/lang/Class;
    const-string v10, "getInstance"

    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Class;

    invoke-virtual {v1, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 22
    .local v4, "methodGetInstance":Ljava/lang/reflect/Method;
    const-string v10, "getCertificatePolicy"

    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Class;

    invoke-virtual {v1, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 23
    .local v3, "methodGetCertPolicy":Ljava/lang/reflect/Method;
    const/4 v8, 0x0

    const/4 v10, 0x0

    new-array v10, v10, [Ljava/lang/Object;

    invoke-virtual {v4, v8, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 24
    .local v6, "objEdm":Ljava/lang/Object;
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v3, v6, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 25
    .local v5, "objCertPolicy":Ljava/lang/Object;
    const/4 v7, 0x0

    .line 26
    .local v7, "policy":Ljava/lang/reflect/Method;
    if-nez p0, :cond_0

    .line 27
    const-string v10, "isRevocationCheckEnabled"

    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Class;

    invoke-virtual {v0, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v7

    .line 33
    :goto_0
    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Object;

    invoke-virtual {v7, v5, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 43
    .end local v0    # "clsCertPolicy":Ljava/lang/Class;
    .end local v1    # "clsEdm":Ljava/lang/Class;
    .end local v3    # "methodGetCertPolicy":Ljava/lang/reflect/Method;
    .end local v4    # "methodGetInstance":Ljava/lang/reflect/Method;
    .end local v5    # "objCertPolicy":Ljava/lang/Object;
    .end local v6    # "objEdm":Ljava/lang/Object;
    .end local v7    # "policy":Ljava/lang/reflect/Method;
    :goto_1
    return v8

    .line 28
    .restart local v0    # "clsCertPolicy":Ljava/lang/Class;
    .restart local v1    # "clsEdm":Ljava/lang/Class;
    .restart local v3    # "methodGetCertPolicy":Ljava/lang/reflect/Method;
    .restart local v4    # "methodGetInstance":Ljava/lang/reflect/Method;
    .restart local v5    # "objCertPolicy":Ljava/lang/Object;
    .restart local v6    # "objEdm":Ljava/lang/Object;
    .restart local v7    # "policy":Ljava/lang/reflect/Method;
    :cond_0
    const/4 v8, 0x1

    if-ne p0, v8, :cond_1

    .line 29
    const-string v10, "isOcspCheckEnabled"

    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Class;

    invoke-virtual {v0, v10, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v7

    goto :goto_0

    :cond_1
    move v8, v9

    .line 31
    goto :goto_1

    .line 34
    .end local v0    # "clsCertPolicy":Ljava/lang/Class;
    .end local v1    # "clsEdm":Ljava/lang/Class;
    .end local v3    # "methodGetCertPolicy":Ljava/lang/reflect/Method;
    .end local v4    # "methodGetInstance":Ljava/lang/reflect/Method;
    .end local v5    # "objCertPolicy":Ljava/lang/Object;
    .end local v6    # "objEdm":Ljava/lang/Object;
    .end local v7    # "policy":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/NoSuchMethodException;
    move v8, v9

    .line 35
    goto :goto_1

    .line 36
    .end local v2    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v2

    .local v2, "e":Ljava/lang/IllegalAccessException;
    move v8, v9

    .line 37
    goto :goto_1

    .line 38
    .end local v2    # "e":Ljava/lang/IllegalAccessException;
    :catch_2
    move-exception v2

    .local v2, "e":Ljava/lang/IllegalArgumentException;
    move v8, v9

    .line 39
    goto :goto_1

    .line 40
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v2

    .local v2, "e":Ljava/lang/reflect/InvocationTargetException;
    move v8, v9

    .line 41
    goto :goto_1

    .line 42
    .end local v2    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v2

    .local v2, "e":Ljava/lang/ClassNotFoundException;
    move v8, v9

    .line 43
    goto :goto_1
.end method

.method public static isOcspCheckEnabled()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/security/pkix/EdmPolicyUtil;->getPolicyValue(I)Z

    move-result v0

    return v0
.end method

.method public static isRevocationCheckEnabled()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/security/pkix/EdmPolicyUtil;->getPolicyValue(I)Z

    move-result v0

    return v0
.end method
