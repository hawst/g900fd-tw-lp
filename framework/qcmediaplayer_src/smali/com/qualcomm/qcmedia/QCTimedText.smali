.class public Lcom/qualcomm/qcmedia/QCTimedText;
.super Ljava/lang/Object;
.source "QCTimedText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;,
        Lcom/qualcomm/qcmedia/QCTimedText$HyperText;,
        Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;,
        Lcom/qualcomm/qcmedia/QCTimedText$Font;,
        Lcom/qualcomm/qcmedia/QCTimedText$Style;,
        Lcom/qualcomm/qcmedia/QCTimedText$Justification;,
        Lcom/qualcomm/qcmedia/QCTimedText$TextPos;,
        Lcom/qualcomm/qcmedia/QCTimedText$CharPos;,
        Lcom/qualcomm/qcmedia/QCTimedText$Text;
    }
.end annotation


# static fields
.field private static final FIRST_PRIVATE_KEY:I = 0x65

.field private static final FIRST_PUBLIC_KEY:I = 0x1

.field public static final KEY_BACKGROUND_COLOR_RGBA:I = 0x3

.field public static final KEY_DISPLAY_FLAGS:I = 0x1

.field public static final KEY_DURATION:I = 0x13

.field private static final KEY_END_CHAR:I = 0x68

.field private static final KEY_FONT_ID:I = 0x69

.field private static final KEY_FONT_SIZE:I = 0x6a

.field private static final KEY_GLOBAL_SETTING:I = 0x65

.field public static final KEY_HEIGHT:I = 0x11

.field public static final KEY_HIGHLIGHT_COLOR_RGBA:I = 0x4

.field private static final KEY_LOCAL_SETTING:I = 0x66

.field public static final KEY_SCROLL_DELAY:I = 0x5

.field private static final KEY_START_CHAR:I = 0x67

.field public static final KEY_START_OFFSET:I = 0x14

.field public static final KEY_START_TIME:I = 0x7

.field public static final KEY_STRUCT_BLINKING_TEXT_LIST:I = 0x8

.field public static final KEY_STRUCT_FONT_LIST:I = 0x9

.field public static final KEY_STRUCT_HIGHLIGHT_LIST:I = 0xa

.field public static final KEY_STRUCT_HYPER_TEXT_LIST:I = 0xb

.field public static final KEY_STRUCT_JUSTIFICATION:I = 0xf

.field public static final KEY_STRUCT_KARAOKE_LIST:I = 0xc

.field public static final KEY_STRUCT_STYLE_LIST:I = 0xd

.field public static final KEY_STRUCT_TEXT:I = 0x10

.field public static final KEY_STRUCT_TEXT_POS:I = 0xe

.field public static final KEY_STYLE_FLAGS:I = 0x2

.field public static final KEY_SUBS_ATOM:I = 0x15

.field private static final KEY_TEXT_COLOR_RGBA:I = 0x6b

.field private static final KEY_TEXT_DISCONTINUITY:I = 0x6e

.field private static final KEY_TEXT_EOS:I = 0x6c

.field private static final KEY_TEXT_FLAG_TYPE:I = 0x6d

.field public static final KEY_TEXT_FORMAT:I = 0x16

.field public static final KEY_WIDTH:I = 0x12

.field public static final KEY_WRAP_TEXT:I = 0x6

.field private static final LAST_PRIVATE_KEY:I = 0x6e

.field private static final LAST_PUBLIC_KEY:I = 0x16

.field private static final TAG:Ljava/lang/String; = "QCTimedText"


# instance fields
.field private mBackgroundColorRGBA:I

.field private mBlinkingPosList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$CharPos;",
            ">;"
        }
    .end annotation
.end field

.field private mDisplayFlags:I

.field private mFontList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$Font;",
            ">;"
        }
    .end annotation
.end field

.field private mHighlightColorRGBA:I

.field private mHighlightPosList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$CharPos;",
            ">;"
        }
    .end annotation
.end field

.field private mHyperTextList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$HyperText;",
            ">;"
        }
    .end annotation
.end field

.field private mJustification:Lcom/qualcomm/qcmedia/QCTimedText$Justification;

.field private mKaraokeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;",
            ">;"
        }
    .end annotation
.end field

.field private final mKeyObjectMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollDelay:I

.field private mStyleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/qualcomm/qcmedia/QCTimedText$Style;",
            ">;"
        }
    .end annotation
.end field

.field private mSubsAtomStruct:Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

.field private mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

.field private mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

.field private mWrapText:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    .line 101
    iput v2, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mDisplayFlags:I

    .line 102
    iput v2, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBackgroundColorRGBA:I

    .line 103
    iput v2, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightColorRGBA:I

    .line 104
    iput v2, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mScrollDelay:I

    .line 105
    iput v2, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mWrapText:I

    .line 107
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBlinkingPosList:Ljava/util/List;

    .line 108
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightPosList:Ljava/util/List;

    .line 109
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKaraokeList:Ljava/util/List;

    .line 110
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mFontList:Ljava/util/List;

    .line 111
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mStyleList:Ljava/util/List;

    .line 112
    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHyperTextList:Ljava/util/List;

    .line 365
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->parseParcel(Landroid/os/Parcel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "parseParcel() fails"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_0
    return-void
.end method

.method private parseParcel(Landroid/os/Parcel;)Z
    .locals 12
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    .line 377
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 378
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v9

    if-nez v9, :cond_0

    .line 379
    const-string v9, "QCTimedText"

    const-string v10, "Invalid mParcel.dataAvail()"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v9, 0x0

    .line 595
    :goto_0
    return v9

    .line 383
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 385
    .local v8, "type":I
    const/16 v9, 0x66

    if-ne v8, v9, :cond_9

    .line 387
    new-instance v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;

    invoke-direct {v9, p0}, Lcom/qualcomm/qcmedia/QCTimedText$Text;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    iput-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    .line 390
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 391
    const/16 v9, 0x16

    if-eq v8, v9, :cond_1

    .line 392
    const-string v9, "QCTimedText"

    const-string v10, "Invalid KEY_TEXT_FORMAT key"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const/4 v9, 0x0

    goto :goto_0

    .line 396
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "format":Ljava/lang/String;
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 401
    const/16 v9, 0x6d

    if-eq v8, v9, :cond_2

    .line 402
    const-string v9, "QCTimedText"

    const-string v10, "Invalid KEY_TEXT_FLAG_TYPE key"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v9, 0x0

    goto :goto_0

    .line 406
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 407
    const/4 v9, 0x2

    if-ne v8, v9, :cond_3

    .line 409
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    iput v8, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->flags:I

    .line 410
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    const/4 v10, 0x0

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->textLen:I

    .line 411
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/16 v10, 0x10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/4 v10, 0x7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/16 v10, 0x11

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/16 v10, 0x12

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/16 v10, 0x13

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    const/16 v10, 0x14

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    const/4 v11, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 421
    :cond_3
    if-eqz v8, :cond_4

    const/4 v9, 0x1

    if-eq v8, v9, :cond_4

    .line 422
    const-string v9, "QCTimedText"

    const-string v10, "Invalid TIMED_TEXT_FLAG_FRAME key"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 425
    :cond_4
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    iput v8, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->flags:I

    .line 427
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 428
    const/16 v9, 0x6e

    if-ne v8, v9, :cond_5

    .line 429
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    const/4 v10, 0x1

    iput-boolean v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->discontinuity:Z

    .line 430
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 434
    :cond_5
    const/16 v9, 0x10

    if-eq v8, v9, :cond_6

    .line 435
    const-string v9, "QCTimedText"

    const-string v10, "Invalid KEY_STRUCT_TEXT key"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 439
    :cond_6
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->textLen:I

    .line 440
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v10

    iput-object v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Text;->text:[B

    .line 442
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextStruct:Lcom/qualcomm/qcmedia/QCTimedText$Text;

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 446
    const/4 v9, 0x7

    if-eq v8, v9, :cond_7

    .line 447
    const-string v9, "QCTimedText"

    const-string v10, "Invalid KEY_START_TIME key"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 450
    :cond_7
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 451
    .local v5, "mStartTimeMs":I
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    .end local v0    # "format":Ljava/lang/String;
    .end local v5    # "mStartTimeMs":I
    :cond_8
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v9

    if-lez v9, :cond_c

    .line 459
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 460
    .local v1, "key":I
    invoke-virtual {p0, v1}, Lcom/qualcomm/qcmedia/QCTimedText;->isValidKey(I)Z

    move-result v9

    if-nez v9, :cond_a

    .line 461
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid timed text key found: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 462
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 453
    .end local v1    # "key":I
    :cond_9
    const/16 v9, 0x65

    if-eq v8, v9, :cond_8

    .line 454
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid timed text key found: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 455
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 465
    .restart local v1    # "key":I
    :cond_a
    const/4 v7, 0x0

    .line 467
    .local v7, "object":Ljava/lang/Object;
    packed-switch v1, :pswitch_data_0

    .line 587
    .end local v7    # "object":Ljava/lang/Object;
    :goto_2
    :pswitch_0
    if-eqz v7, :cond_8

    .line 588
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 589
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 591
    :cond_b
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v10, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 469
    .restart local v7    # "object":Ljava/lang/Object;
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 470
    .local v3, "mHeight":I
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mHeight: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 471
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 476
    .end local v3    # "mHeight":I
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 477
    .local v6, "mWidth":I
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mWidth: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 483
    .end local v6    # "mWidth":I
    :pswitch_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 484
    .local v2, "mDuration":I
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mDuration: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 490
    .end local v2    # "mDuration":I
    :pswitch_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 491
    .local v4, "mStartOffset":I
    const-string v9, "QCTimedText"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mStartOffset: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 498
    .end local v4    # "mStartOffset":I
    :pswitch_5
    new-instance v9, Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

    invoke-direct {v9, p0}, Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    iput-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mSubsAtomStruct:Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

    .line 499
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mSubsAtomStruct:Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;->subsAtomLen:I

    .line 500
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mSubsAtomStruct:Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v10

    iput-object v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;->subsAtomData:[B

    .line 501
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iget-object v11, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mSubsAtomStruct:Lcom/qualcomm/qcmedia/QCTimedText$SubsAtom;

    invoke-virtual {v9, v10, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 506
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readStyle(Landroid/os/Parcel;)V

    .line 507
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mStyleList:Ljava/util/List;

    .line 508
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 511
    .local v7, "object":Ljava/lang/Object;
    :pswitch_7
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readFont(Landroid/os/Parcel;)V

    .line 512
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mFontList:Ljava/util/List;

    .line 513
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 516
    .local v7, "object":Ljava/lang/Object;
    :pswitch_8
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readHighlight(Landroid/os/Parcel;)V

    .line 517
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightPosList:Ljava/util/List;

    .line 518
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 521
    .local v7, "object":Ljava/lang/Object;
    :pswitch_9
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readKaraoke(Landroid/os/Parcel;)V

    .line 522
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKaraokeList:Ljava/util/List;

    .line 523
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 526
    .local v7, "object":Ljava/lang/Object;
    :pswitch_a
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readHyperText(Landroid/os/Parcel;)V

    .line 527
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHyperTextList:Ljava/util/List;

    .line 529
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 532
    .local v7, "object":Ljava/lang/Object;
    :pswitch_b
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->readBlinkingText(Landroid/os/Parcel;)V

    .line 533
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBlinkingPosList:Ljava/util/List;

    .line 535
    .local v7, "object":Ljava/util/List;
    goto/16 :goto_2

    .line 538
    .local v7, "object":Ljava/lang/Object;
    :pswitch_c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iput v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mWrapText:I

    .line 539
    iget v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mWrapText:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 540
    .local v7, "object":Ljava/lang/Integer;
    goto/16 :goto_2

    .line 543
    .local v7, "object":Ljava/lang/Object;
    :pswitch_d
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iput v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightColorRGBA:I

    .line 544
    iget v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightColorRGBA:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 545
    .local v7, "object":Ljava/lang/Integer;
    goto/16 :goto_2

    .line 548
    .local v7, "object":Ljava/lang/Object;
    :pswitch_e
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iput v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mDisplayFlags:I

    .line 549
    iget v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mDisplayFlags:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 550
    .local v7, "object":Ljava/lang/Integer;
    goto/16 :goto_2

    .line 553
    .local v7, "object":Ljava/lang/Object;
    :pswitch_f
    new-instance v9, Lcom/qualcomm/qcmedia/QCTimedText$Justification;

    invoke-direct {v9, p0}, Lcom/qualcomm/qcmedia/QCTimedText$Justification;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    iput-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mJustification:Lcom/qualcomm/qcmedia/QCTimedText$Justification;

    .line 555
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mJustification:Lcom/qualcomm/qcmedia/QCTimedText$Justification;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Justification;->horizontalJustification:I

    .line 556
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mJustification:Lcom/qualcomm/qcmedia/QCTimedText$Justification;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$Justification;->verticalJustification:I

    .line 558
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mJustification:Lcom/qualcomm/qcmedia/QCTimedText$Justification;

    .line 559
    .local v7, "object":Lcom/qualcomm/qcmedia/QCTimedText$Justification;
    goto/16 :goto_2

    .line 562
    .local v7, "object":Ljava/lang/Object;
    :pswitch_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iput v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBackgroundColorRGBA:I

    .line 563
    iget v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBackgroundColorRGBA:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 564
    .local v7, "object":Ljava/lang/Integer;
    goto/16 :goto_2

    .line 567
    .local v7, "object":Ljava/lang/Object;
    :pswitch_11
    new-instance v9, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    invoke-direct {v9, p0}, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    iput-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    .line 569
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;->top:I

    .line 570
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;->left:I

    .line 571
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;->bottom:I

    .line 572
    iget-object v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v10

    iput v10, v9, Lcom/qualcomm/qcmedia/QCTimedText$TextPos;->right:I

    .line 574
    iget-object v7, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mTextPos:Lcom/qualcomm/qcmedia/QCTimedText$TextPos;

    .line 575
    .local v7, "object":Lcom/qualcomm/qcmedia/QCTimedText$TextPos;
    goto/16 :goto_2

    .line 578
    .local v7, "object":Ljava/lang/Object;
    :pswitch_12
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    iput v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mScrollDelay:I

    .line 579
    iget v9, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mScrollDelay:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 580
    .local v7, "object":Ljava/lang/Integer;
    goto/16 :goto_2

    .line 595
    .end local v1    # "key":I
    .end local v7    # "object":Ljava/lang/Integer;
    :cond_c
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 467
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_0
        :pswitch_10
        :pswitch_d
        :pswitch_12
        :pswitch_c
        :pswitch_0
        :pswitch_b
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
        :pswitch_6
        :pswitch_11
        :pswitch_f
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private readBlinkingText(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    .line 738
    new-instance v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;

    invoke-direct {v0, p0}, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 740
    .local v0, "blinkingPos":Lcom/qualcomm/qcmedia/QCTimedText$CharPos;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;->startChar:I

    .line 741
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;->endChar:I

    .line 743
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBlinkingPosList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 744
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBlinkingPosList:Ljava/util/List;

    .line 746
    :cond_0
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mBlinkingPosList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 747
    return-void
.end method

.method private readFont(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    .line 657
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 659
    .local v0, "entryCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 660
    new-instance v1, Lcom/qualcomm/qcmedia/QCTimedText$Font;

    invoke-direct {v1, p0}, Lcom/qualcomm/qcmedia/QCTimedText$Font;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 662
    .local v1, "font":Lcom/qualcomm/qcmedia/QCTimedText$Font;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    iput v5, v1, Lcom/qualcomm/qcmedia/QCTimedText$Font;->ID:I

    .line 663
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 665
    .local v3, "nameLen":I
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 666
    .local v4, "text":[B
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v5, v4, v6, v3}, Ljava/lang/String;-><init>([BII)V

    iput-object v5, v1, Lcom/qualcomm/qcmedia/QCTimedText$Font;->name:Ljava/lang/String;

    .line 668
    iget-object v5, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mFontList:Ljava/util/List;

    if-nez v5, :cond_0

    .line 669
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mFontList:Ljava/util/List;

    .line 671
    :cond_0
    iget-object v5, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mFontList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 659
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 673
    .end local v1    # "font":Lcom/qualcomm/qcmedia/QCTimedText$Font;
    .end local v3    # "nameLen":I
    .end local v4    # "text":[B
    :cond_1
    return-void
.end method

.method private readHighlight(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    .line 679
    new-instance v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;

    invoke-direct {v0, p0}, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 681
    .local v0, "pos":Lcom/qualcomm/qcmedia/QCTimedText$CharPos;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;->startChar:I

    .line 682
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, v0, Lcom/qualcomm/qcmedia/QCTimedText$CharPos;->endChar:I

    .line 684
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightPosList:Ljava/util/List;

    if-nez v1, :cond_0

    .line 685
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightPosList:Ljava/util/List;

    .line 687
    :cond_0
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHighlightPosList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 688
    return-void
.end method

.method private readHyperText(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v5, 0x0

    .line 715
    new-instance v1, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;

    invoke-direct {v1, p0}, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 717
    .local v1, "hyperText":Lcom/qualcomm/qcmedia/QCTimedText$HyperText;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v1, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;->startChar:I

    .line 718
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v1, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;->endChar:I

    .line 720
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 721
    .local v2, "len":I
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 722
    .local v3, "url":[B
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3, v5, v2}, Ljava/lang/String;-><init>([BII)V

    iput-object v4, v1, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;->URL:Ljava/lang/String;

    .line 724
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 725
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 726
    .local v0, "alt":[B
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v0, v5, v2}, Ljava/lang/String;-><init>([BII)V

    iput-object v4, v1, Lcom/qualcomm/qcmedia/QCTimedText$HyperText;->altString:Ljava/lang/String;

    .line 728
    iget-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHyperTextList:Ljava/util/List;

    if-nez v4, :cond_0

    .line 729
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHyperTextList:Ljava/util/List;

    .line 731
    :cond_0
    iget-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mHyperTextList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 732
    return-void
.end method

.method private readKaraoke(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    .line 694
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 696
    .local v0, "entryCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 697
    new-instance v2, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;

    invoke-direct {v2, p0}, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 699
    .local v2, "kara":Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v2, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;->startTimeMs:I

    .line 700
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v2, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;->endTimeMs:I

    .line 701
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v2, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;->startChar:I

    .line 702
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, v2, Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;->endChar:I

    .line 704
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKaraokeList:Ljava/util/List;

    if-nez v3, :cond_0

    .line 705
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKaraokeList:Ljava/util/List;

    .line 707
    :cond_0
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKaraokeList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 696
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 709
    .end local v2    # "kara":Lcom/qualcomm/qcmedia/QCTimedText$Karaoke;
    :cond_1
    return-void
.end method

.method private readStyle(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "mParcel"    # Landroid/os/Parcel;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 602
    new-instance v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;

    invoke-direct {v3, p0}, Lcom/qualcomm/qcmedia/QCTimedText$Style;-><init>(Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 603
    .local v3, "style":Lcom/qualcomm/qcmedia/QCTimedText$Style;
    const/4 v0, 0x0

    .line 605
    .local v0, "endOfStyle":Z
    :goto_0
    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/os/Parcel;->dataAvail()I

    move-result v4

    if-lez v4, :cond_3

    .line 606
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 607
    .local v2, "key":I
    sparse-switch v2, :sswitch_data_0

    .line 640
    invoke-virtual {p1}, Landroid/os/Parcel;->dataPosition()I

    move-result v4

    add-int/lit8 v4, v4, -0x4

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 641
    const/4 v0, 0x1

    goto :goto_0

    .line 609
    :sswitch_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->startChar:I

    goto :goto_0

    .line 613
    :sswitch_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->endChar:I

    goto :goto_0

    .line 617
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->fontID:I

    goto :goto_0

    .line 621
    :sswitch_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 624
    .local v1, "flags":I
    rem-int/lit8 v4, v1, 0x2

    if-ne v4, v5, :cond_0

    move v4, v5

    :goto_1
    iput-boolean v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isBold:Z

    .line 625
    rem-int/lit8 v4, v1, 0x4

    const/4 v7, 0x2

    if-lt v4, v7, :cond_1

    move v4, v5

    :goto_2
    iput-boolean v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isItalic:Z

    .line 626
    div-int/lit8 v4, v1, 0x4

    if-ne v4, v5, :cond_2

    move v4, v5

    :goto_3
    iput-boolean v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isUnderlined:Z

    goto :goto_0

    :cond_0
    move v4, v6

    .line 624
    goto :goto_1

    :cond_1
    move v4, v6

    .line 625
    goto :goto_2

    :cond_2
    move v4, v6

    .line 626
    goto :goto_3

    .line 630
    .end local v1    # "flags":I
    :sswitch_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->fontSize:I

    goto :goto_0

    .line 634
    :sswitch_5
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, v3, Lcom/qualcomm/qcmedia/QCTimedText$Style;->colorRGBA:I

    goto :goto_0

    .line 647
    .end local v2    # "key":I
    :cond_3
    iget-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mStyleList:Ljava/util/List;

    if-nez v4, :cond_4

    .line 648
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mStyleList:Ljava/util/List;

    .line 650
    :cond_4
    iget-object v4, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mStyleList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 651
    return-void

    .line 607
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_3
        0x67 -> :sswitch_0
        0x68 -> :sswitch_1
        0x69 -> :sswitch_2
        0x6a -> :sswitch_4
        0x6b -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public containsKey(I)Z
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 769
    invoke-virtual {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->isValidKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    const/4 v0, 0x1

    .line 772
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObject(I)Ljava/lang/Object;
    .locals 2
    .param p1, "key"    # I

    .prologue
    .line 790
    invoke-virtual {p0, p1}, Lcom/qualcomm/qcmedia/QCTimedText;->containsKey(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 793
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValidKey(I)Z
    .locals 2
    .param p1, "key"    # I

    .prologue
    const/4 v0, 0x1

    .line 755
    if-lt p1, v0, :cond_0

    const/16 v1, 0x16

    if-le p1, v1, :cond_2

    :cond_0
    const/16 v1, 0x65

    if-lt p1, v1, :cond_1

    const/16 v1, 0x6e

    if-le p1, v1, :cond_2

    .line 757
    :cond_1
    const/4 v0, 0x0

    .line 759
    :cond_2
    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1

    .prologue
    .line 778
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCTimedText;->mKeyObjectMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
