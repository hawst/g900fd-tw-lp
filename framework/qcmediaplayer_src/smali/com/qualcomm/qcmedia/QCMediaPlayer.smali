.class public Lcom/qualcomm/qcmedia/QCMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "QCMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;,
        Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;,
        Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;,
        Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;,
        Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;
    }
.end annotation


# static fields
.field public static final KEY_DASH_PAUSE_EVENT:I = 0x1b5a

.field public static final KEY_DASH_REPOSITION_RANGE:I = 0x2328

.field public static final KEY_DASH_RESUME_EVENT:I = 0x1b5b

.field public static final KEY_DASH_SEEK_EVENT:I = 0x1b59

.field public static final KEY_QCTIMEDTEXT_LISTENER:I = 0x1770

.field private static final MEDIA_BUFFERING_UPDATE:I = 0x3

.field private static final MEDIA_ERROR:I = 0x64

.field private static final MEDIA_INFO:I = 0xc8

.field private static final MEDIA_NOP:I = 0x0

.field private static final MEDIA_PLAYBACK_COMPLETE:I = 0x2

.field private static final MEDIA_PREPARED:I = 0x1

.field private static final MEDIA_QOE:I = 0x12c

.field private static final MEDIA_SEEK_COMPLETE:I = 0x4

.field private static final MEDIA_SET_VIDEO_SIZE:I = 0x5

.field private static final MEDIA_TIMED_TEXT:I = 0x63

.field private static final QOEPeriodic:I = 0x4

.field private static final QOEPlay:I = 0x1

.field private static final QOEStop:I = 0x2

.field private static final QOESwitch:I = 0x3

.field private static final TAG:Ljava/lang/String; = "QCMediaPlayer"

.field public static final VALID_GET_PARAM_KEYS:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

.field private mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

.field private mOnMPDAttributeListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

.field private mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

.field private mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

.field private mQCOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    .line 59
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x1f43

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 60
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x1f4a

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x1f48

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 62
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v1, Ljava/lang/Integer;

    const/16 v2, 0x2328

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    .line 69
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 71
    new-instance v1, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;-><init>(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCMediaPlayer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    .line 82
    :goto_0
    sget-object v1, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 84
    const-string v1, "QCMediaPlayer"

    const-string v2, "QCMediaPlayer::QCMediaPlayer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    return-void

    .line 73
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 75
    new-instance v1, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;-><init>(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCMediaPlayer;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    goto :goto_0

    .line 79
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    goto :goto_0
.end method

.method private static QCMediaPlayerNativeEventHandler(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0, "mediaplayer_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 484
    const-string v2, "QCMediaPlayer"

    const-string v3, "QCMediaPlayerNativeEventHandler"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 485
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "mediaplayer_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .line 486
    .local v1, "mp":Lcom/qualcomm/qcmedia/QCMediaPlayer;
    if-nez v1, :cond_1

    .line 488
    const-string v2, "QCMediaPlayer"

    const-string v3, "QCMediaPlayerNativeEventHandler mp == null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    iget-object v2, v1, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    if-eqz v2, :cond_0

    .line 493
    iget-object v2, v1, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 494
    .local v0, "m":Landroid/os/Message;
    const-string v2, "QCMediaPlayer"

    const-string v3, "QCMediaPlayerNativeEventHandler calling mp.mEventHandler.sendMessage()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 495
    iget-object v2, v1, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mEventHandler:Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;

    invoke-virtual {v2, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private QCgetStringParameter(I)Ljava/lang/String;
    .locals 6
    .param p1, "key"    # I

    .prologue
    .line 501
    sget-object v3, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v4, Ljava/lang/Integer;

    invoke-direct {v4, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 502
    const-string v3, "QCMediaPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "QCgetStringParameter Unsupported key "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Return null"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    const/4 v2, 0x0

    .line 514
    :goto_0
    return-object v2

    .line 506
    :cond_0
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 507
    .local v1, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 508
    .local v0, "reply":Landroid/os/Parcel;
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 509
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 510
    invoke-virtual {p0, v1, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V

    .line 511
    invoke-virtual {v0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 512
    .local v2, "ret":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 513
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/qualcomm/qcmedia/QCMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnMPDAttributeListener()V

    return-void
.end method

.method static synthetic access$100(Lcom/qualcomm/qcmedia/QCMediaPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnPreparedListener()V

    return-void
.end method

.method static synthetic access$200(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCTimedText;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;
    .param p1, "x1"    # Lcom/qualcomm/qcmedia/QCTimedText;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->callQCTimedTextListener(Lcom/qualcomm/qcmedia/QCTimedText;)V

    return-void
.end method

.method static synthetic access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/qualcomm/qcmedia/QCMediaPlayer;ILandroid/os/Parcel;)V
    .locals 0
    .param p0, "x0"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/os/Parcel;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnQOEEventListener(ILandroid/os/Parcel;)V

    return-void
.end method

.method private callOnMPDAttributeListener()V
    .locals 3

    .prologue
    .line 164
    const-string v1, "QCMediaPlayer"

    const-string v2, "callOnMPDAttributeListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnMPDAttributeListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

    if-eqz v1, :cond_0

    .line 167
    const/16 v1, 0x1f4a

    invoke-direct {p0, v1}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCgetStringParameter(I)Ljava/lang/String;

    move-result-object v0

    .line 168
    .local v0, "mpdAttributes":Ljava/lang/String;
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnMPDAttributeListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

    const/16 v2, 0x1f42

    invoke-interface {v1, v2, v0, p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;->onMPDAttribute(ILjava/lang/String;Lcom/qualcomm/qcmedia/QCMediaPlayer;)V

    .line 170
    .end local v0    # "mpdAttributes":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private callOnPreparedListener()V
    .locals 2

    .prologue
    .line 157
    const-string v0, "QCMediaPlayer"

    const-string v1, "callOnPreparedListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mQCOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mQCOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-interface {v0, p0}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    .line 160
    :cond_0
    return-void
.end method

.method private callOnQOEEventListener(ILandroid/os/Parcel;)V
    .locals 2
    .param p1, "key"    # I
    .param p2, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 180
    const-string v0, "QCMediaPlayer"

    const-string v1, "callOnQOEEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    invoke-interface {v0, p1, p2, p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;->onQOEAttribute(ILandroid/os/Parcel;Lcom/qualcomm/qcmedia/QCMediaPlayer;)V

    .line 185
    :cond_0
    return-void
.end method

.method private callQCTimedTextListener(Lcom/qualcomm/qcmedia/QCTimedText;)V
    .locals 1
    .param p1, "text"    # Lcom/qualcomm/qcmedia/QCTimedText;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    invoke-interface {v0, p0, p1}, Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;->onQCTimedText(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCTimedText;)V

    .line 177
    :cond_0
    return-void
.end method


# virtual methods
.method public QCGetParameter(I)Ljava/lang/String;
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 301
    const/16 v0, 0x1f42

    if-ne p1, v0, :cond_0

    .line 303
    const/16 p1, 0x1f4a

    .line 306
    :cond_0
    invoke-direct {p0, p1}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCgetStringParameter(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public QCPeriodicParameter(I)Landroid/os/Parcel;
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 310
    iget-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    const/16 v0, 0x1f48

    if-ne p1, v0, :cond_0

    .line 312
    invoke-virtual {p0, p1}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCgetParcelParameter(I)Landroid/os/Parcel;

    move-result-object v0

    .line 314
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public QCSetParameter(II)Z
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 318
    const-string v0, "QCMediaPlayer"

    const-string v1, "QCMediaPlayer : QCSetParameter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    invoke-virtual {p0, p1, p2}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCsetParameter(II)Z

    move-result v0

    return v0
.end method

.method public QCgetParcelParameter(I)Landroid/os/Parcel;
    .locals 5
    .param p1, "key"    # I

    .prologue
    .line 545
    sget-object v2, Lcom/qualcomm/qcmedia/QCMediaPlayer;->VALID_GET_PARAM_KEYS:Ljava/util/HashSet;

    new-instance v3, Ljava/lang/Integer;

    invoke-direct {v3, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 546
    const-string v2, "QCMediaPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "QCgetParcelParameter Unsupported key "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Return null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    const/4 v0, 0x0

    .line 554
    :goto_0
    return-object v0

    .line 549
    :cond_0
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 550
    .local v1, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 551
    .local v0, "reply":Landroid/os/Parcel;
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 552
    invoke-virtual {p0, v1, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V

    .line 553
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0
.end method

.method public QCsetParameter(II)Z
    .locals 4
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 531
    const/4 v2, 0x0

    .line 532
    .local v2, "retval":Z
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 533
    .local v1, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 534
    .local v0, "reply":Landroid/os/Parcel;
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 535
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 536
    invoke-virtual {p0, v1, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V

    .line 537
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 538
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 539
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 540
    return v2

    .line 537
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public QCsetStringParameter(ILjava/lang/String;)Z
    .locals 4
    .param p1, "key"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 518
    const/4 v2, 0x0

    .line 519
    .local v2, "retval":Z
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v1

    .line 520
    .local v1, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 521
    .local v0, "reply":Landroid/os/Parcel;
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 522
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 523
    invoke-virtual {p0, v1, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V

    .line 524
    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v2, 0x1

    .line 525
    :goto_0
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 526
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 527
    return v2

    .line 524
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public TurnOnOffTimedTextListener()V
    .locals 4

    .prologue
    .line 147
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    sget-object v2, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    if-eq v1, v2, :cond_0

    .line 149
    iget-object v1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    .line 150
    .local v0, "val":I
    :goto_0
    const-string v1, "QCMediaPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TurnOnOffTimedTextListener set listener flag"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/16 v1, 0x1770

    invoke-virtual {p0, v1, v0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCsetParameter(II)Z

    .line 153
    .end local v0    # "val":I
    :cond_0
    return-void

    .line 149
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public processMPDAttribute(ILjava/lang/String;)Z
    .locals 2
    .param p1, "key"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 292
    const/4 v0, 0x0

    .line 293
    .local v0, "retval":Z
    const/16 v1, 0x1f42

    if-ne p1, v1, :cond_0

    .line 295
    const/16 p1, 0x1f4b

    .line 297
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->QCsetStringParameter(ILjava/lang/String;)Z

    move-result v1

    return v1
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 130
    const-string v0, "QCMediaPlayer"

    const-string v1, "release"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    iput-object v2, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mQCOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 133
    iput-object v2, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnMPDAttributeListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

    .line 134
    iput-object v2, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    .line 135
    iput-object v2, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    .line 137
    invoke-super {p0}, Landroid/media/MediaPlayer;->release()V

    .line 139
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 140
    return-void
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "QCMediaPlayer"

    const-string v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    invoke-super {p0}, Landroid/media/MediaPlayer;->reset()V

    .line 126
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 127
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 90
    const-string v0, "QCMediaPlayer"

    const-string v1, "setDataSource"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-super {p0, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 92
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 93
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->TurnOnOffTimedTextListener()V

    .line 94
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 99
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "QCMediaPlayer"

    const-string v1, "setDataSource"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    invoke-super {p0, p1, p2, p3}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 101
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 102
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->TurnOnOffTimedTextListener()V

    .line 103
    return-void
.end method

.method public setDataSource(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 108
    const-string v0, "QCMediaPlayer"

    const-string v1, "setDataSource"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-super {p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 111
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->TurnOnOffTimedTextListener()V

    .line 112
    return-void
.end method

.method public setDataSource(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/SecurityException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 117
    .local p2, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v0, "QCMediaPlayer"

    const-string v1, "setDataSource"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    invoke-super {p0, p1, p2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    .line 119
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    iput-object v0, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->ePlayerState:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 120
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->TurnOnOffTimedTextListener()V

    .line 121
    return-void
.end method

.method public setOnMPDAttributeListener(Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

    .prologue
    .line 277
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnMPDAttributeListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;

    .line 278
    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/media/MediaPlayer$OnPreparedListener;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mQCOnPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 231
    const-string v0, "QCMediaPlayer"

    const-string v1, "setOnPreparedListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    return-void
.end method

.method public setOnQCTimedTextListener(Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    .prologue
    .line 215
    const-string v0, "QCMediaPlayer"

    const-string v1, "setOnQCTimedTextListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 217
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    .line 218
    invoke-virtual {p0}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->TurnOnOffTimedTextListener()V

    .line 219
    return-void
.end method

.method public setOnQOEEventListener(Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    .prologue
    .line 354
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    .line 355
    return-void
.end method
