.class public interface abstract Lcom/qualcomm/qcmedia/QCMediaPlayer$OnMPDAttributeListener;
.super Ljava/lang/Object;
.source "QCMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcmedia/QCMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnMPDAttributeListener"
.end annotation


# static fields
.field public static final ATTRIBUTES_TYPE_MPD:I = 0x1f42

.field public static final ATTRIBUTES_WHOLE_MPD:I = 0x1f43

.field public static final INVOKE_ID_GET_ATTRIBUTES_TYPE_MPD:I = 0x1f4a

.field public static final INVOKE_ID_SET_ATTRIBUTES_TYPE_MPD:I = 0x1f4b


# virtual methods
.method public abstract onMPDAttribute(ILjava/lang/String;Lcom/qualcomm/qcmedia/QCMediaPlayer;)V
.end method
