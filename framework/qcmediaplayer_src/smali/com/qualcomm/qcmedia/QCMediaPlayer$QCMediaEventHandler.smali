.class Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;
.super Landroid/os/Handler;
.source "QCMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcmedia/QCMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QCMediaEventHandler"
.end annotation


# instance fields
.field private mQCMediaPlayer:Lcom/qualcomm/qcmedia/QCMediaPlayer;

.field final synthetic this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;


# direct methods
.method public constructor <init>(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCMediaPlayer;Landroid/os/Looper;)V
    .locals 2
    .param p2, "mp"    # Lcom/qualcomm/qcmedia/QCMediaPlayer;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 411
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .line 412
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 413
    const-string v0, "QCMediaPlayer"

    const-string v1, "QCMediaEventHandler calling mp.mEventHandler.sendMessage()m"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    iput-object p2, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->mQCMediaPlayer:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    .line 415
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 419
    const-string v3, "QCMediaPlayer"

    const-string v4, "QCMediaPlayer::QCMediaEventHandler::handleMessage"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    iget v3, p1, Landroid/os/Message;->what:I

    sparse-switch v3, :sswitch_data_0

    .line 468
    const-string v3, "QCMediaPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown message type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    :cond_0
    :goto_0
    return-void

    .line 423
    :sswitch_0
    const-string v3, "QCMediaPlayer"

    const-string v4, "QCMediaEventHandler::handleMessage::MEDIA_PREPARED calling callOnMPDAttributeListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # invokes: Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnMPDAttributeListener()V
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$000(Lcom/qualcomm/qcmedia/QCMediaPlayer;)V

    .line 425
    const-string v3, "QCMediaPlayer"

    const-string v4, "QCMediaEventHandler::handleMessage::MEDIA_PREPARED calling callOnPreparedListener"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # invokes: Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnPreparedListener()V
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$100(Lcom/qualcomm/qcmedia/QCMediaPlayer;)V

    goto :goto_0

    .line 430
    :sswitch_1
    const-string v3, "QCMediaPlayer"

    const-string v4, "QCMediaEventHandler::handleMessage::MEDIA_TIMED_TEXT"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQCTimedTextListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$200(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQCTimedTextListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 433
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v3, v3, Landroid/os/Parcel;

    if-eqz v3, :cond_0

    .line 434
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Parcel;

    .line 435
    .local v1, "parcel":Landroid/os/Parcel;
    new-instance v2, Lcom/qualcomm/qcmedia/QCTimedText;

    invoke-direct {v2, v1}, Lcom/qualcomm/qcmedia/QCTimedText;-><init>(Landroid/os/Parcel;)V

    .line 436
    .local v2, "text":Lcom/qualcomm/qcmedia/QCTimedText;
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # invokes: Lcom/qualcomm/qcmedia/QCMediaPlayer;->callQCTimedTextListener(Lcom/qualcomm/qcmedia/QCTimedText;)V
    invoke-static {v3, v2}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$300(Lcom/qualcomm/qcmedia/QCMediaPlayer;Lcom/qualcomm/qcmedia/QCTimedText;)V

    goto :goto_0

    .line 442
    .end local v1    # "parcel":Landroid/os/Parcel;
    .end local v2    # "text":Lcom/qualcomm/qcmedia/QCTimedText;
    :sswitch_2
    const-string v3, "QCMediaPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "QCMediaEventHandler::handleMessage::MEDIA_QOE Received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 445
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v3, v3, Landroid/os/Parcel;

    if-eqz v3, :cond_0

    .line 447
    const/4 v0, 0x0

    .line 448
    .local v0, "key":I
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/os/Parcel;

    .line 449
    .restart local v1    # "parcel":Landroid/os/Parcel;
    iget v3, p1, Landroid/os/Message;->arg2:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 451
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    const/16 v0, 0x1f45

    .line 462
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # invokes: Lcom/qualcomm/qcmedia/QCMediaPlayer;->callOnQOEEventListener(ILandroid/os/Parcel;)V
    invoke-static {v3, v0, v1}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$500(Lcom/qualcomm/qcmedia/QCMediaPlayer;ILandroid/os/Parcel;)V

    goto :goto_0

    .line 452
    :cond_2
    iget v3, p1, Landroid/os/Message;->arg2:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_3

    .line 454
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    const/16 v0, 0x1f48

    goto :goto_1

    .line 455
    :cond_3
    iget v3, p1, Landroid/os/Message;->arg2:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 457
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    const/16 v0, 0x1f47

    goto :goto_1

    .line 458
    :cond_4
    iget v3, p1, Landroid/os/Message;->arg2:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 460
    iget-object v3, p0, Lcom/qualcomm/qcmedia/QCMediaPlayer$QCMediaEventHandler;->this$0:Lcom/qualcomm/qcmedia/QCMediaPlayer;

    # getter for: Lcom/qualcomm/qcmedia/QCMediaPlayer;->mOnQOEEventListener:Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
    invoke-static {v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer;->access$400(Lcom/qualcomm/qcmedia/QCMediaPlayer;)Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;

    const/16 v0, 0x1f46

    goto :goto_1

    .line 420
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x63 -> :sswitch_1
        0x12c -> :sswitch_2
    .end sparse-switch
.end method
