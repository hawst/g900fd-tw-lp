.class public Lcom/qualcomm/qcmedia/QCTimedText$Style;
.super Ljava/lang/Object;
.source "QCTimedText.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcmedia/QCTimedText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Style"
.end annotation


# instance fields
.field public colorRGBA:I

.field public endChar:I

.field public fontID:I

.field public fontSize:I

.field public isBold:Z

.field public isItalic:Z

.field public isUnderlined:Z

.field public startChar:I

.field final synthetic this$0:Lcom/qualcomm/qcmedia/QCTimedText;


# direct methods
.method public constructor <init>(Lcom/qualcomm/qcmedia/QCTimedText;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 264
    iput-object p1, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->this$0:Lcom/qualcomm/qcmedia/QCTimedText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput v0, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->startChar:I

    .line 230
    iput v0, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->endChar:I

    .line 236
    iput v0, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->fontID:I

    .line 241
    iput-boolean v1, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isBold:Z

    .line 246
    iput-boolean v1, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isItalic:Z

    .line 251
    iput-boolean v1, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->isUnderlined:Z

    .line 256
    iput v0, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->fontSize:I

    .line 262
    iput v0, p0, Lcom/qualcomm/qcmedia/QCTimedText$Style;->colorRGBA:I

    .line 264
    return-void
.end method
