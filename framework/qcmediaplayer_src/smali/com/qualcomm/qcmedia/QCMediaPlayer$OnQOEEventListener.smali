.class public interface abstract Lcom/qualcomm/qcmedia/QCMediaPlayer$OnQOEEventListener;
.super Ljava/lang/Object;
.source "QCMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcmedia/QCMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnQOEEventListener"
.end annotation


# static fields
.field public static final ATTRIBUTES_QOE_EVENT_PERIODIC:I = 0x1f48

.field public static final ATTRIBUTES_QOE_EVENT_PLAY:I = 0x1f45

.field public static final ATTRIBUTES_QOE_EVENT_REG:I = 0x1f44

.field public static final ATTRIBUTES_QOE_EVENT_STOP:I = 0x1f46

.field public static final ATTRIBUTES_QOE_EVENT_SWITCH:I = 0x1f47


# virtual methods
.method public abstract onQOEAttribute(ILandroid/os/Parcel;Lcom/qualcomm/qcmedia/QCMediaPlayer;)V
.end method
