.class final enum Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;
.super Ljava/lang/Enum;
.source "QCMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/qualcomm/qcmedia/QCMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "MediaPlayerState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

.field public static final enum PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

.field public static final enum PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 401
    new-instance v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    const-string v1, "PLAYER_IDLE"

    invoke-direct {v0, v1, v2}, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 402
    new-instance v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    const-string v1, "PLAYER_INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    .line 400
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    sget-object v1, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_IDLE:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->PLAYER_INITIALIZED:Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->$VALUES:[Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 400
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 400
    const-class v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    return-object v0
.end method

.method public static values()[Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;
    .locals 1

    .prologue
    .line 400
    sget-object v0, Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->$VALUES:[Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    invoke-virtual {v0}, [Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/qualcomm/qcmedia/QCMediaPlayer$MediaPlayerState;

    return-object v0
.end method
