.class public Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;
.super Ljava/lang/Object;
.source "CocktailBarPolicy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "CocktailBarPolicy"

.field private static mInstance:Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;


# instance fields
.field private mCocktailMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mInstance:Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    return-void
.end method

.method public static getInstance()Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mInstance:Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;

    if-nez v0, :cond_0

    .line 23
    new-instance v0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;

    invoke-direct {v0}, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;-><init>()V

    sput-object v0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mInstance:Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;

    .line 25
    :cond_0
    sget-object v0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mInstance:Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;

    return-object v0
.end method


# virtual methods
.method public canCloseCocktail(Lcom/samsung/android/cocktailbar/Cocktail;Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;)Z
    .locals 6
    .param p1, "cocktail"    # Lcom/samsung/android/cocktailbar/Cocktail;
    .param p2, "cocktailExtraInfo"    # Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;
    .param p3, "settings"    # Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 165
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getProviderInfo()Lcom/samsung/android/cocktailbar/CocktailProviderInfo;

    move-result-object v1

    .line 166
    .local v1, "info":Lcom/samsung/android/cocktailbar/CocktailProviderInfo;
    if-eqz v1, :cond_4

    .line 167
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v0

    .line 168
    .local v0, "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    if-eqz v0, :cond_0

    .line 169
    invoke-virtual {v0}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 180
    :cond_0
    :pswitch_0
    iget v4, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    if-eqz v4, :cond_3

    .line 181
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v4, v5, :cond_2

    .line 219
    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_1
    :goto_0
    return v2

    .line 173
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :pswitch_1
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    .line 176
    goto :goto_0

    :cond_2
    move v2, v3

    .line 184
    goto :goto_0

    .line 186
    :cond_3
    iget v4, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->category:I

    sparse-switch v4, :sswitch_data_0

    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_4
    move v2, v3

    .line 219
    goto :goto_0

    .line 189
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :sswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v4

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 190
    sget-object v4, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NOTHING:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    .line 191
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v4

    invoke-virtual {p3, v4}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v4

    if-nez v4, :cond_4

    goto :goto_0

    .line 197
    :sswitch_1
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_QUICKTOOL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v4, v5, :cond_5

    .line 198
    sget-object v3, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NOTHING:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v3, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    :cond_5
    move v2, v3

    .line 201
    goto :goto_0

    .line 204
    :sswitch_2
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_TABLE_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v4, v5, :cond_6

    .line 205
    sget-object v3, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NOTHING:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v3, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    :cond_6
    move v2, v3

    .line 208
    goto :goto_0

    .line 211
    :sswitch_3
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NIGHT_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v4, v5, :cond_7

    .line 212
    sget-object v3, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NOTHING:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v3, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    :cond_7
    move v2, v3

    .line 215
    goto :goto_0

    .line 169
    :pswitch_data_0
    .packed-switch 0x10001
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 186
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x20 -> :sswitch_2
        0x80 -> :sswitch_3
    .end sparse-switch
.end method

.method public canSendUpdateIntent(Lcom/samsung/android/cocktailbar/Cocktail;Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;)Z
    .locals 1
    .param p1, "cocktail"    # Lcom/samsung/android/cocktailbar/Cocktail;
    .param p2, "settings"    # Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    const/4 v0, 0x1

    .line 40
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canShowCocktail(Lcom/samsung/android/cocktailbar/Cocktail;Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;)Z
    .locals 6
    .param p1, "cocktail"    # Lcom/samsung/android/cocktailbar/Cocktail;
    .param p2, "cocktailExtraInfo"    # Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;
    .param p3, "settings"    # Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 117
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getProviderInfo()Lcom/samsung/android/cocktailbar/CocktailProviderInfo;

    move-result-object v1

    .line 118
    .local v1, "info":Lcom/samsung/android/cocktailbar/CocktailProviderInfo;
    if-eqz v1, :cond_4

    .line 119
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v0

    .line 120
    .local v0, "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {v0}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 132
    :cond_0
    :pswitch_0
    iget v4, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    if-eqz v4, :cond_3

    .line 133
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v4, v5, :cond_2

    .line 161
    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_1
    :goto_0
    return v2

    .line 125
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :pswitch_1
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    .line 128
    goto :goto_0

    :cond_2
    move v2, v3

    .line 136
    goto :goto_0

    .line 138
    :cond_3
    iget v4, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->category:I

    sparse-switch v4, :sswitch_data_0

    .line 158
    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v4

    invoke-virtual {p3, v4}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v4

    if-nez v4, :cond_1

    move v2, v3

    .line 161
    goto :goto_0

    .line 141
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :sswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v4

    const/high16 v5, 0x10000

    and-int/2addr v4, v5

    if-eqz v4, :cond_4

    .line 142
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v4

    invoke-virtual {p3, v4}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    .line 146
    goto :goto_0

    .line 151
    :sswitch_1
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_QUICKTOOL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    goto :goto_0

    .line 153
    :sswitch_2
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_TABLE_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    goto :goto_0

    .line 155
    :sswitch_3
    iget-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NIGHT_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v4, v5, :cond_1

    move v2, v3

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x10001
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 138
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x20 -> :sswitch_2
        0x80 -> :sswitch_3
    .end sparse-switch
.end method

.method public canUpdateCocktail(Lcom/samsung/android/cocktailbar/Cocktail;Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;)Z
    .locals 8
    .param p1, "cocktail"    # Lcom/samsung/android/cocktailbar/Cocktail;
    .param p2, "cocktailExtraInfo"    # Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;
    .param p3, "settings"    # Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;

    .prologue
    const/high16 v6, 0x10000

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 44
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getProviderInfo()Lcom/samsung/android/cocktailbar/CocktailProviderInfo;

    move-result-object v1

    .line 45
    .local v1, "info":Lcom/samsung/android/cocktailbar/CocktailProviderInfo;
    if-eqz v1, :cond_6

    .line 46
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v0

    .line 47
    .local v0, "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {v0}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 56
    :cond_0
    :pswitch_0
    iget v5, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    if-eqz v5, :cond_3

    .line 57
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils;->COCKTAIL_PRIVATE_MODE_STATE:[Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iget v7, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    aget-object v6, v6, v7

    if-ne v5, v6, :cond_2

    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils;->COCKTAIL_PRIVATE_MODE:[Ljava/lang/String;

    iget v6, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    aget-object v5, v5, v6

    iget-object v6, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->privateMode:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 113
    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_1
    :goto_0
    return v3

    .line 52
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :pswitch_1
    sget-object v4, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    .line 61
    :cond_2
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    move v3, v4

    .line 64
    goto :goto_0

    .line 67
    :cond_3
    iget-object v5, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->privateMode:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 68
    iget-object v5, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->privateMode:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils;->getCocktailUpdateStateByPrivateMode(Ljava/lang/String;)Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    move-result-object v2

    .line 69
    .local v2, "updateState":Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;
    sget-object v5, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NOTHING:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-ne v2, v5, :cond_4

    move v3, v4

    .line 70
    goto :goto_0

    .line 72
    :cond_4
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v2, :cond_1

    move v3, v4

    .line 75
    goto :goto_0

    .line 77
    .end local v2    # "updateState":Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;
    :cond_5
    iget v5, v1, Lcom/samsung/android/cocktailbar/CocktailProviderInfo;->category:I

    sparse-switch v5, :sswitch_data_0

    .line 109
    .end local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :cond_6
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v5

    invoke-virtual {p3, v5}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NORMAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    :cond_7
    move v3, v4

    .line 113
    goto :goto_0

    .line 80
    .restart local v0    # "cInfo":Lcom/samsung/android/cocktailbar/CocktailInfo;
    :sswitch_0
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailId()I

    move-result v5

    invoke-virtual {p3, v5}, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailBarSettings;->isEnabledCocktail(I)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 81
    sget-object v4, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NORMAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    .line 83
    :cond_8
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v5

    and-int/2addr v5, v6

    if-eqz v5, :cond_9

    .line 84
    sget-object v4, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    iput-object v4, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    goto :goto_0

    .line 87
    :cond_9
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_CONTEXTUAL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    move v3, v4

    .line 90
    goto :goto_0

    .line 93
    :sswitch_1
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v5

    and-int/2addr v5, v6

    if-nez v5, :cond_1

    .line 96
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_QUICKTOOL:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    move v3, v4

    goto :goto_0

    .line 98
    :sswitch_2
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v5

    and-int/2addr v5, v6

    if-nez v5, :cond_1

    .line 101
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_TABLE_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    move v3, v4

    goto/16 :goto_0

    .line 103
    :sswitch_3
    invoke-virtual {p1}, Lcom/samsung/android/cocktailbar/Cocktail;->getCocktailInfo()Lcom/samsung/android/cocktailbar/CocktailInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/cocktailbar/CocktailInfo;->getCategory()I

    move-result v5

    and-int/2addr v5, v6

    if-nez v5, :cond_1

    .line 106
    iget-object v5, p2, Lcom/android/server/cocktailbar/CocktailBarManagerServiceImpl$CocktailExtraInfo;->isCoctailUpdated:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    sget-object v6, Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;->COCKTAIL_UPDATED_NIGHT_MODE:Lcom/android/server/cocktailbar/utils/CocktailBarUtils$CocktailUpdatedState;

    if-eq v5, v6, :cond_1

    move v3, v4

    goto/16 :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x10001
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 77
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x20 -> :sswitch_2
        0x80 -> :sswitch_3
    .end sparse-switch
.end method

.method public getCocktailMode()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    return v0
.end method

.method public isAllowTransientBarCocktailBar()Z
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    sparse-switch v0, :sswitch_data_0

    .line 228
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 226
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 223
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
    .end sparse-switch
.end method

.method public setCocktailMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/android/server/cocktailbar/policy/CocktailBarPolicy;->mCocktailMode:I

    .line 30
    return-void
.end method
