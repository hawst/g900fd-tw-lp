.class public Lcom/android/server/am/PreferredPackageManager;
.super Ljava/lang/Object;
.source "PreferredPackageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;,
        Lcom/android/server/am/PreferredPackageManager$PPMLogFormatter;,
        Lcom/android/server/am/PreferredPackageManager$PreferredPackageCounter;,
        Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;
    }
.end annotation


# static fields
.field static final ACTIVITY_PAUSED:I = 0x5

.field static final ACTIVITY_RESUME_FINISHED:I = 0x4

.field static final ACTIVITY_RESUME_STARTED:I = 0x3

.field static DEBUG:Z = false

.field static DEBUG_LOGFILE_ENABLE:Z = false

.field static DEBUG_TRACE:Z = false

.field private static final DEFAULT_PREFERRED_PACKAGES_INFO:Ljava/lang/String; = "/system/etc/preferred.xml"

.field static final FIRST_START_PACKAGE_NUMBER:I = 0x5

.field static final PREFERRED_COUNT_DEVIDE_TERM:I = 0x5265c00

.field static final PREFERRED_DATA_SAVE_TERM:I = 0x1b7740

.field static final PREFERRED_MAX_COUNT:I = 0x32

.field private static final PREFFERED_PACKAGES_INFO:Ljava/lang/String; = "/data/system/preferred.xml"

.field static final SET_PREFERRED_PACKAGE_PSS_MSG:I = 0x2

.field static final START_TOP_PREFERRED_PACKAGES:I = 0x1

.field static final TAG:Ljava/lang/String; = "PreferredPackage"

.field static final WRITE_PREFERRED_DATA_MSG:I = 0x6

.field private static mInstance:Lcom/android/server/am/PreferredPackageManager;


# instance fields
.field private final BLACK_LIST:[Ljava/lang/String;

.field final DELAY_FROM_ACTVITY_RESUME_TO_PREFERRED_START:I

.field private final INSTRUMENTATION_LIST:[Ljava/lang/String;

.field private final LOG_FILE_NAME:Ljava/lang/String;

.field final MAX_CANDIDATE_LIMIT:I

.field final MAX_PREF_STARTED_EMPTY_LIMIT:I

.field final SCORE_FOR_INDIRECT_RELATION:I

.field final SCORE_FOR_PREV:I

.field final SCORE_FOR_PREV_PREV:I

.field private mBgCount:I

.field private mFlags:I

.field private mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mLogger:Ljava/util/logging/Logger;

.field private mLowMemState:Z

.field private mOldAvailMemSize:J

.field private mOldBgCount:I

.field mPkgNameOfPrevActivity:Ljava/lang/String;

.field mPreferredPackages:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;",
            ">;"
        }
    .end annotation
.end field

.field mPreferredStartedEmptyList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mPrevPkgName:Ljava/lang/String;

.field mPrevPrevPkgName:Ljava/lang/String;

.field private mSamePackage:Z

.field private mScanning:Z

.field private mService:Lcom/android/server/am/ActivityManagerService;

.field mToStartList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mTopPreferredPackages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserId:I

.field resumeLock:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    .line 81
    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    .line 82
    sget-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    or-int/lit8 v0, v0, 0x1

    sput-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    .line 154
    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/am/PreferredPackageManager;->mInstance:Lcom/android/server/am/PreferredPackageManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x2

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-array v5, v9, [Ljava/lang/String;

    const-string v6, "com.android.contacts"

    aput-object v6, v5, v8

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->BLACK_LIST:[Ljava/lang/String;

    .line 89
    const/16 v5, 0xf

    new-array v5, v5, [Ljava/lang/String;

    const-string v6, "com.sec.android.app.popupuireceiver"

    aput-object v6, v5, v8

    const-string v6, "com.sec.android.app.servicemodeapp"

    aput-object v6, v5, v9

    const-string v6, "com.sec.android.app.setupwizard"

    aput-object v6, v5, v11

    const/4 v6, 0x3

    const-string v7, "com.sec.keystringscreen"

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-string v7, "com.salab.act"

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const-string v7, "com.sec.android.app.tinym"

    aput-object v7, v5, v6

    const/4 v6, 0x6

    const-string v7, "com.android.packageinstaller"

    aput-object v7, v5, v6

    const/4 v6, 0x7

    const-string v7, "com.sec.android.app.SysDump"

    aput-object v7, v5, v6

    const/16 v6, 0x8

    const-string v7, "com.google.android.gsf.login"

    aput-object v7, v5, v6

    const/16 v6, 0x9

    const-string v7, "com.android.phone"

    aput-object v7, v5, v6

    const/16 v6, 0xa

    const-string v7, "com.samsung.android.app.headlines"

    aput-object v7, v5, v6

    const/16 v6, 0xb

    const-string v7, "com.sec.android.app.videoplayer"

    aput-object v7, v5, v6

    const/16 v6, 0xc

    const-string v7, "com.sec.android.app.camera"

    aput-object v7, v5, v6

    const/16 v6, 0xd

    const-string v7, "com.sec.android.app.parser"

    aput-object v7, v5, v6

    const/16 v6, 0xe

    const-string v7, "com.loaddata.ged"

    aput-object v7, v5, v6

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->INSTRUMENTATION_LIST:[Ljava/lang/String;

    .line 109
    const-string v5, "/data/log/ppm_log.log"

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->LOG_FILE_NAME:Ljava/lang/String;

    .line 112
    iput-boolean v8, p0, Lcom/android/server/am/PreferredPackageManager;->mSamePackage:Z

    .line 113
    const-wide/16 v6, -0x1

    iput-wide v6, p0, Lcom/android/server/am/PreferredPackageManager;->mOldAvailMemSize:J

    .line 114
    iput v10, p0, Lcom/android/server/am/PreferredPackageManager;->mOldBgCount:I

    .line 115
    iput v8, p0, Lcom/android/server/am/PreferredPackageManager;->mBgCount:I

    .line 117
    iput-boolean v8, p0, Lcom/android/server/am/PreferredPackageManager;->mLowMemState:Z

    .line 119
    iput v10, p0, Lcom/android/server/am/PreferredPackageManager;->mFlags:I

    .line 120
    iput v10, p0, Lcom/android/server/am/PreferredPackageManager;->mUserId:I

    .line 122
    iput-boolean v8, p0, Lcom/android/server/am/PreferredPackageManager;->mScanning:Z

    .line 124
    iput-object v12, p0, Lcom/android/server/am/PreferredPackageManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 125
    iput-object v12, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    .line 127
    new-instance v5, Ljava/lang/Object;

    invoke-direct {v5}, Ljava/lang/Object;-><init>()V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->resumeLock:Ljava/lang/Object;

    .line 132
    iput v11, p0, Lcom/android/server/am/PreferredPackageManager;->MAX_PREF_STARTED_EMPTY_LIMIT:I

    .line 134
    const/16 v5, 0x1f4

    iput v5, p0, Lcom/android/server/am/PreferredPackageManager;->DELAY_FROM_ACTVITY_RESUME_TO_PREFERRED_START:I

    .line 138
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    .line 141
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    .line 142
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    .line 143
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    .line 145
    const-string v5, ""

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    .line 146
    const-string v5, ""

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    .line 147
    const-string v5, ""

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    .line 149
    iput v11, p0, Lcom/android/server/am/PreferredPackageManager;->SCORE_FOR_PREV:I

    .line 150
    iput v9, p0, Lcom/android/server/am/PreferredPackageManager;->SCORE_FOR_PREV_PREV:I

    .line 151
    iput v9, p0, Lcom/android/server/am/PreferredPackageManager;->SCORE_FOR_INDIRECT_RELATION:I

    .line 152
    const/4 v5, 0x5

    iput v5, p0, Lcom/android/server/am/PreferredPackageManager;->MAX_CANDIDATE_LIMIT:I

    .line 157
    new-instance v5, Landroid/os/HandlerThread;

    const-string v6, "PackageScanner"

    invoke-direct {v5, v6}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mHandlerThread:Landroid/os/HandlerThread;

    .line 158
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v5}, Landroid/os/HandlerThread;->start()V

    .line 159
    new-instance v5, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    iget-object v6, p0, Lcom/android/server/am/PreferredPackageManager;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, p0, v6}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;-><init>(Lcom/android/server/am/PreferredPackageManager;Landroid/os/Looper;)V

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    .line 161
    sget-boolean v5, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v5, :cond_0

    .line 162
    const-string/jumbo v5, "ppm"

    invoke-static {v5}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mLogger:Ljava/util/logging/Logger;

    .line 164
    :try_start_0
    new-instance v1, Ljava/util/logging/FileHandler;

    const-string v5, "/data/log/ppm_log.log"

    invoke-direct {v1, v5}, Ljava/util/logging/FileHandler;-><init>(Ljava/lang/String;)V

    .line 165
    .local v1, "handler":Ljava/util/logging/Handler;
    new-instance v0, Lcom/android/server/am/PreferredPackageManager$PPMLogFormatter;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lcom/android/server/am/PreferredPackageManager$PPMLogFormatter;-><init>(Lcom/android/server/am/PreferredPackageManager;Lcom/android/server/am/PreferredPackageManager$1;)V

    .line 166
    .local v0, "formatter":Ljava/util/logging/Formatter;
    invoke-virtual {v1, v0}, Ljava/util/logging/Handler;->setFormatter(Ljava/util/logging/Formatter;)V

    .line 167
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mLogger:Ljava/util/logging/Logger;

    invoke-virtual {v5, v1}, Ljava/util/logging/Logger;->addHandler(Ljava/util/logging/Handler;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 177
    .end local v0    # "formatter":Ljava/util/logging/Formatter;
    .end local v1    # "handler":Ljava/util/logging/Handler;
    :cond_0
    :goto_0
    return-void

    .line 168
    :catch_0
    move-exception v3

    .line 169
    .local v3, "ioe":Ljava/io/IOException;
    const-string v5, "PreferredPackage"

    const-string v6, "I/O error occurs while opening /data/log/ppm_log.log"

    invoke-static {v5, v6}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 170
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 171
    .local v2, "iae":Ljava/lang/IllegalArgumentException;
    const-string v5, "PreferredPackage"

    const-string v6, "IllegalArgumentException occurs while opening /data/log/ppm_log.log"

    invoke-static {v5, v6}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    .end local v2    # "iae":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v4

    .line 173
    .local v4, "npe":Ljava/lang/NullPointerException;
    const-string v5, "PreferredPackage"

    const-string v6, "NullPointerException occurs while opening /data/log/ppm_log.log"

    invoke-static {v5, v6}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/PreferredPackageManager;->handleActivityResumeFinished(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 77
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/am/PreferredPackageManager;->handleActivityPaused(Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/android/server/am/PreferredPackageManager;->handleStartTopPreferredPackages(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Lcom/android/server/am/PreferredPackageManager;->handleUpdatePreferredPackage(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/server/am/PreferredPackageManager;)Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/android/server/am/PreferredPackageManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/android/server/am/PreferredPackageManager;->mLowMemState:Z

    return v0
.end method

.method static synthetic access$700(Lcom/android/server/am/PreferredPackageManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/android/server/am/PreferredPackageManager;

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/android/server/am/PreferredPackageManager;->mScanning:Z

    return v0
.end method

.method public static getInstance()Lcom/android/server/am/PreferredPackageManager;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/android/server/am/PreferredPackageManager;->mInstance:Lcom/android/server/am/PreferredPackageManager;

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Lcom/android/server/am/PreferredPackageManager;

    invoke-direct {v0}, Lcom/android/server/am/PreferredPackageManager;-><init>()V

    sput-object v0, Lcom/android/server/am/PreferredPackageManager;->mInstance:Lcom/android/server/am/PreferredPackageManager;

    .line 183
    :cond_0
    sget-object v0, Lcom/android/server/am/PreferredPackageManager;->mInstance:Lcom/android/server/am/PreferredPackageManager;

    return-object v0
.end method

.method protected static getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 920
    const/4 v1, 0x0

    .line 921
    .local v1, "ret":Ljava/lang/String;
    const-string v0, "."

    .line 922
    .local v0, "delims":Ljava/lang/String;
    if-eqz p0, :cond_0

    .line 923
    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, p0, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    .local v2, "st":Ljava/util/StringTokenizer;
    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 925
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "ret":Ljava/lang/String;
    check-cast v1, Ljava/lang/String;

    .restart local v1    # "ret":Ljava/lang/String;
    goto :goto_0

    .line 928
    .end local v2    # "st":Ljava/util/StringTokenizer;
    :cond_0
    return-object v1
.end method

.method private handleActivityPaused(Ljava/lang/String;J)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "lastPauseTime"    # J

    .prologue
    .line 836
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "PreferredPackage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AppStatus : PAUSED          "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "(in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    :cond_0
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AppStatus : PAUSED          "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "(in "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 842
    :cond_1
    const/4 v1, 0x0

    .line 843
    .local v1, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v3

    .line 844
    :try_start_0
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    move-object v1, v0

    .line 845
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 846
    if-nez v1, :cond_2

    .line 848
    const-string v2, "PreferredPackage"

    const-string v3, "Preferred : cannot find resumed info.."

    invoke-static {v2, v3}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    :goto_0
    return-void

    .line 845
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 851
    :cond_2
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v2, :cond_3

    const-string v2, "PreferredPackage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Preferred : update lastPauseTime : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 852
    :cond_3
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Preferred : update lastPauseTime : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 853
    :cond_4
    invoke-virtual {v1, p2, p3}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->updateLastPauseTime(J)V

    .line 854
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->setState(I)V

    goto :goto_0
.end method

.method private handleActivityResumeFinished(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 798
    sget-boolean v4, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v4, :cond_0

    const-string v4, "PreferredPackage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "AppStatus : RESUME FINISHED "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    :cond_0
    sget-boolean v4, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AppStatus : RESUME FINISHED "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ") in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 804
    :cond_1
    const/4 v1, 0x1

    .line 805
    .local v1, "diff":Z
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    if-ne p1, v4, :cond_2

    const/4 v1, 0x0

    .line 807
    :cond_2
    sget-boolean v4, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v4, :cond_3

    const-string v4, "PreferredPackage"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "PackageHistory : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    invoke-static {v6}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-> "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", diff : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :cond_3
    sget-boolean v4, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "PackageHistory : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-> "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", diff : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 815
    :cond_4
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 816
    .local v3, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v3, :cond_5

    .line 833
    :goto_0
    return-void

    .line 817
    :cond_5
    invoke-virtual {v3, v7}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->setState(I)V

    .line 818
    if-eqz v1, :cond_6

    .line 820
    invoke-direct {p0, p1}, Lcom/android/server/am/PreferredPackageManager;->updateIndirectScore(Ljava/lang/String;)V

    .line 821
    invoke-direct {p0, p1}, Lcom/android/server/am/PreferredPackageManager;->increaseScore(Ljava/lang/String;)V

    .line 822
    invoke-direct {p0, p1, v7}, Lcom/android/server/am/PreferredPackageManager;->removeEmptyList(Ljava/lang/String;Z)V

    .line 825
    :cond_6
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    if-eq p1, v4, :cond_7

    .line 826
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    .line 827
    .local v2, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 828
    .local v0, "b":Landroid/os/Bundle;
    const-string v4, "name"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 829
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 830
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v4, v2, v6, v7}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 832
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_7
    iput-object p1, p0, Lcom/android/server/am/PreferredPackageManager;->mPkgNameOfPrevActivity:Ljava/lang/String;

    goto :goto_0
.end method

.method private handleStartTopPreferredPackages(Ljava/lang/String;)V
    .locals 12
    .param p1, "resumedPkgName"    # Ljava/lang/String;

    .prologue
    const-wide/16 v10, 0x40

    const/4 v8, 0x0

    .line 669
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    if-eqz v1, :cond_0

    const-string v1, "PPM_startTop"

    invoke-static {v10, v11, v1, v8}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    .line 671
    :cond_0
    iget-boolean v1, p0, Lcom/android/server/am/PreferredPackageManager;->mLowMemState:Z

    if-eqz v1, :cond_3

    .line 672
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "PreferredPackage"

    const-string v4, "Not enough bg procs. Don\'t start preferred procs"

    invoke-static {v1, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :cond_1
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v1, :cond_2

    const-string v1, "Not enough bg procs. Don\'t start preferred procs"

    invoke-virtual {p0, v1}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 698
    :cond_2
    :goto_0
    return-void

    .line 676
    :cond_3
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/am/PreferredPackageManager;->mScanning:Z

    .line 677
    iget v1, p0, Lcom/android/server/am/PreferredPackageManager;->mBgCount:I

    iput v1, p0, Lcom/android/server/am/PreferredPackageManager;->mOldBgCount:I

    .line 678
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 680
    .local v2, "startTime":J
    invoke-direct {p0, p1}, Lcom/android/server/am/PreferredPackageManager;->selectTopPreferredPkg(Ljava/lang/String;)I

    move-result v1

    const/4 v4, -0x1

    if-ne v1, v4, :cond_6

    .line 681
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_4

    const-string v1, "PreferredPackage"

    const-string v4, "Error selecting TopPP"

    invoke-static {v1, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 682
    :cond_4
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v1, :cond_5

    const-string v1, "Error selecting TopPP"

    invoke-virtual {p0, v1}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 683
    :cond_5
    iput-boolean v8, p0, Lcom/android/server/am/PreferredPackageManager;->mScanning:Z

    goto :goto_0

    .line 687
    :cond_6
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 689
    .local v0, "length":I
    invoke-virtual {p0}, Lcom/android/server/am/PreferredPackageManager;->startPreferredPackages()V

    .line 691
    const-string v1, "PreferredPackage"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "total bgcount : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/android/server/am/PreferredPackageManager;->mBgCount:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", TopPP size : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", spent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 694
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v1, :cond_7

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "total bgcount: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, p0, Lcom/android/server/am/PreferredPackageManager;->mBgCount:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " TopPP size : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 696
    :cond_7
    iput-boolean v8, p0, Lcom/android/server/am/PreferredPackageManager;->mScanning:Z

    .line 697
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    if-eqz v1, :cond_2

    const-string v1, "PPM_startTop"

    invoke-static {v10, v11, v1, v8}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    goto/16 :goto_0
.end method

.method private handleUpdatePreferredPackage(Ljava/lang/String;I)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .prologue
    .line 858
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 859
    .local v2, "sb":Ljava/lang/StringBuilder;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "process starts :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pid : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 860
    const/4 v1, 0x0

    .line 861
    .local v1, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v4

    .line 862
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    move-object v1, v0

    .line 863
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 864
    if-nez v1, :cond_1

    .line 866
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 867
    const-string v3, " -> cannot find in PP info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 868
    const-string v3, "PreferredPackage"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 869
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 882
    :cond_0
    :goto_0
    return-void

    .line 863
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 874
    :cond_1
    iput p2, v1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->pid:I

    .line 875
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->setState(I)V

    .line 876
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 877
    const-string v3, " -> State updated as BINDED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 878
    const-string v3, "PreferredPackage"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 879
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private increaseScore(Ljava/lang/String;)V
    .locals 5
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 932
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "PreferredPackage"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Scoring : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-static {v4}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    :cond_0
    sget-boolean v2, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scoring : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-static {v3}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " by "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 940
    :cond_1
    move-object v1, p1

    .line 941
    .local v1, "resumedPkgName":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 943
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 944
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 945
    .local v0, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-eqz v0, :cond_2

    .line 946
    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->doIncreaseScore(Ljava/lang/String;I)V

    .line 948
    .end local v0    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :cond_2
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 949
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 950
    .restart local v0    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-eqz v0, :cond_3

    .line 951
    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->doIncreaseScore(Ljava/lang/String;I)V

    .line 953
    .end local v0    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :cond_3
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    iput-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    .line 954
    iput-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    .line 956
    :cond_4
    return-void
.end method

.method private isBlackList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 729
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->BLACK_LIST:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 730
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 732
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    return v4

    .line 729
    .restart local v3    # "str":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 732
    .end local v3    # "str":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isInstrumentList(Ljava/lang/String;)Z
    .locals 5
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 736
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->INSTRUMENTATION_LIST:[Ljava/lang/String;

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 737
    .local v3, "str":Ljava/lang/String;
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    .line 739
    .end local v3    # "str":Ljava/lang/String;
    :goto_1
    return v4

    .line 736
    .restart local v3    # "str":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 739
    .end local v3    # "str":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private removeEmptyList(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "update"    # Z

    .prologue
    .line 959
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    monitor-enter v1

    .line 960
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 963
    :cond_0
    if-eqz p2, :cond_1

    .line 964
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    :cond_1
    monitor-exit v1

    .line 966
    return-void

    .line 965
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private selectBestRelationPkg(Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;)Ljava/lang/String;
    .locals 11
    .param p1, "cur"    # Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .prologue
    const/4 v7, 0x5

    const/4 v8, 0x0

    .line 575
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    if-nez v9, :cond_1

    move-object v5, v8

    .line 612
    :cond_0
    :goto_0
    return-object v5

    .line 577
    :cond_1
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 578
    .local v3, "prev":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v3, :cond_2

    .line 579
    const-string v7, "PreferredPackage"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "cannot find package record for "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v8

    .line 580
    goto :goto_0

    .line 583
    :cond_2
    const/4 v5, 0x0

    .line 584
    .local v5, "topPkg":Ljava/lang/String;
    const/4 v6, 0x0

    .line 585
    .local v6, "topScore":I
    iget-object v9, p1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->scoreList:Ljava/util/LinkedList;

    invoke-virtual {v9}, Ljava/util/LinkedList;->size()I

    move-result v2

    .line 586
    .local v2, "limit":I
    if-nez v2, :cond_3

    move-object v5, v8

    goto :goto_0

    .line 588
    :cond_3
    if-ge v2, v7, :cond_7

    .line 589
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v2, :cond_8

    .line 590
    iget-object v7, p1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->scoreList:Ljava/util/LinkedList;

    invoke-virtual {v7, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;

    iget-object v0, v7, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;->pkgName:Ljava/lang/String;

    .line 591
    .local v0, "candidate":Ljava/lang/String;
    iget-object v7, p1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v7, v0}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getIndirectScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 593
    .local v4, "score":I
    sget-boolean v7, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v7, :cond_4

    const-string v7, "PreferredPackage"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "indirect scoring : selectBestRelationPkg candidate = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {v0}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " score="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_4
    sget-boolean v7, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v7, :cond_5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "indirect scoring : selectBestRelationPkg candidate = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v0}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " score="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 598
    :cond_5
    if-ge v6, v4, :cond_6

    .line 599
    move-object v5, v0

    .line 600
    move v6, v4

    .line 589
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "candidate":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v4    # "score":I
    :cond_7
    move v2, v7

    .line 588
    goto :goto_1

    .line 606
    .restart local v1    # "i":I
    :cond_8
    if-nez v5, :cond_0

    .line 607
    sget-boolean v7, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v7, :cond_9

    const-string v7, "PreferredPackage"

    const-string v8, "We cannot find package that meet two conditions.. so return top score package"

    invoke-static {v7, v8}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    :cond_9
    sget-boolean v7, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v7, :cond_a

    const-string v7, "We cannot find package that meet two conditions.. so return top score package"

    invoke-virtual {p0, v7}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 609
    :cond_a
    iget-object v7, p1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->scoreList:Ljava/util/LinkedList;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;

    iget-object v5, v7, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;->pkgName:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private selectTopPreferredPkg(Ljava/lang/String;)I
    .locals 13
    .param p1, "resumedPkgName"    # Ljava/lang/String;

    .prologue
    .line 616
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    if-eqz v9, :cond_0

    const-wide/16 v10, 0x40

    const-string v9, "PPM_selectTop"

    const/4 v12, 0x0

    invoke-static {v10, v11, v9, v12}, Landroid/os/Trace;->asyncTraceBegin(JLjava/lang/String;I)V

    .line 617
    :cond_0
    const/4 v2, 0x0

    .line 619
    .local v2, "addedCount":I
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 620
    .local v3, "currentPkgPpr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v3, :cond_1

    const/4 v9, -0x1

    .line 665
    :goto_0
    return v9

    .line 622
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, ""

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 623
    .local v8, "topPackageNames":Ljava/lang/StringBuilder;
    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    monitor-enter v10

    .line 624
    :try_start_0
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 625
    invoke-direct {p0, v3}, Lcom/android/server/am/PreferredPackageManager;->selectBestRelationPkg(Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;)Ljava/lang/String;

    move-result-object v7

    .line 626
    .local v7, "topPackage":Ljava/lang/String;
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v9, :cond_2

    const-string v9, "PreferredPackage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v12, "selected package using relation is "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_2
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v9, :cond_3

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "selected package using relation is "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 630
    :cond_3
    if-eqz v7, :cond_5

    .line 631
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 632
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v9, :cond_4

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v7}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v11, "(S), "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 633
    :cond_4
    add-int/lit8 v2, v2, 0x1

    .line 635
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v9, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 636
    .local v6, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-eqz v6, :cond_5

    iget v9, v6, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->state:I

    const/4 v11, 0x4

    if-ne v9, v11, :cond_5

    .line 637
    iget-object v11, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    monitor-enter v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 638
    :try_start_1
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    iget-object v12, v6, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    monitor-exit v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 644
    .end local v6    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :cond_5
    :try_start_2
    iget-object v11, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    monitor-enter v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 645
    :try_start_3
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 646
    .local v0, "N":I
    if-lez v0, :cond_7

    .line 647
    const/4 v1, 0x0

    .line 648
    .local v1, "addedCnt":I
    add-int/lit8 v4, v0, -0x1

    .local v4, "i":I
    :goto_1
    if-ltz v4, :cond_7

    .line 649
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredStartedEmptyList:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 650
    .local v5, "pkg":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    invoke-direct {p0, v5}, Lcom/android/server/am/PreferredPackageManager;->isBlackList(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 651
    iget-object v9, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 652
    add-int/lit8 v1, v1, 0x1

    .line 653
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v9, :cond_6

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v12, "(E), "

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 655
    :cond_6
    const/4 v9, 0x2

    if-lt v1, v9, :cond_b

    .line 658
    .end local v1    # "addedCnt":I
    .end local v4    # "i":I
    .end local v5    # "pkg":Ljava/lang/String;
    :cond_7
    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 660
    :try_start_4
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v9, :cond_8

    const-string v9, "PreferredPackage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mTopPreferredPackages : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v9, v11}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    :cond_8
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v9, :cond_9

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "mTopPreferredPackages : "

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 662
    :cond_9
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 664
    sget-boolean v9, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    if-eqz v9, :cond_a

    const-wide/16 v10, 0x40

    const-string v9, "PPM_selectTop"

    const/4 v12, 0x0

    invoke-static {v10, v11, v9, v12}, Landroid/os/Trace;->asyncTraceEnd(JLjava/lang/String;I)V

    .line 665
    :cond_a
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 639
    .end local v0    # "N":I
    .restart local v6    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :catchall_0
    move-exception v9

    :try_start_5
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v9

    .line 662
    .end local v6    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v7    # "topPackage":Ljava/lang/String;
    :catchall_1
    move-exception v9

    monitor-exit v10
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v9

    .line 648
    .restart local v0    # "N":I
    .restart local v1    # "addedCnt":I
    .restart local v4    # "i":I
    .restart local v5    # "pkg":Ljava/lang/String;
    .restart local v7    # "topPackage":Ljava/lang/String;
    :cond_b
    add-int/lit8 v4, v4, -0x1

    goto/16 :goto_1

    .line 658
    .end local v0    # "N":I
    .end local v1    # "addedCnt":I
    .end local v4    # "i":I
    .end local v5    # "pkg":Ljava/lang/String;
    :catchall_2
    move-exception v9

    :try_start_7
    monitor-exit v11
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method private triggerScannerPackageByBGCountChecked(Ljava/lang/String;)V
    .locals 2
    .param p1, "resumedPkgName"    # Ljava/lang/String;

    .prologue
    .line 549
    new-instance v0, Lcom/android/server/am/PreferredPackageManager$2;

    invoke-direct {v0, p0, p1}, Lcom/android/server/am/PreferredPackageManager$2;-><init>(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;)V

    .line 571
    .local v0, "checkAndTrigger":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    invoke-virtual {v1, v0}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;->post(Ljava/lang/Runnable;)Z

    .line 572
    return-void
.end method

.method private updateIndirectScore(Ljava/lang/String;)V
    .locals 4
    .param p1, "curPkg"    # Ljava/lang/String;

    .prologue
    .line 887
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 888
    :cond_0
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_1

    const-string v1, "PreferredPackage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "indirect scoring : updateIndirectScore() mPrevPrev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mPrev="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 890
    :cond_1
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "indirect scoring : updateIndirectScore() mPrevPrev="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPrev="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 917
    :cond_2
    :goto_0
    return-void

    .line 896
    :cond_3
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 897
    const-string v1, "PreferredPackage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "indirect scoring : updateIndirectScore() mPrevPrevPkgName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mPrevPkgName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " current="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    :cond_4
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v1, :cond_5

    .line 901
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "indirect scoring : updateIndirectScore() mPrevPrevPkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mPrevPkgName="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 905
    :cond_5
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 906
    .local v0, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v0, :cond_6

    .line 907
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "PreferredPackage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "cannot find package record for package "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 910
    :cond_6
    iget-object v1, v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 911
    sget-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v1, :cond_2

    const-string v1, "PreferredPackage"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "A->B->A pattern. Skip indirect scoring! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPrevPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 916
    :cond_7
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPrevPkgName:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->doIncreaseIndirectScore(Ljava/lang/String;Ljava/lang/String;I)V

    goto/16 :goto_0
.end method


# virtual methods
.method deleteXml()V
    .locals 5

    .prologue
    .line 1207
    new-instance v1, Ljava/io/File;

    const-string v3, "/data/system/preferred.xml"

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1208
    .local v1, "file":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    const-string v3, "/data/log/ppm_log.log"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1209
    .local v2, "logFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1210
    const-string v3, "PreferredPackage"

    const-string v4, "Preferred xml file does not exist"

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1221
    :goto_0
    return-void

    .line 1214
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 1215
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1220
    const-string v3, "PreferredPackage"

    const-string v4, "Delete preferred xml file /data/system/preferred.xml,/data/log/ppm_log.log"

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1216
    :catch_0
    move-exception v0

    .line 1217
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;)V
    .locals 6
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;

    .prologue
    .line 1224
    const-string v4, "ACTIVITY MANAGER SERVICES (dumpsys activity preferred)"

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1227
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Dump Records : \n\n--- Scores  \n"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1228
    .local v3, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .line 1229
    .local v1, "k":I
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 1230
    .local v2, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->packageName:Ljava/lang/String;

    invoke-static {v5}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1231
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DirectScore     : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getScoreListString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1232
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "IndirectScore   : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getIndirectScoreListString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1233
    add-int/lit8 v1, v1, 0x1

    .line 1234
    goto :goto_0

    .line 1236
    .end local v2    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1237
    invoke-virtual {p2}, Ljava/io/PrintWriter;->println()V

    .line 1238
    invoke-virtual {p0}, Lcom/android/server/am/PreferredPackageManager;->writePackagesInfoToXml()V

    .line 1239
    return-void
.end method

.method protected init(Lcom/android/server/am/ActivityManagerService;II)V
    .locals 4
    .param p1, "service"    # Lcom/android/server/am/ActivityManagerService;
    .param p2, "flags"    # I
    .param p3, "userId"    # I

    .prologue
    .line 187
    iput-object p1, p0, Lcom/android/server/am/PreferredPackageManager;->mService:Lcom/android/server/am/ActivityManagerService;

    .line 188
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 189
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x6

    iput v1, v0, Landroid/os/Message;->what:I

    .line 190
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    const-wide/32 v2, 0x1b7740

    invoke-virtual {v1, v0, v2, v3}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 192
    iput p2, p0, Lcom/android/server/am/PreferredPackageManager;->mFlags:I

    .line 193
    iput p3, p0, Lcom/android/server/am/PreferredPackageManager;->mUserId:I

    .line 195
    invoke-virtual {p0}, Lcom/android/server/am/PreferredPackageManager;->updatePreferredPackagePolicy()V

    .line 197
    invoke-virtual {p0}, Lcom/android/server/am/PreferredPackageManager;->readPackagesFromXml()V

    .line 198
    return-void
.end method

.method isTopPreferredPackage(Ljava/lang/String;)Z
    .locals 2
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 720
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 721
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    monitor-enter v1

    .line 722
    :try_start_0
    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mTopPreferredPackages:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1

    .line 725
    :goto_0
    return v0

    .line 723
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 725
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyActivityStatus(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 14
    .param p1, "kind"    # I
    .param p2, "packageName"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;
    .param p4, "reason"    # Ljava/lang/String;

    .prologue
    .line 743
    move-object/from16 v9, p3

    check-cast v9, Lcom/android/server/am/ActivityRecord;

    .line 744
    .local v9, "r":Lcom/android/server/am/ActivityRecord;
    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/android/server/am/PreferredPackageManager;->isInstrumentList(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_1

    :cond_0
    if-eqz v9, :cond_4

    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    invoke-direct {p0, v10}, Lcom/android/server/am/PreferredPackageManager;->isInstrumentList(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 746
    :cond_1
    move-object/from16 v6, p2

    .line 747
    .local v6, "n":Ljava/lang/String;
    if-eqz v9, :cond_2

    iget-object v6, v9, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    .line 748
    :cond_2
    sget-boolean v10, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v10, :cond_3

    const-string v10, "PreferredPackage"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " is in instrumentation list.. so skip it"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    .end local v6    # "n":Ljava/lang/String;
    :cond_3
    :goto_0
    return-void

    .line 752
    :cond_4
    move-object/from16 v7, p2

    .line 753
    .local v7, "pkgName":Ljava/lang/String;
    if-eqz v9, :cond_5

    .line 754
    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    if-eqz v10, :cond_3

    invoke-virtual {v9}, Lcom/android/server/am/ActivityRecord;->isHomeActivity()Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {v9}, Lcom/android/server/am/ActivityRecord;->isRecentsActivity()Z

    move-result v10

    if-nez v10, :cond_3

    .line 755
    iget-object v7, v9, Lcom/android/server/am/ActivityRecord;->packageName:Ljava/lang/String;

    .line 758
    :cond_5
    const/4 v10, 0x3

    if-ne p1, v10, :cond_8

    .line 759
    sget-boolean v10, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v10, :cond_6

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "AppStatus : RESUME STARTED  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v7}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "(in "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 762
    :cond_6
    iget-object v11, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v11

    .line 763
    :try_start_0
    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v10, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 764
    .local v8, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v8, :cond_7

    if-eqz v9, :cond_7

    invoke-virtual {v9}, Lcom/android/server/am/ActivityRecord;->isHomeActivity()Z

    move-result v10

    if-nez v10, :cond_7

    .line 765
    new-instance v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .end local v8    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    invoke-direct {v8, p0, v7}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;-><init>(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;)V

    .line 766
    .restart local v8    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    iget v10, v10, Lcom/android/server/am/ProcessRecord;->pid:I

    iput v10, v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->pid:I

    .line 767
    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    iget v10, v10, Lcom/android/server/am/ProcessRecord;->uid:I

    iput v10, v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->uid:I

    .line 768
    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->app:Lcom/android/server/am/ProcessRecord;

    iget-wide v12, v10, Lcom/android/server/am/ProcessRecord;->lastPss:J

    iput-wide v12, v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->lastPss:J

    .line 769
    iget-object v10, v9, Lcom/android/server/am/ActivityRecord;->processName:Ljava/lang/String;

    iput-object v10, v8, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->processName:Ljava/lang/String;

    .line 770
    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v10, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    sget-boolean v10, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v10, :cond_7

    const-string v10, "PreferredPackage"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "New app started : "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    :cond_7
    monitor-exit v11

    goto/16 :goto_0

    .end local v8    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10

    .line 777
    :cond_8
    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    invoke-static {v10, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    .line 778
    .local v3, "msg":Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 779
    .local v2, "b":Landroid/os/Bundle;
    const-string v10, "name"

    invoke-virtual {v2, v10, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 781
    packed-switch p1, :pswitch_data_0

    .line 793
    :goto_1
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 794
    iget-object v10, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    invoke-virtual {v10, v3}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 783
    :pswitch_0
    const-string/jumbo v10, "reason"

    move-object/from16 v0, p4

    invoke-virtual {v2, v10, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 786
    :pswitch_1
    const-wide/16 v4, 0x0

    .line 787
    .local v4, "lastPauseTime":J
    if-eqz v9, :cond_9

    .line 788
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 790
    :cond_9
    const-string v10, "lastPauseTime"

    invoke-virtual {v2, v10, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    .line 781
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method notifyMemoryAndBGProcStatus(ZI)V
    .locals 3
    .param p1, "newState"    # Z
    .param p2, "bgProcessCount"    # I

    .prologue
    .line 701
    iget-boolean v0, p0, Lcom/android/server/am/PreferredPackageManager;->mLowMemState:Z

    if-eq v0, p1, :cond_2

    .line 702
    sget-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v1, "PreferredPackage"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "set memory state to "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p1, :cond_3

    const-string v0, "low mem state"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 703
    :cond_0
    sget-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "set memory state to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_4

    const-string v0, "low mem state"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 704
    :cond_1
    iput-boolean p1, p0, Lcom/android/server/am/PreferredPackageManager;->mLowMemState:Z

    .line 706
    :cond_2
    iput p2, p0, Lcom/android/server/am/PreferredPackageManager;->mBgCount:I

    .line 707
    return-void

    .line 702
    :cond_3
    const-string v0, "normal state"

    goto :goto_0

    .line 703
    :cond_4
    const-string v0, "normal state"

    goto :goto_1
.end method

.method processDiedPreferred(Ljava/lang/String;Z)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "killedByAMS"    # Z

    .prologue
    const/4 v5, 0x4

    .line 969
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, ""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 970
    .local v2, "sb":Ljava/lang/StringBuilder;
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AppStatus : DIED "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Lcom/android/server/am/PreferredPackageManager;->getTrimmedStr(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " byAMS : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 973
    :cond_0
    const/4 v1, 0x0

    .line 974
    .local v1, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    iget-object v4, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v4

    .line 975
    :try_start_0
    iget-object v3, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    move-object v1, v0

    .line 976
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 978
    if-nez v1, :cond_2

    .line 980
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 981
    const-string v3, " -> cannot find in PP info"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 982
    const-string v3, "PreferredPackage"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 998
    :cond_1
    :goto_0
    return-void

    .line 976
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 988
    :cond_2
    iget v3, v1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->state:I

    if-eq v3, v5, :cond_1

    .line 990
    invoke-virtual {v1, v5}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->setState(I)V

    .line 991
    const/4 v3, -0x1

    iput v3, v1, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->pid:I

    .line 992
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 993
    const-string v3, " -> State updated as DIED"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 994
    const-string v3, "PreferredPackage"

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 995
    sget-boolean v3, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/android/server/am/PreferredPackageManager;->recordLog(Ljava/lang/String;)V

    .line 997
    :cond_3
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/android/server/am/PreferredPackageManager;->removeEmptyList(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method readPackagesFromXml()V
    .locals 24

    .prologue
    .line 1093
    new-instance v13, Ljava/io/File;

    const-string v4, "/data/system/preferred.xml"

    invoke-direct {v13, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1094
    .local v13, "file":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1095
    new-instance v13, Ljava/io/File;

    .end local v13    # "file":Ljava/io/File;
    const-string v4, "/system/etc/preferred.xml"

    invoke-direct {v13, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1096
    .restart local v13    # "file":Ljava/io/File;
    invoke-virtual {v13}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1098
    const-string v4, "PreferredPackage"

    const-string v23, "Preferred Package Info file does not exits!!"

    move-object/from16 v0, v23

    invoke-static {v4, v0}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1204
    :goto_0
    return-void

    .line 1103
    :cond_0
    const/4 v14, 0x0

    .line 1105
    .local v14, "fileReader":Ljava/io/FileReader;
    :try_start_0
    new-instance v14, Ljava/io/FileReader;

    .end local v14    # "fileReader":Ljava/io/FileReader;
    invoke-direct {v14, v13}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1112
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    :try_start_1
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v18

    .line 1113
    .local v18, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const/4 v3, 0x0

    .line 1114
    .local v3, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    const/16 v21, 0x0

    .line 1115
    .local v21, "read_indirect":Z
    const/4 v10, 0x0

    .line 1116
    .local v10, "bridge":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-interface {v0, v14}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 1118
    :goto_1
    const/4 v5, 0x0

    .line 1119
    .local v5, "pkg":Ljava/lang/String;
    const-wide/16 v6, 0x0

    .line 1120
    .local v6, "lastPauseTime":J
    const/4 v8, 0x0

    .line 1121
    .local v8, "score":Ljava/lang/String;
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    if-nez v4, :cond_1

    .line 1124
    :cond_1
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    const/16 v23, 0x1

    move/from16 v0, v23

    if-ne v4, v0, :cond_2

    .line 1188
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 1189
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "lastPauseTime":J
    .end local v8    # "score":Ljava/lang/String;
    .end local v10    # "bridge":Ljava/lang/String;
    .end local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v21    # "read_indirect":Z
    :catch_0
    move-exception v12

    .line 1191
    .local v12, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_2
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1194
    :goto_2
    invoke-virtual {v12}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    goto :goto_0

    .line 1106
    .end local v12    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v14    # "fileReader":Ljava/io/FileReader;
    :catch_1
    move-exception v12

    .line 1107
    .local v12, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 1128
    .end local v12    # "e":Ljava/io/FileNotFoundException;
    .restart local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v5    # "pkg":Ljava/lang/String;
    .restart local v6    # "lastPauseTime":J
    .restart local v8    # "score":Ljava/lang/String;
    .restart local v10    # "bridge":Ljava/lang/String;
    .restart local v14    # "fileReader":Ljava/io/FileReader;
    .restart local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v21    # "read_indirect":Z
    :cond_2
    :try_start_3
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    const/16 v23, 0x2

    move/from16 v0, v23

    if-ne v4, v0, :cond_3

    .line 1129
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v22

    .line 1130
    .local v22, "tagName":Ljava/lang/String;
    const-string/jumbo v4, "preferred"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1173
    .end local v22    # "tagName":Ljava/lang/String;
    :cond_3
    :goto_3
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    const/16 v23, 0x3

    move/from16 v0, v23

    if-ne v4, v0, :cond_4

    .line 1174
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v22

    .line 1176
    .restart local v22    # "tagName":Ljava/lang/String;
    const-string v4, "item"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 1177
    if-eqz v5, :cond_4

    .line 1186
    .end local v22    # "tagName":Ljava/lang/String;
    :cond_4
    :goto_4
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 1196
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "lastPauseTime":J
    .end local v8    # "score":Ljava/lang/String;
    .end local v10    # "bridge":Ljava/lang/String;
    .end local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v21    # "read_indirect":Z
    :catch_2
    move-exception v12

    .line 1198
    .local v12, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v14}, Ljava/io/FileReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 1201
    :goto_5
    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 1133
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v5    # "pkg":Ljava/lang/String;
    .restart local v6    # "lastPauseTime":J
    .restart local v8    # "score":Ljava/lang/String;
    .restart local v10    # "bridge":Ljava/lang/String;
    .restart local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v21    # "read_indirect":Z
    .restart local v22    # "tagName":Ljava/lang/String;
    :cond_5
    :try_start_5
    const-string v4, "item"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 1134
    invoke-interface/range {v18 .. v18}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v11

    .line 1135
    .local v11, "cnt":I
    const/16 v16, 0x0

    .local v16, "i":I
    :goto_6
    move/from16 v0, v16

    if-ge v0, v11, :cond_9

    .line 1136
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v2

    .line 1137
    .local v2, "attributeName":Ljava/lang/String;
    move-object/from16 v0, v18

    move/from16 v1, v16

    invoke-interface {v0, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v9

    .line 1138
    .local v9, "attributeValue":Ljava/lang/String;
    const-string v4, "name"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1139
    move-object v5, v9

    .line 1135
    :cond_6
    :goto_7
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    .line 1140
    :cond_7
    const-string v4, "lastPauseTime"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1141
    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto :goto_7

    .line 1142
    :cond_8
    const-string/jumbo v4, "score"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1143
    move-object v8, v9

    goto :goto_7

    .line 1147
    .end local v2    # "attributeName":Ljava/lang/String;
    .end local v9    # "attributeValue":Ljava/lang/String;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    move-object/from16 v23, v0

    monitor-enter v23
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 1148
    :try_start_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1149
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .local v20, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-nez v20, :cond_a

    .line 1150
    :try_start_7
    new-instance v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    move-object/from16 v4, p0

    invoke-direct/range {v3 .. v8}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;-><init>(Lcom/android/server/am/PreferredPackageManager;Ljava/lang/String;JLjava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1151
    .end local v20    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1155
    :goto_8
    monitor-exit v23
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1156
    const/4 v5, 0x0

    .line 1158
    goto/16 :goto_3

    .line 1153
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v20    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :cond_a
    :try_start_9
    move-object/from16 v0, v20

    invoke-virtual {v0, v6, v7}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->updateLastPauseTime(J)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-object/from16 v3, v20

    .end local v20    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    goto :goto_8

    .line 1155
    :catchall_0
    move-exception v4

    :goto_9
    :try_start_a
    monitor-exit v23
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    throw v4

    .line 1158
    .end local v11    # "cnt":I
    .end local v16    # "i":I
    :cond_b
    const-string v4, "indirect"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1159
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v10

    .line 1160
    iget-object v4, v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->indirectScoreMap:Ljava/util/HashMap;

    new-instance v23, Ljava/util/HashMap;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v4, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1162
    const/16 v21, 0x1

    goto/16 :goto_3

    .line 1163
    :cond_c
    const-string/jumbo v4, "score_info"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1164
    if-eqz v21, :cond_3

    .line 1165
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v19

    .line 1166
    .local v19, "pkgName":Ljava/lang/String;
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    .line 1167
    .local v17, "indirect_score":I
    iget-object v4, v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->indirectScoreMap:Ljava/util/HashMap;

    invoke-virtual {v4, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/HashMap;

    .line 1168
    .local v15, "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    new-instance v4, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-object/from16 v0, v19

    move/from16 v1, v17

    invoke-direct {v4, v3, v0, v1}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;-><init>(Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;Ljava/lang/String;I)V

    move-object/from16 v0, v19

    invoke-virtual {v15, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_3

    .line 1179
    .end local v15    # "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    .end local v17    # "indirect_score":I
    .end local v19    # "pkgName":Ljava/lang/String;
    :cond_d
    const-string v4, "indirect"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 1180
    const/16 v21, 0x0

    .line 1181
    const/4 v10, 0x0

    goto/16 :goto_4

    .line 1182
    :cond_e
    const-string/jumbo v4, "score_info"

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2

    move-result v4

    if-eqz v4, :cond_4

    goto/16 :goto_4

    .line 1192
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v5    # "pkg":Ljava/lang/String;
    .end local v6    # "lastPauseTime":J
    .end local v8    # "score":Ljava/lang/String;
    .end local v10    # "bridge":Ljava/lang/String;
    .end local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .end local v21    # "read_indirect":Z
    .end local v22    # "tagName":Ljava/lang/String;
    .local v12, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_3
    move-exception v4

    goto/16 :goto_2

    .line 1199
    .local v12, "e":Ljava/io/IOException;
    :catch_4
    move-exception v4

    goto/16 :goto_5

    .line 1155
    .end local v12    # "e":Ljava/io/IOException;
    .restart local v5    # "pkg":Ljava/lang/String;
    .restart local v6    # "lastPauseTime":J
    .restart local v8    # "score":Ljava/lang/String;
    .restart local v10    # "bridge":Ljava/lang/String;
    .restart local v11    # "cnt":I
    .restart local v16    # "i":I
    .restart local v18    # "parser":Lorg/xmlpull/v1/XmlPullParser;
    .restart local v20    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v21    # "read_indirect":Z
    .restart local v22    # "tagName":Ljava/lang/String;
    :catchall_1
    move-exception v4

    move-object/from16 v3, v20

    .end local v20    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    goto/16 :goto_9
.end method

.method recordLog(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 491
    sget-boolean v0, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/am/PreferredPackageManager;->mLogger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    invoke-virtual {v0, v1, p1}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    .line 492
    :cond_0
    return-void
.end method

.method registerReceiver()V
    .locals 3

    .prologue
    .line 496
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 497
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 498
    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 499
    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 500
    invoke-static {}, Landroid/app/ActivityThread;->systemMain()Landroid/app/ActivityThread;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v0

    .line 501
    .local v0, "context":Landroid/content/Context;
    new-instance v2, Lcom/android/server/am/PreferredPackageManager$1;

    invoke-direct {v2, p0}, Lcom/android/server/am/PreferredPackageManager$1;-><init>(Lcom/android/server/am/PreferredPackageManager;)V

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 515
    return-void
.end method

.method setPackagePid(Ljava/lang/String;I)V
    .locals 4
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "pid"    # I

    .prologue
    .line 710
    invoke-direct {p0, p1}, Lcom/android/server/am/PreferredPackageManager;->isBlackList(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 717
    :goto_0
    return-void

    .line 711
    :cond_0
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    .line 712
    .local v1, "msg":Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 713
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "name"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 714
    const-string/jumbo v2, "pid"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 715
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 716
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mHandler:Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;

    invoke-virtual {v2, v1}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method startPreferredPackages()V
    .locals 9

    .prologue
    .line 518
    iget-object v6, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    monitor-enter v6

    .line 519
    :try_start_0
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 520
    .local v2, "pkg":Ljava/lang/String;
    const-string v5, "PreferredPackage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "start package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 522
    :try_start_1
    invoke-static {}, Landroid/app/AppGlobals;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v5

    iget v7, p0, Lcom/android/server/am/PreferredPackageManager;->mFlags:I

    iget v8, p0, Lcom/android/server/am/PreferredPackageManager;->mUserId:I

    invoke-interface {v5, v2, v7, v8}, Landroid/content/pm/IPackageManager;->getPackageInfo(Ljava/lang/String;II)Landroid/content/pm/PackageInfo;

    move-result-object v5

    iget-object v0, v5, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 528
    .local v0, "ai":Landroid/content/pm/ApplicationInfo;
    iget-object v7, p0, Lcom/android/server/am/PreferredPackageManager;->mService:Lcom/android/server/am/ActivityManagerService;

    monitor-enter v7
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 529
    :try_start_2
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5, v0}, Lcom/android/server/am/ActivityManagerService;->realStartPreferredPackages(Landroid/content/pm/ApplicationInfo;)V

    .line 530
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 532
    :try_start_3
    iget-object v7, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v7
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 533
    :try_start_4
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 534
    .local v3, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    if-eqz v3, :cond_0

    .line 535
    const/4 v5, 0x3

    invoke-virtual {v3, v5}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->setState(I)V

    .line 536
    :cond_0
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 542
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .end local v3    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    :goto_1
    const/4 v5, 0x1

    :try_start_5
    invoke-direct {p0, v2, v5}, Lcom/android/server/am/PreferredPackageManager;->removeEmptyList(Ljava/lang/String;Z)V

    goto :goto_0

    .line 545
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "pkg":Ljava/lang/String;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v5

    .line 530
    .restart local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "pkg":Ljava/lang/String;
    :catchall_1
    move-exception v5

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v5
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 537
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :catch_0
    move-exception v5

    goto :goto_1

    .line 536
    .restart local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :catchall_2
    move-exception v5

    :try_start_8
    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    throw v5
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 538
    .end local v0    # "ai":Landroid/content/pm/ApplicationInfo;
    :catch_1
    move-exception v4

    .line 539
    .local v4, "re":Ljava/lang/RuntimeException;
    :try_start_a
    const-string v5, "PreferredPackage"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "package "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " some problems. "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->secE(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 544
    .end local v2    # "pkg":Ljava/lang/String;
    .end local v4    # "re":Ljava/lang/RuntimeException;
    :cond_1
    iget-object v5, p0, Lcom/android/server/am/PreferredPackageManager;->mToStartList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 545
    monitor-exit v6
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 546
    return-void
.end method

.method protected updatePreferredPackagePolicy()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 201
    const-string/jumbo v1, "sys.ppm.debug"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    .line 202
    const-string/jumbo v1, "sys.ppm.debug_logfile_enable"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_LOGFILE_ENABLE:Z

    .line 203
    const-string/jumbo v1, "sys.ppm.debug_trace"

    invoke-static {v1, v3}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/android/server/am/PreferredPackageManager;->DEBUG_TRACE:Z

    .line 204
    const-string/jumbo v1, "sys.ppm.reset"

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 206
    .local v0, "reset":Z
    if-eqz v0, :cond_0

    .line 207
    const-string v1, "PreferredPackage"

    const-string v2, "Reset every PPM records!"

    invoke-static {v1, v2}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-object v2, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    monitor-enter v2

    .line 209
    :try_start_0
    iget-object v1, p0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 210
    monitor-exit v2

    .line 212
    :cond_0
    return-void

    .line 210
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method writePackagesInfoToXml()V
    .locals 30

    .prologue
    .line 1001
    sget-boolean v25, Lcom/android/server/am/PreferredPackageManager;->DEBUG:Z

    if-eqz v25, :cond_0

    const-string v25, "PreferredPackage"

    const-string/jumbo v26, "write preferred data!!"

    invoke-static/range {v25 .. v26}, Landroid/util/Slog;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 1002
    :cond_0
    new-instance v6, Ljava/io/File;

    const-string v25, "/data/system/preferred.xml"

    move-object/from16 v0, v25

    invoke-direct {v6, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1003
    .local v6, "file":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v25

    if-nez v25, :cond_1

    .line 1005
    :try_start_0
    invoke-virtual {v6}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1011
    :cond_1
    :goto_0
    new-instance v23, Ljava/io/File;

    new-instance v25, Ljava/lang/StringBuilder;

    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v26

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    const-string v26, ".tmp"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, v23

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1012
    .local v23, "tempFile":Ljava/io/File;
    new-instance v13, Lcom/android/internal/util/JournaledFile;

    move-object/from16 v0, v23

    invoke-direct {v13, v6, v0}, Lcom/android/internal/util/JournaledFile;-><init>(Ljava/io/File;Ljava/io/File;)V

    .line 1014
    .local v13, "jnfile":Lcom/android/internal/util/JournaledFile;
    const/4 v7, 0x0

    .line 1015
    .local v7, "fstr":Ljava/io/FileOutputStream;
    const/16 v21, 0x0

    .line 1018
    .local v21, "str":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-virtual {v13}, Lcom/android/internal/util/JournaledFile;->chooseForWrite()Ljava/io/File;

    move-result-object v25

    move-object/from16 v0, v25

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_b
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1019
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .local v8, "fstr":Ljava/io/FileOutputStream;
    :try_start_2
    new-instance v22, Ljava/io/BufferedOutputStream;

    move-object/from16 v0, v22

    invoke-direct {v0, v8}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1021
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .local v22, "str":Ljava/io/BufferedOutputStream;
    :try_start_3
    new-instance v24, Lcom/android/internal/util/FastXmlSerializer;

    invoke-direct/range {v24 .. v24}, Lcom/android/internal/util/FastXmlSerializer;-><init>()V

    .line 1022
    .local v24, "xml":Lorg/xmlpull/v1/XmlSerializer;
    const-string/jumbo v25, "utf-8"

    move-object/from16 v0, v24

    move-object/from16 v1, v22

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->setOutput(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1023
    const/16 v25, 0x0

    const/16 v26, 0x1

    invoke-static/range {v26 .. v26}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v26

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->startDocument(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1024
    const-string v25, "http://xmlpull.org/v1/doc/features.html#indent-output"

    const/16 v26, 0x1

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->setFeature(Ljava/lang/String;Z)V

    .line 1027
    const/16 v25, 0x0

    const-string/jumbo v26, "preferred"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1028
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    .line 1030
    .local v14, "key":Ljava/util/Set;
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "iterator":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_a

    .line 1031
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 1032
    .local v15, "keyName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/server/am/PreferredPackageManager;->mPreferredPackages:Ljava/util/HashMap;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;

    .line 1033
    .local v18, "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    invoke-virtual/range {v18 .. v18}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getPackageName()Ljava/lang/String;

    move-result-object v16

    .line 1034
    .local v16, "pkg":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    invoke-virtual/range {v18 .. v18}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getLastPauseTime()J

    move-result-wide v28

    sub-long v26, v26, v28

    const-wide/32 v28, 0x5265c00

    cmp-long v25, v26, v28

    if-lez v25, :cond_2

    .line 1035
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    move-wide/from16 v0, v26

    move-object/from16 v2, v18

    iput-wide v0, v2, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->lastPauseTime:J

    .line 1038
    :cond_2
    const/16 v25, 0x0

    const-string v26, "item"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1039
    const/16 v25, 0x0

    const-string v26, "name"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v16

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1040
    const/16 v25, 0x0

    const-string v26, "lastPauseTime"

    invoke-virtual/range {v18 .. v18}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getLastPauseTime()J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v27

    invoke-interface/range {v24 .. v27}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1041
    const/16 v25, 0x0

    const-string/jumbo v26, "score"

    invoke-virtual/range {v18 .. v18}, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->getScoreListString()Ljava/lang/String;

    move-result-object v27

    invoke-interface/range {v24 .. v27}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1044
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;->indirectScoreMap:Ljava/util/HashMap;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v20

    .line 1046
    .local v20, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    invoke-interface/range {v20 .. v20}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_7

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    .line 1047
    .local v5, "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;"
    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 1048
    .local v17, "pkgName":Ljava/lang/String;
    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/HashMap;

    .line 1049
    .local v9, "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    const/16 v25, 0x0

    const-string v26, "indirect"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1050
    const/16 v25, 0x0

    const-string v26, "name"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v26

    move-object/from16 v3, v17

    invoke-interface {v0, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1053
    invoke-virtual {v9}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v25

    invoke-interface/range {v25 .. v25}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v25

    if-eqz v25, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;

    .line 1054
    .local v19, "psi":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;
    const/16 v25, 0x0

    const-string/jumbo v26, "score_info"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1055
    const/16 v25, 0x0

    const-string/jumbo v26, "pkg"

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;->pkgName:Ljava/lang/String;

    move-object/from16 v27, v0

    invoke-interface/range {v24 .. v27}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1056
    const/16 v25, 0x0

    const-string/jumbo v26, "score"

    move-object/from16 v0, v19

    iget v0, v0, Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;->score:I

    move/from16 v27, v0

    invoke-static/range {v27 .. v27}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v27

    invoke-interface/range {v24 .. v27}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1057
    const/16 v25, 0x0

    const-string/jumbo v26, "score_info"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1071
    .end local v5    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;"
    .end local v9    # "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "iterator":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/util/Set;
    .end local v15    # "keyName":Ljava/lang/String;
    .end local v16    # "pkg":Ljava/lang/String;
    .end local v17    # "pkgName":Ljava/lang/String;
    .end local v18    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v19    # "psi":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;
    .end local v20    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    .end local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :catch_0
    move-exception v4

    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 1072
    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .local v4, "e":Ljava/io/IOException;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    :goto_4
    :try_start_4
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1076
    if-eqz v21, :cond_3

    .line 1078
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    .line 1083
    :cond_3
    :goto_5
    if-eqz v7, :cond_4

    .line 1085
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_7

    .line 1090
    .end local v4    # "e":Ljava/io/IOException;
    :cond_4
    :goto_6
    return-void

    .line 1006
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v13    # "jnfile":Lcom/android/internal/util/JournaledFile;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .end local v23    # "tempFile":Ljava/io/File;
    :catch_1
    move-exception v4

    .line 1007
    .restart local v4    # "e":Ljava/io/IOException;
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    .line 1059
    .end local v4    # "e":Ljava/io/IOException;
    .restart local v5    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;"
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v9    # "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    .restart local v11    # "i$":Ljava/util/Iterator;
    .restart local v12    # "iterator":Ljava/util/Iterator;
    .restart local v13    # "jnfile":Lcom/android/internal/util/JournaledFile;
    .restart local v14    # "key":Ljava/util/Set;
    .restart local v15    # "keyName":Ljava/lang/String;
    .restart local v16    # "pkg":Ljava/lang/String;
    .restart local v17    # "pkgName":Ljava/lang/String;
    .restart local v18    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v20    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v23    # "tempFile":Ljava/io/File;
    .restart local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :cond_5
    const/16 v25, 0x0

    :try_start_7
    const-string v26, "indirect"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    .line 1073
    .end local v5    # "e":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;"
    .end local v9    # "hm":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;"
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v12    # "iterator":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/util/Set;
    .end local v15    # "keyName":Ljava/lang/String;
    .end local v16    # "pkg":Ljava/lang/String;
    .end local v17    # "pkgName":Ljava/lang/String;
    .end local v18    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v20    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    .end local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :catch_2
    move-exception v4

    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 1074
    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .local v4, "e":Ljava/lang/Exception;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    :goto_7
    :try_start_8
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 1076
    if-eqz v21, :cond_6

    .line 1078
    :try_start_9
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    .line 1083
    :cond_6
    :goto_8
    if-eqz v7, :cond_4

    .line 1085
    :try_start_a
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_3

    goto :goto_6

    .line 1086
    :catch_3
    move-exception v25

    goto :goto_6

    .line 1061
    .end local v4    # "e":Ljava/lang/Exception;
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v12    # "iterator":Ljava/util/Iterator;
    .restart local v14    # "key":Ljava/util/Set;
    .restart local v15    # "keyName":Ljava/lang/String;
    .restart local v16    # "pkg":Ljava/lang/String;
    .restart local v18    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .restart local v20    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :cond_7
    const/16 v25, 0x0

    :try_start_b
    const-string v26, "item"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_1

    .line 1076
    .end local v12    # "iterator":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/util/Set;
    .end local v15    # "keyName":Ljava/lang/String;
    .end local v16    # "pkg":Ljava/lang/String;
    .end local v18    # "ppr":Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord;
    .end local v20    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/HashMap<Ljava/lang/String;Lcom/android/server/am/PreferredPackageManager$PreferredPackageRecord$PreferredScoreInfo;>;>;>;"
    .end local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :catchall_0
    move-exception v25

    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    :goto_9
    if-eqz v21, :cond_8

    .line 1078
    :try_start_c
    invoke-virtual/range {v21 .. v21}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_9

    .line 1083
    :cond_8
    :goto_a
    if-eqz v7, :cond_9

    .line 1085
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_a

    .line 1087
    :cond_9
    :goto_b
    throw v25

    .line 1063
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v12    # "iterator":Ljava/util/Iterator;
    .restart local v14    # "key":Ljava/util/Set;
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :cond_a
    const/16 v25, 0x0

    :try_start_e
    const-string/jumbo v26, "preferred"

    invoke-interface/range {v24 .. v26}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    .line 1064
    invoke-interface/range {v24 .. v24}, Lorg/xmlpull/v1/XmlSerializer;->endDocument()V

    .line 1066
    invoke-virtual/range {v22 .. v22}, Ljava/io/BufferedOutputStream;->flush()V

    .line 1067
    invoke-static {v8}, Landroid/os/FileUtils;->sync(Ljava/io/FileOutputStream;)Z

    .line 1068
    invoke-virtual/range {v22 .. v22}, Ljava/io/BufferedOutputStream;->close()V

    .line 1069
    invoke-virtual {v13}, Lcom/android/internal/util/JournaledFile;->commit()V

    .line 1070
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1076
    if-eqz v22, :cond_b

    .line 1078
    :try_start_f
    invoke-virtual/range {v22 .. v22}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_5

    .line 1083
    :cond_b
    :goto_c
    if-eqz v8, :cond_c

    .line 1085
    :try_start_10
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_4

    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 1087
    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 1086
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    :catch_4
    move-exception v25

    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .line 1087
    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto :goto_6

    .line 1079
    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    :catch_5
    move-exception v25

    goto :goto_c

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .end local v12    # "iterator":Ljava/util/Iterator;
    .end local v14    # "key":Ljava/util/Set;
    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .end local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    .local v4, "e":Ljava/io/IOException;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    :catch_6
    move-exception v25

    goto :goto_5

    .line 1086
    :catch_7
    move-exception v25

    goto :goto_6

    .line 1079
    .local v4, "e":Ljava/lang/Exception;
    :catch_8
    move-exception v25

    goto :goto_8

    .end local v4    # "e":Ljava/lang/Exception;
    :catch_9
    move-exception v26

    goto :goto_a

    .line 1086
    :catch_a
    move-exception v26

    goto :goto_b

    .line 1076
    :catchall_1
    move-exception v25

    goto :goto_9

    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v25

    move-object v7, v8

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto :goto_9

    .line 1073
    :catch_b
    move-exception v4

    goto :goto_7

    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    :catch_c
    move-exception v4

    move-object v7, v8

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto :goto_7

    .line 1071
    :catch_d
    move-exception v4

    goto/16 :goto_4

    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    :catch_e
    move-exception v4

    move-object v7, v8

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto/16 :goto_4

    .end local v7    # "fstr":Ljava/io/FileOutputStream;
    .end local v21    # "str":Ljava/io/BufferedOutputStream;
    .restart local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v12    # "iterator":Ljava/util/Iterator;
    .restart local v14    # "key":Ljava/util/Set;
    .restart local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v24    # "xml":Lorg/xmlpull/v1/XmlSerializer;
    :cond_c
    move-object/from16 v21, v22

    .end local v22    # "str":Ljava/io/BufferedOutputStream;
    .restart local v21    # "str":Ljava/io/BufferedOutputStream;
    move-object v7, v8

    .end local v8    # "fstr":Ljava/io/FileOutputStream;
    .restart local v7    # "fstr":Ljava/io/FileOutputStream;
    goto/16 :goto_6
.end method
