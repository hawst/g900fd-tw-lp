.class Lcom/android/server/notification/ConditionProviders$DowntimeCallback;
.super Ljava/lang/Object;
.source "ConditionProviders.java"

# interfaces
.implements Lcom/android/server/notification/DowntimeConditionProvider$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/ConditionProviders;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DowntimeCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/ConditionProviders;


# direct methods
.method private constructor <init>(Lcom/android/server/notification/ConditionProviders;)V
    .locals 0

    .prologue
    .line 544
    iput-object p1, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/notification/ConditionProviders;Lcom/android/server/notification/ConditionProviders$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/server/notification/ConditionProviders;
    .param p2, "x1"    # Lcom/android/server/notification/ConditionProviders$1;

    .prologue
    .line 544
    invoke-direct {p0, p1}, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;-><init>(Lcom/android/server/notification/ConditionProviders;)V

    return-void
.end method


# virtual methods
.method public onDowntimeChanged(Z)V
    .locals 8
    .param p1, "inDowntime"    # Z

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 547
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/notification/ZenModeHelper;->getZenMode()I

    move-result v3

    .line 548
    .local v3, "mode":I
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/notification/ZenModeHelper;->getConfig()Landroid/service/notification/ZenModeConfig;

    move-result-object v2

    .line 550
    .local v2, "config":Landroid/service/notification/ZenModeConfig;
    if-eqz p1, :cond_0

    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    .line 551
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mDowntime:Lcom/android/server/notification/DowntimeConditionProvider;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$500(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/DowntimeConditionProvider;

    move-result-object v4

    invoke-virtual {v2}, Landroid/service/notification/ZenModeConfig;->toDowntimeInfo()Landroid/service/notification/ZenModeConfig$DowntimeInfo;

    move-result-object v5

    invoke-virtual {v4, v5, v6}, Lcom/android/server/notification/DowntimeConditionProvider;->createCondition(Landroid/service/notification/ZenModeConfig$DowntimeInfo;I)Landroid/service/notification/Condition;

    move-result-object v1

    .line 554
    .local v1, "condition":Landroid/service/notification/Condition;
    # getter for: Lcom/android/server/notification/ConditionProviders;->mIsTouchwizDnd:Z
    invoke-static {}, Lcom/android/server/notification/ConditionProviders;->access$600()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 555
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    iget-object v4, v4, Lcom/android/server/notification/ConditionProviders;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "dnd_allowexception"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 556
    .local v0, "allowException":I
    if-ne v0, v6, :cond_3

    .line 557
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    const-string v5, "downtimeEnter-priority"

    invoke-virtual {v4, v6, v5}, Lcom/android/server/notification/ZenModeHelper;->setZenMode(ILjava/lang/String;)V

    .line 565
    .end local v0    # "allowException":I
    :goto_0
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    const-string v5, "downtime"

    invoke-virtual {v4, v1, v5}, Lcom/android/server/notification/ConditionProviders;->setZenModeCondition(Landroid/service/notification/Condition;Ljava/lang/String;)V

    .line 568
    .end local v1    # "condition":Landroid/service/notification/Condition;
    :cond_0
    if-nez p1, :cond_2

    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mDowntime:Lcom/android/server/notification/DowntimeConditionProvider;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$500(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/DowntimeConditionProvider;

    move-result-object v4

    iget-object v5, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mExitCondition:Landroid/service/notification/Condition;
    invoke-static {v5}, Lcom/android/server/notification/ConditionProviders;->access$700(Lcom/android/server/notification/ConditionProviders;)Landroid/service/notification/Condition;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/android/server/notification/DowntimeConditionProvider;->isDowntimeCondition(Landroid/service/notification/Condition;)Z

    move-result v4

    if-eqz v4, :cond_2

    if-eq v3, v6, :cond_1

    if-ne v3, v7, :cond_2

    .line 571
    :cond_1
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    const/4 v5, 0x0

    const-string v6, "downtimeExit"

    invoke-virtual {v4, v5, v6}, Lcom/android/server/notification/ZenModeHelper;->setZenMode(ILjava/lang/String;)V

    .line 573
    :cond_2
    return-void

    .line 559
    .restart local v0    # "allowException":I
    .restart local v1    # "condition":Landroid/service/notification/Condition;
    :cond_3
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    const-string v5, "downtimeEnter-none"

    invoke-virtual {v4, v7, v5}, Lcom/android/server/notification/ZenModeHelper;->setZenMode(ILjava/lang/String;)V

    goto :goto_0

    .line 563
    .end local v0    # "allowException":I
    :cond_4
    iget-object v4, p0, Lcom/android/server/notification/ConditionProviders$DowntimeCallback;->this$0:Lcom/android/server/notification/ConditionProviders;

    # getter for: Lcom/android/server/notification/ConditionProviders;->mZenModeHelper:Lcom/android/server/notification/ZenModeHelper;
    invoke-static {v4}, Lcom/android/server/notification/ConditionProviders;->access$400(Lcom/android/server/notification/ConditionProviders;)Lcom/android/server/notification/ZenModeHelper;

    move-result-object v4

    const-string v5, "downtimeEnter"

    invoke-virtual {v4, v6, v5}, Lcom/android/server/notification/ZenModeHelper;->setZenMode(ILjava/lang/String;)V

    goto :goto_0
.end method
