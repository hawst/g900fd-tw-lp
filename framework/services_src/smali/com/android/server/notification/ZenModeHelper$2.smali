.class Lcom/android/server/notification/ZenModeHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "ZenModeHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/notification/ZenModeHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/notification/ZenModeHelper;


# direct methods
.method constructor <init>(Lcom/android/server/notification/ZenModeHelper;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lcom/android/server/notification/ZenModeHelper$2;->this$0:Lcom/android/server/notification/ZenModeHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 517
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 518
    .local v0, "intentAction":Ljava/lang/String;
    const-string v1, "android.settings.PRAYMODE_PREFERENCE_CHANGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 519
    iget-object v1, p0, Lcom/android/server/notification/ZenModeHelper$2;->this$0:Lcom/android/server/notification/ZenModeHelper;

    invoke-virtual {v1}, Lcom/android/server/notification/ZenModeHelper;->setPrayMode()V

    .line 523
    :goto_0
    return-void

    .line 521
    :cond_0
    iget-object v1, p0, Lcom/android/server/notification/ZenModeHelper$2;->this$0:Lcom/android/server/notification/ZenModeHelper;

    # getter for: Lcom/android/server/notification/ZenModeHelper;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/server/notification/ZenModeHelper;->access$300(Lcom/android/server/notification/ZenModeHelper;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/notification/ZenModeHelper$2;->this$0:Lcom/android/server/notification/ZenModeHelper;

    # getter for: Lcom/android/server/notification/ZenModeHelper;->mRingerModeChanged:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/android/server/notification/ZenModeHelper;->access$200(Lcom/android/server/notification/ZenModeHelper;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
