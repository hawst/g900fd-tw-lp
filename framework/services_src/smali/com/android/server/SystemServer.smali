.class public final Lcom/android/server/SystemServer;
.super Ljava/lang/Object;
.source "SystemServer.java"


# static fields
.field private static final APPWIDGET_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.appwidget.AppWidgetService"

.field private static final BACKUP_MANAGER_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.backup.BackupManagerService$Lifecycle"

.field private static final DIR_ENCRYPTION:Z

.field private static final EARLIEST_SUPPORTED_TIME:J = 0x5265c00L

.field private static final ENCRYPTED_STATE:Ljava/lang/String; = "trigger_restart_min_framework"

.field private static final ENCRYPTING_STATE:Ljava/lang/String; = "trigger_restart_min_framework"

.field private static final ETHERNET_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.ethernet.EthernetService"

.field private static final JOB_SCHEDULER_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.job.JobSchedulerService"

.field private static final MSAP_WIFI_SERVICE_CLASS:Ljava/lang/String; = "com.samsung.android.server.wifi.MsapWifiService"

.field private static final PERSISTENT_DATA_BLOCK_PROP:Ljava/lang/String; = "ro.frp.pst"

.field private static final PRINT_MANAGER_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.print.PrintManagerService"

.field private static final SNAPSHOT_INTERVAL:J = 0x36ee80L

.field private static final TAG:Ljava/lang/String; = "SystemServer"

.field private static final USB_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.usb.UsbService$Lifecycle"

.field private static final VOICE_RECOGNITION_MANAGER_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.voiceinteraction.VoiceInteractionManagerService"

.field private static final WIFI_P2P_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.wifi.p2p.WifiP2pService"

.field private static final WIFI_SERVICE_CLASS:Ljava/lang/String; = "com.android.server.wifi.WifiService"

.field private static final is3LMAllowed:Z


# instance fields
.field private final isElasticEnabled:Z

.field private mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

.field private mAlarmManagerService:Lcom/android/server/AlarmManagerService;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

.field private final mFactoryTestMode:I

.field private mFirstBoot:Z

.field private mInstaller:Lcom/android/server/pm/Installer;

.field private mLightsService:Lcom/android/server/lights/LightsService;

.field private mMultiWindowFacadeService:Lcom/android/server/am/MultiWindowFacadeService;

.field private mOnlyCore:Z

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

.field private mPowerManagerService:Lcom/android/server/power/PowerManagerService;

.field private mProfilerSnapshotTimer:Ljava/util/Timer;

.field private mSystemContext:Landroid/content/Context;

.field private mSystemServiceManager:Lcom/android/server/SystemServiceManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 299
    const-string/jumbo v0, "ro.sec.fle.encryption"

    const-string v1, "false"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/android/server/SystemServer;->DIR_ENCRYPTION:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 291
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/SystemServer;->isElasticEnabled:Z

    .line 315
    invoke-static {}, Landroid/os/FactoryTest;->getMode()I

    move-result v0

    iput v0, p0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    .line 316
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/SystemServer;)Lcom/android/server/SystemServiceManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/SystemServer;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/SystemServer;)Lcom/android/server/am/ActivityManagerService;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/SystemServer;

    .prologue
    .line 230
    iget-object v0, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/server/SystemServer;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/SystemServer;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Throwable;

    .prologue
    .line 230
    invoke-direct {p0, p1, p2}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method private createSystemContext()V
    .locals 3

    .prologue
    .line 452
    invoke-static {}, Landroid/app/ActivityThread;->systemMain()Landroid/app/ActivityThread;

    move-result-object v0

    .line 453
    .local v0, "activityThread":Landroid/app/ActivityThread;
    invoke-virtual {v0}, Landroid/app/ActivityThread;->getSystemContext()Landroid/app/ContextImpl;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    .line 454
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    const v2, 0x103013f

    invoke-virtual {v1, v2}, Landroid/content/Context;->setTheme(I)V

    .line 455
    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 310
    new-instance v0, Lcom/android/server/SystemServer;

    invoke-direct {v0}, Lcom/android/server/SystemServer;-><init>()V

    invoke-direct {v0}, Lcom/android/server/SystemServer;->run()V

    .line 311
    return-void
.end method

.method private static native nativeInit()V
.end method

.method private performPendingShutdown()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 435
    const-string/jumbo v4, "sys.shutdown.requested"

    const-string v5, ""

    invoke-static {v4, v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 437
    .local v2, "shutdownAction":Ljava/lang/String;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 438
    invoke-virtual {v2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x31

    if-ne v4, v5, :cond_0

    move v1, v3

    .line 441
    .local v1, "reboot":Z
    :cond_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-le v4, v3, :cond_2

    .line 442
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 447
    .local v0, "reason":Ljava/lang/String;
    :goto_0
    invoke-static {v1, v0}, Lcom/android/server/power/ShutdownThread;->rebootOrShutdown(ZLjava/lang/String;)V

    .line 449
    .end local v0    # "reason":Ljava/lang/String;
    .end local v1    # "reboot":Z
    :cond_1
    return-void

    .line 444
    .restart local v1    # "reboot":Z
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "reason":Ljava/lang/String;
    goto :goto_0
.end method

.method private reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "e"    # Ljava/lang/Throwable;

    .prologue
    .line 430
    const-string v0, "SystemServer"

    const-string v1, "***********************************************"

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    const-string v0, "SystemServer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BOOT FAILURE "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Slog;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 432
    return-void
.end method

.method private run()V
    .locals 9

    .prologue
    const-wide/32 v4, 0x5265c00

    const-wide/32 v2, 0x36ee80

    const/4 v8, 0x1

    .line 323
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-gez v0, :cond_0

    .line 324
    const-string v0, "SystemServer"

    const-string v1, "System clock is before 1970; setting to 1970."

    invoke-static {v0, v1}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    invoke-static {v4, v5}, Landroid/os/SystemClock;->setCurrentTimeMillis(J)Z

    .line 329
    :cond_0
    const-string v0, "SystemServer"

    const-string v1, "!@Boot: Entered the Android system server!"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 333
    const/16 v0, 0xbc2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-static {v0, v4, v5}, Landroid/util/EventLog;->writeEvent(IJ)I

    .line 342
    const-string v0, "persist.sys.dalvik.vm.lib.2"

    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v1

    invoke-virtual {v1}, Ldalvik/system/VMRuntime;->vmLibrary()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    const-string/jumbo v0, "vold.decrypt"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 345
    .local v6, "cryptState":Ljava/lang/String;
    const-string/jumbo v0, "trigger_restart_min_framework"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 355
    :cond_1
    :goto_0
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 356
    invoke-static {}, Lcom/android/internal/os/SamplingProfilerIntegration;->start()V

    .line 357
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/android/server/SystemServer;->mProfilerSnapshotTimer:Ljava/util/Timer;

    .line 358
    iget-object v0, p0, Lcom/android/server/SystemServer;->mProfilerSnapshotTimer:Ljava/util/Timer;

    new-instance v1, Lcom/android/server/SystemServer$1;

    invoke-direct {v1, p0}, Lcom/android/server/SystemServer$1;-><init>(Lcom/android/server/SystemServer;)V

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 367
    :cond_2
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v0

    invoke-virtual {v0}, Ldalvik/system/VMRuntime;->clearGrowthLimit()V

    .line 371
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v0

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Ldalvik/system/VMRuntime;->setTargetHeapUtilization(F)F

    .line 375
    invoke-static {}, Landroid/os/Build;->ensureFingerprintProperty()V

    .line 379
    invoke-static {v8}, Landroid/os/Environment;->setUserRequired(Z)V

    .line 382
    invoke-static {v8}, Lcom/android/internal/os/BinderInternal;->disableBackgroundScheduling(Z)V

    .line 389
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/os/Process;->setCanSelfBackground(Z)V

    .line 390
    invoke-static {}, Landroid/os/Looper;->prepareMainLooper()V

    .line 393
    const-string v0, "android_servers"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 394
    invoke-static {}, Lcom/android/server/SystemServer;->nativeInit()V

    .line 398
    invoke-direct {p0}, Lcom/android/server/SystemServer;->performPendingShutdown()V

    .line 401
    invoke-direct {p0}, Lcom/android/server/SystemServer;->createSystemContext()V

    .line 404
    new-instance v0, Lcom/android/server/SystemServiceManager;

    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/SystemServiceManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    .line 405
    const-class v0, Lcom/android/server/SystemServiceManager;

    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    invoke-static {v0, v1}, Lcom/android/server/LocalServices;->addService(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 409
    :try_start_0
    invoke-direct {p0}, Lcom/android/server/SystemServer;->startBootstrapServices()V

    .line 410
    invoke-direct {p0}, Lcom/android/server/SystemServer;->startCoreServices()V

    .line 411
    invoke-direct {p0}, Lcom/android/server/SystemServer;->startOtherServices()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 419
    invoke-static {}, Landroid/os/StrictMode;->conditionallyEnableDebugLogging()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 420
    const-string v0, "SystemServer"

    const-string v1, "Enabled StrictMode for system server main thread."

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    :cond_3
    const-string v0, "SystemServer"

    const-string v1, "!@Boot: Loop forever"

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 426
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Main thread loop unexpectedly exited"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_4
    const-string/jumbo v0, "trigger_restart_min_framework"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 350
    invoke-static {}, Lcom/android/server/pm/PackagePrefetcher;->getInstance()Lcom/android/server/pm/PackagePrefetcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackagePrefetcher;->prefetchPermissions()V

    .line 351
    invoke-static {}, Lcom/android/server/pm/PackagePrefetcher;->getInstance()Lcom/android/server/pm/PackagePrefetcher;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/server/pm/PackagePrefetcher;->prefetchPackages()V

    goto/16 :goto_0

    .line 412
    :catch_0
    move-exception v7

    .line 413
    .local v7, "ex":Ljava/lang/Throwable;
    const-string v0, "System"

    const-string v1, "******************************************"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const-string v0, "System"

    const-string v1, "************ Failure starting system services"

    invoke-static {v0, v1, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 415
    throw v7
.end method

.method private startBootstrapServices()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 468
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v3, Lcom/android/server/pm/Installer;

    invoke-virtual {v1, v3}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v1

    check-cast v1, Lcom/android/server/pm/Installer;

    iput-object v1, p0, Lcom/android/server/SystemServer;->mInstaller:Lcom/android/server/pm/Installer;

    .line 471
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v3, Lcom/android/server/am/ActivityManagerService$Lifecycle;

    invoke-virtual {v1, v3}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v1

    check-cast v1, Lcom/android/server/am/ActivityManagerService$Lifecycle;

    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerService$Lifecycle;->getService()Lcom/android/server/am/ActivityManagerService;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    .line 473
    iget-object v1, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    iget-object v3, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    invoke-virtual {v1, v3}, Lcom/android/server/am/ActivityManagerService;->setSystemServiceManager(Lcom/android/server/SystemServiceManager;)V

    .line 479
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v3, Lcom/android/server/power/PowerManagerService;

    invoke-virtual {v1, v3}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v1

    check-cast v1, Lcom/android/server/power/PowerManagerService;

    iput-object v1, p0, Lcom/android/server/SystemServer;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    .line 483
    iget-object v1, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerService;->initPowerManagement()V

    .line 487
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v3, Lcom/android/server/display/DisplayManagerService;

    invoke-virtual {v1, v3}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v1

    check-cast v1, Lcom/android/server/display/DisplayManagerService;

    iput-object v1, p0, Lcom/android/server/SystemServer;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    .line 490
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const/16 v3, 0x64

    invoke-virtual {v1, v3}, Lcom/android/server/SystemServiceManager;->startBootPhase(I)V

    .line 493
    const-string/jumbo v1, "vold.decrypt"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 494
    .local v0, "cryptState":Ljava/lang/String;
    const-string/jumbo v1, "trigger_restart_min_framework"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 495
    const-string v1, "SystemServer"

    const-string v3, "Detected encryption in progress - only parsing core apps"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iput-boolean v2, p0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    .line 510
    :cond_0
    :goto_0
    const-string v1, "SystemServer"

    const-string v3, "!@Boot: Start PackageManagerService"

    invoke-static {v1, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    iget-object v3, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/SystemServer;->mInstaller:Lcom/android/server/pm/Installer;

    iget v1, p0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    if-eqz v1, :cond_2

    move v1, v2

    :goto_1
    iget-boolean v2, p0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    invoke-static {v3, v4, v1, v2}, Lcom/android/server/pm/PackageManagerService;->main(Landroid/content/Context;Lcom/android/server/pm/Installer;ZZ)Lcom/android/server/pm/PackageManagerService;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/SystemServer;->mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

    .line 513
    const-string v1, "SystemServer"

    const-string v2, "!@Boot: End PackageManagerService"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    iget-object v1, p0, Lcom/android/server/SystemServer;->mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v1}, Lcom/android/server/pm/PackageManagerService;->isFirstBoot()Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/server/SystemServer;->mFirstBoot:Z

    .line 515
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 517
    const-string v1, "SystemServer"

    const-string v2, "User Service"

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    const-string/jumbo v1, "user"

    invoke-static {}, Lcom/android/server/pm/UserManagerService;->getInstance()Lcom/android/server/pm/UserManagerService;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 521
    iget-object v1, p0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/android/server/AttributeCache;->init(Landroid/content/Context;)V

    .line 524
    iget-object v1, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v1}, Lcom/android/server/am/ActivityManagerService;->setSystemProcess()V

    .line 525
    return-void

    .line 497
    :cond_1
    const-string/jumbo v1, "trigger_restart_min_framework"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 498
    const-string v1, "SystemServer"

    const-string v3, "Device encrypted - only parsing core apps"

    invoke-static {v1, v3}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    iput-boolean v2, p0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    goto :goto_0

    .line 511
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private startCoreServices()V
    .locals 2

    .prologue
    .line 532
    iget-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v1, Lcom/android/server/lights/LightsService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v0

    check-cast v0, Lcom/android/server/lights/LightsService;

    iput-object v0, p0, Lcom/android/server/SystemServer;->mLightsService:Lcom/android/server/lights/LightsService;

    .line 535
    iget-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v1, Lcom/android/server/BatteryService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 538
    iget-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v1, Lcom/android/server/usage/UsageStatsService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 539
    iget-object v1, p0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    const-class v0, Landroid/app/usage/UsageStatsManagerInternal;

    invoke-static {v0}, Lcom/android/server/LocalServices;->getService(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/usage/UsageStatsManagerInternal;

    invoke-virtual {v1, v0}, Lcom/android/server/am/ActivityManagerService;->setUsageStatsManager(Landroid/app/usage/UsageStatsManagerInternal;)V

    .line 543
    iget-object v0, p0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v1, Lcom/android/server/webkit/WebViewUpdateService;

    invoke-virtual {v0, v1}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 544
    return-void
.end method

.method private static final startDpmService(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2226
    const/4 v4, 0x0

    .line 2227
    .local v4, "dpmObj":Ljava/lang/Object;
    :try_start_0
    const-string v6, "persist.dpm.feature"

    const/4 v7, 0x0

    invoke-static {v6, v7}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 2228
    .local v3, "dpmFeature":I
    const-string v6, "SystemServer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DPM configuration set to "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2230
    if-lez v3, :cond_0

    const/16 v6, 0x8

    if-ge v3, v6, :cond_0

    .line 2231
    new-instance v1, Ldalvik/system/PathClassLoader;

    const-string v6, "/system/framework/com.qti.dpmframework.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 2234
    .local v1, "dpmClassLoader":Ldalvik/system/PathClassLoader;
    const-string v6, "com.qti.dpm.DpmService"

    invoke-virtual {v1, v6}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 2235
    .local v0, "dpmClass":Ljava/lang/Class;
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    invoke-virtual {v0, v6}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    .line 2237
    .local v2, "dpmConstructor":Ljava/lang/reflect/Constructor;
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object p0, v6, v7

    invoke-virtual {v2, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 2239
    if-eqz v4, :cond_0

    :try_start_1
    instance-of v6, v4, Landroid/os/IBinder;

    if-eqz v6, :cond_0

    .line 2240
    const-string v6, "dpmservice"

    check-cast v4, Landroid/os/IBinder;

    .end local v4    # "dpmObj":Ljava/lang/Object;
    invoke-static {v6, v4}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 2241
    const-string v6, "SystemServer"

    const-string v7, "Created DPM Service"

    invoke-static {v6, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 2250
    .end local v0    # "dpmClass":Ljava/lang/Class;
    .end local v1    # "dpmClassLoader":Ldalvik/system/PathClassLoader;
    .end local v2    # "dpmConstructor":Ljava/lang/reflect/Constructor;
    .end local v3    # "dpmFeature":I
    :cond_0
    :goto_0
    return-void

    .line 2243
    .restart local v0    # "dpmClass":Ljava/lang/Class;
    .restart local v1    # "dpmClassLoader":Ldalvik/system/PathClassLoader;
    .restart local v2    # "dpmConstructor":Ljava/lang/reflect/Constructor;
    .restart local v3    # "dpmFeature":I
    :catch_0
    move-exception v5

    .line 2244
    .local v5, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v6, "SystemServer"

    const-string/jumbo v7, "starting DPM Service"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 2247
    .end local v0    # "dpmClass":Ljava/lang/Class;
    .end local v1    # "dpmClassLoader":Ldalvik/system/PathClassLoader;
    .end local v2    # "dpmConstructor":Ljava/lang/reflect/Constructor;
    .end local v3    # "dpmFeature":I
    .end local v5    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v5

    .line 2248
    .local v5, "e":Ljava/lang/Throwable;
    const-string v6, "SystemServer"

    const-string/jumbo v7, "starting DPM Service"

    invoke-static {v6, v7, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static final startEmergencyModeService(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2256
    :try_start_0
    invoke-static {p0}, Lcom/sec/android/emergencymode/EmergencyManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/emergencymode/EmergencyManager;

    move-result-object v1

    .line 2257
    .local v1, "emMgr":Lcom/sec/android/emergencymode/EmergencyManager;
    invoke-virtual {v1}, Lcom/sec/android/emergencymode/EmergencyManager;->readyEmergencyMode()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2262
    .end local v1    # "emMgr":Lcom/sec/android/emergencymode/EmergencyManager;
    :goto_0
    return-void

    .line 2258
    :catch_0
    move-exception v0

    .line 2259
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SystemServer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Starting emergency service failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private startOtherServices()V
    .locals 180

    .prologue
    .line 551
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/server/SystemServer;->mSystemContext:Landroid/content/Context;

    .line 552
    .local v4, "context":Landroid/content/Context;
    const/16 v40, 0x0

    .line 553
    .local v40, "accountManager":Lcom/android/server/accounts/AccountManagerService;
    const/16 v72, 0x0

    .line 554
    .local v72, "contentService":Lcom/android/server/content/ContentService;
    const/16 v168, 0x0

    .line 555
    .local v168, "vibrator":Lcom/android/server/VibratorService;
    const/16 v42, 0x0

    .line 556
    .local v42, "alarm":Landroid/app/IAlarmManager;
    const/16 v131, 0x0

    .line 557
    .local v131, "mountService":Lcom/android/server/MountService;
    const/16 v146, 0x0

    .line 558
    .local v146, "sdpService":Lcom/android/server/SdpManagerService;
    const/4 v8, 0x0

    .line 559
    .local v8, "networkManagement":Lcom/android/server/NetworkManagementService;
    const/4 v7, 0x0

    .line 560
    .local v7, "networkStats":Lcom/android/server/net/NetworkStatsService;
    const/16 v133, 0x0

    .line 561
    .local v133, "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    const/16 v64, 0x0

    .line 562
    .local v64, "connectivity":Lcom/android/server/ConnectivityService;
    const/16 v134, 0x0

    .line 563
    .local v134, "networkScore":Lcom/android/server/NetworkScoreService;
    const/16 v150, 0x0

    .line 564
    .local v150, "serviceDiscovery":Lcom/android/server/NsdService;
    const/16 v179, 0x0

    .line 565
    .local v179, "wm":Lcom/android/server/wm/WindowManagerService;
    const/16 v50, 0x0

    .line 566
    .local v50, "bluetooth":Lcom/android/server/BluetoothManagerService;
    const/16 v166, 0x0

    .line 567
    .local v166, "usb":Lcom/android/server/usb/UsbService;
    const/16 v148, 0x0

    .line 568
    .local v148, "serial":Lcom/android/server/SerialService;
    const/16 v137, 0x0

    .line 569
    .local v137, "networkTimeUpdater":Lcom/android/server/NetworkTimeUpdateService;
    const/16 v61, 0x0

    .line 570
    .local v61, "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    const/16 v106, 0x0

    .line 571
    .local v106, "inputManager":Lcom/android/server/input/InputManagerService;
    const/16 v155, 0x0

    .line 572
    .local v155, "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    const/16 v70, 0x0

    .line 574
    .local v70, "consumerIr":Lcom/android/server/ConsumerIrService;
    const/16 v79, 0x0

    .line 576
    .local v79, "dEncService":Lcom/android/server/DirEncryptService;
    const/16 v117, 0x0

    .line 577
    .local v117, "mHMS":Lcom/android/server/HarmonyEASService;
    const/16 v47, 0x0

    .line 578
    .local v47, "audioService":Landroid/media/AudioService;
    const/16 v81, 0x0

    .line 579
    .local v81, "deviceManager":Lcom/android/server/DeviceManager3LMService;
    const/16 v128, 0x0

    .line 581
    .local v128, "mmsService":Lcom/android/server/MmsServiceBroker;
    const/16 v142, 0x0

    .line 583
    .local v142, "quickconnect":Lcom/android/server/QuickConnectService;
    const/16 v160, 0x0

    .line 584
    .local v160, "timaService":Lcom/android/server/TimaService;
    const/16 v158, 0x0

    .line 586
    .local v158, "timaObserver":Lcom/android/server/TimaObserver;
    const/16 v119, 0x0

    .line 587
    .local v119, "mPersonaService":Lcom/android/server/pm/PersonaManagerService;
    const/16 v121, 0x0

    .line 592
    .local v121, "mRCPManagerService":Lcom/android/server/RCPManagerService;
    const/16 v178, 0x0

    .line 595
    .local v178, "wifiOffloadService":Lcom/android/server/wifioffload/WifiOffloadService;
    const-string v5, "config.disable_storage"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v92

    .line 596
    .local v92, "disableStorage":Z
    const-string v5, "config.disable_media"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v89

    .line 597
    .local v89, "disableMedia":Z
    const-string v5, "config.disable_bluetooth"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v87

    .line 598
    .local v87, "disableBluetooth":Z
    const-string v5, "config.disable_telephony"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v94

    .line 599
    .local v94, "disableTelephony":Z
    const-string v5, "config.disable_location"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v88

    .line 600
    .local v88, "disableLocation":Z
    const-string v5, "config.disable_systemui"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v93

    .line 601
    .local v93, "disableSystemUI":Z
    const-string v5, "config.disable_noncore"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v91

    .line 602
    .local v91, "disableNonCoreServices":Z
    const-string v5, "config.disable_network"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v90

    .line 603
    .local v90, "disableNetwork":Z
    const-string/jumbo v5, "ro.kernel.qemu"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v110

    .line 604
    .local v110, "isEmulator":Z
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x1120082

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v82

    .line 609
    .local v82, "digitalPenCapable":Z
    const/16 v86, 0x1

    .line 611
    .local v86, "disableAtlas":Z
    :try_start_0
    const-string v5, "SystemServer"

    const-string v6, "Reading configuration..."

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    invoke-static {}, Lcom/android/server/SystemConfig;->getInstance()Lcom/android/server/SystemConfig;

    .line 614
    const-string v5, "SystemServer"

    const-string v6, "Scheduling Policy"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    const-string/jumbo v5, "scheduling_policy"

    new-instance v6, Lcom/android/server/os/SchedulingPolicyService;

    invoke-direct {v6}, Lcom/android/server/os/SchedulingPolicyService;-><init>()V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 617
    const-string v5, "SystemServer"

    const-string v6, "Telephony Registry"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 618
    new-instance v156, Lcom/android/server/TelephonyRegistry;

    move-object/from16 v0, v156

    invoke-direct {v0, v4}, Lcom/android/server/TelephonyRegistry;-><init>(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_6a

    .line 619
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .local v156, "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    :try_start_1
    const-string/jumbo v5, "telephony.registry"

    move-object/from16 v0, v156

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 621
    const-string v5, "SystemServer"

    const-string v6, "Entropy Mixer"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    const-string v5, "entropy"

    new-instance v6, Lcom/android/server/EntropyMixer;

    invoke-direct {v6, v4}, Lcom/android/server/EntropyMixer;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    .line 627
    :try_start_2
    const-string v5, "SystemServer"

    const-string v6, "SEAMS"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    const-string v5, "SEAMService"

    new-instance v6, Lcom/android/server/SEAMService;

    invoke-direct {v6, v4}, Lcom/android/server/SEAMService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 637
    :goto_0
    :try_start_3
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v167

    .line 638
    .local v167, "versionInfo":Landroid/os/Bundle;
    const-string v5, "2.0"

    const-string/jumbo v6, "version"

    move-object/from16 v0, v167

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v5

    if-eqz v5, :cond_0

    .line 640
    :try_start_4
    const-string v5, "SystemServer"

    const-string v6, "Persona Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    invoke-static {}, Lcom/android/server/pm/PersonaManagerService;->getInstance()Lcom/android/server/pm/PersonaManagerService;

    move-result-object v119

    .line 642
    const-string v5, "persona"

    move-object/from16 v0, v119

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    .line 649
    :cond_0
    :goto_1
    :try_start_5
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/server/SystemServer;->mContentResolver:Landroid/content/ContentResolver;
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1

    .line 654
    :try_start_6
    const-string v5, "SystemServer"

    const-string v6, "Account Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    new-instance v41, Lcom/android/server/accounts/AccountManagerService;

    move-object/from16 v0, v41

    invoke-direct {v0, v4}, Lcom/android/server/accounts/AccountManagerService;-><init>(Landroid/content/Context;)V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1

    .line 656
    .end local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .local v41, "accountManager":Lcom/android/server/accounts/AccountManagerService;
    :try_start_7
    const-string v5, "account"

    move-object/from16 v0, v41

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_73
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_6b

    move-object/from16 v40, v41

    .line 662
    .end local v41    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .restart local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    :goto_2
    :try_start_8
    const-string/jumbo v5, "ro.product.name"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v120

    .line 663
    .local v120, "mProductName":Ljava/lang/String;
    const-string v5, "ktt"

    move-object/from16 v0, v120

    invoke-virtual {v0, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {}, Landroid/os/FactoryTest;->isFactoryBinary()Z
    :try_end_8
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_1

    move-result v5

    if-nez v5, :cond_1

    .line 666
    :try_start_9
    const-string v5, "SystemServer"

    const-string v6, "KT UCA Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    const-string v5, "ktuca"

    new-instance v6, Landroid/ktuca/KtUcaService;

    invoke-direct {v6, v4}, Landroid/ktuca/KtUcaService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_9} :catch_1

    .line 674
    :cond_1
    :goto_3
    :try_start_a
    const-string v5, "SystemServer"

    const-string v6, "Content Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3c

    const/4 v5, 0x1

    :goto_4
    invoke-static {v4, v5}, Lcom/android/server/content/ContentService;->main(Landroid/content/Context;Z)Lcom/android/server/content/ContentService;

    move-result-object v72

    .line 680
    new-instance v80, Lcom/android/server/DirEncryptService;

    move-object/from16 v0, v80

    invoke-direct {v0, v4}, Lcom/android/server/DirEncryptService;-><init>(Landroid/content/Context;)V
    :try_end_a
    .catch Ljava/lang/RuntimeException; {:try_start_a .. :try_end_a} :catch_1

    .line 682
    .end local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    .local v80, "dEncService":Lcom/android/server/DirEncryptService;
    :try_start_b
    sget-boolean v5, Lcom/android/server/SystemServer;->DIR_ENCRYPTION:Z

    if-eqz v5, :cond_2

    .line 683
    const-string v5, "DirEncryptService"

    move-object/from16 v0, v80

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_b
    .catch Ljava/lang/Throwable; {:try_start_b .. :try_end_b} :catch_5
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_6c

    :cond_2
    move-object/from16 v79, v80

    .line 692
    .end local v80    # "dEncService":Lcom/android/server/DirEncryptService;
    .restart local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    :goto_5
    :try_start_c
    const-string v5, "SystemServer"

    const-string v6, "Reactive Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_1

    .line 694
    :try_start_d
    const-string v5, "ReactiveService"

    new-instance v6, Lcom/android/server/ReactiveService;

    invoke-direct {v6, v4}, Lcom/android/server/ReactiveService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_6
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_1

    .line 701
    :goto_6
    :try_start_e
    const-string v5, "SystemServer"

    const-string v6, "System Content Providers"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->installSystemProviders()V
    :try_end_e
    .catch Ljava/lang/RuntimeException; {:try_start_e .. :try_end_e} :catch_1

    .line 704
    :try_start_f
    const-string/jumbo v5, "sedenial"

    new-instance v6, Lcom/android/server/SEDenialService;

    invoke-direct {v6, v4}, Lcom/android/server/SEDenialService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 705
    const-string v5, "SystemServer"

    const-string v6, "SEDenial service added"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_7
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_1

    .line 710
    :goto_7
    :try_start_10
    const-string v5, "SystemServer"

    const-string v6, "Vibrator Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 711
    new-instance v169, Lcom/android/server/VibratorService;

    move-object/from16 v0, v169

    invoke-direct {v0, v4}, Lcom/android/server/VibratorService;-><init>(Landroid/content/Context;)V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_1

    .line 712
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .local v169, "vibrator":Lcom/android/server/VibratorService;
    :try_start_11
    const-string/jumbo v5, "vibrator"

    move-object/from16 v0, v169

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 715
    sget-boolean v5, Lcom/samsung/android/toolbox/TwToolBoxService;->TOOLBOX_SUPPORT:Z

    if-eqz v5, :cond_3

    .line 716
    const-string v5, "SystemServer"

    const-string v6, "Tw ToolBox Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    new-instance v163, Lcom/samsung/android/toolbox/TwToolBoxService;

    move-object/from16 v0, v163

    invoke-direct {v0, v4}, Lcom/samsung/android/toolbox/TwToolBoxService;-><init>(Landroid/content/Context;)V

    .line 718
    .local v163, "toolbox":Lcom/samsung/android/toolbox/TwToolBoxService;
    const-string/jumbo v5, "tw_toolbox"

    move-object/from16 v0, v163

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 723
    .end local v163    # "toolbox":Lcom/samsung/android/toolbox/TwToolBoxService;
    :cond_3
    const-string v5, "1"

    const-string/jumbo v6, "ro.config.tima"

    const-string v9, "0"

    invoke-static {v6, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_11
    .catch Ljava/lang/RuntimeException; {:try_start_11 .. :try_end_11} :catch_9

    move-result v157

    .line 724
    .local v157, "timaEnabled":Z
    if-eqz v157, :cond_4

    .line 726
    :try_start_12
    const-string v5, "SystemServer"

    const-string v6, "TIMA Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    const/4 v5, -0x2

    invoke-static {v5}, Landroid/os/Process;->setThreadPriority(I)V

    .line 730
    new-instance v161, Lcom/android/server/TimaService;

    move-object/from16 v0, v161

    invoke-direct {v0, v4}, Lcom/android/server/TimaService;-><init>(Landroid/content/Context;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_12} :catch_8
    .catch Ljava/lang/RuntimeException; {:try_start_12 .. :try_end_12} :catch_9

    .line 731
    .end local v160    # "timaService":Lcom/android/server/TimaService;
    .local v161, "timaService":Lcom/android/server/TimaService;
    :try_start_13
    const-string/jumbo v5, "tima"

    move-object/from16 v0, v161

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_13
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_13} :catch_72
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_13} :catch_6d

    move-object/from16 v160, v161

    .line 736
    .end local v161    # "timaService":Lcom/android/server/TimaService;
    .restart local v160    # "timaService":Lcom/android/server/TimaService;
    :goto_8
    const/16 v5, -0x13

    :try_start_14
    invoke-static {v5}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_14 .. :try_end_14} :catch_9

    .line 740
    :try_start_15
    const-string v5, "SystemServer"

    const-string v6, "TIMA Observer"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    new-instance v159, Lcom/android/server/TimaObserver;

    move-object/from16 v0, v159

    invoke-direct {v0, v4}, Lcom/android/server/TimaObserver;-><init>(Landroid/content/Context;)V
    :try_end_15
    .catch Ljava/lang/Throwable; {:try_start_15 .. :try_end_15} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_15 .. :try_end_15} :catch_9

    .end local v158    # "timaObserver":Lcom/android/server/TimaObserver;
    .local v159, "timaObserver":Lcom/android/server/TimaObserver;
    move-object/from16 v158, v159

    .line 749
    .end local v159    # "timaObserver":Lcom/android/server/TimaObserver;
    .restart local v158    # "timaObserver":Lcom/android/server/TimaObserver;
    :goto_9
    :try_start_16
    const-string v5, "3.0"

    const-string/jumbo v6, "ro.config.timaversion"

    const-string v9, "0"

    invoke-static {v6, v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v162

    .line 750
    .local v162, "timaversion":Z
    if-eqz v162, :cond_4

    .line 751
    const-string v5, "com.sec.tima.TimaKeyStoreProvider"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v112

    .line 753
    .local v112, "keyStoreClass":Ljava/lang/Class;
    invoke-virtual/range {v112 .. v112}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/security/Provider;

    invoke-static {v5}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 754
    const-string v5, "SystemServer"

    const-string v6, "Added TimaKesytore provider"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_b
    .catch Ljava/lang/RuntimeException; {:try_start_16 .. :try_end_16} :catch_9

    .line 764
    .end local v112    # "keyStoreClass":Ljava/lang/Class;
    .end local v162    # "timaversion":Z
    :cond_4
    :goto_a
    const/16 v98, 0x1

    .line 765
    .local v98, "enabledMDM":Z
    const/4 v5, 0x1

    move/from16 v0, v98

    if-ne v5, v0, :cond_5

    .line 766
    const/16 v97, 0x1

    .line 767
    .local v97, "enabledCEP":Z
    const/4 v5, 0x1

    move/from16 v0, v97

    if-ne v5, v0, :cond_3d

    .line 769
    :try_start_17
    const-string v5, "SystemServer"

    const-string v6, "CEP Proxy KS Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 770
    const-string v5, "cepproxyks"

    new-instance v6, Lcom/android/server/enterprise/scep/ScepKeystoreProxyService;

    invoke-direct {v6, v4}, Lcom/android/server/enterprise/scep/ScepKeystoreProxyService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_17
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_17} :catch_c
    .catch Ljava/lang/RuntimeException; {:try_start_17 .. :try_end_17} :catch_9

    .line 780
    .end local v97    # "enabledCEP":Z
    :cond_5
    :goto_b
    :try_start_18
    const-string v5, "SystemServer"

    const-string v6, "SSRM Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_18
    .catch Ljava/lang/RuntimeException; {:try_start_18 .. :try_end_18} :catch_9

    .line 782
    :try_start_19
    new-instance v53, Ldalvik/system/PathClassLoader;

    const-string v5, "/system/framework/ssrm.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    move-object/from16 v0, v53

    invoke-direct {v0, v5, v6}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 783
    .local v53, "cfmsClassLoader":Ldalvik/system/PathClassLoader;
    const-string v5, "com.android.server.ssrm.CustomFrequencyManagerService"

    move-object/from16 v0, v53

    invoke-virtual {v0, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v52

    .line 784
    .local v52, "cfmsClass":Ljava/lang/Class;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    const/4 v6, 0x1

    const-class v9, Landroid/app/IActivityManager;

    aput-object v9, v5, v6

    move-object/from16 v0, v52

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v54

    .line 785
    .local v54, "cfmsConstructor":Ljava/lang/reflect/Constructor;
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    aput-object v9, v5, v6

    move-object/from16 v0, v54

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v151

    check-cast v151, Landroid/os/IBinder;

    .line 786
    .local v151, "ssrmService":Landroid/os/IBinder;
    const-string v5, "CustomFrequencyManagerService"

    move-object/from16 v0, v151

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_19} :catch_d
    .catch Ljava/lang/RuntimeException; {:try_start_19 .. :try_end_19} :catch_9

    .line 798
    .end local v52    # "cfmsClass":Ljava/lang/Class;
    .end local v53    # "cfmsClassLoader":Ldalvik/system/PathClassLoader;
    .end local v54    # "cfmsConstructor":Ljava/lang/reflect/Constructor;
    .end local v151    # "ssrmService":Landroid/os/IBinder;
    :goto_c
    :try_start_1a
    invoke-virtual {v4}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v55

    .line 799
    .local v55, "cl":Ljava/lang/ClassLoader;
    const-string v5, "com.samsung.android.smartface.SmartFaceManager"

    const/4 v6, 0x1

    move-object/from16 v0, v55

    invoke-static {v5, v6, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v58

    .line 800
    .local v58, "class_SmartFaceManager":Ljava/lang/Class;
    const-string v5, "SMARTFACE_SERVICE"

    move-object/from16 v0, v58

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v102

    .line 802
    .local v102, "field_SMARTFACE_SERVICE":Ljava/lang/reflect/Field;
    const-string v5, "com.samsung.android.smartface.SmartFaceService"

    const/4 v6, 0x1

    move-object/from16 v0, v55

    invoke-static {v5, v6, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v59

    .line 803
    .local v59, "class_SmartFaceService":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v59

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v69

    .line 805
    .local v69, "constructor_SmartFaceService":Ljava/lang/reflect/Constructor;
    const/4 v5, 0x0

    move-object/from16 v0, v102

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v6, v9

    move-object/from16 v0, v69

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/IBinder;

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 806
    const-string v5, "SystemServer"

    const-string v6, "SmartFaceService loaded!"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_1a} :catch_e
    .catch Ljava/lang/RuntimeException; {:try_start_1a .. :try_end_1a} :catch_9

    .line 813
    .end local v55    # "cl":Ljava/lang/ClassLoader;
    .end local v58    # "class_SmartFaceManager":Ljava/lang/Class;
    .end local v59    # "class_SmartFaceService":Ljava/lang/Class;
    .end local v69    # "constructor_SmartFaceService":Ljava/lang/reflect/Constructor;
    .end local v102    # "field_SMARTFACE_SERVICE":Ljava/lang/reflect/Field;
    :goto_d
    :try_start_1b
    const-string v5, "SystemServer"

    const-string v6, "Consumer IR Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    new-instance v71, Lcom/android/server/ConsumerIrService;

    move-object/from16 v0, v71

    invoke-direct {v0, v4}, Lcom/android/server/ConsumerIrService;-><init>(Landroid/content/Context;)V
    :try_end_1b
    .catch Ljava/lang/RuntimeException; {:try_start_1b .. :try_end_1b} :catch_9

    .line 815
    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .local v71, "consumerIr":Lcom/android/server/ConsumerIrService;
    :try_start_1c
    const-string v5, "consumer_ir"

    move-object/from16 v0, v71

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 817
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/AlarmManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v5

    check-cast v5, Lcom/android/server/AlarmManagerService;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/server/SystemServer;->mAlarmManagerService:Lcom/android/server/AlarmManagerService;

    .line 818
    const-string v5, "alarm"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/app/IAlarmManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/IAlarmManager;

    move-result-object v42

    .line 821
    const-string v5, "SystemServer"

    const-string v6, "Init Watchdog"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 822
    invoke-static {}, Lcom/android/server/Watchdog;->getInstance()Lcom/android/server/Watchdog;

    move-result-object v174

    .line 823
    .local v174, "watchdog":Lcom/android/server/Watchdog;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    move-object/from16 v0, v174

    invoke-virtual {v0, v4, v5}, Lcom/android/server/Watchdog;->init(Landroid/content/Context;Lcom/android/server/am/ActivityManagerService;)V

    .line 825
    const-string v5, "SystemServer"

    const-string v6, "Input Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 826
    new-instance v107, Lcom/android/server/input/InputManagerService;

    move-object/from16 v0, v107

    invoke-direct {v0, v4}, Lcom/android/server/input/InputManagerService;-><init>(Landroid/content/Context;)V
    :try_end_1c
    .catch Ljava/lang/RuntimeException; {:try_start_1c .. :try_end_1c} :catch_6e

    .line 828
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .local v107, "inputManager":Lcom/android/server/input/InputManagerService;
    :try_start_1d
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.feature.sensorhub"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 829
    const-string v5, "SystemServer"

    const-string v6, "Context Aware Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    const-string v5, "context_aware"

    new-instance v6, Lcom/samsung/android/contextaware/manager/ContextAwareService;

    invoke-direct {v6, v4}, Lcom/samsung/android/contextaware/manager/ContextAwareService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 833
    :cond_6
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.feature.sensorhub"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "com.sec.feature.scontext_lite"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 835
    :cond_7
    const-string v5, "SystemServer"

    const-string v6, "SContext Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    const-string/jumbo v5, "scontext"

    new-instance v6, Landroid/hardware/scontext/SContextService;

    invoke-direct {v6, v4}, Landroid/hardware/scontext/SContextService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 840
    :cond_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "com.sec.feature.barcode_emulator"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z
    :try_end_1d
    .catch Ljava/lang/RuntimeException; {:try_start_1d .. :try_end_1d} :catch_10

    move-result v5

    if-eqz v5, :cond_9

    .line 842
    :try_start_1e
    const-string v5, "SystemServer"

    const-string v6, "BarBeamService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 843
    new-instance v49, Landroid/app/BarBeamService;

    move-object/from16 v0, v49

    invoke-direct {v0, v4}, Landroid/app/BarBeamService;-><init>(Landroid/content/Context;)V

    .line 845
    .local v49, "barbeam":Landroid/app/BarBeamService;
    const-string v5, "barbeam"

    move-object/from16 v0, v49

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_1e} :catch_f
    .catch Ljava/lang/RuntimeException; {:try_start_1e .. :try_end_1e} :catch_10

    .line 855
    .end local v49    # "barbeam":Landroid/app/BarBeamService;
    :cond_9
    :goto_e
    :try_start_1f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/am/MultiWindowFacadeService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v5

    check-cast v5, Lcom/android/server/am/MultiWindowFacadeService;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/android/server/SystemServer;->mMultiWindowFacadeService:Lcom/android/server/am/MultiWindowFacadeService;

    .line 861
    const-string v5, "SystemServer"

    const-string v6, "Window Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_3e

    const/4 v5, 0x1

    move v6, v5

    :goto_f
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/server/SystemServer;->mFirstBoot:Z

    if-nez v5, :cond_3f

    const/4 v5, 0x1

    :goto_10
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    move-object/from16 v0, v107

    invoke-static {v4, v0, v6, v5, v9}, Lcom/android/server/wm/WindowManagerService;->main(Landroid/content/Context;Lcom/android/server/input/InputManagerService;ZZZ)Lcom/android/server/wm/WindowManagerService;

    move-result-object v179

    .line 865
    const-string/jumbo v5, "window"

    move-object/from16 v0, v179

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 866
    const-string v5, "input"

    move-object/from16 v0, v107

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 868
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    move-object/from16 v0, v179

    invoke-virtual {v5, v0}, Lcom/android/server/am/ActivityManagerService;->setWindowManager(Lcom/android/server/wm/WindowManagerService;)V

    .line 872
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mMultiWindowFacadeService:Lcom/android/server/am/MultiWindowFacadeService;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/am/MultiWindowFacadeService;->setAcitivityManager(Lcom/android/server/am/ActivityManagerService;)V

    .line 873
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mMultiWindowFacadeService:Lcom/android/server/am/MultiWindowFacadeService;

    move-object/from16 v0, v179

    invoke-virtual {v5, v0}, Lcom/android/server/am/MultiWindowFacadeService;->setWindowManager(Lcom/android/server/wm/WindowManagerService;)V

    .line 877
    invoke-virtual/range {v179 .. v179}, Lcom/android/server/wm/WindowManagerService;->getInputMonitor()Lcom/android/server/wm/InputMonitor;

    move-result-object v5

    move-object/from16 v0, v107

    invoke-virtual {v0, v5}, Lcom/android/server/input/InputManagerService;->setWindowManagerCallbacks(Lcom/android/server/input/InputManagerService$WindowManagerCallbacks;)V

    .line 878
    invoke-virtual/range {v107 .. v107}, Lcom/android/server/input/InputManagerService;->start()V

    .line 881
    const-string v5, "SystemServer"

    const-string v6, "TactileAssist Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 882
    new-instance v154, Lcom/android/server/TactileAssistService;

    move-object/from16 v0, v154

    invoke-direct {v0, v4}, Lcom/android/server/TactileAssistService;-><init>(Landroid/content/Context;)V

    .line 883
    .local v154, "tactileAssist":Lcom/android/server/TactileAssistService;
    const-string/jumbo v5, "tactileassist"

    move-object/from16 v0, v154

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 887
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    invoke-virtual {v5}, Lcom/android/server/display/DisplayManagerService;->windowManagerAndInputReady()V

    .line 892
    if-eqz v110, :cond_40

    .line 893
    const-string v5, "SystemServer"

    const-string v6, "No Bluetooh Service (emulator)"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 914
    :goto_11
    const-string v5, "SystemServer"

    const-string v6, "RCP Manager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1f
    .catch Ljava/lang/RuntimeException; {:try_start_1f .. :try_end_1f} :catch_10

    .line 916
    :try_start_20
    new-instance v122, Lcom/android/server/RCPManagerService;

    move-object/from16 v0, v122

    invoke-direct {v0, v4}, Lcom/android/server/RCPManagerService;-><init>(Landroid/content/Context;)V
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_20 .. :try_end_20} :catch_11
    .catch Ljava/lang/RuntimeException; {:try_start_20 .. :try_end_20} :catch_10

    .line 917
    .end local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .local v122, "mRCPManagerService":Lcom/android/server/RCPManagerService;
    :try_start_21
    const-string/jumbo v5, "rcp"

    move-object/from16 v0, v122

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_21} :catch_71
    .catch Ljava/lang/RuntimeException; {:try_start_21 .. :try_end_21} :catch_70

    move-object/from16 v121, v122

    .line 924
    .end local v122    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .restart local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    :goto_12
    :try_start_22
    new-instance v5, Landroid/security/AndroidKeyStoreProvider;

    invoke-direct {v5}, Landroid/security/AndroidKeyStoreProvider;-><init>()V

    invoke-static {v5}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I
    :try_end_22
    .catch Ljava/lang/RuntimeException; {:try_start_22 .. :try_end_22} :catch_10

    move-object/from16 v70, v71

    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v106, v107

    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    move-object/from16 v168, v169

    .line 931
    .end local v98    # "enabledMDM":Z
    .end local v120    # "mProductName":Ljava/lang/String;
    .end local v154    # "tactileAssist":Lcom/android/server/TactileAssistService;
    .end local v157    # "timaEnabled":Z
    .end local v167    # "versionInfo":Landroid/os/Bundle;
    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .end local v174    # "watchdog":Lcom/android/server/Watchdog;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    :goto_13
    const/16 v152, 0x0

    .line 932
    .local v152, "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    const/16 v139, 0x0

    .line 933
    .local v139, "notification":Landroid/app/INotificationManager;
    const/16 v104, 0x0

    .line 934
    .local v104, "imm":Lcom/android/server/InputMethodManagerService;
    const/16 v172, 0x0

    .line 935
    .local v172, "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    const/16 v113, 0x0

    .line 936
    .local v113, "location":Lcom/android/server/LocationManagerService;
    const/16 v144, 0x0

    .line 937
    .local v144, "sLocation":Landroid/os/IBinder;
    const/16 v73, 0x0

    .line 938
    .local v73, "countryDetector":Lcom/android/server/CountryDetectorService;
    const/16 v164, 0x0

    .line 939
    .local v164, "tsms":Lcom/android/server/TextServicesManagerService;
    const/16 v115, 0x0

    .line 941
    .local v115, "lockSettings":Lcom/android/server/LockSettingsService;
    const/16 v170, 0x0

    .line 943
    .local v170, "vrManager":Lcom/android/server/VRManagerService;
    const/16 v45, 0x0

    .line 944
    .local v45, "atlas":Lcom/android/server/AssetAtlasService;
    const/16 v125, 0x0

    .line 947
    .local v125, "mediaRouter":Lcom/android/server/media/MediaRouterService;
    const/16 v99, 0x0

    .line 951
    .local v99, "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    const/16 v60, 0x0

    .line 955
    .local v60, "cocktailBar":Lcom/android/server/cocktailbar/CocktailBarManagerService;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_a

    .line 959
    :try_start_23
    const-string v5, "SystemServer"

    const-string v6, "Input Method Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 960
    new-instance v105, Lcom/android/server/InputMethodManagerService;

    move-object/from16 v0, v105

    move-object/from16 v1, v179

    invoke-direct {v0, v4, v1}, Lcom/android/server/InputMethodManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    :try_end_23
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_23} :catch_12

    .line 961
    .end local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .local v105, "imm":Lcom/android/server/InputMethodManagerService;
    :try_start_24
    const-string v5, "input_method"

    move-object/from16 v0, v105

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_24 .. :try_end_24} :catch_69

    move-object/from16 v104, v105

    .line 967
    .end local v105    # "imm":Lcom/android/server/InputMethodManagerService;
    .restart local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    :goto_14
    :try_start_25
    const-string v5, "SystemServer"

    const-string v6, "Accessibility Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 968
    const-string v5, "accessibility"

    new-instance v6, Lcom/android/server/accessibility/AccessibilityManagerService;

    invoke-direct {v6, v4}, Lcom/android/server/accessibility/AccessibilityManagerService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_25} :catch_13

    .line 977
    :cond_a
    :goto_15
    :try_start_26
    invoke-virtual/range {v179 .. v179}, Lcom/android/server/wm/WindowManagerService;->displayReady()V
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_26} :catch_14

    .line 983
    :goto_16
    :try_start_27
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->performBootDexOpt()V
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_27 .. :try_end_27} :catch_15

    .line 989
    :goto_17
    :try_start_28
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v5

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x1040491

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    const/4 v9, 0x0

    invoke-interface {v5, v6, v9}, Landroid/app/IActivityManager;->showBootMessage(Ljava/lang/CharSequence;Z)V
    :try_end_28
    .catch Landroid/os/RemoteException; {:try_start_28 .. :try_end_28} :catch_68

    .line 998
    :goto_18
    :try_start_29
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v140

    .line 999
    .local v140, "pM":Landroid/content/pm/PackageManager;
    if-eqz v140, :cond_b

    .line 1000
    const-string v5, "SystemServer"

    const-string v6, "PackageManager is not null!!"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1001
    const-string v5, "com.sec.feature.motionrecognition_service"

    move-object/from16 v0, v140

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 1002
    new-instance v130, Ldalvik/system/PathClassLoader;

    const-string v5, "/system/framework/motionrecognitionservice.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    move-object/from16 v0, v130

    invoke-direct {v0, v5, v6}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 1003
    .local v130, "motionClassLoader":Ldalvik/system/PathClassLoader;
    const-string v5, "com.samsung.android.motion.MotionRecognitionService"

    move-object/from16 v0, v130

    invoke-virtual {v0, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v56

    .line 1004
    .local v56, "class_MotionRecognitionService":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v56

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v67

    .line 1005
    .local v67, "constructor_MotionRecognitionService":Ljava/lang/reflect/Constructor;
    const-string v6, "motion_recognition"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v5, v9

    move-object/from16 v0, v67

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    invoke-static {v6, v5}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1006
    const-string v5, "SystemServer"

    const-string v6, "MotionRecognitionService Service!"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_29
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_29} :catch_16

    .line 1015
    .end local v56    # "class_MotionRecognitionService":Ljava/lang/Class;
    .end local v67    # "constructor_MotionRecognitionService":Ljava/lang/reflect/Constructor;
    .end local v130    # "motionClassLoader":Ldalvik/system/PathClassLoader;
    .end local v140    # "pM":Landroid/content/pm/PackageManager;
    :cond_b
    :goto_19
    const/16 v75, 0x0

    .line 1017
    .local v75, "coverService":Lcom/android/server/cover/CoverManagerService;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_d

    .line 1018
    const-string/jumbo v5, "vold.decrypt"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v77

    .line 1019
    .local v77, "cryptState":Ljava/lang/String;
    const-string/jumbo v5, "trigger_restart_min_framework"

    move-object/from16 v0, v77

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    const-string/jumbo v5, "trigger_restart_min_framework"

    move-object/from16 v0, v77

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_44

    .line 1020
    :cond_c
    const-string v5, "SystemServer"

    const-string v6, "Detected encryption in progress - unable CoverManagerService"

    invoke-static {v5, v6}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    .end local v77    # "cryptState":Ljava/lang/String;
    :cond_d
    :goto_1a
    const-string v5, "0"

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_e

    .line 1043
    :try_start_2a
    const-string v5, "SystemServer"

    const-string v6, "Add FM_RADIO_SERVICE"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1044
    const-string v5, "FMPlayer"

    new-instance v6, Lcom/android/server/FMRadioService;

    invoke-direct {v6, v4}, Lcom/android/server/FMRadioService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1045
    const-string v5, "SystemServer"

    const-string v6, "FMRadio service added.."

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_2a .. :try_end_2a} :catch_18

    .line 1051
    :cond_e
    :goto_1b
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_4d

    .line 1052
    if-nez v92, :cond_f

    const-string v5, "0"

    const-string/jumbo v6, "system_init.startmountservice"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 1059
    :try_start_2b
    const-string v5, "SystemServer"

    const-string v6, "Mount Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1060
    new-instance v132, Lcom/android/server/MountService;

    move-object/from16 v0, v132

    invoke-direct {v0, v4}, Lcom/android/server/MountService;-><init>(Landroid/content/Context;)V
    :try_end_2b
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_2b} :catch_19

    .line 1061
    .end local v131    # "mountService":Lcom/android/server/MountService;
    .local v132, "mountService":Lcom/android/server/MountService;
    :try_start_2c
    const-string v5, "mount"

    move-object/from16 v0, v132

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_2c
    .catch Ljava/lang/Throwable; {:try_start_2c .. :try_end_2c} :catch_66

    move-object/from16 v131, v132

    .line 1069
    .end local v132    # "mountService":Lcom/android/server/MountService;
    .restart local v131    # "mountService":Lcom/android/server/MountService;
    :cond_f
    :goto_1c
    :try_start_2d
    const-string v5, "SystemServer"

    const-string v6, "DirEncryptSerrvice"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    if-eqz v79, :cond_10

    .line 1071
    const-string v5, "SystemServer"

    const-string v6, "DirEncryptService.SystemReady"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    invoke-virtual/range {v79 .. v79}, Lcom/android/server/DirEncryptService;->systemReady()V
    :try_end_2d
    .catch Ljava/lang/Throwable; {:try_start_2d .. :try_end_2d} :catch_1a

    .line 1091
    :cond_10
    :goto_1d
    if-nez v91, :cond_12

    .line 1093
    :try_start_2e
    const-string v5, "SystemServer"

    const-string v6, "LockSettingsService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1094
    new-instance v116, Lcom/android/server/LockSettingsService;

    move-object/from16 v0, v116

    invoke-direct {v0, v4}, Lcom/android/server/LockSettingsService;-><init>(Landroid/content/Context;)V
    :try_end_2e
    .catch Ljava/lang/Throwable; {:try_start_2e .. :try_end_2e} :catch_1b

    .line 1095
    .end local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .local v116, "lockSettings":Lcom/android/server/LockSettingsService;
    :try_start_2f
    const-string v5, "lock_settings"

    move-object/from16 v0, v116

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_2f
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_2f} :catch_65

    move-object/from16 v115, v116

    .line 1100
    .end local v116    # "lockSettings":Lcom/android/server/LockSettingsService;
    .restart local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    :goto_1e
    const-string/jumbo v5, "ro.frp.pst"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_11

    .line 1101
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/PersistentDataBlockService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1106
    :cond_11
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/devicepolicy/DevicePolicyManagerService$Lifecycle;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1111
    :cond_12
    :try_start_30
    const-string v5, "SystemServer"

    const-string v6, "HarmonyEAS service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1112
    new-instance v118, Lcom/android/server/HarmonyEASService;

    move-object/from16 v0, v118

    invoke-direct {v0, v4}, Lcom/android/server/HarmonyEASService;-><init>(Landroid/content/Context;)V
    :try_end_30
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_30} :catch_1c

    .line 1113
    .end local v117    # "mHMS":Lcom/android/server/HarmonyEASService;
    .local v118, "mHMS":Lcom/android/server/HarmonyEASService;
    :try_start_31
    const-string v5, "harmony_eas_service"

    move-object/from16 v0, v118

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1114
    const-string v5, "SystemServer"

    const-string v6, "Harmony EAS service created..."

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_31
    .catch Ljava/lang/Throwable; {:try_start_31 .. :try_end_31} :catch_64

    move-object/from16 v117, v118

    .line 1122
    .end local v118    # "mHMS":Lcom/android/server/HarmonyEASService;
    .restart local v117    # "mHMS":Lcom/android/server/HarmonyEASService;
    :goto_1f
    :try_start_32
    const-string v5, "SystemServer"

    const-string v6, "SdpManagerService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    new-instance v147, Lcom/android/server/SdpManagerService;

    move-object/from16 v0, v147

    invoke-direct {v0, v4}, Lcom/android/server/SdpManagerService;-><init>(Landroid/content/Context;)V
    :try_end_32
    .catch Ljava/lang/Throwable; {:try_start_32 .. :try_end_32} :catch_1d

    .line 1124
    .end local v146    # "sdpService":Lcom/android/server/SdpManagerService;
    .local v147, "sdpService":Lcom/android/server/SdpManagerService;
    :try_start_33
    const-string/jumbo v5, "sdp"

    move-object/from16 v0, v147

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_33
    .catch Ljava/lang/Throwable; {:try_start_33 .. :try_end_33} :catch_63

    move-object/from16 v146, v147

    .line 1131
    .end local v147    # "sdpService":Lcom/android/server/SdpManagerService;
    .restart local v146    # "sdpService":Lcom/android/server/SdpManagerService;
    :goto_20
    if-nez v91, :cond_13

    .line 1133
    :try_start_34
    const-string v5, "SystemServer"

    const-string v6, "Enterprise Policy"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1134
    new-instance v100, Lcom/android/server/enterprise/EnterpriseDeviceManagerService;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

    const/4 v6, 0x0

    move-object/from16 v0, v100

    invoke-direct {v0, v4, v5, v6}, Lcom/android/server/enterprise/EnterpriseDeviceManagerService;-><init>(Landroid/content/Context;Landroid/content/pm/IPackageManager;Landroid/app/admin/IDevicePolicyManager;)V
    :try_end_34
    .catch Ljava/lang/Throwable; {:try_start_34 .. :try_end_34} :catch_1e

    .line 1135
    .end local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .local v100, "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    :try_start_35
    const-string v5, "enterprise_policy"

    move-object/from16 v0, v100

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1136
    const-string v5, "SystemServer"

    const-string v6, "Enterprise Policymanager service created..."

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_35 .. :try_end_35} :catch_62

    move-object/from16 v99, v100

    .line 1143
    .end local v100    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .restart local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    :cond_13
    :goto_21
    if-nez v93, :cond_14

    .line 1145
    :try_start_36
    const-string v5, "SystemServer"

    const-string v6, "Status Bar"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    new-instance v153, Lcom/android/server/statusbar/StatusBarManagerService;

    move-object/from16 v0, v153

    move-object/from16 v1, v179

    invoke-direct {v0, v4, v1}, Lcom/android/server/statusbar/StatusBarManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V
    :try_end_36
    .catch Ljava/lang/Throwable; {:try_start_36 .. :try_end_36} :catch_1f

    .line 1147
    .end local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .local v153, "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    :try_start_37
    const-string/jumbo v5, "statusbar"

    move-object/from16 v0, v153

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_37
    .catch Ljava/lang/Throwable; {:try_start_37 .. :try_end_37} :catch_61

    move-object/from16 v152, v153

    .line 1153
    .end local v153    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .restart local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    :cond_14
    :goto_22
    if-nez v91, :cond_15

    .line 1155
    :try_start_38
    const-string v5, "SystemServer"

    const-string v6, "Clipboard Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1156
    const-string v5, "clipboard"

    new-instance v6, Lcom/android/server/clipboard/ClipboardService;

    invoke-direct {v6, v4}, Lcom/android/server/clipboard/ClipboardService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_38
    .catch Ljava/lang/Throwable; {:try_start_38 .. :try_end_38} :catch_20

    .line 1164
    :cond_15
    :goto_23
    if-nez v91, :cond_16

    .line 1166
    :try_start_39
    const-string v5, "SystemServer"

    const-string v6, "ClipboardEx Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    const-string v5, "clipboardEx"

    new-instance v6, Lcom/android/server/clipboardex/ClipboardExService;

    invoke-direct {v6, v4}, Lcom/android/server/clipboardex/ClipboardExService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_39
    .catch Ljava/lang/Throwable; {:try_start_39 .. :try_end_39} :catch_21

    .line 1175
    :cond_16
    :goto_24
    if-nez v90, :cond_17

    .line 1177
    :try_start_3a
    const-string v5, "SystemServer"

    const-string v6, "NetworkManagement Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1178
    invoke-static {v4}, Lcom/android/server/NetworkManagementService;->create(Landroid/content/Context;)Lcom/android/server/NetworkManagementService;

    move-result-object v8

    .line 1179
    const-string v5, "network_management"

    invoke-static {v5, v8}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_3a .. :try_end_3a} :catch_22

    .line 1187
    :cond_17
    :goto_25
    const-string v5, "SystemServer"

    const-string v6, "SEC_PRODUCT_FEATURE_KNOX_SUPPORT_ABSOLUTE_ANTITHEFT=FALSE - true"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1190
    :try_start_3b
    const-string v5, "SystemServer"

    const-string v6, "Absolute Persistence Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1191
    const-string v5, "ABTPersistenceService"

    new-instance v6, Lcom/absolute/android/persistservice/ABTPersistenceService;

    invoke-direct {v6, v4}, Lcom/absolute/android/persistservice/ABTPersistenceService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3b
    .catch Ljava/lang/Throwable; {:try_start_3b .. :try_end_3b} :catch_23

    .line 1198
    :goto_26
    if-nez v91, :cond_18

    .line 1200
    :try_start_3c
    const-string v5, "SystemServer"

    const-string v6, "Text Service Manager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1201
    new-instance v165, Lcom/android/server/TextServicesManagerService;

    move-object/from16 v0, v165

    invoke-direct {v0, v4}, Lcom/android/server/TextServicesManagerService;-><init>(Landroid/content/Context;)V
    :try_end_3c
    .catch Ljava/lang/Throwable; {:try_start_3c .. :try_end_3c} :catch_24

    .line 1202
    .end local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .local v165, "tsms":Lcom/android/server/TextServicesManagerService;
    :try_start_3d
    const-string/jumbo v5, "textservices"

    move-object/from16 v0, v165

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3d
    .catch Ljava/lang/Throwable; {:try_start_3d .. :try_end_3d} :catch_60

    move-object/from16 v164, v165

    .line 1208
    .end local v165    # "tsms":Lcom/android/server/TextServicesManagerService;
    .restart local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    :cond_18
    :goto_27
    if-nez v90, :cond_4c

    .line 1210
    :try_start_3e
    const-string v5, "SystemServer"

    const-string v6, "Network Score Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    new-instance v135, Lcom/android/server/NetworkScoreService;

    move-object/from16 v0, v135

    invoke-direct {v0, v4}, Lcom/android/server/NetworkScoreService;-><init>(Landroid/content/Context;)V
    :try_end_3e
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_3e} :catch_25

    .line 1212
    .end local v134    # "networkScore":Lcom/android/server/NetworkScoreService;
    .local v135, "networkScore":Lcom/android/server/NetworkScoreService;
    :try_start_3f
    const-string v5, "network_score"

    move-object/from16 v0, v135

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_3f
    .catch Ljava/lang/Throwable; {:try_start_3f .. :try_end_3f} :catch_5f

    move-object/from16 v134, v135

    .line 1218
    .end local v135    # "networkScore":Lcom/android/server/NetworkScoreService;
    .restart local v134    # "networkScore":Lcom/android/server/NetworkScoreService;
    :goto_28
    :try_start_40
    const-string v5, "SystemServer"

    const-string v6, "NetworkStats Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1219
    new-instance v136, Lcom/android/server/net/NetworkStatsService;

    move-object/from16 v0, v136

    move-object/from16 v1, v42

    invoke-direct {v0, v4, v8, v1}, Lcom/android/server/net/NetworkStatsService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/app/IAlarmManager;)V
    :try_end_40
    .catch Ljava/lang/Throwable; {:try_start_40 .. :try_end_40} :catch_26

    .line 1220
    .end local v7    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    .local v136, "networkStats":Lcom/android/server/net/NetworkStatsService;
    :try_start_41
    const-string v5, "netstats"

    move-object/from16 v0, v136

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_41
    .catch Ljava/lang/Throwable; {:try_start_41 .. :try_end_41} :catch_5e

    move-object/from16 v7, v136

    .line 1226
    .end local v136    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    .restart local v7    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    :goto_29
    :try_start_42
    const-string v5, "SystemServer"

    const-string v6, "NetworkPolicy Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1227
    new-instance v3, Lcom/android/server/net/NetworkPolicyManagerService;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    const-string/jumbo v6, "power"

    invoke-static {v6}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v6

    check-cast v6, Landroid/os/IPowerManager;

    invoke-direct/range {v3 .. v8}, Lcom/android/server/net/NetworkPolicyManagerService;-><init>(Landroid/content/Context;Landroid/app/IActivityManager;Landroid/os/IPowerManager;Landroid/net/INetworkStatsService;Landroid/os/INetworkManagementService;)V
    :try_end_42
    .catch Ljava/lang/Throwable; {:try_start_42 .. :try_end_42} :catch_27

    .line 1231
    .end local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .local v3, "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    :try_start_43
    const-string v5, "netpolicy"

    invoke-static {v5, v3}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_43
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_43} :catch_5d

    .line 1236
    :goto_2a
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.wifi.p2p.WifiP2pService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1237
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.wifi.WifiService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1238
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.samsung.android.server.wifi.MsapWifiService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1239
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.wifi.WifiScanningService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1242
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.wifi.RttService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1245
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.hardware.ethernet"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_46

    .line 1252
    :goto_2b
    :try_start_44
    const-string v5, "SystemServer"

    const-string v6, "Connectivity Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1253
    new-instance v65, Lcom/android/server/ConnectivityService;

    move-object/from16 v0, v65

    invoke-direct {v0, v4, v8, v7, v3}, Lcom/android/server/ConnectivityService;-><init>(Landroid/content/Context;Landroid/os/INetworkManagementService;Landroid/net/INetworkStatsService;Landroid/net/INetworkPolicyManager;)V
    :try_end_44
    .catch Ljava/lang/Throwable; {:try_start_44 .. :try_end_44} :catch_28

    .line 1255
    .end local v64    # "connectivity":Lcom/android/server/ConnectivityService;
    .local v65, "connectivity":Lcom/android/server/ConnectivityService;
    :try_start_45
    const-string v5, "connectivity"

    move-object/from16 v0, v65

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1256
    move-object/from16 v0, v65

    invoke-virtual {v7, v0}, Lcom/android/server/net/NetworkStatsService;->bindConnectivityManager(Landroid/net/IConnectivityManager;)V

    .line 1257
    move-object/from16 v0, v65

    invoke-virtual {v3, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->bindConnectivityManager(Landroid/net/IConnectivityManager;)V
    :try_end_45
    .catch Ljava/lang/Throwable; {:try_start_45 .. :try_end_45} :catch_5c

    move-object/from16 v64, v65

    .line 1264
    .end local v65    # "connectivity":Lcom/android/server/ConnectivityService;
    .restart local v64    # "connectivity":Lcom/android/server/ConnectivityService;
    :goto_2c
    :try_start_46
    const-string v5, "SystemServer"

    const-string v6, "SmartBonding Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    invoke-virtual {v4}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v55

    .line 1266
    .restart local v55    # "cl":Ljava/lang/ClassLoader;
    const-string v5, "com.samsung.android.smartbonding.SmartBondingService"

    const/4 v6, 0x1

    move-object/from16 v0, v55

    invoke-static {v5, v6, v0}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v57

    .line 1267
    .local v57, "class_SmartBondingService":Ljava/lang/Class;
    const-string v5, "SMARTBONDING_SERVICE"

    move-object/from16 v0, v57

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v101

    .line 1269
    .local v101, "field_SMARTBONDING_SERVICE":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v57

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v68

    .line 1271
    .local v68, "constructor_SmartBondingService":Ljava/lang/reflect/Constructor;
    const/4 v5, 0x0

    move-object/from16 v0, v101

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v6, v9

    move-object/from16 v0, v68

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/IBinder;

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 1272
    const-string v5, "SystemServer"

    const-string v6, "SmartBonding loaded"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_46
    .catch Ljava/lang/Throwable; {:try_start_46 .. :try_end_46} :catch_29

    .line 1347
    .end local v55    # "cl":Ljava/lang/ClassLoader;
    .end local v57    # "class_SmartBondingService":Ljava/lang/Class;
    .end local v68    # "constructor_SmartBondingService":Ljava/lang/reflect/Constructor;
    .end local v101    # "field_SMARTBONDING_SERVICE":Ljava/lang/reflect/Field;
    :goto_2d
    :try_start_47
    const-string v5, "SystemServer"

    const-string v6, "Network Service Discovery Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    invoke-static {v4}, Lcom/android/server/NsdService;->create(Landroid/content/Context;)Lcom/android/server/NsdService;

    move-result-object v150

    .line 1349
    const-string/jumbo v5, "servicediscovery"

    move-object/from16 v0, v150

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_47
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_47} :catch_2a

    .line 1355
    :goto_2e
    :try_start_48
    const-string v5, "SystemServer"

    const-string v6, "DPM Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1356
    invoke-static {v4}, Lcom/android/server/SystemServer;->startDpmService(Landroid/content/Context;)V
    :try_end_48
    .catch Ljava/lang/Throwable; {:try_start_48 .. :try_end_48} :catch_2b

    .line 1362
    :goto_2f
    if-nez v91, :cond_19

    .line 1364
    :try_start_49
    const-string v5, "SystemServer"

    const-string v6, "UpdateLock Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    const-string/jumbo v5, "updatelock"

    new-instance v6, Lcom/android/server/UpdateLockService;

    invoke-direct {v6, v4}, Lcom/android/server/UpdateLockService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_49
    .catch Ljava/lang/Throwable; {:try_start_49 .. :try_end_49} :catch_2c

    .line 1377
    :cond_19
    :goto_30
    if-eqz v131, :cond_1a

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    if-nez v5, :cond_1a

    .line 1379
    const-string/jumbo v5, "vold.decrypt"

    const-string v6, "null"

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "trigger_restart_min_framework"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1a

    .line 1380
    invoke-virtual/range {v131 .. v131}, Lcom/android/server/MountService;->waitForAsecScan()V

    .line 1384
    :cond_1a
    if-eqz v40, :cond_1b

    .line 1385
    :try_start_4a
    invoke-virtual/range {v40 .. v40}, Lcom/android/server/accounts/AccountManagerService;->systemReady()V
    :try_end_4a
    .catch Ljava/lang/Throwable; {:try_start_4a .. :try_end_4a} :catch_2d

    .line 1391
    :cond_1b
    :goto_31
    if-eqz v72, :cond_1c

    .line 1392
    :try_start_4b
    invoke-virtual/range {v72 .. v72}, Lcom/android/server/content/ContentService;->systemReady()V
    :try_end_4b
    .catch Ljava/lang/Throwable; {:try_start_4b .. :try_end_4b} :catch_2e

    .line 1397
    :cond_1c
    :goto_32
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/notification/NotificationManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1398
    const-string v5, "notification"

    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    move-result-object v139

    .line 1400
    move-object/from16 v0, v139

    invoke-virtual {v3, v0}, Lcom/android/server/net/NetworkPolicyManagerService;->bindNotificationManager(Landroid/app/INotificationManager;)V

    .line 1416
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/storage/DeviceStorageMonitorService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1418
    if-nez v88, :cond_1d

    .line 1420
    :try_start_4c
    const-string v5, "SystemServer"

    const-string v6, "Location Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1421
    new-instance v114, Lcom/android/server/LocationManagerService;

    move-object/from16 v0, v114

    invoke-direct {v0, v4}, Lcom/android/server/LocationManagerService;-><init>(Landroid/content/Context;)V
    :try_end_4c
    .catch Ljava/lang/Throwable; {:try_start_4c .. :try_end_4c} :catch_2f

    .line 1422
    .end local v113    # "location":Lcom/android/server/LocationManagerService;
    .local v114, "location":Lcom/android/server/LocationManagerService;
    :try_start_4d
    const-string v5, "location"

    move-object/from16 v0, v114

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_4d
    .catch Ljava/lang/Throwable; {:try_start_4d .. :try_end_4d} :catch_5b

    move-object/from16 v113, v114

    .line 1428
    .end local v114    # "location":Lcom/android/server/LocationManagerService;
    .restart local v113    # "location":Lcom/android/server/LocationManagerService;
    :goto_33
    :try_start_4e
    const-string v5, "SystemServer"

    const-string v6, "Country Detector"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1429
    new-instance v74, Lcom/android/server/CountryDetectorService;

    move-object/from16 v0, v74

    invoke-direct {v0, v4}, Lcom/android/server/CountryDetectorService;-><init>(Landroid/content/Context;)V
    :try_end_4e
    .catch Ljava/lang/Throwable; {:try_start_4e .. :try_end_4e} :catch_30

    .line 1430
    .end local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .local v74, "countryDetector":Lcom/android/server/CountryDetectorService;
    :try_start_4f
    const-string v5, "country_detector"

    move-object/from16 v0, v74

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_4f
    .catch Ljava/lang/Throwable; {:try_start_4f .. :try_end_4f} :catch_5a

    move-object/from16 v73, v74

    .line 1466
    .end local v74    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .restart local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    :cond_1d
    :goto_34
    :try_start_50
    const-string v5, "SystemServer"

    const-string v6, "SLocation Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1467
    const-string v5, "com.samsung.location.SLocationLoader"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v145

    .line 1468
    .local v145, "sLocationLoader":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "getSLocationService"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v6, v9

    move-object/from16 v0, v145

    invoke-virtual {v0, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v103

    .line 1469
    .local v103, "getSLocationService":Ljava/lang/reflect/Method;
    const/4 v5, 0x0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v6, v9

    move-object/from16 v0, v103

    invoke-virtual {v0, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Landroid/os/IBinder;

    move-object/from16 v144, v0

    .line 1470
    const-string/jumbo v5, "sec_location"

    move-object/from16 v0, v144

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_50
    .catch Ljava/lang/Throwable; {:try_start_50 .. :try_end_50} :catch_31

    .line 1475
    .end local v103    # "getSLocationService":Ljava/lang/reflect/Method;
    .end local v145    # "sLocationLoader":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :goto_35
    if-nez v91, :cond_1e

    .line 1477
    :try_start_51
    const-string v5, "SystemServer"

    const-string v6, "Search Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1478
    const-string/jumbo v5, "search"

    new-instance v6, Lcom/android/server/search/SearchManagerService;

    invoke-direct {v6, v4}, Lcom/android/server/search/SearchManagerService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_51
    .catch Ljava/lang/Throwable; {:try_start_51 .. :try_end_51} :catch_32

    .line 1486
    :cond_1e
    :goto_36
    :try_start_52
    const-string v5, "SystemServer"

    const-string v6, "DropBox Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    const-string v5, "dropbox"

    new-instance v6, Lcom/android/server/DropBoxManagerService;

    new-instance v9, Ljava/io/File;

    const-string v10, "/data/system/dropbox"

    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v4, v9}, Lcom/android/server/DropBoxManagerService;-><init>(Landroid/content/Context;Ljava/io/File;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_52
    .catch Ljava/lang/Throwable; {:try_start_52 .. :try_end_52} :catch_33

    .line 1493
    :goto_37
    if-nez v91, :cond_1f

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x1120039

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_1f

    .line 1496
    :try_start_53
    const-string v5, "SystemServer"

    const-string v6, "Wallpaper Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1497
    new-instance v173, Lcom/android/server/wallpaper/WallpaperManagerService;

    move-object/from16 v0, v173

    invoke-direct {v0, v4}, Lcom/android/server/wallpaper/WallpaperManagerService;-><init>(Landroid/content/Context;)V
    :try_end_53
    .catch Ljava/lang/Throwable; {:try_start_53 .. :try_end_53} :catch_34

    .line 1498
    .end local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    .local v173, "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :try_start_54
    const-string/jumbo v5, "wallpaper"

    move-object/from16 v0, v173

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_54
    .catch Ljava/lang/Throwable; {:try_start_54 .. :try_end_54} :catch_59

    move-object/from16 v172, v173

    .line 1504
    .end local v173    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    .restart local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :cond_1f
    :goto_38
    if-nez v89, :cond_20

    const-string v5, "0"

    const-string/jumbo v6, "system_init.startaudioservice"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_20

    .line 1506
    :try_start_55
    const-string v5, "SystemServer"

    const-string v6, "Audio Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1507
    new-instance v48, Landroid/media/AudioService;

    move-object/from16 v0, v48

    invoke-direct {v0, v4}, Landroid/media/AudioService;-><init>(Landroid/content/Context;)V
    :try_end_55
    .catch Ljava/lang/Throwable; {:try_start_55 .. :try_end_55} :catch_35

    .line 1508
    .end local v47    # "audioService":Landroid/media/AudioService;
    .local v48, "audioService":Landroid/media/AudioService;
    :try_start_56
    const-string v5, "audio"

    move-object/from16 v0, v48

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_56
    .catch Ljava/lang/Throwable; {:try_start_56 .. :try_end_56} :catch_58

    move-object/from16 v47, v48

    .line 1514
    .end local v48    # "audioService":Landroid/media/AudioService;
    .restart local v47    # "audioService":Landroid/media/AudioService;
    :cond_20
    :goto_39
    if-nez v91, :cond_21

    .line 1515
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/DockObserver;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1518
    :cond_21
    if-nez v89, :cond_22

    .line 1520
    :try_start_57
    const-string v5, "SystemServer"

    const-string v6, "Wired Accessory Manager"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1522
    new-instance v5, Lcom/android/server/WiredAccessoryManager;

    move-object/from16 v0, v106

    invoke-direct {v5, v4, v0}, Lcom/android/server/WiredAccessoryManager;-><init>(Landroid/content/Context;Lcom/android/server/input/InputManagerService;)V

    move-object/from16 v0, v106

    invoke-virtual {v0, v5}, Lcom/android/server/input/InputManagerService;->setWiredAccessoryCallbacks(Lcom/android/server/input/InputManagerService$WiredAccessoryCallbacks;)V
    :try_end_57
    .catch Ljava/lang/Throwable; {:try_start_57 .. :try_end_57} :catch_36

    .line 1529
    :cond_22
    :goto_3a
    if-nez v91, :cond_25

    .line 1530
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.hardware.usb.host"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_23

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.hardware.usb.accessory"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_24

    .line 1534
    :cond_23
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.usb.UsbService$Lifecycle"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1538
    :cond_24
    :try_start_58
    const-string v5, "SystemServer"

    const-string v6, "Serial Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1540
    new-instance v149, Lcom/android/server/SerialService;

    move-object/from16 v0, v149

    invoke-direct {v0, v4}, Lcom/android/server/SerialService;-><init>(Landroid/content/Context;)V
    :try_end_58
    .catch Ljava/lang/Throwable; {:try_start_58 .. :try_end_58} :catch_37

    .line 1541
    .end local v148    # "serial":Lcom/android/server/SerialService;
    .local v149, "serial":Lcom/android/server/SerialService;
    :try_start_59
    const-string/jumbo v5, "serial"

    move-object/from16 v0, v149

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_59
    .catch Ljava/lang/Throwable; {:try_start_59 .. :try_end_59} :catch_57

    move-object/from16 v148, v149

    .line 1549
    .end local v149    # "serial":Lcom/android/server/SerialService;
    .restart local v148    # "serial":Lcom/android/server/SerialService;
    :cond_25
    :goto_3b
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_Common_EnableSUA"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_26

    .line 1552
    :try_start_5a
    const-string v5, "SystemServer"

    const-string v6, "KiesUsb Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1553
    const-string v5, "kiesusb"

    new-instance v6, Lcom/android/server/KiesConnectivity/KiesUsbObserver;

    invoke-direct {v6, v4}, Lcom/android/server/KiesConnectivity/KiesUsbObserver;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5a
    .catch Ljava/lang/Throwable; {:try_start_5a .. :try_end_5a} :catch_38

    .line 1560
    :cond_26
    :goto_3c
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/twilight/TwilightService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1562
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/UiModeManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1564
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/job/JobSchedulerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1566
    if-nez v91, :cond_29

    .line 1567
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.software.backup"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 1568
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.backup.BackupManagerService$Lifecycle"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1571
    :cond_27
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.software.app_widgets"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_28

    .line 1572
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.appwidget.AppWidgetService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1575
    :cond_28
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.software.voice_recognizers"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_29

    .line 1576
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.voiceinteraction.VoiceInteractionManagerService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1580
    :cond_29
    const-string/jumbo v5, "ro.SecEDS.enable"

    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    .line 1581
    .local v38, "SecEDSEnable":Ljava/lang/String;
    const-string v5, "SystemServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "SecEDS Service ro.tvout.enable = "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v38

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1582
    const-string v5, "false"

    move-object/from16 v0, v38

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2a

    .line 1585
    :try_start_5b
    const-string v5, "SystemServer"

    const-string v6, "Starting SecEDSEnable Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1586
    const/16 v96, 0x0

    .line 1587
    .local v96, "edsclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v5, "com.android.server.SecExternalDisplayService"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v96

    .line 1588
    if-nez v96, :cond_47

    .line 1589
    const-string v5, "SystemServer"

    const-string v6, "eds Service not exist"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5b
    .catch Ljava/lang/Throwable; {:try_start_5b .. :try_end_5b} :catch_39

    .line 1605
    .end local v96    # "edsclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_2a
    :goto_3d
    :try_start_5c
    const-string v5, "SystemServer"

    const-string v6, "DiskStats Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1606
    const-string v5, "diskstats"

    new-instance v6, Lcom/android/server/DiskStatsService;

    invoke-direct {v6, v4}, Lcom/android/server/DiskStatsService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5c
    .catch Ljava/lang/Throwable; {:try_start_5c .. :try_end_5c} :catch_3a

    .line 1623
    :goto_3e
    :try_start_5d
    const-string v5, "SystemServer"

    const-string v6, "mDNIe Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1624
    const/16 v123, 0x0

    .line 1625
    .local v123, "mdnieClass":Ljava/lang/Class;
    const-string v5, "com.samsung.android.mdnie.MdnieManagerService"

    invoke-static {v5}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v123

    .line 1626
    if-nez v123, :cond_48

    .line 1627
    const-string v5, "SystemServer"

    const-string v6, "Mdnie Service does not exist"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5d
    .catch Ljava/lang/Throwable; {:try_start_5d .. :try_end_5d} :catch_3b

    .line 1640
    .end local v123    # "mdnieClass":Ljava/lang/Class;
    :goto_3f
    :try_start_5e
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v140

    .line 1641
    .restart local v140    # "pM":Landroid/content/pm/PackageManager;
    if-eqz v140, :cond_49

    .line 1643
    const/16 v109, 0x1

    .line 1657
    .local v109, "isAddService":Z
    if-eqz v109, :cond_2b

    .line 1658
    const-string v5, "SystemServer"

    const-string v6, "Starting SpenGestureManagerService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1659
    const-string/jumbo v5, "spengestureservice"

    new-instance v6, Lcom/android/server/smartclip/SpenGestureManagerService;

    move-object/from16 v0, v179

    invoke-direct {v6, v4, v0}, Lcom/android/server/smartclip/SpenGestureManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_5e
    .catch Ljava/lang/Throwable; {:try_start_5e .. :try_end_5e} :catch_3c

    .line 1683
    .end local v109    # "isAddService":Z
    .end local v140    # "pM":Landroid/content/pm/PackageManager;
    :cond_2b
    :goto_40
    :try_start_5f
    const-string v5, "SystemServer"

    const-string v6, "QuickConnect Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1684
    new-instance v143, Lcom/android/server/QuickConnectService;

    move-object/from16 v0, v143

    invoke-direct {v0, v4}, Lcom/android/server/QuickConnectService;-><init>(Landroid/content/Context;)V
    :try_end_5f
    .catch Ljava/lang/Throwable; {:try_start_5f .. :try_end_5f} :catch_3d

    .line 1685
    .end local v142    # "quickconnect":Lcom/android/server/QuickConnectService;
    .local v143, "quickconnect":Lcom/android/server/QuickConnectService;
    :try_start_60
    const-string/jumbo v5, "quickconnect"

    move-object/from16 v0, v143

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_60
    .catch Ljava/lang/Throwable; {:try_start_60 .. :try_end_60} :catch_56

    move-object/from16 v142, v143

    .line 1695
    .end local v143    # "quickconnect":Lcom/android/server/QuickConnectService;
    .restart local v142    # "quickconnect":Lcom/android/server/QuickConnectService;
    :goto_41
    :try_start_61
    const-string v5, "SystemServer"

    const-string v6, "SamplingProfiler Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1696
    const-string/jumbo v5, "samplingprofiler"

    new-instance v6, Lcom/android/server/SamplingProfilerService;

    invoke-direct {v6, v4}, Lcom/android/server/SamplingProfilerService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_61
    .catch Ljava/lang/Throwable; {:try_start_61 .. :try_end_61} :catch_3e

    .line 1702
    :goto_42
    if-nez v90, :cond_2c

    .line 1704
    :try_start_62
    const-string v5, "SystemServer"

    const-string v6, "NetworkTimeUpdateService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1705
    new-instance v138, Lcom/android/server/NetworkTimeUpdateService;

    move-object/from16 v0, v138

    invoke-direct {v0, v4}, Lcom/android/server/NetworkTimeUpdateService;-><init>(Landroid/content/Context;)V
    :try_end_62
    .catch Ljava/lang/Throwable; {:try_start_62 .. :try_end_62} :catch_3f

    .end local v137    # "networkTimeUpdater":Lcom/android/server/NetworkTimeUpdateService;
    .local v138, "networkTimeUpdater":Lcom/android/server/NetworkTimeUpdateService;
    move-object/from16 v137, v138

    .line 1711
    .end local v138    # "networkTimeUpdater":Lcom/android/server/NetworkTimeUpdateService;
    .restart local v137    # "networkTimeUpdater":Lcom/android/server/NetworkTimeUpdateService;
    :cond_2c
    :goto_43
    if-nez v89, :cond_2d

    .line 1713
    :try_start_63
    const-string v5, "SystemServer"

    const-string v6, "CommonTimeManagementService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1714
    new-instance v62, Lcom/android/server/CommonTimeManagementService;

    move-object/from16 v0, v62

    invoke-direct {v0, v4}, Lcom/android/server/CommonTimeManagementService;-><init>(Landroid/content/Context;)V
    :try_end_63
    .catch Ljava/lang/Throwable; {:try_start_63 .. :try_end_63} :catch_40

    .line 1715
    .end local v61    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    .local v62, "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    :try_start_64
    const-string v5, "commontime_management"

    move-object/from16 v0, v62

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_64
    .catch Ljava/lang/Throwable; {:try_start_64 .. :try_end_64} :catch_55

    move-object/from16 v61, v62

    .line 1721
    .end local v62    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    .restart local v61    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    :cond_2d
    :goto_44
    if-nez v90, :cond_2e

    .line 1723
    :try_start_65
    const-string v5, "SystemServer"

    const-string v6, "CertBlacklister"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1724
    new-instance v5, Lcom/android/server/CertBlacklister;

    invoke-direct {v5, v4}, Lcom/android/server/CertBlacklister;-><init>(Landroid/content/Context;)V
    :try_end_65
    .catch Ljava/lang/Throwable; {:try_start_65 .. :try_end_65} :catch_41

    .line 1730
    :cond_2e
    :goto_45
    if-nez v91, :cond_2f

    .line 1732
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/dreams/DreamManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1735
    :cond_2f
    if-nez v91, :cond_30

    if-nez v86, :cond_30

    .line 1737
    :try_start_66
    const-string v5, "SystemServer"

    const-string v6, "Assets Atlas Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1738
    new-instance v46, Lcom/android/server/AssetAtlasService;

    move-object/from16 v0, v46

    invoke-direct {v0, v4}, Lcom/android/server/AssetAtlasService;-><init>(Landroid/content/Context;)V
    :try_end_66
    .catch Ljava/lang/Throwable; {:try_start_66 .. :try_end_66} :catch_42

    .line 1739
    .end local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .local v46, "atlas":Lcom/android/server/AssetAtlasService;
    :try_start_67
    const-string v5, "assetatlas"

    move-object/from16 v0, v46

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_67
    .catch Ljava/lang/Throwable; {:try_start_67 .. :try_end_67} :catch_54

    move-object/from16 v45, v46

    .line 1745
    .end local v46    # "atlas":Lcom/android/server/AssetAtlasService;
    .restart local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    :cond_30
    :goto_46
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.software.print"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_31

    .line 1746
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.print.PrintManagerService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    .line 1749
    :cond_31
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/restrictions/RestrictionsManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1751
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/media/MediaSessionService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1753
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.hardware.hdmi.cec"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_32

    .line 1754
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/hdmi/HdmiControlService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1757
    :cond_32
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string v6, "android.software.live_tv"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_33

    .line 1758
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/tv/TvInputManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1761
    :cond_33
    if-nez v91, :cond_34

    .line 1763
    :try_start_68
    const-string v5, "SystemServer"

    const-string v6, "Media Router Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1764
    new-instance v126, Lcom/android/server/media/MediaRouterService;

    move-object/from16 v0, v126

    invoke-direct {v0, v4}, Lcom/android/server/media/MediaRouterService;-><init>(Landroid/content/Context;)V
    :try_end_68
    .catch Ljava/lang/Throwable; {:try_start_68 .. :try_end_68} :catch_43

    .line 1765
    .end local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .local v126, "mediaRouter":Lcom/android/server/media/MediaRouterService;
    :try_start_69
    const-string v5, "media_router"

    move-object/from16 v0, v126

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_69
    .catch Ljava/lang/Throwable; {:try_start_69 .. :try_end_69} :catch_53

    move-object/from16 v125, v126

    .line 1770
    .end local v126    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .restart local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    :goto_47
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/trust/TrustManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1772
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/fingerprint/FingerprintService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1775
    :try_start_6a
    const-string v5, "SystemServer"

    const-string v6, "BackgroundDexOptService"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1776
    invoke-static {v4}, Lcom/android/server/pm/BackgroundDexOptService;->schedule(Landroid/content/Context;)V
    :try_end_6a
    .catch Ljava/lang/Throwable; {:try_start_6a .. :try_end_6a} :catch_44

    .line 1783
    :cond_34
    :goto_48
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/pm/LauncherAppsService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1812
    const-string/jumbo v5, "ro.bluetooth.wipower"

    const/4 v6, 0x0

    invoke-static {v5, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v111

    .line 1814
    .local v111, "isWipowerEnabled":Z
    if-eqz v111, :cond_4a

    .line 1816
    :try_start_6b
    const-string/jumbo v39, "wbc_service"

    .line 1817
    .local v39, "WBC_SERVICE_NAME":Ljava/lang/String;
    const-string v5, "SystemServer"

    const-string v6, "WipowerBatteryControl Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1819
    new-instance v176, Ldalvik/system/PathClassLoader;

    const-string v5, "/system/framework/com.quicinc.wbc.jar:/system/framework/com.quicinc.wbcservice.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    move-object/from16 v0, v176

    invoke-direct {v0, v5, v6}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 1822
    .local v176, "wbcClassLoader":Ldalvik/system/PathClassLoader;
    const-string v5, "com.quicinc.wbcservice.WbcService"

    move-object/from16 v0, v176

    invoke-virtual {v0, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v175

    .line 1823
    .local v175, "wbcClass":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v175

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v78

    .line 1824
    .local v78, "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/lang/Class;>;"
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v177

    .line 1825
    .local v177, "wbcObject":Ljava/lang/Object;
    const-string v5, "SystemServer"

    const-string v6, "Successfully loaded WbcService class"

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1826
    const-string/jumbo v5, "wbc_service"

    check-cast v177, Landroid/os/IBinder;

    .end local v177    # "wbcObject":Ljava/lang/Object;
    move-object/from16 v0, v177

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6b
    .catch Ljava/lang/Throwable; {:try_start_6b .. :try_end_6b} :catch_45

    .line 1834
    .end local v39    # "WBC_SERVICE_NAME":Ljava/lang/String;
    .end local v78    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/lang/Class;>;"
    .end local v175    # "wbcClass":Ljava/lang/Class;
    .end local v176    # "wbcClassLoader":Ldalvik/system/PathClassLoader;
    :goto_49
    if-eqz v82, :cond_35

    .line 1836
    :try_start_6c
    const-string v5, "SystemServer"

    const-string v6, "Digital Pen Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1837
    new-instance v84, Ldalvik/system/PathClassLoader;

    const-string v5, "/system/framework/DigitalPenService.jar"

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    move-object/from16 v0, v84

    invoke-direct {v0, v5, v6}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 1840
    .local v84, "digitalPenClassLoader":Ldalvik/system/PathClassLoader;
    const-string v5, "com.qti.snapdragon.digitalpen.DigitalPenService"

    move-object/from16 v0, v84

    invoke-virtual {v0, v5}, Ldalvik/system/PathClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v83

    .line 1842
    .local v83, "digitalPenClass":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v83

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v78

    .line 1843
    .restart local v78    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/lang/Class;>;"
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    move-object/from16 v0, v78

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v85

    .line 1844
    .local v85, "digitalPenRemoteObject":Ljava/lang/Object;
    const-string v5, "SystemServer"

    const-string v6, "Successfully loaded DigitalPenService class"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1845
    const-string v5, "DigitalPen"

    check-cast v85, Landroid/os/IBinder;

    .end local v85    # "digitalPenRemoteObject":Ljava/lang/Object;
    move-object/from16 v0, v85

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6c
    .catch Ljava/lang/Throwable; {:try_start_6c .. :try_end_6c} :catch_46

    .line 1854
    .end local v78    # "ctor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<Ljava/lang/Class;>;"
    .end local v83    # "digitalPenClass":Ljava/lang/Class;
    .end local v84    # "digitalPenClassLoader":Ldalvik/system/PathClassLoader;
    :cond_35
    :goto_4a
    :try_start_6d
    const-string v5, "SystemServer"

    const-string v6, "MiniModeAppManager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1856
    sget-object v5, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-static {v5}, Ldalvik/system/VMRuntime;->getInstructionSet(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v108

    .line 1857
    .local v108, "instructionSet":Ljava/lang/String;
    new-instance v55, Ldalvik/system/DexClassLoader;

    const-string v5, "/system/framework/minimode.jar"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/data/dalvik-cache/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v108

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, "/"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v9, 0x0

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v10

    move-object/from16 v0, v55

    invoke-direct {v0, v5, v6, v9, v10}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    .line 1859
    .restart local v55    # "cl":Ljava/lang/ClassLoader;
    const-string v5, "com.sec.minimode.manager.MiniModeAppManagerService"

    move-object/from16 v0, v55

    invoke-virtual {v0, v5}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v36

    .line 1860
    .local v36, "MiniModeAppManagerServiceClass":Ljava/lang/Class;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v36

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v37

    .line 1861
    .local v37, "MiniModeAppManagerServiceContructor":Ljava/lang/reflect/Constructor;
    const-string v6, "mini_mode_app_manager"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v5, v9

    move-object/from16 v0, v37

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    invoke-static {v6, v5}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6d
    .catch Ljava/lang/Throwable; {:try_start_6d .. :try_end_6d} :catch_47

    .line 1870
    .end local v36    # "MiniModeAppManagerServiceClass":Ljava/lang/Class;
    .end local v37    # "MiniModeAppManagerServiceContructor":Ljava/lang/reflect/Constructor;
    .end local v55    # "cl":Ljava/lang/ClassLoader;
    .end local v108    # "instructionSet":Ljava/lang/String;
    :goto_4b
    :try_start_6e
    const-string v5, "SystemServer"

    const-string v6, "VoIPInterfaceManager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1871
    const-string/jumbo v5, "voip"

    new-instance v6, Lcom/android/server/VoIPInterfaceManager;

    invoke-direct {v6, v4}, Lcom/android/server/VoIPInterfaceManager;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6e
    .catch Ljava/lang/Throwable; {:try_start_6e .. :try_end_6e} :catch_48

    .line 1878
    .end local v38    # "SecEDSEnable":Ljava/lang/String;
    .end local v111    # "isWipowerEnabled":Z
    :goto_4c
    if-nez v91, :cond_36

    .line 1879
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/media/projection/MediaProjectionManagerService;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    .line 1883
    :cond_36
    invoke-static {}, Lcom/samsung/appdisabler/AppDisablerService;->configurationFileExists()Z

    move-result v5

    if-eqz v5, :cond_37

    .line 1885
    :try_start_6f
    new-instance v43, Lcom/samsung/appdisabler/AppDisablerService;

    move-object/from16 v0, v43

    invoke-direct {v0, v4}, Lcom/samsung/appdisabler/AppDisablerService;-><init>(Landroid/content/Context;)V

    .line 1886
    .local v43, "appDisablerService":Lcom/samsung/appdisabler/AppDisablerService;
    const-string v5, "SamsungAppDisabler"

    move-object/from16 v0, v43

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_6f
    .catch Ljava/lang/Throwable; {:try_start_6f .. :try_end_6f} :catch_49

    .line 1894
    .end local v43    # "appDisablerService":Lcom/samsung/appdisabler/AppDisablerService;
    :cond_37
    :goto_4d
    invoke-virtual/range {v179 .. v179}, Lcom/android/server/wm/WindowManagerService;->detectSafeMode()Z

    move-result v35

    .line 1895
    .local v35, "safeMode":Z
    if-eqz v35, :cond_4b

    .line 1896
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->enterSafeMode()V

    .line 1898
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v5

    invoke-virtual {v5}, Ldalvik/system/VMRuntime;->disableJitCompilation()V

    .line 1905
    :goto_4e
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-class v6, Lcom/android/server/MmsServiceBroker;

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/Class;)Lcom/android/server/SystemService;

    move-result-object v128

    .end local v128    # "mmsService":Lcom/android/server/MmsServiceBroker;
    check-cast v128, Lcom/android/server/MmsServiceBroker;

    .line 1910
    .restart local v128    # "mmsService":Lcom/android/server/MmsServiceBroker;
    :try_start_70
    invoke-virtual/range {v168 .. v168}, Lcom/android/server/VibratorService;->systemReady()V
    :try_end_70
    .catch Ljava/lang/Throwable; {:try_start_70 .. :try_end_70} :catch_4a

    .line 1915
    :goto_4f
    if-eqz v115, :cond_38

    .line 1917
    :try_start_71
    invoke-virtual/range {v115 .. v115}, Lcom/android/server/LockSettingsService;->systemReady()V
    :try_end_71
    .catch Ljava/lang/Throwable; {:try_start_71 .. :try_end_71} :catch_4b

    .line 1924
    :cond_38
    :goto_50
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const/16 v6, 0x1e0

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startBootPhase(I)V

    .line 1926
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const/16 v6, 0x1f4

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startBootPhase(I)V

    .line 1929
    if-eqz v99, :cond_39

    .line 1930
    invoke-virtual/range {v99 .. v99}, Lcom/android/server/enterprise/EnterpriseDeviceManagerService;->systemReady()V

    .line 1931
    const-string v5, "SystemServer"

    const-string v6, "enterprisePolicy systemReady"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1936
    :cond_39
    :try_start_72
    invoke-virtual/range {v179 .. v179}, Lcom/android/server/wm/WindowManagerService;->systemReady()V
    :try_end_72
    .catch Ljava/lang/Throwable; {:try_start_72 .. :try_end_72} :catch_4c

    .line 1941
    :goto_51
    if-eqz v35, :cond_3a

    .line 1942
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v5}, Lcom/android/server/am/ActivityManagerService;->showSafeModeOverlay()V

    .line 1948
    :cond_3a
    invoke-virtual/range {v179 .. v179}, Lcom/android/server/wm/WindowManagerService;->computeNewConfiguration()Landroid/content/res/Configuration;

    move-result-object v63

    .line 1949
    .local v63, "config":Landroid/content/res/Configuration;
    new-instance v127, Landroid/util/DisplayMetrics;

    invoke-direct/range {v127 .. v127}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1950
    .local v127, "metrics":Landroid/util/DisplayMetrics;
    const-string/jumbo v5, "window"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v171

    check-cast v171, Landroid/view/WindowManager;

    .line 1951
    .local v171, "w":Landroid/view/WindowManager;
    invoke-interface/range {v171 .. v171}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    move-object/from16 v0, v127

    invoke-virtual {v5, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1952
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move-object/from16 v0, v63

    move-object/from16 v1, v127

    invoke-virtual {v5, v0, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    .line 1956
    :try_start_73
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPowerManagerService:Lcom/android/server/power/PowerManagerService;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    invoke-virtual {v6}, Lcom/android/server/am/ActivityManagerService;->getAppOpsService()Lcom/android/internal/app/IAppOpsService;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/android/server/power/PowerManagerService;->systemReady(Lcom/android/internal/app/IAppOpsService;)V
    :try_end_73
    .catch Ljava/lang/Throwable; {:try_start_73 .. :try_end_73} :catch_4d

    .line 1963
    :goto_52
    :try_start_74
    const-string v5, "SystemServer"

    const-string v6, "LightsService systemReady"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1964
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mLightsService:Lcom/android/server/lights/LightsService;

    invoke-virtual {v5}, Lcom/android/server/lights/LightsService;->systemReady()V
    :try_end_74
    .catch Ljava/lang/Throwable; {:try_start_74 .. :try_end_74} :catch_4e

    .line 1970
    :goto_53
    :try_start_75
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mPackageManagerService:Lcom/android/server/pm/PackageManagerService;

    invoke-virtual {v5}, Lcom/android/server/pm/PackageManagerService;->systemReady()V
    :try_end_75
    .catch Ljava/lang/Throwable; {:try_start_75 .. :try_end_75} :catch_4f

    .line 1977
    :goto_54
    :try_start_76
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mDisplayManagerService:Lcom/android/server/display/DisplayManagerService;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/android/server/SystemServer;->mOnlyCore:Z

    move/from16 v0, v35

    invoke-virtual {v5, v0, v6}, Lcom/android/server/display/DisplayManagerService;->systemReady(ZZ)V
    :try_end_76
    .catch Ljava/lang/Throwable; {:try_start_76 .. :try_end_76} :catch_50

    .line 1983
    :goto_55
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v167

    .line 1984
    .restart local v167    # "versionInfo":Landroid/os/Bundle;
    const-string v5, "2.0"

    const-string/jumbo v6, "version"

    move-object/from16 v0, v167

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3b

    .line 1986
    :try_start_77
    invoke-virtual/range {v119 .. v119}, Lcom/android/server/pm/PersonaManagerService;->systemReady()V
    :try_end_77
    .catch Ljava/lang/Throwable; {:try_start_77 .. :try_end_77} :catch_51

    .line 1993
    :cond_3b
    :goto_56
    :try_start_78
    invoke-virtual/range {v146 .. v146}, Lcom/android/server/SdpManagerService;->systemReady()V
    :try_end_78
    .catch Ljava/lang/Throwable; {:try_start_78 .. :try_end_78} :catch_52

    .line 1999
    :goto_57
    move-object/from16 v12, v131

    .line 2000
    .local v12, "mountServiceF":Lcom/android/server/MountService;
    move-object v14, v8

    .line 2001
    .local v14, "networkManagementF":Lcom/android/server/NetworkManagementService;
    move-object v15, v7

    .line 2002
    .local v15, "networkStatsF":Lcom/android/server/net/NetworkStatsService;
    move-object/from16 v16, v3

    .line 2003
    .local v16, "networkPolicyF":Lcom/android/server/net/NetworkPolicyManagerService;
    move-object/from16 v17, v64

    .line 2004
    .local v17, "connectivityF":Lcom/android/server/ConnectivityService;
    move-object/from16 v13, v134

    .line 2005
    .local v13, "networkScoreF":Lcom/android/server/NetworkScoreService;
    move-object/from16 v19, v172

    .line 2006
    .local v19, "wallpaperF":Lcom/android/server/wallpaper/WallpaperManagerService;
    move-object/from16 v20, v104

    .line 2007
    .local v20, "immF":Lcom/android/server/InputMethodManagerService;
    move-object/from16 v22, v113

    .line 2008
    .local v22, "locationF":Lcom/android/server/LocationManagerService;
    move-object/from16 v23, v144

    .line 2009
    .local v23, "sLocationF":Landroid/os/IBinder;
    move-object/from16 v24, v73

    .line 2010
    .local v24, "countryDetectorF":Lcom/android/server/CountryDetectorService;
    move-object/from16 v25, v137

    .line 2011
    .local v25, "networkTimeUpdaterF":Lcom/android/server/NetworkTimeUpdateService;
    move-object/from16 v26, v61

    .line 2012
    .local v26, "commonTimeMgmtServiceF":Lcom/android/server/CommonTimeManagementService;
    move-object/from16 v27, v164

    .line 2013
    .local v27, "textServiceManagerServiceF":Lcom/android/server/TextServicesManagerService;
    move-object/from16 v21, v152

    .line 2015
    .local v21, "statusBarF":Lcom/android/server/statusbar/StatusBarManagerService;
    move-object/from16 v28, v170

    .line 2017
    .local v28, "vrManagerF":Ljava/lang/Object;
    move-object/from16 v29, v45

    .line 2018
    .local v29, "atlasF":Lcom/android/server/AssetAtlasService;
    move-object/from16 v30, v106

    .line 2019
    .local v30, "inputManagerF":Lcom/android/server/input/InputManagerService;
    move-object/from16 v31, v155

    .line 2020
    .local v31, "telephonyRegistryF":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v32, v125

    .line 2021
    .local v32, "mediaRouterF":Lcom/android/server/media/MediaRouterService;
    move-object/from16 v18, v47

    .line 2022
    .local v18, "audioServiceF":Landroid/media/AudioService;
    move-object/from16 v129, v128

    .line 2024
    .local v129, "mmsServiceF":Lcom/android/server/MmsServiceBroker;
    move-object/from16 v33, v75

    .line 2028
    .local v33, "coverServiceF":Lcom/android/server/cover/CoverManagerService;
    move-object/from16 v34, v60

    .line 2036
    .local v34, "cocktailBarF":Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mActivityManagerService:Lcom/android/server/am/ActivityManagerService;

    new-instance v9, Lcom/android/server/SystemServer$2;

    move-object/from16 v10, p0

    move-object v11, v4

    invoke-direct/range {v9 .. v35}, Lcom/android/server/SystemServer$2;-><init>(Lcom/android/server/SystemServer;Landroid/content/Context;Lcom/android/server/MountService;Lcom/android/server/NetworkScoreService;Lcom/android/server/NetworkManagementService;Lcom/android/server/net/NetworkStatsService;Lcom/android/server/net/NetworkPolicyManagerService;Lcom/android/server/ConnectivityService;Landroid/media/AudioService;Lcom/android/server/wallpaper/WallpaperManagerService;Lcom/android/server/InputMethodManagerService;Lcom/android/server/statusbar/StatusBarManagerService;Lcom/android/server/LocationManagerService;Landroid/os/IBinder;Lcom/android/server/CountryDetectorService;Lcom/android/server/NetworkTimeUpdateService;Lcom/android/server/CommonTimeManagementService;Lcom/android/server/TextServicesManagerService;Lcom/android/server/VRManagerService;Lcom/android/server/AssetAtlasService;Lcom/android/server/input/InputManagerService;Lcom/android/server/TelephonyRegistry;Lcom/android/server/media/MediaRouterService;Lcom/android/server/cover/CoverManagerService;Lcom/android/server/cocktailbar/CocktailBarManagerService;Z)V

    invoke-virtual {v5, v9}, Lcom/android/server/am/ActivityManagerService;->systemReady(Ljava/lang/Runnable;)V

    .line 2214
    return-void

    .line 629
    .end local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v12    # "mountServiceF":Lcom/android/server/MountService;
    .end local v13    # "networkScoreF":Lcom/android/server/NetworkScoreService;
    .end local v14    # "networkManagementF":Lcom/android/server/NetworkManagementService;
    .end local v15    # "networkStatsF":Lcom/android/server/net/NetworkStatsService;
    .end local v16    # "networkPolicyF":Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v17    # "connectivityF":Lcom/android/server/ConnectivityService;
    .end local v18    # "audioServiceF":Landroid/media/AudioService;
    .end local v19    # "wallpaperF":Lcom/android/server/wallpaper/WallpaperManagerService;
    .end local v20    # "immF":Lcom/android/server/InputMethodManagerService;
    .end local v21    # "statusBarF":Lcom/android/server/statusbar/StatusBarManagerService;
    .end local v22    # "locationF":Lcom/android/server/LocationManagerService;
    .end local v23    # "sLocationF":Landroid/os/IBinder;
    .end local v24    # "countryDetectorF":Lcom/android/server/CountryDetectorService;
    .end local v25    # "networkTimeUpdaterF":Lcom/android/server/NetworkTimeUpdateService;
    .end local v26    # "commonTimeMgmtServiceF":Lcom/android/server/CommonTimeManagementService;
    .end local v27    # "textServiceManagerServiceF":Lcom/android/server/TextServicesManagerService;
    .end local v28    # "vrManagerF":Ljava/lang/Object;
    .end local v29    # "atlasF":Lcom/android/server/AssetAtlasService;
    .end local v30    # "inputManagerF":Lcom/android/server/input/InputManagerService;
    .end local v31    # "telephonyRegistryF":Lcom/android/server/TelephonyRegistry;
    .end local v32    # "mediaRouterF":Lcom/android/server/media/MediaRouterService;
    .end local v33    # "coverServiceF":Lcom/android/server/cover/CoverManagerService;
    .end local v34    # "cocktailBarF":Ljava/lang/Object;
    .end local v35    # "safeMode":Z
    .end local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .end local v60    # "cocktailBar":Lcom/android/server/cocktailbar/CocktailBarManagerService;
    .end local v63    # "config":Landroid/content/res/Configuration;
    .end local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .end local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .end local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .end local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .end local v113    # "location":Lcom/android/server/LocationManagerService;
    .end local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .end local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .end local v127    # "metrics":Landroid/util/DisplayMetrics;
    .end local v129    # "mmsServiceF":Lcom/android/server/MmsServiceBroker;
    .end local v139    # "notification":Landroid/app/INotificationManager;
    .end local v144    # "sLocation":Landroid/os/IBinder;
    .end local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .end local v167    # "versionInfo":Landroid/os/Bundle;
    .end local v170    # "vrManager":Lcom/android/server/VRManagerService;
    .end local v171    # "w":Landroid/view/WindowManager;
    .end local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    .restart local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    :catch_0
    move-exception v95

    .line 630
    .local v95, "e":Ljava/lang/Throwable;
    :try_start_79
    const-string v5, "SystemServer"

    const-string v6, "Failure starting SE Android Manager Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_79
    .catch Ljava/lang/RuntimeException; {:try_start_79 .. :try_end_79} :catch_1

    goto/16 :goto_0

    .line 926
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1
    move-exception v95

    move-object/from16 v155, v156

    .line 927
    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .local v95, "e":Ljava/lang/RuntimeException;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    :goto_58
    const-string v5, "System"

    const-string v6, "******************************************"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 928
    const-string v5, "System"

    const-string v6, "************ Failure starting core service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_13

    .line 643
    .end local v95    # "e":Ljava/lang/RuntimeException;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v167    # "versionInfo":Landroid/os/Bundle;
    :catch_2
    move-exception v95

    .line 644
    .local v95, "e":Ljava/lang/Throwable;
    :try_start_7a
    const-string v5, "SystemServer"

    const-string v6, "Failure starting Persona Manager Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 657
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_3
    move-exception v95

    .line 658
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_59
    const-string v5, "SystemServer"

    const-string v6, "Failure starting Account Manager"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 668
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v120    # "mProductName":Ljava/lang/String;
    :catch_4
    move-exception v95

    .line 669
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting KT UCA Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7a
    .catch Ljava/lang/RuntimeException; {:try_start_7a .. :try_end_7a} :catch_1

    goto/16 :goto_3

    .line 675
    .end local v95    # "e":Ljava/lang/Throwable;
    :cond_3c
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 685
    .end local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    .restart local v80    # "dEncService":Lcom/android/server/DirEncryptService;
    :catch_5
    move-exception v95

    .line 686
    .restart local v95    # "e":Ljava/lang/Throwable;
    :try_start_7b
    const-string v5, "SystemServer"

    const-string v6, "Failure starting DirEncryptService"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7b
    .catch Ljava/lang/RuntimeException; {:try_start_7b .. :try_end_7b} :catch_6c

    .line 687
    const/16 v79, 0x0

    .end local v80    # "dEncService":Lcom/android/server/DirEncryptService;
    .restart local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    goto/16 :goto_5

    .line 695
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_6
    move-exception v95

    .line 697
    .restart local v95    # "e":Ljava/lang/Throwable;
    :try_start_7c
    const-string v5, "SystemServer"

    const-string v6, "Failed to add Reactive Service."

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_6

    .line 706
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_7
    move-exception v95

    .line 707
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Registration of denial service failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7c
    .catch Ljava/lang/RuntimeException; {:try_start_7c .. :try_end_7c} :catch_1

    goto/16 :goto_7

    .line 732
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v157    # "timaEnabled":Z
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_8
    move-exception v95

    .line 733
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_5a
    :try_start_7d
    const-string/jumbo v5, "starting TimaService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_8

    .line 926
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v157    # "timaEnabled":Z
    :catch_9
    move-exception v95

    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto :goto_58

    .line 743
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v157    # "timaEnabled":Z
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_a
    move-exception v95

    .line 744
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting TimaObserver"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    .line 756
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_b
    move-exception v95

    .line 757
    .local v95, "e":Ljava/lang/Exception;
    const-string v5, "SystemServer"

    const-string v6, "Unable to add TimaKesytore provider"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 758
    invoke-virtual/range {v95 .. v95}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_a

    .line 771
    .end local v95    # "e":Ljava/lang/Exception;
    .restart local v97    # "enabledCEP":Z
    .restart local v98    # "enabledMDM":Z
    :catch_c
    move-exception v95

    .line 772
    .local v95, "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting CEP Proxy Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_b

    .line 775
    .end local v95    # "e":Ljava/lang/Throwable;
    :cond_3d
    const-string v5, "SystemServer"

    const-string v6, "MDM is enabled, but CEP is not enabled"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_b

    .line 787
    .end local v97    # "enabledCEP":Z
    :catch_d
    move-exception v95

    .line 788
    .local v95, "e":Ljava/lang/Exception;
    const-string v5, "SystemServer"

    const-string/jumbo v6, "ssrm.jar not found"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    invoke-virtual/range {v95 .. v95}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_c

    .line 807
    .end local v95    # "e":Ljava/lang/Exception;
    :catch_e
    move-exception v95

    .line 808
    .restart local v95    # "e":Ljava/lang/Exception;
    const-string v5, "Fail to start SmartFaceService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7d
    .catch Ljava/lang/RuntimeException; {:try_start_7d .. :try_end_7d} :catch_9

    goto/16 :goto_d

    .line 846
    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v95    # "e":Ljava/lang/Exception;
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v174    # "watchdog":Lcom/android/server/Watchdog;
    :catch_f
    move-exception v95

    .line 847
    .local v95, "e":Ljava/lang/Throwable;
    :try_start_7e
    const-string v5, "SystemServer"

    const-string v6, "Failure starting BarBeam Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_e

    .line 926
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_10
    move-exception v95

    move-object/from16 v70, v71

    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v106, v107

    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto/16 :goto_58

    .line 862
    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :cond_3e
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_f

    :cond_3f
    const/4 v5, 0x0

    goto/16 :goto_10

    .line 894
    .restart local v154    # "tactileAssist":Lcom/android/server/TactileAssistService;
    :cond_40
    move-object/from16 v0, p0

    iget v5, v0, Lcom/android/server/SystemServer;->mFactoryTestMode:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_41

    .line 895
    const-string v5, "SystemServer"

    const-string v6, "No Bluetooth Service (factory test)"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_11

    .line 896
    :cond_41
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const-string v6, "android.hardware.bluetooth"

    invoke-virtual {v5, v6}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_42

    .line 898
    const-string v5, "SystemServer"

    const-string v6, "No Bluetooth Service (Bluetooth Hardware Not Present)"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_11

    .line 899
    :cond_42
    if-eqz v87, :cond_43

    .line 900
    const-string v5, "SystemServer"

    const-string v6, "Bluetooth Service disabled by config"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_11

    .line 902
    :cond_43
    const-string v5, "SystemServer"

    const-string v6, "Bluetooth Manager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 903
    new-instance v51, Lcom/android/server/BluetoothManagerService;

    move-object/from16 v0, v51

    invoke-direct {v0, v4}, Lcom/android/server/BluetoothManagerService;-><init>(Landroid/content/Context;)V
    :try_end_7e
    .catch Ljava/lang/RuntimeException; {:try_start_7e .. :try_end_7e} :catch_10

    .line 904
    .end local v50    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    .local v51, "bluetooth":Lcom/android/server/BluetoothManagerService;
    :try_start_7f
    const-string v5, "bluetooth_manager"

    move-object/from16 v0, v51

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V

    .line 907
    const-string v5, "SystemServer"

    const-string v6, "Bluetooth Secure Mode Manager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 908
    const-string v5, "bluetooth_secure_mode_manager"

    new-instance v6, Landroid/app/BluetoothSecureManagerService;

    invoke-direct {v6, v4}, Landroid/app/BluetoothSecureManagerService;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v6}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_7f
    .catch Ljava/lang/RuntimeException; {:try_start_7f .. :try_end_7f} :catch_6f

    move-object/from16 v50, v51

    .end local v51    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    .restart local v50    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    goto/16 :goto_11

    .line 918
    :catch_11
    move-exception v95

    .line 919
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_5b
    :try_start_80
    const-string v5, "SystemServer"

    const-string v6, "Failure starting RCP Manager Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_80
    .catch Ljava/lang/RuntimeException; {:try_start_80 .. :try_end_80} :catch_10

    goto/16 :goto_12

    .line 962
    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v98    # "enabledMDM":Z
    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v120    # "mProductName":Ljava/lang/String;
    .end local v154    # "tactileAssist":Lcom/android/server/TactileAssistService;
    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v157    # "timaEnabled":Z
    .end local v167    # "versionInfo":Landroid/os/Bundle;
    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .end local v174    # "watchdog":Lcom/android/server/Watchdog;
    .restart local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .restart local v60    # "cocktailBar":Lcom/android/server/cocktailbar/CocktailBarManagerService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .restart local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .restart local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v113    # "location":Lcom/android/server/LocationManagerService;
    .restart local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .restart local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .restart local v139    # "notification":Landroid/app/INotificationManager;
    .restart local v144    # "sLocation":Landroid/os/IBinder;
    .restart local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v170    # "vrManager":Lcom/android/server/VRManagerService;
    .restart local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :catch_12
    move-exception v95

    .line 963
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_5c
    const-string/jumbo v5, "starting Input Manager Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_14

    .line 970
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_13
    move-exception v95

    .line 971
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting Accessibility Manager"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_15

    .line 978
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_14
    move-exception v95

    .line 979
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making display ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_16

    .line 984
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_15
    move-exception v95

    .line 985
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "performing boot dexopt"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_17

    .line 1009
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_16
    move-exception v95

    .line 1010
    .local v95, "e":Ljava/lang/Exception;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting MotionRecognitionService"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_19

    .line 1022
    .end local v95    # "e":Ljava/lang/Exception;
    .restart local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v77    # "cryptState":Ljava/lang/String;
    :cond_44
    if-nez v91, :cond_d

    .line 1023
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v141

    .line 1024
    .local v141, "packageMgr":Landroid/content/pm/PackageManager;
    const-string v5, "com.sec.feature.cover.flip"

    move-object/from16 v0, v141

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_45

    const-string v5, "com.sec.feature.cover.sview"

    move-object/from16 v0, v141

    invoke-virtual {v0, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1027
    :cond_45
    :try_start_81
    const-string v5, "SystemServer"

    const-string v6, "CoverManager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1028
    new-instance v76, Lcom/android/server/cover/CoverManagerService;

    move-object/from16 v0, v76

    move-object/from16 v1, v179

    move-object/from16 v2, v106

    invoke-direct {v0, v4, v1, v2}, Lcom/android/server/cover/CoverManagerService;-><init>(Landroid/content/Context;Lcom/android/server/wm/WindowManagerService;Lcom/android/server/input/InputManagerService;)V
    :try_end_81
    .catch Ljava/lang/Throwable; {:try_start_81 .. :try_end_81} :catch_17

    .line 1029
    .end local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .local v76, "coverService":Lcom/android/server/cover/CoverManagerService;
    :try_start_82
    const-string v5, "cover"

    move-object/from16 v0, v76

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_82
    .catch Ljava/lang/Throwable; {:try_start_82 .. :try_end_82} :catch_67

    move-object/from16 v75, v76

    .line 1032
    .end local v76    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    goto/16 :goto_1a

    .line 1030
    :catch_17
    move-exception v95

    .line 1031
    .local v95, "e":Ljava/lang/Throwable;
    :goto_5d
    const-string v5, "SystemServer"

    const-string v6, "Failure starting CoverManager Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1a

    .line 1046
    .end local v77    # "cryptState":Ljava/lang/String;
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v141    # "packageMgr":Landroid/content/pm/PackageManager;
    :catch_18
    move-exception v95

    .line 1047
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "Failure starting FM Radio Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1b

    .line 1062
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_19
    move-exception v95

    .line 1063
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_5e
    const-string/jumbo v5, "starting Mount Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1c

    .line 1074
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1a
    move-exception v95

    .line 1075
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting DirEncryption service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1d

    .line 1096
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1b
    move-exception v95

    .line 1097
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_5f
    const-string/jumbo v5, "starting LockSettingsService service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1e

    .line 1115
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1c
    move-exception v95

    .line 1116
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_60
    const-string v5, "SystemServer"

    const-string v6, "Failure starting Harmony EAS service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1f

    .line 1125
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1d
    move-exception v95

    .line 1126
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_61
    const-string/jumbo v5, "unable to start SdpManagerService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_20

    .line 1137
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1e
    move-exception v95

    .line 1138
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_62
    const-string v5, "SystemServer"

    const-string v6, "Failure starting EnterpriseDeviceManagerService"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_21

    .line 1148
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_1f
    move-exception v95

    .line 1149
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_63
    const-string/jumbo v5, "starting StatusBarManagerService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_22

    .line 1158
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_20
    move-exception v95

    .line 1159
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting Clipboard Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_23

    .line 1169
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_21
    move-exception v95

    .line 1170
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting ClipboardEx Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_24

    .line 1180
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_22
    move-exception v95

    .line 1181
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting NetworkManagement Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_25

    .line 1193
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_23
    move-exception v95

    .line 1194
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting Absolute Persistence Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_26

    .line 1203
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_24
    move-exception v95

    .line 1204
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_64
    const-string/jumbo v5, "starting Text Service Manager Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_27

    .line 1213
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_25
    move-exception v95

    .line 1214
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_65
    const-string/jumbo v5, "starting Network Score Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_28

    .line 1221
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_26
    move-exception v95

    .line 1222
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_66
    const-string/jumbo v5, "starting NetworkStats Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_29

    .line 1232
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_27
    move-exception v95

    move-object/from16 v3, v133

    .line 1233
    .end local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_67
    const-string/jumbo v5, "starting NetworkPolicy Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2a

    .line 1247
    .end local v95    # "e":Ljava/lang/Throwable;
    :cond_46
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/server/SystemServer;->mSystemServiceManager:Lcom/android/server/SystemServiceManager;

    const-string v6, "com.android.server.ethernet.EthernetService"

    invoke-virtual {v5, v6}, Lcom/android/server/SystemServiceManager;->startService(Ljava/lang/String;)Lcom/android/server/SystemService;

    goto/16 :goto_2b

    .line 1258
    :catch_28
    move-exception v95

    .line 1259
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_68
    const-string/jumbo v5, "starting Connectivity Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2c

    .line 1273
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_29
    move-exception v95

    .line 1274
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting SmartBondingService Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2d

    .line 1351
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2a
    move-exception v95

    .line 1352
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting Service Discovery Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2e

    .line 1357
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2b
    move-exception v95

    .line 1358
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting DpmService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2f

    .line 1367
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2c
    move-exception v95

    .line 1368
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting UpdateLockService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_30

    .line 1386
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2d
    move-exception v95

    .line 1387
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Account Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_31

    .line 1393
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2e
    move-exception v95

    .line 1394
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Content Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_32

    .line 1423
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_2f
    move-exception v95

    .line 1424
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_69
    const-string/jumbo v5, "starting Location Manager"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_33

    .line 1431
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_30
    move-exception v95

    .line 1432
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6a
    const-string/jumbo v5, "starting Country Detector"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_34

    .line 1471
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_31
    move-exception v95

    .line 1472
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Loading SLocation has been failed, error or not support"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {v95 .. v95}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_35

    .line 1480
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_32
    move-exception v95

    .line 1481
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting Search Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_36

    .line 1489
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_33
    move-exception v95

    .line 1490
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting DropBoxManagerService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_37

    .line 1499
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_34
    move-exception v95

    .line 1500
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6b
    const-string/jumbo v5, "starting Wallpaper Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_38

    .line 1509
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_35
    move-exception v95

    .line 1510
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6c
    const-string/jumbo v5, "starting Audio Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_39

    .line 1524
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_36
    move-exception v95

    .line 1525
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting WiredAccessoryManager"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3a

    .line 1542
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_37
    move-exception v95

    .line 1543
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6d
    const-string v5, "SystemServer"

    const-string v6, "Failure starting SerialService"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3b

    .line 1554
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_38
    move-exception v95

    .line 1555
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "Failue staring KiesUsbObserver Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3c

    .line 1593
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v38    # "SecEDSEnable":Ljava/lang/String;
    .restart local v96    # "edsclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :cond_47
    :try_start_83
    const-string v5, "SystemServer"

    const-string v6, "edsclass Service exist"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1594
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/Class;

    move-object/from16 v44, v0

    .line 1595
    .local v44, "arg":[Ljava/lang/Class;
    const/4 v5, 0x0

    const-class v6, Landroid/content/Context;

    aput-object v6, v44, v5

    .line 1596
    move-object/from16 v0, v96

    move-object/from16 v1, v44

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v66

    .line 1597
    .local v66, "constructor":Ljava/lang/reflect/Constructor;
    const-string v6, "SecExternalDisplayService"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v4, v5, v9

    move-object/from16 v0, v66

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/IBinder;

    invoke-static {v6, v5}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_83
    .catch Ljava/lang/Throwable; {:try_start_83 .. :try_end_83} :catch_39

    goto/16 :goto_3d

    .line 1599
    .end local v44    # "arg":[Ljava/lang/Class;
    .end local v66    # "constructor":Ljava/lang/reflect/Constructor;
    .end local v96    # "edsclass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    :catch_39
    move-exception v95

    .line 1600
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting eds Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3d

    .line 1607
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_3a
    move-exception v95

    .line 1608
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting DiskStats Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3e

    .line 1629
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v123    # "mdnieClass":Ljava/lang/Class;
    :cond_48
    const/4 v5, 0x1

    :try_start_84
    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v5, v6

    move-object/from16 v0, v123

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v66

    .line 1630
    .restart local v66    # "constructor":Ljava/lang/reflect/Constructor;
    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v4, v5, v6

    move-object/from16 v0, v66

    invoke-virtual {v0, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v124

    check-cast v124, Landroid/os/IBinder;

    .line 1631
    .local v124, "mdnieService":Landroid/os/IBinder;
    const-string v5, "mDNIe"

    move-object/from16 v0, v124

    invoke-static {v5, v0}, Landroid/os/ServiceManager;->addService(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_84
    .catch Ljava/lang/Throwable; {:try_start_84 .. :try_end_84} :catch_3b

    goto/16 :goto_3f

    .line 1633
    .end local v66    # "constructor":Ljava/lang/reflect/Constructor;
    .end local v123    # "mdnieClass":Ljava/lang/Class;
    .end local v124    # "mdnieService":Landroid/os/IBinder;
    :catch_3b
    move-exception v95

    .line 1634
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "Failed To Start Mdnie Service "

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3f

    .line 1662
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v140    # "pM":Landroid/content/pm/PackageManager;
    :cond_49
    :try_start_85
    const-string v5, "SystemServer"

    const-string v6, "Failed to start SpenGestureManagerService : PackageManager is null!!"

    invoke-static {v5, v6}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_85
    .catch Ljava/lang/Throwable; {:try_start_85 .. :try_end_85} :catch_3c

    goto/16 :goto_40

    .line 1664
    .end local v140    # "pM":Landroid/content/pm/PackageManager;
    :catch_3c
    move-exception v95

    .line 1665
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting SpenGestureManagerService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_40

    .line 1686
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_3d
    move-exception v95

    .line 1687
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6e
    const-string/jumbo v5, "starting QuickConnect Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_41

    .line 1698
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_3e
    move-exception v95

    .line 1699
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting SamplingProfiler Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_42

    .line 1706
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_3f
    move-exception v95

    .line 1707
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting NetworkTimeUpdate service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_43

    .line 1716
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_40
    move-exception v95

    .line 1717
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_6f
    const-string/jumbo v5, "starting CommonTimeManagementService service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_44

    .line 1725
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_41
    move-exception v95

    .line 1726
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting CertBlacklister"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_45

    .line 1740
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_42
    move-exception v95

    .line 1741
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_70
    const-string/jumbo v5, "starting AssetAtlasService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_46

    .line 1766
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_43
    move-exception v95

    .line 1767
    .restart local v95    # "e":Ljava/lang/Throwable;
    :goto_71
    const-string/jumbo v5, "starting MediaRouterService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_47

    .line 1777
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_44
    move-exception v95

    .line 1778
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting BackgroundDexOptService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_48

    .line 1827
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v111    # "isWipowerEnabled":Z
    :catch_45
    move-exception v95

    .line 1828
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting WipowerBatteryControl Service"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_49

    .line 1831
    .end local v95    # "e":Ljava/lang/Throwable;
    :cond_4a
    const-string v5, "SystemServer"

    const-string v6, "Wipower not supported"

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_49

    .line 1846
    :catch_46
    move-exception v95

    .line 1847
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string/jumbo v5, "starting DigitalPenService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4a

    .line 1863
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_47
    move-exception v95

    .line 1864
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting MiniModeAppManager Service"

    move-object/from16 v0, v95

    invoke-static {v5, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_4b

    .line 1872
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_48
    move-exception v95

    .line 1873
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "SystemServer"

    const-string v6, "Failure starting VoIPInterfaceManager Service"

    invoke-static {v5, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4c

    .line 1887
    .end local v38    # "SecEDSEnable":Ljava/lang/String;
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v111    # "isWipowerEnabled":Z
    :catch_49
    move-exception v95

    .line 1888
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "Failure starting SamsungAppDisablerService"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4d

    .line 1901
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v35    # "safeMode":Z
    :cond_4b
    invoke-static {}, Ldalvik/system/VMRuntime;->getRuntime()Ldalvik/system/VMRuntime;

    move-result-object v5

    invoke-virtual {v5}, Ldalvik/system/VMRuntime;->startJitCompilation()V

    goto/16 :goto_4e

    .line 1911
    :catch_4a
    move-exception v95

    .line 1912
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Vibrator Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4f

    .line 1918
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_4b
    move-exception v95

    .line 1919
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Lock Settings Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_50

    .line 1937
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_4c
    move-exception v95

    .line 1938
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Window Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_51

    .line 1957
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v63    # "config":Landroid/content/res/Configuration;
    .restart local v127    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v171    # "w":Landroid/view/WindowManager;
    :catch_4d
    move-exception v95

    .line 1958
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Power Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_52

    .line 1965
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_4e
    move-exception v95

    .line 1966
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Lights Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_53

    .line 1971
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_4f
    move-exception v95

    .line 1972
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Package Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_54

    .line 1978
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_50
    move-exception v95

    .line 1979
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Display Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_55

    .line 1987
    .end local v95    # "e":Ljava/lang/Throwable;
    .restart local v167    # "versionInfo":Landroid/os/Bundle;
    :catch_51
    move-exception v95

    .line 1988
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Persona Manager Service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_56

    .line 1994
    .end local v95    # "e":Ljava/lang/Throwable;
    :catch_52
    move-exception v95

    .line 1995
    .restart local v95    # "e":Ljava/lang/Throwable;
    const-string v5, "making Sdp manager service ready"

    move-object/from16 v0, p0

    move-object/from16 v1, v95

    invoke-direct {v0, v5, v1}, Lcom/android/server/SystemServer;->reportWtf(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_57

    .line 1766
    .end local v35    # "safeMode":Z
    .end local v63    # "config":Landroid/content/res/Configuration;
    .end local v95    # "e":Ljava/lang/Throwable;
    .end local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .end local v127    # "metrics":Landroid/util/DisplayMetrics;
    .end local v167    # "versionInfo":Landroid/os/Bundle;
    .end local v171    # "w":Landroid/view/WindowManager;
    .restart local v38    # "SecEDSEnable":Ljava/lang/String;
    .restart local v126    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    :catch_53
    move-exception v95

    move-object/from16 v125, v126

    .end local v126    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .restart local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    goto/16 :goto_71

    .line 1740
    .end local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .restart local v46    # "atlas":Lcom/android/server/AssetAtlasService;
    :catch_54
    move-exception v95

    move-object/from16 v45, v46

    .end local v46    # "atlas":Lcom/android/server/AssetAtlasService;
    .restart local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    goto/16 :goto_70

    .line 1716
    .end local v61    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    .restart local v62    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    :catch_55
    move-exception v95

    move-object/from16 v61, v62

    .end local v62    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    .restart local v61    # "commonTimeMgmtService":Lcom/android/server/CommonTimeManagementService;
    goto/16 :goto_6f

    .line 1686
    .end local v142    # "quickconnect":Lcom/android/server/QuickConnectService;
    .restart local v143    # "quickconnect":Lcom/android/server/QuickConnectService;
    :catch_56
    move-exception v95

    move-object/from16 v142, v143

    .end local v143    # "quickconnect":Lcom/android/server/QuickConnectService;
    .restart local v142    # "quickconnect":Lcom/android/server/QuickConnectService;
    goto/16 :goto_6e

    .line 1542
    .end local v38    # "SecEDSEnable":Ljava/lang/String;
    .end local v148    # "serial":Lcom/android/server/SerialService;
    .restart local v149    # "serial":Lcom/android/server/SerialService;
    :catch_57
    move-exception v95

    move-object/from16 v148, v149

    .end local v149    # "serial":Lcom/android/server/SerialService;
    .restart local v148    # "serial":Lcom/android/server/SerialService;
    goto/16 :goto_6d

    .line 1509
    .end local v47    # "audioService":Landroid/media/AudioService;
    .restart local v48    # "audioService":Landroid/media/AudioService;
    :catch_58
    move-exception v95

    move-object/from16 v47, v48

    .end local v48    # "audioService":Landroid/media/AudioService;
    .restart local v47    # "audioService":Landroid/media/AudioService;
    goto/16 :goto_6c

    .line 1499
    .end local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    .restart local v173    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :catch_59
    move-exception v95

    move-object/from16 v172, v173

    .end local v173    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    .restart local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    goto/16 :goto_6b

    .line 1431
    .end local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .restart local v74    # "countryDetector":Lcom/android/server/CountryDetectorService;
    :catch_5a
    move-exception v95

    move-object/from16 v73, v74

    .end local v74    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .restart local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    goto/16 :goto_6a

    .line 1423
    .end local v113    # "location":Lcom/android/server/LocationManagerService;
    .restart local v114    # "location":Lcom/android/server/LocationManagerService;
    :catch_5b
    move-exception v95

    move-object/from16 v113, v114

    .end local v114    # "location":Lcom/android/server/LocationManagerService;
    .restart local v113    # "location":Lcom/android/server/LocationManagerService;
    goto/16 :goto_69

    .line 1258
    .end local v64    # "connectivity":Lcom/android/server/ConnectivityService;
    .restart local v65    # "connectivity":Lcom/android/server/ConnectivityService;
    :catch_5c
    move-exception v95

    move-object/from16 v64, v65

    .end local v65    # "connectivity":Lcom/android/server/ConnectivityService;
    .restart local v64    # "connectivity":Lcom/android/server/ConnectivityService;
    goto/16 :goto_68

    .line 1232
    :catch_5d
    move-exception v95

    goto/16 :goto_67

    .line 1221
    .end local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .end local v7    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    .restart local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v136    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    :catch_5e
    move-exception v95

    move-object/from16 v7, v136

    .end local v136    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    .restart local v7    # "networkStats":Lcom/android/server/net/NetworkStatsService;
    goto/16 :goto_66

    .line 1213
    .end local v134    # "networkScore":Lcom/android/server/NetworkScoreService;
    .restart local v135    # "networkScore":Lcom/android/server/NetworkScoreService;
    :catch_5f
    move-exception v95

    move-object/from16 v134, v135

    .end local v135    # "networkScore":Lcom/android/server/NetworkScoreService;
    .restart local v134    # "networkScore":Lcom/android/server/NetworkScoreService;
    goto/16 :goto_65

    .line 1203
    .end local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .restart local v165    # "tsms":Lcom/android/server/TextServicesManagerService;
    :catch_60
    move-exception v95

    move-object/from16 v164, v165

    .end local v165    # "tsms":Lcom/android/server/TextServicesManagerService;
    .restart local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    goto/16 :goto_64

    .line 1148
    .end local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .restart local v153    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    :catch_61
    move-exception v95

    move-object/from16 v152, v153

    .end local v153    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .restart local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    goto/16 :goto_63

    .line 1137
    .end local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .restart local v100    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    :catch_62
    move-exception v95

    move-object/from16 v99, v100

    .end local v100    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .restart local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    goto/16 :goto_62

    .line 1125
    .end local v146    # "sdpService":Lcom/android/server/SdpManagerService;
    .restart local v147    # "sdpService":Lcom/android/server/SdpManagerService;
    :catch_63
    move-exception v95

    move-object/from16 v146, v147

    .end local v147    # "sdpService":Lcom/android/server/SdpManagerService;
    .restart local v146    # "sdpService":Lcom/android/server/SdpManagerService;
    goto/16 :goto_61

    .line 1115
    .end local v117    # "mHMS":Lcom/android/server/HarmonyEASService;
    .restart local v118    # "mHMS":Lcom/android/server/HarmonyEASService;
    :catch_64
    move-exception v95

    move-object/from16 v117, v118

    .end local v118    # "mHMS":Lcom/android/server/HarmonyEASService;
    .restart local v117    # "mHMS":Lcom/android/server/HarmonyEASService;
    goto/16 :goto_60

    .line 1096
    .end local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .restart local v116    # "lockSettings":Lcom/android/server/LockSettingsService;
    :catch_65
    move-exception v95

    move-object/from16 v115, v116

    .end local v116    # "lockSettings":Lcom/android/server/LockSettingsService;
    .restart local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    goto/16 :goto_5f

    .line 1062
    .end local v131    # "mountService":Lcom/android/server/MountService;
    .restart local v132    # "mountService":Lcom/android/server/MountService;
    :catch_66
    move-exception v95

    move-object/from16 v131, v132

    .end local v132    # "mountService":Lcom/android/server/MountService;
    .restart local v131    # "mountService":Lcom/android/server/MountService;
    goto/16 :goto_5e

    .line 1030
    .end local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v76    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v77    # "cryptState":Ljava/lang/String;
    .restart local v141    # "packageMgr":Landroid/content/pm/PackageManager;
    :catch_67
    move-exception v95

    move-object/from16 v75, v76

    .end local v76    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    goto/16 :goto_5d

    .line 993
    .end local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .end local v77    # "cryptState":Ljava/lang/String;
    .end local v141    # "packageMgr":Landroid/content/pm/PackageManager;
    :catch_68
    move-exception v5

    goto/16 :goto_18

    .line 962
    .end local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .restart local v105    # "imm":Lcom/android/server/InputMethodManagerService;
    :catch_69
    move-exception v95

    move-object/from16 v104, v105

    .end local v105    # "imm":Lcom/android/server/InputMethodManagerService;
    .restart local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    goto/16 :goto_5c

    .line 926
    .end local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .end local v60    # "cocktailBar":Lcom/android/server/cocktailbar/CocktailBarManagerService;
    .end local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .end local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .end local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .end local v113    # "location":Lcom/android/server/LocationManagerService;
    .end local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .end local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .end local v139    # "notification":Landroid/app/INotificationManager;
    .end local v144    # "sLocation":Landroid/os/IBinder;
    .end local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .end local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .end local v170    # "vrManager":Lcom/android/server/VRManagerService;
    .end local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :catch_6a
    move-exception v95

    goto/16 :goto_58

    .end local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v41    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v167    # "versionInfo":Landroid/os/Bundle;
    :catch_6b
    move-exception v95

    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v40, v41

    .end local v41    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .restart local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    goto/16 :goto_58

    .end local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v80    # "dEncService":Lcom/android/server/DirEncryptService;
    .restart local v120    # "mProductName":Ljava/lang/String;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    :catch_6c
    move-exception v95

    move-object/from16 v79, v80

    .end local v80    # "dEncService":Lcom/android/server/DirEncryptService;
    .restart local v79    # "dEncService":Lcom/android/server/DirEncryptService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    goto/16 :goto_58

    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v160    # "timaService":Lcom/android/server/TimaService;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v157    # "timaEnabled":Z
    .restart local v161    # "timaService":Lcom/android/server/TimaService;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_6d
    move-exception v95

    move-object/from16 v160, v161

    .end local v161    # "timaService":Lcom/android/server/TimaService;
    .restart local v160    # "timaService":Lcom/android/server/TimaService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto/16 :goto_58

    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v98    # "enabledMDM":Z
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_6e
    move-exception v95

    move-object/from16 v70, v71

    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto/16 :goto_58

    .end local v50    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v51    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v154    # "tactileAssist":Lcom/android/server/TactileAssistService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v174    # "watchdog":Lcom/android/server/Watchdog;
    :catch_6f
    move-exception v95

    move-object/from16 v70, v71

    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v106, v107

    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    move-object/from16 v50, v51

    .end local v51    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    .restart local v50    # "bluetooth":Lcom/android/server/BluetoothManagerService;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto/16 :goto_58

    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v122    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_70
    move-exception v95

    move-object/from16 v121, v122

    .end local v122    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .restart local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    move-object/from16 v70, v71

    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    move-object/from16 v155, v156

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    move-object/from16 v106, v107

    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    move-object/from16 v168, v169

    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    goto/16 :goto_58

    .line 918
    .end local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .end local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v168    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v122    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .restart local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v169    # "vibrator":Lcom/android/server/VibratorService;
    :catch_71
    move-exception v95

    move-object/from16 v121, v122

    .end local v122    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    .restart local v121    # "mRCPManagerService":Lcom/android/server/RCPManagerService;
    goto/16 :goto_5b

    .line 732
    .end local v71    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .end local v98    # "enabledMDM":Z
    .end local v107    # "inputManager":Lcom/android/server/input/InputManagerService;
    .end local v154    # "tactileAssist":Lcom/android/server/TactileAssistService;
    .end local v160    # "timaService":Lcom/android/server/TimaService;
    .end local v174    # "watchdog":Lcom/android/server/Watchdog;
    .restart local v70    # "consumerIr":Lcom/android/server/ConsumerIrService;
    .restart local v106    # "inputManager":Lcom/android/server/input/InputManagerService;
    .restart local v161    # "timaService":Lcom/android/server/TimaService;
    :catch_72
    move-exception v95

    move-object/from16 v160, v161

    .end local v161    # "timaService":Lcom/android/server/TimaService;
    .restart local v160    # "timaService":Lcom/android/server/TimaService;
    goto/16 :goto_5a

    .line 657
    .end local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .end local v120    # "mProductName":Ljava/lang/String;
    .end local v157    # "timaEnabled":Z
    .end local v169    # "vibrator":Lcom/android/server/VibratorService;
    .restart local v41    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .restart local v168    # "vibrator":Lcom/android/server/VibratorService;
    :catch_73
    move-exception v95

    move-object/from16 v40, v41

    .end local v41    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    .restart local v40    # "accountManager":Lcom/android/server/accounts/AccountManagerService;
    goto/16 :goto_59

    .end local v156    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .end local v167    # "versionInfo":Landroid/os/Bundle;
    .restart local v45    # "atlas":Lcom/android/server/AssetAtlasService;
    .restart local v60    # "cocktailBar":Lcom/android/server/cocktailbar/CocktailBarManagerService;
    .restart local v73    # "countryDetector":Lcom/android/server/CountryDetectorService;
    .restart local v75    # "coverService":Lcom/android/server/cover/CoverManagerService;
    .restart local v99    # "enterprisePolicy":Lcom/android/server/enterprise/EnterpriseDeviceManagerService;
    .restart local v104    # "imm":Lcom/android/server/InputMethodManagerService;
    .restart local v113    # "location":Lcom/android/server/LocationManagerService;
    .restart local v115    # "lockSettings":Lcom/android/server/LockSettingsService;
    .restart local v125    # "mediaRouter":Lcom/android/server/media/MediaRouterService;
    .restart local v139    # "notification":Landroid/app/INotificationManager;
    .restart local v144    # "sLocation":Landroid/os/IBinder;
    .restart local v152    # "statusBar":Lcom/android/server/statusbar/StatusBarManagerService;
    .restart local v155    # "telephonyRegistry":Lcom/android/server/TelephonyRegistry;
    .restart local v164    # "tsms":Lcom/android/server/TextServicesManagerService;
    .restart local v170    # "vrManager":Lcom/android/server/VRManagerService;
    .restart local v172    # "wallpaper":Lcom/android/server/wallpaper/WallpaperManagerService;
    :cond_4c
    move-object/from16 v3, v133

    .end local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    goto/16 :goto_2f

    .end local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    :cond_4d
    move-object/from16 v3, v133

    .end local v133    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    .restart local v3    # "networkPolicy":Lcom/android/server/net/NetworkPolicyManagerService;
    goto/16 :goto_4c
.end method

.method static final startSystemUi(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2217
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2218
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.systemui"

    const-string v3, "com.android.systemui.SystemUIService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2221
    sget-object v1, Landroid/os/UserHandle;->OWNER:Landroid/os/UserHandle;

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 2222
    return-void
.end method
