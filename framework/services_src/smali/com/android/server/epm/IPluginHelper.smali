.class public interface abstract Lcom/android/server/epm/IPluginHelper;
.super Ljava/lang/Object;
.source "IPluginHelper.java"


# virtual methods
.method public abstract getCategory()I
.end method

.method public abstract getDependencies(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getPluginList()Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getTargetAppList(Ljava/lang/String;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onBootCompleted()V
.end method

.method public abstract onPluginDisabled(Ljava/lang/String;)V
.end method

.method public abstract onPluginEnabled(Ljava/lang/String;)V
.end method

.method public abstract onPluginInstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
.end method

.method public abstract onPluginUninstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
.end method

.method public abstract parsePlugins()V
.end method

.method public abstract setCallback(Landroid/app/epm/IPluginManagerCallback;)V
.end method
