.class public interface abstract Lcom/android/server/epm/PluginConstants;
.super Ljava/lang/Object;
.source "PluginConstants.java"


# static fields
.field public static final CATEGORY_LANG:I = 0x2

.field public static final CATEGORY_PLUGIN:I = 0x0

.field public static final CATEGORY_THEME:I = 0x1

.field public static final COLUMN_NAME_CATEGORY:Ljava/lang/String; = "category"

.field public static final COLUMN_NAME_SOURCE_CLASS:Ljava/lang/String; = "sourceClass"

.field public static final COLUMN_NAME_SOURCE_PACKAGE:Ljava/lang/String; = "sourcePackage"

.field public static final COLUMN_NAME_TARGET_CLASS:Ljava/lang/String; = "targetClass"

.field public static final COLUMN_NAME_TARGET_PACKAGE:Ljava/lang/String; = "targetPackage"

.field public static final COMMA_SEP:Ljava/lang/String; = ","

.field public static final CONSTRUCT_DATA:I = 0x1

.field public static final DATABASE_NAME:Ljava/lang/String; = "FeatureInformation.db"

.field public static final DATABASE_VERSION:I = 0x1

.field public static final DEAFULT_EXPIRY_TIME:I = 0x2

.field public static final DEBUG_ELASTIC:Z = false

.field public static final DELIMITER:Ljava/lang/String; = "#"

.field public static final DESC:Ljava/lang/String; = "desc"

.field public static final EXIT_THREAD:I = 0x4

.field public static final EXPIRY_TIME:Ljava/lang/String; = "expiry_time"

.field public static final FEATURE_INFORMATION:Ljava/lang/String; = "FEATURE_INFO"

.field public static final KILL_ALL_PARENT:I = 0x3

.field public static final KILL_PARENT:I = 0x2

.field public static final LOCK_WALLPAPER_NAME:Ljava/lang/String; = "lockscreen_wallpaper.png"

.field public static final MASTER:Ljava/lang/String; = "master"

.field public static final MASTER_VERSION:Ljava/lang/String; = "version"

.field public static final META_DATA_LANGUAGE:Ljava/lang/String; = "elastic.language.ttf"

.field public static final MODULE_LANGUAGE:Ljava/lang/String; = "module_lang"

.field public static final MODULE_OVERLAY:Ljava/lang/String; = "module_overlay"

.field public static final MODULE_THEME:Ljava/lang/String; = "module_theme"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final OVERLAY:Ljava/lang/String; = "overlay"

.field public static final PACKAGE:Ljava/lang/String; = "package"

.field public static final PATH_FONT:Ljava/lang/String; = "/data/downloaded_fonts"

.field public static final PATH_LOCAL_TEMP:Ljava/lang/String; = "/data/local/tmp"

.field public static final PATH_LOCK_WALLPAPER:Ljava/lang/String; = "/data/overlays/lockwallpaper"

.field public static final PATH_OVERLAY:Ljava/lang/String; = "/data/overlays"

.field public static final PERMISSION_OVERLAY_LANGUAGE:Ljava/lang/String; = "com.samsung.android.permission.SAMSUNG_OVERLAY_LANGUAGE"

.field public static final PERMISSION_OVERLAY_THEME:Ljava/lang/String; = "com.samsung.android.permission.SAMSUNG_OVERLAY_THEME"

.field public static final PERMISSION_PLUGIN_NATIVE:Ljava/lang/String; = "com.samsung.android.permission.FEATURE_INJECTION"

.field public static final PLUGIN_DISABLE:I = 0x3

.field public static final PLUGIN_ENABLE:I = 0x2

.field public static final PLUGIN_INSTALL:I = 0x0

.field public static final PLUGIN_UNINSTALL:I = 0x1

.field public static final SHARED_PREF_FOR_THEMES:Ljava/lang/String; = "store"

.field public static final TABLE_NAME:Ljava/lang/String; = "featureInformation"

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final TYPE_INTEGER:Ljava/lang/String; = " INTEGER"

.field public static final TYPE_LANGUAGE:I = 0x1

.field public static final TYPE_TEXT:Ljava/lang/String; = " TEXT"

.field public static final TYPE_THEME:I = 0x0

.field public static final WALLPAPER:Ljava/lang/String; = "wallpaper"

.field public static final WALLPAPER_NAME:Ljava/lang/String; = "defalut.png"

.field public static final XML_FILE_PATH:Ljava/lang/String; = "/data/downloaded_fonts/downloaded_fonts.xml"

.field public static final XML_META_DATA_OVERLAY:Ljava/lang/String; = "samsung.overlay"

.field public static final XML_META_DATA_PLUGIN_NATIVE:Ljava/lang/String; = "samsung.injection"

.field public static final _ID:Ljava/lang/String; = "id"
