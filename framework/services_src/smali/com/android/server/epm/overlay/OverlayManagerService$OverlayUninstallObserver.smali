.class Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;
.super Landroid/content/pm/IPackageDeleteObserver$Stub;
.source "OverlayManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/epm/overlay/OverlayManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverlayUninstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/epm/overlay/OverlayManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/epm/overlay/OverlayManagerService;)V
    .locals 0

    .prologue
    .line 641
    iput-object p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    invoke-direct {p0}, Landroid/content/pm/IPackageDeleteObserver$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/epm/overlay/OverlayManagerService;Lcom/android/server/epm/overlay/OverlayManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;
    .param p2, "x1"    # Lcom/android/server/epm/overlay/OverlayManagerService$1;

    .prologue
    .line 641
    invoke-direct {p0, p1}, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;)V

    return-void
.end method


# virtual methods
.method public packageDeleted(Ljava/lang/String;I)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 648
    :try_start_0
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # operator-- for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$1210(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    .line 649
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayTargetMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$400(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 661
    .local v1, "targetPackageName":Ljava/lang/String;
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    iget-object v2, v2, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    if-eqz v2, :cond_0

    .line 662
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$1200(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    move-result v2

    if-nez v2, :cond_0

    .line 663
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    iget-object v2, v2, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    invoke-interface {v2, p1}, Landroid/app/epm/IPluginManagerCallback;->onUninstallCompleted(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    .end local v1    # "targetPackageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 666
    :catch_0
    move-exception v0

    .line 667
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
