.class public Lcom/android/server/epm/overlay/ThemeManagerService;
.super Lcom/android/server/epm/overlay/OverlayManagerService;
.source "ThemeManagerService.java"


# static fields
.field private static final DEBUG_ELASTIC:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private isLiveWallpaper:Z

.field private mContext:Landroid/content/Context;

.field private mDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field private mPackageName:Ljava/lang/String;

.field private wallManager:Landroid/app/WallpaperManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/android/server/epm/overlay/ThemeManagerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/server/epm/overlay/OverlayManagerService;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->isLiveWallpaper:Z

    .line 64
    iput-object p1, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    .line 65
    iget-object v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/epm/overlay/ThemeManagerService;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/ThemeManagerService;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/server/epm/overlay/ThemeManagerService;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0
    .param p0, "x0"    # Lcom/android/server/epm/overlay/ThemeManagerService;
    .param p1, "x1"    # Landroid/graphics/drawable/BitmapDrawable;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method private applyDefaultWallpaper()V
    .locals 6

    .prologue
    .line 178
    iget-object v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v4

    .line 179
    .local v4, "wallManager":Landroid/app/WallpaperManager;
    iget-boolean v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->isLiveWallpaper:Z

    if-eqz v5, :cond_1

    .line 180
    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.service.wallpaper.LIVE_WALLPAPER_CHOOSER"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v5, 0x10000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 182
    iget-object v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 194
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    const-string v3, "/data/overlays/defalut.png"

    .line 185
    .local v3, "path":Ljava/lang/String;
    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 187
    .local v2, "mBitmap":Landroid/graphics/Bitmap;
    if-eqz v2, :cond_0

    .line 188
    :try_start_0
    invoke-virtual {v4, v2}, Landroid/app/WallpaperManager;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 190
    :catch_0
    move-exception v0

    .line 191
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private applyNewWallpaper()V
    .locals 6

    .prologue
    .line 123
    :try_start_0
    iget-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->wallManager:Landroid/app/WallpaperManager;

    if-nez v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->wallManager:Landroid/app/WallpaperManager;

    .line 126
    :cond_0
    iget-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mWallpaperName:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 127
    iget-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mWallpaperName:Ljava/lang/String;

    const-string v4, "drawable"

    iget-object v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mPackageName:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 130
    .local v1, "id":I
    if-eqz v1, :cond_1

    .line 131
    iget-object v2, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->wallManager:Landroid/app/WallpaperManager;

    invoke-virtual {v2, v1}, Landroid/app/WallpaperManager;->setResource(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    .end local v1    # "id":I
    :cond_1
    :goto_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private saveDefaultWallpaper()V
    .locals 3

    .prologue
    .line 145
    iget-object v1, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/WallpaperManager;->getInstance(Landroid/content/Context;)Landroid/app/WallpaperManager;

    move-result-object v0

    .line 146
    .local v0, "wallManager":Landroid/app/WallpaperManager;
    invoke-virtual {v0}, Landroid/app/WallpaperManager;->getWallpaperInfo()Landroid/app/WallpaperInfo;

    move-result-object v1

    if-nez v1, :cond_0

    .line 147
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/android/server/epm/overlay/ThemeManagerService$1;

    invoke-direct {v2, p0, v0}, Lcom/android/server/epm/overlay/ThemeManagerService$1;-><init>(Lcom/android/server/epm/overlay/ThemeManagerService;Landroid/app/WallpaperManager;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 173
    :goto_0
    return-void

    .line 171
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->isLiveWallpaper:Z

    goto :goto_0
.end method


# virtual methods
.method public extractWallpaper()V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/android/server/epm/overlay/ThemeManagerService;->saveDefaultWallpaper()V

    .line 116
    invoke-direct {p0}, Lcom/android/server/epm/overlay/ThemeManagerService;->applyNewWallpaper()V

    .line 117
    return-void
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x1

    return v0
.end method

.method public onPluginInstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgManager"    # Landroid/content/pm/PackageManager;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/epm/overlay/OverlayManagerService;->onPluginInstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 88
    iput-object p1, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mContext:Landroid/content/Context;

    .line 89
    iput-object p3, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mPackageName:Ljava/lang/String;

    .line 90
    iget-object v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mWallpaperName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/android/server/epm/overlay/ThemeManagerService;->extractWallpaper()V

    .line 93
    :cond_0
    return-void
.end method

.method public onPluginUninstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgManager"    # Landroid/content/pm/PackageManager;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/android/server/epm/overlay/OverlayManagerService;->onPluginUninstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mWallpaperName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/android/server/epm/overlay/ThemeManagerService;->applyDefaultWallpaper()V

    .line 106
    :cond_0
    return-void
.end method

.method public parsePlugins()V
    .locals 9

    .prologue
    .line 209
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " parsePlugins -- "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    iget-object v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "com.samsung.android.permission.SAMSUNG_OVERLAY_THEME"

    aput-object v8, v6, v7

    const/16 v7, 0x80

    invoke-virtual {v5, v6, v7}, Landroid/content/pm/PackageManager;->getPackagesHoldingPermissions([Ljava/lang/String;I)Ljava/util/List;

    move-result-object v3

    .line 215
    .local v3, "packageInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "packageInfos ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_2

    .line 217
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "total packageInfos ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/PackageInfo;

    .line 220
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "packageInfo ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    const/4 v0, 0x0

    .line 222
    .local v0, "aInfo":Landroid/content/pm/ApplicationInfo;
    if-eqz v2, :cond_1

    .line 223
    iget-object v0, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 224
    :cond_1
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "aInfo ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    if-eqz v0, :cond_0

    .line 227
    iget-object v5, p0, Lcom/android/server/epm/overlay/ThemeManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const-string/jumbo v6, "samsung.overlay"

    invoke-virtual {v0, v5, v6}, Landroid/content/pm/ApplicationInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 229
    .local v4, "parser":Landroid/content/res/XmlResourceParser;
    sget-object v5, Lcom/android/server/epm/overlay/ThemeManagerService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "aInfo ="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 230
    iget-boolean v5, v0, Landroid/content/pm/ApplicationInfo;->enabled:Z

    invoke-super {p0, v4, v5}, Lcom/android/server/epm/overlay/OverlayManagerService;->parseMetaData(Landroid/content/res/XmlResourceParser;Z)V

    goto :goto_0

    .line 235
    .end local v0    # "aInfo":Landroid/content/pm/ApplicationInfo;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v4    # "parser":Landroid/content/res/XmlResourceParser;
    :cond_2
    return-void
.end method

.method public setCallback(Landroid/app/epm/IPluginManagerCallback;)V
    .locals 0
    .param p1, "callback"    # Landroid/app/epm/IPluginManagerCallback;

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcom/android/server/epm/overlay/OverlayManagerService;->setCallback(Landroid/app/epm/IPluginManagerCallback;)V

    .line 202
    return-void
.end method
