.class Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;
.super Landroid/content/pm/IPackageInstallObserver$Stub;
.source "OverlayManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/epm/overlay/OverlayManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverlayInstallObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/epm/overlay/OverlayManagerService;


# direct methods
.method private constructor <init>(Lcom/android/server/epm/overlay/OverlayManagerService;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    invoke-direct {p0}, Landroid/content/pm/IPackageInstallObserver$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/epm/overlay/OverlayManagerService;Lcom/android/server/epm/overlay/OverlayManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;
    .param p2, "x1"    # Lcom/android/server/epm/overlay/OverlayManagerService$1;

    .prologue
    .line 256
    invoke-direct {p0, p1}, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;)V

    return-void
.end method


# virtual methods
.method public packageInstalled(Ljava/lang/String;I)V
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "returnCode"    # I

    .prologue
    .line 264
    if-eqz p1, :cond_0

    .line 266
    :try_start_0
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # operator++ for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$208(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    .line 267
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$300(Lcom/android/server/epm/overlay/OverlayManagerService;)Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 268
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/content/pm/PackageInfo;->overlayTarget:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 272
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayTargetMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$400(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, v1, Landroid/content/pm/PackageInfo;->overlayTarget:Ljava/lang/String;

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$300(Lcom/android/server/epm/overlay/OverlayManagerService;)Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 278
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    iget-object v2, v2, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$200(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    move-result v2

    iget-object v3, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I
    invoke-static {v3}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$500(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 280
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    iget-object v2, v2, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    const/16 v3, 0x64

    invoke-interface {v2, p1, v3}, Landroid/app/epm/IPluginManagerCallback;->onInstallCompleted(Ljava/lang/String;I)V

    .line 285
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    const/4 v3, 0x0

    # setter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I
    invoke-static {v2, v3}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$202(Lcom/android/server/epm/overlay/OverlayManagerService;I)I

    .line 286
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    const/4 v3, 0x0

    # setter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I
    invoke-static {v2, v3}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$502(Lcom/android/server/epm/overlay/OverlayManagerService;I)I

    .line 287
    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$600()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "packageInstalled ALL COMPLETE--> mMasterPackageName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mMasterPackageName:Ljava/lang/String;
    invoke-static {v4}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$700(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mMasterPackageName:Ljava/lang/String;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$700(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 290
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;
    invoke-static {v2}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$300(Lcom/android/server/epm/overlay/OverlayManagerService;)Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->mMasterPackageName:Ljava/lang/String;
    invoke-static {v3}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$700(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 302
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    :goto_0
    return-void

    .line 294
    .restart local v1    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    iget-object v2, v2, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    iget-object v3, p0, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;->this$0:Lcom/android/server/epm/overlay/OverlayManagerService;

    # getter for: Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I
    invoke-static {v3}, Lcom/android/server/epm/overlay/OverlayManagerService;->access$200(Lcom/android/server/epm/overlay/OverlayManagerService;)I

    move-result v3

    mul-int/lit8 v3, v3, 0xa

    invoke-interface {v2, p1, v3}, Landroid/app/epm/IPluginManagerCallback;->onInstallCompleted(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 298
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
