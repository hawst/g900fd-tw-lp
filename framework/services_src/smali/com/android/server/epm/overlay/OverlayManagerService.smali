.class public Lcom/android/server/epm/overlay/OverlayManagerService;
.super Ljava/lang/Object;
.source "OverlayManagerService.java"

# interfaces
.implements Lcom/android/server/epm/IPluginHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;,
        Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;,
        Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;
    }
.end annotation


# static fields
.field private static final DEBUG_ELASTIC:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private KEY_VALUE:Ljava/lang/String;

.field private expiryTime:I

.field mCallback:Landroid/app/epm/IPluginManagerCallback;

.field private mContext:Landroid/content/Context;

.field private mMasterPackageName:Ljava/lang/String;

.field private mNoOfOverlays:I

.field private mOverlay:Ljava/io/File;

.field private mOverlayInstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;

.field private mOverlayUninstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;

.field private mPackageManager:Landroid/content/pm/PackageManager;

.field public mWallpaperName:Ljava/lang/String;

.field private masterDetailsMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;",
            ">;"
        }
    .end annotation
.end field

.field private masterOverlaysMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private masterPackageName:Ljava/lang/String;

.field private masterStatus:Z

.field private masterVersion:Ljava/lang/String;

.field private overlayCount:I

.field private overlayTargetMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private overlaysForuninstall:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/android/server/epm/overlay/OverlayManagerService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/android/server/epm/overlay/OverlayManagerService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    .line 80
    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I

    .line 82
    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlay:Ljava/io/File;

    .line 84
    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterPackageName:Ljava/lang/String;

    .line 85
    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterVersion:Ljava/lang/String;

    .line 86
    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->expiryTime:I

    .line 87
    const-string/jumbo v0, "value"

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->KEY_VALUE:Ljava/lang/String;

    .line 88
    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I

    .line 98
    iput-object p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    .line 99
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 100
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayTargetMap:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayTargetMap:Ljava/util/HashMap;

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    if-nez v0, :cond_2

    .line 109
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    .line 111
    :cond_2
    return-void
.end method

.method static synthetic access$100(Lcom/android/server/epm/overlay/OverlayManagerService;Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/content/pm/PackageManager;
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lcom/android/server/epm/overlay/OverlayManagerService;->installOverlays(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->KEY_VALUE:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I

    return v0
.end method

.method static synthetic access$1210(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I

    return v0
.end method

.method static synthetic access$200(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/server/epm/overlay/OverlayManagerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I

    return p1
.end method

.method static synthetic access$208(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 2
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/server/epm/overlay/OverlayManagerService;)Landroid/content/pm/PackageManager;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlayTargetMap:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    return v0
.end method

.method static synthetic access$502(Lcom/android/server/epm/overlay/OverlayManagerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    return p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/android/server/epm/overlay/OverlayManagerService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/server/epm/overlay/OverlayManagerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mMasterPackageName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/android/server/epm/overlay/OverlayManagerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->expiryTime:I

    return v0
.end method

.method static synthetic access$900(Lcom/android/server/epm/overlay/OverlayManagerService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/android/server/epm/overlay/OverlayManagerService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private installOverlays(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgManager"    # Landroid/content/pm/PackageManager;
    .param p3, "masterPackageName"    # Ljava/lang/String;

    .prologue
    .line 209
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 210
    .local v0, "am":Landroid/content/res/AssetManager;
    const-string v7, "overlay"

    invoke-virtual {v0, v7}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 211
    .local v4, "mOverlayApks":[Ljava/lang/String;
    const-string/jumbo v7, "wallpaper"

    invoke-virtual {v0, v7}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 212
    .local v5, "mWallPaperImage":[Ljava/lang/String;
    array-length v7, v4

    iput v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    .line 220
    iget v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    if-lez v7, :cond_1

    .line 221
    new-instance v1, Ljava/io/File;

    const-string v7, "/data/overlays"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .local v1, "downloadedDir":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 223
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 226
    :cond_0
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mNoOfOverlays:I

    if-ge v3, v7, :cond_1

    .line 230
    aget-object v7, v4, v3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "overlay/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v4, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    const-string v9, "/data/overlays"

    invoke-virtual {p0, v7, v8, v9}, Lcom/android/server/epm/overlay/OverlayManagerService;->doCopy(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;

    move-result-object v7

    iput-object v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlay:Ljava/io/File;

    .line 237
    const-string v7, "/data/overlays"

    const/16 v8, 0x1ff

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    move-result v6

    .line 241
    .local v6, "stat":I
    iget-object v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlay:Ljava/io/File;

    const/16 v8, 0x1ff

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/io/File;III)I

    move-result v6

    .line 246
    iget-object v7, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v8, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlay:Ljava/io/File;

    invoke-static {v8}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayInstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/content/pm/PackageManager;->installPackage(Landroid/net/Uri;Landroid/content/pm/IPackageInstallObserver;ILjava/lang/String;)V

    .line 226
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 249
    .end local v1    # "downloadedDir":Ljava/io/File;
    .end local v3    # "i":I
    .end local v6    # "stat":I
    :cond_1
    array-length v7, v5

    if-lez v7, :cond_2

    .line 250
    const/4 v7, 0x0

    aget-object v7, v5, v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v9, "wallpaper/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v8

    const-string v9, "/data/overlays"

    invoke-virtual {p0, v7, v8, v9}, Lcom/android/server/epm/overlay/OverlayManagerService;->doCopy(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 255
    .end local v0    # "am":Landroid/content/res/AssetManager;
    .end local v4    # "mOverlayApks":[Ljava/lang/String;
    .end local v5    # "mWallPaperImage":[Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 252
    :catch_0
    move-exception v2

    .line 253
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private startExpiryTimerForUninstall(Ljava/lang/String;)V
    .locals 6
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 436
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 437
    .local v0, "mTimer":Ljava/util/Timer;
    new-instance v1, Lcom/android/server/epm/overlay/OverlayManagerService$2;

    invoke-direct {v1, p0, p1, v0}, Lcom/android/server/epm/overlay/OverlayManagerService$2;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;Ljava/lang/String;Ljava/util/Timer;)V

    .line 462
    .local v1, "mTask":Ljava/util/TimerTask;
    const-wide/16 v2, 0x0

    const-wide/32 v4, 0xea60

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 463
    return-void
.end method


# virtual methods
.method public copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 358
    const/16 v2, 0x400

    new-array v0, v2, [B

    .line 360
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "read":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 361
    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 363
    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    .line 364
    return-void
.end method

.method public doCopy(Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)Ljava/io/File;
    .locals 11
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "dirPath"    # Ljava/lang/String;

    .prologue
    .line 312
    const/4 v0, 0x0

    .line 314
    .local v0, "apkFile":Ljava/io/File;
    :try_start_0
    const-string v7, "S_HW_Touch.ogg"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "TW_Touch.ogg"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 315
    :cond_0
    new-instance v2, Ljava/io/File;

    const-string v7, "/data/data/com.sec.android.theme/effects/"

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 316
    .local v2, "dirFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_1

    .line 317
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 319
    :cond_1
    new-instance v1, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "/data/data/com.sec.android.theme/effects/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .end local v0    # "apkFile":Ljava/io/File;
    .local v1, "apkFile":Ljava/io/File;
    move-object v0, v1

    .line 334
    .end local v1    # "apkFile":Ljava/io/File;
    .end local v2    # "dirFile":Ljava/io/File;
    .restart local v0    # "apkFile":Ljava/io/File;
    :goto_0
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 335
    .local v5, "os":Ljava/io/OutputStream;
    invoke-virtual {p0, p2, v5}, Lcom/android/server/epm/overlay/OverlayManagerService;->copyFile(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 336
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    .line 337
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 338
    const-string/jumbo v7, "wallpaper.png"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 341
    const-string v7, "/data/overlays/lockwallpaper"

    const/16 v8, 0x1ff

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    move-result v6

    .line 344
    .local v6, "stat":I
    const-string v7, "/data/overlays/lockwallpaper/lockscreen_wallpaper.png"

    const/16 v8, 0x1ff

    const/4 v9, -0x1

    const/4 v10, -0x1

    invoke-static {v7, v8, v9, v10}, Landroid/os/FileUtils;->setPermissions(Ljava/lang/String;III)I

    .line 351
    .end local v5    # "os":Ljava/io/OutputStream;
    .end local v6    # "stat":I
    :cond_2
    :goto_1
    return-object v0

    .line 322
    :cond_3
    const-string/jumbo v7, "wallpaper.png"

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 323
    new-instance v3, Ljava/io/File;

    const-string v7, "/data/overlays/lockwallpaper"

    invoke-direct {v3, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 324
    .local v3, "downloadedDir":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    .line 325
    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 327
    :cond_4
    new-instance v1, Ljava/io/File;

    const-string v7, "/data/overlays/lockwallpaper"

    const-string v8, "lockscreen_wallpaper.png"

    invoke-direct {v1, v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .end local v0    # "apkFile":Ljava/io/File;
    .restart local v1    # "apkFile":Ljava/io/File;
    move-object v0, v1

    .line 329
    .end local v1    # "apkFile":Ljava/io/File;
    .restart local v0    # "apkFile":Ljava/io/File;
    goto :goto_0

    .line 330
    .end local v3    # "downloadedDir":Ljava/io/File;
    :cond_5
    new-instance v1, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v0    # "apkFile":Ljava/io/File;
    .restart local v1    # "apkFile":Ljava/io/File;
    move-object v0, v1

    .end local v1    # "apkFile":Ljava/io/File;
    .restart local v0    # "apkFile":Ljava/io/File;
    goto :goto_0

    .line 348
    :catch_0
    move-exception v4

    .line 349
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public getCategory()I
    .locals 1

    .prologue
    .line 688
    const/4 v0, 0x0

    return v0
.end method

.method public getData(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "string"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 469
    const-string/jumbo v1, "store"

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 470
    .local v0, "mPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v0, p2, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public getDependencies(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPluginList()Ljava/util/HashMap;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 137
    .local v0, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 138
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 139
    .local v3, "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 140
    .local v4, "pkg":Ljava/lang/String;
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    .line 141
    .local v2, "obj":Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mTitle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mDesc:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mCategory:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "#"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v7, v2, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 143
    .local v5, "value":Ljava/lang/String;
    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 146
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "obj":Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;
    .end local v3    # "packages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v4    # "pkg":Ljava/lang/String;
    .end local v5    # "value":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public getTargetAppList(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public onBootCompleted()V
    .locals 0

    .prologue
    .line 594
    return-void
.end method

.method public onPluginDisabled(Ljava/lang/String;)V
    .locals 10
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 560
    if-eqz p1, :cond_0

    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-nez v6, :cond_1

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    iget-boolean v6, v6, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    if-eqz v6, :cond_0

    .line 565
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    iput-boolean v9, v6, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    .line 566
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    .line 567
    .local v3, "manager":Landroid/app/ActivityManager;
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 568
    .local v5, "overlays":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 569
    .local v0, "i":I
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_0

    .line 570
    sget-object v6, Lcom/android/server/epm/overlay/OverlayManagerService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPluginDisabled ..overlays.size()="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 572
    .local v4, "overlayPkg":Ljava/lang/String;
    sget-object v6, Lcom/android/server/epm/overlay/OverlayManagerService;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "onPluginDisabled .. mPackageManager="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", overlayPkg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v7, 0x2

    invoke-virtual {v6, v4, v7, v9}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 574
    if-nez v0, :cond_2

    .line 575
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "current_sec_theme_package"

    const-string v8, ""

    invoke-static {v6, v7, v8}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 576
    new-instance v2, Landroid/content/Intent;

    const-string v6, "HOME_SCREEN_THEME_CHANGED"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 577
    .local v2, "intent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v6, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 578
    const/4 v0, -0x1

    goto :goto_1

    .line 584
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v4    # "overlayPkg":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v7, "lockscreen_wallpaper_path"

    const-string v8, ""

    const/4 v9, -0x2

    invoke-static {v6, v7, v8, v9}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    goto/16 :goto_0
.end method

.method public onPluginEnabled(Ljava/lang/String;)V
    .locals 12
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 504
    if-eqz p1, :cond_0

    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-nez v9, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 507
    :cond_1
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    iget-boolean v9, v9, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    if-eq v9, v10, :cond_0

    .line 510
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    iput-boolean v10, v9, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    .line 513
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    const-string v10, "activity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager;

    .line 515
    .local v6, "manager":Landroid/app/ActivityManager;
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    invoke-virtual {v9, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/ArrayList;

    .line 516
    .local v8, "overlays":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .line 517
    .local v2, "i":I
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 521
    .local v7, "overlayPkg":Ljava/lang/String;
    :try_start_0
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v7, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    .line 524
    .local v4, "info":Landroid/content/pm/PackageInfo;
    if-eqz v4, :cond_2

    .line 525
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v10, 0x1

    const/4 v11, 0x0

    invoke-virtual {v9, v7, v10, v11}, Landroid/content/pm/PackageManager;->setApplicationEnabledSetting(Ljava/lang/String;II)V

    .line 526
    if-nez v2, :cond_2

    .line 527
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "current_sec_theme_package"

    invoke-static {v9, v10, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 528
    new-instance v5, Landroid/content/Intent;

    const-string v9, "HOME_SCREEN_THEME_CHANGED"

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 529
    .local v5, "intent":Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 530
    const/4 v2, -0x1

    goto :goto_1

    .line 533
    .end local v4    # "info":Landroid/content/pm/PackageInfo;
    .end local v5    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v1

    .line 534
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 543
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v7    # "overlayPkg":Ljava/lang/String;
    :cond_3
    const-string v0, "/data/overlays/lockwallpaper/lockscreen_wallpaper.png"

    .line 544
    .local v0, "copiedLocation":Ljava/lang/String;
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "lockscreen_wallpaper_path"

    const/4 v11, -0x2

    invoke-static {v9, v10, v0, v11}, Landroid/provider/Settings$System;->putStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;I)Z

    .line 547
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterVersion:Ljava/lang/String;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterVersion:Ljava/lang/String;

    const-string v10, "Trial Version"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 548
    iget-object v9, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterPackageName:Ljava/lang/String;

    invoke-direct {p0, v9}, Lcom/android/server/epm/overlay/OverlayManagerService;->startExpiryTimerForUninstall(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public onPluginInstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgManager"    # Landroid/content/pm/PackageManager;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 158
    if-nez p3, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iput-object p3, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mMasterPackageName:Ljava/lang/String;

    .line 162
    iput-object p2, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mPackageManager:Landroid/content/pm/PackageManager;

    .line 164
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayInstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;

    if-nez v4, :cond_2

    .line 165
    new-instance v4, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;Lcom/android/server/epm/overlay/OverlayManagerService$1;)V

    iput-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayInstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayInstallObserver;

    .line 168
    :cond_2
    const/4 v0, 0x0

    .line 170
    .local v0, "aInfo":Landroid/content/pm/ApplicationInfo;
    const/16 v4, 0x80

    :try_start_0
    invoke-virtual {p2, p3, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_0

    .line 175
    const-string/jumbo v4, "samsung.overlay"

    invoke-virtual {v0, p2, v4}, Landroid/content/pm/ApplicationInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v3

    .line 181
    .local v3, "parser":Landroid/content/res/XmlResourceParser;
    if-eqz v3, :cond_0

    .line 182
    const/4 v4, 0x0

    invoke-virtual {p1, p3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v2

    .line 185
    .local v2, "masterContext":Landroid/content/Context;
    new-instance v4, Ljava/lang/Thread;

    new-instance v5, Lcom/android/server/epm/overlay/OverlayManagerService$1;

    invoke-direct {v5, p0, v2, p2, p3}, Lcom/android/server/epm/overlay/OverlayManagerService$1;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    .line 192
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/android/server/epm/overlay/OverlayManagerService;->parseMetaData(Landroid/content/res/XmlResourceParser;Z)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 195
    .end local v2    # "masterContext":Landroid/content/Context;
    .end local v3    # "parser":Landroid/content/res/XmlResourceParser;
    :catch_0
    move-exception v1

    .line 196
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPluginUninstalled(Landroid/content/Context;Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pkgManager"    # Landroid/content/pm/PackageManager;
    .param p3, "packageName"    # Ljava/lang/String;

    .prologue
    .line 604
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayUninstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;

    if-nez v4, :cond_0

    .line 605
    new-instance v4, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5}, Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;Lcom/android/server/epm/overlay/OverlayManagerService$1;)V

    iput-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayUninstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;

    .line 607
    :cond_0
    if-eqz p3, :cond_1

    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 636
    :cond_1
    :goto_0
    return-void

    .line 610
    :cond_2
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    .line 611
    .local v0, "details":Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;
    iget-boolean v4, v0, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;->mStatus:Z

    iput-boolean v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterStatus:Z

    .line 615
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    invoke-virtual {v4, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 619
    .local v2, "overlayList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 621
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    iput v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->overlaysForuninstall:I

    .line 624
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 627
    .local v3, "overlayPackage":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mOverlayUninstallObserver:Lcom/android/server/epm/overlay/OverlayManagerService$OverlayUninstallObserver;

    const/4 v5, 0x0

    invoke-virtual {p2, v3, v4, v5}, Landroid/content/pm/PackageManager;->deletePackage(Ljava/lang/String;Landroid/content/pm/IPackageDeleteObserver;I)V

    goto :goto_1

    .line 629
    .end local v3    # "overlayPackage":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    invoke-virtual {v4, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 630
    iget-object v4, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    invoke-virtual {v4, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public parseMetaData(Landroid/content/res/XmlResourceParser;Z)V
    .locals 13
    .param p1, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p2, "status"    # Z

    .prologue
    .line 374
    :try_start_0
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    .line 375
    .local v6, "eventType":I
    const/4 v8, 0x0

    .line 376
    .local v8, "i":I
    const/4 v10, 0x0

    .line 378
    .local v10, "packageNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    const/4 v0, 0x1

    if-eq v6, v0, :cond_2

    .line 379
    if-nez v6, :cond_1

    .line 424
    :cond_0
    :goto_1
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v6

    goto :goto_0

    .line 380
    :cond_1
    const/4 v0, 0x2

    if-ne v6, v0, :cond_6

    .line 383
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "master"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 384
    const/4 v0, 0x0

    const-string v1, "package"

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterPackageName:Ljava/lang/String;

    .line 391
    iget-object v11, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterDetailsMap:Ljava/util/HashMap;

    iget-object v12, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterPackageName:Ljava/lang/String;

    new-instance v0, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;

    const/4 v1, 0x0

    const-string/jumbo v2, "title"

    invoke-interface {p1, v1, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v1, 0x0

    const-string v3, "desc"

    invoke-interface {p1, v1, v3}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "theme"

    move-object v1, p0

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/android/server/epm/overlay/OverlayManagerService$MasterDetails;-><init>(Lcom/android/server/epm/overlay/OverlayManagerService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v11, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    const/4 v0, 0x0

    const-string/jumbo v1, "version"

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterVersion:Ljava/lang/String;

    .line 396
    const/4 v0, 0x0

    const-string v1, "expiry_time"

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 397
    const/4 v0, 0x0

    const-string v1, "expiry_time"

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->expiryTime:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 428
    .end local v6    # "eventType":I
    .end local v8    # "i":I
    .end local v10    # "packageNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v7

    .line 429
    .local v7, "ex":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    .line 431
    .end local v7    # "ex":Ljava/lang/Exception;
    :cond_2
    return-void

    .line 399
    .restart local v6    # "eventType":I
    .restart local v8    # "i":I
    .restart local v10    # "packageNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const/4 v0, 0x2

    :try_start_1
    iput v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->expiryTime:I

    goto :goto_1

    .line 405
    :cond_4
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "overlay"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 406
    new-instance v10, Ljava/util/ArrayList;

    .end local v10    # "packageNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 407
    .restart local v10    # "packageNamesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .local v9, "i1":I
    :goto_2
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 408
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 407
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 411
    .end local v9    # "i1":I
    :cond_5
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "wallpaper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    const/4 v0, 0x0

    const-string v1, "name"

    invoke-interface {p1, v0, v1}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mWallpaperName:Ljava/lang/String;

    goto/16 :goto_1

    .line 416
    :cond_6
    const/4 v0, 0x3

    if-ne v6, v0, :cond_7

    .line 419
    invoke-interface {p1}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "overlay"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    iget-object v0, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterOverlaysMap:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->masterPackageName:Ljava/lang/String;

    invoke-virtual {v0, v1, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 422
    :cond_7
    const/4 v0, 0x4

    if-ne v6, v0, :cond_0

    goto/16 :goto_1
.end method

.method public parsePlugins()V
    .locals 0

    .prologue
    .line 697
    return-void
.end method

.method public setCallback(Landroid/app/epm/IPluginManagerCallback;)V
    .locals 0
    .param p1, "callback"    # Landroid/app/epm/IPluginManagerCallback;

    .prologue
    .line 680
    iput-object p1, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->mCallback:Landroid/app/epm/IPluginManagerCallback;

    .line 681
    return-void
.end method

.method public storeData(Landroid/content/Context;I)V
    .locals 4
    .param p1, "mContext"    # Landroid/content/Context;
    .param p2, "val"    # I

    .prologue
    .line 478
    const-string/jumbo v2, "store"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 479
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 480
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/android/server/epm/overlay/OverlayManagerService;->KEY_VALUE:Ljava/lang/String;

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 482
    return-void
.end method
