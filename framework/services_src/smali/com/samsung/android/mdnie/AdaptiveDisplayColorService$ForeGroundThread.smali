.class final Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
.super Ljava/lang/Thread;
.source "AdaptiveDisplayColorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ForeGroundThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0

    .prologue
    .line 378
    iput-object p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p2, "x1"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;

    .prologue
    .line 378
    invoke-direct {p0, p1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 382
    :cond_0
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # getter for: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    invoke-static {v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$900(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-static {}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->interrupted()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 384
    const-wide/16 v2, 0x12c

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    .line 385
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # getter for: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z
    invoke-static {v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$1000(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 387
    :try_start_2
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    iget-object v1, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mProcessObserver:Landroid/app/IProcessObserver;

    const/4 v2, -0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IProcessObserver;->onForegroundActivitiesChanged(IIZ)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "e":Ljava/lang/Exception;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 392
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v1

    goto :goto_0

    .line 398
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # setter for: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    invoke-static {v1, v5}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$902(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    .line 400
    :goto_1
    return-void

    .line 395
    :catch_2
    move-exception v0

    .line 396
    .restart local v0    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 398
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # setter for: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    invoke-static {v1, v5}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$902(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    goto :goto_1

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # setter for: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    invoke-static {v2, v5}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$902(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    throw v1
.end method
