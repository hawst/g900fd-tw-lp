.class final Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;
.super Landroid/os/Handler;
.source "AdaptiveDisplayColorService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ScrControlHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Landroid/os/Looper;)V
    .locals 1
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 799
    iput-object p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .line 800
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 801
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 805
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 827
    :goto_0
    return-void

    .line 807
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->sendRgbAverage()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2200(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 810
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->animateScrRGB()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2300(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 813
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setVideoMode()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2400(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 816
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setBrowserMode()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2500(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 819
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->terminateVideoMode()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2600(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 822
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->this$0:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    # invokes: Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->terminateScrRGB()V
    invoke-static {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->access$2700(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    goto :goto_0

    .line 805
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
