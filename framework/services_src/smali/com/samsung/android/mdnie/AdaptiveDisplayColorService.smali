.class public Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
.super Ljava/lang/Object;
.source "AdaptiveDisplayColorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;,
        Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;,
        Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;,
        Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScreenWatchingReceiver;
    }
.end annotation


# static fields
.field private static final EAD_ON:Ljava/lang/String; = "sys.adaptivedisplay.eadon"

.field private static final TAG:Ljava/lang/String; = "AdaptiveDisplayColorService"


# instance fields
.field private final ACTION_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String;

.field private final ANIMATION_DEBOUNCE_MILLIS:J

.field private final ANIMATION_MAX_COUNT:F

.field private final APP_MANAGER_NAME:Ljava/lang/String;

.field private final BROWSER_MODE_DEBOUNCE_MILLIS:J

.field private final BROWSER_NAMES:[Ljava/lang/String;

.field private final DEBUG:Z

.field private final EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String;

.field private final FOREGROUND_THREAD_DELAY_MILLIS:J

.field private final LIGHT_SENSOR_RAW_DATA_PATH:Ljava/lang/String;

.field private final LIGHT_SENSOR_READ_DELAY:I

.field private final MAX_RGB_SENSOR_COUNT:I

.field private final MDNIE_BROWSER_MODE_NUMBER:Ljava/lang/String;

.field private final MDNIE_READING_MODE_NUMBER:Ljava/lang/String;

.field private final MDNIE_VIDEO_MODE_NUMBER:Ljava/lang/String;

.field private final MEDIA_PLAYER_NAMES:[Ljava/lang/String;

.field private final MSG_ANIMATE_SCR_RGB:I

.field private final MSG_SEND_RGB_AVERAGE:I

.field private final MSG_SET_BROWSER_MODE:I

.field private final MSG_SET_VIDEO_MODE:I

.field private final MSG_TERMINATE_SCR_RGB:I

.field private final MSG_TERMINATE_VIDEO_MODE:I

.field private final MULTI_SCREEN_DEBOUNCE_MILLIS:J

.field private final NUMBER_COEFFICIENT_VALUE:I

.field private final RGB_DEBOUNCE_MILLIS:J

.field private final RGB_FLOAT_MAX:F

.field private final RGB_INTEGER_MAX:I

.field private final SBROWSER_NAME:Ljava/lang/String;

.field private final SCENARIO_FILE_PATH:Ljava/lang/String;

.field private final SCREEN_MODE_AUTOMATIC_SETTING:Ljava/lang/String;

.field private final SCREEN_MODE_SETTING:Ljava/lang/String;

.field private final SCR_FILE_PATH:Ljava/lang/String;

.field private final SETUP_WIZARD_NAME:Ljava/lang/String;

.field private final SSRM_INTENT_MDNIE_SETTING:Ljava/lang/String;

.field private final TEST_RGB_EXPONENTIAL:I

.field private final VIDEO_MODE_DEBOUNCE_MILLIS:J

.field private exitHomeDelay:I

.field private exitHomeDelayTime:J

.field private exitMenuDelay:I

.field private exitMenuDelayTime:J

.field private foregroundDelayTime:J

.field private isLockScreenOn:Z

.field private mAceessibilityEnabled:Z

.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAutoModeEnabled:Z

.field private mAvgB:F

.field private mAvgG:F

.field private mAvgR:F

.field private mBrowserScenarioEnabled:Z

.field private mCoefficientValueArray:[F

.field private mColorBlindEnabled:Z

.field private final mContext:Landroid/content/Context;

.field private mCountAnimationValue:I

.field private mCountSensorValue:I

.field private mCoverManager:Lcom/samsung/android/cover/CoverManager;

.field private mCoverState:Z

.field private mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

.field private mDefaultAdjustB:I

.field private mDefaultAdjustG:I

.field private mDefaultAdjustR:I

.field private mDefaultB:I

.field private mDefaultEbookB:I

.field private mDefaultEbookG:I

.field private mDefaultEbookR:I

.field private mDefaultG:I

.field private mDefaultR:I

.field private mEBookScenarioIntented:Z

.field private mEbookAdjustB:I

.field private mEbookAdjustG:I

.field private mEbookAdjustR:I

.field private mEbookScenarioEnabled:Z

.field private mEnvironmentDisplayColorServiceEnable:Z

.field private mFinalIntAvgB:I

.field private mFinalIntAvgG:I

.field private mFinalIntAvgR:I

.field private mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

.field private mForegroundThreadWork:Z

.field private mGreyScaleModeEnabled:Z

.field private mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsFirstStart:Z

.field private mLastAvgB:F

.field private mLastAvgG:F

.field private mLastAvgR:F

.field private mLastChangedRgbTime:J

.field private mLightSensorB:I

.field private mLightSensorDelay:I

.field private mLightSensorG:I

.field private mLightSensorR:I

.field private mMultiWindowOn:Z

.field private mNegativeColorEnabled:Z

.field private mPowerSavingEnabled:Z

.field private mPrevContorlZone:I

.field private mPrevIntAvgB:I

.field private mPrevIntAvgG:I

.field private mPrevIntAvgR:I

.field mProcessObserver:Landroid/app/IProcessObserver;

.field private mRgbSensor:Landroid/hardware/Sensor;

.field private mRgbSensorListener:Landroid/hardware/SensorEventListener;

.field private mRgbThreshold:F

.field private mScrFileExists:Z

.field private mScreenCurtainEnabled:Z

.field private mScreenMode:I

.field private mScreenStateOn:Z

.field private mScreenWatchingReceiver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScreenWatchingReceiver;

.field private mSensorEnabled:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mSensorValueValid:Z

.field private mSettingCondition:Z

.field private mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

.field private mSumLux:I

.field private mTempIntAvgB:I

.field private mTempIntAvgG:I

.field private mTempIntAvgR:I

.field private mTestScrB:I

.field private mTestScrG:I

.field private mTestScrR:I

.field private mThreadEnableCondition:Z

.field private mUltraPowerSavingModeEnabled:Z

.field private mUseAdaptiveDisplayColorServiceConfig:Z

.field private mUseEnvironmentDisplayColorConfig:Z

.field private mValidZone:Z

.field private mVideoScenarioEnabled:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v9, "eng"

    sget-object v10, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    .line 72
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->RGB_INTEGER_MAX:I

    .line 73
    const/high16 v9, 0x437f0000    # 255.0f

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->RGB_FLOAT_MAX:F

    .line 75
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_SEND_RGB_AVERAGE:I

    .line 76
    const/4 v9, 0x1

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_ANIMATE_SCR_RGB:I

    .line 77
    const/4 v9, 0x2

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_SET_VIDEO_MODE:I

    .line 78
    const/4 v9, 0x3

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_SET_BROWSER_MODE:I

    .line 79
    const/4 v9, 0x4

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_TERMINATE_VIDEO_MODE:I

    .line 80
    const/4 v9, 0x5

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MSG_TERMINATE_SCR_RGB:I

    .line 82
    const/high16 v9, 0x41a00000    # 20.0f

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->ANIMATION_MAX_COUNT:F

    .line 83
    const v9, 0x989680

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->TEST_RGB_EXPONENTIAL:I

    .line 84
    const/4 v9, 0x2

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->LIGHT_SENSOR_READ_DELAY:I

    .line 85
    const/16 v9, 0x14

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MAX_RGB_SENSOR_COUNT:I

    .line 86
    const/16 v9, 0x24

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->NUMBER_COEFFICIENT_VALUE:I

    .line 88
    const-wide/16 v10, 0x1770

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->RGB_DEBOUNCE_MILLIS:J

    .line 89
    const-wide/16 v10, 0xfa

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->ANIMATION_DEBOUNCE_MILLIS:J

    .line 90
    const-wide/16 v10, 0x12c

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->FOREGROUND_THREAD_DELAY_MILLIS:J

    .line 91
    const-wide/16 v10, 0x1f4

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->VIDEO_MODE_DEBOUNCE_MILLIS:J

    .line 92
    const-wide/16 v10, 0x12c

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->BROWSER_MODE_DEBOUNCE_MILLIS:J

    .line 93
    const-wide/16 v10, 0x2bc

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MULTI_SCREEN_DEBOUNCE_MILLIS:J

    .line 95
    const-string/jumbo v9, "screen_mode_automatic_setting"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SCREEN_MODE_AUTOMATIC_SETTING:Ljava/lang/String;

    .line 96
    const-string/jumbo v9, "screen_mode_setting"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SCREEN_MODE_SETTING:Ljava/lang/String;

    .line 97
    const-string v9, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->ACTION_NOTIFY_MULTIWINDOW_STATUS:Ljava/lang/String;

    .line 98
    const-string v9, "com.sec.android.extra.MULTIWINDOW_RUNNING"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->EXTRA_MULTIWINDOW_RUNNING:Ljava/lang/String;

    .line 99
    const-string v9, "com.sec.android.intent.action.SSRM_MDNIE_CHANGED"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SSRM_INTENT_MDNIE_SETTING:Ljava/lang/String;

    .line 100
    const-string v9, "1"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MDNIE_VIDEO_MODE_NUMBER:Ljava/lang/String;

    .line 101
    const-string v9, "8"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MDNIE_BROWSER_MODE_NUMBER:Ljava/lang/String;

    .line 102
    const-string v9, "9"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MDNIE_READING_MODE_NUMBER:Ljava/lang/String;

    .line 104
    const-string v9, "/sys/class/mdnie/mdnie/sensorRGB"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SCR_FILE_PATH:Ljava/lang/String;

    .line 105
    const-string v9, "/sys/class/mdnie/mdnie/scenario"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SCENARIO_FILE_PATH:Ljava/lang/String;

    .line 106
    const-string v9, "/sys/class/sensors/light_sensor/raw_data"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->LIGHT_SENSOR_RAW_DATA_PATH:Ljava/lang/String;

    .line 107
    const-string v9, "com.sec.android.app.SecSetupWizard"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SETUP_WIZARD_NAME:Ljava/lang/String;

    .line 108
    const-string v9, "com.android.systemui"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->APP_MANAGER_NAME:Ljava/lang/String;

    .line 109
    const-string v9, "com.sec.android.app.sbrowser"

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->SBROWSER_NAME:Ljava/lang/String;

    .line 110
    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "com.sec.android.app.sbrowser"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "com.android.chrome"

    aput-object v11, v9, v10

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->BROWSER_NAMES:[Ljava/lang/String;

    .line 115
    const/16 v9, 0x9

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "com.zgz.supervideo"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "com.kmplayer"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "com.pg.gom"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string v11, "com.gretech.gomplayer"

    aput-object v11, v9, v10

    const/4 v10, 0x4

    const-string v11, "com.mxtech.videoplayer"

    aput-object v11, v9, v10

    const/4 v10, 0x5

    const-string v11, "com.nhn.android.naverplayer"

    aput-object v11, v9, v10

    const/4 v10, 0x6

    const-string v11, "com.inisoft.mediaplayer"

    aput-object v11, v9, v10

    const/4 v10, 0x7

    const-string v11, "com.google.android.youtube"

    aput-object v11, v9, v10

    const/16 v10, 0x8

    const-string v11, "com.google.android.videos"

    aput-object v11, v9, v10

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MEDIA_PLAYER_NAMES:[Ljava/lang/String;

    .line 129
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseAdaptiveDisplayColorServiceConfig:Z

    .line 130
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseEnvironmentDisplayColorConfig:Z

    .line 131
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEnvironmentDisplayColorServiceEnable:Z

    .line 132
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    .line 133
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    .line 143
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAceessibilityEnabled:Z

    .line 144
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    .line 145
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorValueValid:Z

    .line 146
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mValidZone:Z

    .line 147
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isLockScreenOn:Z

    .line 148
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mMultiWindowOn:Z

    .line 149
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForegroundThreadWork:Z

    .line 151
    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverState:Z

    .line 153
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUltraPowerSavingModeEnabled:Z

    .line 154
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenCurtainEnabled:Z

    .line 155
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mNegativeColorEnabled:Z

    .line 156
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mColorBlindEnabled:Z

    .line 157
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAutoModeEnabled:Z

    .line 158
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEBookScenarioIntented:Z

    .line 159
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    .line 160
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mVideoScenarioEnabled:Z

    .line 161
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    .line 163
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingCondition:Z

    .line 164
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPowerSavingEnabled:Z

    .line 165
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mGreyScaleModeEnabled:Z

    .line 167
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenMode:I

    .line 169
    const/4 v9, -0x1

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevContorlZone:I

    .line 171
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    .line 179
    const/4 v9, 0x1

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrR:I

    .line 180
    const/4 v9, 0x1

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrG:I

    .line 181
    const/4 v9, 0x1

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrB:I

    .line 188
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    .line 189
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    .line 190
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    .line 191
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    .line 192
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    .line 193
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    .line 194
    const v9, 0x3d8f5c29    # 0.07f

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbThreshold:F

    .line 196
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    .line 198
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 199
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 200
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    .line 202
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgR:I

    .line 203
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgG:I

    .line 204
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgB:I

    .line 206
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgR:I

    .line 207
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgG:I

    .line 208
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgB:I

    .line 210
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    .line 211
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    .line 212
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    .line 214
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookR:I

    .line 215
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookG:I

    .line 216
    const/16 v9, 0xff

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookB:I

    .line 218
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultAdjustR:I

    .line 219
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultAdjustG:I

    .line 220
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultAdjustB:I

    .line 222
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustR:I

    .line 223
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustG:I

    .line 224
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustB:I

    .line 226
    const/4 v9, 0x0

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    .line 228
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    .line 232
    const/4 v9, 0x0

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 403
    new-instance v9, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;

    invoke-direct {v9, p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mProcessObserver:Landroid/app/IProcessObserver;

    .line 528
    new-instance v9, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$2;

    invoke-direct {v9, p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$2;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbSensorListener:Landroid/hardware/SensorEventListener;

    .line 558
    new-instance v9, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$3;

    invoke-direct {v9, p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$3;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    .line 235
    iput-object p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    .line 237
    new-instance v9, Landroid/os/HandlerThread;

    const-string v10, "AdaptiveDisplayColorServiceThread"

    invoke-direct {v9, v10}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandlerThread:Landroid/os/HandlerThread;

    .line 238
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v9}, Landroid/os/HandlerThread;->start()V

    .line 239
    new-instance v9, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    iget-object v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v10}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v10

    invoke-direct {v9, p0, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Landroid/os/Looper;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    .line 241
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x1120099

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseAdaptiveDisplayColorServiceConfig:Z

    .line 245
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x112009a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    iput-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseEnvironmentDisplayColorConfig:Z

    .line 248
    const-string/jumbo v9, "sys.adaptivedisplay.eadon"

    const-string v10, "false"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    new-instance v9, Landroid/hardware/SystemSensorManager;

    iget-object v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v11}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->getLooper()Landroid/os/Looper;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/content/Context;Landroid/os/Looper;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorManager:Landroid/hardware/SensorManager;

    .line 251
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorManager:Landroid/hardware/SensorManager;

    const/4 v10, 0x5

    invoke-virtual {v9, v10}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbSensor:Landroid/hardware/Sensor;

    .line 253
    new-instance v9, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    iget-object v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-direct {v9, p0, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Landroid/os/Handler;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    .line 255
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 257
    .local v7, "resolver":Landroid/content/ContentResolver;
    new-instance v9, Lcom/samsung/android/cover/CoverManager;

    iget-object v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-direct {v9, v10}, Lcom/samsung/android/cover/CoverManager;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    .line 259
    const-string v9, "lcd_curtain"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 261
    const-string v9, "high_contrast"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 263
    const-string v9, "color_blind"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 266
    const-string/jumbo v9, "powersaving_switch"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 272
    const-string/jumbo v9, "ultra_powersaving_mode"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 274
    const-string/jumbo v9, "screen_mode_automatic_setting"

    invoke-static {v9}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingsObserver:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$SettingsObserver;

    invoke-virtual {v7, v9, v10, v11}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 277
    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    .line 278
    .local v6, "intentFilter":Landroid/content/IntentFilter;
    const-string v9, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 279
    const-string v9, "android.intent.action.SCREEN_ON"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 280
    const-string v9, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 281
    const-string v9, "android.intent.action.USER_PRESENT"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 282
    const-string v9, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 283
    const-string v9, "com.sec.android.intent.action.SSRM_MDNIE_CHANGED"

    invoke-virtual {v6, v9}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 284
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    new-instance v10, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScreenWatchingReceiver;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScreenWatchingReceiver;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;)V

    invoke-virtual {v9, v10, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 286
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    const-string v10, "activity"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager;

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mActivityManager:Landroid/app/ActivityManager;

    .line 288
    iget-boolean v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseEnvironmentDisplayColorConfig:Z

    if-eqz v9, :cond_4

    .line 289
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x1070057

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v0

    .line 290
    .local v0, "adj_rgb":[I
    const/4 v9, 0x0

    aget v9, v0, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustR:I

    .line 291
    const/4 v9, 0x1

    aget v9, v0, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustG:I

    .line 292
    const/4 v9, 0x2

    aget v9, v0, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustB:I

    .line 294
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x1070058

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v8

    .line 295
    .local v8, "test_rgb":[I
    const/4 v9, 0x0

    aget v9, v8, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrR:I

    .line 296
    const/4 v9, 0x1

    aget v9, v8, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrG:I

    .line 297
    const/4 v9, 0x2

    aget v9, v8, v9

    iput v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrB:I

    .line 299
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x10e0099

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 300
    .local v4, "foregroundDelay":I
    const-wide/16 v10, 0x12c

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->foregroundDelayTime:J

    .line 301
    if-eqz v4, :cond_0

    .line 302
    int-to-long v10, v4

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->foregroundDelayTime:J

    .line 304
    :cond_0
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x10e009a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 305
    .local v2, "exitHomeDelay":I
    const-wide/16 v10, 0x12c

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitHomeDelayTime:J

    .line 306
    if-eqz v2, :cond_1

    .line 307
    int-to-long v10, v2

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitHomeDelayTime:J

    .line 309
    :cond_1
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x10e009b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 310
    .local v3, "exitMenuDelay":I
    const-wide/16 v10, 0x2bc

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitMenuDelayTime:J

    .line 311
    if-eqz v3, :cond_2

    .line 312
    int-to-long v10, v3

    iput-wide v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitMenuDelayTime:J

    .line 314
    :cond_2
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x1070059

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 316
    .local v1, "coefficientStringArray":[Ljava/lang/String;
    const/16 v9, 0x24

    new-array v9, v9, [F

    iput-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    .line 318
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    array-length v9, v1

    if-ge v5, v9, :cond_3

    .line 319
    iget-object v9, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    aget-object v10, v1, v5

    invoke-static {v10}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    aput v10, v9, v5

    .line 318
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 321
    :cond_3
    const-string/jumbo v9, "sys.adaptivedisplay.eadon"

    const-string/jumbo v10, "true"

    invoke-static {v9, v10}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .end local v0    # "adj_rgb":[I
    .end local v1    # "coefficientStringArray":[Ljava/lang/String;
    .end local v2    # "exitHomeDelay":I
    .end local v3    # "exitMenuDelay":I
    .end local v4    # "foregroundDelay":I
    .end local v5    # "i":I
    .end local v8    # "test_rgb":[I
    :cond_4
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isLockScreenOn:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Landroid/app/ActivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mActivityManager:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->monitorForegroundBrowser(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    return v0
.end method

.method static synthetic access$1402(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # I

    .prologue
    .line 67
    iput p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    return p1
.end method

.method static synthetic access$1410(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->getRgbFromLightSensor()V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorValueValid:Z

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorR:I

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorG:I

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorB:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->boot_complete()V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;JIIII)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # J
    .param p3, "x2"    # I
    .param p4, "x3"    # I
    .param p5, "x4"    # I
    .param p6, "x5"    # I

    .prologue
    .line 67
    invoke-direct/range {p0 .. p6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->handleRgbSensorEvent(JIIII)V

    return-void
.end method

.method static synthetic access$2102(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverState:Z

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->sendRgbAverage()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->animateScrRGB()V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setVideoMode()V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setBrowserMode()V

    return-void
.end method

.method static synthetic access$2600(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->terminateVideoMode()V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->terminateScrRGB()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->receive_screen_on_intent()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->receive_screen_off_intent()V

    return-void
.end method

.method static synthetic access$502(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mMultiWindowOn:Z

    return p1
.end method

.method static synthetic access$602(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Z

    .prologue
    .line 67
    iput-boolean p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEBookScenarioIntented:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setting_is_changed()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;)Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;
    .param p1, "x1"    # Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    return-object p1
.end method

.method private animateScrRGB()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/high16 v12, 0x41a00000    # 20.0f

    const/16 v11, 0xff

    .line 1026
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isInBoundary()I

    move-result v4

    .line 1027
    .local v4, "state":I
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    .line 1029
    if-lez v4, :cond_1

    .line 1031
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    const/16 v10, 0x14

    if-ne v7, v10, :cond_2

    .line 1033
    iget v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgR:I

    .line 1034
    .local v3, "r":I
    iget v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgG:I

    .line 1035
    .local v1, "g":I
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgB:I

    .line 1037
    .local v0, "b":I
    if-lez v3, :cond_1

    if-gt v3, v11, :cond_1

    if-lez v1, :cond_1

    if-gt v1, v11, :cond_1

    if-lez v0, :cond_1

    if-gt v0, v11, :cond_1

    .line 1038
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    if-ne v3, v7, :cond_0

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    if-ne v1, v7, :cond_0

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    if-eq v0, v7, :cond_1

    .line 1039
    :cond_0
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1040
    .local v6, "str":Ljava/lang/String;
    const-string v7, "/sys/class/mdnie/mdnie/sensorRGB"

    invoke-direct {p0, v7, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->fileWriteString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1041
    iput v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 1042
    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 1043
    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    .line 1078
    .end local v0    # "b":I
    .end local v1    # "g":I
    .end local v3    # "r":I
    .end local v6    # "str":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 1049
    :cond_2
    const/4 v2, 0x0

    .line 1050
    .local v2, "gap":I
    const/4 v5, 0x0

    .line 1051
    .local v5, "step":F
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgR:I

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgR:I

    sub-int v2, v7, v10

    .line 1052
    int-to-float v7, v2

    div-float/2addr v7, v12

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    int-to-float v10, v10

    mul-float v5, v7, v10

    .line 1053
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgR:I

    float-to-int v10, v5

    add-int v3, v7, v10

    .line 1055
    .restart local v3    # "r":I
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgG:I

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgG:I

    sub-int v2, v7, v10

    .line 1056
    int-to-float v7, v2

    div-float/2addr v7, v12

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    int-to-float v10, v10

    mul-float v5, v7, v10

    .line 1057
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgG:I

    float-to-int v10, v5

    add-int v1, v7, v10

    .line 1059
    .restart local v1    # "g":I
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgB:I

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgB:I

    sub-int v2, v7, v10

    .line 1060
    int-to-float v7, v2

    div-float/2addr v7, v12

    iget v10, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    int-to-float v10, v10

    mul-float v5, v7, v10

    .line 1061
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgB:I

    float-to-int v10, v5

    add-int v0, v7, v10

    .line 1063
    .restart local v0    # "b":I
    if-lez v3, :cond_4

    if-gt v3, v11, :cond_4

    if-lez v1, :cond_4

    if-gt v1, v11, :cond_4

    if-lez v0, :cond_4

    if-gt v0, v11, :cond_4

    .line 1064
    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    if-ne v3, v7, :cond_3

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    if-ne v1, v7, :cond_3

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    if-eq v0, v7, :cond_4

    .line 1065
    :cond_3
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v10, " "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1066
    .restart local v6    # "str":Ljava/lang/String;
    const-string v7, "/sys/class/mdnie/mdnie/sensorRGB"

    invoke-direct {p0, v7, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->fileWriteString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    iput v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 1068
    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 1069
    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    .line 1073
    .end local v6    # "str":Ljava/lang/String;
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 1074
    .local v8, "time":J
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v7, v13}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 1075
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    const-wide/16 v10, 0xfa

    add-long/2addr v10, v8

    invoke-virtual {v7, v13, v10, v11}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    goto/16 :goto_0
.end method

.method private boot_complete()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    .line 430
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    .line 432
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustR:I

    add-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookR:I

    .line 433
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustG:I

    add-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookG:I

    .line 434
    iget v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustB:I

    add-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookB:I

    .line 435
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setting_is_changed()V

    .line 436
    return-void
.end method

.method private enableRgbSensor(Z)V
    .locals 10
    .param p1, "enable"    # Z

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x1

    const/16 v7, 0xff

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 499
    new-instance v0, Ljava/io/File;

    const-string v2, "/sys/class/mdnie/mdnie/sensorRGB"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 500
    .local v0, "file":Ljava/io/File;
    const/4 v1, 0x0

    .line 501
    .local v1, "white_scr":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 502
    const-string v2, "AdaptiveDisplayColorService"

    const-string v3, "StatFs returns null."

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 505
    const-string v2, "AdaptiveDisplayColorService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableRgbSensor : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 507
    :cond_1
    if-eqz p1, :cond_2

    .line 508
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->initRgbAverage()V

    .line 509
    iput-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    .line 510
    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevContorlZone:I

    .line 511
    iput v5, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    .line 512
    iput v5, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    .line 513
    iput v5, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    .line 514
    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorDelay:I

    .line 515
    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbSensorListener:Landroid/hardware/SensorEventListener;

    iget-object v4, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbSensor:Landroid/hardware/Sensor;

    iget-object v5, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v2, v3, v4, v9, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    .line 525
    :goto_0
    iput-boolean p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    .line 526
    return-void

    .line 517
    :cond_2
    iput v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 518
    iput v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 519
    iput v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    .line 520
    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v2, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 521
    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v2, v8}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 522
    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v2, v9}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 523
    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbSensorListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    goto :goto_0
.end method

.method private fileWriteString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "str"    # Ljava/lang/String;

    .prologue
    .line 1097
    const/4 v2, 0x0

    .line 1098
    .local v2, "out":Ljava/io/FileOutputStream;
    iget-boolean v4, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    if-eqz v4, :cond_0

    .line 1099
    const-string v4, "AdaptiveDisplayColorService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fileWriteString : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  value : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1103
    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1109
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .local v3, "out":Ljava/io/FileOutputStream;
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 1110
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    move-object v2, v3

    .line 1119
    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 1104
    :catch_0
    move-exception v0

    .line 1105
    .local v0, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    const-string v4, "AdaptiveDisplayColorService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "fileWriteString : file not found : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 1111
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v0

    .line 1112
    .local v0, "e":Ljava/io/IOException;
    :goto_1
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 1114
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 1115
    :catch_2
    move-exception v1

    .line 1116
    .local v1, "err":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1111
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "err":Ljava/lang/Exception;
    .end local v2    # "out":Ljava/io/FileOutputStream;
    .restart local v3    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v0

    move-object v2, v3

    .end local v3    # "out":Ljava/io/FileOutputStream;
    .restart local v2    # "out":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method private getRgbFromLightSensor()V
    .locals 5

    .prologue
    .line 664
    const/4 v1, 0x0

    .line 666
    .local v1, "raw":Ljava/lang/String;
    :try_start_0
    const-string v3, "/sys/class/sensors/light_sensor/raw_data"

    invoke-direct {p0, v3}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->getStringFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 667
    if-eqz v1, :cond_0

    .line 668
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 669
    .local v2, "rawDatas":[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorR:I

    .line 670
    const/4 v3, 0x1

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorG:I

    .line 671
    const/4 v3, 0x2

    aget-object v3, v2, v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLightSensorB:I

    .line 672
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorValueValid:Z

    .line 679
    .end local v2    # "rawDatas":[Ljava/lang/String;
    :goto_0
    return-void

    .line 675
    :cond_0
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorValueValid:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 676
    :catch_0
    move-exception v0

    .line 677
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private getStringFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "fileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x80

    const/4 v10, 0x0

    .line 682
    const/4 v5, 0x0

    .line 683
    .local v5, "in":Ljava/io/InputStream;
    const/16 v0, 0x80

    .line 684
    .local v0, "MAX_BUFFER_SIZE":I
    new-array v1, v11, [B

    .line 685
    .local v1, "buffer":[B
    const/4 v8, 0x0

    .line 686
    .local v8, "value":Ljava/lang/String;
    const/4 v7, 0x0

    .line 688
    .local v7, "length":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v11, :cond_0

    .line 689
    aput-byte v10, v1, v4

    .line 688
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 692
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/FileInputStream;

    new-instance v10, Ljava/io/File;

    invoke-direct {v10, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v10}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 693
    .end local v5    # "in":Ljava/io/InputStream;
    .local v6, "in":Ljava/io/InputStream;
    if-eqz v6, :cond_2

    .line 694
    :try_start_1
    invoke-virtual {v6, v1}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .line 695
    if-eqz v7, :cond_1

    .line 696
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    add-int/lit8 v11, v7, -0x1

    invoke-direct {v9, v1, v10, v11}, Ljava/lang/String;-><init>([BII)V

    .end local v8    # "value":Ljava/lang/String;
    .local v9, "value":Ljava/lang/String;
    move-object v8, v9

    .line 698
    .end local v9    # "value":Ljava/lang/String;
    .restart local v8    # "value":Ljava/lang/String;
    :cond_1
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 707
    :cond_2
    if-eqz v6, :cond_5

    .line 709
    :try_start_2
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-object v5, v6

    .line 715
    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :cond_3
    :goto_1
    return-object v8

    .line 710
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catch_0
    move-exception v3

    .line 711
    .local v3, "ee":Ljava/io/IOException;
    const-string v10, "AdaptiveDisplayColorService"

    const-string v11, "File Close error"

    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v5, v6

    .line 712
    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_1

    .line 700
    .end local v3    # "ee":Ljava/io/IOException;
    :catch_1
    move-exception v10

    .line 707
    :goto_2
    if-eqz v5, :cond_3

    .line 709
    :try_start_3
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 710
    :catch_2
    move-exception v3

    .line 711
    .restart local v3    # "ee":Ljava/io/IOException;
    const-string v10, "AdaptiveDisplayColorService"

    const-string v11, "File Close error"

    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 703
    .end local v3    # "ee":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 704
    .local v2, "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 705
    const-string v10, "AdaptiveDisplayColorService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "IOException : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 707
    if-eqz v5, :cond_3

    .line 709
    :try_start_5
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_1

    .line 710
    :catch_4
    move-exception v3

    .line 711
    .restart local v3    # "ee":Ljava/io/IOException;
    const-string v10, "AdaptiveDisplayColorService"

    const-string v11, "File Close error"

    invoke-static {v10, v11}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 707
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "ee":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    :goto_4
    if-eqz v5, :cond_4

    .line 709
    :try_start_6
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .line 712
    :cond_4
    :goto_5
    throw v10

    .line 710
    :catch_5
    move-exception v3

    .line 711
    .restart local v3    # "ee":Ljava/io/IOException;
    const-string v11, "AdaptiveDisplayColorService"

    const-string v12, "File Close error"

    invoke-static {v11, v12}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    .line 707
    .end local v3    # "ee":Ljava/io/IOException;
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catchall_1
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_4

    .line 703
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catch_6
    move-exception v2

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 700
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catch_7
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_2

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :cond_5
    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_1
.end method

.method private handleRgbSensorEvent(JIIII)V
    .locals 3
    .param p1, "time"    # J
    .param p3, "r"    # I
    .param p4, "g"    # I
    .param p5, "b"    # I
    .param p6, "lux"    # I

    .prologue
    .line 719
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isInBoundary()I

    move-result v0

    .line 720
    .local v0, "isInControlZone":I
    iget v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevContorlZone:I

    if-eq v1, v0, :cond_0

    .line 721
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 722
    iput-wide p1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastChangedRgbTime:J

    .line 723
    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevContorlZone:I

    .line 724
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->initRgbAverage()V

    .line 727
    :cond_0
    if-lez v0, :cond_1

    .line 728
    invoke-direct {p0, p3, p4, p5, p6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->setAverageValue(IIII)V

    .line 730
    :cond_1
    return-void
.end method

.method private initRgbAverage()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 744
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    iput v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    .line 745
    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    .line 746
    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    .line 747
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mValidZone:Z

    .line 748
    return-void
.end method

.method private isInBoundary()I
    .locals 1

    .prologue
    .line 735
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    if-eqz v0, :cond_0

    .line 736
    const/4 v0, 0x2

    .line 740
    :goto_0
    return v0

    .line 737
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-eqz v0, :cond_1

    .line 738
    const/4 v0, 0x1

    goto :goto_0

    .line 740
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private max_num(II)I
    .locals 0
    .param p1, "a"    # I
    .param p2, "b"    # I

    .prologue
    .line 1131
    if-lt p1, p2, :cond_0

    .line 1134
    .end local p1    # "a":I
    :goto_0
    return p1

    .restart local p1    # "a":I
    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method private min_num(II)I
    .locals 0
    .param p1, "a"    # I
    .param p2, "b"    # I

    .prologue
    .line 1123
    if-ge p1, p2, :cond_0

    .line 1126
    .end local p1    # "a":I
    :goto_0
    return p1

    .restart local p1    # "a":I
    :cond_0
    move p1, p2

    goto :goto_0
.end method

.method private monitorForegroundBrowser(Ljava/lang/String;)V
    .locals 13
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x2

    const/4 v10, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 567
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isLockScreenOn:Z

    if-eqz v8, :cond_2

    .line 568
    const-string v7, "com.sec.android.app.SecSetupWizard"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 569
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->isLockScreenOn:Z

    .line 570
    :cond_0
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-eqz v7, :cond_1

    .line 571
    invoke-direct {p0, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    .line 661
    :cond_1
    :goto_0
    return-void

    .line 574
    :cond_2
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mMultiWindowOn:Z

    if-nez v8, :cond_3

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverState:Z

    if-nez v8, :cond_5

    .line 575
    :cond_3
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-eqz v7, :cond_1

    .line 576
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    .line 577
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-eqz v7, :cond_4

    .line 578
    invoke-direct {p0, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    .line 579
    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 580
    .local v4, "time":J
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v6, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 581
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v6, v10, v4, v5}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_0

    .line 585
    .end local v4    # "time":J
    :cond_5
    const/4 v1, 0x0

    .line 586
    .local v1, "isBrowser":Z
    const/4 v2, 0x0

    .line 587
    .local v2, "isReading":Z
    const/4 v3, 0x0

    .line 589
    .local v3, "isVideo":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MEDIA_PLAYER_NAMES:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_6

    .line 590
    iget-object v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->MEDIA_PLAYER_NAMES:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {p1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 591
    const/4 v3, 0x1

    .line 596
    :cond_6
    const/4 v0, 0x0

    :goto_2
    iget-object v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->BROWSER_NAMES:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_9

    .line 597
    iget-object v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->BROWSER_NAMES:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 598
    const/4 v1, 0x1

    .line 596
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 589
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 602
    :cond_9
    const-string/jumbo v8, "sys.ssrm.mdnie"

    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "9"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 603
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEBookScenarioIntented:Z

    or-int/2addr v2, v8

    .line 605
    if-eqz v2, :cond_c

    .line 606
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    if-nez v8, :cond_1

    .line 607
    iput-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    .line 608
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseEnvironmentDisplayColorConfig:Z

    if-eqz v8, :cond_b

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    if-eqz v8, :cond_b

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    if-nez v8, :cond_a

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-eqz v8, :cond_b

    :cond_a
    move v6, v7

    :cond_b
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEnvironmentDisplayColorServiceEnable:Z

    .line 609
    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEnvironmentDisplayColorServiceEnable:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-nez v6, :cond_1

    .line 610
    invoke-direct {p0, v7}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    goto/16 :goto_0

    .line 614
    :cond_c
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    if-eqz v8, :cond_d

    .line 615
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    .line 616
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-eqz v7, :cond_1

    .line 617
    invoke-direct {p0, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    goto/16 :goto_0

    .line 620
    :cond_d
    if-eqz v3, :cond_e

    .line 621
    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mVideoScenarioEnabled:Z

    if-nez v6, :cond_1

    .line 622
    iput-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mVideoScenarioEnabled:Z

    .line 623
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 624
    .restart local v4    # "time":J
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v6, v11}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 625
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    const-wide/16 v8, 0x1f4

    add-long/2addr v8, v4

    invoke-virtual {v6, v11, v8, v9}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    goto/16 :goto_0

    .line 627
    .end local v4    # "time":J
    :cond_e
    if-eqz v1, :cond_12

    .line 628
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-nez v8, :cond_1

    .line 629
    iput-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    .line 630
    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUseEnvironmentDisplayColorConfig:Z

    if-eqz v8, :cond_10

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    if-eqz v8, :cond_10

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    if-nez v8, :cond_f

    iget-boolean v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-eqz v8, :cond_10

    :cond_f
    move v6, v7

    :cond_10
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEnvironmentDisplayColorServiceEnable:Z

    .line 631
    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEnvironmentDisplayColorServiceEnable:Z

    if-eqz v6, :cond_11

    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-nez v6, :cond_11

    .line 632
    invoke-direct {p0, v7}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    .line 634
    :cond_11
    const/16 v6, 0x8

    invoke-static {v6}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    goto/16 :goto_0

    .line 637
    :cond_12
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mVideoScenarioEnabled:Z

    if-eqz v7, :cond_14

    .line 638
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mVideoScenarioEnabled:Z

    .line 639
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 640
    .restart local v4    # "time":J
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v7, v12}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 641
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v7, v12, v4, v5}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    .line 654
    .end local v4    # "time":J
    :cond_13
    :goto_3
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-eqz v7, :cond_1

    .line 655
    invoke-direct {p0, v6}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    goto/16 :goto_0

    .line 642
    :cond_14
    iget-boolean v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    if-eqz v7, :cond_13

    .line 643
    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    .line 644
    const-string v7, "com.android.systemui"

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 645
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 646
    .restart local v4    # "time":J
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v7, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 647
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    iget-wide v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitMenuDelayTime:J

    add-long/2addr v8, v4

    invoke-virtual {v7, v10, v8, v9}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_3

    .line 649
    .end local v4    # "time":J
    :cond_15
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 650
    .restart local v4    # "time":J
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v7, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 651
    iget-object v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    iget-wide v8, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->exitHomeDelayTime:J

    add-long/2addr v8, v4

    invoke-virtual {v7, v10, v8, v9}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    goto :goto_3
.end method

.method private receive_screen_off_intent()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 451
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    .line 452
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingCondition:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    .line 453
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSensorEnabled:Z

    if-eqz v0, :cond_0

    .line 454
    invoke-direct {p0, v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->enableRgbSensor(Z)V

    .line 456
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    if-eqz v0, :cond_1

    .line 457
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->interrupt()V

    .line 461
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 452
    goto :goto_0
.end method

.method private receive_screen_on_intent()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 439
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    .line 440
    iput-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    .line 441
    iget-boolean v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingCondition:Z

    if-eqz v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    .line 442
    iget-boolean v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    if-nez v0, :cond_0

    .line 444
    new-instance v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;)V

    iput-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    .line 445
    iget-object v0, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->start()V

    .line 448
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 441
    goto :goto_0
.end method

.method private sendForcedRGB(IIII)V
    .locals 3
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "lux"    # I

    .prologue
    .line 751
    add-int v1, p1, p2

    add-int v0, v1, p3

    .line 753
    .local v0, "sumRGB":I
    int-to-float v1, p1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    .line 754
    int-to-float v1, p2

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    .line 755
    int-to-float v1, p3

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    .line 756
    iput p4, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    .line 757
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    .line 759
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->sendRgbAverage()V

    .line 760
    return-void
.end method

.method private sendRgbAverage()V
    .locals 31

    .prologue
    .line 832
    const/16 v22, 0x0

    .local v22, "scrR":I
    const/16 v21, 0x0

    .local v21, "scrG":I
    const/16 v19, 0x0

    .line 833
    .local v19, "scrB":I
    const/16 v18, 0x0

    .local v18, "scr400LuxR":I
    const/16 v17, 0x0

    .local v17, "scr400LuxG":I
    const/16 v16, 0x0

    .line 835
    .local v16, "scr400LuxB":I
    const/4 v5, 0x0

    .local v5, "adjustR":I
    const/4 v4, 0x0

    .local v4, "adjustG":I
    const/4 v3, 0x0

    .line 837
    .local v3, "adjustB":I
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    .line 838
    .local v11, "isEbookmode":Z
    const/16 v27, 0x0

    .line 840
    .local v27, "white_scr":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    move/from16 v28, v0

    if-lez v28, :cond_7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    .line 841
    .local v8, "count":I
    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    move/from16 v28, v0

    int-to-float v0, v8

    move/from16 v29, v0

    div-float v15, v28, v29

    .line 842
    .local v15, "r":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    move/from16 v28, v0

    int-to-float v0, v8

    move/from16 v29, v0

    div-float v10, v28, v29

    .line 843
    .local v10, "g":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    move/from16 v28, v0

    int-to-float v0, v8

    move/from16 v29, v0

    div-float v7, v28, v29

    .line 844
    .local v7, "b":F
    move-object/from16 v0, p0

    iput v15, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    .line 845
    move-object/from16 v0, p0

    iput v10, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    .line 846
    move-object/from16 v0, p0

    iput v7, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    .line 847
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    move/from16 v28, v0

    div-int v6, v28, v8

    .line 849
    .local v6, "avgLux":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    move/from16 v28, v0

    if-eqz v28, :cond_0

    .line 850
    const-string v28, "AdaptiveDisplayColorService"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v30, "AvgR : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", AvgG : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", AvgB : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    move/from16 v30, v0

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", avg lux : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 853
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->initRgbAverage()V

    .line 856
    :try_start_0
    const-string v28, "/sys/class/mdnie/mdnie/sensorRGB"

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    invoke-direct {v0, v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->getStringFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 857
    if-eqz v27, :cond_1

    .line 858
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v28

    const-string v29, " "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 859
    .local v20, "scrDatas":[Ljava/lang/String;
    const/16 v28, 0x0

    aget-object v28, v20, v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 860
    const/16 v28, 0x1

    aget-object v28, v20, v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 861
    const/16 v28, 0x2

    aget-object v28, v20, v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v28

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 866
    .end local v20    # "scrDatas":[Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 867
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    .line 868
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    .line 869
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    .line 873
    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    move/from16 v28, v0

    if-eqz v28, :cond_3

    .line 874
    if-eqz v11, :cond_9

    .line 875
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookR:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    .line 876
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookG:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    .line 877
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookB:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    .line 883
    :goto_2
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    .line 886
    :cond_3
    move v13, v6

    .line 887
    .local v13, "luxValue":I
    const/4 v12, -0x1

    .line 889
    .local v12, "log2Lux":I
    const/16 v28, 0x4

    move/from16 v0, v28

    if-ge v13, v0, :cond_b

    .line 890
    if-eqz v11, :cond_a

    .line 891
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookR:I

    move/from16 v22, v0

    .line 892
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookG:I

    move/from16 v21, v0

    .line 893
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookB:I

    move/from16 v19, v0

    .line 1005
    :goto_3
    if-eqz v22, :cond_6

    if-eqz v21, :cond_6

    if-eqz v19, :cond_6

    .line 1007
    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgR:I

    .line 1008
    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgG:I

    .line 1009
    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mFinalIntAvgB:I

    .line 1010
    const/16 v28, 0x0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountAnimationValue:I

    .line 1012
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->DEBUG:Z

    move/from16 v28, v0

    if-eqz v28, :cond_4

    .line 1013
    const-string v28, "AdaptiveDisplayColorService"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v30, "scrR : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", scrG : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ", scrB : "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v28 .. v29}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    move/from16 v28, v0

    move/from16 v0, v22

    move/from16 v1, v28

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    move/from16 v28, v0

    move/from16 v0, v21

    move/from16 v1, v28

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    move/from16 v28, v0

    move/from16 v0, v19

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    .line 1016
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgR:I

    .line 1017
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgG:I

    .line 1018
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTempIntAvgB:I

    .line 1019
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->animateScrRGB()V

    .line 1022
    :cond_6
    return-void

    .line 840
    .end local v6    # "avgLux":I
    .end local v7    # "b":F
    .end local v8    # "count":I
    .end local v10    # "g":F
    .end local v12    # "log2Lux":I
    .end local v13    # "luxValue":I
    .end local v15    # "r":F
    :cond_7
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 863
    .restart local v6    # "avgLux":I
    .restart local v7    # "b":F
    .restart local v8    # "count":I
    .restart local v10    # "g":F
    .restart local v15    # "r":F
    :catch_0
    move-exception v9

    .line 864
    .local v9, "e":Ljava/io/IOException;
    :try_start_1
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 866
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 867
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    .line 868
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    .line 869
    const/16 v28, 0xff

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    goto/16 :goto_1

    .line 866
    .end local v9    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v28

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mIsFirstStart:Z

    move/from16 v29, v0

    if-eqz v29, :cond_8

    .line 867
    const/16 v29, 0xff

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    .line 868
    const/16 v29, 0xff

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    .line 869
    const/16 v29, 0xff

    move/from16 v0, v29

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    :cond_8
    throw v28

    .line 879
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgR:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    .line 880
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgG:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    .line 881
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPrevIntAvgB:I

    move/from16 v28, v0

    move/from16 v0, v28

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    goto/16 :goto_2

    .line 895
    .restart local v12    # "log2Lux":I
    .restart local v13    # "luxValue":I
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    move/from16 v22, v0

    .line 896
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    move/from16 v21, v0

    .line 897
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    move/from16 v19, v0

    goto/16 :goto_3

    .line 901
    :cond_b
    :goto_4
    if-eqz v13, :cond_c

    .line 902
    shr-int/lit8 v13, v13, 0x1

    .line 903
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    .line 906
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    move/from16 v28, v0

    if-nez v28, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mBrowserScenarioEnabled:Z

    move/from16 v28, v0

    if-eqz v28, :cond_13

    .line 907
    :cond_d
    if-eqz v11, :cond_e

    .line 908
    move-object/from16 v0, p0

    iget v5, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustR:I

    .line 909
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustG:I

    .line 910
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookAdjustB:I

    .line 917
    :goto_5
    const v28, 0x4b189680    # 1.0E7f

    mul-float v28, v28, v15

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrR:I

    move/from16 v29, v0

    div-int v26, v28, v29

    .line 918
    .local v26, "testR":I
    const v28, 0x4b189680    # 1.0E7f

    mul-float v28, v28, v10

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrG:I

    move/from16 v29, v0

    div-int v25, v28, v29

    .line 919
    .local v25, "testG":I
    const v28, 0x4b189680    # 1.0E7f

    mul-float v28, v28, v7

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mTestScrB:I

    move/from16 v29, v0

    div-int v24, v28, v29

    .line 921
    .local v24, "testB":I
    move-object/from16 v0, p0

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v14

    .line 922
    .local v14, "maxValue":I
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v14, v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v14

    .line 924
    move/from16 v0, v26

    if-ne v14, v0, :cond_f

    .line 925
    const/16 v22, 0xff

    .line 927
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0x0

    aget v28, v28, v29

    mul-float v28, v28, v15

    mul-float v28, v28, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1

    aget v29, v29, v30

    mul-float v29, v29, v10

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x2

    aget v29, v29, v30

    mul-float v29, v29, v15

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x3

    aget v29, v29, v30

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x4

    aget v29, v29, v30

    mul-float v29, v29, v15

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x5

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 929
    .local v23, "temp":F
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v21, v0

    .line 930
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v21

    .line 932
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0x6

    aget v28, v28, v29

    mul-float v28, v28, v15

    mul-float v28, v28, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x7

    aget v29, v29, v30

    mul-float v29, v29, v7

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x8

    aget v29, v29, v30

    mul-float v29, v29, v15

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x9

    aget v29, v29, v30

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0xa

    aget v29, v29, v30

    mul-float v29, v29, v15

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0xb

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 934
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v19, v0

    .line 935
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v19

    .line 964
    :goto_6
    add-int v18, v22, v5

    .line 965
    add-int v17, v21, v4

    .line 966
    add-int v16, v19, v3

    .line 968
    const/16 v28, 0x190

    move/from16 v0, v28

    if-ge v6, v0, :cond_12

    .line 970
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mEbookScenarioEnabled:Z

    move/from16 v28, v0

    if-eqz v28, :cond_11

    .line 971
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookR:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v18

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v22, v28, 0x8

    .line 972
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v22

    .line 974
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookG:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v17

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v21, v28, 0x8

    .line 975
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v21

    .line 977
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultEbookB:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v16

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v19, v28, 0x8

    .line 978
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v19

    goto/16 :goto_3

    .line 912
    .end local v14    # "maxValue":I
    .end local v23    # "temp":F
    .end local v24    # "testB":I
    .end local v25    # "testG":I
    .end local v26    # "testR":I
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    move/from16 v28, v0

    move/from16 v0, v28

    add-int/lit16 v5, v0, -0xff

    .line 913
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    move/from16 v28, v0

    move/from16 v0, v28

    add-int/lit16 v4, v0, -0xff

    .line 914
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    move/from16 v28, v0

    move/from16 v0, v28

    add-int/lit16 v3, v0, -0xff

    goto/16 :goto_5

    .line 937
    .restart local v14    # "maxValue":I
    .restart local v24    # "testB":I
    .restart local v25    # "testG":I
    .restart local v26    # "testR":I
    :cond_f
    move/from16 v0, v24

    if-ne v14, v0, :cond_10

    .line 938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0xc

    aget v28, v28, v29

    mul-float v28, v28, v15

    mul-float v28, v28, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0xd

    aget v29, v29, v30

    mul-float v29, v29, v7

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0xe

    aget v29, v29, v30

    mul-float v29, v29, v15

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0xf

    aget v29, v29, v30

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x10

    aget v29, v29, v30

    mul-float v29, v29, v15

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x11

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 940
    .restart local v23    # "temp":F
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v22, v0

    .line 941
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v22

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0x12

    aget v28, v28, v29

    mul-float v28, v28, v10

    mul-float v28, v28, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x13

    aget v29, v29, v30

    mul-float v29, v29, v7

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x14

    aget v29, v29, v30

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x15

    aget v29, v29, v30

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x16

    aget v29, v29, v30

    mul-float v29, v29, v10

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x17

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 945
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v21, v0

    .line 946
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v21

    .line 948
    const/16 v19, 0xff

    goto/16 :goto_6

    .line 951
    .end local v23    # "temp":F
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0x18

    aget v28, v28, v29

    mul-float v28, v28, v15

    mul-float v28, v28, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x19

    aget v29, v29, v30

    mul-float v29, v29, v10

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1a

    aget v29, v29, v30

    mul-float v29, v29, v15

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1b

    aget v29, v29, v30

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1c

    aget v29, v29, v30

    mul-float v29, v29, v15

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1d

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 953
    .restart local v23    # "temp":F
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v22, v0

    .line 954
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v22

    .line 956
    const/16 v21, 0xff

    .line 958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v28, v0

    const/16 v29, 0x1e

    aget v28, v28, v29

    mul-float v28, v28, v10

    mul-float v28, v28, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x1f

    aget v29, v29, v30

    mul-float v29, v29, v7

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x20

    aget v29, v29, v30

    mul-float v29, v29, v10

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x21

    aget v29, v29, v30

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x22

    aget v29, v29, v30

    mul-float v29, v29, v10

    mul-float v29, v29, v7

    add-float v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoefficientValueArray:[F

    move-object/from16 v29, v0

    const/16 v30, 0x23

    aget v29, v29, v30

    add-float v23, v28, v29

    .line 960
    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v28, v28, v23

    move/from16 v0, v28

    float-to-int v0, v0

    move/from16 v19, v0

    .line 961
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v19

    goto/16 :goto_6

    .line 980
    :cond_11
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultR:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v18

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v22, v28, 0x8

    .line 981
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v22

    .line 983
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultG:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v17

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v21, v28, 0x8

    .line 984
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v21

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v21

    .line 986
    rsub-int/lit8 v28, v12, 0x8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mDefaultB:I

    move/from16 v29, v0

    mul-int v28, v28, v29

    mul-int v29, v12, v16

    add-int v28, v28, v29

    add-int/lit8 v28, v28, 0x4

    div-int/lit8 v19, v28, 0x8

    .line 987
    const/16 v28, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->max_num(II)I

    move-result v28

    const/16 v29, 0xff

    move-object/from16 v0, p0

    move/from16 v1, v28

    move/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->min_num(II)I

    move-result v19

    goto/16 :goto_3

    .line 992
    :cond_12
    move/from16 v22, v18

    .line 993
    move/from16 v21, v17

    .line 994
    move/from16 v19, v16

    goto/16 :goto_3

    .line 999
    .end local v14    # "maxValue":I
    .end local v23    # "temp":F
    .end local v24    # "testB":I
    .end local v25    # "testG":I
    .end local v26    # "testR":I
    :cond_13
    const/16 v22, 0x0

    .line 1000
    const/16 v21, 0x0

    .line 1001
    const/16 v19, 0x0

    goto/16 :goto_3
.end method

.method private setAverageValue(IIII)V
    .locals 11
    .param p1, "r"    # I
    .param p2, "g"    # I
    .param p3, "b"    # I
    .param p4, "lux"    # I

    .prologue
    const/4 v10, 0x0

    .line 763
    add-int v6, p1, p2

    add-int v3, v6, p3

    .line 764
    .local v3, "sumRGB":I
    const/4 v2, 0x0

    .local v2, "ratioR":F
    const/4 v1, 0x0

    .local v1, "ratioG":F
    const/4 v0, 0x0

    .line 766
    .local v0, "ratioB":F
    int-to-float v6, p1

    int-to-float v7, v3

    div-float v2, v6, v7

    .line 767
    int-to-float v6, p2

    int-to-float v7, v3

    div-float v1, v6, v7

    .line 768
    int-to-float v6, p3

    int-to-float v7, v3

    div-float v0, v6, v7

    .line 770
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    sub-float v6, v2, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbThreshold:F

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_0

    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    sub-float v6, v1, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbThreshold:F

    cmpl-float v6, v6, v7

    if-gtz v6, :cond_0

    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    sub-float v6, v0, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mRgbThreshold:F

    cmpl-float v6, v6, v7

    if-lez v6, :cond_3

    .line 771
    :cond_0
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    add-float/2addr v6, v2

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    .line 772
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    add-float/2addr v6, v1

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    .line 773
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    add-float/2addr v6, v0

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    .line 774
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    add-int/2addr v6, p4

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSumLux:I

    .line 775
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    .line 777
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    const/16 v7, 0x14

    if-lt v6, v7, :cond_1

    .line 778
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgR:F

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgR:F

    .line 779
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgG:F

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgG:F

    .line 780
    iget v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAvgB:F

    iget v7, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCountSensorValue:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    iput v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mLastAvgB:F

    .line 782
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->initRgbAverage()V

    .line 785
    :cond_1
    iget-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mValidZone:Z

    if-nez v6, :cond_2

    .line 786
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 787
    .local v4, "time":J
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mValidZone:Z

    .line 788
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v6, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    .line 789
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    const-wide/16 v8, 0x1770

    add-long/2addr v8, v4

    invoke-virtual {v6, v10, v8, v9}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->sendEmptyMessageAtTime(IJ)Z

    .line 796
    .end local v4    # "time":J
    :cond_2
    :goto_0
    return-void

    .line 793
    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->initRgbAverage()V

    .line 794
    iget-object v6, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mHandler:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;

    invoke-virtual {v6, v10}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ScrControlHandler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setBrowserMode()V
    .locals 1

    .prologue
    .line 1085
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 1086
    return-void
.end method

.method private setVideoMode()V
    .locals 1

    .prologue
    .line 1081
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 1082
    return-void
.end method

.method private setting_is_changed()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 464
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 466
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v1, "lcd_curtain"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenCurtainEnabled:Z

    .line 467
    const-string v1, "high_contrast"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_3

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mNegativeColorEnabled:Z

    .line 468
    const-string v1, "color_blind"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mColorBlindEnabled:Z

    .line 470
    const-string/jumbo v1, "powersaving_switch"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_5

    move v1, v2

    :goto_3
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPowerSavingEnabled:Z

    .line 474
    const-string/jumbo v1, "ultra_powersaving_mode"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_4
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUltraPowerSavingModeEnabled:Z

    .line 475
    const-string/jumbo v1, "screen_mode_automatic_setting"

    invoke-static {v0, v1, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v2, :cond_7

    move v1, v2

    :goto_5
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAutoModeEnabled:Z

    .line 476
    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenCurtainEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mNegativeColorEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mColorBlindEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mUltraPowerSavingModeEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mPowerSavingEnabled:Z

    if-nez v1, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mAutoModeEnabled:Z

    if-eqz v1, :cond_8

    move v1, v2

    :goto_6
    iput-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingCondition:Z

    .line 478
    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mScreenStateOn:Z

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mSettingCondition:Z

    if-eqz v1, :cond_9

    :goto_7
    iput-boolean v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    .line 480
    iget-boolean v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mThreadEnableCondition:Z

    if-eqz v1, :cond_a

    .line 481
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    if-eqz v1, :cond_0

    .line 482
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/cover/CoverManager;->registerListener(Lcom/samsung/android/cover/CoverManager$StateListener;)V

    .line 483
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    if-nez v1, :cond_1

    .line 484
    new-instance v1, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;-><init>(Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$1;)V

    iput-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    .line 485
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->start()V

    .line 496
    :cond_1
    :goto_8
    return-void

    :cond_2
    move v1, v3

    .line 466
    goto/16 :goto_0

    :cond_3
    move v1, v3

    .line 467
    goto :goto_1

    :cond_4
    move v1, v3

    .line 468
    goto :goto_2

    :cond_5
    move v1, v3

    .line 470
    goto :goto_3

    :cond_6
    move v1, v3

    .line 474
    goto :goto_4

    :cond_7
    move v1, v3

    .line 475
    goto :goto_5

    :cond_8
    move v1, v3

    .line 476
    goto :goto_6

    :cond_9
    move v2, v3

    .line 478
    goto :goto_7

    .line 488
    :cond_a
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    if-eqz v1, :cond_b

    .line 489
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverManager:Lcom/samsung/android/cover/CoverManager;

    iget-object v2, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mCoverStateListener:Lcom/samsung/android/cover/CoverManager$StateListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/cover/CoverManager;->unregisterListener(Lcom/samsung/android/cover/CoverManager$StateListener;)V

    .line 490
    :cond_b
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    if-eqz v1, :cond_1

    .line 491
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 492
    iget-object v1, p0, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService;->mForeGroundThread:Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;

    invoke-virtual {v1}, Lcom/samsung/android/mdnie/AdaptiveDisplayColorService$ForeGroundThread;->interrupt()V

    goto :goto_8
.end method

.method private terminateScrRGB()V
    .locals 1

    .prologue
    .line 1093
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 1094
    return-void
.end method

.method private terminateVideoMode()V
    .locals 1

    .prologue
    .line 1089
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 1090
    return-void
.end method
