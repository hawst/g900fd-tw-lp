.class public Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;
.super Lcom/samsung/surfacewidget/gl/Program;
.source "ProgramDisplayImage.java"


# instance fields
.field private final mFragmentShader:Ljava/lang/String;

.field mTextureCoords:Ljava/nio/FloatBuffer;

.field mTriangleVertices:Ljava/nio/FloatBuffer;

.field private final mVertexShader:Ljava/lang/String;

.field maPositionHandle:I

.field maTexCoordsHandle:I

.field msTextureHandle:I

.field muAlphaHandle:I

.field muModelMatrixHandle:I

.field muPosOffsetHandle:I

.field muPosScaleHandle:I

.field muProjectionMatrixHandle:I

.field muTexOffsetHandle:I

.field muTexScaleHandle:I

.field muViewMatrixHandle:I

.field muZHandle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6
    invoke-direct {p0}, Lcom/samsung/surfacewidget/gl/Program;-><init>()V

    .line 8
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maPositionHandle:I

    .line 9
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maTexCoordsHandle:I

    .line 10
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muAlphaHandle:I

    .line 11
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosScaleHandle:I

    .line 12
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosOffsetHandle:I

    .line 13
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexScaleHandle:I

    .line 14
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexOffsetHandle:I

    .line 15
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->msTextureHandle:I

    .line 16
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muZHandle:I

    .line 18
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muProjectionMatrixHandle:I

    .line 19
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muViewMatrixHandle:I

    .line 20
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muModelMatrixHandle:I

    .line 22
    sget-object v0, Lcom/samsung/surfacewidget/gl/ShaderUtil;->mQuadVertices:[F

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->allocBufferForAttribute([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mTriangleVertices:Ljava/nio/FloatBuffer;

    .line 23
    sget-object v0, Lcom/samsung/surfacewidget/gl/ShaderUtil;->mQuadTextureCoordinates:[F

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->allocBufferForAttribute([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mTextureCoords:Ljava/nio/FloatBuffer;

    .line 93
    const-string v0, "precision mediump float;\nattribute vec3 aPosition;\nattribute vec2 aTexCoords;\nuniform mat4 proj_matrix;\nuniform mat4 view_matrix;\nuniform mat4 model_matrix;\nuniform vec3 posScale;\nuniform vec3 posOffset;\nuniform vec2 texScale;\nuniform vec2 texOffset;\nuniform float z;\nvarying vec2  vTexCoord;\nvoid main(void)\n{\n   gl_Position = proj_matrix * view_matrix * model_matrix * vec4(aPosition.xy * posScale.xy + posOffset.xy, z, 1.0);\n   vTexCoord = aTexCoords * texScale + texOffset;\n}\n"

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mVertexShader:Ljava/lang/String;

    .line 112
    const-string v0, "precision mediump float;\nuniform sampler2D texture;\nuniform float alpha;\nvarying vec2 vTexCoord;\nvoid main(void)\n{ \n   gl_FragColor = alpha * texture2D( texture, vTexCoord );\n}\n"

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mFragmentShader:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public draw()V
    .locals 3

    .prologue
    .line 88
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maPositionHandle:I

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mTriangleVertices:Ljava/nio/FloatBuffer;

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->sendVertexAttribute(ILjava/nio/FloatBuffer;I)V

    .line 89
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maTexCoordsHandle:I

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mTextureCoords:Ljava/nio/FloatBuffer;

    const/4 v2, 0x2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->sendVertexAttribute(ILjava/nio/FloatBuffer;I)V

    .line 90
    const/4 v0, 0x4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->mTriangleVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 91
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 26
    const-string v0, "precision mediump float;\nattribute vec3 aPosition;\nattribute vec2 aTexCoords;\nuniform mat4 proj_matrix;\nuniform mat4 view_matrix;\nuniform mat4 model_matrix;\nuniform vec3 posScale;\nuniform vec3 posOffset;\nuniform vec2 texScale;\nuniform vec2 texOffset;\nuniform float z;\nvarying vec2  vTexCoord;\nvoid main(void)\n{\n   gl_Position = proj_matrix * view_matrix * model_matrix * vec4(aPosition.xy * posScale.xy + posOffset.xy, z, 1.0);\n   vTexCoord = aTexCoords * texScale + texOffset;\n}\n"

    const-string v1, "precision mediump float;\nuniform sampler2D texture;\nuniform float alpha;\nvarying vec2 vTexCoord;\nvoid main(void)\n{ \n   gl_FragColor = alpha * texture2D( texture, vTexCoord );\n}\n"

    invoke-super {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Program;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    const-string v0, "aPosition"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getAttributeLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maPositionHandle:I

    .line 29
    const-string v0, "aTexCoords"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getAttributeLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->maTexCoordsHandle:I

    .line 30
    const-string v0, "posScale"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosScaleHandle:I

    .line 31
    const-string v0, "posOffset"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosOffsetHandle:I

    .line 32
    const-string v0, "texScale"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexScaleHandle:I

    .line 33
    const-string v0, "texOffset"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexOffsetHandle:I

    .line 34
    const-string v0, "alpha"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muAlphaHandle:I

    .line 35
    const-string v0, "texture"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->msTextureHandle:I

    .line 36
    const-string v0, "z"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muZHandle:I

    .line 38
    const-string v0, "proj_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muProjectionMatrixHandle:I

    .line 39
    const-string v0, "view_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muViewMatrixHandle:I

    .line 40
    const-string v0, "model_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muModelMatrixHandle:I

    .line 42
    return-void
.end method

.method public predrawAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muAlphaHandle:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 57
    return-void
.end method

.method public predrawPositionOffset(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosOffsetHandle:I

    invoke-static {v0, p1, p2, p3}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 61
    return-void
.end method

.method public predrawPositionScale(FFF)V
    .locals 1
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F
    .param p3, "scaleZ"    # F

    .prologue
    .line 64
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muPosScaleHandle:I

    invoke-static {v0, p1, p2, p3}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 65
    return-void
.end method

.method public predrawTexture(I)V
    .locals 2
    .param p1, "textureId"    # I

    .prologue
    .line 45
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 46
    const/16 v0, 0xde1

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 47
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->msTextureHandle:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 49
    return-void
.end method

.method public predrawTextureOffset(FF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexOffsetHandle:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 69
    return-void
.end method

.method public predrawTextureScale(FF)V
    .locals 1
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F

    .prologue
    .line 72
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muTexScaleHandle:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glUniform2f(IFF)V

    .line 73
    return-void
.end method

.method public predrawZ(F)V
    .locals 1
    .param p1, "z"    # F

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muZHandle:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 53
    return-void
.end method

.method public setModelMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 84
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muModelMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 85
    return-void
.end method

.method public setProjectionMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 76
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muProjectionMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 77
    return-void
.end method

.method public setViewMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 80
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramDisplayImage;->muViewMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 81
    return-void
.end method
