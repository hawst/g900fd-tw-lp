.class public Lcom/samsung/surfacewidget/gl/GLUtil;
.super Ljava/lang/Object;
.source "GLUtil.java"


# static fields
.field static final DEBUGGING:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calcCameraMatrix(FFF)[F
    .locals 9
    .param p0, "eyeX"    # F
    .param p1, "eyeY"    # F
    .param p2, "eyeZ"    # F

    .prologue
    const/4 v3, 0x0

    .line 95
    const/high16 v7, 0x3f800000    # 1.0f

    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v5, v3

    move v6, v3

    move v8, v3

    invoke-static/range {v0 .. v8}, Lcom/samsung/surfacewidget/gl/GLUtil;->calcCameraMatrix(FFFFFFFFF)[F

    move-result-object v0

    return-object v0
.end method

.method public static calcCameraMatrix(FFFFFFFFF)[F
    .locals 11
    .param p0, "eyeX"    # F
    .param p1, "eyeY"    # F
    .param p2, "eyeZ"    # F
    .param p3, "focusX"    # F
    .param p4, "focusY"    # F
    .param p5, "focusZ"    # F
    .param p6, "upX"    # F
    .param p7, "upY"    # F
    .param p8, "upZ"    # F

    .prologue
    .line 104
    const/16 v1, 0x10

    new-array v0, v1, [F

    .line 105
    .local v0, "matrix":[F
    const/4 v1, 0x0

    move v2, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    move/from16 v10, p8

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 109
    return-object v0
.end method

.method public static calcOrthogonalMatrix(FF)[F
    .locals 2
    .param p0, "w"    # F
    .param p1, "h"    # F

    .prologue
    .line 74
    const v0, 0x3ecccccd    # 0.4f

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {p0, p1, v0, v1}, Lcom/samsung/surfacewidget/gl/GLUtil;->calcOrthogonalMatrix(FFFF)[F

    move-result-object v0

    return-object v0
.end method

.method public static calcOrthogonalMatrix(FFFF)[F
    .locals 11
    .param p0, "w"    # F
    .param p1, "h"    # F
    .param p2, "nearRatio"    # F
    .param p3, "farRatio"    # F

    .prologue
    const/4 v10, 0x0

    const/16 v9, 0x10

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 78
    new-array v0, v9, [F

    .line 79
    .local v0, "projection":[F
    mul-float v8, p0, v7

    .line 80
    .local v8, "focalDepth":F
    neg-float v3, p0

    div-float v2, v3, v7

    div-float v3, p0, v7

    div-float v4, p1, v7

    neg-float v5, p1

    div-float/2addr v5, v7

    mul-float v6, p2, v8

    mul-float v7, v8, p3

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    .line 85
    new-array v6, v9, [F

    .line 86
    .local v6, "pretrans":[F
    new-array v2, v9, [F

    .line 87
    .local v2, "resultMatrix":[F
    invoke-static {v6, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 88
    neg-float v3, v8

    invoke-static {v6, v1, v10, v10, v3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    move v3, v1

    move-object v4, v0

    move v5, v1

    move v7, v1

    .line 89
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 90
    return-object v2
.end method

.method public static calcPerspectiveMatrix(FFFF)[F
    .locals 10
    .param p0, "fovy"    # F
    .param p1, "aspect"    # F
    .param p2, "zNear"    # F
    .param p3, "zFar"    # F

    .prologue
    .line 64
    float-to-double v6, p0

    const-wide v8, 0x3f81df46a2529d39L    # 0.008726646259971648

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->tan(D)D

    move-result-wide v6

    double-to-float v1, v6

    mul-float v5, p2, v1

    .line 65
    .local v5, "top":F
    neg-float v4, v5

    .line 66
    .local v4, "bottom":F
    mul-float v2, v4, p1

    .line 67
    .local v2, "left":F
    mul-float v3, v5, p1

    .line 68
    .local v3, "right":F
    const/16 v1, 0x10

    new-array v0, v1, [F

    .line 69
    .local v0, "projection":[F
    const/4 v1, 0x0

    move v6, p2

    move v7, p3

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    .line 70
    return-object v0
.end method

.method public static calcProjectionMatrix(FF)[F
    .locals 2
    .param p0, "w"    # F
    .param p1, "h"    # F

    .prologue
    .line 44
    const v0, 0x3ecccccd    # 0.4f

    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {p0, p1, v0, v1}, Lcom/samsung/surfacewidget/gl/GLUtil;->calcProjectionMatrix(FFFF)[F

    move-result-object v0

    return-object v0
.end method

.method public static calcProjectionMatrix(FFFF)[F
    .locals 11
    .param p0, "w"    # F
    .param p1, "h"    # F
    .param p2, "nearRatio"    # F
    .param p3, "farRatio"    # F

    .prologue
    const/16 v10, 0x10

    const/4 v9, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 48
    new-array v0, v10, [F

    .line 49
    .local v0, "projection":[F
    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float v8, p0, v3

    .line 50
    .local v8, "focalDepth":F
    neg-float v3, p0

    div-float v2, v3, v7

    div-float v3, p0, v7

    div-float v4, p1, v7

    neg-float v5, p1

    div-float/2addr v5, v7

    mul-float v6, p2, v8

    mul-float v7, v8, p3

    invoke-static/range {v0 .. v7}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    .line 55
    new-array v6, v10, [F

    .line 56
    .local v6, "pretrans":[F
    new-array v2, v10, [F

    .line 57
    .local v2, "resultMatrix":[F
    invoke-static {v6, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 58
    invoke-static {v6, v1, v9, v9, v9}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    move v3, v1

    move-object v4, v0

    move v5, v1

    move v7, v1

    .line 59
    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 60
    return-object v2
.end method

.method public static createCubemap([Landroid/graphics/Bitmap;Z)I
    .locals 9
    .param p0, "bitmaps"    # [Landroid/graphics/Bitmap;
    .param p1, "recycle"    # Z

    .prologue
    const v8, 0x812f

    const/16 v7, 0x2601

    const/4 v6, 0x1

    const v4, 0x8513

    const/4 v5, 0x0

    .line 147
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 148
    const/16 v3, 0xde1

    invoke-static {v3, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 149
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 150
    new-array v1, v6, [I

    .line 151
    .local v1, "ids":[I
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 152
    invoke-static {v6, v1, v5}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 153
    aget v2, v1, v5

    .line 154
    .local v2, "textureId":I
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 155
    invoke-static {v4, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 156
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 157
    const/16 v3, 0x2802

    invoke-static {v4, v3, v8}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 159
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 160
    const/16 v3, 0x2803

    invoke-static {v4, v3, v8}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 162
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 163
    const/16 v3, 0x2800

    invoke-static {v4, v3, v7}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 165
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 166
    const/16 v3, 0x2801

    invoke-static {v4, v3, v7}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 168
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 170
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v0, v3, :cond_1

    .line 172
    const v3, 0x8515

    add-int/2addr v3, v0

    aget-object v4, p0, v0

    invoke-static {v3, v5, v4, v5}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 176
    const-string v3, "texImage2D"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 177
    if-eqz p1, :cond_0

    .line 178
    aget-object v3, p0, v0

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    .line 170
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_1
    const-string v3, "createCubemap"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 183
    return v2
.end method

.method public static deleteTexture(I)V
    .locals 3
    .param p0, "texture"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 187
    if-ltz p0, :cond_0

    .line 188
    new-array v0, v2, [I

    aput p0, v0, v1

    .line 189
    .local v0, "textures":[I
    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 191
    .end local v0    # "textures":[I
    :cond_0
    return-void
.end method

.method public static loadBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resource"    # I

    .prologue
    .line 113
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-gtz v1, :cond_1

    .line 115
    :cond_0
    const-string v1, "GLUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to load resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_1
    return-object v0
.end method

.method public static loadTexture(Landroid/graphics/Bitmap;Z)I
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "recycle"    # Z

    .prologue
    const/4 v2, 0x1

    const v6, 0x47012f00    # 33071.0f

    const v5, 0x46180400    # 9729.0f

    const/4 v4, 0x0

    const/16 v3, 0xde1

    .line 125
    new-array v1, v2, [I

    .line 126
    .local v1, "ids":[I
    invoke-static {v2, v1, v4}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 127
    aget v0, v1, v4

    .line 129
    .local v0, "id":I
    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 132
    const/16 v2, 0x2801

    invoke-static {v3, v2, v5}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 133
    const/16 v2, 0x2800

    invoke-static {v3, v2, v5}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 134
    const/16 v2, 0x2802

    invoke-static {v3, v2, v6}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 135
    const/16 v2, 0x2803

    invoke-static {v3, v2, v6}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 138
    invoke-static {v3, v4, p0, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 140
    if-eqz p1, :cond_0

    .line 141
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    .line 143
    :cond_0
    return v0
.end method

.method public static readRawFile(Landroid/content/Context;I)[B
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    .line 23
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    .line 25
    .local v3, "is":Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {v3}, Ljava/io/InputStream;->available()I

    move-result v0

    .line 26
    .local v0, "count":I
    new-array v1, v0, [B

    .line 27
    .local v1, "data":[B
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    if-eqz v3, :cond_0

    .line 35
    :try_start_1
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 40
    .end local v0    # "count":I
    .end local v1    # "data":[B
    :cond_0
    :goto_0
    return-object v1

    .line 36
    .restart local v0    # "count":I
    .restart local v1    # "data":[B
    :catch_0
    move-exception v2

    .line 37
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 30
    .end local v0    # "count":I
    .end local v1    # "data":[B
    .end local v2    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v2

    .line 31
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 34
    if-eqz v3, :cond_1

    .line 35
    :try_start_3
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 40
    :cond_1
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 36
    :catch_2
    move-exception v2

    .line 37
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 33
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 34
    if-eqz v3, :cond_2

    .line 35
    :try_start_4
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 38
    :cond_2
    :goto_2
    throw v4

    .line 36
    :catch_3
    move-exception v2

    .line 37
    .restart local v2    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2
.end method
