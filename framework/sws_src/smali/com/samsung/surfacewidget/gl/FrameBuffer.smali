.class public Lcom/samsung/surfacewidget/gl/FrameBuffer;
.super Ljava/lang/Object;
.source "FrameBuffer.java"


# instance fields
.field mDepthId:I

.field mHeight:I

.field mId:I

.field mTextureId:I

.field mWidth:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    .line 10
    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    .line 11
    iput v0, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    .line 12
    iput v0, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    .line 14
    return-void
.end method

.method static allocDepthTexture(II)I
    .locals 12
    .param p0, "w"    # I
    .param p1, "h"    # I

    .prologue
    const/16 v11, 0x2600

    const/16 v2, 0x1902

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/16 v0, 0xde1

    .line 189
    new-array v9, v3, [I

    .line 190
    .local v9, "ids":[I
    invoke-static {v3, v9, v1}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 191
    aget v10, v9, v1

    .line 193
    .local v10, "textureId":I
    invoke-static {v0, v10}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 196
    const/16 v7, 0x1405

    const/4 v8, 0x0

    move v3, p0

    move v4, p1

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 199
    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 200
    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 201
    const/16 v1, 0x2801

    invoke-static {v0, v1, v11}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 202
    const/16 v1, 0x2800

    invoke-static {v0, v1, v11}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 203
    return v10
.end method

.method static allocTexture(II)I
    .locals 12
    .param p0, "w"    # I
    .param p1, "h"    # I

    .prologue
    const/16 v11, 0x2600

    const/16 v2, 0x1908

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/16 v0, 0xde1

    .line 168
    new-array v9, v3, [I

    .line 169
    .local v9, "ids":[I
    invoke-static {v3, v9, v1}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 170
    aget v10, v9, v1

    .line 171
    .local v10, "textureId":I
    const-string v3, "Texture2d::glGenTextures"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 172
    invoke-static {v0, v10}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 173
    const-string v3, "Texture2d::glBindTexture"

    invoke-static {v3}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 174
    const/16 v7, 0x1401

    const/4 v8, 0x0

    move v3, p0

    move v4, p1

    move v5, v1

    move v6, v2

    invoke-static/range {v0 .. v8}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    .line 175
    const-string v1, "Texture2d::glTexImage2D"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 176
    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 177
    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 180
    const/16 v1, 0x2800

    invoke-static {v0, v1, v11}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 181
    const/16 v1, 0x2801

    invoke-static {v0, v1, v11}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 182
    const-string v0, "allocTexture"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    return v10
.end method

.method public static bindMainBuffer()V
    .locals 2

    .prologue
    .line 145
    const v0, 0x8d40

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 146
    const-string v0, "FrameBuffer::unbind -- glBindFramebuffer 0"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 148
    return-void
.end method

.method static clear(FFFF)V
    .locals 1
    .param p0, "r"    # F
    .param p1, "g"    # F
    .param p2, "b"    # F
    .param p3, "a"    # F

    .prologue
    .line 136
    invoke-static {p0, p1, p2, p3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 137
    const-string v0, "FrameBuffer::glClearColor"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 138
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 139
    const-string v0, "FrameBuffer::glClear"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 140
    return-void
.end method

.method static deallocTexture(I)V
    .locals 3
    .param p0, "textureId"    # I

    .prologue
    const/4 v2, 0x1

    .line 159
    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    .line 160
    .local v0, "ib":Ljava/nio/IntBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    .line 161
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 162
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glDeleteTextures(ILjava/nio/IntBuffer;)V

    .line 164
    return-void
.end method

.method static setViewport(IIII)V
    .locals 1
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "w"    # I
    .param p3, "h"    # I

    .prologue
    .line 152
    invoke-static {p0, p1, p2, p3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 153
    const-string v0, "FrameBuffer::setViewport"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 155
    return-void
.end method


# virtual methods
.method allocDepthBuffer()V
    .locals 6

    .prologue
    const v5, 0x8d41

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 89
    :goto_0
    return-void

    .line 78
    :cond_0
    const v1, 0x8d40

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 79
    const-string v1, "FrameBuffer::glBindFramebuffer"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 81
    new-array v0, v4, [I

    .line 82
    .local v0, "ids":[I
    invoke-static {v4, v0, v3}, Landroid/opengl/GLES20;->glGenRenderbuffers(I[II)V

    .line 83
    aget v1, v0, v3

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    .line 84
    const-string v1, "FrameBuffer::glGenRenderbuffers (depth)"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 85
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindRenderbuffer(II)V

    .line 86
    const-string v1, "FrameBuffer::glBindRenderbuffer (depth)"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 87
    const v1, 0x81a5

    iget v2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    invoke-static {v5, v1, v2, v3}, Landroid/opengl/GLES20;->glRenderbufferStorage(IIII)V

    .line 88
    const-string v1, "FrameBuffer::glRenderbufferStorage (depth)"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    goto :goto_0
.end method

.method public bind()V
    .locals 2

    .prologue
    .line 112
    const v0, 0x8d40

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 113
    const-string v0, "FrameBuffer::glBindFramebuffer"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 114
    return-void
.end method

.method bind(I)V
    .locals 5
    .param p1, "textureId"    # I

    .prologue
    const/4 v4, 0x0

    const v3, 0x8d40

    .line 93
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v0

    invoke-static {v3, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 94
    const-string v0, "FrameBuffer::glBindFramebuffer"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 95
    const v0, 0x8ce0

    const/16 v1, 0xde1

    invoke-static {v3, v0, v1, p1, v4}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 96
    const-string v0, "FrameBuffer::glFramebufferTexture2D"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 98
    const v0, 0x8d00

    const v1, 0x8d41

    iget v2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    invoke-static {v3, v0, v1, v2}, Landroid/opengl/GLES20;->glFramebufferRenderbuffer(IIII)V

    .line 99
    const-string v0, "FrameBuffer::glFramebufferTexture2D (depth)"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 102
    iget v0, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    invoke-static {v4, v4, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 103
    const-string v0, "FrameBuffer::glViewport"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 104
    invoke-static {v3}, Landroid/opengl/GLES20;->glCheckFramebufferStatus(I)I

    move-result v0

    const v1, 0x8cd5

    if-eq v0, v1, :cond_0

    .line 106
    const-string v0, "FrameBuffer bind"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glCheckFramebufferStatus failed for framebuffer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 65
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->hasErrors()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    invoke-static {v2}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v0

    .line 68
    .local v0, "ib":Ljava/nio/IntBuffer;
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mId:I

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    .line 69
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 70
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glDeleteFramebuffers(ILjava/nio/IntBuffer;)V

    .line 71
    const-string v1, "FrameBuffer::glDeleteFramebuffers"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 73
    .end local v0    # "ib":Ljava/nio/IntBuffer;
    :cond_0
    return-void
.end method

.method public getBoundTexture()I
    .locals 1

    .prologue
    .line 17
    iget v0, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    return v0
.end method

.method hasErrors()Z
    .locals 1

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method id()I
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mId:I

    return v0
.end method

.method public init(Ljava/lang/String;II)V
    .locals 4
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 22
    new-array v0, v2, [I

    .line 23
    .local v0, "ids":[I
    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    .line 24
    aget v1, v0, v1

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mId:I

    .line 25
    const-string v1, "FrameBuffer::glGenFramebuffers"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 26
    iput p2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    .line 27
    iput p3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    .line 29
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    .line 30
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->allocDepthBuffer()V

    .line 31
    invoke-static {p2, p3}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->allocTexture(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    .line 32
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->bind(I)V

    .line 33
    const-string v1, "XXX"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Framebuffer created: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mTextureId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", depthId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-void
.end method

.method public initDepthTexture(Ljava/lang/String;II)V
    .locals 7
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v6, 0x1

    const v5, 0x8d40

    const/4 v4, 0x0

    .line 38
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    .line 39
    invoke-static {p2, p3}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->allocDepthTexture(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    .line 41
    const-string v1, "XXX"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Framebuffer depth texture created: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->id()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mTextureId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", depthId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mDepthId:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    new-array v0, v6, [I

    .line 45
    .local v0, "ids":[I
    invoke-static {v6, v0, v4}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    .line 46
    aget v1, v0, v4

    iput v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mId:I

    .line 47
    const-string v1, "FrameBuffer::glGenFramebuffers"

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 48
    iput p2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    .line 49
    iput p3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    .line 52
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mId:I

    invoke-static {v5, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 54
    const v1, 0x8d00

    const/16 v2, 0xde1

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mTextureId:I

    invoke-static {v5, v1, v2, v3, v4}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 56
    invoke-static {v5}, Landroid/opengl/GLES20;->glCheckFramebufferStatus(I)I

    move-result v1

    const v2, 0x8cd5

    if-eq v1, v2, :cond_0

    .line 58
    const-string v1, "XXX"

    const-string v2, "ERROR: Frame buffer not set up correctly"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    :cond_0
    return-void
.end method

.method public readPixels()[I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 122
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->bind()V

    .line 123
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    iget v2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    mul-int/2addr v1, v2

    new-array v7, v1, [I

    .line 124
    .local v7, "pixels":[I
    iget v1, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    iget v2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    mul-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v6

    .line 125
    .local v6, "buffer":Ljava/nio/IntBuffer;
    invoke-virtual {v6, v0}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 126
    iget v2, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mWidth:I

    iget v3, p0, Lcom/samsung/surfacewidget/gl/FrameBuffer;->mHeight:I

    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v1, v0

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 127
    invoke-virtual {v6, v7}, Ljava/nio/IntBuffer;->get([I)Ljava/nio/IntBuffer;

    .line 128
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->unbind()V

    .line 129
    return-object v7
.end method

.method public unbind()V
    .locals 0

    .prologue
    .line 118
    invoke-static {}, Lcom/samsung/surfacewidget/gl/FrameBuffer;->bindMainBuffer()V

    .line 119
    return-void
.end method
