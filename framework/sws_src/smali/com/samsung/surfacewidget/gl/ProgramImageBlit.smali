.class public Lcom/samsung/surfacewidget/gl/ProgramImageBlit;
.super Lcom/samsung/surfacewidget/gl/Program;
.source "ProgramImageBlit.java"


# instance fields
.field private final mFragmentShader:Ljava/lang/String;

.field mTriangleVertices:Ljava/nio/FloatBuffer;

.field private final mVertexShader:Ljava/lang/String;

.field maPositionHandle:I

.field msTextureHandle:I

.field muAlphaHandle:I

.field muModelMatrixHandle:I

.field muProjectionMatrixHandle:I

.field muScaleHandle:I

.field muViewMatrixHandle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 6
    invoke-direct {p0}, Lcom/samsung/surfacewidget/gl/Program;-><init>()V

    .line 8
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->maPositionHandle:I

    .line 9
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muScaleHandle:I

    .line 10
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muAlphaHandle:I

    .line 11
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->msTextureHandle:I

    .line 13
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muProjectionMatrixHandle:I

    .line 14
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muViewMatrixHandle:I

    .line 15
    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muModelMatrixHandle:I

    .line 17
    sget-object v0, Lcom/samsung/surfacewidget/gl/ShaderUtil;->mQuadVertices:[F

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->allocBufferForAttribute([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->mTriangleVertices:Ljava/nio/FloatBuffer;

    .line 64
    const-string v0, "precision mediump float;\nuniform mat4 proj_matrix;\nuniform mat4 view_matrix;\nuniform mat4 model_matrix;\nattribute vec3 aPosition;\nuniform vec3 scale;\nvarying vec2  vTexCoord;\nvoid main(void)\n{\n   gl_Position = proj_matrix * view_matrix * model_matrix * vec4(aPosition*scale, 1.0);\n   vTexCoord = aPosition.xy * 0.5 + 0.5;\n}\n"

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->mVertexShader:Ljava/lang/String;

    .line 78
    const-string v0, "precision mediump float;\nuniform sampler2D texture;\nuniform float alpha;\nvarying vec2 vTexCoord;\nvoid main(void)\n{ \n   gl_FragColor = texture2D( texture, vTexCoord );\n   gl_FragColor.a = alpha;\n}\n"

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->mFragmentShader:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public draw()V
    .locals 3

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->maPositionHandle:I

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->mTriangleVertices:Ljava/nio/FloatBuffer;

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->sendVertexAttribute(ILjava/nio/FloatBuffer;I)V

    .line 61
    const/4 v0, 0x4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->mTriangleVertices:Ljava/nio/FloatBuffer;

    invoke-virtual {v2}, Ljava/nio/FloatBuffer;->limit()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 62
    return-void
.end method

.method public init()V
    .locals 2

    .prologue
    .line 20
    const-string v0, "precision mediump float;\nuniform mat4 proj_matrix;\nuniform mat4 view_matrix;\nuniform mat4 model_matrix;\nattribute vec3 aPosition;\nuniform vec3 scale;\nvarying vec2  vTexCoord;\nvoid main(void)\n{\n   gl_Position = proj_matrix * view_matrix * model_matrix * vec4(aPosition*scale, 1.0);\n   vTexCoord = aPosition.xy * 0.5 + 0.5;\n}\n"

    const-string v1, "precision mediump float;\nuniform sampler2D texture;\nuniform float alpha;\nvarying vec2 vTexCoord;\nvoid main(void)\n{ \n   gl_FragColor = texture2D( texture, vTexCoord );\n   gl_FragColor.a = alpha;\n}\n"

    invoke-super {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Program;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v0, "aPosition"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getAttributeLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->maPositionHandle:I

    .line 23
    const-string v0, "scale"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muScaleHandle:I

    .line 24
    const-string v0, "alpha"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muAlphaHandle:I

    .line 25
    const-string v0, "texture"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->msTextureHandle:I

    .line 27
    const-string v0, "proj_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muProjectionMatrixHandle:I

    .line 28
    const-string v0, "view_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muViewMatrixHandle:I

    .line 29
    const-string v0, "model_matrix"

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->getUniformLocation(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muModelMatrixHandle:I

    .line 30
    return-void
.end method

.method public predrawAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 44
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muAlphaHandle:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 45
    return-void
.end method

.method public predrawScale(FFF)V
    .locals 1
    .param p1, "scaleX"    # F
    .param p2, "scaleY"    # F
    .param p3, "scaleZ"    # F

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muScaleHandle:I

    invoke-static {v0, p1, p2, p3}, Landroid/opengl/GLES20;->glUniform3f(IFFF)V

    .line 41
    return-void
.end method

.method public predrawTexture(I)V
    .locals 2
    .param p1, "textureId"    # I

    .prologue
    .line 33
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 34
    const/16 v0, 0xde1

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 35
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->msTextureHandle:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 37
    return-void
.end method

.method public setModelMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 56
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muModelMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 57
    return-void
.end method

.method public setProjectionMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 48
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muProjectionMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 49
    return-void
.end method

.method public setViewMatrix([F)V
    .locals 3
    .param p1, "matrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 52
    iget v0, p0, Lcom/samsung/surfacewidget/gl/ProgramImageBlit;->muViewMatrixHandle:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 53
    return-void
.end method
