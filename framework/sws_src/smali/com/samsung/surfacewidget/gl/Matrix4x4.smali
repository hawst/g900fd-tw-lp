.class public Lcom/samsung/surfacewidget/gl/Matrix4x4;
.super Ljava/lang/Object;
.source "Matrix4x4.java"


# instance fields
.field public mData:[F

.field private mTempData:[F

.field private mTempData2:[F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    .line 8
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    .line 9
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    .line 12
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->identity()V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/samsung/surfacewidget/gl/Matrix4x4;)V
    .locals 2
    .param p1, "matrix"    # Lcom/samsung/surfacewidget/gl/Matrix4x4;

    .prologue
    const/16 v1, 0x10

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    .line 8
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    .line 9
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    .line 16
    iget-object v0, p1, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-direct {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->copyData([F[F)V

    .line 17
    return-void
.end method

.method private copyData([F[F)V
    .locals 2
    .param p1, "src"    # [F
    .param p2, "dst"    # [F

    .prologue
    .line 66
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 67
    aget v1, p1, v0

    aput v1, p2, v0

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 69
    :cond_0
    return-void
.end method

.method public static multiplyMM(Lcom/samsung/surfacewidget/gl/Matrix4x4;Lcom/samsung/surfacewidget/gl/Matrix4x4;Lcom/samsung/surfacewidget/gl/Matrix4x4;)V
    .locals 6
    .param p0, "result"    # Lcom/samsung/surfacewidget/gl/Matrix4x4;
    .param p1, "lhs"    # Lcom/samsung/surfacewidget/gl/Matrix4x4;
    .param p2, "rhs"    # Lcom/samsung/surfacewidget/gl/Matrix4x4;

    .prologue
    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-virtual {p1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->getData()[F

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->getData()[F

    move-result-object v4

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 83
    return-void
.end method

.method public static multiplyMM(Lcom/samsung/surfacewidget/gl/Matrix4x4;[F[F)V
    .locals 6
    .param p0, "result"    # Lcom/samsung/surfacewidget/gl/Matrix4x4;
    .param p1, "lhs"    # [F
    .param p2, "rhs"    # [F

    .prologue
    const/4 v1, 0x0

    .line 86
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    move-object v2, p1

    move v3, v1

    move-object v4, p2

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 87
    return-void
.end method


# virtual methods
.method public getData()[F
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    return-object v0
.end method

.method public identity()V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 25
    return-void
.end method

.method public inverse()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-static {v0, v2, v1, v2}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 57
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-direct {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->copyData([F[F)V

    .line 58
    return-void
.end method

.method public inverseTranspose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-static {v0, v2, v1, v2}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 62
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    invoke-static {v0, v2, v1, v2}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    .line 63
    return-void
.end method

.method public log()V
    .locals 5

    .prologue
    .line 72
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 73
    const-string v1, "Matrix4x4"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    mul-int/lit8 v4, v0, 0x4

    add-int/lit8 v4, v4, 0x0

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    mul-int/lit8 v4, v0, 0x4

    add-int/lit8 v4, v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    mul-int/lit8 v4, v0, 0x4

    add-int/lit8 v4, v4, 0x2

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    mul-int/lit8 v4, v0, 0x4

    add-int/lit8 v4, v4, 0x3

    aget v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    return-void
.end method

.method public rotate(FFFF)V
    .locals 6
    .param p1, "degrees"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 34
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 35
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    iget-object v2, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v4, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 36
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-direct {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->copyData([F[F)V

    .line 37
    return-void
.end method

.method public scale(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    const/4 v1, 0x0

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 41
    return-void
.end method

.method public setRotate(FFFF)V
    .locals 6
    .param p1, "degrees"    # F
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    const/4 v1, 0x0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 30
    return-void
.end method

.method public translate(FFF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 45
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    invoke-static {v0, v1, p1, p2, p3}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 46
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    iget-object v2, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v4, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 47
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData2:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-direct {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->copyData([F[F)V

    .line 48
    return-void
.end method

.method public transpose()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-static {v0, v2, v1, v2}, Landroid/opengl/Matrix;->transposeM([FI[FI)V

    .line 52
    iget-object v0, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mTempData:[F

    iget-object v1, p0, Lcom/samsung/surfacewidget/gl/Matrix4x4;->mData:[F

    invoke-direct {p0, v0, v1}, Lcom/samsung/surfacewidget/gl/Matrix4x4;->copyData([F[F)V

    .line 53
    return-void
.end method
