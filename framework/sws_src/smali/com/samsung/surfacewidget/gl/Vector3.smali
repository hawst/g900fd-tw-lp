.class public Lcom/samsung/surfacewidget/gl/Vector3;
.super Ljava/lang/Object;
.source "Vector3.java"


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 5
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 6
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 9
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 1
    .param p1, "aX"    # F
    .param p2, "aY"    # F
    .param p3, "aZ"    # F

    .prologue
    const/4 v0, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 5
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 6
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 20
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/surfacewidget/gl/Vector3;->set(FFF)V

    .line 21
    return-void
.end method

.method public constructor <init>(Lcom/samsung/surfacewidget/gl/Vector3;)V
    .locals 1
    .param p1, "v"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 5
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 6
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 12
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/gl/Vector3;->set(Lcom/samsung/surfacewidget/gl/Vector3;)V

    .line 13
    return-void
.end method

.method public constructor <init>(Lcom/samsung/surfacewidget/gl/Vector3;Lcom/samsung/surfacewidget/gl/Vector3;)V
    .locals 4
    .param p1, "start"    # Lcom/samsung/surfacewidget/gl/Vector3;
    .param p2, "end"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 5
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 6
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 24
    iget v0, p2, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v1, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    sub-float/2addr v0, v1

    iget v1, p2, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v2, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    sub-float/2addr v1, v2

    iget v2, p2, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    sub-float/2addr v2, v3

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/surfacewidget/gl/Vector3;->set(FFF)V

    .line 25
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "values"    # [F

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 5
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 6
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 16
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/gl/Vector3;->set([F)V

    .line 17
    return-void
.end method


# virtual methods
.method public add(FFF)V
    .locals 1
    .param p1, "aX"    # F
    .param p2, "aY"    # F
    .param p3, "aZ"    # F

    .prologue
    .line 76
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 77
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 78
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    add-float/2addr v0, p3

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 79
    return-void
.end method

.method public add(Lcom/samsung/surfacewidget/gl/Vector3;)V
    .locals 2
    .param p1, "v"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    .line 82
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v1, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 83
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v1, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 84
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v1, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 85
    return-void
.end method

.method public add([F)V
    .locals 2
    .param p1, "values"    # [F

    .prologue
    .line 70
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    const/4 v1, 0x0

    aget v1, p1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 71
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    const/4 v1, 0x1

    aget v1, p1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 72
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    const/4 v1, 0x2

    aget v1, p1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 73
    return-void
.end method

.method cross(Lcom/samsung/surfacewidget/gl/Vector3;)V
    .locals 5
    .param p1, "v"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    .line 88
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v4, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    mul-float/2addr v3, v4

    sub-float v0, v2, v3

    .line 89
    .local v0, "newX":F
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v4, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    mul-float/2addr v3, v4

    sub-float v1, v2, v3

    .line 90
    .local v1, "newY":F
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v4, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 91
    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 92
    iput v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 93
    return-void
.end method

.method public length()F
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/Vector3;->lengthSquared()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public lengthSquared()F
    .locals 3

    .prologue
    .line 40
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public limit(F)V
    .locals 2
    .param p1, "value"    # F

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/Vector3;->lengthSquared()F

    move-result v0

    mul-float v1, p1, p1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/Vector3;->normalize()V

    .line 111
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/gl/Vector3;->scale(F)V

    .line 113
    :cond_0
    return-void
.end method

.method public normalize()V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/gl/Vector3;->length()F

    move-result v1

    .line 55
    .local v1, "length":F
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v0, v2, v1

    .line 56
    .local v0, "inverse":F
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 57
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 58
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 59
    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    mul-float/2addr v2, v0

    iput v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 61
    :cond_0
    return-void
.end method

.method public scale(F)V
    .locals 1
    .param p1, "s"    # F

    .prologue
    .line 48
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 49
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 50
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 51
    return-void
.end method

.method public set(FFF)V
    .locals 0
    .param p1, "aX"    # F
    .param p2, "aY"    # F
    .param p3, "aZ"    # F

    .prologue
    .line 28
    iput p1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 29
    iput p2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 30
    iput p3, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 31
    return-void
.end method

.method public set(Lcom/samsung/surfacewidget/gl/Vector3;)V
    .locals 1
    .param p1, "v"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    .line 34
    iget v0, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 35
    iget v0, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 36
    iget v0, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 37
    return-void
.end method

.method public set([F)V
    .locals 1
    .param p1, "values"    # [F

    .prologue
    .line 64
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    .line 65
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    .line 66
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    .line 67
    return-void
.end method

.method public subtract(Lcom/samsung/surfacewidget/gl/Vector3;)Lcom/samsung/surfacewidget/gl/Vector3;
    .locals 5
    .param p1, "v"    # Lcom/samsung/surfacewidget/gl/Vector3;

    .prologue
    .line 105
    new-instance v0, Lcom/samsung/surfacewidget/gl/Vector3;

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    iget v2, p1, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    iget v3, p1, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    iget v4, p1, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    sub-float/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/surfacewidget/gl/Vector3;-><init>(FFF)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toStringInt()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->x:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->y:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/surfacewidget/gl/Vector3;->z:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
