.class public Lcom/samsung/surfacewidget/gl/Program;
.super Ljava/lang/Object;
.source "Program.java"


# static fields
.field static final DEBUGGING:Z

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mProgramId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "XXX"

    sput-object v0, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Program;->mProgramId:I

    return-void
.end method

.method public static allocBufferForAttribute([F)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0, "data"    # [F

    .prologue
    .line 135
    array-length v1, p0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 136
    .local v0, "buffer":Ljava/nio/FloatBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 137
    return-object v0
.end method

.method public static allocBufferForAttribute([I)Ljava/nio/IntBuffer;
    .locals 3
    .param p0, "data"    # [I

    .prologue
    .line 141
    array-length v1, p0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v0

    .line 142
    .local v0, "buffer":Ljava/nio/IntBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/IntBuffer;->put([I)Ljava/nio/IntBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    .line 143
    return-object v0
.end method

.method public static checkGLError(Ljava/lang/String;)I
    .locals 1
    .param p0, "op"    # Ljava/lang/String;

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public static createProgram(Ljava/lang/String;Ljava/lang/String;)I
    .locals 8
    .param p0, "vertexSource"    # Ljava/lang/String;
    .param p1, "fragmentSource"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 45
    const-string v5, "XXX"

    const-string v6, "loading vertexShader"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    const v5, 0x8b31

    invoke-static {v5, p0}, Lcom/samsung/surfacewidget/gl/Program;->loadShader(ILjava/lang/String;)I

    move-result v3

    .line 47
    .local v3, "vertexShader":I
    const-string v5, "loadShader vertexShader"

    invoke-static {v5}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 48
    if-nez v3, :cond_1

    .line 49
    const-string v5, "XXX"

    const-string v6, "loading vertexShader failed"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v4

    .line 77
    :cond_0
    :goto_0
    return v2

    .line 54
    :cond_1
    const-string v5, "XXX"

    const-string v6, "loading pixelShader"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const v5, 0x8b30

    invoke-static {v5, p1}, Lcom/samsung/surfacewidget/gl/Program;->loadShader(ILjava/lang/String;)I

    move-result v1

    .line 56
    .local v1, "pixelShader":I
    const-string v5, "loadShader pixelShader"

    invoke-static {v5}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 57
    const-string v5, "XXX"

    const-string v6, "loadShaders done"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v2

    .line 61
    .local v2, "program":I
    if-eqz v2, :cond_0

    .line 62
    invoke-static {v2, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 63
    sget-object v5, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const-string v5, "glAttachShader"

    invoke-static {v5}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 65
    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 66
    const-string v5, "glAttachShader"

    invoke-static {v5}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 67
    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 68
    new-array v0, v7, [I

    .line 69
    .local v0, "linkStatus":[I
    const v5, 0x8b82

    invoke-static {v2, v5, v0, v4}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 70
    aget v4, v0, v4

    if-eq v4, v7, :cond_0

    .line 71
    sget-object v4, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    const-string v5, "Could not link program: "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    sget-object v4, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 74
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getAttributeLocation(ILjava/lang/String;)I
    .locals 3
    .param p0, "programId"    # I
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 126
    invoke-static {p0, p1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    .line 127
    .local v0, "v":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glGetAttributeLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 128
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not get id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->logError(Ljava/lang/String;)V

    .line 131
    :cond_0
    return v0
.end method

.method public static getUniformLocation(ILjava/lang/String;)I
    .locals 3
    .param p0, "programId"    # I
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-static {p0, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 118
    .local v0, "v":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glGetUniformLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 119
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 120
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not get id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->logError(Ljava/lang/String;)V

    .line 122
    :cond_0
    return v0
.end method

.method public static loadShader(ILjava/lang/String;)I
    .locals 5
    .param p0, "shaderType"    # I
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 81
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 82
    .local v1, "shader":I
    if-eqz v1, :cond_0

    .line 83
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 84
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 85
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 86
    .local v0, "compiled":[I
    const v2, 0x8b81

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 87
    aget v2, v0, v3

    if-nez v2, :cond_0

    .line 88
    sget-object v2, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not compile shader "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    sget-object v2, Lcom/samsung/surfacewidget/gl/Program;->TAG:Ljava/lang/String;

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 92
    const/4 v1, 0x0

    .line 95
    .end local v0    # "compiled":[I
    :cond_0
    return v1
.end method

.method public static logError(Ljava/lang/String;)V
    .locals 0
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 114
    return-void
.end method


# virtual methods
.method public getAttributeLocation(Ljava/lang/String;)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    iget v1, p0, Lcom/samsung/surfacewidget/gl/Program;->mProgramId:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    .line 22
    .local v0, "location":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "glGetAttribLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 23
    return v0
.end method

.method public getUniformLocation(Ljava/lang/String;)I
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 27
    iget v1, p0, Lcom/samsung/surfacewidget/gl/Program;->mProgramId:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 28
    .local v0, "location":I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getUniformLocation "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 29
    return v0
.end method

.method public init(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "vs"    # Ljava/lang/String;
    .param p2, "fs"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-static {p1, p2}, Lcom/samsung/surfacewidget/gl/Program;->createProgram(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/gl/Program;->mProgramId:I

    .line 18
    return-void
.end method

.method public sendVertexAttribute(ILjava/nio/FloatBuffer;I)V
    .locals 6
    .param p1, "handle"    # I
    .param p2, "buffer"    # Ljava/nio/FloatBuffer;
    .param p3, "valuesPerPoint"    # I

    .prologue
    const/4 v3, 0x0

    .line 37
    invoke-virtual {p2, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 38
    const/16 v2, 0x1406

    move v0, p1

    move v1, p3

    move v4, v3

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 39
    const-string v0, "glVertexAttribPointer"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 40
    invoke-static {p1}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 41
    const-string v0, "glEnableVertexAttribArray"

    invoke-static {v0}, Lcom/samsung/surfacewidget/gl/Program;->checkGLError(Ljava/lang/String;)I

    .line 42
    return-void
.end method

.method public use()V
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/surfacewidget/gl/Program;->mProgramId:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 34
    return-void
.end method
