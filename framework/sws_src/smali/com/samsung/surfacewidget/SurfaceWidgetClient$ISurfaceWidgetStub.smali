.class final Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;
.super Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;
.source "SurfaceWidgetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ISurfaceWidgetStub"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SurfaceWidgetClient$ISurfaceWidgetStub"


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/surfacewidget/SurfaceWidgetClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 1390
    invoke-direct {p0}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;-><init>()V

    .line 1391
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 1392
    return-void
.end method


# virtual methods
.method public checkVersion(I)V
    .locals 2
    .param p1, "hostAppSurfaceWidgetServiceVersion"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1396
    const v0, 0x39f50

    if-eq p1, v0, :cond_0

    .line 1397
    const-string v0, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v1, "Surface widget host application SurfaceWidgetService version does not match the SurfaceWidgetServive library this SurfaceWidget was compiled with!"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1399
    :cond_0
    return-void
.end method

.method public onContentDescriptionRequest(I)V
    .locals 4
    .param p1, "instance"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1723
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "Surface widget onContentDescriptionRequest"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1725
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1726
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1727
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1736
    :goto_0
    return-void

    .line 1731
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1732
    .local v0, "onContenDescritionMessage":Landroid/os/Message;
    const/16 v2, 0xe

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1733
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1735
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onDestroy(IZ)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "isRemovedFromIdle"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1422
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget destroy on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1425
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1426
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1434
    :goto_0
    return-void

    .line 1429
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1430
    .local v0, "destroyMessage":Landroid/os/Message;
    const/16 v2, 0x8

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1431
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1432
    if-eqz p2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 1433
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1432
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onKeyEvent(ILandroid/view/KeyEvent;)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1547
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1548
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1549
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    :goto_0
    return-void

    .line 1553
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1554
    .local v0, "keyEventMessage":Landroid/os/Message;
    const/4 v2, 0x5

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1555
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1556
    iput-object p2, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1557
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onKeyboardCompleted(IILjava/lang/String;)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "textViewIdentifier"    # I
    .param p3, "txt"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1646
    const-string v1, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Surface widget recieved edit text string"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on instance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1649
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1650
    .local v0, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v0, :cond_0

    .line 1651
    const-string v1, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v2, "service is no longer present"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1656
    :goto_0
    return-void

    .line 1655
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onKeyboardCompleted(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public onLauncherTiltChanged(IFFF)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1583
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget recieved tile a.k.a. shadow vector changed event on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1588
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_1

    .line 1589
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    :cond_0
    :goto_0
    return-void

    .line 1593
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1594
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1595
    invoke-virtual {v0, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onLauncherTiltChanged(FFF)V

    goto :goto_0
.end method

.method public onPause(I)V
    .locals 5
    .param p1, "instance"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1438
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget pause on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1441
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1442
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1450
    :goto_0
    return-void

    .line 1446
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1447
    .local v0, "pauseMessage":Landroid/os/Message;
    const/4 v2, 0x6

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1448
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1449
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPositionOffsetChanged(IFFF)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1564
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget position changed  x = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " y = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " z = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1567
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1568
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_1

    .line 1569
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1577
    :cond_0
    :goto_0
    return-void

    .line 1573
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1574
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1575
    invoke-virtual {v0, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPositionOffsetChanged(FFF)V

    goto :goto_0
.end method

.method public onResume(I)V
    .locals 5
    .param p1, "instance"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1455
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget resume on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1457
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1458
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1460
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1469
    :goto_0
    return-void

    .line 1464
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1465
    .local v0, "resumeMessage":Landroid/os/Message;
    const/4 v2, 0x7

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1466
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1467
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSurfaceDestroyed(I)V
    .locals 5
    .param p1, "instance"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1601
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget destroyed! on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1603
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1604
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1605
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1614
    :goto_0
    return-void

    .line 1610
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1611
    .local v0, "destroySurfaceMessage":Landroid/os/Message;
    const/16 v2, 0xb

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1612
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1613
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSurfaceSizeChanged(ILandroid/view/Surface;IIZII)V
    .locals 10
    .param p1, "instance"    # I
    .param p2, "surface"    # Landroid/view/Surface;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "changed"    # Z
    .param p6, "spanX"    # I
    .param p7, "spanY"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1474
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1475
    .local v9, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v9, :cond_0

    .line 1476
    const-string v0, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v1, "service is no longer present"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1481
    :goto_0
    return-void

    .line 1479
    :cond_0
    invoke-virtual {v9}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v8, v0, Landroid/content/res/Configuration;->orientation:I

    .local v8, "orientation":I
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    .line 1480
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->onSurfaceSizeChangedWithRotation(ILandroid/view/Surface;IIZIII)V

    goto :goto_0
.end method

.method public onSurfaceSizeChangedWithRotation(ILandroid/view/Surface;IIZIII)V
    .locals 10
    .param p1, "instance"    # I
    .param p2, "surface"    # Landroid/view/Surface;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "changed"    # Z
    .param p6, "spanX"    # I
    .param p7, "spanY"    # I
    .param p8, "orientation"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1487
    const-string v6, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Surface widget size changed width = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " height = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " on instance = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " time = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1491
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1492
    .local v5, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v5, :cond_0

    .line 1493
    const-string v6, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v7, "service is no longer present"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1527
    :goto_0
    return-void

    .line 1497
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;
    invoke-static {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$100(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 1503
    .local v4, "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v4, :cond_1

    .line 1504
    invoke-virtual {v4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->lockSizeChanged()V

    .line 1507
    :cond_1
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v3

    .line 1508
    .local v3, "onSurfaceSizeChangedMessage":Landroid/os/Message;
    const/16 v6, 0xd

    iput v6, v3, Landroid/os/Message;->what:I

    .line 1509
    iput p1, v3, Landroid/os/Message;->arg1:I

    .line 1510
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1511
    .local v2, "data":Landroid/os/Bundle;
    const-string v6, "surface"

    invoke-virtual {v2, v6, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1512
    const-string v6, "width"

    invoke-virtual {v2, v6, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1513
    const-string v6, "height"

    invoke-virtual {v2, v6, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1514
    const-string v6, "changed"

    invoke-virtual {v2, v6, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1515
    const-string v6, "spanX"

    move/from16 v0, p6

    invoke-virtual {v2, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1516
    const-string v6, "spanY"

    move/from16 v0, p7

    invoke-virtual {v2, v6, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1518
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1521
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z
    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$200()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    move/from16 v0, p8

    if-eq v0, v6, :cond_2

    .line 1523
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$300(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Ljava/util/HashMap;

    move-result-object v6

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1525
    :cond_2
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onVerticalTouch(ILandroid/view/MotionEvent;)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "touchEvent"    # Landroid/view/MotionEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1532
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1533
    .local v0, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v0, :cond_0

    .line 1534
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1543
    :goto_0
    return-void

    .line 1538
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1539
    .local v1, "touchMessage":Landroid/os/Message;
    const/4 v2, 0x4

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1540
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 1541
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1542
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onVisibilityChanged(IZ)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "visibility"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1404
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget visibility changed visibility = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1407
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1408
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_1

    .line 1409
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1418
    :cond_0
    :goto_0
    return-void

    .line 1413
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1415
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1416
    invoke-virtual {v0, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setVisibility(Z)V

    goto :goto_0
.end method

.method public setCallback(ILcom/samsung/surfacewidget/IRemoteServiceCallback;)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "callback"    # Lcom/samsung/surfacewidget/IRemoteServiceCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1620
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCallback on instance/callback = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1623
    if-nez p2, :cond_0

    .line 1625
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "setCallback callback was null...invalid value"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1640
    :goto_0
    return-void

    .line 1629
    :cond_0
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1630
    .local v0, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v0, :cond_1

    .line 1631
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1635
    :cond_1
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1636
    .local v1, "setCallbackMessage":Landroid/os/Message;
    const/16 v2, 0xc

    iput v2, v1, Landroid/os/Message;->what:I

    .line 1637
    iput p1, v1, Landroid/os/Message;->arg1:I

    .line 1638
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1639
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public setFocus(IZ)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "focusOn"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1667
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget setFocus on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1669
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1670
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_0

    .line 1671
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1681
    :goto_0
    return-void

    .line 1675
    :cond_0
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1676
    .local v0, "focusMessage":Landroid/os/Message;
    const/16 v2, 0x9

    iput v2, v0, Landroid/os/Message;->what:I

    .line 1677
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 1678
    if-eqz p2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput v2, v0, Landroid/os/Message;->arg2:I

    .line 1680
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 1678
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public setProviderInfo(ILcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "info"    # Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1704
    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "null SurfaceWidgetProviderInfo"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1707
    :cond_0
    const-string v1, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Provider info set to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " on instance = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1711
    .local v0, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v0, :cond_1

    .line 1712
    const-string v1, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v2, "service is no longer present"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1718
    :goto_0
    return-void

    .line 1716
    :cond_1
    iget v1, p2, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->updatePeriodMillis:I

    # setter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUpdatePeriodMillis:I
    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$402(Lcom/samsung/surfacewidget/SurfaceWidgetClient;I)I

    .line 1717
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mProviderInfos:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$500(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/util/SparseArray;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public setUpdateTime(II)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "updateTimeMillis"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1686
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget setUpdateTime to = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1690
    .local v1, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v1, :cond_1

    .line 1691
    const-string v2, "SurfaceWidgetClient$ISurfaceWidgetStub"

    const-string v3, "service is no longer present"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699
    :cond_0
    :goto_0
    return-void

    .line 1695
    :cond_1
    invoke-virtual {v1, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1696
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1697
    invoke-virtual {v0, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setUpdateTime(I)V

    goto :goto_0
.end method
