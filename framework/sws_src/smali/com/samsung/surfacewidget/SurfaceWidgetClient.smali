.class public Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.super Landroid/app/Service;
.source "SurfaceWidgetClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;,
        Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;,
        Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;,
        Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;
    }
.end annotation


# static fields
.field static final CLEARCANVAS:I = 0xa

.field private static final DEBUGGABLE:Z = true

.field static final DESTROY:I = 0x8

.field static final DESTROYSURFACE:I = 0xb

.field static final FOCUS:I = 0x9

.field static final KEYEVENT:I = 0x5

.field static final LAYOUTVIEWS:I = 0x2

.field public static final MSG_ENUM_COUNT:I = 0xf

.field static final ONCONTENTDESCRIPTION:I = 0xe

.field static final ONSTART:I = 0x0

.field static final PAUSE:I = 0x6

.field static final PLAYSOUND:I = 0x3

.field static final RENDERVIEWS:I = 0x1

.field static final RESUME:I = 0x7

.field static final SETCALLBACK:I = 0xc

.field static final SURFACESIZECHANGED:I = 0xd

.field static final SURFACE_WIDGET_CHECKIN_VERSION:I = 0x6

.field static final SURFACE_WIDGET_SERVICE_VERSION:I = 0x39f50

.field private static final TAG:Ljava/lang/String; = "SurfaceWidgetClient"

.field static final TOUCHEVENT:I = 0x4

.field private static sRotationFeatureEnabled:Z


# instance fields
.field private mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

.field private mCrashAllInsances:Z

.field private mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

.field private mOrientation:I

.field private mPendingOnSizedChangedMsg:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mProviderInfos:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRotationLock:Z

.field private mTalkBackFocused:Z

.field private mTalkBackPaint:Landroid/graphics/Paint;

.field private mTalkbackFocusArea:Landroid/graphics/Rect;

.field private mUIHandler:Landroid/os/Handler;

.field private mUpdatePeriodMillis:I

.field private mWidgetInstances:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetRootViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mbStoppedAllInstances:Z

.field private renderArea:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 78
    new-instance v0, Lcom/samsung/surfacewidget/SynchronizedSparseArray;

    invoke-direct {v0}, Lcom/samsung/surfacewidget/SynchronizedSparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    .line 79
    new-instance v0, Lcom/samsung/surfacewidget/SynchronizedSparseArray;

    invoke-direct {v0}, Lcom/samsung/surfacewidget/SynchronizedSparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mProviderInfos:Landroid/util/SparseArray;

    .line 84
    new-instance v0, Lcom/samsung/surfacewidget/SynchronizedSparseArray;

    invoke-direct {v0}, Lcom/samsung/surfacewidget/SynchronizedSparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    .line 87
    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    .line 89
    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    .line 114
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCrashAllInsances:Z

    .line 118
    iput v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUpdatePeriodMillis:I

    .line 121
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mbStoppedAllInstances:Z

    .line 123
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->renderArea:Landroid/graphics/Rect;

    .line 124
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackFocused:Z

    .line 125
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkbackFocusArea:Landroid/graphics/Rect;

    .line 126
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackPaint:Landroid/graphics/Paint;

    .line 129
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    .line 130
    iput v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mOrientation:I

    .line 131
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;

    .line 1885
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$200()Z
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/surfacewidget/SurfaceWidgetClient;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    .param p1, "x1"    # I

    .prologue
    .line 68
    iput p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUpdatePeriodMillis:I

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mProviderInfos:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/surfacewidget/SurfaceWidgetClient;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setChildTalkBackFocused(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/surfacewidget/SurfaceWidgetClient;IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    return-object v0
.end method

.method private crashAllInstances(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 956
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "crashAllInstances() called"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 957
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCrashAllInsances:Z

    .line 959
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->stopAllInstances()V

    .line 961
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    monitor-enter v3

    .line 962
    const/4 v0, 0x0

    .local v0, "i":I
    :try_start_0
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v1

    .local v1, "size":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 963
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v2, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestDestroy(Ljava/lang/String;)V

    .line 962
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 965
    :cond_0
    monitor-exit v3

    .line 967
    return-void

    .line 965
    .end local v1    # "size":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private static destroyRootView(Ljava/lang/Object;)V
    .locals 4
    .param p0, "viewRootImpl"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x0

    .line 1314
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v1, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1316
    .local v0, "topParentViewGroup":Landroid/view/ViewGroup;
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1318
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;->DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1329
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1331
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->DESTROY_HARDWARE_RENDERER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v1, p0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1356
    return-void
.end method

.method private drawSurfaceWidgetViews(I)V
    .locals 11
    .param p1, "instance"    # I

    .prologue
    .line 1157
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v5

    .line 1158
    .local v5, "view":Landroid/view/View;
    if-nez v5, :cond_1

    .line 1212
    :cond_0
    :goto_0
    return-void

    .line 1160
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v2

    .line 1161
    .local v2, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v2, :cond_0

    .line 1163
    monitor-enter v2

    .line 1165
    :try_start_0
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->renderArea:Landroid/graphics/Rect;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v9

    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Rect;->set(IIII)V

    .line 1166
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getSurface()Landroid/view/Surface;

    move-result-object v3

    .line 1167
    .local v3, "surface":Landroid/view/Surface;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1168
    :cond_2
    const-string v6, "SurfaceWidgetClient"

    const-string v7, "Can\'t find valid Surface. Is your Surface ready?"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->w(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1169
    :try_start_1
    monitor-exit v2

    goto :goto_0

    .line 1211
    .end local v3    # "surface":Landroid/view/Surface;
    :catchall_0
    move-exception v6

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 1172
    .restart local v3    # "surface":Landroid/view/Surface;
    :cond_3
    :try_start_2
    iget-boolean v4, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    .line 1173
    .local v4, "surfaceSizeChanged":Z
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->renderArea:Landroid/graphics/Rect;

    invoke-virtual {v3, v6}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    .line 1174
    .local v0, "drawingCanvas":Landroid/graphics/Canvas;
    if-eqz v0, :cond_5

    .line 1175
    const/4 v6, 0x0

    sget-object v7, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1176
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPredraw()V

    .line 1177
    invoke-virtual {v2, v0, v5}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onDraw(Landroid/graphics/Canvas;Landroid/view/View;)V

    .line 1178
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPostdraw()V

    .line 1179
    iget-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackFocused:Z

    if-eqz v6, :cond_4

    .line 1181
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackPaint:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1182
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackPaint:Landroid/graphics/Paint;

    const/16 v7, 0xff

    const/4 v8, 0x0

    const/16 v9, 0x9b

    const/4 v10, 0x0

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Paint;->setARGB(IIII)V

    .line 1183
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackPaint:Landroid/graphics/Paint;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1184
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkbackFocusArea:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6, v7}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1185
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackFocused:Z

    .line 1187
    :cond_4
    invoke-virtual {v3, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1189
    if-eqz v4, :cond_5

    .line 1190
    const/4 v6, 0x0

    :try_start_3
    iput-boolean v6, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    .line 1192
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v6

    iget v7, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    iget v8, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    invoke-interface {v6, v7, v8}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->acceptedSurfaceSize(II)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1211
    .end local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .end local v3    # "surface":Landroid/view/Surface;
    .end local v4    # "surfaceSizeChanged":Z
    :cond_5
    :goto_1
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1195
    .restart local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .restart local v3    # "surface":Landroid/view/Surface;
    .restart local v4    # "surfaceSizeChanged":Z
    :catch_0
    move-exception v1

    .line 1196
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_5
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    .line 1199
    .end local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v3    # "surface":Landroid/view/Surface;
    .end local v4    # "surfaceSizeChanged":Z
    :catch_1
    move-exception v1

    .line 1206
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    .line 1207
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 1208
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 1209
    const-string v6, "SurfaceWidget"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1
.end method

.method private static getAbsolutePosition(Landroid/view/View;)Landroid/graphics/Point;
    .locals 5
    .param p0, "aView"    # Landroid/view/View;

    .prologue
    .line 1747
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1748
    .local v0, "absPosition":Landroid/graphics/Point;
    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 1749
    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->y:I

    .line 1750
    move-object v3, p0

    .line 1751
    .local v3, "view":Landroid/view/View;
    :goto_0
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1753
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    .end local v3    # "view":Landroid/view/View;
    check-cast v3, Landroid/view/View;

    .line 1754
    .restart local v3    # "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 1755
    .local v1, "left":I
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v2

    .line 1756
    .local v2, "top":I
    iget v4, v0, Landroid/graphics/Point;->x:I

    add-int/2addr v4, v1

    iput v4, v0, Landroid/graphics/Point;->x:I

    .line 1757
    iget v4, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v4, v2

    iput v4, v0, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 1759
    .end local v1    # "left":I
    .end local v2    # "top":I
    :cond_0
    return-object v0
.end method

.method private getRenderer(IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 4
    .param p1, "instance"    # I
    .param p2, "doprint"    # Z

    .prologue
    .line 1099
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 1100
    .local v0, "lRendererThread":Ljava/lang/Object;
    if-nez v0, :cond_1

    .line 1101
    if-eqz p2, :cond_0

    const-string v1, "SurfaceWidgetClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "render thread not found -- instance: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1102
    :cond_0
    const/4 v0, 0x0

    .line 1105
    .end local v0    # "lRendererThread":Ljava/lang/Object;
    :goto_0
    return-object v0

    .line 1104
    .restart local v0    # "lRendererThread":Ljava/lang/Object;
    :cond_1
    instance-of v1, v0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    if-eqz v1, :cond_2

    .line 1105
    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    goto :goto_0

    .line 1107
    :cond_2
    const-string v1, "SurfaceWidgetClient"

    const-string v2, "The renderer was not an instance of SurfaceWidgetRenderer."

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1108
    const-string v1, "SurfaceWidgetClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The renderer was : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1109
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->printInstances()V

    .line 1110
    new-instance v1, Ljava/lang/ClassCastException;

    const-string v2, "Renderer was not an instance of SurfaceWidgetRenderer. Check dump for details"

    invoke-direct {v1, v2}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static getVersion()I
    .locals 1

    .prologue
    .line 233
    const v0, 0x39f50

    return v0
.end method

.method public static isRotationEnabled()Z
    .locals 1

    .prologue
    .line 1360
    sget-boolean v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    return v0
.end method

.method private static prepareRootView(ILandroid/view/View;)Ljava/lang/Object;
    .locals 10
    .param p0, "instance"    # I
    .param p1, "topParent"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    .line 1268
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1269
    .local v1, "context":Landroid/content/Context;
    const-string v5, "window"

    invoke-virtual {v1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 1272
    .local v2, "defDisplay":Landroid/view/Display;
    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->newInst(Landroid/content/Context;Landroid/view/Display;)Ljava/lang/Object;

    move-result-object v4

    .line 1275
    .local v4, "viewRoot":Ljava/lang/Object;
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    if-eqz v5, :cond_0

    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-nez v5, :cond_0

    .line 1276
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$CompatibilityInfoHolder;->newInst()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1285
    :cond_0
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    if-eqz v5, :cond_1

    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    invoke-virtual {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->isValid()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1286
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    new-array v6, v9, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v4, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 1289
    :cond_1
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, v4, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1295
    invoke-static {v4, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->newInst(Ljava/lang/Object;Landroid/view/Display;)Ljava/lang/Object;

    move-result-object v0

    .line 1297
    .local v0, "attachInfo":Ljava/lang/Object;
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    .line 1299
    .local v3, "mHandler":Landroid/os/Handler;
    if-eqz v0, :cond_3

    .line 1300
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v5, v6, :cond_2

    .line 1301
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_IGNOREDIRTYSTATE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1303
    :cond_2
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_HANDLER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1304
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_VIEW_ROOT_IMPL:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, v0, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1305
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_ROOT_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, v0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1307
    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;->M_ATTACH_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, p1, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->set(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 1310
    :cond_3
    return-object v4
.end method

.method private printInstances()V
    .locals 7

    .prologue
    .line 1248
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    monitor-enter v4

    .line 1249
    :try_start_0
    const-string v3, "SurfaceWidgetClient"

    const-string v5, "---------- mWidgetInstances dump -----------"

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1250
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 1251
    .local v2, "size":I
    const-string v3, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Number of instances:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    if-lez v2, :cond_0

    .line 1253
    const-string v3, "SurfaceWidgetClient"

    const-string v5, "mWidgetInstances content:"

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 1255
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 1256
    .local v1, "renderer":Ljava/lang/Object;
    const-string v3, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "  Renderer at position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1257
    const-string v3, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    key="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1258
    const-string v3, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    class="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    const-string v3, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "    hashCode="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1262
    .end local v0    # "i":I
    .end local v1    # "renderer":Ljava/lang/Object;
    :cond_0
    const-string v3, "SurfaceWidgetClient"

    const-string v5, "<<<---------- mWidgetInstances dump ----------->>>"

    invoke-static {v3, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1263
    monitor-exit v4

    .line 1264
    return-void

    .line 1263
    .end local v2    # "size":I
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method private putRenderer(ILcom/samsung/surfacewidget/SurfaceWidgetRenderer;)V
    .locals 3
    .param p1, "instance"    # I
    .param p2, "lRendererThread"    # Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .prologue
    .line 1215
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1217
    const-string v0, "SurfaceWidgetClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Renderer : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " added to mWidgetInstances for instance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->printInstances()V

    .line 1222
    return-void
.end method

.method private removeAllRenderers()V
    .locals 2

    .prologue
    .line 1242
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->printInstances()V

    .line 1243
    const-string v0, "SurfaceWidgetClient"

    const-string v1, "removing all renderers from mWidgetInstances"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1245
    return-void
.end method

.method private removeRenderer(I)V
    .locals 4
    .param p1, "instance"    # I

    .prologue
    .line 1226
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1227
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1228
    const-string v1, "SurfaceWidgetClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Renderer : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " deleted from mWidgetInstances for instance: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1235
    :goto_0
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->printInstances()V

    .line 1238
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 1239
    return-void

    .line 1232
    :cond_0
    const-string v1, "SurfaceWidgetClient"

    const-string v2, "Renderer was null when getting dump from removeRenderer"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setChildTalkBackFocused(Landroid/view/View;)V
    .locals 7
    .param p1, "aViewTalkBackFocused"    # Landroid/view/View;

    .prologue
    .line 1741
    invoke-static {p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getAbsolutePosition(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v0

    .line 1742
    .local v0, "absXY":Landroid/graphics/Point;
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackFocused:Z

    .line 1743
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkbackFocusArea:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    iget v4, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1744
    return-void
.end method

.method private setRotationLock(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "rotationLock"    # Z

    .prologue
    .line 819
    sget-boolean v2, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-nez v2, :cond_1

    .line 832
    :cond_0
    :goto_0
    return-void

    .line 821
    :cond_1
    iput-boolean p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    .line 823
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v1

    .line 824
    .local v1, "viewRoot":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 828
    :try_start_0
    check-cast v1, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    .end local v1    # "viewRoot":Landroid/view/View;
    invoke-virtual {v1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->setRotationLock(Z)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "e":Ljava/lang/Throwable;
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "SurfaceWidget setRotationLock failed!"

    invoke-static {v2, v3, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private stopAllInstances()V
    .locals 8

    .prologue
    .line 930
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    monitor-enter v5

    .line 932
    :try_start_0
    iget-boolean v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mbStoppedAllInstances:Z

    if-eqz v4, :cond_0

    monitor-exit v5

    .line 953
    :goto_0
    return-void

    .line 934
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mbStoppedAllInstances:Z

    .line 936
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v3

    .local v3, "size":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 937
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 938
    .local v2, "lThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 941
    const-wide/16 v6, 0x1f4

    :try_start_1
    invoke-virtual {v2, v6, v7}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 936
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 942
    :catch_0
    move-exception v0

    .line 943
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 946
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "i":I
    .end local v2    # "lThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .end local v3    # "size":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .restart local v1    # "i":I
    .restart local v3    # "size":I
    :cond_1
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 947
    iget-boolean v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCrashAllInsances:Z

    if-nez v4, :cond_2

    .line 948
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->removeAllRenderers()V

    goto :goto_0

    .line 951
    :cond_2
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCrashAllInsances:Z

    goto :goto_0
.end method


# virtual methods
.method protected acceptFocus(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "acceptedFocus"    # Z

    .prologue
    .line 720
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 721
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 723
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->acceptedFocus(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 730
    :goto_0
    return-void

    .line 724
    :catch_0
    move-exception v0

    .line 725
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 728
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "no renderer or callback not set!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public createRenderer(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "instance"    # I
    .param p4, "uiHandler"    # Landroid/os/Handler;

    .prologue
    .line 147
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V

    .line 150
    .local v0, "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    return-object v0
.end method

.method public createUIHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getUIHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method protected customMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 1063
    const/4 v0, 0x0

    return v0
.end method

.method public getProviderInfo(I)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 342
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mProviderInfos:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    return-object v0
.end method

.method protected getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 1095
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    return-object v0
.end method

.method protected getRootView(I)Landroid/view/View;
    .locals 3
    .param p1, "instance"    # I

    .prologue
    .line 1122
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 1123
    .local v1, "viewRootImpl":Ljava/lang/Object;
    if-eqz v1, :cond_0

    .line 1124
    sget-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v2, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1128
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getUIHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 995
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected handleLayoutViewsMessage(I)V
    .locals 0
    .param p1, "instance"    # I

    .prologue
    .line 1014
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->layoutSurfaceWidgetsViews(I)V

    .line 1015
    return-void
.end method

.method protected handleRenderViewsMessage(I)V
    .locals 0
    .param p1, "instance"    # I

    .prologue
    .line 1005
    invoke-direct {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->drawSurfaceWidgetViews(I)V

    .line 1006
    return-void
.end method

.method protected handleRequestKeyboard(IILjava/lang/String;)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "textViewIdentifier"    # I
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 492
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 493
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v1, :cond_0

    .line 508
    :goto_0
    return-void

    .line 497
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    if-nez v2, :cond_1

    .line 498
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "handleRequestKeyboard called with callback to launcher null!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 503
    :cond_1
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2, p2, p3}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestKeyboard(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 504
    :catch_0
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "requestKeyboard failed!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 506
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 354
    :try_start_0
    const-string v1, "SurfaceWidgetClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "FATAL: Unexpected exception in SurfaceWidget:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, p2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 355
    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->crashAllInstances(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    return-void

    .line 356
    :catch_0
    move-exception v0

    .line 357
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetClient"

    const-string v2, "Can\'t properly crash and destroy widget"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected layoutSurfaceWidgetsViews(I)V
    .locals 6
    .param p1, "instance"    # I

    .prologue
    .line 1133
    sget-boolean v2, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    if-eqz v2, :cond_1

    .line 1153
    :cond_0
    :goto_0
    return-void

    .line 1135
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1137
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v1

    .line 1138
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 1139
    monitor-enter v0

    .line 1140
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->measure(II)V

    .line 1143
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 1144
    sget-boolean v2, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-nez v2, :cond_2

    .line 1147
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestDraw()V

    .line 1148
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 1149
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1151
    :cond_2
    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "aIntent"    # Landroid/content/Intent;

    .prologue
    .line 214
    const-string v0, "SurfaceWidgetClient"

    const-string v1, "SurfaceWidget service bound"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;-><init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    .line 217
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 219
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;

    invoke-direct {v0, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ISurfaceWidgetStub;-><init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)V

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 171
    sget-boolean v4, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-nez v4, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mOrientation:I

    iget v5, p1, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v5, :cond_2

    .line 187
    const-string v4, "SurfaceWidgetClient"

    const-string v5, "onConfigurationChanged, but orientation is same with previous one. so skip!"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_2
    iget v4, p1, Landroid/content/res/Configuration;->orientation:I

    iput v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mOrientation:I

    .line 193
    const-string v4, "SurfaceWidgetClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onConfigurationChanged, Orientation : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mOrientation:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-super {p0, p1}, Landroid/app/Service;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 197
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v2

    .local v2, "size":I
    :goto_1
    if-ge v0, v2, :cond_0

    .line 198
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 199
    .local v3, "widgetInstance":I
    const/4 v4, 0x1

    invoke-direct {p0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setRotationLock(IZ)V

    .line 200
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->size()I

    move-result v4

    if-lez v4, :cond_3

    .line 201
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mPendingOnSizedChangedMsg:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Message;

    .line 202
    .local v1, "msg":Landroid/os/Message;
    if-eqz v1, :cond_3

    .line 203
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v4, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 205
    .end local v1    # "msg":Landroid/os/Message;
    :cond_3
    invoke-virtual {p0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onContentRequest(I)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onContentDescriptionRequest(I)V
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 611
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 612
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 613
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onContentDescriptionRequest()V

    .line 615
    :cond_0
    return-void
.end method

.method protected onContentRequest(I)V
    .locals 0
    .param p1, "instance"    # I

    .prologue
    .line 334
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 159
    const-string v0, "SurfaceWidgetClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onCreate() called"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;

    invoke-direct {v0, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;-><init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    .line 161
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    .line 162
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mOrientation:I

    .line 163
    return-void

    .line 161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 292
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 293
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 294
    const-string v3, "SurfaceWidgetClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " onDestroy() called"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->stopAllInstances()V

    .line 298
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    if-eqz v3, :cond_0

    .line 299
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    invoke-virtual {v3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->resetHandler()V

    .line 300
    iput-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mExceptionHandler:Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;

    .line 303
    :cond_0
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    .line 304
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mProviderInfos:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    .line 306
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v1

    .line 307
    .local v1, "size":I
    if-lez v1, :cond_2

    .line 308
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 309
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    .line 310
    .local v2, "viewRootImpl":Ljava/lang/Object;
    if-eqz v2, :cond_1

    .line 311
    invoke-static {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->destroyRootView(Ljava/lang/Object;)V

    .line 308
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    .end local v0    # "i":I
    .end local v2    # "viewRootImpl":Ljava/lang/Object;
    :cond_2
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->clear()V

    .line 316
    return-void
.end method

.method protected declared-synchronized onDestroy(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "isRemovedFromIdle"    # Z

    .prologue
    .line 627
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 628
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v1, :cond_0

    .line 629
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 631
    :try_start_1
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 635
    :goto_0
    const/4 v3, 0x0

    :try_start_2
    invoke-virtual {v1, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setCallback(Lcom/samsung/surfacewidget/IRemoteServiceCallback;)V

    .line 636
    const/4 v1, 0x0

    .line 637
    invoke-direct {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->removeRenderer(I)V

    .line 640
    :cond_0
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 641
    .local v2, "viewRootImpl":Ljava/lang/Object;
    if-eqz v2, :cond_1

    .line 642
    invoke-static {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->destroyRootView(Ljava/lang/Object;)V

    .line 643
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 646
    :cond_1
    invoke-static {}, Ljava/lang/System;->gc()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 647
    monitor-exit p0

    return-void

    .line 632
    .end local v2    # "viewRootImpl":Ljava/lang/Object;
    :catch_0
    move-exception v0

    .line 633
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 627
    .end local v0    # "e":Ljava/lang/InterruptedException;
    .end local v1    # "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected onFinishInflate(ILandroid/view/View;)V
    .locals 0
    .param p1, "instance"    # I
    .param p2, "topView"    # Landroid/view/View;

    .prologue
    .line 1073
    return-void
.end method

.method protected onKeyEvent(ILandroid/view/KeyEvent;)V
    .locals 5
    .param p1, "instance"    # I
    .param p2, "keyEvent"    # Landroid/view/KeyEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 518
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v1, :cond_0

    .line 539
    :goto_0
    return-void

    .line 522
    :cond_0
    monitor-enter v1

    .line 523
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onKeyEvent(Landroid/view/KeyEvent;)V

    .line 527
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v2

    .line 528
    .local v2, "viewRoot":Landroid/view/View;
    if-nez v2, :cond_1

    .line 529
    monitor-exit v1

    goto :goto_0

    .line 538
    .end local v2    # "viewRoot":Landroid/view/View;
    :catchall_0
    move-exception v3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 533
    .restart local v2    # "viewRoot":Landroid/view/View;
    :cond_1
    :try_start_1
    invoke-virtual {v2, p2}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 538
    :try_start_2
    monitor-exit v1

    goto :goto_0

    .line 534
    :catch_0
    move-exception v0

    .line 535
    .local v0, "e":Ljava/lang/Throwable;
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "SurfaceWidget dispatchKeyEvent failed!"

    invoke-static {v3, v4, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 536
    new-instance v3, Landroid/os/RemoteException;

    const-string v4, "SurfaceWidget dispatchKeyEvent failed"

    invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method protected onKeyboardCompleted(IILjava/lang/String;)V
    .locals 1
    .param p1, "instance"    # I
    .param p2, "textViewId"    # I
    .param p3, "textEntered"    # Ljava/lang/String;

    .prologue
    .line 1082
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 1083
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 1084
    invoke-virtual {v0, p2, p3}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onKeyboardCompleted(ILjava/lang/String;)V

    .line 1086
    :cond_0
    return-void
.end method

.method protected onPause(I)V
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 603
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 604
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 605
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPause()V

    .line 607
    :cond_0
    return-void
.end method

.method protected onResume(I)V
    .locals 2
    .param p1, "instance"    # I

    .prologue
    .line 585
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 586
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 587
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setRotationLock(IZ)V

    .line 588
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onResume()V

    .line 592
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestLayout()V

    .line 594
    :cond_0
    return-void
.end method

.method protected onSurfaceDestroy(I)V
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 656
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 657
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v0, :cond_0

    .line 658
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->releaseSurface()V

    .line 660
    :cond_0
    return-void
.end method

.method protected onSurfaceSizeChanged(ILandroid/os/Bundle;)V
    .locals 12
    .param p1, "instance"    # I
    .param p2, "data"    # Landroid/os/Bundle;

    .prologue
    .line 777
    const-string v8, "surface"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/Surface;

    .line 778
    .local v1, "surface":Landroid/view/Surface;
    const-string v8, "width"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 779
    .local v2, "width":I
    const-string v8, "height"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 780
    .local v3, "height":I
    const-string v8, "changed"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 781
    .local v6, "changed":Z
    const-string v8, "spanX"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 782
    .local v4, "spanX":I
    const-string v8, "spanY"

    invoke-virtual {p2, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 785
    .local v5, "spanY":I
    const-string v8, "SurfaceWidgetClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Changing surface widget size width = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " height = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " on instance = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " time = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 790
    .local v0, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v0, :cond_2

    .line 791
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {p0, v8, v9, p1, v10}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->createRenderer(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v0

    .line 794
    if-nez v0, :cond_1

    .line 795
    const-string v8, "SurfaceWidgetClient"

    const-string v9, "renderer failed to create!  Must override createRenderer function!"

    invoke-static {v8, v9}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    :cond_0
    :goto_0
    return-void

    .line 799
    :cond_1
    iget v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUpdatePeriodMillis:I

    invoke-virtual {v0, v8}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setUpdateTime(I)V

    .line 800
    invoke-direct {p0, p1, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->putRenderer(ILcom/samsung/surfacewidget/SurfaceWidgetRenderer;)V

    .line 803
    :cond_2
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setSurface(Landroid/view/Surface;IIII)V

    .line 806
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->isAlive()Z

    move-result v8

    if-nez v8, :cond_3

    .line 807
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalThreadStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 813
    :cond_3
    :goto_1
    sget-boolean v8, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v8, :cond_0

    .line 814
    const/4 v8, 0x0

    invoke-direct {p0, p1, v8}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setRotationLock(IZ)V

    goto :goto_0

    .line 809
    :catch_0
    move-exception v7

    .line 810
    .local v7, "e":Ljava/lang/IllegalThreadStateException;
    const-string v8, "SurfaceWidgetClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to start renderer thread: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected onTouchEvent(ILandroid/view/MotionEvent;)V
    .locals 6
    .param p1, "instance"    # I
    .param p2, "touchEvent"    # Landroid/view/MotionEvent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 548
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 549
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v1, :cond_0

    .line 576
    :goto_0
    return-void

    .line 554
    :cond_0
    const-string v3, "SurfaceWidgetClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTouchEvent:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 555
    monitor-enter v1

    .line 556
    :try_start_0
    invoke-virtual {v1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onTouchEvent(Landroid/view/MotionEvent;)V

    .line 560
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v2

    .line 561
    .local v2, "viewRoot":Landroid/view/View;
    if-nez v2, :cond_2

    .line 562
    instance-of v3, v1, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;

    if-nez v3, :cond_1

    .line 564
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "non-GL SurfaceWidget has no Android views viewRootImpl is null...this is a problem"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 575
    .end local v2    # "viewRoot":Landroid/view/View;
    :catchall_0
    move-exception v3

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 570
    .restart local v2    # "viewRoot":Landroid/view/View;
    :cond_2
    :try_start_1
    invoke-virtual {v2, p2}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 575
    :try_start_2
    monitor-exit v1

    goto :goto_0

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Ljava/lang/Throwable;
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "SurfaceWidget dispatchTouchEvent failed!"

    invoke-static {v3, v4, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 573
    new-instance v3, Landroid/os/RemoteException;

    const-string v4, "SurfaceWidget dispatchTouchEvent failed"

    invoke-direct {v3, v4}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public onTrimMemory(I)V
    .locals 14
    .param p1, "level"    # I

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/app/Service;->onTrimMemory(I)V

    .line 244
    const-string v10, "SurfaceWidgetClient"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SurfaceWidgetClient onTrimMemory level: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const/16 v10, 0x3c

    if-eq p1, v10, :cond_0

    const/16 v10, 0x50

    if-ne p1, v10, :cond_5

    .line 247
    :cond_0
    iget-object v11, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    monitor-enter v11

    .line 248
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 249
    .local v8, "time":J
    const-wide/16 v2, 0x0

    .line 250
    .local v2, "longestPausedDuration":J
    const/4 v1, -0x1

    .line 253
    .local v1, "instance":I
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v7

    .local v7, "size":I
    :goto_0
    if-ge v0, v7, :cond_2

    .line 254
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 255
    .local v6, "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    invoke-virtual {v6}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->isPaused()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 256
    iget-wide v12, v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPauseTime:J

    sub-long v4, v8, v12

    .line 257
    .local v4, "pausedDuration":J
    cmp-long v10, v4, v2

    if-lez v10, :cond_1

    .line 258
    move-wide v2, v4

    .line 259
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 253
    .end local v4    # "pausedDuration":J
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    .end local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :cond_2
    const/4 v10, -0x1

    if-ne v1, v10, :cond_3

    .line 265
    const/4 v0, 0x0

    :goto_1
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v10

    if-ge v0, v10, :cond_3

    .line 266
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 267
    .restart local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    iget-boolean v10, v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    if-nez v10, :cond_6

    .line 268
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    .line 274
    .end local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :cond_3
    const/4 v10, -0x1

    if-eq v1, v10, :cond_4

    .line 275
    iget-object v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetInstances:Landroid/util/SparseArray;

    invoke-virtual {v10, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 276
    .restart local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v6, :cond_4

    .line 277
    const-string v10, "SurfaceWidgetClient"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "releasing widget instance (paused: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-boolean v13, v6, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "): "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-virtual {v6}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->releaseResources()V

    .line 282
    .end local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :cond_4
    monitor-exit v11

    .line 284
    .end local v0    # "i":I
    .end local v1    # "instance":I
    .end local v2    # "longestPausedDuration":J
    .end local v7    # "size":I
    .end local v8    # "time":J
    :cond_5
    return-void

    .line 265
    .restart local v0    # "i":I
    .restart local v1    # "instance":I
    .restart local v2    # "longestPausedDuration":J
    .restart local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .restart local v7    # "size":I
    .restart local v8    # "time":J
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 282
    .end local v0    # "i":I
    .end local v1    # "instance":I
    .end local v2    # "longestPausedDuration":J
    .end local v6    # "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .end local v7    # "size":I
    .end local v8    # "time":J
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v10
.end method

.method protected releaseFocus(I)V
    .locals 5
    .param p1, "instance"    # I

    .prologue
    .line 699
    const-string v2, "SurfaceWidgetClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Surface widget releaseFocus on instance = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 702
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 704
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->releaseFocus()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    :goto_0
    return-void

    .line 705
    :catch_0
    move-exception v0

    .line 706
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 709
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "no renderer or callback not set!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected requestPositionUpdates(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "on"    # Z

    .prologue
    .line 917
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 918
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 920
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestPositionUpdates(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 927
    :goto_0
    return-void

    .line 921
    :catch_0
    move-exception v0

    .line 922
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 925
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "no renderer or callback not set!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected requestTilt(IZ)V
    .locals 4
    .param p1, "instance"    # I
    .param p2, "on"    # Z

    .prologue
    .line 896
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 897
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 899
    :try_start_0
    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2, p2}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestTilt(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 906
    :goto_0
    return-void

    .line 900
    :catch_0
    move-exception v0

    .line 901
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 904
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const-string v2, "SurfaceWidgetClient"

    const-string v3, "no renderer or callback not set!"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected setCallback(ILcom/samsung/surfacewidget/IRemoteServiceCallback;)V
    .locals 7
    .param p1, "instance"    # I
    .param p2, "callback"    # Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    .prologue
    const/4 v6, 0x0

    .line 734
    const-string v3, "SurfaceWidgetClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setting Callback on instance/callback = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    if-nez p2, :cond_0

    .line 738
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "setCallback callback was null...invalid value"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    :goto_0
    return-void

    .line 742
    :cond_0
    const/4 v1, 0x0

    .line 743
    .local v1, "newRenderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    invoke-direct {p0, p1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v2

    .line 744
    .local v2, "oldRenderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    instance-of v3, v2, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;

    if-eqz v3, :cond_3

    .line 745
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "old renderer was gl renderer. calling onDestroy"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 746
    invoke-virtual {p0, p1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy(IZ)V

    .line 751
    :cond_1
    :goto_1
    if-nez v1, :cond_2

    .line 752
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "new renderer is null. creating renderer!"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 753
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {p0, v3, v4, p1, v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->createRenderer(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 755
    invoke-direct {p0, p1, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->putRenderer(ILcom/samsung/surfacewidget/SurfaceWidgetRenderer;)V

    .line 757
    :cond_2
    if-nez v1, :cond_4

    .line 758
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "renderer failed to create!  Must override createRenderer function!"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 747
    :cond_3
    if-eqz v2, :cond_1

    .line 748
    const-string v3, "SurfaceWidgetClient"

    const-string v4, "set new renderer same as old renderer"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 749
    move-object v1, v2

    goto :goto_1

    .line 762
    :cond_4
    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUpdatePeriodMillis:I

    invoke-virtual {v1, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setUpdateTime(I)V

    .line 763
    iput-object p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    .line 764
    invoke-virtual {v1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setCallback(Lcom/samsung/surfacewidget/IRemoteServiceCallback;)V

    .line 767
    const v3, 0x39f50

    :try_start_0
    invoke-interface {p2, v3}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->checkVersion(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 768
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method protected setContentView(II)V
    .locals 10
    .param p1, "instance"    # I
    .param p2, "xmlResourceId"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 370
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 371
    .local v1, "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v1, :cond_0

    .line 372
    const-string v6, "SurfaceWidgetClient"

    const-string v7, "no renderer set!"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    :goto_0
    return-void

    .line 376
    :cond_0
    sget-boolean v6, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v6, :cond_1

    iget-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    if-eqz v6, :cond_1

    .line 377
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v4

    .line 378
    .local v4, "viewRoot":Landroid/view/View;
    if-eqz v4, :cond_1

    move-object v6, v4

    .line 379
    check-cast v6, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    invoke-virtual {v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->removeAllViews()V

    .line 380
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    check-cast v4, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    .end local v4    # "viewRoot":Landroid/view/View;
    invoke-virtual {v6, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 381
    .local v3, "view":Landroid/view/View;
    invoke-virtual {p0, p1, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onFinishInflate(ILandroid/view/View;)V

    .line 383
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9, v7}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 384
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8, v7}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 386
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleLayoutViewsMessage(I)V

    goto :goto_0

    .line 392
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v6, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    .line 393
    .local v5, "viewRootImpl":Ljava/lang/Object;
    if-eqz v5, :cond_2

    .line 394
    invoke-static {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->destroyRootView(Ljava/lang/Object;)V

    .line 395
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v6, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 398
    :cond_2
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    iget-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    invoke-direct {v2, p0, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;-><init>(Landroid/content/Context;Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;Z)V

    .line 400
    .local v2, "topParent":Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;
    invoke-static {p1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->prepareRootView(ILandroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 402
    .local v0, "fakeViewRoot":Ljava/lang/Object;
    if-nez v0, :cond_3

    .line 403
    const-string v6, "SurfaceWidgetClient"

    const-string v7, "Failed to prepare rootview!"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_3
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    invoke-virtual {v6, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 408
    .restart local v3    # "view":Landroid/view/View;
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v6, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 409
    invoke-virtual {p0, p1, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onFinishInflate(ILandroid/view/View;)V

    .line 413
    sget-boolean v6, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v6, :cond_4

    .line 416
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9, v7}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 417
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v6, v8, v7}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 420
    :cond_4
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleLayoutViewsMessage(I)V

    goto :goto_0
.end method

.method protected setContentView(ILandroid/view/View;)V
    .locals 9
    .param p1, "instance"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 433
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 434
    .local v1, "renderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v1, :cond_0

    .line 435
    const-string v5, "SurfaceWidgetClient"

    const-string v6, "no renderer set!"

    invoke-static {v5, v6}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 482
    :goto_0
    return-void

    .line 439
    :cond_0
    sget-boolean v5, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    if-eqz v5, :cond_1

    .line 440
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v3

    .line 441
    .local v3, "viewRoot":Landroid/view/View;
    if-eqz v3, :cond_1

    move-object v5, v3

    .line 442
    check-cast v5, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    invoke-virtual {v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->removeAllViews()V

    .line 443
    check-cast v3, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    .end local v3    # "viewRoot":Landroid/view/View;
    invoke-virtual {v3, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->addView(Landroid/view/View;)V

    .line 445
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 446
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 448
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleLayoutViewsMessage(I)V

    goto :goto_0

    .line 454
    :cond_1
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    .line 455
    .local v4, "viewRootImpl":Ljava/lang/Object;
    if-eqz v4, :cond_2

    .line 456
    invoke-static {v4}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->destroyRootView(Ljava/lang/Object;)V

    .line 457
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v5, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 460
    :cond_2
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;

    iget-boolean v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mRotationLock:Z

    invoke-direct {v2, p0, v1, v5}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;-><init>(Landroid/content/Context;Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;Z)V

    .line 462
    .local v2, "topParent":Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;
    invoke-static {p1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->prepareRootView(ILandroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 464
    .local v0, "fakeViewRoot":Ljava/lang/Object;
    if-nez v0, :cond_3

    .line 465
    const-string v5, "SurfaceWidgetClient"

    const-string v6, "Failed to prepare rootview!"

    invoke-static {v5, v6}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 469
    :cond_3
    invoke-virtual {v2, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->addView(Landroid/view/View;)V

    .line 470
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mWidgetRootViews:Landroid/util/SparseArray;

    invoke-virtual {v5, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 474
    sget-boolean v5, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z

    if-eqz v5, :cond_4

    .line 477
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 478
    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v5, v7, v6}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 481
    :cond_4
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleLayoutViewsMessage(I)V

    goto :goto_0
.end method

.method protected setFocus(IZ)Z
    .locals 5
    .param p1, "instance"    # I
    .param p2, "focusOn"    # Z

    .prologue
    const/4 v4, 0x0

    .line 669
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v1

    .line 670
    .local v1, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    const/4 v0, 0x0

    .line 672
    .local v0, "acceptedFocus":Z
    if-eqz v1, :cond_0

    .line 673
    invoke-virtual {v1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setFocus(Z)Z

    move-result v0

    .line 676
    :cond_0
    if-eqz p2, :cond_2

    .line 677
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v2

    .line 679
    .local v2, "root":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 680
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v3

    or-int/2addr v0, v3

    .line 687
    .end local v2    # "root":Landroid/view/View;
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->acceptFocus(IZ)V

    .line 689
    return v4

    .line 684
    :cond_2
    iput-boolean v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mTalkBackFocused:Z

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 3
    .param p1, "launchIntent"    # Landroid/content/Intent;

    .prologue
    .line 971
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v1

    .line 973
    .local v1, "intentString":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    invoke-interface {v2, v1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->startActivity(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 977
    :goto_0
    return-void

    .line 974
    :catch_0
    move-exception v0

    .line 975
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopService(Landroid/content/Intent;)Z
    .locals 2
    .param p1, "name"    # Landroid/content/Intent;

    .prologue
    .line 323
    const-string v0, "SurfaceWidgetClient"

    const-string v1, "stopService called!"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->stopAllInstances()V

    .line 326
    invoke-super {p0, p1}, Landroid/app/Service;->stopService(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
