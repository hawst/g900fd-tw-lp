.class public abstract Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;
.super Landroid/os/Binder;
.source "ISurfaceWidgetService.java"

# interfaces
.implements Lcom/samsung/surfacewidget/ISurfaceWidgetService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/ISurfaceWidgetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.surfacewidget.ISurfaceWidgetService"

.field static final TRANSACTION_checkVersion:I = 0x1

.field static final TRANSACTION_onContentDescriptionRequest:I = 0x11

.field static final TRANSACTION_onDestroy:I = 0xa

.field static final TRANSACTION_onKeyEvent:I = 0x6

.field static final TRANSACTION_onKeyboardCompleted:I = 0xd

.field static final TRANSACTION_onLauncherTiltChanged:I = 0xb

.field static final TRANSACTION_onPause:I = 0x8

.field static final TRANSACTION_onPositionOffsetChanged:I = 0xc

.field static final TRANSACTION_onResume:I = 0x9

.field static final TRANSACTION_onSurfaceDestroyed:I = 0x4

.field static final TRANSACTION_onSurfaceSizeChanged:I = 0x2

.field static final TRANSACTION_onSurfaceSizeChangedWithRotation:I = 0x12

.field static final TRANSACTION_onVerticalTouch:I = 0x5

.field static final TRANSACTION_onVisibilityChanged:I = 0x7

.field static final TRANSACTION_setCallback:I = 0x3

.field static final TRANSACTION_setFocus:I = 0xe

.field static final TRANSACTION_setProviderInfo:I = 0x10

.field static final TRANSACTION_setUpdateTime:I = 0xf


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/surfacewidget/ISurfaceWidgetService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/surfacewidget/ISurfaceWidgetService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/surfacewidget/ISurfaceWidgetService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 273
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 42
    :sswitch_0
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 50
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->checkVersion(I)V

    goto :goto_0

    .line 55
    .end local v1    # "_arg0":I
    :sswitch_2
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 59
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/Surface;

    .line 66
    .local v2, "_arg1":Landroid/view/Surface;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 68
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 70
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v5, v9

    .line 72
    .local v5, "_arg4":Z
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 74
    .local v6, "_arg5":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .local v7, "_arg6":I
    move-object v0, p0

    .line 75
    invoke-virtual/range {v0 .. v7}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onSurfaceSizeChanged(ILandroid/view/Surface;IIZII)V

    goto :goto_0

    .line 63
    .end local v2    # "_arg1":Landroid/view/Surface;
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Z
    .end local v6    # "_arg5":I
    .end local v7    # "_arg6":I
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/view/Surface;
    goto :goto_1

    .line 80
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/view/Surface;
    :sswitch_3
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 84
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    .line 85
    .local v2, "_arg1":Lcom/samsung/surfacewidget/IRemoteServiceCallback;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->setCallback(ILcom/samsung/surfacewidget/IRemoteServiceCallback;)V

    goto :goto_0

    .line 90
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/samsung/surfacewidget/IRemoteServiceCallback;
    :sswitch_4
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 93
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onSurfaceDestroyed(I)V

    goto :goto_0

    .line 98
    .end local v1    # "_arg0":I
    :sswitch_5
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 102
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    sget-object v0, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/MotionEvent;

    .line 108
    .local v2, "_arg1":Landroid/view/MotionEvent;
    :goto_2
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onVerticalTouch(ILandroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 106
    .end local v2    # "_arg1":Landroid/view/MotionEvent;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/view/MotionEvent;
    goto :goto_2

    .line 113
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/view/MotionEvent;
    :sswitch_6
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 117
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 118
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/KeyEvent;

    .line 123
    .local v2, "_arg1":Landroid/view/KeyEvent;
    :goto_3
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onKeyEvent(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 121
    .end local v2    # "_arg1":Landroid/view/KeyEvent;
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/view/KeyEvent;
    goto :goto_3

    .line 128
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Landroid/view/KeyEvent;
    :sswitch_7
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 132
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v2, v9

    .line 133
    .local v2, "_arg1":Z
    :goto_4
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onVisibilityChanged(IZ)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_4
    move v2, v5

    .line 132
    goto :goto_4

    .line 138
    .end local v1    # "_arg0":I
    :sswitch_8
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 141
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onPause(I)V

    goto/16 :goto_0

    .line 146
    .end local v1    # "_arg0":I
    :sswitch_9
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 149
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onResume(I)V

    goto/16 :goto_0

    .line 154
    .end local v1    # "_arg0":I
    :sswitch_a
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 158
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v2, v9

    .line 159
    .restart local v2    # "_arg1":Z
    :goto_5
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onDestroy(IZ)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_5
    move v2, v5

    .line 158
    goto :goto_5

    .line 164
    .end local v1    # "_arg0":I
    :sswitch_b
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 168
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 170
    .local v2, "_arg1":F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 172
    .local v3, "_arg2":F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 173
    .local v4, "_arg3":F
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onLauncherTiltChanged(IFFF)V

    goto/16 :goto_0

    .line 178
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":F
    .end local v3    # "_arg2":F
    .end local v4    # "_arg3":F
    :sswitch_c
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 182
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 184
    .restart local v2    # "_arg1":F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v3

    .line 186
    .restart local v3    # "_arg2":F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v4

    .line 187
    .restart local v4    # "_arg3":F
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onPositionOffsetChanged(IFFF)V

    goto/16 :goto_0

    .line 192
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":F
    .end local v3    # "_arg2":F
    .end local v4    # "_arg3":F
    :sswitch_d
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 196
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 198
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 199
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onKeyboardCompleted(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 204
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_e
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 208
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v2, v9

    .line 209
    .local v2, "_arg1":Z
    :goto_6
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->setFocus(IZ)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_6
    move v2, v5

    .line 208
    goto :goto_6

    .line 214
    .end local v1    # "_arg0":I
    :sswitch_f
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 218
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 219
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->setUpdateTime(II)V

    goto/16 :goto_0

    .line 224
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_10
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 228
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 229
    sget-object v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    .line 234
    .local v2, "_arg1":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    :goto_7
    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->setProviderInfo(ILcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;)V

    goto/16 :goto_0

    .line 232
    .end local v2    # "_arg1":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    :cond_7
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    goto :goto_7

    .line 239
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    :sswitch_11
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 242
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onContentDescriptionRequest(I)V

    goto/16 :goto_0

    .line 247
    .end local v1    # "_arg0":I
    :sswitch_12
    const-string v0, "com.samsung.surfacewidget.ISurfaceWidgetService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 251
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 252
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/Surface;

    .line 258
    .local v2, "_arg1":Landroid/view/Surface;
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 260
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 262
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    move v5, v9

    .line 264
    .restart local v5    # "_arg4":Z
    :cond_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 266
    .restart local v6    # "_arg5":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 268
    .restart local v7    # "_arg6":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .local v8, "_arg7":I
    move-object v0, p0

    .line 269
    invoke-virtual/range {v0 .. v8}, Lcom/samsung/surfacewidget/ISurfaceWidgetService$Stub;->onSurfaceSizeChangedWithRotation(ILandroid/view/Surface;IIZIII)V

    goto/16 :goto_0

    .line 255
    .end local v2    # "_arg1":Landroid/view/Surface;
    .end local v3    # "_arg2":I
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Z
    .end local v6    # "_arg5":I
    .end local v7    # "_arg6":I
    .end local v8    # "_arg7":I
    :cond_9
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/view/Surface;
    goto :goto_8

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
