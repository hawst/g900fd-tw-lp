.class final Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;
.super Ljava/lang/Object;
.source "SurfaceWidgetClient.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "ExceptionHandler"
.end annotation


# instance fields
.field private mDefaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

.field final synthetic this$0:Lcom/samsung/surfacewidget/SurfaceWidgetClient;


# direct methods
.method public constructor <init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0
    .param p2, "defaultExceptionHandler"    # Ljava/lang/Thread$UncaughtExceptionHandler;

    .prologue
    .line 1366
    iput-object p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->this$0:Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1367
    iput-object p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->mDefaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 1368
    return-void
.end method


# virtual methods
.method public resetHandler()V
    .locals 1

    .prologue
    .line 1380
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->mDefaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 1381
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->mDefaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 1382
    return-void
.end method

.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "thread"    # Ljava/lang/Thread;
    .param p2, "ex"    # Ljava/lang/Throwable;

    .prologue
    .line 1373
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->this$0:Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleUncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 1376
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ExceptionHandler;->mDefaultExceptionHandler:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 1377
    return-void
.end method
