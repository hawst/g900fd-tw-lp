.class final Lcom/samsung/surfacewidget/SynchronizedSparseArray;
.super Landroid/util/SparseArray;
.source "SynchronizedSparseArray.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/SparseArray",
        "<TE;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    invoke-direct {p0}, Landroid/util/SparseArray;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "initialCapacity"    # I

    .prologue
    .line 20
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    invoke-direct {p0, p1}, Landroid/util/SparseArray;-><init>(I)V

    return-void
.end method


# virtual methods
.method public declared-synchronized append(ILjava/lang/Object;)V
    .locals 1
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 36
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    .local p2, "value":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clear()V
    .locals 1

    .prologue
    .line 35
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/util/SparseArray;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clone()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 8
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SynchronizedSparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized delete(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 25
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->delete(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(I)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    .prologue
    .line 24
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    .local p2, "valueIfKeyNotFound":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized indexOfKey(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 33
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized indexOfValue(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)I"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    .local p1, "value":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized keyAt(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 30
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->keyAt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized put(ILjava/lang/Object;)V
    .locals 1
    .param p1, "key"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    .local p2, "value":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized remove(I)V
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 26
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->remove(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeAt(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 27
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->removeAt(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setValueAt(ILjava/lang/Object;)V
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    .local p2, "value":Ljava/lang/Object;, "TE;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized size()I
    .locals 1

    .prologue
    .line 29
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/util/SparseArray;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized valueAt(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Lcom/samsung/surfacewidget/SynchronizedSparseArray;, "Lcom/samsung/surfacewidget/SynchronizedSparseArray<TE;>;"
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
