.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FieldHelper"
.end annotation


# instance fields
.field private final mField:Ljava/lang/reflect/Field;


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 4
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 260
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 263
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 264
    .local v1, "tempField":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    :goto_0
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    .line 271
    return-void

    .line 265
    .end local v1    # "tempField":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find field"

    invoke-static {v2, v3, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 267
    const/4 v1, 0x0

    .restart local v1    # "tempField":Ljava/lang/reflect/Field;
    goto :goto_0
.end method


# virtual methods
.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "target"    # Ljava/lang/Object;

    .prologue
    .line 274
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_0

    .line 276
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 282
    :goto_0
    return-object v1

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "failed to get field"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 282
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public set(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "target"    # Ljava/lang/Object;
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 286
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    if-eqz v1, :cond_0

    .line 288
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->mField:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    const/4 v1, 0x1

    .line 295
    :goto_0
    return v1

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "failed to set field"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 295
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
