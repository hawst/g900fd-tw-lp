.class public Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
.super Landroid/appwidget/AppWidgetProviderInfo;
.source "SurfaceWidgetProviderInfo.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "SurfaceWidgetInfo"

.field private static final WIDGET_CATEGORY:Ljava/lang/String; = "widgetCategory"

.field private static final WIDGET_CONFIGURE:Ljava/lang/String; = "configure"

.field private static final WIDGET_DESCRIPTION:Ljava/lang/String; = "description"

.field private static final WIDGET_ICON:Ljava/lang/String; = "icon"

.field private static final WIDGET_INITIAL_KEYGUARD_LAYOUT:Ljava/lang/String; = "initialKeyguardLayout"

.field private static final WIDGET_INITIAL_LAYOUT:Ljava/lang/String; = "initialLayout"

.field private static final WIDGET_MAX_RESIZE_HEIGHT:Ljava/lang/String; = "maxResizeHeight"

.field private static final WIDGET_MAX_RESIZE_WIDTH:Ljava/lang/String; = "maxResizeWidth"

.field private static final WIDGET_MIN_HEIGHT:Ljava/lang/String; = "minHeight"

.field private static final WIDGET_MIN_RESIZE_HEIGHT:Ljava/lang/String; = "minResizeHeight"

.field private static final WIDGET_MIN_RESIZE_WIDTH:Ljava/lang/String; = "minResizeWidth"

.field private static final WIDGET_MIN_WIDTH:Ljava/lang/String; = "minWidth"

.field private static final WIDGET_PREVIEW_IMAGE:Ljava/lang/String; = "previewImage"

.field private static final WIDGET_RESIZE_MODE:Ljava/lang/String; = "resizeMode"

.field private static final WIDGET_SINGLE_INSTANCE:Ljava/lang/String; = "singleInstance"

.field private static final WIDGET_THEME:Ljava/lang/String; = "widgetTheme"

.field private static final WIDGET_UPDATE_TIME:Ljava/lang/String; = "updatePeriodMillis"

.field private static final XMLVAL_BOTH:Ljava/lang/String; = "both"

.field private static final XMLVAL_EASY_LAUNCHER:Ljava/lang/String; = "easylauncher"

.field private static final XMLVAL_FAVORITE:Ljava/lang/String; = "favorite"

.field private static final XMLVAL_HOME_SCREEN:Ljava/lang/String; = "home_screen"

.field private static final XMLVAL_HORIZONTAL:Ljava/lang/String; = "horizontal"

.field private static final XMLVAL_KEYGUARD:Ljava/lang/String; = "keyguard"

.field private static final XMLVAL_MAGAZINE:Ljava/lang/String; = "magazine"

.field private static final XMLVAL_NONE:Ljava/lang/String; = "none"

.field private static final XMLVAL_VERTICAL:Ljava/lang/String; = "vertical"


# instance fields
.field private final mMaxResizeHeight:I

.field private final mMaxResizeWidth:I

.field private final mThemeName:Ljava/lang/String;

.field private final mbOneInstanceOnly:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$1;

    invoke-direct {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$1;-><init>()V

    sput-object v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/res/Resources;Landroid/content/ComponentName;Landroid/content/res/XmlResourceParser;Landroid/content/pm/ResolveInfo;)V
    .locals 10
    .param p1, "pkgMgr"    # Landroid/content/pm/PackageManager;
    .param p2, "pkgRes"    # Landroid/content/res/Resources;
    .param p3, "provider"    # Landroid/content/ComponentName;
    .param p4, "xml"    # Landroid/content/res/XmlResourceParser;
    .param p5, "info"    # Landroid/content/pm/ResolveInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;,
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 297
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    .line 299
    iput-object p3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 302
    const-string v2, "minWidth"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    .line 303
    const-string v2, "minHeight"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    .line 304
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    if-gtz v2, :cond_1

    .line 305
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "minWidth / minHeight must be > 0"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 307
    :cond_1
    const-string v2, "configure"

    invoke-static {p4, p2, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 308
    .local v0, "configureClass":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 309
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    .line 312
    :cond_2
    const-string v2, "icon"

    invoke-interface {p4, v3, v2, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->icon:I

    .line 313
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->icon:I

    if-nez v2, :cond_3

    .line 314
    invoke-virtual {p5}, Landroid/content/pm/ResolveInfo;->getIconResource()I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->icon:I

    .line 317
    :cond_3
    const-string v2, "initialLayout"

    invoke-interface {p4, v3, v2, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialLayout:I

    .line 320
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v2, v4, :cond_4

    .line 322
    const-string v2, "initialKeyguardLayout"

    invoke-interface {p4, v3, v2, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialKeyguardLayout:I

    .line 325
    :cond_4
    const-string v2, "widgetTheme"

    invoke-static {p4, p2, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 326
    .local v1, "tempStr":Ljava/lang/String;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_5
    move-object v2, v3

    :goto_0
    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    .line 328
    const-string v2, "description"

    invoke-static {p4, p2, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    .line 329
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 330
    invoke-virtual {p5, p1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    .line 333
    :cond_6
    const-string v2, "singleInstance"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlBoolean(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    .line 336
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v2, v4, :cond_7

    .line 339
    const-string v2, "widgetCategory"

    const-string v4, "home_screen"

    invoke-static {p4, p2, v2, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    const-string v2, "home_screen"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 341
    iput v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    .line 355
    :cond_7
    :goto_1
    const-string v2, "resizeMode"

    const-string v4, "none"

    invoke-static {p4, p2, v2, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 356
    const-string v2, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 357
    iput v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    .line 370
    :goto_2
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    and-int/lit8 v2, v2, 0x1

    if-nez v2, :cond_10

    .line 371
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeWidth:I

    .line 377
    :goto_3
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    and-int/lit8 v2, v2, 0x2

    if-nez v2, :cond_11

    .line 378
    iget v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeHeight:I

    .line 384
    :goto_4
    const-string v2, "previewImage"

    invoke-interface {p4, v3, v2, v6}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->previewImage:I

    .line 386
    const-string v2, "updatePeriodMillis"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlInt(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->updatePeriodMillis:I

    .line 387
    return-void

    :cond_8
    move-object v2, v1

    .line 326
    goto :goto_0

    .line 342
    :cond_9
    const-string v2, "keyguard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 343
    iput v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    goto :goto_1

    .line 345
    :cond_a
    const-string v2, "both"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 346
    iput v9, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    goto :goto_1

    .line 349
    :cond_b
    const-string v2, "SurfaceWidgetInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Provider: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " specified an invalid widgetCategory of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iput v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    goto :goto_1

    .line 358
    :cond_c
    const-string v2, "horizontal"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 359
    iput v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    goto :goto_2

    .line 360
    :cond_d
    const-string v2, "vertical"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 361
    iput v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    goto :goto_2

    .line 362
    :cond_e
    const-string v2, "both"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 363
    iput v9, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    goto/16 :goto_2

    .line 366
    :cond_f
    const-string v2, "SurfaceWidgetInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Provider: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " specified an invalid resizeMode of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    iput v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    goto/16 :goto_2

    .line 373
    :cond_10
    const-string v2, "minResizeWidth"

    iget v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    invoke-static {p4, p2, v2, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeWidth:I

    .line 374
    const-string v2, "maxResizeWidth"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    goto/16 :goto_3

    .line 380
    :cond_11
    const-string v2, "minResizeHeight"

    iget v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    invoke-static {p4, p2, v2, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeHeight:I

    .line 381
    const-string v2, "maxResizeHeight"

    invoke-static {p4, p2, v2, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    goto/16 :goto_4
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x1

    .line 262
    invoke-direct {p0, p1}, Landroid/appwidget/AppWidgetProviderInfo;-><init>(Landroid/os/Parcel;)V

    .line 263
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    .line 265
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    .line 266
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    .line 267
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$1;

    .prologue
    .line 200
    invoke-direct {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method protected constructor <init>(Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;)V
    .locals 2
    .param p1, "that"    # Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    .prologue
    const/4 v1, 0x0

    .line 269
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProviderInfo;-><init>()V

    .line 271
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    .line 272
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minWidth:I

    .line 273
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minHeight:I

    .line 274
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeHeight:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeWidth:I

    .line 275
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeHeight:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->minResizeHeight:I

    .line 276
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->updatePeriodMillis:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->updatePeriodMillis:I

    .line 277
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialLayout:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialLayout:I

    .line 278
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialKeyguardLayout:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->initialKeyguardLayout:I

    .line 279
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    .line 280
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_2
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    .line 281
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->icon:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->icon:I

    .line 282
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->previewImage:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->previewImage:I

    .line 283
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->autoAdvanceViewId:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->autoAdvanceViewId:I

    .line 284
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->resizeMode:I

    .line 285
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->widgetCategory:I

    .line 288
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    .line 289
    iget-boolean v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    .line 290
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    .line 291
    iget v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    iput v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    .line 292
    return-void

    .line 271
    :cond_0
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->clone()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->configure:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->clone()Landroid/content/ComponentName;

    move-result-object v0

    goto :goto_1

    .line 280
    :cond_2
    iget-object v0, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->label:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public static create(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Landroid/content/ComponentName;Ljava/lang/String;)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;
    .param p2, "cn"    # Landroid/content/ComponentName;
    .param p3, "key"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x1

    .line 459
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 460
    .local v1, "pkgMgr":Landroid/content/pm/PackageManager;
    invoke-virtual {p2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    .line 462
    .local v2, "pkgRes":Landroid/content/res/Resources;
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-virtual {v0, v1, p3}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;

    move-result-object v4

    .line 465
    .local v4, "xml":Landroid/content/res/XmlResourceParser;
    if-eqz v4, :cond_1

    .line 466
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v9

    .line 468
    .local v9, "type":I
    :goto_0
    const/4 v0, 0x2

    if-eq v9, v0, :cond_0

    if-eq v9, v10, :cond_0

    .line 469
    invoke-interface {v4}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v9

    goto :goto_0

    .line 471
    :cond_0
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-object v3, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;-><init>(Landroid/content/pm/PackageManager;Landroid/content/res/Resources;Landroid/content/ComponentName;Landroid/content/res/XmlResourceParser;Landroid/content/pm/ResolveInfo;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4

    .line 496
    .end local v1    # "pkgMgr":Landroid/content/pm/PackageManager;
    .end local v2    # "pkgRes":Landroid/content/res/Resources;
    .end local v4    # "xml":Landroid/content/res/XmlResourceParser;
    .end local v9    # "type":I
    :goto_1
    return-object v0

    .line 473
    :catch_0
    move-exception v6

    .line 474
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v0, "SurfaceWidgetInfo"

    const-string v3, "failed to load find package"

    invoke-static {v0, v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 496
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_1
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 475
    :catch_1
    move-exception v6

    .line 476
    .local v6, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v0, "SurfaceWidgetInfo"

    const-string v3, "failed to read XML"

    invoke-static {v0, v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 477
    .end local v6    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_2
    move-exception v6

    .line 478
    .local v6, "e":Ljava/io/IOException;
    const-string v0, "SurfaceWidgetInfo"

    const-string v3, "failed to read XML"

    invoke-static {v0, v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 479
    .end local v6    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    .line 480
    .local v6, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v0, "SurfaceWidgetInfo"

    const-string v3, "XML resources failed"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 481
    .end local v6    # "e":Landroid/content/res/Resources$NotFoundException;
    :catch_4
    move-exception v6

    .line 482
    .local v6, "e":Ljava/lang/IllegalArgumentException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "\'s widget info needs to be updated"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 483
    .local v8, "msg":Ljava/lang/String;
    const-string v0, "SurfaceWidgetInfo"

    invoke-static {v0, v8, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 484
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v7

    .line 485
    .local v7, "mainLooper":Landroid/os/Looper;
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-ne v0, v7, :cond_2

    .line 486
    invoke-static {p0, v8, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_2

    .line 488
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, v7}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v3, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$2;

    invoke-direct {v3, p0, v8}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo$2;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method

.method private static loadXmlBoolean(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Z)Z
    .locals 5
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "pkgRes"    # Landroid/content/res/Resources;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Z

    .prologue
    const/4 v4, 0x0

    .line 501
    const/4 v3, 0x0

    invoke-interface {p0, v4, p2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 502
    .local v1, "refId":I
    if-eqz v1, :cond_0

    .line 504
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getBoolean(I)Z
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 511
    .local v2, "value":Z
    :goto_0
    return v2

    .line 505
    .end local v2    # "value":Z
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    move v2, p3

    .line 507
    .restart local v2    # "value":Z
    goto :goto_0

    .line 509
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "value":Z
    :cond_0
    invoke-interface {p0, v4, p2, p3}, Landroid/content/res/XmlResourceParser;->getAttributeBooleanValue(Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v2

    .restart local v2    # "value":Z
    goto :goto_0
.end method

.method private static loadXmlDimension(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I
    .locals 5
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "pkgRes"    # Landroid/content/res/Resources;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "defaultValue"    # I

    .prologue
    const/4 v4, 0x0

    .line 516
    const/4 v3, 0x0

    invoke-interface {p0, v4, p2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 517
    .local v1, "refId":I
    if-eqz v1, :cond_0

    .line 519
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 526
    .local v2, "value":I
    :goto_0
    return v2

    .line 520
    .end local v2    # "value":I
    :catch_0
    move-exception v0

    .line 521
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    move v2, p3

    .line 522
    .restart local v2    # "value":I
    goto :goto_0

    .line 524
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "value":I
    :cond_0
    invoke-interface {p0, v4, p2, p3}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .restart local v2    # "value":I
    goto :goto_0
.end method

.method private static loadXmlInt(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;I)I
    .locals 5
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "pkgRes"    # Landroid/content/res/Resources;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "defaultValue"    # I

    .prologue
    const/4 v4, 0x0

    .line 531
    const/4 v3, 0x0

    invoke-interface {p0, v4, p2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 532
    .local v1, "refId":I
    if-eqz v1, :cond_0

    .line 534
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 541
    .local v2, "value":I
    :goto_0
    return v2

    .line 535
    .end local v2    # "value":I
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    move v2, p3

    .line 537
    .restart local v2    # "value":I
    goto :goto_0

    .line 539
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "value":I
    :cond_0
    invoke-interface {p0, v4, p2, p3}, Landroid/content/res/XmlResourceParser;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v2

    .restart local v2    # "value":I
    goto :goto_0
.end method

.method private static loadXmlString(Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "parser"    # Landroid/content/res/XmlResourceParser;
    .param p1, "pkgRes"    # Landroid/content/res/Resources;
    .param p2, "attrName"    # Ljava/lang/String;
    .param p3, "defaultValue"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 546
    const/4 v3, 0x0

    invoke-interface {p0, v4, p2, v3}, Landroid/content/res/XmlResourceParser;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v1

    .line 547
    .local v1, "refId":I
    if-eqz v1, :cond_1

    .line 549
    :try_start_0
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 559
    .local v2, "value":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 550
    .end local v2    # "value":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 551
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    move-object v2, p3

    .line 552
    .restart local v2    # "value":Ljava/lang/String;
    goto :goto_0

    .line 554
    .end local v0    # "e":Landroid/content/res/Resources$NotFoundException;
    .end local v2    # "value":Ljava/lang/String;
    :cond_1
    invoke-interface {p0, v4, p2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 555
    .restart local v2    # "value":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 556
    move-object v2, p3

    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic clone()Landroid/appwidget/AppWidgetProviderInfo;
    .locals 1

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->clone()Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    .locals 1

    .prologue
    .line 453
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    invoke-direct {v0, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;-><init>(Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->clone()Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 429
    if-ne p1, p0, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v1

    .line 432
    :cond_1
    instance-of v3, p1, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    if-nez v3, :cond_2

    move v1, v2

    .line 433
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 435
    check-cast v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    .line 437
    .local v0, "item":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    iget-object v3, v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    .line 395
    const-string v0, "SurfaceWidgetInfo"

    const-string v1, "no class name set!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    const/4 v0, 0x0

    .line 399
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getComponentName()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    return-object v0
.end method

.method public getMaxResizeHeight()I
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    return v0
.end method

.method public getMaxResizeWidth()I
    .locals 1

    .prologue
    .line 407
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-nez v0, :cond_0

    .line 420
    const-string v0, "SurfaceWidgetInfo"

    const-string v1, "no package name set!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 421
    const/4 v0, 0x0

    .line 424
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getThemeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 443
    const/16 v0, 0x11

    .line 444
    .local v0, "hashCode":I
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->hashCode()I

    move-result v1

    add-int/lit16 v0, v1, 0x20f

    .line 445
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 446
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 448
    :cond_0
    return v0
.end method

.method public isOneInstanceOnly()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SurfaceWidgetProviderInfo(provider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 254
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProviderInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 255
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mThemeName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 256
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mbOneInstanceOnly:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 257
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeWidth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 258
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->mMaxResizeHeight:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    return-void

    .line 256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
