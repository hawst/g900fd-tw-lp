.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGL;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EGL"
.end annotation


# static fields
.field public static final CREATE_WINDOW_SURFACE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 212
    :try_start_0
    new-instance v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-string v2, "com.google.android.gles_jni.EGLImpl"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v3, "_eglCreateWindowSurface"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljavax/microedition/khronos/egl/EGLDisplay;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Ljavax/microedition/khronos/egl/EGLConfig;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Ljava/lang/Object;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-class v6, [I

    aput-object v6, v4, v5

    invoke-direct {v1, v2, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGL;->CREATE_WINDOW_SURFACE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 223
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "unable to find required class"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 221
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
