.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MethodHelper"
.end annotation


# instance fields
.field private final mMethod:Ljava/lang/reflect/Method;


# direct methods
.method public varargs constructor <init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V
    .locals 4
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 306
    .local p1, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .local p3, "paramTypes":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    :try_start_0
    invoke-virtual {p1, p2, p3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 310
    .local v1, "tempMethod":Ljava/lang/reflect/Method;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 316
    :goto_0
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->mMethod:Ljava/lang/reflect/Method;

    .line 317
    return-void

    .line 311
    .end local v1    # "tempMethod":Ljava/lang/reflect/Method;
    :catch_0
    move-exception v0

    .line 312
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find method"

    invoke-static {v2, v3, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313
    const/4 v1, 0x0

    .restart local v1    # "tempMethod":Ljava/lang/reflect/Method;
    goto :goto_0
.end method


# virtual methods
.method public varargs invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1, "target"    # Ljava/lang/Object;
    .param p2, "args"    # [Ljava/lang/Object;

    .prologue
    .line 320
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->mMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 322
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->mMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v1, p1, p2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 328
    :goto_0
    return-object v1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "failed to invoke method"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 328
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isValid()Z
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;->mMethod:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
