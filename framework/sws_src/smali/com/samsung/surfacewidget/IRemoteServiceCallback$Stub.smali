.class public abstract Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;
.super Landroid/os/Binder;
.source "IRemoteServiceCallback.java"

# interfaces
.implements Lcom/samsung/surfacewidget/IRemoteServiceCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/IRemoteServiceCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.surfacewidget.IRemoteServiceCallback"

.field static final TRANSACTION_acceptedFocus:I = 0x7

.field static final TRANSACTION_acceptedSurfaceSize:I = 0xb

.field static final TRANSACTION_checkVersion:I = 0x1

.field static final TRANSACTION_releaseFocus:I = 0x8

.field static final TRANSACTION_requestDestroy:I = 0x9

.field static final TRANSACTION_requestKeyboard:I = 0x3

.field static final TRANSACTION_requestPositionUpdates:I = 0x4

.field static final TRANSACTION_requestSizeChange:I = 0x2

.field static final TRANSACTION_requestTalkbackSay:I = 0x6

.field static final TRANSACTION_requestTilt:I = 0x5

.field static final TRANSACTION_setOpaqueness:I = 0xa

.field static final TRANSACTION_startActivity:I = 0xd

.field static final TRANSACTION_updateContentDescription:I = 0xc


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 22
    const-string v0, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/surfacewidget/IRemoteServiceCallback;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 30
    if-nez p0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    .line 33
    :cond_0
    const-string v1, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 34
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    if-eqz v1, :cond_1

    .line 35
    check-cast v0, Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 41
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 45
    sparse-switch p1, :sswitch_data_0

    .line 161
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 49
    :sswitch_0
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 54
    :sswitch_1
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 57
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->checkVersion(I)V

    goto :goto_0

    .line 62
    .end local v0    # "_arg0":I
    :sswitch_2
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 66
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 67
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestSizeChange(II)V

    goto :goto_0

    .line 72
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    :sswitch_3
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 76
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 77
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestKeyboard(ILjava/lang/String;)V

    goto :goto_0

    .line 82
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    :sswitch_4
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    .line 85
    .local v0, "_arg0":Z
    :cond_0
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestPositionUpdates(Z)V

    goto :goto_0

    .line 90
    .end local v0    # "_arg0":Z
    :sswitch_5
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    move v0, v2

    .line 93
    .restart local v0    # "_arg0":Z
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestTilt(Z)V

    goto :goto_0

    .line 98
    .end local v0    # "_arg0":Z
    :sswitch_6
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 101
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestTalkbackSay(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_7
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    .line 109
    .local v0, "_arg0":Z
    :cond_2
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->acceptedFocus(Z)V

    goto :goto_0

    .line 114
    .end local v0    # "_arg0":Z
    :sswitch_8
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->releaseFocus()V

    goto :goto_0

    .line 120
    :sswitch_9
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 123
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->requestDestroy(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 128
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_a
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    move v0, v2

    .line 131
    .local v0, "_arg0":Z
    :cond_3
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->setOpaqueness(Z)V

    goto/16 :goto_0

    .line 136
    .end local v0    # "_arg0":Z
    :sswitch_b
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 140
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 141
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->acceptedSurfaceSize(II)V

    goto/16 :goto_0

    .line 146
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":I
    :sswitch_c
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->updateContentDescription(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 154
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_d
    const-string v3, "com.samsung.surfacewidget.IRemoteServiceCallback"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 156
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 157
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/IRemoteServiceCallback$Stub;->startActivity(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 45
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
