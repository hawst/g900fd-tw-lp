.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "View"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;
    }
.end annotation


# static fields
.field public static final DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

.field public static final M_ATTACH_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_PARENT:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 126
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-class v1, Landroid/view/View;

    const-string v2, "mAttachInfo"

    invoke-direct {v0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;->M_ATTACH_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 127
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-class v1, Landroid/view/View;

    const-string v2, "mParent"

    invoke-direct {v0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;->M_PARENT:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 128
    new-instance v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-class v1, Landroid/view/View;

    const-string v2, "dispatchDetachedFromWindow"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Class;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v0, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;->DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    .line 130
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
