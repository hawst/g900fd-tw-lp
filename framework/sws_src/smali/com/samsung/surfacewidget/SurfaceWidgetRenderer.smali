.class public Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
.super Ljava/lang/Thread;
.source "SurfaceWidgetRenderer.java"


# static fields
.field private static final DEBUGGABLE:Z = true

.field private static final TAG:Ljava/lang/String; = "SurfaceWidget.Renderer"

.field private static final UNDEFINED_ORIENTATION:I = -0x1


# instance fields
.field protected mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

.field protected final mContext:Landroid/content/Context;

.field private mErrorString:Ljava/lang/String;

.field private mHasError:Z

.field protected final mInstance:I

.field mPauseTime:J

.field private mPreviousTime:J

.field protected mRedrawNeeded:Z

.field private mRedrawOnlyWhenDirty:Z

.field protected final mResources:Landroid/content/res/Resources;

.field mResourcesReleased:Z

.field protected mSurface:Landroid/view/Surface;

.field protected mSurfaceHeight:I

.field protected mSurfaceSpanX:I

.field protected mSurfaceSpanY:I

.field protected mSurfaceWidth:I

.field protected final mUIHandler:Landroid/os/Handler;

.field private mUpdateTime:I

.field mbDrawOneFrame:Z

.field protected volatile mbFinished:Z

.field protected mbHasFocus:Z

.field protected mbPaused:Z

.field private mbResumeFirstFrame:Z

.field mbResumeNeedsRecalled:Z

.field mbSizeChangedLock:Z

.field mbStartCalled:Z

.field mbSurfaceSizeChanged:Z

.field protected mbUseNewSurface:Z

.field protected mbVisible:Z

.field private renderArea:Landroid/graphics/Rect;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "instance"    # I
    .param p4, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 221
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 44
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbStartCalled:Z

    .line 45
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeNeedsRecalled:Z

    .line 54
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbVisible:Z

    .line 63
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbFinished:Z

    .line 72
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbHasFocus:Z

    .line 81
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    .line 128
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    .line 136
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbUseNewSurface:Z

    .line 138
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    .line 146
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    .line 187
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawOnlyWhenDirty:Z

    .line 196
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    .line 198
    iput v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    .line 200
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeFirstFrame:Z

    .line 204
    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 206
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mResourcesReleased:Z

    .line 207
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPauseTime:J

    .line 209
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mHasError:Z

    .line 210
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mErrorString:Ljava/lang/String;

    .line 212
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSizeChangedLock:Z

    .line 626
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->renderArea:Landroid/graphics/Rect;

    .line 222
    iput-object p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mResources:Landroid/content/res/Resources;

    .line 223
    iput-object p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mContext:Landroid/content/Context;

    .line 224
    iput p3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    .line 225
    iput-object p4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    .line 226
    return-void
.end method


# virtual methods
.method clearNewSurface()V
    .locals 9

    .prologue
    .line 630
    const-string v4, "SURFACETEST"

    const-string v5, "Broadcast message sending function is added(When errors occured in clearNewSurface())"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.sec.android.test.clearnewsurfaceerror"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 633
    .local v2, "errorintent":Landroid/content/Intent;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->renderArea:Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v8

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 634
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getSurface()Landroid/view/Surface;

    move-result-object v3

    .line 635
    .local v3, "surface":Landroid/view/Surface;
    if-nez v3, :cond_1

    .line 637
    const-string v4, "SurfaceWidget.Renderer"

    const-string v5, "Can\'t find valid Surface. Is your Surface ready?"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    .end local v3    # "surface":Landroid/view/Surface;
    :cond_0
    :goto_0
    return-void

    .line 640
    .restart local v3    # "surface":Landroid/view/Surface;
    :cond_1
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->renderArea:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v0

    .line 641
    .local v0, "drawingCanvas":Landroid/graphics/Canvas;
    if-eqz v0, :cond_0

    .line 643
    monitor-enter p0
    :try_end_0
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    .line 644
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    :try_start_1
    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 645
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 647
    :try_start_2
    invoke-virtual {v3, v0}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    .line 649
    .end local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .end local v3    # "surface":Landroid/view/Surface;
    :catch_0
    move-exception v1

    .line 650
    .local v1, "e":Landroid/view/Surface$OutOfResourcesException;
    const-string v4, "SURFACETEST"

    const-string v5, "OutOfResourcesException is occured in clearNewSurface"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 652
    invoke-virtual {v1}, Landroid/view/Surface$OutOfResourcesException;->printStackTrace()V

    goto :goto_0

    .line 645
    .end local v1    # "e":Landroid/view/Surface$OutOfResourcesException;
    .restart local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .restart local v3    # "surface":Landroid/view/Surface;
    :catchall_0
    move-exception v4

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v4
    :try_end_4
    .catch Landroid/view/Surface$OutOfResourcesException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2

    .line 653
    .end local v0    # "drawingCanvas":Landroid/graphics/Canvas;
    .end local v3    # "surface":Landroid/view/Surface;
    :catch_1
    move-exception v1

    .line 654
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "SURFACETEST"

    const-string v5, "IllegalArgumentException is occured in clearNewSurface"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 656
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 657
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 658
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v4, "SURFACETEST"

    const-string v5, "NullPointerException is occured in clearNewSurface"

    invoke-static {v4, v5}, Lcom/samsung/surfacewidget/Log;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 659
    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 660
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0
.end method

.method protected finish()V
    .locals 1

    .prologue
    .line 1020
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbFinished:Z

    .line 1022
    monitor-enter p0

    .line 1023
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 1024
    monitor-exit p0

    .line 1025
    return-void

    .line 1024
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mErrorString:Ljava/lang/String;

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 768
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    return v0
.end method

.method public getInstanceID()I
    .locals 1

    .prologue
    .line 846
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    return v0
.end method

.method protected getOpaqueness()Z
    .locals 1

    .prologue
    .line 1035
    const/4 v0, 0x0

    return v0
.end method

.method public getResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method public getSpanX()I
    .locals 1

    .prologue
    .line 814
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanX:I

    return v0
.end method

.method public getSpanY()I
    .locals 1

    .prologue
    .line 822
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanY:I

    return v0
.end method

.method public getSurface()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    return-object v0
.end method

.method public getUIHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 854
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public getUpdateTime()I
    .locals 1

    .prologue
    .line 862
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 759
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    return v0
.end method

.method handleClear()V
    .locals 2

    .prologue
    .line 620
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 621
    .local v0, "renderMsg":Landroid/os/Message;
    const/16 v1, 0xa

    iput v1, v0, Landroid/os/Message;->what:I

    .line 622
    iget v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 623
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 624
    return-void
.end method

.method handleOnDraw()V
    .locals 2

    .prologue
    .line 609
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 610
    .local v0, "renderMsg":Landroid/os/Message;
    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    .line 611
    iget v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 612
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 613
    return-void
.end method

.method handleOnPostdraw()V
    .locals 0

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPostdraw()V

    .line 601
    return-void
.end method

.method handleOnPredraw()V
    .locals 1

    .prologue
    .line 588
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mResourcesReleased:Z

    if-eqz v0, :cond_0

    .line 589
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mResourcesReleased:Z

    .line 590
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->restoreResources()V

    .line 592
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onPredraw()V

    .line 593
    return-void
.end method

.method hasError()Z
    .locals 1

    .prologue
    .line 562
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mHasError:Z

    return v0
.end method

.method public hasFocus()Z
    .locals 1

    .prologue
    .line 887
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbHasFocus:Z

    return v0
.end method

.method public isPaused()Z
    .locals 1

    .prologue
    .line 807
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    return v0
.end method

.method isSizeChangedLocked()Z
    .locals 1

    .prologue
    .line 380
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSizeChangedLock:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 802
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbVisible:Z

    return v0
.end method

.method lockSizeChanged()V
    .locals 2

    .prologue
    .line 369
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "lockSizeChanged"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSizeChangedLock:Z

    .line 371
    return-void
.end method

.method onContentDescriptionRequest()V
    .locals 5

    .prologue
    .line 441
    const-string v2, "SurfaceWidget.Renderer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onContentDescriptionRequest() instance: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onContentDescriptionUpdate()Ljava/lang/String;

    move-result-object v1

    .line 445
    .local v1, "freshContentDescription":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->updateContentDescription(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 449
    :goto_0
    return-void

    .line 446
    :catch_0
    move-exception v0

    .line 447
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onContentDescriptionUpdate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 460
    const-string v0, ""

    .line 462
    .local v0, "newContent":Ljava/lang/String;
    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;Landroid/view/View;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "mainView"    # Landroid/view/View;

    .prologue
    .line 282
    invoke-virtual {p2, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 283
    return-void
.end method

.method protected onKeyEvent(Landroid/view/KeyEvent;)V
    .locals 2
    .param p1, "aKeyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 968
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "onKeyEvent"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    return-void
.end method

.method protected onKeyboardCompleted(ILjava/lang/String;)V
    .locals 2
    .param p1, "textViewId"    # I
    .param p2, "charactersEntered"    # Ljava/lang/String;

    .prologue
    .line 978
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "onKeyboardCompleted"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    return-void
.end method

.method protected onLauncherTiltChanged(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 1000
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "onLauncherTiltChanged"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1001
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    .line 244
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPauseTime:J

    .line 245
    return-void
.end method

.method protected onPositionOffsetChanged(FFF)V
    .locals 2
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 1013
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "onPositionOffsetChanged"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1014
    return-void
.end method

.method protected onPostdraw()V
    .locals 0

    .prologue
    .line 339
    return-void
.end method

.method protected onPredraw()V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeFirstFrame:Z

    .line 255
    monitor-enter p0

    .line 256
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 257
    monitor-exit p0

    .line 258
    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "spanX"    # I
    .param p4, "spanY"    # I

    .prologue
    .line 351
    const-string v0, "SurfaceWidget.Renderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSizeChanged: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 356
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestLayout()V

    .line 357
    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->isRotationEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 358
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestDraw()V

    .line 360
    monitor-enter p0

    .line 361
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 362
    monitor-exit p0

    .line 365
    :cond_0
    return-void

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onStart()V
    .locals 2

    .prologue
    .line 266
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "SurfaceWidgetRenderer onStart"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 269
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestOnStart()V

    .line 270
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 292
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "SurfaceWidgetRenderer onStop"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->releaseSurface()V

    .line 294
    return-void
.end method

.method protected onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "aMotionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 987
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "onTouchEvent"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    return-void
.end method

.method protected redrawNeeded()Z
    .locals 1

    .prologue
    .line 421
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    return v0
.end method

.method protected releaseResources()V
    .locals 0

    .prologue
    .line 322
    return-void
.end method

.method protected declared-synchronized releaseSurface()V
    .locals 3

    .prologue
    .line 302
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 303
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "Release Surface"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 304
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    .line 306
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "Release Surface:DONE"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->isRotationEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    :cond_0
    monitor-exit p0

    return-void

    .line 302
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method requestDestroy(Ljava/lang/String;)V
    .locals 4
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 430
    const-string v1, "SurfaceWidget.Renderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestDestroy instance/reason/callback: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 433
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestDestroy(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 435
    :catch_0
    move-exception v0

    .line 436
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public requestDraw()V
    .locals 1

    .prologue
    .line 409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    .line 410
    monitor-enter p0

    .line 412
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 413
    monitor-exit p0

    .line 414
    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method requestLayout()V
    .locals 2

    .prologue
    .line 388
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 389
    .local v0, "layoutMsg":Landroid/os/Message;
    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    .line 390
    iget v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 391
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 392
    return-void
.end method

.method protected requestNewSurfaceSize(II)V
    .locals 3
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 929
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    if-nez v1, :cond_0

    .line 930
    const-string v1, "SurfaceWidget.Renderer"

    const-string v2, "requestNewSurface failed because no callback to launcher set!"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 939
    :goto_0
    return-void

    .line 935
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestSizeChange(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 936
    :catch_0
    move-exception v0

    .line 937
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method requestOnStart()V
    .locals 2

    .prologue
    .line 399
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 400
    .local v0, "msg":Landroid/os/Message;
    const/4 v1, 0x0

    iput v1, v0, Landroid/os/Message;->what:I

    .line 401
    iget v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 402
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUIHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 403
    return-void
.end method

.method protected requestTilt(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    .line 949
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    if-nez v1, :cond_0

    .line 950
    const-string v1, "SurfaceWidget.Renderer"

    const-string v2, "requestTilt failed because no callback to launcher set!"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 959
    :goto_0
    return-void

    .line 955
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestTilt(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 956
    :catch_0
    move-exception v0

    .line 957
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected restoreResources()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    .line 472
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onStart()V

    .line 473
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPreviousTime:J

    .line 474
    :goto_0
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbFinished:Z

    if-nez v3, :cond_2

    .line 476
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbUseNewSurface:Z

    if-eqz v3, :cond_1

    .line 478
    instance-of v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;

    if-eqz v3, :cond_0

    move-object v3, p0

    .line 480
    check-cast v3, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;

    invoke-virtual {v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->reinitEGLSurface()V

    .line 483
    :cond_0
    iput-boolean v14, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbUseNewSurface:Z

    .line 486
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->hasError()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 487
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getError()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestDestroy(Ljava/lang/String;)V

    .line 554
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onStop()V

    .line 555
    return-void

    .line 491
    :cond_3
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbPaused:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbVisible:Z

    if-eqz v3, :cond_4

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    if-nez v3, :cond_5

    instance-of v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;

    if-nez v3, :cond_5

    :cond_4
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    if-nez v3, :cond_5

    .line 493
    monitor-enter p0

    .line 496
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 501
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 498
    :catch_0
    move-exception v2

    .line 499
    .local v2, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v3, "SurfaceWidget.Renderer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "SurfaceWidgetRenderer waiting done by interrupt: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mInstance:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " -- "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 504
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_5
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    move-result v3

    if-nez v3, :cond_7

    .line 505
    :cond_6
    const-string v3, "SurfaceWidget.Renderer"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "invalid surface in SurfaceWidget mSurface:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Lcom/samsung/surfacewidget/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 510
    :cond_7
    const-wide/16 v4, 0x5

    .line 512
    .local v4, "kMinSleepPerFrame":J
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    .line 513
    .local v8, "time":J
    iget-wide v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPreviousTime:J

    sub-long v0, v8, v10

    .line 514
    .local v0, "dt":J
    iput-wide v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPreviousTime:J

    .line 515
    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    int-to-long v6, v3

    .line 517
    .local v6, "sleepTime":J
    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    int-to-long v10, v3

    cmp-long v3, v0, v10

    if-lez v3, :cond_c

    .line 518
    const-wide/16 v6, 0x5

    .line 523
    :goto_2
    :try_start_3
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeFirstFrame:Z

    if-nez v3, :cond_8

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    if-nez v3, :cond_8

    .line 525
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V

    .line 526
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iput-wide v10, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mPreviousTime:J

    .line 529
    :cond_8
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeFirstFrame:Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 534
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->handleOnPredraw()V

    .line 535
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawOnlyWhenDirty:Z

    if-eqz v3, :cond_9

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    if-eqz v3, :cond_b

    .line 537
    :cond_9
    monitor-enter p0

    .line 540
    :try_start_4
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    if-eqz v3, :cond_a

    .line 542
    const-string v3, "SurfaceWidget.Renderer"

    const-string v10, "SurfaceWidget drawing first frame"

    invoke-static {v3, v10}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->handleOnDraw()V

    .line 546
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawNeeded:Z

    .line 547
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 548
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 550
    :cond_b
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->handleOnPostdraw()V

    goto/16 :goto_0

    .line 520
    :cond_c
    const-wide/16 v10, 0x5

    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    int-to-long v12, v3

    sub-long/2addr v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_2

    .line 530
    :catch_1
    move-exception v2

    .line 531
    .restart local v2    # "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_3

    .line 548
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :catchall_1
    move-exception v3

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v3
.end method

.method protected setCallback(Lcom/samsung/surfacewidget/IRemoteServiceCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    .prologue
    .line 895
    iput-object p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mCallback:Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    .line 896
    return-void
.end method

.method setError(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "label"    # Ljava/lang/String;
    .param p2, "reason"    # Ljava/lang/String;

    .prologue
    .line 578
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mErrorString:Ljava/lang/String;

    .line 579
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mHasError:Z

    .line 580
    const-string v0, "SurfaceWidget.Renderer"

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mErrorString:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    return-void
.end method

.method protected setFocus(Z)Z
    .locals 1
    .param p1, "focusOn"    # Z

    .prologue
    .line 881
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbHasFocus:Z

    .line 882
    const/4 v0, 0x0

    return v0
.end method

.method public setRedrawOnlyWhenDirty(Z)V
    .locals 0
    .param p1, "on"    # Z

    .prologue
    .line 235
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mRedrawOnlyWhenDirty:Z

    .line 236
    return-void
.end method

.method protected declared-synchronized setSurface(Landroid/view/Surface;IIII)V
    .locals 10
    .param p1, "aSurface"    # Landroid/view/Surface;
    .param p2, "aWidth"    # I
    .param p3, "aHeight"    # I
    .param p4, "aSpanX"    # I
    .param p5, "aSpanY"    # I

    .prologue
    .line 676
    monitor-enter p0

    :try_start_0
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v6}, Landroid/view/Surface;->isValid()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v6}, Landroid/view/Surface;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/view/Surface;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v5, 0x1

    .line 678
    .local v5, "isSameSurface":Z
    :goto_0
    const-string v6, "SurfaceWidget.Renderer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "New Dimensions: aWidth= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | aHeight= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | aSpanX= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " | aspanY= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    if-lez p2, :cond_4

    if-lez p3, :cond_4

    const/4 v4, 0x1

    .line 681
    .local v4, "isDimensValid":Z
    :goto_1
    iget v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    if-ne v6, p2, :cond_0

    iget v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    if-eq v6, p3, :cond_5

    :cond_0
    const/4 v6, 0x1

    :goto_2
    iput-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    .line 683
    const-string v6, "SurfaceWidget.Renderer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mbSurfaceSizeChanged = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-boolean v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 684
    const-string v6, "SurfaceWidget.Renderer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "isSameSurface = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    iget-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    if-nez v6, :cond_1

    if-nez v5, :cond_2

    :cond_1
    if-nez v4, :cond_6

    .line 689
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->unlockSizeChanged()V

    .line 690
    invoke-virtual {p1}, Landroid/view/Surface;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 751
    :goto_3
    monitor-exit p0

    return-void

    .line 676
    .end local v4    # "isDimensValid":Z
    .end local v5    # "isSameSurface":Z
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 679
    .restart local v5    # "isSameSurface":Z
    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 681
    .restart local v4    # "isDimensValid":Z
    :cond_5
    const/4 v6, 0x0

    goto :goto_2

    .line 694
    :cond_6
    :try_start_1
    iget-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSurfaceSizeChanged:Z

    if-eqz v6, :cond_7

    if-eqz v5, :cond_7

    .line 696
    iput p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    .line 697
    iput p3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    .line 698
    iput p4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanX:I

    .line 699
    iput p5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanY:I

    .line 701
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->unlockSizeChanged()V

    .line 703
    iget v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    iget v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    iget v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanX:I

    iget v9, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanY:I

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onSizeChanged(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_3

    .line 676
    .end local v4    # "isDimensValid":Z
    .end local v5    # "isSameSurface":Z
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 708
    .restart local v4    # "isDimensValid":Z
    .restart local v5    # "isSameSurface":Z
    :cond_7
    const/4 v6, 0x1

    :try_start_2
    iput-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbUseNewSurface:Z

    .line 710
    iput p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    .line 711
    iput p3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    .line 712
    iput p4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanX:I

    .line 713
    iput p5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanY:I

    .line 715
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->releaseSurface()V

    .line 717
    iput-object p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    .line 720
    instance-of v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v6, :cond_9

    .line 726
    :try_start_3
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    const-string v7, "mCanvas"

    invoke-virtual {v6, v7}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 727
    .local v0, "canvas":Ljava/lang/reflect/Field;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 729
    :try_start_4
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 730
    .local v1, "currentCanvas":Ljava/lang/Object;
    if-nez v1, :cond_8

    .line 731
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurface:Landroid/view/Surface;

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7}, Landroid/graphics/Canvas;-><init>()V

    invoke-virtual {v0, v6, v7}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 742
    .end local v0    # "canvas":Ljava/lang/reflect/Field;
    .end local v1    # "currentCanvas":Ljava/lang/Object;
    :cond_8
    :goto_4
    :try_start_5
    const-string v6, "SurfaceWidget.Renderer"

    const-string v7, "ClearSurface"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 743
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->clearNewSurface()V

    .line 746
    :cond_9
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->unlockSizeChanged()V

    .line 750
    iget v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceWidth:I

    iget v7, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceHeight:I

    iget v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanX:I

    iget v9, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mSurfaceSpanY:I

    invoke-virtual {p0, v6, v7, v8, v9}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onSizeChanged(IIII)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 732
    .restart local v0    # "canvas":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v2

    .line 733
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    :try_start_6
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/NoSuchFieldException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    .line 737
    .end local v0    # "canvas":Ljava/lang/reflect/Field;
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v3

    .line 738
    .local v3, "e1":Ljava/lang/NoSuchFieldException;
    :try_start_7
    invoke-virtual {v3}, Ljava/lang/NoSuchFieldException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 734
    .end local v3    # "e1":Ljava/lang/NoSuchFieldException;
    .restart local v0    # "canvas":Ljava/lang/reflect/Field;
    :catch_2
    move-exception v2

    .line 735
    .local v2, "e":Ljava/lang/IllegalAccessException;
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_8
    .catch Ljava/lang/NoSuchFieldException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4
.end method

.method protected setUpdateTime(I)V
    .locals 3
    .param p1, "updateTimeMillis"    # I

    .prologue
    .line 906
    iput p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mUpdateTime:I

    .line 908
    const-string v0, "SurfaceWidget.Renderer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateTime set on renderer = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 909
    monitor-enter p0

    .line 910
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 911
    monitor-exit p0

    .line 912
    return-void

    .line 911
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setUpdateTimePublic(I)V
    .locals 0
    .param p1, "updateTimeMillis"    # I

    .prologue
    .line 920
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->setUpdateTime(I)V

    .line 921
    return-void
.end method

.method protected setVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 787
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbVisible:Z

    .line 789
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbVisible:Z

    if-eqz v0, :cond_0

    .line 791
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbDrawOneFrame:Z

    .line 792
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestDraw()V

    .line 793
    monitor-enter p0

    .line 795
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 796
    monitor-exit p0

    .line 798
    :cond_0
    return-void

    .line 796
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method unlockSizeChanged()V
    .locals 2

    .prologue
    .line 375
    const-string v0, "SurfaceWidget.Renderer"

    const-string v1, "unlockSizeChanged"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbSizeChangedLock:Z

    .line 377
    return-void
.end method
