.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGLSurface;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "EGLSurface"
.end annotation


# static fields
.field private static final CONSTRUCTOR:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 232
    :try_start_0
    const-string v1, "com.google.android.gles_jni.EGLSurfaceImpl"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    sput-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGLSurface;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    .line 234
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGLSurface;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    .line 242
    return-void

    .line 235
    :catch_0
    move-exception v0

    .line 236
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "unable to find required constructor"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 237
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 238
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "unable to find required class"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 240
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInst(Ljava/lang/Integer;)Ljava/lang/Object;
    .locals 4
    .param p0, "surfaceId"    # Ljava/lang/Integer;

    .prologue
    .line 246
    :try_start_0
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$EGLSurface;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 251
    :goto_0
    return-object v1

    .line 247
    :catch_0
    move-exception v0

    .line 248
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "failed to construct a new EGLSurface"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 251
    const/4 v1, 0x0

    goto :goto_0
.end method
