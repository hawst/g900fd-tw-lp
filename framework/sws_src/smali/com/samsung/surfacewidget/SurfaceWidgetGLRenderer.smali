.class public abstract Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;
.super Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
.source "SurfaceWidgetGLRenderer.java"


# static fields
.field private static final DEBUGGABLE:Z = true

.field static final EGL_CONTEXT_CLIENT_VERSION:I = 0x3098

.field private static final EGL_ERROR:Ljava/lang/String; = "EGL Error"

.field static final EGL_OPENGL_ES2_BIT:I = 0x4

.field private static final OPENGL_ERROR:Ljava/lang/String; = "OpenGL Error"

.field private static final TAG:Ljava/lang/String; = "SurfaceWidget.GLRenderer"


# instance fields
.field private mEglConfig:Landroid/opengl/EGLConfig;

.field private mEglContext:Landroid/opengl/EGLContext;

.field protected mEglDisplay:Landroid/opengl/EGLDisplay;

.field protected mEglSurface:Landroid/opengl/EGLSurface;

.field private mbEglInitialized:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "instance"    # I
    .param p4, "handler"    # Landroid/os/Handler;

    .prologue
    .line 49
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbEglInitialized:Z

    .line 50
    return-void
.end method

.method private chooseEglConfig()Landroid/opengl/EGLConfig;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 364
    new-array v6, v5, [I

    .line 365
    .local v6, "configsCount":[I
    new-array v3, v5, [Landroid/opengl/EGLConfig;

    .line 366
    .local v3, "configs":[Landroid/opengl/EGLConfig;
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->getConfig()[I

    move-result-object v1

    .line 367
    .local v1, "configSpec":[I
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    move v4, v2

    move v7, v2

    invoke-static/range {v0 .. v7}, Landroid/opengl/EGL14;->eglChooseConfig(Landroid/opengl/EGLDisplay;[II[Landroid/opengl/EGLConfig;II[II)Z

    move-result v0

    if-nez v0, :cond_1

    .line 368
    const-string v0, "EGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "eglChooseConfig failed "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 369
    :cond_1
    aget v0, v6, v2

    if-lez v0, :cond_0

    .line 370
    aget-object v0, v3, v2

    goto :goto_0
.end method

.method private clearViewPort()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 267
    const-string v0, "SurfaceWidget.GLRenderer"

    const-string v1, "clearing viewport"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    iget v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurfaceWidth:I

    iget v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurfaceHeight:I

    invoke-static {v3, v3, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 269
    invoke-static {v2, v2, v2, v2}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 270
    const/16 v0, 0x4100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 271
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    const-string v0, "EGL Error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initial eglSwapBuffers failed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :cond_0
    return-void
.end method

.method private destroyEGLSurface()V
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    const-string v0, "EGL Error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglMakeCurrent in finishGL() failed with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    const-string v0, "EGL Error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglDestroySurface in finishGL() failed with error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_1
    return-void
.end method

.method private getConfig()[I
    .locals 1

    .prologue
    .line 376
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x10
        0x3026
        0x0
        0x3038
    .end array-data
.end method


# virtual methods
.method protected checkCurrent()V
    .locals 4

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbEglInitialized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    if-nez v0, :cond_2

    .line 253
    :cond_0
    const-string v0, "EGL Error"

    const-string v1, "invalid egl/eglContext/eglSurface"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    :cond_1
    :goto_0
    return-void

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    invoke-static {}, Landroid/opengl/EGL14;->eglGetCurrentContext()Landroid/opengl/EGLContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/opengl/EGLContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    const/16 v1, 0x3059

    invoke-static {v1}, Landroid/opengl/EGL14;->eglGetCurrentSurface(I)Landroid/opengl/EGLSurface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/opengl/EGLSurface;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 258
    :cond_3
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    const-string v0, "EGL Error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglMakeCurrent failed error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected checkEglError()I
    .locals 4

    .prologue
    .line 113
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v0

    .line 114
    .local v0, "error":I
    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 115
    const-string v1, "SurfaceWidget.GLRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EGL error = 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v1, "EGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    return v0
.end method

.method protected checkGlError()I
    .locals 4

    .prologue
    .line 189
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    .line 190
    .local v0, "error":I
    if-eqz v0, :cond_0

    .line 191
    const-string v1, "OpenGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "error code: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    .end local v0    # "error":I
    :goto_0
    return v0

    .restart local v0    # "error":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method createContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;)Landroid/opengl/EGLContext;
    .locals 3
    .param p1, "eglDisplay"    # Landroid/opengl/EGLDisplay;
    .param p2, "eglConfig"    # Landroid/opengl/EGLConfig;

    .prologue
    .line 359
    const/4 v1, 0x3

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 360
    .local v0, "attrib_list":[I
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v0, v2}, Landroid/opengl/EGL14;->eglCreateContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Landroid/opengl/EGLContext;[II)Landroid/opengl/EGLContext;

    move-result-object v1

    return-object v1

    .line 359
    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method createEGLSurface()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 339
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    .line 341
    :cond_0
    const-string v1, "SurfaceWidget.GLRenderer"

    const-string v2, "mSurface wasn\'t valid to create EGL surface with"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 356
    :cond_1
    :goto_0
    return-void

    .line 348
    :cond_2
    const/4 v1, 0x1

    new-array v0, v1, [I

    const/16 v1, 0x3038

    aput v1, v0, v4

    .line 349
    .local v0, "attrib_list":[I
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglConfig:Landroid/opengl/EGLConfig;

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    invoke-static {v1, v2, v3, v0, v4}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 350
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-ne v1, v2, :cond_1

    .line 352
    :cond_3
    const-string v1, "EGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "eglCreateWindowSurface failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v3

    invoke-static {v3}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    goto :goto_0
.end method

.method protected finishGL()V
    .locals 4

    .prologue
    .line 215
    const-string v1, "SurfaceWidget.GLRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finishGL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mInstance:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    if-nez v1, :cond_0

    .line 219
    const-string v1, "SurfaceWidget.GLRenderer"

    const-string v2, "mEglDisplay null in finishGL...must be destroyed already"

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    :goto_0
    return-void

    .line 225
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->destroyEGLSurface()V

    .line 227
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 229
    const-string v1, "EGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "eglDestroyContext in finishGL() failed with error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v3

    invoke-static {v3}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_1
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v1}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 234
    const-string v1, "EGL Error"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "eglTerminate in finishGL() failed with error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v3

    invoke-static {v3}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbEglInitialized:Z

    .line 238
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    .line 239
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    .line 240
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidget.GLRenderer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "finishGL: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method handleOnDraw()V
    .locals 6

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->checkCurrent()V

    .line 127
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->isSizeChangedLocked()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 128
    const-string v3, "SurfaceWidget.GLRenderer"

    const-string v4, "handleOnDraw sizeChangedLocked! skip onGLDraw"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->checkGlError()I

    move-result v3

    if-eqz v3, :cond_2

    .line 132
    const-string v3, "SurfaceWidget.GLRenderer"

    const-string v4, "start handleOnDraw has OpenGL error!"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 135
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbSurfaceSizeChanged:Z

    .line 136
    .local v2, "surfaceSizeChange":Z
    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbSurfaceSizeChanged:Z

    if-eqz v3, :cond_3

    .line 137
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbSurfaceSizeChanged:Z

    .line 138
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->clearViewPort()V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->releaseResources()V

    .line 141
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->restoreResources()V

    .line 144
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->hasError()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 145
    const-string v3, "SurfaceWidget.GLRenderer"

    const-string v4, "handleOnDraw hasError! skip onGLDraw"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 149
    :cond_4
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    if-nez v3, :cond_5

    .line 151
    const-string v3, "SurfaceWidget.GLRenderer"

    const-string v4, "Surface is null! skip onGLDraw"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 153
    :cond_5
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v3}, Landroid/view/Surface;->isValid()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbFinished:Z

    if-eqz v3, :cond_7

    .line 154
    :cond_6
    const-string v3, "SurfaceWidget.GLRenderer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Surface valid:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    invoke-virtual {v5}, Landroid/view/Surface;->isValid()Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " finished: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbFinished:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "! skip onGLDraw"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 157
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->onGLDraw()V

    .line 159
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->checkGlError()I

    move-result v3

    if-eqz v3, :cond_8

    .line 160
    const-string v3, "SurfaceWidget.GLRenderer"

    const-string v4, "after handleOnDraw has OpenGL error!"

    invoke-static {v3, v4}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    :cond_8
    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    invoke-static {v3, v4}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-boolean v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbFinished:Z

    if-nez v3, :cond_9

    .line 165
    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v1

    .line 166
    .local v1, "error":I
    const-string v3, "EGL Error"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "eglSwapBuffers failed. code: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    .end local v1    # "error":I
    :cond_9
    if-eqz v2, :cond_0

    .line 171
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 173
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v3

    iget v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurfaceWidth:I

    iget v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurfaceHeight:I

    invoke-interface {v3, v4, v5}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->acceptedSurfaceSize(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected initGL()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x0

    .line 283
    const-string v2, "SurfaceWidget.GLRenderer"

    const-string v3, "initGL()"

    invoke-static {v2, v3}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mSurface:Landroid/view/Surface;

    if-nez v2, :cond_1

    .line 287
    const-string v2, "Failed to initialize OpenGL"

    const-string v3, "surface is null"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-static {v6}, Landroid/opengl/EGL14;->eglGetDisplay(I)Landroid/opengl/EGLDisplay;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    .line 292
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-ne v2, v3, :cond_2

    .line 293
    const-string v2, "EGL Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "eglGetDisplay failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 297
    :cond_2
    new-array v0, v4, [I

    .line 298
    .local v0, "majorVersion":[I
    new-array v1, v4, [I

    .line 299
    .local v1, "minorVersion":[I
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    invoke-static {v2, v0, v6, v1, v6}, Landroid/opengl/EGL14;->eglInitialize(Landroid/opengl/EGLDisplay;[II[II)Z

    move-result v2

    if-nez v2, :cond_3

    .line 300
    const-string v2, "EGL Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "eglInitialize failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 304
    :cond_3
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->chooseEglConfig()Landroid/opengl/EGLConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglConfig:Landroid/opengl/EGLConfig;

    .line 305
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglConfig:Landroid/opengl/EGLConfig;

    if-nez v2, :cond_4

    .line 306
    const-string v2, "EGL Error"

    const-string v3, "eglConfig not initialized"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :cond_4
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglConfig:Landroid/opengl/EGLConfig;

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->createContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;)Landroid/opengl/EGLContext;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    .line 311
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    if-ne v2, v3, :cond_5

    .line 313
    const-string v2, "EGL Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createContext failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->createEGLSurface()V

    .line 318
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->hasError()Z

    move-result v2

    if-nez v2, :cond_0

    .line 322
    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v5, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    invoke-static {v2, v3, v4, v5}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 323
    const-string v2, "EGL Error"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "eglMakeCurrent failed "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbEglInitialized:Z

    .line 328
    iput-boolean v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbUseNewSurface:Z

    .line 330
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->clearViewPort()V

    goto/16 :goto_0
.end method

.method protected onGLDraw()V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->onStart()V

    .line 86
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->initGL()V

    .line 87
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->hasError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->getError()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->requestDestroy(Ljava/lang/String;)V

    .line 90
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "SurfaceWidget.GLRenderer"

    const-string v1, "ON STOP CALLED!"

    invoke-static {v0, v1}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->finishGL()V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->releaseSurface()V

    .line 104
    return-void
.end method

.method reinitEGLSurface()V
    .locals 4

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mbEglInitialized:Z

    if-eqz v0, :cond_0

    .line 57
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->destroyEGLSurface()V

    .line 59
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->createEGLSurface()V

    .line 61
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglDisplay:Landroid/opengl/EGLDisplay;

    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglSurface:Landroid/opengl/EGLSurface;

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->mEglContext:Landroid/opengl/EGLContext;

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    const-string v0, "EGL Error"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "eglMakeCurrent failed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/opengl/EGL14;->eglGetError()I

    move-result v2

    invoke-static {v2}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetGLRenderer;->setError(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method
