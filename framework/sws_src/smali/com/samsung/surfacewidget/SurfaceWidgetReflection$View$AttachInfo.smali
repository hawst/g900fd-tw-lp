.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttachInfo"
.end annotation


# static fields
.field private static final CONSTRUCTOR:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field public static final M_HANDLER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_IGNOREDIRTYSTATE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_ROOT_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_VIEW_ROOT_IMPL:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 141
    :try_start_0
    const-string v2, "android.view.View$AttachInfo"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 142
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mHandler"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_HANDLER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 143
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mViewRootImpl"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_VIEW_ROOT_IMPL:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 144
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mRootView"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_ROOT_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 146
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_0

    .line 147
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mIgnoreDirtyState"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_IGNOREDIRTYSTATE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 151
    :goto_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    .line 152
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-string v4, "android.view.IWindowSession"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "android.view.IWindow"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-class v4, Landroid/view/Display;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "android.view.ViewRootImpl"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-class v4, Landroid/os/Handler;

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "android.view.View$AttachInfo$Callbacks"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    .line 167
    :goto_1
    sget-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 175
    return-void

    .line 149
    :cond_0
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->M_IGNOREDIRTYSTATE:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 168
    :catch_0
    move-exception v1

    .line 169
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find required class"

    invoke-static {v2, v3, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 160
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_1
    const/4 v2, 0x5

    :try_start_1
    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-string v4, "android.view.IWindowSession"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "android.view.IWindow"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "android.view.ViewRootImpl"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-class v4, Landroid/os/Handler;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "android.view.View$AttachInfo$Callbacks"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 171
    :catch_1
    move-exception v1

    .line 172
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find required constructor"

    invoke-static {v2, v3, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 173
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInst(Ljava/lang/Object;Landroid/view/Display;)Ljava/lang/Object;
    .locals 6
    .param p0, "viewRoot"    # Ljava/lang/Object;
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    const/4 v1, 0x0

    .line 179
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 180
    sget-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_WINDOW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x5

    aput-object p0, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 199
    :goto_0
    return-object v1

    .line 188
    :cond_0
    sget-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$View$AttachInfo;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_WINDOW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    invoke-virtual {v5, p0}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const/4 v5, 0x0

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object p0, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "failed to construct a new AttachInfo"

    invoke-static {v2, v3, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
