.class final Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;
.super Landroid/os/Handler;
.source "SurfaceWidgetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ServiceHandler"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SurfaceWidgetClient$ServiceHandler"


# instance fields
.field private final mServiceRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/surfacewidget/SurfaceWidgetClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/samsung/surfacewidget/SurfaceWidgetClient;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .prologue
    .line 1890
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1891
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    .line 1892
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1896
    iget-object v8, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ServiceHandler;->mServiceRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    .line 1897
    .local v5, "service":Lcom/samsung/surfacewidget/SurfaceWidgetClient;
    if-nez v5, :cond_1

    .line 1898
    const-string v6, "SurfaceWidgetClient$ServiceHandler"

    const-string v7, "service is no longer present"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1996
    :cond_0
    :goto_0
    return-void

    .line 1902
    :cond_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    .line 1904
    .local v1, "inst":I
    # invokes: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    invoke-static {v5, v1, v7}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$700(Lcom/samsung/surfacewidget/SurfaceWidgetClient;IZ)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v2

    .line 1905
    .local v2, "lRenderer":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-nez v2, :cond_2

    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0xc

    if-eq v8, v9, :cond_2

    iget v8, p1, Landroid/os/Message;->what:I

    const/16 v9, 0xd

    if-eq v8, v9, :cond_2

    .line 1906
    const-string v6, "SurfaceWidgetClient$ServiceHandler"

    const-string v7, "renderer is null in handle message?"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1910
    :cond_2
    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    .line 1992
    invoke-virtual {v5, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->customMessage(Landroid/os/Message;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1993
    const-string v6, "SurfaceWidgetClient$ServiceHandler"

    const-string v7, "unhandled message!!"

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1912
    :pswitch_0
    const-string v7, "SurfaceWidgetClient$ServiceHandler"

    const-string v8, "onContentRequest from ONSTART"

    invoke-static {v7, v8}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1913
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onContentRequest(I)V

    .line 1915
    iput-boolean v6, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbStartCalled:Z

    .line 1916
    iget-boolean v6, v2, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeNeedsRecalled:Z

    if-eqz v6, :cond_0

    .line 1917
    const-string v6, "SurfaceWidgetClient$ServiceHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "resume called before onStart...so calling onResume after onStart on instance = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1918
    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onResume(I)V

    goto :goto_0

    .line 1923
    :pswitch_1
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->handleRenderViewsMessage(I)V

    goto :goto_0

    .line 1926
    :pswitch_2
    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1927
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->layoutSurfaceWidgetsViews(I)V

    goto :goto_0

    .line 1932
    :pswitch_3
    :try_start_0
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/view/MotionEvent;

    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onTouchEvent(ILandroid/view/MotionEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1933
    :catch_0
    move-exception v0

    .line 1934
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 1939
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_4
    :try_start_1
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Landroid/view/KeyEvent;

    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onKeyEvent(ILandroid/view/KeyEvent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 1940
    :catch_1
    move-exception v0

    .line 1941
    .restart local v0    # "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    .line 1945
    .end local v0    # "e":Landroid/os/RemoteException;
    :pswitch_5
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onPause(I)V

    goto/16 :goto_0

    .line 1948
    :pswitch_6
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRenderer(I)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    move-result-object v3

    .line 1949
    .local v3, "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    if-eqz v3, :cond_0

    .line 1950
    iget-boolean v8, v3, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbStartCalled:Z

    if-eqz v8, :cond_3

    .line 1951
    iput-boolean v7, v3, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeNeedsRecalled:Z

    .line 1952
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onResume(I)V

    goto/16 :goto_0

    .line 1954
    :cond_3
    const-string v7, "SurfaceWidgetClient$ServiceHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onResume called before onStart was....so postponing sending of onResume...until onStart / finishinflate finish for instance = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/surfacewidget/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956
    iput-boolean v6, v3, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->mbResumeNeedsRecalled:Z

    goto/16 :goto_0

    .line 1961
    .end local v3    # "lRendererThread":Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    :pswitch_7
    iget v8, p1, Landroid/os/Message;->arg2:I

    if-ne v8, v6, :cond_4

    :goto_1
    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy(IZ)V

    goto/16 :goto_0

    :cond_4
    move v6, v7

    goto :goto_1

    .line 1964
    :pswitch_8
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onSurfaceDestroy(I)V

    goto/16 :goto_0

    .line 1967
    :pswitch_9
    iget v8, p1, Landroid/os/Message;->arg2:I

    if-ne v8, v6, :cond_5

    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setFocus(IZ)Z

    goto/16 :goto_0

    .line 1968
    :cond_5
    invoke-virtual {v5, v1, v7}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setFocus(IZ)Z

    goto/16 :goto_0

    .line 1971
    :pswitch_a
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->clearNewSurface()V

    goto/16 :goto_0

    .line 1977
    :pswitch_b
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->getRootView(I)Landroid/view/View;

    move-result-object v4

    .line 1978
    .local v4, "root":Landroid/view/View;
    if-eqz v4, :cond_0

    .line 1979
    iget v6, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v4, v6}, Landroid/view/View;->playSoundEffect(I)V

    goto/16 :goto_0

    .line 1983
    .end local v4    # "root":Landroid/view/View;
    :pswitch_c
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setCallback(ILcom/samsung/surfacewidget/IRemoteServiceCallback;)V

    goto/16 :goto_0

    .line 1986
    :pswitch_d
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v6

    invoke-virtual {v5, v1, v6}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onSurfaceSizeChanged(ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 1989
    :pswitch_e
    invoke-virtual {v5, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onContentDescriptionRequest(I)V

    goto/16 :goto_0

    .line 1910
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_b
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
