.class public final enum Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;
.super Ljava/lang/Enum;
.source "Animate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/animate/Animate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "InterpolationType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field public static final enum EAccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field public static final enum ECosine:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field public static final enum EDeccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field public static final enum ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field public static final enum ESmoothStep:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 9
    new-instance v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const-string v1, "ELinear"

    invoke-direct {v0, v1, v2}, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    new-instance v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const-string v1, "ECosine"

    invoke-direct {v0, v1, v3}, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ECosine:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    new-instance v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const-string v1, "ESmoothStep"

    invoke-direct {v0, v1, v4}, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ESmoothStep:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    new-instance v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const-string v1, "EAccelerate"

    invoke-direct {v0, v1, v5}, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EAccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    new-instance v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const-string v1, "EDeccelerate"

    invoke-direct {v0, v1, v6}, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EDeccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v1, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ECosine:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ESmoothStep:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EAccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EDeccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->$VALUES:[Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    return-object v0
.end method

.method public static values()[Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->$VALUES:[Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    invoke-virtual {v0}, [Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    return-object v0
.end method
