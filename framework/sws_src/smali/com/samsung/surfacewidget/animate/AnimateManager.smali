.class public Lcom/samsung/surfacewidget/animate/AnimateManager;
.super Ljava/lang/Object;
.source "AnimateManager.java"


# static fields
.field private static mManager:Lcom/samsung/surfacewidget/animate/AnimateManager;


# instance fields
.field private mAnimates:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/surfacewidget/animate/Animate;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    .line 18
    return-void
.end method

.method public static getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mManager:Lcom/samsung/surfacewidget/animate/AnimateManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/surfacewidget/animate/AnimateManager;

    invoke-direct {v0}, Lcom/samsung/surfacewidget/animate/AnimateManager;-><init>()V

    sput-object v0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mManager:Lcom/samsung/surfacewidget/animate/AnimateManager;

    .line 13
    :cond_0
    sget-object v0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mManager:Lcom/samsung/surfacewidget/animate/AnimateManager;

    return-object v0
.end method


# virtual methods
.method public declared-synchronized addAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 3
    .param p1, "newAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 22
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const-string v0, "AnimationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animate = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was already in the animation manager!?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :goto_0
    monitor-exit p0

    return-void

    .line 28
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized clearAllAnimates()V
    .locals 1

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    monitor-exit p0

    return-void

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 3
    .param p1, "removeAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const-string v0, "AnimationManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "animation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not removed.  Item not in list"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    :cond_0
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized update(F)V
    .locals 2
    .param p1, "deltaTimeSeconds"    # F

    .prologue
    .line 46
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/AnimateManager;->mAnimates:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/Animate;

    invoke-virtual {v1, p1}, Lcom/samsung/surfacewidget/animate/Animate;->updateTime(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 50
    :cond_0
    monitor-exit p0

    return-void

    .line 46
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
