.class public interface abstract Lcom/samsung/surfacewidget/animate/AnimateListener;
.super Ljava/lang/Object;
.source "AnimateListener.java"


# virtual methods
.method public abstract AnimationEnded(Lcom/samsung/surfacewidget/animate/Animate;)V
.end method

.method public abstract AnimationPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
.end method

.method public abstract AnimationStarted(Lcom/samsung/surfacewidget/animate/Animate;)V
.end method

.method public abstract AnimationUnPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
.end method

.method public abstract AnimationUpdated(Lcom/samsung/surfacewidget/animate/Animate;)V
.end method
