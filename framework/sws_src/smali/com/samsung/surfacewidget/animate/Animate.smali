.class public Lcom/samsung/surfacewidget/animate/Animate;
.super Ljava/lang/Object;
.source "Animate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;
    }
.end annotation


# instance fields
.field private mAnimateListeners:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lcom/samsung/surfacewidget/animate/AnimateListener;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentValue:F

.field private mDurationTime:F

.field private mEllapsedTime:F

.field private mEndTime:F

.field private mFromValue:F

.field private mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

.field private mLoopDelayTime:F

.field private mStartTime:F

.field private mToValue:F

.field private mbAnimateRunning:Z

.field private mbEnded:Z

.field private mbLoopedAnimation:Z

.field private mbPaused:Z

.field private mbReverse:Z

.field private mbReverseOnLoop:Z


# direct methods
.method protected constructor <init>()V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 12
    sget-object v5, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v7, v1

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/samsung/surfacewidget/animate/Animate;-><init>(FFFFLcom/samsung/surfacewidget/animate/Animate$InterpolationType;ZFZ)V

    .line 13
    return-void
.end method

.method public constructor <init>(FFFFLcom/samsung/surfacewidget/animate/Animate$InterpolationType;)V
    .locals 9
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "startTimeSeconds"    # F
    .param p4, "endTimeSeconds"    # F
    .param p5, "type"    # Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    .prologue
    const/4 v6, 0x0

    .line 16
    const/4 v7, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/samsung/surfacewidget/animate/Animate;-><init>(FFFFLcom/samsung/surfacewidget/animate/Animate$InterpolationType;ZFZ)V

    .line 17
    return-void
.end method

.method public constructor <init>(FFFFLcom/samsung/surfacewidget/animate/Animate$InterpolationType;ZFZ)V
    .locals 3
    .param p1, "from"    # F
    .param p2, "to"    # F
    .param p3, "startTimeSeconds"    # F
    .param p4, "endTimeSeconds"    # F
    .param p5, "type"    # Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;
    .param p6, "loop"    # Z
    .param p7, "loopDelaySeconds"    # F
    .param p8, "reverseOnLoop"    # Z

    .prologue
    const/4 v2, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    .line 312
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbLoopedAnimation:Z

    .line 313
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    .line 314
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbPaused:Z

    .line 315
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverse:Z

    .line 316
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverseOnLoop:Z

    .line 317
    iput-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbEnded:Z

    .line 319
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    .line 320
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    .line 321
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    .line 325
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    .line 326
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    .line 327
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    .line 328
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    .line 329
    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mLoopDelayTime:F

    .line 331
    sget-object v0, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    iput-object v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    .line 20
    invoke-virtual {p0, p1}, Lcom/samsung/surfacewidget/animate/Animate;->setFromValue(F)V

    .line 21
    invoke-virtual {p0, p2}, Lcom/samsung/surfacewidget/animate/Animate;->setToValue(F)V

    .line 22
    invoke-virtual {p0, p3}, Lcom/samsung/surfacewidget/animate/Animate;->setStartTime(F)V

    .line 23
    invoke-virtual {p0, p4}, Lcom/samsung/surfacewidget/animate/Animate;->setEndTime(F)V

    .line 24
    invoke-virtual {p0, p6, p7}, Lcom/samsung/surfacewidget/animate/Animate;->setLoopAnimation(ZF)V

    .line 25
    invoke-virtual {p0, p5}, Lcom/samsung/surfacewidget/animate/Animate;->setInterpolationType(Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;)V

    .line 26
    invoke-virtual {p0, p8}, Lcom/samsung/surfacewidget/animate/Animate;->setReverseOnLoop(Z)V

    .line 28
    invoke-static {}, Lcom/samsung/surfacewidget/animate/AnimateManager;->getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/surfacewidget/animate/AnimateManager;->addAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 29
    return-void
.end method

.method private declared-synchronized notifyAnimateEnded()V
    .locals 2

    .prologue
    .line 271
    monitor-enter p0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbEnded:Z

    .line 272
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 274
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/AnimateListener;

    invoke-interface {v1, p0}, Lcom/samsung/surfacewidget/animate/AnimateListener;->AnimationEnded(Lcom/samsung/surfacewidget/animate/Animate;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 276
    :cond_0
    monitor-exit p0

    return-void

    .line 271
    .end local v0    # "i":I
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized notifyAnimatePaused()V
    .locals 2

    .prologue
    .line 286
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 288
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/AnimateListener;

    invoke-interface {v1, p0}, Lcom/samsung/surfacewidget/animate/AnimateListener;->AnimationPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_0
    monitor-exit p0

    return-void

    .line 286
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized notifyAnimateStarted()V
    .locals 2

    .prologue
    .line 279
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 281
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/AnimateListener;

    invoke-interface {v1, p0}, Lcom/samsung/surfacewidget/animate/AnimateListener;->AnimationStarted(Lcom/samsung/surfacewidget/animate/Animate;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 283
    :cond_0
    monitor-exit p0

    return-void

    .line 279
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized notifyAnimateUnPaused()V
    .locals 2

    .prologue
    .line 293
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 295
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/AnimateListener;

    invoke-interface {v1, p0}, Lcom/samsung/surfacewidget/animate/AnimateListener;->AnimationUnPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 297
    :cond_0
    monitor-exit p0

    return-void

    .line 293
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized notifyAnimateUpdated()V
    .locals 2

    .prologue
    .line 300
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 302
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/surfacewidget/animate/AnimateListener;

    invoke-interface {v1, p0}, Lcom/samsung/surfacewidget/animate/AnimateListener;->AnimationUpdated(Lcom/samsung/surfacewidget/animate/Animate;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_0
    monitor-exit p0

    return-void

    .line 300
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method


# virtual methods
.method public accelerationInterpolation(FFF)F
    .locals 1
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "time"    # F

    .prologue
    .line 225
    mul-float v0, p3, p3

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/surfacewidget/animate/Animate;->linearInterpolation(FFF)F

    move-result v0

    return v0
.end method

.method public declared-synchronized addAnimateListener(Lcom/samsung/surfacewidget/animate/AnimateListener;)Z
    .locals 2
    .param p1, "listener"    # Lcom/samsung/surfacewidget/animate/AnimateListener;

    .prologue
    .line 253
    monitor-enter p0

    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 255
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->get(I)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 258
    const/4 v1, 0x0

    .line 263
    :goto_1
    monitor-exit p0

    return v1

    .line 253
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 262
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 263
    const/4 v1, 0x1

    goto :goto_1

    .line 253
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method calculateDuration()V
    .locals 2

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 107
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 108
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    iget v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    .line 109
    :cond_0
    return-void
.end method

.method public cosineInterpolation(FFF)F
    .locals 6
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "time"    # F

    .prologue
    .line 214
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    float-to-double v4, p3

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    neg-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    double-to-float v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float v0, v1, v2

    .line 215
    .local v0, "cosTime":F
    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/surfacewidget/animate/Animate;->linearInterpolation(FFF)F

    move-result v1

    return v1
.end method

.method public decelerationInterpolation(FFF)F
    .locals 3
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "time"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 230
    sub-float v0, v2, p3

    sub-float v1, v2, p3

    mul-float/2addr v0, v1

    sub-float v0, v2, v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/surfacewidget/animate/Animate;->linearInterpolation(FFF)F

    move-result v0

    return v0
.end method

.method public destroyAnimation()V
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lcom/samsung/surfacewidget/animate/AnimateManager;->getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/surfacewidget/animate/AnimateManager;->removeAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 33
    return-void
.end method

.method public endAnimation()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateEnded()V

    .line 59
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    iput v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    .line 61
    return-void
.end method

.method public getCurrentValue()F
    .locals 1

    .prologue
    .line 125
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    return v0
.end method

.method getDuration()F
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    return v0
.end method

.method getEndTime()F
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    return v0
.end method

.method getStartTime()F
    .locals 1

    .prologue
    .line 77
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    return v0
.end method

.method public isAnimationRunning()Z
    .locals 1

    .prologue
    .line 129
    iget-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    return v0
.end method

.method public linearInterpolation(FFF)F
    .locals 1
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "time"    # F

    .prologue
    .line 209
    sub-float v0, p2, p1

    mul-float/2addr v0, p3

    add-float/2addr v0, p1

    return v0
.end method

.method public pauseAnimation()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimatePaused()V

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbPaused:Z

    .line 43
    return-void
.end method

.method public declared-synchronized removeAnimateListener(Lcom/samsung/surfacewidget/animate/AnimateListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/surfacewidget/animate/AnimateListener;

    .prologue
    .line 267
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mAnimateListeners:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbEnded:Z

    .line 247
    return-void
.end method

.method public reverse()V
    .locals 2

    .prologue
    .line 99
    iget-boolean v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverse:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverse:Z

    .line 100
    iget v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    .line 101
    .local v0, "tempTo":F
    iget v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iput v1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    .line 102
    iput v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    .line 103
    return-void

    .line 99
    .end local v0    # "tempTo":F
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method setDuration(F)V
    .locals 0
    .param p1, "durationInSeconds"    # F

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    .line 65
    return-void
.end method

.method setEndTime(F)V
    .locals 0
    .param p1, "endTimeInSeconds"    # F

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    .line 86
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/animate/Animate;->calculateDuration()V

    .line 87
    return-void
.end method

.method setFromValue(F)V
    .locals 0
    .param p1, "from"    # F

    .prologue
    .line 112
    iput p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    .line 113
    return-void
.end method

.method setInterpolationType(Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;)V
    .locals 0
    .param p1, "aType"    # Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    .line 91
    return-void
.end method

.method setLoopAnimation(ZF)V
    .locals 0
    .param p1, "loop"    # Z
    .param p2, "delayBetweenLoopsInSeconds"    # F

    .prologue
    .line 120
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbLoopedAnimation:Z

    .line 121
    iput p2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mLoopDelayTime:F

    .line 122
    return-void
.end method

.method setReverseOnLoop(Z)V
    .locals 0
    .param p1, "reverseOnLoop"    # Z

    .prologue
    .line 95
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverseOnLoop:Z

    .line 96
    return-void
.end method

.method setStartTime(F)V
    .locals 0
    .param p1, "startTimeInSeconds"    # F

    .prologue
    .line 72
    iput p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    .line 73
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/animate/Animate;->calculateDuration()V

    .line 74
    return-void
.end method

.method setToValue(F)V
    .locals 0
    .param p1, "to"    # F

    .prologue
    .line 116
    iput p1, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    .line 117
    return-void
.end method

.method public smoothStepInterpolation(FFF)F
    .locals 3
    .param p1, "x0"    # F
    .param p2, "x1"    # F
    .param p3, "time"    # F

    .prologue
    .line 220
    mul-float v0, p3, p3

    const/high16 v1, 0x40400000    # 3.0f

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, p3

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/surfacewidget/animate/Animate;->linearInterpolation(FFF)F

    move-result v0

    return v0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateStarted()V

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    .line 38
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateEnded()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    .line 54
    return-void
.end method

.method public unPauseAnimation()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateUnPaused()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbPaused:Z

    .line 48
    return-void
.end method

.method public updateTime(F)V
    .locals 4
    .param p1, "deltaTimeSeconds"    # F

    .prologue
    .line 134
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbLoopedAnimation:Z

    if-eqz v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbPaused:Z

    if-eqz v2, :cond_2

    .line 205
    :cond_1
    :goto_0
    return-void

    .line 139
    :cond_2
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    add-float/2addr v2, p1

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    .line 141
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbEnded:Z

    if-nez v2, :cond_3

    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_3

    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 143
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    sub-float v1, v2, v3

    .line 144
    .local v1, "timeEllapsed":F
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    div-float v0, v1, v2

    .line 146
    .local v0, "percentThroughAnimation":F
    iget-object v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v3, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ELinear:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    if-ne v2, v3, :cond_6

    .line 149
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    invoke-virtual {p0, v2, v3, v0}, Lcom/samsung/surfacewidget/animate/Animate;->linearInterpolation(FFF)F

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    .line 176
    :goto_1
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateUpdated()V

    .line 179
    .end local v0    # "percentThroughAnimation":F
    .end local v1    # "timeEllapsed":F
    :cond_3
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbEnded:Z

    if-nez v2, :cond_1

    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_1

    .line 182
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbAnimateRunning:Z

    if-eqz v2, :cond_4

    .line 183
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    .line 184
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;->notifyAnimateUpdated()V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/animate/Animate;->endAnimation()V

    .line 189
    :cond_4
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbLoopedAnimation:Z

    if-eqz v2, :cond_1

    .line 191
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mLoopDelayTime:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 195
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEllapsedTime:F

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    .line 196
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mStartTime:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mDurationTime:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mEndTime:F

    .line 198
    iget-boolean v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mbReverseOnLoop:Z

    if-eqz v2, :cond_5

    .line 199
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/animate/Animate;->reverse()V

    .line 201
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/animate/Animate;->start()V

    goto :goto_0

    .line 151
    .restart local v0    # "percentThroughAnimation":F
    .restart local v1    # "timeEllapsed":F
    :cond_6
    iget-object v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v3, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EAccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    if-ne v2, v3, :cond_7

    .line 153
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    invoke-virtual {p0, v2, v3, v0}, Lcom/samsung/surfacewidget/animate/Animate;->accelerationInterpolation(FFF)F

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    goto :goto_1

    .line 160
    :cond_7
    iget-object v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v3, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->EDeccelerate:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    if-ne v2, v3, :cond_8

    .line 162
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    invoke-virtual {p0, v2, v3, v0}, Lcom/samsung/surfacewidget/animate/Animate;->decelerationInterpolation(FFF)F

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    goto :goto_1

    .line 164
    :cond_8
    iget-object v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v3, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ECosine:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    if-ne v2, v3, :cond_9

    .line 166
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    invoke-virtual {p0, v2, v3, v0}, Lcom/samsung/surfacewidget/animate/Animate;->cosineInterpolation(FFF)F

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    goto :goto_1

    .line 168
    :cond_9
    iget-object v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mInterpolationType:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    sget-object v3, Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;->ESmoothStep:Lcom/samsung/surfacewidget/animate/Animate$InterpolationType;

    if-ne v2, v3, :cond_a

    .line 170
    iget v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mFromValue:F

    iget v3, p0, Lcom/samsung/surfacewidget/animate/Animate;->mToValue:F

    invoke-virtual {p0, v2, v3, v0}, Lcom/samsung/surfacewidget/animate/Animate;->smoothStepInterpolation(FFF)F

    move-result v2

    iput v2, p0, Lcom/samsung/surfacewidget/animate/Animate;->mCurrentValue:F

    goto/16 :goto_1

    .line 174
    :cond_a
    const-string v2, "Animate"

    const-string v3, "unknown interpolation type"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method
