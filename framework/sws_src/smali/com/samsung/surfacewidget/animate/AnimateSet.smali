.class public Lcom/samsung/surfacewidget/animate/AnimateSet;
.super Lcom/samsung/surfacewidget/animate/Animate;
.source "AnimateSet.java"

# interfaces
.implements Lcom/samsung/surfacewidget/animate/AnimateListener;


# instance fields
.field mAnimates:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<[",
            "Lcom/samsung/surfacewidget/animate/Animate;",
            ">;"
        }
    .end annotation
.end field

.field mEndTimeAnimate:Lcom/samsung/surfacewidget/animate/Animate;

.field mPlayingIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/surfacewidget/animate/Animate;-><init>()V

    .line 7
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    .line 9
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mEndTimeAnimate:Lcom/samsung/surfacewidget/animate/Animate;

    .line 10
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    .line 13
    return-void
.end method

.method private startAnimates(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 42
    iget-object v3, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v3, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/samsung/surfacewidget/animate/Animate;

    .line 43
    .local v1, "animates":[Lcom/samsung/surfacewidget/animate/Animate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v1

    if-ge v2, v3, :cond_0

    .line 44
    aget-object v0, v1, v2

    .line 45
    .local v0, "a":Lcom/samsung/surfacewidget/animate/Animate;
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/animate/Animate;->reset()V

    .line 46
    invoke-static {}, Lcom/samsung/surfacewidget/animate/AnimateManager;->getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/surfacewidget/animate/AnimateManager;->removeAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 47
    invoke-static {}, Lcom/samsung/surfacewidget/animate/AnimateManager;->getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/samsung/surfacewidget/animate/AnimateManager;->addAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 49
    invoke-virtual {v0, p0}, Lcom/samsung/surfacewidget/animate/Animate;->addAnimateListener(Lcom/samsung/surfacewidget/animate/AnimateListener;)Z

    .line 51
    invoke-virtual {v0}, Lcom/samsung/surfacewidget/animate/Animate;->start()V

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 53
    .end local v0    # "a":Lcom/samsung/surfacewidget/animate/Animate;
    :cond_0
    return-void
.end method


# virtual methods
.method public AnimationEnded(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 2
    .param p1, "aAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 67
    invoke-static {}, Lcom/samsung/surfacewidget/animate/AnimateManager;->getInstance()Lcom/samsung/surfacewidget/animate/AnimateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/surfacewidget/animate/AnimateManager;->removeAnimate(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 68
    invoke-virtual {p1, p0}, Lcom/samsung/surfacewidget/animate/Animate;->removeAnimateListener(Lcom/samsung/surfacewidget/animate/AnimateListener;)Z

    .line 69
    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    .line 70
    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 71
    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    invoke-direct {p0, v0}, Lcom/samsung/surfacewidget/animate/AnimateSet;->startAnimates(I)V

    .line 73
    :cond_0
    return-void
.end method

.method public AnimationPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 0
    .param p1, "aAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 83
    return-void
.end method

.method public AnimationStarted(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 0
    .param p1, "aAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 78
    return-void
.end method

.method public AnimationUnPaused(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 0
    .param p1, "aAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 88
    return-void
.end method

.method public AnimationUpdated(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 0
    .param p1, "aAnimate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 93
    return-void
.end method

.method getStartTime()F
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public isAnimationRunning()Z
    .locals 2

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public playSequentially(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 2
    .param p1, "animate"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 16
    const/4 v1, 0x1

    new-array v0, v1, [Lcom/samsung/surfacewidget/animate/Animate;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 17
    .local v0, "as":[Lcom/samsung/surfacewidget/animate/Animate;
    iget-object v1, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 18
    return-void
.end method

.method public playSequentially([Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 2
    .param p1, "animates"    # [Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 21
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 22
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/samsung/surfacewidget/animate/AnimateSet;->playSequentially(Lcom/samsung/surfacewidget/animate/Animate;)V

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_0
    return-void
.end method

.method public playTogether([Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 1
    .param p1, "animates"    # [Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 28
    return-void
.end method

.method public remove(Lcom/samsung/surfacewidget/animate/Animate;)V
    .locals 0
    .param p1, "a"    # Lcom/samsung/surfacewidget/animate/Animate;

    .prologue
    .line 31
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mAnimates:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    .line 38
    iget v0, p0, Lcom/samsung/surfacewidget/animate/AnimateSet;->mPlayingIndex:I

    invoke-direct {p0, v0}, Lcom/samsung/surfacewidget/animate/AnimateSet;->startAnimates(I)V

    goto :goto_0
.end method
