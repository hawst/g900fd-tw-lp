.class public final Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;
.super Ljava/lang/Object;
.source "SurfaceWidgetReflection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetReflection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ViewRootImpl"
.end annotation


# static fields
.field private static final CONSTRUCTOR:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<*>;"
        }
    .end annotation
.end field

.field public static final DESTROY_HARDWARE_RENDERER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

.field public static final DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

.field public static final GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

.field public static final M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final M_WINDOW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

.field public static final SET_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x11

    .line 68
    :try_start_0
    const-string v2, "android.view.ViewRootImpl"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 69
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mView"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 70
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v2, v3, :cond_0

    .line 71
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mCompatibilityInfo"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 75
    :goto_0
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    const-string v3, "mWindow"

    invoke-direct {v2, v0, v3}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_WINDOW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;

    .line 77
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v2, v7, :cond_1

    .line 78
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-string v3, "getWindowSession"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/os/Looper;

    aput-object v6, v4, v5

    invoke-direct {v2, v0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    .line 83
    :goto_1
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-string v3, "dispatchDetachedFromWindow"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-direct {v2, v0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->DISPATCH_DETACHED_FROM_WIN:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    .line 85
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-string v3, "destroyHardwareRenderer"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Class;

    invoke-direct {v2, v0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->DESTROY_HARDWARE_RENDERER:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    .line 87
    new-instance v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    const-string v3, "setView"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/view/View;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-class v6, Landroid/view/WindowManager$LayoutParams;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-class v6, Landroid/view/View;

    aput-object v6, v4, v5

    invoke-direct {v2, v0, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;-><init>(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)V

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->SET_VIEW:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;

    .line 89
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v2, v7, :cond_2

    .line 90
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Landroid/view/Display;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    .line 94
    :goto_2
    sget-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 102
    return-void

    .line 73
    :cond_0
    const/4 v2, 0x0

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->M_COMPAT_INFO:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$FieldHelper;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 95
    :catch_0
    move-exception v1

    .line 96
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find required class"

    invoke-static {v2, v3, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 97
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 80
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :cond_1
    const/4 v2, 0x0

    :try_start_1
    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->GET_WINDOW_SESSION:Lcom/samsung/surfacewidget/SurfaceWidgetReflection$MethodHelper;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 98
    :catch_1
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "SurfaceWidgetReflection"

    const-string v3, "unable to find required constructor"

    invoke-static {v2, v3, v1}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 92
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :cond_2
    const/4 v2, 0x1

    :try_start_2
    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    sput-object v2, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newInst(Landroid/content/Context;Landroid/view/Display;)Ljava/lang/Object;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "display"    # Landroid/view/Display;

    .prologue
    .line 106
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 107
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 115
    :goto_0
    return-object v1

    .line 109
    :cond_0
    sget-object v1, Lcom/samsung/surfacewidget/SurfaceWidgetReflection$ViewRootImpl;->CONSTRUCTOR:Ljava/lang/reflect/Constructor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "SurfaceWidgetReflection"

    const-string v2, "failed to construct a new ViewRootImpl"

    invoke-static {v1, v2, v0}, Lcom/samsung/surfacewidget/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    const/4 v1, 0x0

    goto :goto_0
.end method
