.class final Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;
.super Landroid/widget/FrameLayout;
.source "SurfaceWidgetClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ParentViewGroup"
.end annotation


# instance fields
.field mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

.field mContext:Landroid/content/Context;

.field private final mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

.field private mRotationLock:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "renderer"    # Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .param p3, "rotationLock"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1772
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1767
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRotationLock:Z

    .line 1769
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    .line 1770
    iput-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mContext:Landroid/content/Context;

    .line 1773
    iput-object p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mContext:Landroid/content/Context;

    .line 1774
    iput-object p2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    .line 1775
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    .line 1776
    iput-boolean p3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRotationLock:Z

    .line 1777
    return-void
.end method

.method private childContains(Landroid/view/View;FF)Z
    .locals 5
    .param p1, "child"    # Landroid/view/View;
    .param p2, "touchX"    # F
    .param p3, "touchY"    # F

    .prologue
    .line 1819
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    .line 1820
    .local v1, "left":F
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v2

    .line 1821
    .local v2, "top":F
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v3, v4

    .line 1822
    .local v3, "width":F
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v0, v4

    .line 1823
    .local v0, "height":F
    cmpl-float v4, p2, v1

    if-lez v4, :cond_0

    add-float v4, v1, v3

    cmpg-float v4, p2, v4

    if-gez v4, :cond_0

    cmpl-float v4, p3, v2

    if-lez v4, :cond_0

    add-float v4, v2, v0

    cmpg-float v4, p3, v4

    if-gez v4, :cond_0

    .line 1825
    const/4 v4, 0x1

    .line 1827
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private wasChildTouched(Landroid/view/ViewGroup;FF)Landroid/view/View;
    .locals 9
    .param p1, "rootGroup"    # Landroid/view/ViewGroup;
    .param p2, "touchX"    # F
    .param p3, "touchY"    # F

    .prologue
    .line 1831
    const/4 v4, 0x0

    .line 1832
    .local v4, "lowestChildHit":Landroid/view/View;
    const/4 v3, 0x0

    .line 1833
    .local v3, "lastChildCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-ge v2, v6, :cond_2

    .line 1834
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1836
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->childContains(Landroid/view/View;FF)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1838
    move-object v1, v0

    .line 1839
    .local v1, "childTouched":Landroid/view/View;
    instance-of v6, v1, Landroid/view/ViewGroup;

    if-eqz v6, :cond_0

    move-object v6, v1

    .line 1840
    check-cast v6, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v7

    int-to-float v7, v7

    sub-float v7, p2, v7

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v8

    int-to-float v8, v8

    sub-float v8, p3, v8

    invoke-direct {p0, v6, v7, v8}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->wasChildTouched(Landroid/view/ViewGroup;FF)Landroid/view/View;

    move-result-object v5

    .line 1841
    .local v5, "subChildTouched":Landroid/view/View;
    if-eqz v5, :cond_0

    .line 1843
    move-object v1, v5

    .line 1847
    .end local v5    # "subChildTouched":Landroid/view/View;
    :cond_0
    if-eqz v1, :cond_1

    if-gt v3, v2, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1849
    move-object v4, v1

    .line 1850
    move v3, v2

    .line 1833
    .end local v1    # "childTouched":Landroid/view/View;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1855
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    return-object v4
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 1781
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    .line 1782
    .local v4, "result":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    if-ne v6, v8, :cond_3

    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mAccessibilityMgr:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v6}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1783
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    invoke-direct {p0, p0, v6, v7}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->wasChildTouched(Landroid/view/ViewGroup;FF)Landroid/view/View;

    move-result-object v2

    .line 1784
    .local v2, "childTouched":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 1785
    move-object v1, v2

    .line 1786
    .local v1, "childBeingRead":Landroid/view/View;
    :goto_0
    if-eqz v1, :cond_3

    .line 1788
    invoke-virtual {v1, v8}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 1790
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/samsung/surfacewidget/SurfaceWidgetClient;

    # invokes: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setChildTalkBackFocused(Landroid/view/View;)V
    invoke-static {v6, v1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$600(Lcom/samsung/surfacewidget/SurfaceWidgetClient;Landroid/view/View;)V

    .line 1791
    const/16 v6, 0x40

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 1793
    invoke-virtual {v1}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1794
    .local v0, "ch":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1795
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .end local v1    # "childBeingRead":Landroid/view/View;
    check-cast v1, Landroid/view/View;

    .line 1796
    .restart local v1    # "childBeingRead":Landroid/view/View;
    goto :goto_0

    .line 1798
    :cond_1
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1800
    .local v5, "talkback":Ljava/lang/String;
    if-nez v5, :cond_2

    .line 1801
    invoke-virtual {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1802
    :cond_2
    if-eqz v5, :cond_4

    .line 1804
    :try_start_0
    iget-object v6, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v6}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getCallback()Lcom/samsung/surfacewidget/IRemoteServiceCallback;

    move-result-object v6

    invoke-interface {v6, v5}, Lcom/samsung/surfacewidget/IRemoteServiceCallback;->requestTalkbackSay(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1815
    .end local v0    # "ch":Ljava/lang/CharSequence;
    .end local v1    # "childBeingRead":Landroid/view/View;
    .end local v2    # "childTouched":Landroid/view/View;
    .end local v5    # "talkback":Ljava/lang/String;
    :cond_3
    return v4

    .line 1806
    .restart local v0    # "ch":Ljava/lang/CharSequence;
    .restart local v1    # "childBeingRead":Landroid/view/View;
    .restart local v2    # "childTouched":Landroid/view/View;
    .restart local v5    # "talkback":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 1807
    .local v3, "e":Ljava/lang/Exception;
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 1810
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .end local v1    # "childBeingRead":Landroid/view/View;
    check-cast v1, Landroid/view/View;

    .restart local v1    # "childBeingRead":Landroid/view/View;
    goto :goto_0
.end method

.method public invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;
    .locals 2
    .param p1, "location"    # [I
    .param p2, "dirty"    # Landroid/graphics/Rect;

    .prologue
    .line 1873
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->invalidateChildInParent([ILandroid/graphics/Rect;)Landroid/view/ViewParent;

    move-result-object v0

    .line 1875
    .local v0, "viewP":Landroid/view/ViewParent;
    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z
    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$200()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRotationLock:Z

    if-nez v1, :cond_1

    .line 1876
    :cond_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v1}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->requestLayout()V

    .line 1877
    :cond_1
    return-object v0
.end method

.method public requestLayout()V
    .locals 5

    .prologue
    .line 1860
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    if-eqz v0, :cond_1

    # getter for: Lcom/samsung/surfacewidget/SurfaceWidgetClient;->sRotationFeatureEnabled:Z
    invoke-static {}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->access$200()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRotationLock:Z

    if-nez v0, :cond_1

    .line 1861
    :cond_0
    iget-object v1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    monitor-enter v1

    .line 1862
    :try_start_0
    iget-object v0, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v0}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->measure(II)V

    .line 1864
    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v3}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRenderer:Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;

    invoke-virtual {v4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;->getHeight()I

    move-result v4

    invoke-virtual {p0, v0, v2, v3, v4}, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->layout(IIII)V

    .line 1866
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1868
    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->requestLayout()V

    .line 1869
    return-void

    .line 1866
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setRotationLock(Z)V
    .locals 0
    .param p1, "rotationLock"    # Z

    .prologue
    .line 1881
    iput-boolean p1, p0, Lcom/samsung/surfacewidget/SurfaceWidgetClient$ParentViewGroup;->mRotationLock:Z

    .line 1882
    return-void
.end method
