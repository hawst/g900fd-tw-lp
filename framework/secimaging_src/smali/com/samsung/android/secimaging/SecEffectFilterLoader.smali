.class public Lcom/samsung/android/secimaging/SecEffectFilterLoader;
.super Ljava/lang/Object;
.source "SecEffectFilterLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;
    }
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider/filters"

.field private static final BASE_URI:Landroid/net/Uri;

.field private static final DELETED:Ljava/lang/String; = "deleted"

.field private static final EFFECT_NAME:Ljava/lang/String; = "name"

.field private static final FILE_NAME:Ljava/lang/String; = "filename"

.field private static final FILTER_AUTHORITY:Ljava/lang/String; = "com.samsung.android.provider.filterprovider/filters/include_deleted"

.field private static final FILTER_PROJECTION:[Ljava/lang/String;

.field private static final FILTER_TYPE:Ljava/lang/String; = "filter_type"

.field private static final FILTER_URI:Landroid/net/Uri;

.field private static final HANDLER:Ljava/lang/String; = "handler"

.field private static final HEIGHT:Ljava/lang/String; = "height"

.field private static final INDEX_DELETED:I = 0xd

.field private static final INDEX_EFFECT_NAME:I = 0x1

.field private static final INDEX_FILE_NAME:I = 0x2

.field private static final INDEX_FILTER_TYPE:I = 0x5

.field private static final INDEX_HANDLER:I = 0xb

.field private static final INDEX_HEIGHT:I = 0xa

.field private static final INDEX_ID:I = 0x0

.field private static final INDEX_MVENDOR:I = 0x4

.field private static final INDEX_PACKAGE_NAME:I = 0xc

.field private static final INDEX_POSX:I = 0x7

.field private static final INDEX_POSY:I = 0x8

.field private static final INDEX_TITLE:I = 0x6

.field private static final INDEX_VERSION:I = 0x3

.field private static final INDEX_WIDTH:I = 0x9

.field private static final PACKAGE_NAME:Ljava/lang/String; = "package_name"

.field private static final POSX:Ljava/lang/String; = "posx"

.field private static final POSY:Ljava/lang/String; = "posy"

.field private static final TAG:Ljava/lang/String;

.field private static final TITLE:Ljava/lang/String; = "title"

.field private static final VENDOR:Ljava/lang/String; = "vendor"

.field private static final VERSION:Ljava/lang/String; = "version"

.field private static final WIDTH:Ljava/lang/String; = "width"

.field private static final _ID:Ljava/lang/String; = "_ID"

.field private static mEffectFilters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 16
    const-class v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    .line 19
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->BASE_URI:Landroid/net/Uri;

    .line 22
    const-string v0, "content://com.samsung.android.provider.filterprovider/filters/include_deleted"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->FILTER_URI:Landroid/net/Uri;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    .line 58
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "filename"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "version"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "vendor"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "filter_type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "posx"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "posy"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "width"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "height"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "handler"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "package_name"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "deleted"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "mContext"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    const-string v1, "create SecEffectFilterLoader"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    iput-object p1, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mContext:Landroid/content/Context;

    .line 66
    return-void
.end method

.method private getEffectFilterName(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 232
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    # invokes: Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->getEffectFiterName()Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->access$000(Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;)Ljava/lang/String;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getEffectFilterPackageName(I)Ljava/lang/String;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 239
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 240
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    invoke-virtual {v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->getEffectFilterPackage()Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getEffectFilterTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 222
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 223
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    invoke-virtual {v1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->getEffectFilterTitle()Ljava/lang/String;

    move-result-object v0

    .line 224
    .local v0, "title":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 228
    .end local v0    # "title":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 226
    .restart local v0    # "title":Ljava/lang/String;
    :cond_0
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    # invokes: Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->getEffectFiterName()Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->access$000(Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 228
    .end local v0    # "title":Ljava/lang/String;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 152
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    const-string v1, "clear SecEffectFilters"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 154
    return-void
.end method

.method public getEffectFilterCount()I
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 218
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getEffectFilterForSet(I)Ljava/lang/String;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 263
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 264
    .local v2, "sb":Ljava/lang/StringBuffer;
    invoke-direct {p0, p1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterName(I)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "fullName":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterPackageName(I)Ljava/lang/String;

    move-result-object v1

    .line 266
    .local v1, "packageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 267
    invoke-virtual {v2, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 268
    :cond_0
    sget-object v3, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getEffectFilterForSet: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getEffectFilterId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 249
    const/4 v1, -0x1

    .line 250
    .local v1, "mFilterId":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 251
    invoke-direct {p0, v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterPackageName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterTitle(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 252
    move v1, v0

    .line 256
    :cond_0
    return v1

    .line 250
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getEffectFilterList(I)Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 208
    sget-object v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    return-object v0
.end method

.method public loadSecEffectFilters()Z
    .locals 22

    .prologue
    .line 72
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    const-string v2, "loadSecEffectFilters"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    const/4 v7, 0x0

    .line 75
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 76
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->FILTER_URI:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->FILTER_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 77
    const/4 v11, 0x0

    .line 78
    .local v11, "filterCount":I
    const/16 v18, 0x0

    .line 80
    .local v18, "totalFilterCount":I
    if-nez v7, :cond_1

    .line 81
    const/4 v1, 0x0

    .line 141
    if-eqz v7, :cond_0

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 145
    .end local v11    # "filterCount":I
    .end local v18    # "totalFilterCount":I
    :cond_0
    :goto_0
    return v1

    .line 83
    .restart local v11    # "filterCount":I
    .restart local v18    # "totalFilterCount":I
    :cond_1
    :goto_1
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 84
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 86
    .local v15, "id":I
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 87
    .local v10, "effectName":Ljava/lang/String;
    if-eqz v10, :cond_1

    const-string v1, "Unnamed filter"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 90
    const/4 v1, 0x2

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 91
    .local v3, "filterName":Ljava/lang/String;
    if-eqz v3, :cond_1

    const-string v1, "Unnamed filter"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    const/4 v1, 0x3

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 96
    .local v4, "version":I
    const/4 v1, 0x4

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 97
    .local v19, "vendor":Ljava/lang/String;
    if-eqz v19, :cond_1

    const-string v1, "Unknown vendor"

    move-object/from16 v0, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 100
    const/4 v1, 0x5

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 101
    .local v12, "filter_type":Ljava/lang/String;
    if-eqz v12, :cond_1

    const-string v1, "Unknown FilterType"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 104
    const/4 v1, 0x6

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 105
    .local v5, "title":Ljava/lang/String;
    if-eqz v5, :cond_1

    const-string v1, "Unknown title"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 108
    const/4 v1, 0x7

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 109
    .local v16, "posx":I
    const/16 v1, 0x8

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v17

    .line 110
    .local v17, "posy":I
    const/16 v1, 0x9

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    .line 111
    .local v20, "width":I
    const/16 v1, 0xa

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 112
    .local v14, "height":I
    const/16 v1, 0xb

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 113
    .local v13, "handler":Ljava/lang/String;
    if-eqz v13, :cond_1

    invoke-virtual {v13}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 116
    const/16 v1, 0xc

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 117
    .local v6, "packageName":Ljava/lang/String;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    add-int/lit8 v18, v18, 0x1

    .line 121
    const/16 v1, 0xd

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 123
    .local v8, "deleted":Ljava/lang/String;
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "id["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], effectName["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], filterName["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], version["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], vendor["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], filter_type["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], title["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], posx["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], posy["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], width["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], height["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], handler["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], packageName["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "], deleted["

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v21, "]"

    move-object/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    sget-object v21, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mEffectFilters:Ljava/util/ArrayList;

    new-instance v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;-><init>(Lcom/samsung/android/secimaging/SecEffectFilterLoader;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 128
    add-int/lit8 v11, v11, 0x1

    .line 129
    goto/16 :goto_1

    .line 130
    .end local v3    # "filterName":Ljava/lang/String;
    .end local v4    # "version":I
    .end local v5    # "title":Ljava/lang/String;
    .end local v6    # "packageName":Ljava/lang/String;
    .end local v8    # "deleted":Ljava/lang/String;
    .end local v10    # "effectName":Ljava/lang/String;
    .end local v12    # "filter_type":Ljava/lang/String;
    .end local v13    # "handler":Ljava/lang/String;
    .end local v14    # "height":I
    .end local v15    # "id":I
    .end local v16    # "posx":I
    .end local v17    # "posy":I
    .end local v19    # "vendor":Ljava/lang/String;
    .end local v20    # "width":I
    :cond_2
    if-nez v11, :cond_5

    .line 131
    if-nez v18, :cond_3

    .line 132
    sget-object v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->TAG:Ljava/lang/String;

    const-string v2, "Preload filters has not been initialized properly... reset!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->mContext:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v21, "com.samsung.filterinstaller.RESET_DB"

    move-object/from16 v0, v21

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :cond_3
    if-eqz v7, :cond_4

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 145
    .end local v11    # "filterCount":I
    .end local v18    # "totalFilterCount":I
    :cond_4
    :goto_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 136
    .restart local v11    # "filterCount":I
    .restart local v18    # "totalFilterCount":I
    :cond_5
    const/4 v1, 0x1

    .line 141
    if-eqz v7, :cond_0

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 138
    .end local v11    # "filterCount":I
    .end local v18    # "totalFilterCount":I
    :catch_0
    move-exception v9

    .line 139
    .local v9, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 141
    if-eqz v7, :cond_4

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 141
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_6

    .line 142
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v1
.end method
