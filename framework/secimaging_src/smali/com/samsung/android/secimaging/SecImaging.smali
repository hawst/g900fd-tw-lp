.class public Lcom/samsung/android/secimaging/SecImaging;
.super Ljava/lang/Object;
.source "SecImaging.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/secimaging/SecImaging$EventHandler;,
        Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    }
.end annotation


# static fields
.field private static final SEC_IMAGING_MANAGER_MSG_PROGRESS:I = 0x1

.field private static final SEC_IMAGING_MANAGER_MSG_RESULT_COMPLETED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SecImaging"

.field private static sLibraryLoaded:Z


# instance fields
.field private mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

.field private mNativeObject:J

.field private mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

.field private mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 54
    sput-boolean v4, Lcom/samsung/android/secimaging/SecImaging;->sLibraryLoaded:Z

    .line 62
    :try_start_0
    const-string v1, "secimaging"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 63
    const/4 v1, 0x1

    sput-boolean v1, Lcom/samsung/android/secimaging/SecImaging;->sLibraryLoaded:Z
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    .local v0, "ule":Ljava/lang/UnsatisfiedLinkError;
    :goto_0
    return-void

    .line 64
    .end local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    :catch_0
    move-exception v0

    .line 65
    .restart local v0    # "ule":Ljava/lang/UnsatisfiedLinkError;
    const-string v1, "SecImaging"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to load the native library: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    sput-boolean v4, Lcom/samsung/android/secimaging/SecImaging;->sLibraryLoaded:Z

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    .line 77
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .line 80
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 81
    new-instance v1, Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;-><init>(Lcom/samsung/android/secimaging/SecImaging;Lcom/samsung/android/secimaging/SecImaging;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 83
    new-instance v1, Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;-><init>(Lcom/samsung/android/secimaging/SecImaging;Lcom/samsung/android/secimaging/SecImaging;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    goto :goto_0

    .line 85
    :cond_1
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .line 58
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    .line 98
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .line 101
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_1

    .line 102
    new-instance v1, Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;-><init>(Lcom/samsung/android/secimaging/SecImaging;Lcom/samsung/android/secimaging/SecImaging;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    .line 108
    :goto_0
    new-instance v1, Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-direct {v1, p1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    .line 109
    iget-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->loadSecEffectFilters()Z

    .line 111
    :cond_0
    return-void

    .line 103
    :cond_1
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 104
    new-instance v1, Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-direct {v1, p0, p0, v0}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;-><init>(Lcom/samsung/android/secimaging/SecImaging;Lcom/samsung/android/secimaging/SecImaging;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    goto :goto_0

    .line 106
    :cond_2
    iput-object v1, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/secimaging/SecImaging;)Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/secimaging/SecImaging;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    return-object v0
.end method

.method private native getValue(II)I
.end method

.method private native native_clear()V
.end method

.method static native native_convertBitmapData([IIII)[B
.end method

.method private native native_create(Ljava/lang/Object;ILjava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method private native native_deInitialize()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method static native native_decodeJpegToBitmap([BII)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doEffect(Ljava/lang/String;Ljava/lang/String;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doEffect([BIII)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doEffect([IIII)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doProcess()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doProcessSync(Ljava/lang/Object;II)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doProcessSync(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_doProcessSync([III)[I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_initialize(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setEffect(Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setInput([III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native native_setOutput([III)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0, "secimaging_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    .line 339
    if-eqz p0, :cond_0

    .line 340
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "secimaging_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/secimaging/SecImaging;

    .line 341
    .local v1, "s":Lcom/samsung/android/secimaging/SecImaging;
    if-nez v1, :cond_1

    .line 351
    .end local v1    # "s":Lcom/samsung/android/secimaging/SecImaging;
    :cond_0
    :goto_0
    return-void

    .line 343
    .restart local v1    # "s":Lcom/samsung/android/secimaging/SecImaging;
    :cond_1
    iget-object v2, v1, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    if-eqz v2, :cond_2

    .line 344
    iget-object v2, v1, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 345
    .local v0, "m":Landroid/os/Message;
    iget-object v2, v1, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    invoke-virtual {v2, v0}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 347
    .end local v0    # "m":Landroid/os/Message;
    :cond_2
    const-string v2, "SecImaging"

    const-string v3, "mEventHandler is null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private native setInt(II)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method


# virtual methods
.method public deInitialize()V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/samsung/android/secimaging/SecImaging;->native_deInitialize()V

    .line 264
    return-void
.end method

.method public doProcess(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 236
    if-nez p1, :cond_0

    .line 237
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Input image is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 239
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v2, v3, :cond_2

    .line 240
    const-string v2, "SecImaging"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Bitmap format is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Conversion required to ARGB_8888"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 244
    .local v0, "image_ARGB888":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    .line 245
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, v0, v2, v3}, Lcom/samsung/android/secimaging/SecImaging;->native_doProcessSync(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 247
    .local v1, "image_result":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 248
    const/4 v0, 0x0

    .line 256
    .end local v0    # "image_ARGB888":Landroid/graphics/Bitmap;
    .end local v1    # "image_result":Landroid/graphics/Bitmap;
    :goto_0
    return-object v1

    .line 253
    .restart local v0    # "image_ARGB888":Landroid/graphics/Bitmap;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 256
    .end local v0    # "image_ARGB888":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, p1, v2, v3}, Lcom/samsung/android/secimaging/SecImaging;->native_doProcessSync(Ljava/lang/Object;II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    move-object v1, v2

    goto :goto_0
.end method

.method public doProcess(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "InFilePath"    # Ljava/lang/String;
    .param p2, "OutFilePath"    # Ljava/lang/String;

    .prologue
    .line 224
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/secimaging/SecImaging;->native_doProcessSync(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    return-void
.end method

.method public doProcess([III)[I
    .locals 1
    .param p1, "inputBuffer"    # [I
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 212
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/secimaging/SecImaging;->native_doProcessSync([III)[I

    move-result-object v0

    return-object v0
.end method

.method public getEffectFilterId(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-nez v0, :cond_0

    .line 145
    const/4 v0, -0x1

    .line 146
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterId(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public getSupportedEffectFilterCount()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 132
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterCount()I

    move-result v0

    goto :goto_0
.end method

.method public getSupportedEffectFilterList(I)Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-nez v0, :cond_0

    .line 121
    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0, p1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterList(I)Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue(I)I
    .locals 1
    .param p1, "key"    # I

    .prologue
    .line 167
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/secimaging/SecImaging;->getValue(II)I

    move-result v0

    return v0
.end method

.method public initialize()V
    .locals 1

    .prologue
    .line 187
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/samsung/android/secimaging/SecImaging;->native_initialize(Ljava/lang/Object;)V

    .line 188
    return-void
.end method

.method native native_getOptionValue(Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionF(Ljava/lang/String;F)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionI(Ljava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method native native_setOptionS(Ljava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/RuntimeException;
        }
    .end annotation
.end method

.method public release()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mEventHandler:Lcom/samsung/android/secimaging/SecImaging$EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->clear()V

    .line 272
    :cond_1
    return-void
.end method

.method public setEffect(Ljava/lang/String;)V
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/samsung/android/secimaging/SecImaging;->native_setEffect(Ljava/lang/String;)V

    .line 199
    return-void
.end method

.method public setEffectFilter(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 155
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging;->mSecEffectFilterLoader:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-virtual {v0, p1}, Lcom/samsung/android/secimaging/SecEffectFilterLoader;->getEffectFilterForSet(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/secimaging/SecImaging;->setEffect(Ljava/lang/String;)V

    .line 157
    :cond_0
    return-void
.end method

.method public setSecImagingManagerListener(Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;)V
    .locals 2
    .param p1, "l"    # Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .prologue
    .line 304
    const-string v0, "SecImaging"

    const-string v1, "setSecImagingManagerListener"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iput-object p1, p0, Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    .line 306
    return-void
.end method

.method public setValue(II)V
    .locals 0
    .param p1, "key"    # I
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 180
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/secimaging/SecImaging;->setInt(II)I

    .line 181
    return-void
.end method
