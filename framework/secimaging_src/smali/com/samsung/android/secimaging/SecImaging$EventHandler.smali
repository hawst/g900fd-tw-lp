.class Lcom/samsung/android/secimaging/SecImaging$EventHandler;
.super Landroid/os/Handler;
.source "SecImaging.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/secimaging/SecImaging;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field private mImaging:Lcom/samsung/android/secimaging/SecImaging;

.field final synthetic this$0:Lcom/samsung/android/secimaging/SecImaging;


# direct methods
.method public constructor <init>(Lcom/samsung/android/secimaging/SecImaging;Lcom/samsung/android/secimaging/SecImaging;Landroid/os/Looper;)V
    .locals 0
    .param p2, "c"    # Lcom/samsung/android/secimaging/SecImaging;
    .param p3, "looper"    # Landroid/os/Looper;

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->this$0:Lcom/samsung/android/secimaging/SecImaging;

    .line 313
    invoke-direct {p0, p3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 314
    iput-object p2, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->mImaging:Lcom/samsung/android/secimaging/SecImaging;

    .line 315
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 318
    const-string v0, "SecImaging"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMessage, what = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arg1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " arg2 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 331
    const-string v0, "SecImaging"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown message type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 321
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->this$0:Lcom/samsung/android/secimaging/SecImaging;

    # getter for: Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    invoke-static {v0}, Lcom/samsung/android/secimaging/SecImaging;->access$000(Lcom/samsung/android/secimaging/SecImaging;)Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->this$0:Lcom/samsung/android/secimaging/SecImaging;

    # getter for: Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    invoke-static {v0}, Lcom/samsung/android/secimaging/SecImaging;->access$000(Lcom/samsung/android/secimaging/SecImaging;)Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;->onSecImagingManagerProgress(I)V

    goto :goto_0

    .line 326
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->this$0:Lcom/samsung/android/secimaging/SecImaging;

    # getter for: Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    invoke-static {v0}, Lcom/samsung/android/secimaging/SecImaging;->access$000(Lcom/samsung/android/secimaging/SecImaging;)Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecImaging$EventHandler;->this$0:Lcom/samsung/android/secimaging/SecImaging;

    # getter for: Lcom/samsung/android/secimaging/SecImaging;->mOnSecImagingManagerListener:Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;
    invoke-static {v0}, Lcom/samsung/android/secimaging/SecImaging;->access$000(Lcom/samsung/android/secimaging/SecImaging;)Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;

    move-result-object v1

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-interface {v1, v0}, Lcom/samsung/android/secimaging/SecImaging$OnSecImagingManagerListener;->onSecImagingManagerCompleted([B)V

    goto :goto_0

    .line 319
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
