.class public Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;
.super Ljava/lang/Object;
.source "SecEffectFilterLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/secimaging/SecEffectFilterLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EffectFilterItem"
.end annotation


# instance fields
.field private mFilter:Ljava/lang/String;

.field private mPackageName:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;

.field private mVersion:I

.field final synthetic this$0:Lcom/samsung/android/secimaging/SecEffectFilterLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/android/secimaging/SecEffectFilterLoader;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "filter"    # Ljava/lang/String;
    .param p3, "version"    # I
    .param p4, "title"    # Ljava/lang/String;
    .param p5, "packageName"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->this$0:Lcom/samsung/android/secimaging/SecEffectFilterLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p2, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mFilter:Ljava/lang/String;

    .line 170
    iput p3, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mVersion:I

    .line 171
    iput-object p4, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mTitle:Ljava/lang/String;

    .line 172
    iput-object p5, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mPackageName:Ljava/lang/String;

    .line 173
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->getEffectFiterName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getEffectFiterName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mFilter:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getEffectFilterPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getEffectFilterTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getEffectFilterVersion()I
    .locals 1

    .prologue
    .line 184
    iget v0, p0, Lcom/samsung/android/secimaging/SecEffectFilterLoader$EffectFilterItem;->mVersion:I

    return v0
.end method
