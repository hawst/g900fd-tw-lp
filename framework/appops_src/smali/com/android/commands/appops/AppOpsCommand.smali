.class public Lcom/android/commands/appops/AppOpsCommand;
.super Lcom/android/internal/os/BaseCommand;
.source "AppOpsCommand.java"


# static fields
.field private static final ARGUMENT_USER:Ljava/lang/String; = "--user"

.field private static final COMMAND_SET:Ljava/lang/String; = "set"

.field private static final MODE_ALLOW:Ljava/lang/String; = "allow"

.field private static final MODE_DEFAULT:Ljava/lang/String; = "default"

.field private static final MODE_DENY:Ljava/lang/String; = "deny"

.field private static final MODE_IGNORE:Ljava/lang/String; = "ignore"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/android/internal/os/BaseCommand;-><init>()V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 1
    .param p0, "args"    # [Ljava/lang/String;

    .prologue
    .line 38
    new-instance v0, Lcom/android/commands/appops/AppOpsCommand;

    invoke-direct {v0}, Lcom/android/commands/appops/AppOpsCommand;-><init>()V

    invoke-virtual {v0, p0}, Lcom/android/commands/appops/AppOpsCommand;->run([Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method private runSet()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    const/4 v6, 0x0

    .line 76
    .local v6, "packageName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 77
    .local v4, "op":Ljava/lang/String;
    const/4 v2, 0x0

    .line 78
    .local v2, "mode":Ljava/lang/String;
    const/4 v9, -0x2

    .line 79
    .local v9, "userId":I
    :goto_0
    invoke-virtual {p0}, Lcom/android/commands/appops/AppOpsCommand;->nextArg()Ljava/lang/String;

    move-result-object v1

    .local v1, "argument":Ljava/lang/String;
    if-eqz v1, :cond_4

    .line 80
    const-string v10, "--user"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/android/commands/appops/AppOpsCommand;->nextArgRequired()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    goto :goto_0

    .line 83
    :cond_0
    if-nez v6, :cond_1

    .line 84
    move-object v6, v1

    goto :goto_0

    .line 85
    :cond_1
    if-nez v4, :cond_2

    .line 86
    move-object v4, v1

    goto :goto_0

    .line 87
    :cond_2
    if-nez v2, :cond_3

    .line 88
    move-object v2, v1

    goto :goto_0

    .line 90
    :cond_3
    new-instance v10, Ljava/lang/IllegalArgumentException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Unsupported argument: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 95
    :cond_4
    if-nez v6, :cond_5

    .line 96
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Package name not specified."

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 97
    :cond_5
    if-nez v4, :cond_6

    .line 98
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Operation not specified."

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 99
    :cond_6
    if-nez v2, :cond_7

    .line 100
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Mode not specified."

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 103
    :cond_7
    invoke-static {v4}, Landroid/app/AppOpsManager;->strOpToOp(Ljava/lang/String;)I

    move-result v5

    .line 105
    .local v5, "opInt":I
    const/4 v10, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_8
    :goto_1
    packed-switch v10, :pswitch_data_0

    .line 119
    new-instance v10, Ljava/lang/IllegalArgumentException;

    const-string v11, "Mode is invalid."

    invoke-direct {v10, v11}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 105
    :sswitch_0
    const-string v11, "allow"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v10, 0x0

    goto :goto_1

    :sswitch_1
    const-string v11, "deny"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v10, 0x1

    goto :goto_1

    :sswitch_2
    const-string v11, "ignore"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v10, 0x2

    goto :goto_1

    :sswitch_3
    const-string v11, "default"

    invoke-virtual {v2, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const/4 v10, 0x3

    goto :goto_1

    .line 107
    :pswitch_0
    const/4 v3, 0x0

    .line 124
    .local v3, "modeInt":I
    :goto_2
    const/4 v10, -0x2

    if-ne v9, v10, :cond_9

    .line 125
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v9

    .line 128
    :cond_9
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v7

    .line 129
    .local v7, "pm":Landroid/content/pm/IPackageManager;
    const-string v10, "appops"

    invoke-static {v10}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/android/internal/app/IAppOpsService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/app/IAppOpsService;

    move-result-object v0

    .line 131
    .local v0, "appOpsService":Lcom/android/internal/app/IAppOpsService;
    invoke-interface {v7, v6, v9}, Landroid/content/pm/IPackageManager;->getPackageUid(Ljava/lang/String;I)I

    move-result v8

    .line 132
    .local v8, "uid":I
    if-gez v8, :cond_a

    .line 133
    new-instance v10, Ljava/lang/Exception;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "No UID for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " for user "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v10

    .line 110
    .end local v0    # "appOpsService":Lcom/android/internal/app/IAppOpsService;
    .end local v3    # "modeInt":I
    .end local v7    # "pm":Landroid/content/pm/IPackageManager;
    .end local v8    # "uid":I
    :pswitch_1
    const/4 v3, 0x2

    .line 111
    .restart local v3    # "modeInt":I
    goto :goto_2

    .line 113
    .end local v3    # "modeInt":I
    :pswitch_2
    const/4 v3, 0x1

    .line 114
    .restart local v3    # "modeInt":I
    goto :goto_2

    .line 116
    .end local v3    # "modeInt":I
    :pswitch_3
    const/4 v3, 0x3

    .line 117
    .restart local v3    # "modeInt":I
    goto :goto_2

    .line 135
    .restart local v0    # "appOpsService":Lcom/android/internal/app/IAppOpsService;
    .restart local v7    # "pm":Landroid/content/pm/IPackageManager;
    .restart local v8    # "uid":I
    :cond_a
    invoke-interface {v0, v5, v8, v6, v3}, Lcom/android/internal/app/IAppOpsService;->setMode(IILjava/lang/String;I)V

    .line 136
    return-void

    .line 105
    :sswitch_data_0
    .sparse-switch
        -0x46f4022e -> :sswitch_2
        0x2efe0c -> :sswitch_1
        0x589a349 -> :sswitch_0
        0x5c13d641 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public onRun()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/android/commands/appops/AppOpsCommand;->nextArgRequired()Ljava/lang/String;

    move-result-object v0

    .line 56
    .local v0, "command":Ljava/lang/String;
    const/4 v1, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown command \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\'."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56
    :pswitch_0
    const-string v2, "set"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 58
    :pswitch_1
    invoke-direct {p0}, Lcom/android/commands/appops/AppOpsCommand;->runSet()V

    .line 64
    return-void

    .line 56
    :pswitch_data_0
    .packed-switch 0x1bc62
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method

.method public onShowUsage(Ljava/io/PrintStream;)V
    .locals 1
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 43
    const-string v0, "usage: adb shell appops set <PACKAGE> <OP> <allow|ignore|deny|default> [--user <USER_ID>]\n  <PACKAGE> an Android package name.\n  <OP>      an AppOps operation.\n  <USER_ID> the user id under which the package is installed. If --user is not\n            specified, the current user is assumed.\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 49
    return-void
.end method
