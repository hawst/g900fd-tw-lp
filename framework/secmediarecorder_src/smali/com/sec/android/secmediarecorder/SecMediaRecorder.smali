.class public Lcom/sec/android/secmediarecorder/SecMediaRecorder;
.super Ljava/lang/Object;
.source "SecMediaRecorder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$AppStyle;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$VideoEncoder;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$AudioEncoder;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$OutputFormat;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$VideoSource;,
        Lcom/sec/android/secmediarecorder/SecMediaRecorder$AudioSource;
    }
.end annotation


# static fields
.field private static final MEDIARECORDER_CMD_PAUSE:I = 0x3e8

.field private static final MEDIARECORDER_CMD_RESUME:I = 0x3e9

.field public static final MEDIA_ERROR_SERVER_DIED:I = 0x64

.field public static final MEDIA_RECORDER_ERROR_BUFFER_OVERFLOW:I = 0x2

.field public static final MEDIA_RECORDER_ERROR_UNKNOWN:I = 0x1

.field public static final MEDIA_RECORDER_INFO_COMPLETION_STATUS:I = 0x322

.field public static final MEDIA_RECORDER_INFO_DURATION_PROGRESS:I = 0x385

.field public static final MEDIA_RECORDER_INFO_FILESIZE_PROGRESS:I = 0x384

.field public static final MEDIA_RECORDER_INFO_MAX_DURATION_REACHED:I = 0x320

.field public static final MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED:I = 0x321

.field public static final MEDIA_RECORDER_INFO_PROGRESS_FRAME_STATUS:I = 0x323

.field public static final MEDIA_RECORDER_INFO_PROGRESS_TIME_STATUS:I = 0x324

.field public static final MEDIA_RECORDER_INFO_UNKNOWN:I = 0x1

.field public static final MEDIA_RECORDER_TRACK_INFO_COMPLETION_STATUS:I = 0x3e8

.field public static final MEDIA_RECORDER_TRACK_INFO_DATA_KBYTES:I = 0x3f1

.field public static final MEDIA_RECORDER_TRACK_INFO_DURATION_MS:I = 0x3eb

.field public static final MEDIA_RECORDER_TRACK_INFO_ENCODED_FRAMES:I = 0x3ed

.field public static final MEDIA_RECORDER_TRACK_INFO_INITIAL_DELAY_MS:I = 0x3ef

.field public static final MEDIA_RECORDER_TRACK_INFO_LIST_END:I = 0x7d0

.field public static final MEDIA_RECORDER_TRACK_INFO_LIST_START:I = 0x3e8

.field public static final MEDIA_RECORDER_TRACK_INFO_MAX_CHUNK_DUR_MS:I = 0x3ec

.field public static final MEDIA_RECORDER_TRACK_INFO_PROGRESS_IN_TIME:I = 0x3e9

.field public static final MEDIA_RECORDER_TRACK_INFO_START_OFFSET_MS:I = 0x3f0

.field public static final MEDIA_RECORDER_TRACK_INFO_TYPE:I = 0x3ea

.field public static final MEDIA_RECORDER_TRACK_INTER_CHUNK_TIME_MS:I = 0x3ee

.field private static final TAG:Ljava/lang/String; = "SecMediaRecorder"


# instance fields
.field private mAudioSource:I

.field private mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

.field private mFd:Ljava/io/FileDescriptor;

.field private mIsAudioEnabled:Z

.field private mIsVideoRecordEnabled:Z

.field private mNativeContext:J

.field private mOnErrorListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;

.field private mOnInfoListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

.field private mPath:Ljava/lang/String;

.field private mSurface:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 105
    const-string v0, "secmediarecorder_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 106
    invoke-static {}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_init()V

    .line 107
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-boolean v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsAudioEnabled:Z

    .line 130
    iput-boolean v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsVideoRecordEnabled:Z

    .line 131
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    .line 140
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .local v0, "looper":Landroid/os/Looper;
    if-eqz v0, :cond_0

    .line 141
    new-instance v2, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    invoke-direct {v2, p0, p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;-><init>(Lcom/sec/android/secmediarecorder/SecMediaRecorder;Lcom/sec/android/secmediarecorder/SecMediaRecorder;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    .line 148
    :goto_0
    invoke-static {}, Landroid/app/ActivityThread;->currentPackageName()Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "packageName":Ljava/lang/String;
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {p0, v2, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_setup(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    return-void

    .line 142
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_0
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 143
    new-instance v2, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    invoke-direct {v2, p0, p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;-><init>(Lcom/sec/android/secmediarecorder/SecMediaRecorder;Lcom/sec/android/secmediarecorder/SecMediaRecorder;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    goto :goto_0

    .line 145
    :cond_1
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    goto :goto_0
.end method

.method private native _prepare()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native _setAudioSource(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setOutputFile(Ljava/io/FileDescriptor;JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native _setVideoSource(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method static synthetic access$000(Lcom/sec/android/secmediarecorder/SecMediaRecorder;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mNativeContext:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/sec/android/secmediarecorder/SecMediaRecorder;)Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mOnErrorListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/secmediarecorder/SecMediaRecorder;)Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mOnInfoListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

    return-object v0
.end method

.method private static checkAudioRecordEnabled()Z
    .locals 5

    .prologue
    .line 1313
    const/4 v1, 0x1

    .line 1315
    .local v1, "enabled":Z
    :try_start_0
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v2

    .line 1317
    .local v2, "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/sec/enterprise/RestrictionPolicy;->isAudioRecordAllowed(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1322
    .end local v2    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_0
    if-nez v1, :cond_1

    .line 1323
    const-string v3, "SecMediaRecorder"

    const-string v4, "AUDIO RECORD IS DISABLED"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1324
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x2710

    if-ge v3, v4, :cond_0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x4e1f

    if-gt v3, v4, :cond_1

    .line 1326
    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 1327
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 1330
    :cond_1
    return v1

    .line 1318
    :catch_0
    move-exception v0

    .line 1319
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static checkMicrophoneEnabled()Z
    .locals 5

    .prologue
    .line 1281
    const/4 v1, 0x1

    .line 1284
    .local v1, "enabled":Z
    :try_start_0
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v2

    .line 1286
    .local v2, "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/sec/enterprise/RestrictionPolicy;->isMicrophoneEnabled(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1292
    .end local v2    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_0
    if-nez v1, :cond_0

    .line 1294
    const-string v3, "SecMediaRecorder"

    const-string v4, "MICROPHONE IS DISABLED"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x2710

    if-lt v3, v4, :cond_0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x4e1f

    if-gt v3, v4, :cond_0

    .line 1298
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 1299
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 1303
    :cond_0
    return v1

    .line 1288
    :catch_0
    move-exception v0

    .line 1289
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private static checkVideoRecordEnabled()Z
    .locals 5

    .prologue
    .line 1339
    const/4 v1, 0x1

    .line 1341
    .local v1, "enabled":Z
    :try_start_0
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v2

    .line 1343
    .local v2, "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/sec/enterprise/RestrictionPolicy;->isVideoRecordAllowed(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1348
    .end local v2    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_0
    if-nez v1, :cond_1

    .line 1349
    const-string v3, "SecMediaRecorder"

    const-string v4, "VIDEO RECORD IS DISABLED"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1350
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x2710

    if-ge v3, v4, :cond_0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    const/16 v4, 0x4e1f

    if-gt v3, v4, :cond_1

    .line 1352
    :cond_0
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->killProcess(I)V

    .line 1353
    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/System;->exit(I)V

    .line 1356
    :cond_1
    return v1

    .line 1344
    :catch_0
    move-exception v0

    .line 1345
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static final getAudioSourceMax()I
    .locals 1

    .prologue
    .line 482
    const/16 v0, 0x18

    return v0
.end method

.method private final native native_finalize()V
.end method

.method private static final native native_init()V
.end method

.method private final native native_registerRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)I
.end method

.method private native native_reset()V
.end method

.method private final native native_sendcommand(III)V
.end method

.method private final native native_setup(Ljava/lang/Object;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private final native native_unregisterRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)I
.end method

.method private static postEventFromNative(Ljava/lang/Object;IIILjava/lang/Object;)V
    .locals 4
    .param p0, "secmediarecorder_ref"    # Ljava/lang/Object;
    .param p1, "what"    # I
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    .line 1249
    check-cast p0, Ljava/lang/ref/WeakReference;

    .end local p0    # "secmediarecorder_ref":Ljava/lang/Object;
    invoke-virtual {p0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;

    .line 1250
    .local v1, "mr":Lcom/sec/android/secmediarecorder/SecMediaRecorder;
    if-nez v1, :cond_1

    .line 1275
    :cond_0
    :goto_0
    return-void

    .line 1254
    :cond_1
    iget-boolean v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsAudioEnabled:Z

    if-ne v2, v3, :cond_2

    .line 1255
    invoke-static {}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->checkMicrophoneEnabled()Z

    .line 1258
    :cond_2
    iget v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    if-eq v2, v3, :cond_3

    iget v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/4 v3, 0x5

    if-eq v2, v3, :cond_3

    iget v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v3, 0x9

    if-eq v2, v3, :cond_3

    iget v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_4

    :cond_3
    invoke-static {}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->checkAudioRecordEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1265
    :cond_4
    iget-boolean v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsVideoRecordEnabled:Z

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->checkVideoRecordEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1271
    :cond_5
    iget-object v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    if-eqz v2, :cond_0

    .line 1272
    iget-object v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    invoke-virtual {v2, p1, p2, p3, p4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1273
    .local v0, "m":Landroid/os/Message;
    iget-object v2, v1, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    invoke-virtual {v2, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private setAuxVideoParameters()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 811
    invoke-static {v5}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    .line 812
    .local v0, "profile":Landroid/media/CamcorderProfile;
    if-eqz v0, :cond_0

    .line 813
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "video-aux-param-width=%d"

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, v0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 814
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "video-aux-param-height=%d"

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 815
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "video-aux-param-encoding-bitrate=%d"

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, v0, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 819
    :goto_0
    return-void

    .line 817
    :cond_0
    const-string v1, "SecMediaRecorder"

    const-string v2, "setAuxVideoParameters profile is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static final native setGLSurface(Landroid/view/Surface;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method private native setParameter(Ljava/lang/String;)V
.end method


# virtual methods
.method protected finalize()V
    .locals 0

    .prologue
    .line 1394
    invoke-direct {p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_finalize()V

    return-void
.end method

.method public native getMaxAmplitude()I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native getSurface()Landroid/view/Surface;
.end method

.method public final pause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1430
    const/16 v0, 0x3e8

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_sendcommand(III)V

    .line 1431
    return-void
.end method

.method public prepare()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const-wide/16 v2, 0x0

    .line 908
    const/4 v10, 0x1

    .line 909
    .local v10, "videoRecordEnabled":Z
    const/4 v6, 0x1

    .line 910
    .local v6, "audioRecordEnabled":Z
    iget-boolean v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsVideoRecordEnabled:Z

    if-eqz v0, :cond_0

    .line 912
    :try_start_0
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v9

    .line 913
    .local v9, "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/sec/enterprise/RestrictionPolicy;->isVideoRecordAllowed(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v10

    .line 917
    .end local v9    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_0
    if-nez v10, :cond_0

    .line 918
    new-instance v0, Ljava/io/IOException;

    const-string v1, "MDM is blocking video recording"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 914
    :catch_0
    move-exception v7

    .line 915
    .local v7, "e":Ljava/lang/Exception;
    const/4 v10, 0x1

    goto :goto_0

    .line 921
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_0
    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v1, 0xa

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v1, 0xe

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/16 v1, 0x15

    if-eq v0, v1, :cond_1

    iget v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 930
    :cond_1
    :try_start_1
    invoke-static {}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getInstance()Landroid/sec/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/sec/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/sec/enterprise/RestrictionPolicy;

    move-result-object v9

    .line 931
    .restart local v9    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/sec/enterprise/RestrictionPolicy;->isAudioRecordAllowed(Z)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v6

    .line 935
    .end local v9    # "rp":Landroid/sec/enterprise/RestrictionPolicy;
    :goto_1
    if-nez v6, :cond_2

    .line 936
    new-instance v0, Ljava/io/IOException;

    const-string v1, "MDM is blocking audio recording"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 932
    :catch_1
    move-exception v7

    .line 933
    .restart local v7    # "e":Ljava/lang/Exception;
    const/4 v6, 0x1

    goto :goto_1

    .line 941
    .end local v7    # "e":Ljava/lang/Exception;
    :cond_2
    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 942
    new-instance v8, Ljava/io/RandomAccessFile;

    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mPath:Ljava/lang/String;

    const-string v1, "rws"

    invoke-direct {v8, v0, v1}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    .local v8, "file":Ljava/io/RandomAccessFile;
    :try_start_2
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->_setOutputFile(Ljava/io/FileDescriptor;JJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 946
    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    .line 954
    .end local v8    # "file":Ljava/io/RandomAccessFile;
    :goto_2
    invoke-direct {p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->_prepare()V

    .line 955
    return-void

    .line 946
    .restart local v8    # "file":Ljava/io/RandomAccessFile;
    :catchall_0
    move-exception v0

    invoke-virtual {v8}, Ljava/io/RandomAccessFile;->close()V

    throw v0

    .line 948
    .end local v8    # "file":Ljava/io/RandomAccessFile;
    :cond_3
    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mFd:Ljava/io/FileDescriptor;

    if-eqz v0, :cond_4

    .line 949
    iget-object v1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mFd:Ljava/io/FileDescriptor;

    move-object v0, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->_setOutputFile(Ljava/io/FileDescriptor;JJ)V

    goto :goto_2

    .line 951
    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No valid output file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public registerRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)V
    .locals 0
    .param p1, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 1605
    invoke-direct {p0, p1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_registerRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)I

    .line 1606
    return-void
.end method

.method public native release()V
.end method

.method public reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 992
    invoke-direct {p0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_reset()V

    .line 995
    iget-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mEventHandler:Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder$EventHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 997
    iput-boolean v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsAudioEnabled:Z

    .line 998
    iput-boolean v2, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsVideoRecordEnabled:Z

    .line 999
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    .line 1001
    return-void
.end method

.method public final resume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1439
    const/16 v0, 0x3e9

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_sendcommand(III)V

    .line 1440
    return-void
.end method

.method public set3dVideo(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1561
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-three-d-video="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1562
    return-void
.end method

.method public setAlbum(Ljava/lang/String;)V
    .locals 2
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 1477
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-album="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1478
    return-void
.end method

.method public setAudioChannels(I)V
    .locals 2
    .param p1, "numChannels"    # I

    .prologue
    .line 766
    if-gtz p1, :cond_0

    .line 767
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Number of channels is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 769
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-number-of-channels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 770
    return-void
.end method

.method public native setAudioEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setAudioEncodingBitRate(I)V
    .locals 2
    .param p1, "bitRate"    # I

    .prologue
    .line 782
    if-gtz p1, :cond_0

    .line 783
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Audio encoding bit rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 785
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-encoding-bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 786
    return-void
.end method

.method public setAudioSamplingRate(I)V
    .locals 2
    .param p1, "samplingRate"    # I

    .prologue
    .line 751
    if-gtz p1, :cond_0

    .line 752
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Audio sampling rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 754
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "audio-param-sampling-rate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 755
    return-void
.end method

.method public setAudioSource(I)V
    .locals 1
    .param p1, "audio_source"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 453
    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    .line 456
    invoke-static {}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->checkMicrophoneEnabled()Z

    .line 457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsAudioEnabled:Z

    .line 460
    :cond_0
    iput p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mAudioSource:I

    .line 462
    invoke-direct {p0, p1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->_setAudioSource(I)V

    .line 464
    return-void
.end method

.method public setAuthor(I)V
    .locals 2
    .param p1, "author"    # I

    .prologue
    .line 1508
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-author="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1509
    return-void
.end method

.method public setAuxiliaryOutputFile(Ljava/io/FileDescriptor;)V
    .locals 2
    .param p1, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 829
    const-string v0, "SecMediaRecorder"

    const-string v1, "setAuxiliaryOutputFile(FileDescriptor) is no longer supported."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    return-void
.end method

.method public setAuxiliaryOutputFile(Ljava/lang/String;)V
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 840
    const-string v0, "SecMediaRecorder"

    const-string v1, "setAuxiliaryOutputFile(String) is no longer supported."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 841
    return-void
.end method

.method public native setCamera(Lcom/sec/android/seccamera/SecCamera;)V
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end method

.method public setCaptureRate(D)V
    .locals 9
    .param p1, "fps"    # D

    .prologue
    const/4 v8, 0x0

    .line 555
    const-string v4, "time-lapse-enable=1"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 557
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, p1

    .line 558
    .local v0, "timeBetweenFrameCapture":D
    const-wide v4, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v4, v0

    double-to-long v2, v4

    .line 559
    .local v2, "timeBetweenFrameCaptureUs":J
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "time-between-time-lapse-frame-capture=%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 561
    return-void
.end method

.method public setCityId(J)V
    .locals 3
    .param p1, "cityId"    # J

    .prologue
    .line 1554
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-cityid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1555
    return-void
.end method

.method public setDualCapture(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1534
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-dual-capture="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1535
    return-void
.end method

.method public native setDurationInterval(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public native setFileSizeInterval(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public final setHDMICableConnected(I)V
    .locals 2
    .param p1, "connected"    # I

    .prologue
    .line 1448
    const/16 v0, 0x461

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_sendcommand(III)V

    .line 1449
    return-void
.end method

.method public setIFrameInterval(I)V
    .locals 2
    .param p1, "seconds"    # I

    .prologue
    .line 1574
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "video-param-i-frames-interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1575
    return-void
.end method

.method public setLocation(FF)V
    .locals 9
    .param p1, "latitude"    # F
    .param p2, "longitude"    # F

    .prologue
    const v8, 0x461c4000    # 10000.0f

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 605
    mul-float v3, p1, v8

    float-to-double v4, v3

    add-double/2addr v4, v6

    double-to-int v0, v4

    .line 606
    .local v0, "latitudex10000":I
    mul-float v3, p2, v8

    float-to-double v4, v3

    add-double/2addr v4, v6

    double-to-int v1, v4

    .line 608
    .local v1, "longitudex10000":I
    const v3, 0xdbba0

    if-gt v0, v3, :cond_0

    const v3, -0xdbba0

    if-ge v0, v3, :cond_1

    .line 609
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Latitude: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " out of range."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 610
    .local v2, "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 612
    .end local v2    # "msg":Ljava/lang/String;
    :cond_1
    const v3, 0x1b7740

    if-gt v1, v3, :cond_2

    const v3, -0x1b7740

    if-ge v1, v3, :cond_3

    .line 613
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Longitude: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " out of range"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 614
    .restart local v2    # "msg":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 617
    .end local v2    # "msg":Ljava/lang/String;
    :cond_3
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "param-geotag-latitude="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 618
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "param-geotag-longitude="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 619
    return-void
.end method

.method public native setMaxDuration(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public native setMaxFileSize(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation
.end method

.method public setOnErrorListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;

    .prologue
    .line 1060
    iput-object p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mOnErrorListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnErrorListener;

    .line 1061
    return-void
.end method

.method public setOnInfoListener(Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

    .prologue
    .line 1184
    iput-object p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mOnInfoListener:Lcom/sec/android/secmediarecorder/SecMediaRecorder$OnInfoListener;

    .line 1185
    return-void
.end method

.method public setOrientationHint(I)V
    .locals 3
    .param p1, "degrees"    # I

    .prologue
    .line 579
    if-eqz p1, :cond_0

    const/16 v0, 0x5a

    if-eq p1, v0, :cond_0

    const/16 v0, 0xb4

    if-eq p1, v0, :cond_0

    const/16 v0, 0x10e

    if-eq p1, v0, :cond_0

    .line 583
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported angle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 585
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "video-param-rotation-angle-degrees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 586
    return-void
.end method

.method public setOutputFile(Ljava/io/FileDescriptor;)V
    .locals 1
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 872
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mPath:Ljava/lang/String;

    .line 873
    iput-object p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mFd:Ljava/io/FileDescriptor;

    .line 874
    return-void
.end method

.method public setOutputFile(Ljava/lang/String;)V
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 886
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mFd:Ljava/io/FileDescriptor;

    .line 887
    iput-object p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mPath:Ljava/lang/String;

    .line 888
    return-void
.end method

.method public native setOutputFormat(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setPerformer(Ljava/lang/String;)V
    .locals 2
    .param p1, "performer"    # Ljava/lang/String;

    .prologue
    .line 1467
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-performer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1468
    return-void
.end method

.method public setPreviewDisplay(Landroid/view/Surface;)V
    .locals 0
    .param p1, "sv"    # Landroid/view/Surface;

    .prologue
    .line 194
    iput-object p1, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mSurface:Landroid/view/Surface;

    .line 195
    return-void
.end method

.method public setProfile(Landroid/media/CamcorderProfile;)V
    .locals 2
    .param p1, "profile"    # Landroid/media/CamcorderProfile;

    .prologue
    .line 520
    iget v0, p1, Landroid/media/CamcorderProfile;->fileFormat:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setOutputFormat(I)V

    .line 521
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setVideoFrameRate(I)V

    .line 522
    iget v0, p1, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v1, p1, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setVideoSize(II)V

    .line 523
    iget v0, p1, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setVideoEncodingBitRate(I)V

    .line 524
    iget v0, p1, Landroid/media/CamcorderProfile;->videoCodec:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setVideoEncoder(I)V

    .line 525
    iget v0, p1, Landroid/media/CamcorderProfile;->quality:I

    const/16 v1, 0x3e8

    if-lt v0, v1, :cond_1

    iget v0, p1, Landroid/media/CamcorderProfile;->quality:I

    const/16 v1, 0x3ee

    if-gt v0, v1, :cond_1

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 529
    :cond_1
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    if-ltz v0, :cond_0

    .line 530
    iget v0, p1, Landroid/media/CamcorderProfile;->audioBitRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncodingBitRate(I)V

    .line 531
    iget v0, p1, Landroid/media/CamcorderProfile;->audioChannels:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioChannels(I)V

    .line 532
    iget v0, p1, Landroid/media/CamcorderProfile;->audioSampleRate:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioSamplingRate(I)V

    .line 533
    iget v0, p1, Landroid/media/CamcorderProfile;->audioCodec:I

    invoke-virtual {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setAudioEncoder(I)V

    goto :goto_0
.end method

.method public setRecordingMode(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 1525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-recording-mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1526
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 1457
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1458
    return-void
.end method

.method public native setVideoEncoder(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setVideoEncodingBitRate(I)V
    .locals 2
    .param p1, "bitRate"    # I

    .prologue
    .line 798
    if-gtz p1, :cond_0

    .line 799
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Video encoding bit rate is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 801
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "video-param-encoding-bitrate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 802
    return-void
.end method

.method public native setVideoFrameRate(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native setVideoSize(II)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public setVideoSource(I)V
    .locals 1
    .param p1, "video_source"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 499
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->mIsVideoRecordEnabled:Z

    .line 500
    invoke-direct {p0, p1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->_setVideoSource(I)V

    .line 502
    return-void
.end method

.method public setWeather(I)V
    .locals 2
    .param p1, "weather"    # I

    .prologue
    .line 1544
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "param-meta-weather="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V

    .line 1545
    return-void
.end method

.method public setZoom(I)V
    .locals 5
    .param p1, "zoomLevel"    # I

    .prologue
    .line 850
    if-gez p1, :cond_0

    .line 851
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "zoom level  is not positive"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 855
    :cond_0
    :try_start_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "zoom=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->setParameter(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 859
    :goto_0
    return-void

    .line 856
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public native start()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public native stop()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method public unregisterRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)V
    .locals 0
    .param p1, "camera"    # Lcom/sec/android/seccamera/SecCamera;

    .prologue
    .line 1595
    invoke-direct {p0, p1}, Lcom/sec/android/secmediarecorder/SecMediaRecorder;->native_unregisterRecordingSurface(Lcom/sec/android/seccamera/SecCamera;)I

    .line 1596
    return-void
.end method
