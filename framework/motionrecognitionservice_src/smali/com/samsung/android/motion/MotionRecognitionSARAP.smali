.class public abstract Lcom/samsung/android/motion/MotionRecognitionSARAP;
.super Ljava/lang/Object;
.source "MotionRecognitionSARAP.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/motion/MotionRecognitionSARAP$1;,
        Lcom/samsung/android/motion/MotionRecognitionSARAP$PowerBckOffReceiver;,
        Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;
    }
.end annotation


# static fields
.field private static final ACTION_POWER_BACKOFF:Ljava/lang/String; = "com.samsung.MotionRecognitionService.action.POWER_BACKOFF"

.field private static final AUTO_POWER_OFF_REQUEST:I = 0x0

.field protected static final BOTTOM_UP:I = 0x2

.field public static ENG_MODE:Z = false

.field protected static final FLAT_DOWN:I = 0x1

.field protected static final FLAT_UP:I = 0x0

.field private static final MAX_MAGNITUDE:F = 10.8f

.field private static final MAX_TILT:I = 0x64

.field private static final MIN_MAGNITUDE:F = 8.8f

.field private static final MIN_TILT:I = 0x50

.field protected static final NO_FLAT:I = -0x1

.field private static final RADIANS_TO_DEGREES:F = 57.29578f

.field private static final TAG:Ljava/lang/String; = "MotionRecognitionSARAP"


# instance fields
.field private cnt:I

.field private mAcquireWakelock:Z

.field private mAlarm:Landroid/app/AlarmManager;

.field private final mAutoPowerOffLock:Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mPm:Landroid/os/PowerManager;

.field private mPollingTime:J

.field private mProxlistener:Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;

.field private mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSm:Landroid/hardware/SensorManager;

.field private mStatus:Z

.field private mbProximity:Z

.field private mfilter:Landroid/content/IntentFilter;

.field private moldcondition:I

.field private moldstatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 59
    const-string v0, "ro.build.type"

    const-string v1, "Default"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->ENG_MODE:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    const/4 v2, -0x1

    const/4 v4, 0x0

    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    .line 48
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAutoPowerOffLock:Ljava/lang/Object;

    .line 50
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    .line 51
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    .line 52
    const-wide/32 v2, 0x186a0

    iput-wide v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPollingTime:J

    .line 54
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAcquireWakelock:Z

    .line 88
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mbProximity:Z

    .line 91
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    .line 92
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    .line 93
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    const-string v2, "sensor"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/SensorManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    .line 94
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    const-string v2, "alarm"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAlarm:Landroid/app/AlarmManager;

    .line 95
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.MotionRecognitionService.action.POWER_BACKOFF"

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 96
    .local v0, "powerBackOffIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPendingIntent:Landroid/app/PendingIntent;

    .line 97
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mfilter:Landroid/content/IntentFilter;

    .line 98
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mfilter:Landroid/content/IntentFilter;

    const-string v2, "com.samsung.MotionRecognitionService.action.POWER_BACKOFF"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    new-instance v1, Lcom/samsung/android/motion/MotionRecognitionSARAP$PowerBckOffReceiver;

    invoke-direct {v1, p0, v5}, Lcom/samsung/android/motion/MotionRecognitionSARAP$PowerBckOffReceiver;-><init>(Lcom/samsung/android/motion/MotionRecognitionSARAP;Lcom/samsung/android/motion/MotionRecognitionSARAP$1;)V

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 101
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPm:Landroid/os/PowerManager;

    .line 102
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPm:Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "back_off"

    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;

    .line 103
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 105
    new-instance v1, Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;

    invoke-direct {v1, p0}, Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;-><init>(Lcom/samsung/android/motion/MotionRecognitionSARAP;)V

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mProxlistener:Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;

    .line 106
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/android/motion/MotionRecognitionSARAP;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSARAP;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mbProximity:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/motion/MotionRecognitionSARAP;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSARAP;

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->acquire()V

    return-void
.end method

.method private acquire()V
    .locals 4

    .prologue
    .line 117
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAutoPowerOffLock:Ljava/lang/Object;

    monitor-enter v1

    .line 118
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAcquireWakelock:Z

    .line 120
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    .line 121
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, p0, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 122
    const-string v0, "MotionRecognitionSARAP"

    const-string v2, "acquire"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    monitor-exit v1

    .line 124
    return-void

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private checkTilt(FFF)I
    .locals 11
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    const/16 v10, 0x64

    const/16 v9, 0x50

    const/4 v3, -0x1

    const/4 v8, 0x0

    const-wide v6, 0x404ca5dc20000000L    # 57.295780181884766

    .line 253
    mul-float v4, p1, p1

    mul-float v5, p2, p2

    add-float/2addr v4, v5

    mul-float v5, p3, p3

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 254
    .local v0, "magnitude":F
    const v4, 0x410ccccd    # 8.8f

    cmpg-float v4, v0, v4

    if-ltz v4, :cond_0

    const v4, 0x412ccccd    # 10.8f

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    .line 255
    :cond_0
    const-string v4, "MotionRecognitionSARAP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "magnitude error : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", magnitude = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :goto_0
    return v3

    .line 258
    :cond_1
    div-float v4, p3, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v2, v4

    .line 259
    .local v2, "tiltAngle_Z":I
    div-float v4, p2, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->asin(D)D

    move-result-wide v4

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v1, v4

    .line 260
    .local v1, "tiltAngle_Y":I
    const-string v4, "MotionRecognitionSARAP"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "tiltAngle_Z : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  tiltAngle_Y : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-lt v4, v9, :cond_3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v10, :cond_3

    .line 263
    cmpl-float v3, p3, v8

    if-ltz v3, :cond_2

    .line 264
    const-string v3, "MotionRecognitionSARAP"

    const-string v4, " # FLAT_UP "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    const/4 v3, 0x0

    goto :goto_0

    .line 268
    :cond_2
    const-string v3, "MotionRecognitionSARAP"

    const-string v4, " # FLAT_DOWN "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v3, 0x1

    goto :goto_0

    .line 272
    :cond_3
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-lt v4, v9, :cond_4

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    if-gt v4, v10, :cond_4

    .line 273
    cmpg-float v4, p2, v8

    if-gtz v4, :cond_4

    .line 274
    const-string v3, "MotionRecognitionSARAP"

    const-string v4, " # BOTTOM_UP "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    const/4 v3, 0x2

    goto :goto_0

    .line 278
    :cond_4
    const-string v4, "MotionRecognitionSARAP"

    const-string v5, " # NO_FLAT "

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private release()V
    .locals 4

    .prologue
    .line 126
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAutoPowerOffLock:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 128
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    .line 129
    iget-wide v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPollingTime:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->updatePollingTime(J)V

    .line 130
    const-string v0, "MotionRecognitionSARAP"

    const-string v2, "release"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAcquireWakelock:Z

    .line 133
    monitor-exit v1

    .line 134
    return-void

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private setCondition()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 211
    sget-boolean v0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->ENG_MODE:Z

    if-eqz v0, :cond_7

    .line 212
    const-string v0, "MotionRecognitionSARAP"

    const-string v1, "ENG mode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-nez v0, :cond_1

    .line 214
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p0, v5}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 216
    :cond_0
    iput v5, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    .line 250
    :goto_0
    return-void

    .line 217
    :cond_1
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-ne v0, v2, :cond_3

    .line 218
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v2, v0, :cond_2

    .line 219
    invoke-virtual {p0, v2}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 220
    :cond_2
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 221
    :cond_3
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-ne v0, v3, :cond_5

    .line 222
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v3, v0, :cond_4

    .line 223
    invoke-virtual {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 224
    :cond_4
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 226
    :cond_5
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v4, v0, :cond_6

    .line 227
    invoke-virtual {p0, v4}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 228
    :cond_6
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 231
    :cond_7
    const-string v0, "MotionRecognitionSARAP"

    const-string v1, "USER mode"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mbProximity:Z

    if-ne v0, v2, :cond_9

    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-nez v0, :cond_9

    .line 233
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eqz v0, :cond_8

    .line 234
    invoke-virtual {p0, v5}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 235
    :cond_8
    iput v5, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 236
    :cond_9
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mbProximity:Z

    if-nez v0, :cond_b

    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-ne v0, v2, :cond_b

    .line 237
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v2, v0, :cond_a

    .line 238
    invoke-virtual {p0, v2}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 239
    :cond_a
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 240
    :cond_b
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-ne v0, v3, :cond_d

    .line 241
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v3, v0, :cond_c

    .line 242
    invoke-virtual {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 243
    :cond_c
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0

    .line 245
    :cond_d
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    if-eq v4, v0, :cond_e

    .line 246
    invoke-virtual {p0, v4}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->onChangeSARCondition(I)V

    .line 247
    :cond_e
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    goto :goto_0
.end method

.method private updatePollingTime(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAlarm:Landroid/app/AlarmManager;

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    add-long/2addr v2, p1

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 155
    const-string v0, "MotionRecognitionSARAP"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "update PowerBackOffTime :: set the Time after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    return-void
.end method


# virtual methods
.method protected disableSARBackOff()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 159
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAutoPowerOffLock:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    if-ne v0, v2, :cond_2

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    .line 165
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAcquireWakelock:Z

    if-ne v0, v2, :cond_0

    .line 166
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->release()V

    .line 169
    :cond_0
    sget-boolean v0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->ENG_MODE:Z

    if-nez v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mProxlistener:Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAlarm:Landroid/app/AlarmManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 173
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    .line 174
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldcondition:I

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mbProximity:Z

    .line 178
    const-string v0, "MotionRecognitionSARAP"

    const-string v2, "update PowerBackOffTime :: mAlarm is canceled."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    monitor-exit v1

    .line 180
    :goto_0
    return-void

    .line 163
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected enableSARBackOff(J)V
    .locals 5
    .param p1, "pollingtime"    # J

    .prologue
    .line 137
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mAutoPowerOffLock:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    if-nez v0, :cond_1

    .line 139
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mStatus:Z

    .line 143
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mfilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 145
    sget-boolean v0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->ENG_MODE:Z

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mProxlistener:Lcom/samsung/android/motion/MotionRecognitionSARAP$ProxEventListener;

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mSm:Landroid/hardware/SensorManager;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v0, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 148
    :cond_0
    iput-wide p1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPollingTime:J

    .line 149
    iget-wide v2, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->mPollingTime:J

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->updatePollingTime(J)V

    .line 150
    monitor-exit v1

    .line 151
    :goto_0
    return-void

    .line 141
    :cond_1
    monitor-exit v1

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 207
    return-void
.end method

.method public abstract onChangeSARCondition(I)V
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x1

    .line 186
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    if-ne v1, v5, :cond_3

    .line 188
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v5

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v4, 0x2

    aget v3, v3, v4

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->checkTilt(FFF)I

    move-result v0

    .line 189
    .local v0, "status":I
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    if-eq v0, v1, :cond_0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 190
    :cond_0
    const-string v1, "MotionRecognitionSARAP"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " Status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    .line 192
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->setCondition()V

    .line 194
    :cond_1
    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->moldstatus:I

    .line 195
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    if-lt v1, v5, :cond_2

    .line 196
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->release()V

    .line 198
    :cond_2
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionSARAP;->cnt:I

    .line 200
    .end local v0    # "status":I
    :cond_3
    return-void
.end method
