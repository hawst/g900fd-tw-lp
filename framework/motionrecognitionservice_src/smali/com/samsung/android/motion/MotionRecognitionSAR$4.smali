.class Lcom/samsung/android/motion/MotionRecognitionSAR$4;
.super Ljava/lang/Object;
.source "MotionRecognitionSAR.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionSAR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 306
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x0

    .line 310
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    .line 312
    .local v1, "sensor":Landroid/hardware/Sensor;
    const-string v2, "ril.backoffstate"

    invoke-static {v2, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 313
    .local v0, "backoffstate":I
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ril.backoffstate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 316
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    # setter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal:S
    invoke-static {v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$302(Lcom/samsung/android/motion/MotionRecognitionSAR;S)S

    .line 318
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal:S
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$300(Lcom/samsung/android/motion/MotionRecognitionSAR;)S

    move-result v2

    if-lez v2, :cond_1

    .line 320
    and-int/lit8 v0, v0, -0x3

    .line 321
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PROX_SENSOR far,  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0xc8

    invoke-virtual {v3, v4, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 325
    :cond_1
    or-int/lit8 v0, v0, 0x2

    .line 326
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PROX_SENSOR close, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 328
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
