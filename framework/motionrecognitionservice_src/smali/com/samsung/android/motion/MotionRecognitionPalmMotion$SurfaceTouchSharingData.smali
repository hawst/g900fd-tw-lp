.class Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;
.super Ljava/lang/Object;
.source "MotionRecognitionPalmMotion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionPalmMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SurfaceTouchSharingData"
.end annotation


# instance fields
.field private Actiond:[I

.field private Major:[I

.field private Minor:[I

.field private Nd:[I

.field private SSsd:[[I

.field private Sed:[[F

.field private SmeanX:F

.field private SmeanY:F

.field private Spalm:F

.field private Ssd:[[I

.field private SsumEccen:F

.field public SsumMajor:F

.field private SsumMinor:F

.field private SsumX:F

.field private SsumY:F

.field private SvarX:F

.field private SvarY:F

.field private Sxd:[[I

.field private Syd:[[I

.field private isPrintDebugLog:Z

.field public mAction:I

.field private mIsRotated:Z

.field private mLastGetSettingVariableTime:D

.field private mLastPalmSweepTouchUpTime:D

.field private mMaxSize:F

.field private mMotionEvent:Landroid/view/MotionEvent;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field public mSettingPalmSweep:I

.field public mSettingPalmTouch:I

.field public mSettingSurfaceMotionEngine:I

.field private mTimeGapUPDown:D

.field private mTouchCnt:I

.field private preSumSize:F

.field private tCurrentTime:D

.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;


# direct methods
.method private constructor <init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v5, 0xa

    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 197
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    filled-new-array {v5, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    .line 199
    filled-new-array {v5, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    .line 200
    filled-new-array {v5, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Ssd:[[I

    .line 201
    filled-new-array {v5, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sed:[[F

    .line 202
    filled-new-array {v5, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SSsd:[[I

    .line 203
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Major:[I

    .line 204
    new-array v0, v5, [I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Minor:[I

    .line 205
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    .line 206
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    .line 208
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    .line 209
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    .line 211
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    .line 212
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mMaxSize:F

    .line 215
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumX:F

    .line 216
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumY:F

    .line 219
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F

    .line 220
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    .line 221
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    .line 222
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    .line 223
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanY:F

    .line 224
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->preSumSize:F

    .line 225
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Spalm:F

    .line 229
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z

    .line 232
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mSettingSurfaceMotionEngine:I

    .line 234
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mSettingPalmSweep:I

    .line 236
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mSettingPalmTouch:I

    .line 238
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I

    .line 239
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenHeight:I

    .line 241
    iput-wide v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->tCurrentTime:D

    .line 242
    iput-wide v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mLastPalmSweepTouchUpTime:D

    .line 244
    iput-wide v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTimeGapUPDown:D

    .line 245
    iput-wide v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mLastGetSettingVariableTime:D

    .line 248
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->isPrintDebugLog:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;Lcom/samsung/android/motion/MotionRecognitionPalmMotion$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion;
    .param p2, "x1"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$1;

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;-><init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Spalm:F

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mMotionEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$1900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    return v0
.end method


# virtual methods
.method public Initialize()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 400
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    const/16 v5, 0xa

    if-ge v2, v5, :cond_1

    .line 401
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v5, 0x3

    if-ge v1, v5, :cond_0

    .line 402
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v5, v5, v2

    aput v10, v5, v1

    .line 403
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v5, v5, v2

    aput v10, v5, v1

    .line 404
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Ssd:[[I

    aget-object v5, v5, v2

    aput v10, v5, v1

    .line 405
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sed:[[F

    aget-object v5, v5, v2

    aput v11, v5, v1

    .line 401
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 407
    :cond_0
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Major:[I

    aput v10, v5, v2

    .line 400
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 410
    .end local v1    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    iget-object v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    const/4 v9, 0x2

    aput v10, v8, v9

    aput v10, v6, v7

    aput v10, v5, v10

    .line 411
    iput v10, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    .line 412
    iput v11, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->preSumSize:F

    .line 413
    iput v11, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mMaxSize:F

    .line 415
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$500(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "window"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    .line 416
    .local v4, "wm":Landroid/view/WindowManager;
    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 417
    .local v0, "disp":Landroid/view/Display;
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 418
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 420
    iget v5, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I

    .line 421
    iget v5, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenHeight:I

    .line 422
    return-void
.end method

.method public PreUpdate(Landroid/view/MotionEvent;)V
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 252
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    iput v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mAction:I

    .line 253
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v8, 0x2

    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v10, 0x1

    aget v9, v9, v10

    aput v9, v7, v8

    .line 254
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v8, 0x1

    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v10, 0x0

    aget v9, v9, v10

    aput v9, v7, v8

    .line 255
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v8, 0x0

    iget v9, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mAction:I

    aput v9, v7, v8

    .line 258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    if-nez v7, :cond_2

    .line 260
    :try_start_0
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mContext:Landroid/content/Context;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$500(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Landroid/content/Context;

    move-result-object v7

    const-string v8, "window"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    .line 261
    .local v6, "wm":Landroid/view/WindowManager;
    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    .line 263
    .local v2, "disp":Landroid/view/Display;
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 264
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v2, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 266
    const/4 v1, 0x1

    .line 267
    .local v1, "bitMask":I
    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v7

    and-int/lit8 v7, v7, 0x1

    if-nez v7, :cond_1

    .line 268
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    .end local v1    # "bitMask":I
    .end local v2    # "disp":Landroid/view/Display;
    .end local v5    # "metrics":Landroid/util/DisplayMetrics;
    .end local v6    # "wm":Landroid/view/WindowManager;
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-double v8, v8

    iput-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->tCurrentTime:D

    .line 278
    iget-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->tCurrentTime:D

    iget-wide v10, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mLastPalmSweepTouchUpTime:D

    sub-double/2addr v8, v10

    iput-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTimeGapUPDown:D

    .line 279
    iget-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTimeGapUPDown:D

    const-wide/16 v10, 0x0

    cmpl-double v7, v8, v10

    if-ltz v7, :cond_0

    iget-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTimeGapUPDown:D

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    cmpg-double v7, v8, v10

    if-gez v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Actiond:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 281
    const/4 v7, 0x2

    iput v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mAction:I

    .line 282
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmSweepRecognizer:Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$600(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;

    move-result-object v7

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mPreSweepDown:Z
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->access$700(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 283
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmSweepRecognizer:Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$600(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;

    move-result-object v7

    const/4 v8, 0x1

    # setter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z
    invoke-static {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->access$802(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;Z)Z

    .line 284
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    const/4 v8, 0x1

    # setter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmMotionRecognizer:Z
    invoke-static {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$902(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;Z)Z

    .line 285
    const-string v7, "PalmMotion"

    const-string v8, "[Surface Touch] MotionEvent change to ACTION_MOVE"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_0
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 294
    .local v0, "N":I
    const/4 v7, 0x0

    iput v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    .line 295
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v0, :cond_3

    .line 296
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getTouchMajor(I)F

    move-result v8

    float-to-int v8, v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    .line 295
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 270
    .end local v0    # "N":I
    .end local v4    # "i":I
    .restart local v1    # "bitMask":I
    .restart local v2    # "disp":Landroid/view/Display;
    .restart local v5    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v6    # "wm":Landroid/view/WindowManager;
    :cond_1
    const/4 v7, 0x1

    :try_start_1
    iput-boolean v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 272
    .end local v1    # "bitMask":I
    .end local v2    # "disp":Landroid/view/Display;
    .end local v5    # "metrics":Landroid/util/DisplayMetrics;
    .end local v6    # "wm":Landroid/view/WindowManager;
    :catch_0
    move-exception v3

    .line 273
    .local v3, "e":Ljava/lang/Exception;
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z

    .line 274
    const-string v7, "PalmMotion"

    const-string v8, "[Surface Touch] Default Rotate = false"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_0

    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    long-to-double v8, v8

    iput-wide v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mLastPalmSweepTouchUpTime:D

    goto :goto_1

    .line 299
    .restart local v0    # "N":I
    .restart local v4    # "i":I
    :cond_3
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmMotionRecognizer:Z
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 301
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    const/high16 v8, 0x42c80000    # 100.0f

    cmpl-float v7, v7, v8

    if-gez v7, :cond_4

    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    const/high16 v8, 0x42480000    # 50.0f

    cmpl-float v7, v7, v8

    if-ltz v7, :cond_5

    iget-boolean v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z

    if-eqz v7, :cond_5

    .line 302
    :cond_4
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    const/4 v8, 0x1

    # setter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmMotionRecognizer:Z
    invoke-static {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$902(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;Z)Z

    .line 305
    :cond_5
    return-void
.end method

.method public Update(Landroid/view/MotionEvent;)V
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    .line 309
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mMotionEvent:Landroid/view/MotionEvent;

    .line 312
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumX:F

    .line 313
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumY:F

    .line 314
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F

    .line 315
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    .line 316
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanY:F

    .line 318
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    .line 319
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Spalm:F

    .line 321
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    .line 323
    .local v0, "N":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 324
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 325
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v3, v3, v2

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v5, v5, v2

    aget v5, v5, v1

    aput v5, v3, v4

    .line 326
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v3, v3, v2

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v5, v5, v2

    aget v5, v5, v1

    aput v5, v3, v4

    .line 327
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Ssd:[[I

    aget-object v3, v3, v2

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Ssd:[[I

    aget-object v5, v5, v2

    aget v5, v5, v1

    aput v5, v3, v4

    .line 328
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sed:[[F

    aget-object v3, v3, v2

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sed:[[F

    aget-object v5, v5, v2

    aget v5, v5, v1

    aput v5, v3, v4

    .line 329
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SSsd:[[I

    aget-object v3, v3, v2

    add-int/lit8 v4, v1, 0x1

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SSsd:[[I

    aget-object v5, v5, v2

    aget v5, v5, v1

    aput v5, v3, v4

    .line 324
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 323
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 333
    .end local v1    # "i":I
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, v0, :cond_3

    .line 334
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v3, v3, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    float-to-int v4, v4

    aput v4, v3, v7

    .line 335
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v3, v3, v1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    float-to-int v4, v4

    aput v4, v3, v7

    .line 337
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Minor:[I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getTouchMinor(I)F

    move-result v4

    float-to-int v4, v4

    aput v4, v3, v1

    .line 338
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPalm(I)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPalm(I)F

    move-result v3

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Spalm:F

    .line 333
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 341
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v8

    aput v4, v3, v9

    .line 342
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v7

    aput v4, v3, v8

    .line 343
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aput v0, v3, v7

    .line 345
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    .line 347
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    if-ne v3, v8, :cond_4

    .line 348
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v4, v4, v7

    aget v4, v4, v7

    aput v4, v3, v8

    .line 349
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v4, v4, v7

    aget v4, v4, v7

    aput v4, v3, v8

    .line 350
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v4, v4, v7

    aget v4, v4, v8

    aput v4, v3, v9

    .line 351
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v3, v3, v7

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v4, v4, v7

    aget v4, v4, v8

    aput v4, v3, v9

    .line 354
    :cond_4
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v3, v3, v7

    if-ge v1, v3, :cond_5

    .line 355
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumX:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v4, v4, v1

    aget v4, v4, v7

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumX:F

    .line 356
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumY:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v4, v4, v1

    aget v4, v4, v7

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumY:F

    .line 358
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Minor:[I

    aget v4, v4, v1

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    .line 354
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 360
    :cond_5
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumX:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v7

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    .line 361
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumY:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v7

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanY:F

    .line 362
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F

    .line 373
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    .line 374
    iput v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    .line 376
    const/4 v1, 0x0

    :goto_4
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v3, v3, v7

    if-ge v1, v3, :cond_6

    .line 377
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v4, v4, v1

    aget v4, v4, v7

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    .line 378
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v4, v4, v1

    aget v4, v4, v7

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanY:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanY:F

    sub-float/2addr v5, v6

    mul-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    .line 376
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 381
    :cond_6
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v7

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    .line 382
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I

    aget v4, v4, v7

    int-to-float v4, v4

    div-float/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    .line 384
    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->isPrintDebugLog:Z

    if-nez v3, :cond_7

    const/4 v3, 0x5

    if-lt v0, v3, :cond_7

    .line 385
    iput-boolean v8, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->isPrintDebugLog:Z

    .line 387
    :cond_7
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->DEBUG:Z
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$1000()Z

    move-result v3

    if-eqz v3, :cond_8

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    const/16 v4, 0x32

    if-ge v3, v4, :cond_8

    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->isPrintDebugLog:Z

    if-eqz v3, :cond_8

    .line 388
    const/4 v1, 0x0

    :goto_5
    if-ge v1, v0, :cond_8

    .line 389
    const-string v3, "PalmMotion"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[ST] cnt:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " N:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " action:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " x:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Sxd:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " y:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Syd:[[I

    aget-object v5, v5, v1

    aget v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SsumEccen:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " pa:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPalm(I)F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sumMajor:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sumMinor:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMinor:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SvarX:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " SvarY:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarY:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_5

    .line 395
    :cond_8
    return-void
.end method
