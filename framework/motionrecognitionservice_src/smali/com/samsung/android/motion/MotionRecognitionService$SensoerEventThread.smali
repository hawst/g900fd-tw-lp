.class Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;
.super Ljava/lang/Object;
.source "MotionRecognitionService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SensoerEventThread"
.end annotation


# instance fields
.field eventthread:Ljava/lang/Thread;

.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionService;


# direct methods
.method private constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0

    .prologue
    .line 515
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;Lcom/samsung/android/motion/MotionRecognitionService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p2, "x1"    # Lcom/samsung/android/motion/MotionRecognitionService$1;

    .prologue
    .line 515
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 542
    const-string v1, "MotionRecognitionService"

    const-string v2, "Thread starting."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    const/4 v0, -0x1

    .line 544
    .local v0, "ret":I
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionService;->mListener:Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->initMotionService(Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;)I
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionService;->access$2000(Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;)I

    .line 545
    :goto_0
    if-gez v0, :cond_0

    .line 546
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "try start thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->runMoitonService()I
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->access$2100()I

    move-result v0

    goto :goto_0

    .line 549
    :cond_0
    const-string v1, "MotionRecognitionService"

    const-string v2, "Thread stopping."

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    .line 551
    return-void
.end method

.method public start()V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    if-nez v0, :cond_0

    .line 519
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    .line 520
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 525
    :goto_0
    return-void

    .line 522
    :cond_0
    const-string v0, "MotionRecognitionService"

    const-string v1, "can not start thread."

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()V
    .locals 6

    .prologue
    .line 527
    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->deinitMotionService()I
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1900()I

    .line 528
    const/4 v0, 0x5

    .line 529
    .local v0, "cnt":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    if-eqz v3, :cond_1

    add-int/lit8 v1, v0, -0x1

    .end local v0    # "cnt":I
    .local v1, "cnt":I
    if-nez v0, :cond_0

    .line 531
    const-wide/16 v4, 0xa

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 535
    .end local v1    # "cnt":I
    .restart local v0    # "cnt":I
    goto :goto_0

    .line 532
    .end local v0    # "cnt":I
    .restart local v1    # "cnt":I
    :catch_0
    move-exception v2

    .line 534
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v0, v1

    .line 535
    .end local v1    # "cnt":I
    .restart local v0    # "cnt":I
    goto :goto_0

    .end local v0    # "cnt":I
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .restart local v1    # "cnt":I
    :cond_0
    move v0, v1

    .line 537
    .end local v1    # "cnt":I
    .restart local v0    # "cnt":I
    :cond_1
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Thread stopped."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->eventthread:Ljava/lang/Thread;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    return-void
.end method
