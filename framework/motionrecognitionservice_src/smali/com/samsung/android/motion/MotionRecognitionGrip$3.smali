.class Lcom/samsung/android/motion/MotionRecognitionGrip$3;
.super Ljava/lang/Object;
.source "MotionRecognitionGrip.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionGrip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionGrip;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionGrip;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 333
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v1, 0x0

    .line 337
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    .line 338
    .local v0, "sensor":Landroid/hardware/Sensor;
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_1

    .line 339
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionGrip;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v1

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    # invokes: Lcom/samsung/android/motion/MotionRecognitionGrip;->setProximity_flat(Z)V
    invoke-static {v2, v1}, Lcom/samsung/android/motion/MotionRecognitionGrip;->access$300(Lcom/samsung/android/motion/MotionRecognitionGrip;Z)V

    .line 340
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionGrip;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionGrip;->setPowerState_flat()Z
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionGrip;->access$400(Lcom/samsung/android/motion/MotionRecognitionGrip;)Z

    .line 342
    :cond_1
    return-void
.end method
