.class Lcom/samsung/android/motion/MotionRecognitionSAR$2;
.super Lcom/samsung/android/motion/MotionRecognitionSARAP;
.source "MotionRecognitionSAR.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/motion/MotionRecognitionSAR;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionSAR;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-direct {p0, p2}, Lcom/samsung/android/motion/MotionRecognitionSARAP;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onChangeSARCondition(I)V
    .locals 7
    .param p1, "condition"    # I

    .prologue
    const/4 v6, 0x0

    .line 139
    const-string v3, "ril.backoffstate"

    invoke-static {v3, v6}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 140
    .local v0, "backoffstate":I
    move v1, v0

    .line 141
    .local v1, "currentstate":I
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "current backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const/4 v2, -0x1

    .line 144
    .local v2, "status":I
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->isCoverEquipment()Z

    move-result v3

    if-nez v3, :cond_5

    .line 145
    if-nez p1, :cond_2

    .line 146
    or-int/lit8 v0, v0, 0x40

    .line 147
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FLAT_UP, ril.backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    const/16 v2, 0x64

    .line 163
    :cond_0
    :goto_0
    if-eq v1, v0, :cond_1

    .line 164
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 165
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v2, v0, v6}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 170
    :cond_1
    :goto_1
    return-void

    .line 149
    :cond_2
    const/4 v3, 0x1

    if-ne p1, v3, :cond_3

    .line 150
    or-int/lit8 v0, v0, 0x40

    .line 151
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "FLAT_DOWN, ril.backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    const/16 v2, 0x64

    goto :goto_0

    .line 153
    :cond_3
    const/4 v3, -0x1

    if-ne p1, v3, :cond_4

    .line 154
    and-int/lit8 v0, v0, -0x41

    .line 155
    const-string v3, "MotionRecognitionService"

    const-string v4, "NO_FLAT, POWER_DEFAULT "

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const/16 v2, 0xc8

    goto :goto_0

    .line 157
    :cond_4
    const/4 v3, 0x2

    if-ne p1, v3, :cond_0

    .line 158
    or-int/lit8 v0, v0, 0x40

    .line 159
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "BOTTOM_UP, ril.backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/16 v2, 0x64

    goto :goto_0

    .line 168
    :cond_5
    const-string v3, "MotionRecognitionService"

    const-string v4, " isCover == true. Skip power backoff  "

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
