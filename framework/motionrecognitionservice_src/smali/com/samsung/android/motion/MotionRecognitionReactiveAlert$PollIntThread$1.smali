.class Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;
.super Landroid/os/Handler;
.source "MotionRecognitionReactiveAlert.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v8, 0x31

    const/16 v7, 0x30

    const/16 v6, 0x2d

    const/16 v5, 0x1e

    const/4 v4, 0x1

    .line 86
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v5, :cond_6

    .line 87
    const/4 v0, 0x0

    .line 90
    .local v0, "ret_accint":I
    const-string v1, "MotionRecognitionService"

    const-string v2, "mReactiveAlert_Wakelock++  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$000(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 93
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mLockACCInt:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$100(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 97
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->readACCInt()I
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$200(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)I

    move-result v0

    .line 98
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    const-string v1, "MotionRecognitionService"

    const-string v2, "mReactiveAlert_Wakelock--  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$000(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 101
    if-ne v0, v8, :cond_1

    .line 103
    const-string v1, "MotionRecognitionService"

    const-string v2, "skip poll motion_int  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->disableAccINT()V

    .line 145
    .end local v0    # "ret_accint":I
    :cond_0
    :goto_1
    return-void

    .line 98
    .restart local v0    # "ret_accint":I
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    .line 108
    :cond_1
    const-string v1, "MotionRecognitionService"

    const-string v2, "poll_accint++  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_2
    const-wide/16 v2, 0x64

    :try_start_3
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 114
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mLockACCInt:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$100(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 115
    :try_start_4
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->readACCInt()I
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$200(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)I

    move-result v0

    .line 116
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 118
    if-ne v0, v7, :cond_3

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mEnableAccINT:Z
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$300(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Z

    move-result v1

    if-eq v1, v4, :cond_2

    .line 120
    :cond_3
    const-string v1, "MotionRecognitionService"

    const-string v2, "poll_accint--  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const-string v1, "MotionRecognitionService"

    const-string v2, "mReactiveAlert_Wakelock++  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$000(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 123
    if-ne v0, v8, :cond_5

    .line 126
    const-string v1, "MotionRecognitionService"

    const-string v2, " send reactive alert "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 128
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->disableAccINT()V

    .line 130
    const-wide/16 v2, 0x3e8

    :try_start_5
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    .line 138
    :cond_4
    :goto_3
    const-string v1, "MotionRecognitionService"

    const-string v2, "mReactiveAlert_Wakelock--  "

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mReactiveAlert_Wakelock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$000(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_1

    .line 116
    :catchall_1
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v1

    .line 134
    :cond_5
    if-eq v0, v7, :cond_4

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->mEnableAccINT:Z
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->access$300(Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;)Z

    move-result v1

    if-ne v1, v4, :cond_4

    .line 136
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->this$0:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->disableAccINT()V

    goto :goto_3

    .line 142
    .end local v0    # "ret_accint":I
    :cond_6
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v6, :cond_0

    .line 143
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread$1;->this$1:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;

    iget-object v1, v1, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert$PollIntThread;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 94
    .restart local v0    # "ret_accint":I
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 112
    :catch_1
    move-exception v1

    goto/16 :goto_2

    .line 131
    :catch_2
    move-exception v1

    goto :goto_3
.end method
