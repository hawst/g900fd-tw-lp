.class public Lcom/samsung/android/motion/MotionRecognitionSAR;
.super Ljava/lang/Object;
.source "MotionRecognitionSAR.java"


# static fields
.field public static final MSG_BACK_OFF:I = 0x64

.field public static final MSG_DEFAULT:I = 0xc8

.field private static final SAR_DEVICE_FLAT:I = 0x8

.field private static final SAR_DEVICE_FLAT_AP:I = 0x40

.field private static final SAR_DEVICE_GRIP:I = 0x1

.field private static final SAR_DEVICE_PORTRAIT:I = 0x80

.field private static final SAR_DEVICE_PROX:I = 0x2

.field private static final SAR_DEVICE_TURNOVER:I = 0x10

.field private static final TAG:Ljava/lang/String; = "MotionRecognitionService"

.field private static final TRANSMIT_POWER_DEFAULT:I


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrPowerState_flat:Z

.field private mGripEnabled:Z

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsFlat:Z

.field private mIsPowerConnected:Z

.field mLockGripMotion:Ljava/lang/Object;

.field private mPhoneService:Lcom/android/internal/telephony/ITelephony;

.field private mPostGripVal:S

.field private mPostProxVal:S

.field private mPostProxVal_Flat:Z

.field private mProxEnabled:Z

.field private mProxEnabled_flat:Z

.field private mSContextEnabled_flat:Z

.field private mSContextEnabled_portait:Z

.field private mSContextEnabled_turnover:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field private mSarAPBackoff:Lcom/samsung/android/motion/MotionRecognitionSARAP;

.field private mWindowManager:Landroid/view/IWindowManager;

.field private final mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

.field private final mySContexPortraitMotionListener:Landroid/hardware/scontext/SContextListener;

.field private final mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

.field private final sensorGripListener:Landroid/hardware/SensorEventListener;

.field private sensorManager:Landroid/hardware/SensorManager;

.field private final sensorProxListener:Landroid/hardware/SensorEventListener;

.field private final sensorProxListener_Flat:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    .line 69
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mLockGripMotion:Ljava/lang/Object;

    .line 77
    iput-short v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostGripVal:S

    .line 78
    iput-short v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal:S

    .line 79
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal_Flat:Z

    .line 80
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mCurrPowerState_flat:Z

    .line 81
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsFlat:Z

    .line 230
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$3;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorGripListener:Landroid/hardware/SensorEventListener;

    .line 302
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$4;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener:Landroid/hardware/SensorEventListener;

    .line 480
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$5;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    .line 498
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$6;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    .line 606
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$7;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    .line 694
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionSAR$8;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexPortraitMotionListener:Landroid/hardware/scontext/SContextListener;

    .line 90
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    .line 91
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    .line 92
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    .line 93
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled:Z

    .line 94
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled_flat:Z

    .line 95
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_flat:Z

    .line 96
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_turnover:Z

    .line 97
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_portait:Z

    .line 98
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mCurrPowerState_flat:Z

    .line 100
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "sensorThread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandlerThread:Landroid/os/HandlerThread;

    .line 101
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 102
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$1;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/motion/MotionRecognitionSAR$1;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    .line 133
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSAR$2;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/motion/MotionRecognitionSAR$2;-><init>(Lcom/samsung/android/motion/MotionRecognitionSAR;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSarAPBackoff:Lcom/samsung/android/motion/MotionRecognitionSARAP;

    .line 172
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/motion/MotionRecognitionSAR;)Lcom/android/internal/telephony/ITelephony;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/motion/MotionRecognitionSAR;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_TurnOver(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/android/motion/MotionRecognitionSAR;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_Portait(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/motion/MotionRecognitionSAR;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-short v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostGripVal:S

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/motion/MotionRecognitionSAR;S)S
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # S

    .prologue
    .line 43
    iput-short p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostGripVal:S

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/motion/MotionRecognitionSAR;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-short v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal:S

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/android/motion/MotionRecognitionSAR;S)S
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # S

    .prologue
    .line 43
    iput-short p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal:S

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/motion/MotionRecognitionSAR;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setProximity_flat(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/motion/MotionRecognitionSAR;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_flat()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/android/motion/MotionRecognitionSAR;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setFlat(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/motion/MotionRecognitionSAR;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled_flat:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/motion/MotionRecognitionSAR;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled_flat:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/hardware/SensorEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSAR;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method private isSpeakerOn()Z
    .locals 2

    .prologue
    .line 575
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 576
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mAudioManager:Landroid/media/AudioManager;

    .line 577
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    .line 578
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    .line 579
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFlat(Z)V
    .locals 3
    .param p1, "flat"    # Z

    .prologue
    .line 476
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsFlat:Z

    .line 477
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFlat : flat motion event = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsFlat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    return-void
.end method

.method private setPowerState_Portait(Z)V
    .locals 5
    .param p1, "action"    # Z

    .prologue
    const/4 v4, 0x0

    .line 675
    const-string v1, "ril.backoffstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 676
    .local v0, "backoffstate":I
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    if-nez p1, :cond_1

    .line 679
    and-int/lit16 v0, v0, -0x81

    .line 680
    const-string v1, "MotionRecognitionService"

    const-string v2, "Portait, POWER_DEFAULT "

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 682
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 690
    :cond_0
    :goto_0
    return-void

    .line 684
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->isCoverEquipment()Z

    move-result v1

    if-nez v1, :cond_0

    .line 685
    or-int/lit16 v0, v0, 0x80

    .line 686
    const-string v1, "MotionRecognitionService"

    const-string v2, "Portait, POWER_BACKOFF"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 688
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private setPowerState_TurnOver(Z)V
    .locals 5
    .param p1, "action"    # Z

    .prologue
    const/4 v4, 0x0

    .line 583
    const-string v1, "ril.backoffstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 584
    .local v0, "backoffstate":I
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    if-nez p1, :cond_1

    .line 587
    and-int/lit8 v0, v0, -0x11

    .line 588
    const-string v1, "MotionRecognitionService"

    const-string v2, "TurnOver, POWER_DEFAULT "

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 590
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 602
    :cond_0
    :goto_0
    return-void

    .line 592
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->isCoverEquipment()Z

    move-result v1

    if-nez v1, :cond_0

    .line 593
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->isSpeakerOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 594
    const-string v1, "MotionRecognitionService"

    const-string v2, "skip : speaker on"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 597
    :cond_2
    or-int/lit8 v0, v0, 0x10

    .line 598
    const-string v1, "MotionRecognitionService"

    const-string v2, "TurnOver, POWER_BACKOFF"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 600
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private setPowerState_flat()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 442
    const-string v3, "ril.backoffstate"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 443
    .local v0, "backoffstate":I
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 445
    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal_Flat:Z

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsFlat:Z

    and-int/2addr v3, v4

    if-eqz v3, :cond_2

    const/4 v1, 0x1

    .line 446
    .local v1, "status":Z
    :goto_0
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " prox : flat  =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal_Flat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsFlat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " last status =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mCurrPowerState_flat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mCurrPowerState_flat:Z

    if-eq v3, v1, :cond_4

    .line 450
    if-nez v1, :cond_3

    .line 451
    and-int/lit8 v0, v0, -0x9

    .line 452
    const-string v3, "MotionRecognitionService"

    const-string v4, "Flat, POWER_DEFAULT "

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 453
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 454
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v5, 0xc8

    invoke-virtual {v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 466
    :cond_0
    :goto_1
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mCurrPowerState_flat:Z

    .line 467
    :cond_1
    return v1

    .end local v1    # "status":Z
    :cond_2
    move v1, v2

    .line 445
    goto :goto_0

    .line 456
    .restart local v1    # "status":Z
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->isCoverEquipment()Z

    move-result v3

    if-nez v3, :cond_1

    .line 458
    or-int/lit8 v0, v0, 0x8

    .line 459
    const-string v3, "MotionRecognitionService"

    const-string v4, "Flat, POWER_BACKOFF"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 460
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_0

    .line 461
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v5, 0x64

    invoke-virtual {v4, v5, v0, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 464
    :cond_4
    const-string v2, "MotionRecognitionService"

    const-string v3, "PowerState is not changed"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private setProximity_flat(Z)V
    .locals 3
    .param p1, "proxival"    # Z

    .prologue
    .line 471
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal_Flat:Z

    .line 472
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProximity_flat : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostProxVal_Flat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    return-void
.end method


# virtual methods
.method public disableFlatMotion()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 360
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_flat:Z

    if-ne v0, v4, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 362
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_flat:Z

    .line 365
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled_flat:Z

    if-ne v0, v4, :cond_1

    .line 366
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 367
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled_flat:Z

    .line 370
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setProximity_flat(Z)V

    .line 371
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setFlat(Z)V

    .line 373
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_2

    .line 374
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_flat()Z

    .line 378
    :goto_0
    return v3

    .line 376
    :cond_2
    const-string v0, "MotionRecognitionService"

    const-string v1, "mPhoneService is null  "

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected disableFlatMotionAP()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 394
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSarAPBackoff:Lcom/samsung/android/motion/MotionRecognitionSARAP;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->disableSARBackOff()V

    .line 396
    const-string v1, "ril.backoffstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 397
    .local v0, "backoffstate":I
    and-int/lit8 v0, v0, -0x41

    .line 398
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableFlatMotionAP, backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 399
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 400
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 401
    :cond_0
    return v4
.end method

.method public disableGripSensor()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 190
    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 191
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorGripListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 192
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    .line 194
    const-string v1, "ril.backoffstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 195
    .local v0, "backoffstate":I
    and-int/lit8 v0, v0, -0x2

    .line 196
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableGripSensor, backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 200
    .end local v0    # "backoffstate":I
    :cond_0
    return v4
.end method

.method public disablePortraitMotion()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 662
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_portait:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 663
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexPortraitMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 664
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_portait:Z

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_1

    .line 667
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_Portait(Z)V

    .line 671
    :goto_0
    return v3

    .line 669
    :cond_1
    const-string v0, "MotionRecognitionService"

    const-string v1, "mPhoneService is null  "

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public disableProxSensor()I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 288
    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 289
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v1, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 290
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled:Z

    .line 292
    const-string v1, "ril.backoffstate"

    invoke-static {v1, v4}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 293
    .local v0, "backoffstate":I
    and-int/lit8 v0, v0, -0x3

    .line 294
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "disableProxSensor, backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 296
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v0, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 298
    .end local v0    # "backoffstate":I
    :cond_0
    return v4
.end method

.method public disableTurnOverMotion()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 562
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_turnover:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 563
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 564
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_turnover:Z

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_1

    .line 567
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerState_TurnOver(Z)V

    .line 571
    :goto_0
    return v3

    .line 569
    :cond_1
    const-string v0, "MotionRecognitionService"

    const-string v1, "mPhoneService is null  "

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public enableFlatMotion()I
    .locals 4

    .prologue
    .line 338
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 339
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 341
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_1

    .line 342
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    .line 344
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 345
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 346
    .local v0, "tmpPM":Landroid/content/pm/PackageManager;
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 347
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v1, :cond_2

    .line 348
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 350
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_flat:Z

    if-nez v1, :cond_3

    .line 351
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 352
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_flat:Z

    .line 356
    .end local v0    # "tmpPM":Landroid/content/pm/PackageManager;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method protected enableFlatMotionAP()I
    .locals 4

    .prologue
    .line 383
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 384
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 386
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v0, :cond_1

    .line 387
    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    .line 389
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSarAPBackoff:Lcom/samsung/android/motion/MotionRecognitionSARAP;

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSARAP;->enableSARBackOff(J)V

    .line 390
    const/4 v0, 0x0

    return v0
.end method

.method public enableGripSensor()I
    .locals 4

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 178
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 180
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    if-nez v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorGripListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    const v3, 0x10018

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    .line 186
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public enablePortraitMotion()I
    .locals 4

    .prologue
    .line 639
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 640
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 642
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_1

    .line 643
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    .line 645
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 646
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 647
    .local v0, "tmpPM":Landroid/content/pm/PackageManager;
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 648
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v1, :cond_2

    .line 649
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 651
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_portait:Z

    if-nez v1, :cond_3

    .line 652
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexPortraitMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 653
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_portait:Z

    .line 657
    .end local v0    # "tmpPM":Landroid/content/pm/PackageManager;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public enableProxSensor()I
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 276
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 278
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled:Z

    if-nez v0, :cond_1

    .line 279
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorProxListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->sensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mProxEnabled:Z

    .line 284
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public enableTurnOverMotion()I
    .locals 4

    .prologue
    .line 540
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 541
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 543
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_1

    .line 544
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mWindowManager:Landroid/view/IWindowManager;

    .line 546
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 547
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 548
    .local v0, "tmpPM":Landroid/content/pm/PackageManager;
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 549
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v1, :cond_2

    .line 550
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mContext:Landroid/content/Context;

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 552
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_turnover:Z

    if-nez v1, :cond_3

    .line 553
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 554
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mSContextEnabled_turnover:Z

    .line 558
    .end local v0    # "tmpPM":Landroid/content/pm/PackageManager;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public getPowerConnected()Z
    .locals 3

    .prologue
    .line 226
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getPowerConnected  = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    return v0
.end method

.method public isCoverEquipment()Z
    .locals 9

    .prologue
    .line 405
    const/4 v5, 0x0

    .line 406
    .local v5, "isCover":Z
    const/4 v3, 0x0

    .line 407
    .local v3, "fis":Ljava/io/FileInputStream;
    const/16 v0, 0x32

    .line 409
    .local v0, "data":I
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mLockGripMotion:Ljava/lang/Object;

    monitor-enter v7

    .line 412
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v6, "/sys/bus/w1/devices/w1_bus_master1/w1_master_check_id"

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    move-object v3, v4

    .line 417
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_0
    if-nez v3, :cond_1

    .line 418
    :try_start_1
    const-string v6, "MotionRecognitionService"

    const-string v8, "Output file is null!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 435
    :goto_1
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 436
    const-string v6, "MotionRecognitionService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " cover status =  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    const/16 v6, 0x32

    if-ne v0, v6, :cond_2

    const/4 v5, 0x0

    .line 438
    :goto_2
    return v5

    .line 413
    :catch_0
    move-exception v1

    .line 414
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string v6, "MotionRecognitionService"

    const-string v8, "File not found!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 424
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 425
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 426
    if-eqz v3, :cond_0

    .line 428
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 433
    :cond_0
    :goto_3
    :try_start_6
    const-string v6, "MotionRecognitionService"

    const-string v8, "File read fail!!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 435
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v6

    .line 420
    :cond_1
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->read()I

    move-result v0

    .line 422
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 429
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 430
    .local v2, "err":Ljava/io/IOException;
    :try_start_8
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 437
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "err":Ljava/io/IOException;
    :cond_2
    const/4 v5, 0x1

    goto :goto_2
.end method

.method public setPowerConnected(Z)V
    .locals 7
    .param p1, "connect"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 205
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    .line 206
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPowerConnected  = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mGripEnabled:Z

    if-ne v2, v6, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mIsPowerConnected:Z

    if-ne v2, v6, :cond_0

    .line 209
    const-string v2, "ril.backoffstate"

    invoke-static {v2, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 210
    .local v0, "backoffstate":I
    and-int/lit8 v1, v0, -0x2

    .line 211
    .local v1, "state":I
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "backoffstate  = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , state ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    if-eq v1, v0, :cond_1

    .line 214
    and-int/lit8 v0, v0, -0x2

    .line 215
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GRIP_SENSOR far,  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 217
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;

    const/16 v4, 0xc8

    invoke-virtual {v3, v4, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 222
    .end local v0    # "backoffstate":I
    .end local v1    # "state":I
    :cond_0
    :goto_0
    return-void

    .line 219
    .restart local v0    # "backoffstate":I
    .restart local v1    # "state":I
    :cond_1
    const-string v2, "MotionRecognitionService"

    const-string v3, "skip setTransmitPower. "

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
