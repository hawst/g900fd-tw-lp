.class final Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;
.super Landroid/database/ContentObserver;
.source "MotionRecognitionSettings.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MotionSettingsObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;


# direct methods
.method public constructor <init>(Lcom/samsung/android/motion/MotionRecognitionSettings;Landroid/os/Handler;)V
    .locals 1
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;->this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;

    .line 222
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 223
    const/4 v0, 0x0

    # setter for: Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I
    invoke-static {p1, v0}, Lcom/samsung/android/motion/MotionRecognitionSettings;->access$002(Lcom/samsung/android/motion/MotionRecognitionSettings;I)I

    .line 224
    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 234
    .local v0, "motion_sensors":I
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;->this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionSettings;->updateCurrentSettings()V

    .line 237
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;->this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v1}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getUsedSensorForEvents()I

    move-result v0

    .line 239
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;->this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;->this$0:Lcom/samsung/android/motion/MotionRecognitionSettings;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSettings;->access$000(Lcom/samsung/android/motion/MotionRecognitionSettings;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/motion/MotionRecognitionSettings;->onChangeMotionSettings(II)V

    .line 241
    return-void
.end method
