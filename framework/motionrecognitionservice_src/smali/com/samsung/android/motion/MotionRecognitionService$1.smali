.class Lcom/samsung/android/motion/MotionRecognitionService$1;
.super Landroid/content/BroadcastReceiver;
.source "MotionRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionService;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public callSARDeviceID(ILjava/lang/String;)V
    .locals 6
    .param p1, "deviceID"    # I
    .param p2, "callAction"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 335
    const/4 v0, -0x1

    .line 336
    .local v0, "ret":I
    const/4 v2, 0x0

    .local v2, "type":I
    :goto_0
    const/16 v3, 0x8

    if-ge v2, v3, :cond_7

    .line 337
    shl-int v1, v5, v2

    .line 338
    .local v1, "sar_type":I
    and-int v3, v1, p1

    if-eqz v3, :cond_0

    .line 340
    packed-switch v2, :pswitch_data_0

    .line 372
    :pswitch_0
    const-string v3, "MotionRecognitionService"

    const-string v4, "DEFAULT  "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 342
    :pswitch_1
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_GRIP "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 344
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enableGripSensor()I

    .line 345
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v3, v5}, Lcom/samsung/android/motion/MotionRecognitionService;->access$202(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    goto :goto_1

    .line 347
    :cond_1
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disableGripSensor()I

    .line 348
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/4 v4, 0x0

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v3, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->access$202(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    goto :goto_1

    .line 352
    :pswitch_2
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_PROX "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enableProxSensor()I

    move-result v0

    .line 354
    :goto_2
    goto :goto_1

    .line 353
    :cond_2
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disableProxSensor()I

    move-result v0

    goto :goto_2

    .line 356
    :pswitch_3
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_FLAT "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enableFlatMotion()I

    move-result v0

    .line 358
    :goto_3
    goto :goto_1

    .line 357
    :cond_3
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disableFlatMotion()I

    move-result v0

    goto :goto_3

    .line 360
    :pswitch_4
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_ACCFLAT "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_4

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enableFlatMotionAP()I

    move-result v0

    .line 362
    :goto_4
    goto/16 :goto_1

    .line 361
    :cond_4
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disableFlatMotionAP()I

    move-result v0

    goto :goto_4

    .line 364
    :pswitch_5
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_TURNOVER "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_5

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enableTurnOverMotion()I

    move-result v0

    .line 366
    :goto_5
    goto/16 :goto_1

    .line 365
    :cond_5
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disableTurnOverMotion()I

    move-result v0

    goto :goto_5

    .line 368
    :pswitch_6
    const-string v3, "MotionRecognitionService"

    const-string v4, "SAR_PORTRAIT "

    invoke-static {v3, v4}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    const-string v3, "on"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-ne v3, v5, :cond_6

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->enablePortraitMotion()I

    move-result v0

    .line 370
    :goto_6
    goto/16 :goto_1

    .line 369
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->disablePortraitMotion()I

    move-result v0

    goto :goto_6

    .line 377
    .end local v1    # "sar_type":I
    :cond_7
    return-void

    .line 340
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_6
    .end packed-switch
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, -0x1

    const/4 v9, 0x0

    const/4 v10, 0x1

    .line 230
    const/4 v6, -0x1

    .line 231
    .local v6, "ret":I
    const-string v7, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 232
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_SCREEN_ON"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$000(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    move-result-object v7

    const/16 v8, 0x56

    invoke-virtual {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;->sendEmptyMessage(I)Z

    .line 332
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    const-string v7, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 235
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_SCREEN_OFF"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$000(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    move-result-object v7

    const/16 v8, 0x57

    invoke-virtual {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 237
    :cond_2
    const-string v7, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 238
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_BOOT_COMPLETED"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iput-boolean v10, v7, Lcom/samsung/android/motion/MotionRecognitionService;->mBootCompeleted:Z

    .line 240
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iput-boolean v10, v7, Lcom/samsung/android/motion/MotionRecognitionService;->mScreenOn:Z

    goto :goto_0

    .line 242
    :cond_3
    const-string v7, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 243
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_BATTERY_CHANGED"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    const-string v7, "plugged"

    invoke-virtual {p2, v7, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 245
    .local v5, "mPlugType":I
    if-lez v5, :cond_4

    .line 246
    const-string v7, "MotionRecognitionService"

    const-string v8, "Plugged"

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerConnected(Z)V

    goto :goto_0

    .line 249
    :cond_4
    const-string v7, "MotionRecognitionService"

    const-string v8, "Unplugged"

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 252
    .end local v5    # "mPlugType":I
    :cond_5
    const-string v7, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 253
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_POWER_CONNECTED"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$200(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 255
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v7

    invoke-virtual {v7, v10}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerConnected(Z)V

    goto/16 :goto_0

    .line 257
    :cond_6
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mGripSensorEnabled= "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v9}, Lcom/samsung/android/motion/MotionRecognitionService;->access$200(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 259
    :cond_7
    const-string v7, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 260
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_POWER_DISCONNECTED"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$200(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 262
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/samsung/android/motion/MotionRecognitionSAR;->setPowerConnected(Z)V

    goto/16 :goto_0

    .line 264
    :cond_8
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mGripSensorEnabled = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z
    invoke-static {v9}, Lcom/samsung/android/motion/MotionRecognitionService;->access$200(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 267
    :cond_9
    const-string v7, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    const-string v7, "android.intent.action.BATTERY_LOW"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 270
    :cond_a
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mPowerTime:J
    invoke-static {v7, v8, v9}, Lcom/samsung/android/motion/MotionRecognitionService;->access$302(Lcom/samsung/android/motion/MotionRecognitionService;J)J

    goto/16 :goto_0

    .line 271
    :cond_b
    const-string v7, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 272
    const-string v7, "isEnable"

    invoke-virtual {p2, v7, v9}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 273
    .local v4, "isEnable":Z
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive : ACTION_MOTIONS_SETTINGS_CHANGED - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    if-eqz v4, :cond_d

    .line 276
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$400(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSettings;

    move-result-object v7

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$400(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionSettings;->updateCurrentSettings()V

    .line 277
    :cond_c
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$500(Lcom/samsung/android/motion/MotionRecognitionService;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_0

    .line 278
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->initializeMotionEngine()V
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$600(Lcom/samsung/android/motion/MotionRecognitionService;)V

    .line 279
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$700(Lcom/samsung/android/motion/MotionRecognitionService;)I

    move-result v7

    if-lez v7, :cond_0

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$800(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 280
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v9, 0x132

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z
    invoke-static {v8, v9}, Lcom/samsung/android/motion/MotionRecognitionService;->access$900(Lcom/samsung/android/motion/MotionRecognitionService;I)Z

    move-result v8

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z
    invoke-static {v7, v8}, Lcom/samsung/android/motion/MotionRecognitionService;->access$802(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    goto/16 :goto_0

    .line 283
    :cond_d
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mregisteredEvents_open:I
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1000(Lcom/samsung/android/motion/MotionRecognitionService;)I

    move-result v7

    if-nez v7, :cond_0

    .line 284
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->finalizeMotionEngine()V
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1100(Lcom/samsung/android/motion/MotionRecognitionService;)V

    goto/16 :goto_0

    .line 286
    .end local v4    # "isEnable":Z
    :cond_e
    const-string v7, "android.intent.action.USER_PRESENT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 287
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive : ACTION_USER_PRESENT  :: UNLOCK SCREEN"

    invoke-static {v7, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 289
    :cond_f
    const-string v7, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    .line 290
    const-string v7, "MotionRecognitionService"

    const-string v8, "  mReceiver.onReceive :VOLUME_CHANGED "

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    new-array v1, v10, [I

    .local v1, "data":[I
    aput v10, v1, v9

    goto/16 :goto_0

    .line 294
    .end local v1    # "data":[I
    :cond_10
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.GRIPSENSOR_CP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 295
    const-string v7, "cmd"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    .local v0, "callAction":Ljava/lang/String;
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive :CALL_ACTION_CHANGED : GRIPSENSOR_CP, callAction = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    const-string v7, "on"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v10, :cond_11

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->enableGripSensor()I

    move-result v6

    .line 298
    :goto_1
    goto/16 :goto_0

    .line 297
    :cond_11
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->disableGripSensor()I

    move-result v6

    goto :goto_1

    .line 299
    .end local v0    # "callAction":Ljava/lang/String;
    :cond_12
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.FLATMOTION_CP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_14

    .line 300
    const-string v7, "cmd"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 301
    .restart local v0    # "callAction":Ljava/lang/String;
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive :CALL_ACTION_CHANGED : FLATMOTION_CP, callAction = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    const-string v7, "on"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v10, :cond_13

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->enableFlatMotion()I

    move-result v6

    .line 303
    :goto_2
    goto/16 :goto_0

    .line 302
    :cond_13
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->disableFlatMotion()I

    move-result v6

    goto :goto_2

    .line 304
    .end local v0    # "callAction":Ljava/lang/String;
    :cond_14
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.TURNOVERMOTION_CP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_16

    .line 305
    const-string v7, "cmd"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 306
    .restart local v0    # "callAction":Ljava/lang/String;
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive :CALL_ACTION_CHANGED : TURNOVERMOTION_CP, callAction = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const-string v7, "on"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v10, :cond_15

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->enableTurnOverMotion()I

    move-result v6

    .line 308
    :goto_3
    goto/16 :goto_0

    .line 307
    :cond_15
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->disableTurnOverMotion()I

    move-result v6

    goto :goto_3

    .line 309
    .end local v0    # "callAction":Ljava/lang/String;
    :cond_16
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.PROXIMITY_CP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 310
    const-string v7, "cmd"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 311
    .restart local v0    # "callAction":Ljava/lang/String;
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive :CALL_ACTION_CHANGED : PROXIMITY_CP, callAction = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    const-string v7, "on"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v10, :cond_17

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->enableProxSensor()I

    move-result v6

    .line 313
    :goto_4
    goto/16 :goto_0

    .line 312
    :cond_17
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionService$1;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;
    invoke-static {v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/android/motion/MotionRecognitionGrip;->disableProxSensor()I

    move-result v6

    goto :goto_4

    .line 314
    .end local v0    # "callAction":Ljava/lang/String;
    :cond_18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.SARDEVICE_CP"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    .line 315
    const-string v7, "cmd"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 316
    .restart local v0    # "callAction":Ljava/lang/String;
    const-string v7, "device"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 317
    .local v2, "deviceID":I
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "  mReceiver.onReceive :CALL_ACTION_CHANGED : SARDEVICE_CP, callAction = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " deviceID = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 318
    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/motion/MotionRecognitionService$1;->callSARDeviceID(ILjava/lang/String;)V

    goto/16 :goto_0

    .line 320
    .end local v0    # "callAction":Ljava/lang/String;
    .end local v2    # "deviceID":I
    :cond_19
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    const-string v8, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 321
    const-string v7, "android.intent.extra.DOCK_STATE"

    invoke-virtual {p2, v7, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 322
    .local v3, "extra":I
    const-string v7, "MotionRecognitionService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onReceive() getIntExtra "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    if-eqz v3, :cond_1a

    .line 325
    const-string v7, "MotionRecognitionService"

    const-string v8, "SERVICETYPE_MOTION stop"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 329
    :cond_1a
    const-string v7, "MotionRecognitionService"

    const-string v8, "SERVICETYPE_MOTION start"

    invoke-static {v7, v8}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
