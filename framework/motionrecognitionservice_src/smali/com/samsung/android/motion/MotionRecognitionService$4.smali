.class Lcom/samsung/android/motion/MotionRecognitionService$4;
.super Ljava/lang/Object;
.source "MotionRecognitionService.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionService;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0

    .prologue
    .line 1674
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 1677
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    .line 1681
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const v2, 0x10016

    if-ne v1, v2, :cond_0

    .line 1682
    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    float-to-int v0, v1

    .line 1683
    .local v0, "nextRotation":I
    packed-switch v0, :pswitch_data_0

    .line 1691
    .end local v0    # "nextRotation":I
    :cond_0
    :goto_0
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " .updateOrientation : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget v3, v3, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1692
    return-void

    .line 1684
    .restart local v0    # "nextRotation":I
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v2, 0x1c

    iput v2, v1, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    goto :goto_0

    .line 1685
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v2, 0x1d

    iput v2, v1, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    goto :goto_0

    .line 1686
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v2, 0x1e

    iput v2, v1, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    goto :goto_0

    .line 1687
    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService$4;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v2, 0x1f

    iput v2, v1, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    goto :goto_0

    .line 1683
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
