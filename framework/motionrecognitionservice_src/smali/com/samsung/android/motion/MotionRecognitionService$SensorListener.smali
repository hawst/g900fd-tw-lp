.class public Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;
.super Ljava/lang/Object;
.source "MotionRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SensorListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionService;


# direct methods
.method public constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0

    .prologue
    .line 1661
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSensorChanged(IIII)V
    .locals 2
    .param p1, "v1"    # I
    .param p2, "v2"    # I
    .param p3, "v3"    # I
    .param p4, "v4"    # I

    .prologue
    .line 1664
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(IIII)V
    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/android/motion/MotionRecognitionService;->access$3200(Lcom/samsung/android/motion/MotionRecognitionService;IIII)V

    .line 1665
    const/16 v0, 0x47

    if-eq p1, v0, :cond_0

    const/16 v0, 0x63

    if-ne p1, v0, :cond_2

    .line 1666
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z
    invoke-static {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionService;->access$3302(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    .line 1670
    :cond_1
    :goto_0
    return-void

    .line 1667
    :cond_2
    const/16 v0, 0x6f

    if-eq p1, v0, :cond_1

    .line 1668
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z
    invoke-static {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionService;->access$3302(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    goto :goto_0
.end method
