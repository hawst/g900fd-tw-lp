.class public Lcom/samsung/android/motion/MotionRecognitionService;
.super Lcom/samsung/android/motion/IMotionRecognitionService$Stub;
.source "MotionRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;,
        Lcom/samsung/android/motion/MotionRecognitionService$EventPool;,
        Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;,
        Lcom/samsung/android/motion/MotionRecognitionService$Listener;,
        Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;
    }
.end annotation


# static fields
.field private static final ACTION_MOTIONS_SETTINGS_CHANGED:Ljava/lang/String; = "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

.field private static final CONFIG_BROADCAST_INTENT:Z = false

.field private static final CONFIG_ENGINE_SEND_GYRO:Z = false

.field private static final DEBUG_LEVEL_HIGH:I = 0x4948

.field private static final DEBUG_LEVEL_LOW:I = 0x4f4c

.field private static final DEBUG_LEVEL_MID:I = 0x494d

.field private static final EVENT_FROM_ENGINE:I = 0x55

.field private static final EVENT_SCREEN_OFF:I = 0x57

.field private static final EVENT_SCREEN_ON:I = 0x56

.field private static final MOTION_COMMAND_ACC_SENSOR_DISABLE:I = 0x12d

.field private static final MOTION_COMMAND_ACC_SENSOR_ENABLE:I = 0x12c

.field private static final MOTION_COMMAND_CALIBRATE_GYRO:I = 0x136

.field private static final MOTION_COMMAND_GYRO_SENSOR_DISABLE:I = 0x12f

.field private static final MOTION_COMMAND_GYRO_SENSOR_ENABLE:I = 0x12e

.field private static final MOTION_COMMAND_LIGHT_SENSOR_DISABLE:I = 0x135

.field private static final MOTION_COMMAND_LIGHT_SENSOR_ENABLE:I = 0x134

.field private static final MOTION_COMMAND_MRENGINE_RESET:I = 0x137

.field private static final MOTION_COMMAND_PANNINGD_DISABLE:I = 0x133

.field private static final MOTION_COMMAND_PANNINGD_ENABLE:I = 0x132

.field private static final MOTION_COMMAND_PROX_SENSOR_DISABLE:I = 0x131

.field private static final MOTION_COMMAND_PROX_SENSOR_ENABLE:I = 0x130

.field private static final MOTION_COMMAND_SET_WINSET_ANGLE_RESET_END:I = 0xc7

.field private static final MOTION_COMMAND_SET_WINSET_ANGLE_RESET_START:I = 0x64

.field private static final MOTION_COMMAND_SMART_SCROLL_END:I = 0x12b

.field private static final MOTION_COMMAND_SMART_SCROLL_START:I = 0xc8

.field private static final SAR_ACCFLAT:I = 0x6

.field private static final SAR_ALL:I = 0x8

.field private static final SAR_FLAT:I = 0x3

.field private static final SAR_GRIP:I = 0x0

.field private static final SAR_PORTRAIT:I = 0x7

.field private static final SAR_PROX:I = 0x1

.field private static final SAR_TURNOVER:I = 0x4

.field private static final TAG:Ljava/lang/String; = "MotionRecognitionService"

.field protected static final TYPE_CONTROLMOTION:I = 0x1001b

.field protected static final TYPE_MOTIONRECOGNITION:I = 0x10017

.field protected static final TYPE_SCREEN_ORIENTATION:I = 0x10016

.field private static final localLOGD:Z = false

.field private static final localLOGV:Z = false

.field private static final localLOGVV:Z = false

.field private static final mSensitivityDef:I = 0x5

.field private static final mSensitivityMax:I = 0xa


# instance fields
.field mBootCompeleted:Z

.field private final mContext:Landroid/content/Context;

.field mEngineInitialized:Z

.field private mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

.field mEventThread:Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;

.field mFlatEvent:Z

.field private mFlipCover:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

.field private mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;

.field private mGripSensorEnabled:Z

.field private mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

.field private mIsFlat:Z

.field mListener:Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/motion/MotionRecognitionService$Listener;",
            ">;"
        }
    .end annotation
.end field

.field private mLockEngine:Ljava/lang/Object;

.field private mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

.field private mPM:Landroid/os/PowerManager;

.field private mPalmMotion:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

.field private mPostMotionVal:[I

.field private mPowerTime:J

.field private mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

.field final mReceiver:Landroid/content/BroadcastReceiver;

.field mRotate:I

.field private mRotationSensor:Landroid/hardware/Sensor;

.field private mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;

.field mScreenOn:Z

.field mSensorDelayInMsec:I

.field mSensorsEnabled:I

.field mSensorsUsed:I

.field private mThread:Landroid/os/HandlerThread;

.field private mWakelock:Landroid/os/PowerManager$WakeLock;

.field private mWm:Landroid/view/WindowManager;

.field private mbEnabledPanning:Z

.field private mbmultiwindow:Z

.field mfdAccint:I

.field private mrefCntEvents:[I

.field private mrefCntEvents_open:[I

.field private mrefSmartScroll:I

.field private mregisteredEvents_open:I

.field private msspenabled:Z

.field private refPanningDEnabled:I

.field private final rotationListener:Landroid/hardware/SensorEventListener;

.field private sensorManager:Landroid/hardware/SensorManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 190
    const-string v0, "nativemr"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 191
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0x15

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 381
    invoke-direct {p0}, Lcom/samsung/android/motion/IMotionRecognitionService$Stub;-><init>()V

    .line 101
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z

    .line 114
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPowerTime:J

    .line 118
    iput-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mThread:Landroid/os/HandlerThread;

    .line 120
    iput-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    .line 124
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    .line 126
    iput-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    .line 128
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;

    .line 132
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    .line 134
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mBootCompeleted:Z

    .line 136
    iput-boolean v6, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mScreenOn:Z

    .line 138
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mFlatEvent:Z

    .line 141
    const/16 v2, 0x1c

    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    .line 143
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    .line 145
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    .line 147
    const/16 v2, 0x14

    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorDelayInMsec:I

    .line 149
    const/4 v2, -0x1

    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mfdAccint:I

    .line 151
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    .line 159
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    .line 165
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbmultiwindow:Z

    .line 166
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    .line 167
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    .line 201
    iput-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventThread:Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;

    .line 203
    iput-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListener:Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

    .line 227
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/motion/MotionRecognitionService$1;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 1656
    const/4 v2, 0x4

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPostMotionVal:[I

    .line 1657
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z

    .line 1674
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/motion/MotionRecognitionService$4;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->rotationListener:Landroid/hardware/SensorEventListener;

    .line 382
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    .line 384
    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "motion_recognition"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mThread:Landroid/os/HandlerThread;

    .line 385
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    .line 386
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    .line 387
    .local v1, "looper_svchandler":Landroid/os/Looper;
    if-eqz v1, :cond_1

    .line 388
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    invoke-direct {v2, p0, v1}, Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    .line 394
    const-string v2, "power"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPM:Landroid/os/PowerManager;

    .line 395
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPM:Landroid/os/PowerManager;

    const-string v3, "motion_service"

    invoke-virtual {v2, v6, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    .line 396
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 398
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    invoke-direct {v2}, Lcom/samsung/android/motion/MotionRecognitionService$EventPool;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    .line 400
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    const-string v3, "sensor"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/hardware/SensorManager;

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    .line 402
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    const v3, 0x10016

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mRotationSensor:Landroid/hardware/Sensor;

    .line 403
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mRotationSensor:Landroid/hardware/Sensor;

    if-nez v2, :cond_0

    .line 404
    const-string v2, "MotionRecognitionService"

    const-string v3, "Rotation Sensor : Fail"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_0
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.sensorhub"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    .line 407
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  SSP enabled : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$2;

    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    invoke-direct {v2, p0, p1, v3}, Lcom/samsung/android/motion/MotionRecognitionService$2;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    .line 417
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    invoke-direct {v2, p1, v3}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;-><init>(Landroid/content/Context;Z)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mFlipCover:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    .line 418
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionGrip;

    invoke-direct {v2, p1}, Lcom/samsung/android/motion/MotionRecognitionGrip;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;

    .line 419
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-direct {v2, p1}, Lcom/samsung/android/motion/MotionRecognitionSAR;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;

    .line 441
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    invoke-direct {v2, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPalmMotion:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    .line 442
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPalmMotion:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    invoke-virtual {v2}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->enablePalmMotion()V

    .line 444
    new-array v2, v7, [I

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents:[I

    .line 445
    new-array v2, v7, [I

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents_open:[I

    .line 446
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$3;

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    invoke-direct {v2, p0, p1, v3}, Lcom/samsung/android/motion/MotionRecognitionService$3;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    .line 476
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mFlipCover:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    invoke-virtual {v2}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->enableFlipCover()V

    .line 479
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.multiwindow"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbmultiwindow:Z

    .line 480
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Multi-Window enabled : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbmultiwindow:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 483
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 484
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 485
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 486
    const-string v2, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 487
    const-string v2, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 488
    const-string v2, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 489
    const-string v2, "android.intent.action.BATTERY_LOW"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 490
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 491
    const-string v2, "com.sec.motions.MOTIONS_SETTINGS_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 492
    const-string v2, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 493
    const-string v2, "android.intent.action.DOCK_EVENT"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 494
    const-string v2, "android.intent.action.GRIPSENSOR_CP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 495
    const-string v2, "android.intent.action.PROXIMITY_CP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 496
    const-string v2, "android.intent.action.FLATMOTION_CP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 497
    const-string v2, "android.intent.action.TURNOVERMOTION_CP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 498
    const-string v2, "android.intent.action.SARDEVICE_CP"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 506
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 508
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWm:Landroid/view/WindowManager;

    .line 510
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

    invoke-direct {v2, p0}, Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListener:Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

    .line 511
    new-instance v2, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;

    invoke-direct {v2, p0, v5}, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;Lcom/samsung/android/motion/MotionRecognitionService$1;)V

    iput-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventThread:Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;

    .line 512
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventThread:Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;

    invoke-virtual {v2}, Lcom/samsung/android/motion/MotionRecognitionService$SensoerEventThread;->start()V

    .line 514
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :goto_0
    return-void

    .line 390
    :cond_1
    const-string v2, "MotionRecognitionService"

    const-string v3, " failed getting looper for service handler "

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected static EncodeLog(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 206
    const-string v3, "ro.debug_level"

    const-string v4, "Unknown"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 207
    .local v2, "state":Ljava/lang/String;
    const/4 v0, -0x1

    .line 209
    .local v0, "debugLevel":I
    const-string v3, "Unknown"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 210
    const-string p0, " "

    .line 224
    .end local p0    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 214
    .restart local p0    # "path":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x2

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 219
    const/16 v3, 0x4f4c

    if-ne v0, v3, :cond_2

    .line 220
    const-string p0, " "

    goto :goto_0

    .line 215
    :catch_0
    move-exception v1

    .line 216
    .local v1, "ne":Ljava/lang/NumberFormatException;
    const-string p0, " "

    goto :goto_0

    .line 221
    .end local v1    # "ne":Ljava/lang/NumberFormatException;
    :cond_2
    const/16 v3, 0x494d

    if-eq v0, v3, :cond_0

    const/16 v3, 0x4948

    if-eq v0, v3, :cond_0

    .line 224
    const-string p0, " "

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSAR;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSARmotion:Lcom/samsung/android/motion/MotionRecognitionSAR;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/android/motion/MotionRecognitionService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mregisteredEvents_open:I

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->finalizeMotionEngine()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionGrip;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mGripMotion:Lcom/samsung/android/motion/MotionRecognitionGrip;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionService$EventPool;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/android/motion/MotionRecognitionService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/android/motion/MotionRecognitionService;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V

    return-void
.end method

.method static synthetic access$1900()I
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->deinitMotionService()I

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/android/motion/MotionRecognitionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;

    .prologue
    .line 68
    invoke-static {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->initMotionService(Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;)I

    move-result v0

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mGripSensorEnabled:Z

    return p1
.end method

.method static synthetic access$2100()I
    .locals 1

    .prologue
    .line 68
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->runMoitonService()I

    move-result v0

    return v0
.end method

.method static synthetic access$2200(Lcom/samsung/android/motion/MotionRecognitionService;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents:[I

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/android/motion/MotionRecognitionService;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents_open:[I

    return-object v0
.end method

.method static synthetic access$2410(Lcom/samsung/android/motion/MotionRecognitionService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->updateRegisteredEvents()V

    return-void
.end method

.method static synthetic access$2600(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->disableUnusedSensors()V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/android/motion/MotionRecognitionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbmultiwindow:Z

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/android/motion/MotionRecognitionService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/android/motion/MotionRecognitionService;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWakelock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/android/motion/MotionRecognitionService;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # J

    .prologue
    .line 68
    iput-wide p1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mPowerTime:J

    return-wide p1
.end method

.method static synthetic access$3100(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionFlipCover;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mFlipCover:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/android/motion/MotionRecognitionService;IIII)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(IIII)V

    return-void
.end method

.method static synthetic access$3302(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/android/motion/MotionRecognitionService;)Lcom/samsung/android/motion/MotionRecognitionSettings;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/android/motion/MotionRecognitionService;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/android/motion/MotionRecognitionService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->initializeMotionEngine()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/motion/MotionRecognitionService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    return v0
.end method

.method static synthetic access$710(Lcom/samsung/android/motion/MotionRecognitionService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/android/motion/MotionRecognitionService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # Z

    .prologue
    .line 68
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/android/motion/MotionRecognitionService;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionService;
    .param p1, "x1"    # I

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    move-result v0

    return v0
.end method

.method private controlMotionSensor(I)Z
    .locals 1
    .param p1, "cmd"    # I

    .prologue
    .line 554
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionService;->setMotionCMD(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 555
    const/4 v0, 0x1

    .line 557
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native deinitMotionService()I
.end method

.method private static native disableMotionSensor()I
.end method

.method private disableUnusedSensors()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1302
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    .line 1303
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 1304
    shl-int v2, v5, v1

    .line 1305
    .local v2, "motion_sensor":I
    invoke-direct {p0, v2, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V

    .line 1303
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1307
    .end local v2    # "motion_sensor":I
    :cond_0
    invoke-direct {p0, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->enableAllSensors(Z)V

    .line 1323
    :cond_1
    return-void

    .line 1309
    .end local v1    # "i":I
    :cond_2
    const/4 v0, 0x0

    .line 1311
    .local v0, "bSensorsUsed":I
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getUsedSensorForEvents()I

    move-result v0

    .line 1313
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v6, :cond_1

    .line 1314
    shl-int v2, v5, v1

    .line 1315
    .restart local v2    # "motion_sensor":I
    and-int v3, v0, v2

    if-nez v3, :cond_3

    .line 1316
    invoke-direct {p0, v2}, Lcom/samsung/android/motion/MotionRecognitionService;->isSensorUsed(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1317
    invoke-direct {p0, v2, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V

    .line 1318
    invoke-direct {p0, v2, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V

    .line 1313
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private enableAllSensors(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 1292
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 1293
    const/4 v2, 0x1

    shl-int v1, v2, v0

    .line 1294
    .local v1, "motion_sensor":I
    invoke-direct {p0, v1, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V

    .line 1292
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1296
    .end local v1    # "motion_sensor":I
    :cond_0
    return-void
.end method

.method private static native enableMotionSensor()I
.end method

.method private enableSensor(IZ)V
    .locals 2
    .param p1, "motion_sensor"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 1226
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionManager;->isValidMotionSensor(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1286
    :cond_0
    :goto_0
    return-void

    .line 1229
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->isSensorEnabled(I)Z

    move-result v0

    if-eq p2, v0, :cond_0

    .line 1231
    if-eqz p2, :cond_2

    .line 1232
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->isSensorUsed(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1234
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    .line 1239
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1241
    :pswitch_1
    const/16 v0, 0x12c

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1245
    :pswitch_2
    const/16 v0, 0x12e

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1249
    :pswitch_3
    const/16 v0, 0x130

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1253
    :pswitch_4
    const/16 v0, 0x134

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1260
    :cond_2
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    .line 1265
    packed-switch p1, :pswitch_data_1

    :pswitch_5
    goto :goto_0

    .line 1267
    :pswitch_6
    const/16 v0, 0x12d

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1271
    :pswitch_7
    const/16 v0, 0x12f

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1275
    :pswitch_8
    const/16 v0, 0x131

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1279
    :pswitch_9
    const/16 v0, 0x135

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    goto :goto_0

    .line 1239
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    .line 1265
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_5
        :pswitch_8
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_9
    .end packed-switch
.end method

.method private finalizeMotionEngine()V
    .locals 3

    .prologue
    .line 587
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-nez v0, :cond_0

    .line 605
    :goto_0
    return-void

    .line 590
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->setAccIntStatus(I)V

    .line 592
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;

    monitor-enter v1

    .line 593
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    .line 594
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->disableMotionSensor()I

    move-result v0

    if-gez v0, :cond_1

    .line 595
    const-string v0, "MotionRecognitionService"

    const-string v2, "Fail to disableMotionSensor !"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 598
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->enableAllSensors(Z)V

    .line 600
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->rotationListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 602
    const-string v0, "MotionRecognitionService"

    const-string v2, "  .finalizeMotionEngine"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z

    .line 604
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getWindowRotation()I
    .locals 3

    .prologue
    .line 628
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWm:Landroid/view/WindowManager;

    if-nez v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWm:Landroid/view/WindowManager;

    .line 630
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWm:Landroid/view/WindowManager;

    if-eqz v1, :cond_1

    .line 631
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mWm:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 632
    .local v0, "disp":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 634
    .end local v0    # "disp":Landroid/view/Display;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static native initMotionService(Lcom/samsung/android/motion/MotionRecognitionService$SensorListener;)I
.end method

.method private initializeMotionEngine()V
    .locals 6

    .prologue
    .line 562
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-eqz v0, :cond_1

    .line 583
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v0}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isMotionEngineEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const-string v0, "MotionRecognitionService"

    const-string v1, "  .initializeMotionEngine"

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 573
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;

    monitor-enter v1

    .line 574
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    .line 575
    invoke-static {}, Lcom/samsung/android/motion/MotionRecognitionService;->enableMotionSensor()I

    move-result v0

    if-gez v0, :cond_2

    .line 576
    const-string v0, "MotionRecognitionService"

    const-string v2, "Fail to enableMotionSensor !"

    invoke-static {v0, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->enableAllSensors(Z)V

    .line 580
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->rotationListener:Landroid/hardware/SensorEventListener;

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mRotationSensor:Landroid/hardware/Sensor;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 581
    const-string v0, "MotionRecognitionService"

    const-string v2, " failed to register for rotation"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 582
    :cond_3
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private isSensorEnabled(I)Z
    .locals 2
    .param p1, "motion_sensor"    # I

    .prologue
    const/4 v0, 0x0

    .line 1201
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionManager;->isValidMotionSensor(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1203
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    and-int/2addr v1, p1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSensorUsed(I)Z
    .locals 2
    .param p1, "motion_sensor"    # I

    .prologue
    const/4 v0, 0x0

    .line 1192
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionManager;->isValidMotionSensor(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1194
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    and-int/2addr v1, p1

    if-ne v1, p1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static native runMoitonService()I
.end method

.method private sendMotionEvent(I)V
    .locals 1
    .param p1, "motion"    # I

    .prologue
    const/4 v0, 0x0

    .line 624
    invoke-direct {p0, p1, v0, v0, v0}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(IIII)V

    .line 625
    return-void
.end method

.method private sendMotionEvent(IIII)V
    .locals 26
    .param p1, "motion"    # I
    .param p2, "dataX"    # I
    .param p3, "dataY"    # I
    .param p4, "dataZ"    # I

    .prologue
    .line 638
    move/from16 v6, p2

    .line 639
    .local v6, "panningDx":I
    move/from16 v8, p3

    .line 640
    .local v8, "panningDy":I
    move/from16 v10, p4

    .line 641
    .local v10, "panningDz":I
    const/16 v20, 0x0

    .line 642
    .local v20, "walkingStatus":I
    const/4 v4, 0x1

    .line 643
    .local v4, "bSendMotion":Z
    const/16 v21, 0x0

    .line 644
    .local v21, "windowrotation":I
    if-lez p1, :cond_1

    const/16 v22, 0x78

    move/from16 v0, p1

    move/from16 v1, v22

    if-ge v0, v1, :cond_1

    .line 646
    sparse-switch p1, :sswitch_data_0

    .line 751
    :cond_0
    :goto_0
    if-eqz v4, :cond_1

    .line 752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/motion/MotionRecognitionService$EventPool;->obtain()Lcom/samsung/android/motion/MREvent;

    move-result-object v5

    .line 753
    .local v5, "motionEvent":Lcom/samsung/android/motion/MREvent;
    move/from16 v0, p1

    invoke-virtual {v5, v0}, Lcom/samsung/android/motion/MREvent;->setMotion(I)V

    .line 762
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(Lcom/samsung/android/motion/MREvent;)V

    .line 772
    .end local v5    # "motionEvent":Lcom/samsung/android/motion/MREvent;
    :cond_1
    if-nez v6, :cond_2

    if-eqz v8, :cond_3

    .line 773
    :cond_2
    const/16 v16, 0x0

    .line 774
    .local v16, "tilt":I
    move v7, v6

    .line 775
    .local v7, "panningDxImage":I
    move v9, v8

    .line 776
    .local v9, "panningDyImage":I
    move v11, v10

    .line 778
    .local v11, "panningDzImage":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mRotate:I

    move/from16 v22, v0

    packed-switch v22, :pswitch_data_0

    .line 783
    move/from16 v16, v8

    .line 788
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    move-object/from16 v22, v0

    const/16 v23, 0x3d

    const/16 v24, 0x1

    invoke-virtual/range {v22 .. v24}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getSettingMotionSensitivity(II)I

    move-result v13

    .line 789
    .local v13, "sensitivityPanningIcon":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    move-object/from16 v22, v0

    const/16 v23, 0x3d

    const/16 v24, 0x2

    invoke-virtual/range {v22 .. v24}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getSettingMotionSensitivity(II)I

    move-result v14

    .line 790
    .local v14, "sensitivityPanningImage":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    move-object/from16 v22, v0

    const/16 v23, 0x48

    const/16 v24, 0x0

    invoke-virtual/range {v22 .. v24}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getSettingMotionSensitivity(II)I

    move-result v15

    .line 792
    .local v15, "sensitivityTilt":I
    add-int/lit8 v22, v13, -0x5

    mul-int v22, v22, v6

    div-int/lit8 v22, v22, 0xa

    add-int v6, v6, v22

    .line 793
    add-int/lit8 v22, v13, -0x5

    mul-int v22, v22, v8

    div-int/lit8 v22, v22, 0xa

    add-int v8, v8, v22

    .line 795
    add-int/lit8 v22, v14, -0x5

    mul-int v22, v22, v6

    div-int/lit8 v22, v22, 0xa

    add-int v7, v7, v22

    .line 796
    add-int/lit8 v22, v14, -0x5

    mul-int v22, v22, v8

    div-int/lit8 v22, v22, 0xa

    add-int v9, v9, v22

    .line 797
    add-int/lit8 v22, v14, -0x5

    mul-int v22, v22, v10

    div-int/lit8 v22, v22, 0xa

    add-int v11, v11, v22

    .line 798
    add-int/lit8 v22, v15, -0x5

    mul-int v22, v22, v16

    div-int/lit8 v22, v22, 0xa

    add-int v16, v16, v22

    .line 801
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/motion/MotionRecognitionService$EventPool;->obtain()Lcom/samsung/android/motion/MREvent;

    move-result-object v12

    .line 802
    .local v12, "panningEvent":Lcom/samsung/android/motion/MREvent;
    const/16 v22, 0x3d

    move/from16 v0, v22

    invoke-virtual {v12, v0}, Lcom/samsung/android/motion/MREvent;->setMotion(I)V

    .line 803
    invoke-virtual {v12, v6}, Lcom/samsung/android/motion/MREvent;->setPanningDx(I)V

    .line 804
    invoke-virtual {v12, v8}, Lcom/samsung/android/motion/MREvent;->setPanningDy(I)V

    .line 805
    invoke-virtual {v12, v10}, Lcom/samsung/android/motion/MREvent;->setPanningDz(I)V

    .line 806
    invoke-virtual {v12, v7}, Lcom/samsung/android/motion/MREvent;->setPanningDxImage(I)V

    .line 807
    invoke-virtual {v12, v9}, Lcom/samsung/android/motion/MREvent;->setPanningDyImage(I)V

    .line 808
    invoke-virtual {v12, v11}, Lcom/samsung/android/motion/MREvent;->setPanningDzImage(I)V

    .line 811
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(Lcom/samsung/android/motion/MREvent;)V

    .line 813
    if-eqz v16, :cond_3

    .line 814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mEventPool:Lcom/samsung/android/motion/MotionRecognitionService$EventPool;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Lcom/samsung/android/motion/MotionRecognitionService$EventPool;->obtain()Lcom/samsung/android/motion/MREvent;

    move-result-object v17

    .line 815
    .local v17, "tiltEvent":Lcom/samsung/android/motion/MREvent;
    const/16 v22, 0x48

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MREvent;->setMotion(I)V

    .line 816
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MREvent;->setTilt(I)V

    .line 817
    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MREvent;->setWalkingStatus(I)V

    .line 818
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionService;->sendMotionEvent(Lcom/samsung/android/motion/MREvent;)V

    .line 823
    .end local v7    # "panningDxImage":I
    .end local v9    # "panningDyImage":I
    .end local v11    # "panningDzImage":I
    .end local v12    # "panningEvent":Lcom/samsung/android/motion/MREvent;
    .end local v13    # "sensitivityPanningIcon":I
    .end local v14    # "sensitivityPanningImage":I
    .end local v15    # "sensitivityTilt":I
    .end local v16    # "tilt":I
    .end local v17    # "tiltEvent":Lcom/samsung/android/motion/MREvent;
    :cond_3
    return-void

    .line 648
    :sswitch_0
    const/16 p1, 0x1

    .line 649
    goto/16 :goto_0

    .line 651
    :sswitch_1
    const/16 p1, 0x2

    .line 652
    goto/16 :goto_0

    .line 654
    :sswitch_2
    const/16 p1, 0x3

    .line 655
    goto/16 :goto_0

    .line 657
    :sswitch_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v22

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mPowerTime:J

    move-wide/from16 v24, v0

    sub-long v18, v22, v24

    .line 658
    .local v18, "time":J
    const-wide/16 v22, 0x0

    cmp-long v22, v18, v22

    if-lez v22, :cond_0

    const-wide/16 v22, 0x7d0

    cmp-long v22, v18, v22

    if-gez v22, :cond_0

    .line 660
    const-string v22, "MotionRecognitionService"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "  .sendMotionEvent : ignore TWO_TAPPING. (time="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v23

    const-string v24, ")"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    const/4 v4, 0x0

    .line 662
    goto/16 :goto_0

    .line 666
    .end local v18    # "time":J
    :sswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 667
    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_4

    .line 668
    const/16 p1, 0x68

    goto/16 :goto_0

    .line 669
    :cond_4
    if-nez v21, :cond_5

    .line 670
    const/16 p1, 0x67

    goto/16 :goto_0

    .line 672
    :cond_5
    const/16 p1, 0x0

    .line 673
    goto/16 :goto_0

    .line 675
    :sswitch_5
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 676
    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_6

    .line 677
    const/16 p1, 0x67

    goto/16 :goto_0

    .line 678
    :cond_6
    if-nez v21, :cond_7

    .line 679
    const/16 p1, 0x68

    goto/16 :goto_0

    .line 681
    :cond_7
    const/16 p1, 0x0

    .line 683
    goto/16 :goto_0

    .line 685
    :sswitch_6
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 686
    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_8

    if-nez v21, :cond_9

    .line 687
    :cond_8
    const/16 p1, 0x69

    goto/16 :goto_0

    .line 689
    :cond_9
    const/16 p1, 0x0

    .line 691
    goto/16 :goto_0

    .line 693
    :sswitch_7
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 694
    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    if-nez v21, :cond_b

    .line 695
    :cond_a
    const/16 p1, 0x6a

    goto/16 :goto_0

    .line 697
    :cond_b
    const/16 p1, 0x0

    .line 700
    goto/16 :goto_0

    .line 702
    :sswitch_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 703
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    .line 704
    const/16 p1, 0x67

    goto/16 :goto_0

    .line 705
    :cond_c
    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    .line 706
    const/16 p1, 0x68

    goto/16 :goto_0

    .line 708
    :cond_d
    const/16 p1, 0x0

    .line 710
    goto/16 :goto_0

    .line 712
    :sswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 713
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_e

    .line 714
    const/16 p1, 0x68

    goto/16 :goto_0

    .line 715
    :cond_e
    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_f

    .line 716
    const/16 p1, 0x67

    goto/16 :goto_0

    .line 718
    :cond_f
    const/16 p1, 0x0

    .line 719
    goto/16 :goto_0

    .line 721
    :sswitch_a
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 722
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_10

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_11

    .line 723
    :cond_10
    const/16 p1, 0x69

    goto/16 :goto_0

    .line 725
    :cond_11
    const/16 p1, 0x0

    .line 726
    goto/16 :goto_0

    .line 728
    :sswitch_b
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->getWindowRotation()I

    move-result v21

    .line 729
    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_12

    const/16 v22, 0x3

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    .line 730
    :cond_12
    const/16 p1, 0x6a

    goto/16 :goto_0

    .line 732
    :cond_13
    const/16 p1, 0x0

    .line 733
    goto/16 :goto_0

    .line 735
    :sswitch_c
    const/4 v4, 0x0

    .line 736
    const-string v22, "MotionRecognitionService"

    const-string v23, " .received : SMART_SCROLL_CAMERA_OFF"

    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 740
    :sswitch_d
    const/4 v4, 0x0

    .line 741
    const-string v22, "MotionRecognitionService"

    const-string v23, " .received : SMART_SCROLL_CAMERA_ON"

    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 745
    :sswitch_e
    const-string v22, "MotionRecognitionService"

    const-string v23, " .received : SMART_RELAY"

    invoke-static/range {v22 .. v23}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 779
    .restart local v7    # "panningDxImage":I
    .restart local v9    # "panningDyImage":I
    .restart local v11    # "panningDzImage":I
    .restart local v16    # "tilt":I
    :pswitch_0
    move/from16 v16, v6

    goto/16 :goto_1

    .line 780
    :pswitch_1
    neg-int v0, v8

    move/from16 v16, v0

    goto/16 :goto_1

    .line 781
    :pswitch_2
    neg-int v0, v6

    move/from16 v16, v0

    goto/16 :goto_1

    .line 646
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_3
        0x2f -> :sswitch_1
        0x30 -> :sswitch_2
        0x3f -> :sswitch_0
        0x67 -> :sswitch_4
        0x68 -> :sswitch_5
        0x69 -> :sswitch_6
        0x6a -> :sswitch_7
        0x6b -> :sswitch_8
        0x6c -> :sswitch_9
        0x6d -> :sswitch_a
        0x6e -> :sswitch_b
        0x6f -> :sswitch_c
        0x70 -> :sswitch_d
        0x71 -> :sswitch_e
    .end sparse-switch

    .line 778
    :pswitch_data_0
    .packed-switch 0x1d
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private sendMotionEvent(Lcom/samsung/android/motion/MREvent;)V
    .locals 3
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 608
    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    .line 609
    .local v0, "motion":I
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v2, v0}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isActivatedEvent(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 620
    :cond_0
    :goto_0
    return-void

    .line 613
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 614
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 615
    .local v1, "msg":Landroid/os/Message;
    const/16 v2, 0x55

    iput v2, v1, Landroid/os/Message;->what:I

    .line 616
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 618
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mHandler:Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;

    invoke-virtual {v2, v1}, Lcom/samsung/android/motion/MotionRecognitionService$ServiceHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method private static native setMotionCMD(I)I
.end method

.method private static native stopMoitonService()I
.end method

.method private updateRegisteredEvents()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 844
    const/4 v1, 0x0

    .line 845
    .local v1, "registeredEvents":I
    const/4 v2, 0x0

    .line 846
    .local v2, "registeredEvents_Open":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x15

    if-ge v0, v3, :cond_2

    .line 848
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents:[I

    aget v3, v3, v0

    if-eqz v3, :cond_0

    .line 849
    shl-int v3, v4, v0

    or-int/2addr v1, v3

    .line 850
    :cond_0
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents_open:[I

    aget v3, v3, v0

    if-eqz v3, :cond_1

    .line 851
    shl-int v3, v4, v0

    or-int/2addr v2, v3

    .line 846
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 853
    :cond_2
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mregisteredEvents_open:I

    .line 854
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " open api "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mregisteredEvents_open:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v3, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionSettings;->setRegisteredEvents(II)V

    .line 857
    return-void
.end method

.method private useSensor(IZ)V
    .locals 2
    .param p1, "motion_sensor"    # I
    .param p2, "used"    # Z

    .prologue
    .line 1210
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionManager;->isValidMotionSensor(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1219
    :cond_0
    :goto_0
    return-void

    .line 1213
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionService;->isSensorUsed(I)Z

    move-result v0

    if-eq p2, v0, :cond_0

    .line 1214
    if-eqz p2, :cond_2

    .line 1215
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    goto :goto_0

    .line 1217
    :cond_2
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    goto :goto_0
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 10
    .param p1, "fd"    # Ljava/io/FileDescriptor;
    .param p2, "pw"    # Ljava/io/PrintWriter;
    .param p3, "args"    # [Ljava/lang/String;

    .prologue
    .line 1158
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    const-string v8, "android.permission.DUMP"

    invoke-virtual {v5, v8}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v5

    if-eqz v5, :cond_0

    .line 1160
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Permission Denial: can\'t dump MotionService from from pid="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", uid="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " without permission "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "android.permission.DUMP"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1186
    :goto_0
    return-void

    .line 1166
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 1167
    .local v6, "time":J
    const-string v5, "motion_recognition"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1169
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " : mSensorsUsed="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsUsed:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, ", mSensorsEnabled="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mSensorsEnabled:I

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " : currentTimeMillis="

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1172
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    monitor-enter v8

    .line 1173
    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " : listener count="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1175
    const/4 v1, 0x0

    .line 1176
    .local v1, "i":I
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    move v2, v1

    .end local v1    # "i":I
    .local v2, "i":I
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    .line 1177
    .local v4, "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "   mListeners["

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v1, v2, 0x1

    .end local v2    # "i":I
    .restart local v1    # "i":I
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, "] = mMotionSensors="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v9, v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionSensors:I

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, ", mUseAlways="

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v9, v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mUseAlways:Z

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1179
    :try_start_1
    iget-object v5, v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mToken:Landroid/os/IBinder;

    invoke-static {v5}, Lcom/samsung/android/motion/IMotionRecognitionCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/motion/IMotionRecognitionCallback;

    move-result-object v0

    .line 1180
    .local v0, "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "                   "

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-interface {v0}, Lcom/samsung/android/motion/IMotionRecognitionCallback;->getListenerInfo()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .end local v0    # "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    :goto_2
    move v2, v1

    .line 1183
    .end local v1    # "i":I
    .restart local v2    # "i":I
    goto :goto_1

    .line 1184
    .end local v4    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_1
    :try_start_2
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 1185
    monitor-exit v8

    goto/16 :goto_0

    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1181
    .restart local v1    # "i":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :catch_0
    move-exception v5

    goto :goto_2
.end method

.method public getPickUpMotionStatus()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 836
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    const v4, 0x8000

    invoke-virtual {v3, v4}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isActivatedMotion(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isActivatedMotion(I)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    move v0, v2

    .line 839
    .local v0, "isEnabled":Z
    :goto_0
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " pick up status : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v3}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isMotionEngineEnabled()Z

    move-result v3

    if-ne v3, v2, :cond_2

    :goto_1
    return v2

    .end local v0    # "isEnabled":Z
    :cond_1
    move v0, v1

    .line 836
    goto :goto_0

    .restart local v0    # "isEnabled":Z
    :cond_2
    move v2, v1

    .line 840
    goto :goto_1
.end method

.method public getSSPstatus()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 829
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    .line 830
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ssp status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    return v0
.end method

.method public registerCallback(Landroid/os/IBinder;II)V
    .locals 18
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "motion_sensors"    # I
    .param p3, "motion_events"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 861
    const/4 v9, 0x0

    .line 862
    .local v9, "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    const/4 v4, 0x0

    .line 863
    .local v4, "busemotionalways":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    monitor-enter v15

    .line 864
    :try_start_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    .line 865
    .local v11, "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    iget-object v14, v11, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mToken:Landroid/os/IBinder;

    move-object/from16 v0, p1

    if-ne v0, v14, :cond_0

    .line 866
    monitor-exit v15

    .line 1001
    .end local v11    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_1
    :goto_0
    return-void

    .line 871
    :cond_2
    if-nez v9, :cond_c

    .line 872
    if-nez p3, :cond_3

    and-int/lit8 v14, p2, 0x2

    if-eqz v14, :cond_3

    .line 876
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    .line 878
    :cond_3
    const/high16 v14, 0x40000000    # 2.0f

    move/from16 v0, p2

    if-ne v0, v14, :cond_4

    .line 879
    const/4 v4, 0x1

    .line 882
    :cond_4
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/16 v14, 0x15

    if-ge v7, v14, :cond_9

    .line 883
    const/4 v14, 0x1

    shl-int v12, v14, v7

    .line 884
    .local v12, "motion_event":I
    and-int v14, v12, p3

    if-eqz v14, :cond_6

    .line 885
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents:[I

    aget v16, v14, v7

    add-int/lit8 v16, v16, 0x1

    aput v16, v14, v7

    .line 886
    const/4 v14, 0x1

    if-ne v4, v14, :cond_5

    .line 887
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents_open:[I

    aget v16, v14, v7

    add-int/lit8 v16, v16, 0x1

    aput v16, v14, v7

    .line 889
    :cond_5
    sparse-switch v12, :sswitch_data_0

    .line 882
    :cond_6
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 895
    :sswitch_0
    or-int/lit8 p2, p2, 0x1

    .line 896
    goto :goto_2

    .line 898
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v14, v12}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isActivatedMotion(I)Z

    move-result v14

    if-eqz v14, :cond_7

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    if-nez v14, :cond_7

    .line 899
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    invoke-virtual {v14}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->enableAccINT()V

    goto :goto_2

    .line 968
    .end local v7    # "i":I
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v12    # "motion_event":I
    :catchall_0
    move-exception v14

    :goto_3
    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v14

    .line 901
    .restart local v7    # "i":I
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v12    # "motion_event":I
    :cond_7
    :try_start_1
    const-string v14, "MotionRecognitionService"

    const-string v16, " smart alert is disabled by setting or ssp is activated : "

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 906
    :sswitch_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    if-nez v14, :cond_8

    .line 908
    :cond_8
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    .line 913
    :sswitch_3
    or-int/lit8 p2, p2, 0x3

    .line 916
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    goto :goto_2

    .line 926
    :sswitch_4
    or-int/lit8 p2, p2, 0x3

    .line 927
    goto :goto_2

    .line 931
    :sswitch_5
    or-int/lit8 p2, p2, 0x7

    .line 932
    goto :goto_2

    .line 934
    :sswitch_6
    const/16 p2, 0xf

    .line 935
    goto :goto_2

    .line 944
    .end local v12    # "motion_event":I
    :cond_9
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->updateRegisteredEvents()V

    .line 946
    new-instance v10, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    invoke-direct {v10, v0, v1, v2, v3}, Lcom/samsung/android/motion/MotionRecognitionService$Listener;-><init>(Lcom/samsung/android/motion/MotionRecognitionService;Landroid/os/IBinder;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 947
    .end local v9    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    .local v10, "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    if-nez p1, :cond_10

    .line 948
    :try_start_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v0, v10}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 952
    :goto_4
    const-string v6, ""

    .line 953
    .local v6, "client":Ljava/lang/String;
    if-eqz p1, :cond_a

    .line 954
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v10, v14}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 957
    :try_start_3
    invoke-static/range {p1 .. p1}, Lcom/samsung/android/motion/IMotionRecognitionCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/motion/IMotionRecognitionCallback;

    move-result-object v5

    .line 958
    .local v5, "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    invoke-interface {v5}, Lcom/samsung/android/motion/IMotionRecognitionCallback;->getListenerInfo()Ljava/lang/String;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v6

    .line 962
    .end local v5    # "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    :cond_a
    :goto_5
    :try_start_4
    const-string v14, "MotionRecognitionService"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "  .registerCallback : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", motion_sensors="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, ", client="

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-static {v6}, Lcom/samsung/android/motion/MotionRecognitionService;->EncodeLog(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v14, v0}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/lang/Object;->notify()V

    .line 964
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mIsFlat:Z

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v14, v0, :cond_b

    .line 965
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    const/16 v16, 0x2d

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->setAccIntStatus(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_b
    move-object v9, v10

    .line 968
    .end local v6    # "client":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v10    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    .restart local v9    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_c
    :try_start_5
    monitor-exit v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 972
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-nez v14, :cond_11

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mScreenOn:Z

    if-eqz v14, :cond_11

    .line 973
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->initializeMotionEngine()V

    .line 977
    :cond_d
    :goto_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v14}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getUsedSensorForEvents()I

    move-result p2

    .line 978
    const/4 v7, 0x0

    .restart local v7    # "i":I
    :goto_7
    const/4 v14, 0x4

    if-ge v7, v14, :cond_12

    .line 979
    const/4 v14, 0x1

    shl-int v13, v14, v7

    .line 980
    .local v13, "motion_sensor":I
    and-int v14, v13, p2

    if-eqz v14, :cond_f

    .line 981
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V

    .line 982
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;

    monitor-enter v15

    .line 983
    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-eqz v14, :cond_e

    .line 984
    const/4 v14, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v14}, Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V

    .line 985
    :cond_e
    monitor-exit v15
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 978
    :cond_f
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    .line 950
    .end local v9    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    .end local v13    # "motion_sensor":I
    .restart local v10    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_10
    :try_start_7
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v14, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_4

    .line 968
    :catchall_1
    move-exception v14

    move-object v9, v10

    .end local v10    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    .restart local v9    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    goto/16 :goto_3

    .line 974
    .end local v7    # "i":I
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mScreenOn:Z

    if-nez v14, :cond_d

    const/high16 v14, 0x100000

    and-int v14, v14, p3

    if-eqz v14, :cond_d

    .line 975
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/motion/MotionRecognitionService;->initializeMotionEngine()V

    goto :goto_6

    .line 985
    .restart local v7    # "i":I
    .restart local v13    # "motion_sensor":I
    :catchall_2
    move-exception v14

    :try_start_8
    monitor-exit v15
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v14

    .line 991
    .end local v13    # "motion_sensor":I
    :cond_12
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    if-lez v14, :cond_1

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    if-nez v14, :cond_1

    .line 992
    const/16 v14, 0x132

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    move-result v14

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    goto/16 :goto_0

    .line 959
    .end local v9    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    .restart local v6    # "client":Ljava/lang/String;
    .restart local v10    # "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :catch_0
    move-exception v14

    goto/16 :goto_5

    .line 889
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_0
        0x4 -> :sswitch_1
        0x8 -> :sswitch_4
        0x10 -> :sswitch_3
        0x20 -> :sswitch_3
        0x40 -> :sswitch_3
        0x80 -> :sswitch_4
        0x100 -> :sswitch_4
        0x200 -> :sswitch_4
        0x400 -> :sswitch_5
        0x800 -> :sswitch_5
        0x1000 -> :sswitch_4
        0x2000 -> :sswitch_0
        0x4000 -> :sswitch_4
        0x8000 -> :sswitch_0
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_0
        0x80000 -> :sswitch_2
        0x100000 -> :sswitch_5
        0x1fffff -> :sswitch_6
    .end sparse-switch
.end method

.method public resetMotionEngine()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 1149
    const-string v1, "MotionRecognitionService"

    const-string v2, "  .resetMotionEngine"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    if-nez v1, :cond_1

    .line 1152
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/16 v1, 0x137

    invoke-direct {p0, v1}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMotionAngle(Landroid/os/IBinder;I)V
    .locals 9
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "status"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1102
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    if-nez v5, :cond_0

    .line 1126
    :goto_0
    return-void

    .line 1103
    :cond_0
    const/4 v3, 0x0

    .line 1105
    .local v3, "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    iget-object v6, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    monitor-enter v6

    .line 1106
    :try_start_0
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    .line 1107
    .local v4, "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    iget-object v5, v4, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mToken:Landroid/os/IBinder;

    if-ne p1, v5, :cond_1

    .line 1108
    move-object v3, v4

    .line 1112
    .end local v4    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_2
    const-string v1, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1114
    .local v1, "client":Ljava/lang/String;
    :try_start_1
    invoke-static {p1}, Lcom/samsung/android/motion/IMotionRecognitionCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/motion/IMotionRecognitionCallback;

    move-result-object v0

    .line 1115
    .local v0, "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    invoke-interface {v0}, Lcom/samsung/android/motion/IMotionRecognitionCallback;->getListenerInfo()Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1119
    .end local v0    # "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    :goto_1
    if-eqz v3, :cond_3

    .line 1120
    add-int/lit8 v5, p2, 0x64

    :try_start_2
    invoke-direct {p0, v5}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    .line 1121
    const-string v5, "MotionRecognitionService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "  .setMotionAngle : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", client="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionService;->EncodeLog(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/lang/Object;->notify()V

    .line 1125
    monitor-exit v6

    goto :goto_0

    .end local v1    # "client":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v5

    .line 1116
    .restart local v1    # "client":Ljava/lang/String;
    .restart local v2    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method public setMotionTiltLevel(IIIIII)V
    .locals 3
    .param p1, "stopUp"    # I
    .param p2, "level1Up"    # I
    .param p3, "level2Up"    # I
    .param p4, "stopDown"    # I
    .param p5, "level1Down"    # I
    .param p6, "level2Down"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1130
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionService;->sensorManager:Landroid/hardware/SensorManager;

    if-nez v0, :cond_0

    .line 1145
    :goto_0
    return-void

    .line 1133
    :cond_0
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " @ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1144
    const-string v0, "MotionRecognitionService"

    const-string v1, "  .does not support setMotionTiltLevel "

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unregisterCallback(Landroid/os/IBinder;)V
    .locals 12
    .param p1, "binder"    # Landroid/os/IBinder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1006
    const/4 v5, 0x0

    .line 1007
    .local v5, "l":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    const-string v2, ""

    .line 1009
    .local v2, "client":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    monitor-enter v9

    .line 1010
    :try_start_0
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    .line 1011
    .local v6, "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    iget-object v8, v6, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mToken:Landroid/os/IBinder;

    if-ne p1, v8, :cond_0

    .line 1012
    move-object v5, v6

    .line 1017
    .end local v6    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_1
    if-eqz v5, :cond_8

    .line 1019
    iget v8, v5, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionEvents:I

    if-nez v8, :cond_2

    iget v8, v5, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionSensors:I

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_2

    .line 1020
    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    .line 1021
    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    if-nez v8, :cond_2

    .line 1022
    const/16 v8, 0x133

    invoke-direct {p0, v8}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    .line 1025
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v8, 0x15

    if-ge v3, v8, :cond_6

    .line 1026
    const/4 v8, 0x1

    shl-int v7, v8, v3

    .line 1027
    .local v7, "motion_event":I
    iget v8, v5, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionEvents:I

    and-int/2addr v8, v7

    if-eqz v8, :cond_4

    .line 1028
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents:[I

    aget v10, v8, v3

    add-int/lit8 v10, v10, -0x1

    aput v10, v8, v3

    .line 1029
    iget v8, v5, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionSensors:I

    const/high16 v10, 0x40000000    # 2.0f

    and-int/2addr v8, v10

    if-eqz v8, :cond_3

    .line 1030
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefCntEvents_open:[I

    aget v10, v8, v3

    add-int/lit8 v10, v10, -0x1

    aput v10, v8, v3

    .line 1031
    :cond_3
    sparse-switch v7, :sswitch_data_0

    .line 1025
    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1033
    :sswitch_0
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mMotionSettings:Lcom/samsung/android/motion/MotionRecognitionSettings;

    invoke-virtual {v8}, Lcom/samsung/android/motion/MotionRecognitionSettings;->isMotionEngineEnabled()Z

    move-result v8

    if-eqz v8, :cond_5

    iget-boolean v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->msspenabled:Z

    if-nez v8, :cond_5

    .line 1034
    const-string v8, "MotionRecognitionService"

    const-string v10, " disable reactive alert mode "

    invoke-static {v8, v10}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1035
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mReactiveAlert:Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;

    invoke-virtual {v8}, Lcom/samsung/android/motion/MotionRecognitionReactiveAlert;->disableAccINT()V

    .line 1036
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mFlatEvent:Z

    goto :goto_1

    .line 1090
    .end local v3    # "i":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "motion_event":I
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v8

    .line 1038
    .restart local v3    # "i":I
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v7    # "motion_event":I
    :cond_5
    :try_start_1
    const-string v8, "MotionRecognitionService"

    const-string v10, " smart alert is disabled by setting or ssp is activated : "

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1042
    :sswitch_1
    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mrefSmartScroll:I

    .line 1049
    :sswitch_2
    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    .line 1050
    iget v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I

    if-nez v8, :cond_4

    .line 1051
    const/16 v8, 0x133

    invoke-direct {p0, v8}, Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z

    .line 1052
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z

    goto :goto_1

    .line 1061
    .end local v7    # "motion_event":I
    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->updateRegisteredEvents()V

    .line 1062
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1064
    if-eqz p1, :cond_7

    .line 1065
    const/4 v8, 0x0

    invoke-interface {p1, v5, v8}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1067
    :try_start_2
    invoke-static {p1}, Lcom/samsung/android/motion/IMotionRecognitionCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/android/motion/IMotionRecognitionCallback;

    move-result-object v1

    .line 1068
    .local v1, "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    invoke-interface {v1}, Lcom/samsung/android/motion/IMotionRecognitionCallback;->getListenerInfo()Ljava/lang/String;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    .line 1072
    .end local v1    # "cb":Lcom/samsung/android/motion/IMotionRecognitionCallback;
    :cond_7
    :goto_2
    :try_start_3
    const-string v8, "MotionRecognitionService"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "  .unregisterCallback : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", client="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionService;->EncodeLog(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/lang/Object;->notify()V

    .line 1076
    .end local v3    # "i":I
    :cond_8
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->disableUnusedSensors()V

    .line 1078
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-nez v8, :cond_a

    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->finalizeMotionEngine()V

    .line 1090
    :cond_9
    :goto_3
    monitor-exit v9

    .line 1091
    return-void

    .line 1079
    :cond_a
    iget-boolean v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mScreenOn:Z

    if-nez v8, :cond_9

    .line 1080
    const/4 v0, 0x0

    .line 1081
    .local v0, "benabledSmartrelay":Z
    iget-object v8, p0, Lcom/samsung/android/motion/MotionRecognitionService;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_b
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_c

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/motion/MotionRecognitionService$Listener;

    .line 1082
    .restart local v6    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    iget v8, v6, Lcom/samsung/android/motion/MotionRecognitionService$Listener;->mMotionEvents:I

    const/high16 v10, 0x100000

    and-int/2addr v8, v10

    if-eqz v8, :cond_b

    .line 1083
    const-string v8, "MotionRecognitionService"

    const-string v10, "  SMART_RELAY is activated"

    invoke-static {v8, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1084
    const/4 v0, 0x1

    .line 1088
    .end local v6    # "listener":Lcom/samsung/android/motion/MotionRecognitionService$Listener;
    :cond_c
    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionService;->finalizeMotionEngine()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    .line 1069
    .end local v0    # "benabledSmartrelay":Z
    .restart local v3    # "i":I
    :catch_0
    move-exception v8

    goto :goto_2

    .line 1031
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x10 -> :sswitch_2
        0x20 -> :sswitch_2
        0x40 -> :sswitch_2
        0x80000 -> :sswitch_1
    .end sparse-switch
.end method

.method public useMotionAlways(Landroid/os/IBinder;Z)V
    .locals 0
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "bUseAlways"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 1096
    return-void
.end method
