.class Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;
.super Ljava/lang/Object;
.source "MotionRecognitionPalmMotion.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionPalmMotion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PalmSweepRecognizer"
.end annotation


# instance fields
.field private final PALM_SWEEP_SIZE_TH:I

.field public final SWEEP_LEFT:I

.field public final SWEEP_RIGHT:I

.field private TILT_TO_ZOOM_XVAR:F

.field mArrValidMajor:[Z

.field private mDiffx:F

.field private mFalseSizeCnt:I

.field private mLRSDCnt:I

.field private mLRSizeDecreased:Z

.field private mMoveXTH:I

.field private mPreSweepDown:Z

.field private mSmeanX:F

.field private mSmeanXpre:F

.field private mSweepDown:Z

.field private mSweepInitDiffx:F

.field private mSweepLeftReverseSum:F

.field private mSweepReverse:F

.field private mSweepRightReverseSum:F

.field private mSweepVarX:F

.field private mVarXBig:Z

.field private mbPalmSwipeDetected:Z

.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;


# direct methods
.method private constructor <init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 466
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    .line 468
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mPreSweepDown:Z

    .line 469
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mbPalmSwipeDetected:Z

    .line 471
    const/16 v0, 0x32

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->PALM_SWEEP_SIZE_TH:I

    .line 473
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    .line 477
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->SWEEP_RIGHT:I

    .line 479
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->SWEEP_LEFT:I

    .line 482
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSizeDecreased:Z

    .line 483
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mVarXBig:Z

    .line 484
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSDCnt:I

    .line 485
    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mMoveXTH:I

    .line 486
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    .line 487
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    .line 488
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    .line 489
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    .line 490
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    .line 491
    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepInitDiffx:F

    .line 492
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepVarX:F

    .line 493
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepReverse:F

    .line 495
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    .line 497
    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mFalseSizeCnt:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;Lcom/samsung/android/motion/MotionRecognitionPalmMotion$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion;
    .param p2, "x1"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$1;

    .prologue
    .line 466
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;-><init>(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)V

    return-void
.end method

.method private CalculateHorizontalSweepData(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V
    .locals 7
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 562
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->SetArrMaxTouchMajor(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V

    .line 564
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    if-nez v0, :cond_2

    iget v0, p1, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    const/high16 v1, 0x42c80000    # 100.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-gtz v0, :cond_1

    :cond_0
    iget v0, p1, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    const/high16 v1, 0x42480000    # 50.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2000(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)Z

    move-result v0

    if-eqz v0, :cond_2

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_2

    :cond_1
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1300(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)[I

    move-result-object v2

    aget v2, v2, v4

    add-int/lit8 v2, v2, 0x4

    int-to-float v2, v2

    div-float/2addr v1, v2

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 566
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepInitDiffx:F

    .line 567
    iput-boolean v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    .line 568
    iput-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSizeDecreased:Z

    .line 569
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)[I

    move-result-object v1

    aget v1, v1, v4

    add-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepVarX:F

    .line 571
    const-string v0, "PalmMotion"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[Surface touch Event] Palm swipe start, touchCnt:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepInitDiffx:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " m:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumMajor:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " TILT_TO_ZOOM_XVAR: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->Nd:[I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)[I

    move-result-object v3

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 575
    :cond_2
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_4

    .line 576
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    if-eqz v0, :cond_4

    .line 578
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SsumEccen:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2100(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    cmpg-float v0, v0, v5

    if-gez v0, :cond_4

    .line 579
    iput-boolean v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSizeDecreased:Z

    .line 580
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSDCnt:I

    if-gez v0, :cond_3

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSDCnt:I

    .line 581
    :cond_3
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mFalseSizeCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mFalseSizeCnt:I

    .line 586
    :cond_4
    iget v0, p1, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mAction:I

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    if-eqz v0, :cond_6

    .line 587
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    .line 588
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    sub-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    .line 589
    :cond_5
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    .line 592
    :cond_6
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SvarX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1300(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepVarX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    iput-boolean v6, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mVarXBig:Z

    .line 593
    :cond_7
    return-void
.end method

.method private GetRangeNum(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 521
    const/4 v0, 0x0

    .line 522
    .local v0, "result":I
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v2

    float-to-int v1, v2

    .line 524
    .local v1, "x":I
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mIsRotated:Z
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2000(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 526
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    if-ge v1, v2, :cond_0

    .line 528
    const/4 v0, 0x0

    .line 558
    :goto_0
    return v0

    .line 530
    :cond_0
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x3

    if-ge v1, v2, :cond_1

    .line 532
    const/4 v0, 0x1

    goto :goto_0

    .line 536
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 541
    :cond_2
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    div-int/lit8 v2, v2, 0x4

    if-ge v1, v2, :cond_3

    .line 543
    const/4 v0, 0x0

    goto :goto_0

    .line 545
    :cond_3
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    div-int/lit8 v2, v2, 0x4

    if-ge v1, v2, :cond_4

    .line 547
    const/4 v0, 0x1

    goto :goto_0

    .line 549
    :cond_4
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    if-ge v1, v2, :cond_5

    .line 551
    const/4 v0, 0x2

    goto :goto_0

    .line 555
    :cond_5
    const/4 v0, 0x3

    goto :goto_0
.end method

.method private RocognizeHorizontalSweepData(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V
    .locals 9
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 596
    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSizeDecreased:Z

    if-ne v2, v5, :cond_0

    .line 597
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mTouchCnt:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$2200(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSDCnt:I

    sub-int/2addr v2, v3

    const/16 v3, 0xa

    if-le v2, v3, :cond_0

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mFalseSizeCnt:I

    const/16 v3, 0x11

    if-le v2, v3, :cond_0

    .line 598
    iput-boolean v7, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    .line 599
    const-string v2, "PalmMotion"

    const-string v3, "[Surface Touch Event] Palm Swipe Fail 3 - FalseSize "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    :cond_0
    const/4 v1, 0x0

    .line 604
    .local v1, "validMajorCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_2

    .line 606
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v2, v2, v0

    if-ne v2, v5, :cond_1

    .line 608
    add-int/lit8 v1, v1, 0x1

    .line 604
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 612
    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mPalmTouchRecognizer:Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmTouchRecognizer;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$2300(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmTouchRecognizer;

    move-result-object v2

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmTouchRecognizer;->mPalmDown:Z
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmTouchRecognizer;->access$2400(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmTouchRecognizer;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 613
    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepInitDiffx:F

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v3

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    .line 614
    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mVarXBig:Z

    if-nez v2, :cond_6

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mMoveXTH:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    if-lt v1, v6, :cond_6

    .line 615
    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepReverse:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_4

    .line 616
    const-string v2, "PalmMotion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Surface Touch Event] Palm Swipe - RIGHT, ValidMajor[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] , dist:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mGestureSurfaceTouch:Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$1600(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;->onSweepDown(I)Z

    .line 618
    iput-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mbPalmSwipeDetected:Z

    .line 635
    :cond_3
    :goto_1
    return-void

    .line 619
    :cond_4
    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepReverse:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_5

    .line 620
    const-string v2, "PalmMotion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Surface Touch Event] Palm Swipe - LEFT, ValidMajor[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] , dist:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->this$0:Lcom/samsung/android/motion/MotionRecognitionPalmMotion;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->mGestureSurfaceTouch:Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion;->access$1600(Lcom/samsung/android/motion/MotionRecognitionPalmMotion;)Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$GestureSurfaceTouch;->onSweepDown(I)Z

    .line 622
    iput-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mbPalmSwipeDetected:Z

    goto :goto_1

    .line 625
    :cond_5
    const-string v2, "PalmMotion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Surface Touch Event] Palm Swipe Fail 1 - mSweepRightReverseSum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mSweepLeftReverseSum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 631
    :cond_6
    const-string v2, "PalmMotion"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[Surface Touch Event] Palm Swipe Fail 2 - validMajor[ "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v7

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v6

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v4, v4, v8

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ] , dist:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mVarXBig:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mVarXBig:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private SetArrMaxTouchMajor(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V
    .locals 3
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 511
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->GetRangeNum(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v0

    .line 513
    .local v0, "range":I
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aget-boolean v1, v1, v0

    if-nez v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    const/4 v2, 0x1

    aput-boolean v2, v1, v0

    .line 517
    :cond_0
    return-void
.end method

.method static synthetic access$700(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;

    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mPreSweepDown:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;
    .param p1, "x1"    # Z

    .prologue
    .line 466
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mPreSweepDown:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;

    .prologue
    .line 466
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;
    .param p1, "x1"    # Z

    .prologue
    .line 466
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    return p1
.end method


# virtual methods
.method public Initialize(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V
    .locals 5
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 649
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mMoveXTH:I

    .line 650
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenHeight:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1700(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v1

    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v2

    if-le v1, v2, :cond_0

    .line 651
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenWidth:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1800(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    .line 655
    :goto_0
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    .line 656
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mPreSweepDown:Z

    .line 657
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mbPalmSwipeDetected:Z

    .line 658
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    .line 659
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    .line 660
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSizeDecreased:Z

    .line 661
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mLRSDCnt:I

    .line 662
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepLeftReverseSum:F

    .line 663
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepRightReverseSum:F

    .line 664
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mDiffx:F

    .line 665
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mVarXBig:Z

    .line 666
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepVarX:F

    .line 667
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    const/high16 v2, 0x40600000    # 3.5f

    div-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepReverse:F

    .line 668
    iput v4, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepInitDiffx:F

    .line 669
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mFalseSizeCnt:I

    .line 670
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 672
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mArrValidMajor:[Z

    aput-boolean v3, v1, v0

    .line 670
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 653
    .end local v0    # "i":I
    :cond_0
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mScreenHeight:I
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1700(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->TILT_TO_ZOOM_XVAR:F

    goto :goto_0

    .line 674
    .restart local v0    # "i":I
    :cond_1
    return-void
.end method

.method public Process(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V
    .locals 2
    .param p1, "data"    # Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;

    .prologue
    .line 639
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanXpre:F

    .line 640
    # getter for: Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->SmeanX:F
    invoke-static {p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->access$1900(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSmeanX:F

    .line 641
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->CalculateHorizontalSweepData(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V

    .line 642
    iget v0, p1, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;->mAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mbPalmSwipeDetected:Z

    if-nez v0, :cond_0

    .line 643
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->RocognizeHorizontalSweepData(Lcom/samsung/android/motion/MotionRecognitionPalmMotion$SurfaceTouchSharingData;)V

    .line 645
    :cond_0
    return-void
.end method

.method public getSweepDown()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    return v0
.end method

.method public setSweepDown(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 506
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionPalmMotion$PalmSweepRecognizer;->mSweepDown:Z

    .line 507
    return-void
.end method
