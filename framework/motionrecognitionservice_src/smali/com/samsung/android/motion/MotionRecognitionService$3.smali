.class Lcom/samsung/android/motion/MotionRecognitionService$3;
.super Lcom/samsung/android/motion/MotionRecognitionSettings;
.source "MotionRecognitionService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/motion/MotionRecognitionService;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionService;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionService;Landroid/content/Context;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # Landroid/os/Handler;

    .prologue
    .line 446
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    invoke-direct {p0, p2, p3}, Lcom/samsung/android/motion/MotionRecognitionSettings;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChangeMotionSettings(II)V
    .locals 8
    .param p1, "motion_sensors"    # I
    .param p2, "motion_events"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 449
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_3

    .line 450
    shl-int v1, v6, v0

    .line 451
    .local v1, "motion_sensor":I
    and-int v2, v1, p1

    if-eqz v2, :cond_1

    .line 452
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V
    invoke-static {v2, v1, v6}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1500(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V

    .line 453
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1600(Lcom/samsung/android/motion/MotionRecognitionService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 454
    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget-boolean v2, v2, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-eqz v2, :cond_0

    .line 455
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/4 v4, 0x1

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V
    invoke-static {v2, v1, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1700(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V

    .line 456
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->refPanningDEnabled:I
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionService;->access$700(Lcom/samsung/android/motion/MotionRecognitionService;)I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionService;->access$800(Lcom/samsung/android/motion/MotionRecognitionService;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 457
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/16 v5, 0x132

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->controlMotionSensor(I)Z
    invoke-static {v4, v5}, Lcom/samsung/android/motion/MotionRecognitionService;->access$900(Lcom/samsung/android/motion/MotionRecognitionService;I)Z

    move-result v4

    # setter for: Lcom/samsung/android/motion/MotionRecognitionService;->mbEnabledPanning:Z
    invoke-static {v2, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->access$802(Lcom/samsung/android/motion/MotionRecognitionService;Z)Z

    .line 460
    :cond_0
    monitor-exit v3

    .line 449
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 460
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 462
    :cond_1
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->useSensor(IZ)V
    invoke-static {v2, v1, v7}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1500(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V

    .line 463
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionService;->mLockEngine:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1600(Lcom/samsung/android/motion/MotionRecognitionService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 464
    :try_start_1
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    iget-boolean v2, v2, Lcom/samsung/android/motion/MotionRecognitionService;->mEngineInitialized:Z

    if-eqz v2, :cond_2

    .line 465
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionService$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionService;

    const/4 v4, 0x0

    # invokes: Lcom/samsung/android/motion/MotionRecognitionService;->enableSensor(IZ)V
    invoke-static {v2, v1, v4}, Lcom/samsung/android/motion/MotionRecognitionService;->access$1700(Lcom/samsung/android/motion/MotionRecognitionService;IZ)V

    .line 467
    :cond_2
    monitor-exit v3

    goto :goto_1

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v2

    .line 474
    .end local v1    # "motion_sensor":I
    :cond_3
    return-void
.end method
