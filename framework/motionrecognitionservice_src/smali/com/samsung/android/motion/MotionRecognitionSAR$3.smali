.class Lcom/samsung/android/motion/MotionRecognitionSAR$3;
.super Ljava/lang/Object;
.source "MotionRecognitionSAR.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionSAR;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionSAR;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 234
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 7
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/16 v6, 0xc8

    const/4 v5, 0x0

    .line 238
    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    .line 240
    .local v1, "sensor":Landroid/hardware/Sensor;
    const-string v2, "ril.backoffstate"

    invoke-static {v2, v5}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 241
    .local v0, "backoffstate":I
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " ril.backoffstate = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    invoke-virtual {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->getPowerConnected()Z

    move-result v2

    if-nez v2, :cond_2

    .line 244
    const-string v2, "MotionRecognitionService"

    const-string v3, "mIsPowerConnected is false"

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v2

    const v3, 0x10018

    if-ne v2, v3, :cond_0

    .line 247
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v5

    float-to-int v3, v3

    int-to-short v3, v3

    # setter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostGripVal:S
    invoke-static {v2, v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$202(Lcom/samsung/android/motion/MotionRecognitionSAR;S)S

    .line 248
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mPostGripVal:S
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$200(Lcom/samsung/android/motion/MotionRecognitionSAR;)S

    move-result v2

    if-lez v2, :cond_1

    .line 250
    and-int/lit8 v0, v0, -0x2

    .line 251
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GRIP_SENSOR far,  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 268
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    or-int/lit8 v0, v0, 0x1

    .line 256
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GRIP_SENSOR close, "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 258
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x64

    invoke-virtual {v3, v4, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 262
    :cond_2
    const-string v2, "MotionRecognitionService"

    const-string v3, "mIsPowerConnected is true. Skip grip sensor event "

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    and-int/lit8 v0, v0, -0x2

    .line 264
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GRIP_SENSOR far,  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 266
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSAR$3;->this$0:Lcom/samsung/android/motion/MotionRecognitionSAR;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionSAR;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionSAR;->access$100(Lcom/samsung/android/motion/MotionRecognitionSAR;)Landroid/os/Handler;

    move-result-object v3

    invoke-virtual {v3, v6, v0, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
