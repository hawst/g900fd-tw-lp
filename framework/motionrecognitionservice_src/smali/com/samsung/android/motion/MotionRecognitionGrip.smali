.class public Lcom/samsung/android/motion/MotionRecognitionGrip;
.super Ljava/lang/Object;
.source "MotionRecognitionGrip.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MotionRecognitionService"

.field private static final TRANSMIT_POWER_DEFAULT:I = 0x0

.field private static final TRANSMIT_POWER_FOR_PROXIMITY:I = 0x2

.field private static final TRANSMIT_POWER_WIFI_HOTSPOT:I = 0x1


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field private mContext:Landroid/content/Context;

.field private mCurrPowerState_flat:Z

.field private mGripEnabled:Z

.field private mIsFlat:Z

.field private mLockGripMotion:Ljava/lang/Object;

.field private mPhoneService:Lcom/android/internal/telephony/ITelephony;

.field private mPostGripVal:S

.field private mPostProxVal:S

.field private mPostProxVal_Flat:Z

.field private mProxEnabled:Z

.field private mProxEnabled_flat:Z

.field private mSContextEnabled_flat:Z

.field private mSContextEnabled_turnover:Z

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field private mWindowManager:Landroid/view/IWindowManager;

.field private final mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

.field private final mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

.field private final sensorGripListener:Landroid/hardware/SensorEventListener;

.field private sensorManager:Landroid/hardware/SensorManager;

.field private final sensorProxListener:Landroid/hardware/SensorEventListener;

.field private final sensorProxListener_Flat:Landroid/hardware/SensorEventListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x5

    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mLockGripMotion:Ljava/lang/Object;

    .line 57
    iput-short v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostGripVal:S

    .line 58
    iput-short v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal:S

    .line 59
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal_Flat:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mCurrPowerState_flat:Z

    .line 61
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mIsFlat:Z

    .line 130
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionGrip$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionGrip$1;-><init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorGripListener:Landroid/hardware/SensorEventListener;

    .line 168
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionGrip$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionGrip$2;-><init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener:Landroid/hardware/SensorEventListener;

    .line 328
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionGrip$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionGrip$3;-><init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    .line 346
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionGrip$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionGrip$4;-><init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    .line 452
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionGrip$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/motion/MotionRecognitionGrip$5;-><init>(Lcom/samsung/android/motion/MotionRecognitionGrip;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    .line 67
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    .line 68
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    .line 69
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mGripEnabled:Z

    .line 70
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled:Z

    .line 71
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled_flat:Z

    .line 72
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_flat:Z

    .line 73
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_turnover:Z

    .line 74
    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mCurrPowerState_flat:Z

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/motion/MotionRecognitionGrip;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-short v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostGripVal:S

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/motion/MotionRecognitionGrip;S)S
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # S

    .prologue
    .line 38
    iput-short p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostGripVal:S

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/android/motion/MotionRecognitionGrip;)Lcom/android/internal/telephony/ITelephony;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/android/motion/MotionRecognitionGrip;)S
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-short v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal:S

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/motion/MotionRecognitionGrip;S)S
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # S

    .prologue
    .line 38
    iput-short p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal:S

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/motion/MotionRecognitionGrip;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setProximity_flat(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/motion/MotionRecognitionGrip;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setPowerState_flat()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/android/motion/MotionRecognitionGrip;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setFlat(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/motion/MotionRecognitionGrip;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled_flat:Z

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/android/motion/MotionRecognitionGrip;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled_flat:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/android/motion/MotionRecognitionGrip;)Landroid/hardware/SensorEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/android/motion/MotionRecognitionGrip;)Landroid/hardware/SensorManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/android/motion/MotionRecognitionGrip;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionGrip;
    .param p1, "x1"    # Z

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setPowerState_TurnOver(Z)V

    return-void
.end method

.method private isSpeakerOn()Z
    .locals 2

    .prologue
    .line 417
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mAudioManager:Landroid/media/AudioManager;

    if-nez v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mAudioManager:Landroid/media/AudioManager;

    .line 419
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    .line 420
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isSpeakerphoneOn()Z

    move-result v0

    .line 421
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFlat(Z)V
    .locals 3
    .param p1, "flat"    # Z

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mIsFlat:Z

    .line 325
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setFlat : flat motion event = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mIsFlat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    return-void
.end method

.method private setPowerState_TurnOver(Z)V
    .locals 4
    .param p1, "action"    # Z

    .prologue
    .line 425
    const-string v1, "ril.backoffstate"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 426
    .local v0, "backoffstate":I
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "backoffstate = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    if-nez p1, :cond_1

    .line 429
    and-int/lit8 v0, v0, -0x2

    .line 430
    const-string v1, "MotionRecognitionService"

    const-string v2, "POWER_DEFAULT "

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1, v0}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 448
    :cond_0
    :goto_0
    return-void

    .line 436
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionGrip;->isCoverEquipment()Z

    move-result v1

    if-nez v1, :cond_0

    .line 437
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionGrip;->isSpeakerOn()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 438
    const-string v1, "MotionRecognitionService"

    const-string v2, "skip : speaker on"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 441
    :cond_2
    or-int/lit8 v0, v0, 0x1

    .line 442
    const-string v1, "MotionRecognitionService"

    const-string v2, "POWER_BACKOFF"

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    :try_start_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v1, v0}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 445
    :catch_0
    move-exception v1

    goto :goto_0

    .line 433
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private setPowerState_flat()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 284
    const-string v3, "ril.backoffstate"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 285
    .local v0, "backoffstate":I
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "backoffstate = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal_Flat:Z

    iget-boolean v4, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mIsFlat:Z

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 288
    .local v2, "status":Z
    :cond_0
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " prox : flat  =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal_Flat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mIsFlat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " last status =  "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mCurrPowerState_flat:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    iget-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mCurrPowerState_flat:Z

    if-eq v3, v2, :cond_3

    .line 292
    if-nez v2, :cond_2

    .line 293
    and-int/lit8 v0, v0, -0x2

    .line 294
    const-string v3, "MotionRecognitionService"

    const-string v4, "POWER_DEFAULT "

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :try_start_0
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3, v0}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :goto_0
    iput-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mCurrPowerState_flat:Z

    .line 315
    :cond_1
    return v2

    .line 297
    :catch_0
    move-exception v1

    .line 298
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "MotionRecognitionService"

    const-string v4, "RemoteException 1"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionGrip;->isCoverEquipment()Z

    move-result v3

    if-nez v3, :cond_1

    .line 303
    or-int/lit8 v0, v0, 0x1

    .line 304
    const-string v3, "MotionRecognitionService"

    const-string v4, "POWER_BACKOFF"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    invoke-interface {v3, v0}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 307
    :catch_1
    move-exception v1

    .line 308
    .restart local v1    # "e":Landroid/os/RemoteException;
    const-string v3, "MotionRecognitionService"

    const-string v4, "RemoteException 2"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 312
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_3
    const-string v3, "MotionRecognitionService"

    const-string v4, "PowerState is not changed"

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setProximity_flat(Z)V
    .locals 3
    .param p1, "proxival"    # Z

    .prologue
    .line 319
    iput-boolean p1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal_Flat:Z

    .line 320
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProximity_flat : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPostProxVal_Flat:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    return-void
.end method


# virtual methods
.method public disableFlatMotion()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 230
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_flat:Z

    if-ne v0, v4, :cond_0

    .line 231
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 232
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_flat:Z

    .line 235
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled_flat:Z

    if-ne v0, v4, :cond_1

    .line 236
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener_Flat:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 237
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled_flat:Z

    .line 240
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setProximity_flat(Z)V

    .line 241
    invoke-direct {p0, v3}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setFlat(Z)V

    .line 242
    invoke-direct {p0}, Lcom/samsung/android/motion/MotionRecognitionGrip;->setPowerState_flat()Z

    .line 243
    return v3
.end method

.method public disableGripSensor()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 91
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mGripEnabled:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 92
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorGripListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 93
    iput-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mGripEnabled:Z

    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_0
    :goto_0
    return v2

    .line 97
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disableProxSensor()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener:Landroid/hardware/SensorEventListener;

    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    .line 119
    iput-boolean v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled:Z

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/internal/telephony/ITelephony;->setTransmitPower(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    :cond_0
    :goto_0
    return v2

    .line 123
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public disableTurnOverMotion()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 409
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_turnover:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 411
    iput-boolean v3, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_turnover:Z

    .line 413
    :cond_0
    return v3
.end method

.method public enableFlatMotion()I
    .locals 4

    .prologue
    .line 208
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 209
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_1

    .line 212
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mWindowManager:Landroid/view/IWindowManager;

    .line 214
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 215
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 216
    .local v0, "tmpPM":Landroid/content/pm/PackageManager;
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 217
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v1, :cond_2

    .line 218
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 220
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_flat:Z

    if-nez v1, :cond_3

    .line 221
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexFlatMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 222
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_flat:Z

    .line 226
    .end local v0    # "tmpPM":Landroid/content/pm/PackageManager;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public enableGripSensor()I
    .locals 4

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 79
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 81
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mGripEnabled:Z

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorGripListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    const v3, 0x10018

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mGripEnabled:Z

    .line 87
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public enableProxSensor()I
    .locals 4

    .prologue
    .line 104
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v0, :cond_0

    .line 105
    const-string v0, "phone"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 107
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled:Z

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorProxListener:Landroid/hardware/SensorEventListener;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->sensorManager:Landroid/hardware/SensorManager;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mProxEnabled:Z

    .line 113
    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public enableTurnOverMotion()I
    .locals 4

    .prologue
    .line 387
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    if-nez v1, :cond_0

    .line 388
    const-string v1, "phone"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mPhoneService:Lcom/android/internal/telephony/ITelephony;

    .line 390
    :cond_0
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mWindowManager:Landroid/view/IWindowManager;

    if-nez v1, :cond_1

    .line 391
    const-string v1, "window"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mWindowManager:Landroid/view/IWindowManager;

    .line 393
    :cond_1
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    if-eqz v1, :cond_3

    .line 394
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 395
    .local v0, "tmpPM":Landroid/content/pm/PackageManager;
    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 396
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v1, :cond_2

    .line 397
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mContext:Landroid/content/Context;

    const-string v2, "scontext"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/scontext/SContextManager;

    iput-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 399
    :cond_2
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_turnover:Z

    if-nez v1, :cond_3

    .line 400
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mySContexTurnOverMotionListener:Landroid/hardware/scontext/SContextListener;

    const/16 v3, 0x16

    invoke-virtual {v1, v2, v3}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 401
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mSContextEnabled_turnover:Z

    .line 405
    .end local v0    # "tmpPM":Landroid/content/pm/PackageManager;
    :cond_3
    const/4 v1, 0x0

    return v1
.end method

.method public isCoverEquipment()Z
    .locals 9

    .prologue
    .line 247
    const/4 v5, 0x0

    .line 248
    .local v5, "isCover":Z
    const/4 v3, 0x0

    .line 249
    .local v3, "fis":Ljava/io/FileInputStream;
    const/16 v0, 0x32

    .line 251
    .local v0, "data":I
    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionGrip;->mLockGripMotion:Ljava/lang/Object;

    monitor-enter v7

    .line 254
    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    const-string v6, "/sys/bus/w1/devices/w1_bus_master1/w1_master_check_id"

    invoke-direct {v4, v6}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    move-object v3, v4

    .line 259
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_0
    if-nez v3, :cond_1

    .line 260
    :try_start_1
    const-string v6, "MotionRecognitionService"

    const-string v8, "Output file is null!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 277
    :goto_1
    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 278
    const-string v6, "MotionRecognitionService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " cover status =  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const/16 v6, 0x32

    if-ne v0, v6, :cond_2

    const/4 v5, 0x0

    .line 280
    :goto_2
    return v5

    .line 255
    :catch_0
    move-exception v1

    .line 256
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_3
    const-string v6, "MotionRecognitionService"

    const-string v8, "File not found!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 266
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    :catch_1
    move-exception v1

    .line 267
    .local v1, "e":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 268
    if-eqz v3, :cond_0

    .line 270
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 275
    :cond_0
    :goto_3
    :try_start_6
    const-string v6, "MotionRecognitionService"

    const-string v8, "File read fail!!"

    invoke-static {v6, v8}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 277
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    throw v6

    .line 262
    :cond_1
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->read()I

    move-result v0

    .line 264
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_1

    .line 271
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 272
    .local v2, "err":Ljava/lang/Exception;
    :try_start_8
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_3

    .line 279
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "err":Ljava/lang/Exception;
    :cond_2
    const/4 v5, 0x1

    goto :goto_2
.end method
