.class public abstract Lcom/samsung/android/motion/MotionRecognitionSettings;
.super Ljava/lang/Object;
.source "MotionRecognitionSettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;
    }
.end annotation


# static fields
.field public static final CONFIG_READ_SETTINGS:Z = true

.field public static final PANNING_ICON:I = 0x1

.field public static final PANNING_IMAGE:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MotionRecognitionService"


# instance fields
.field private mCurrentEnabledMotions:I

.field private mCurrentMotionSettings:I

.field private final mLock:Ljava/lang/Object;

.field private mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

.field private mRegisteredMotions:I

.field private mRegisteredMotionsforOpen:I

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    .line 44
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 45
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mLock:Ljava/lang/Object;

    .line 47
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    .line 48
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    .line 53
    new-instance v0, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-direct {v0, p0, p2}, Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;-><init>(Lcom/samsung/android/motion/MotionRecognitionSettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    .line 55
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_engine"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 58
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_shake"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 60
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 62
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_overturn"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 64
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_merged_mute_pause"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 66
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_zooming"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 68
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_tilt_to_list_scrolling"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 70
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pan_to_browse_image"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 73
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_panning"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 75
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_double_tap"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 77
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_unlock_camera_short_cut"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 79
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_pick_up_to_call_out"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 81
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "lock_motion_tilt_to_unlock"

    invoke-static {v1}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 83
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "smart_scroll"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 88
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_peek_view_albums_list"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 90
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "motion_peek_chapter_preview"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 94
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "master_arc_motion"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 97
    iget-object v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v1, "master_side_motion"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mMotionSettingsObserver:Lcom/samsung/android/motion/MotionRecognitionSettings$MotionSettingsObserver;

    invoke-virtual {v0, v1, v3, v2, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;I)V

    .line 101
    invoke-virtual {p0}, Lcom/samsung/android/motion/MotionRecognitionSettings;->updateCurrentSettings()V

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/motion/MotionRecognitionSettings;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSettings;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/android/motion/MotionRecognitionSettings;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/motion/MotionRecognitionSettings;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    return p1
.end method

.method private getregisteredMotionforEvents(I)I
    .locals 2
    .param p1, "motion"    # I

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    .local v0, "panningEvent":Z
    const/4 v1, 0x0

    .line 372
    .local v1, "registeredEvents":I
    sparse-switch p1, :sswitch_data_0

    .line 414
    :goto_0
    if-eqz v0, :cond_0

    .line 415
    const v1, 0x80060

    .line 419
    :cond_0
    return v1

    .line 373
    :sswitch_0
    const/16 v1, 0x8

    goto :goto_0

    .line 374
    :sswitch_1
    const/4 v1, 0x1

    goto :goto_0

    .line 376
    :sswitch_2
    const/4 v1, 0x2

    goto :goto_0

    .line 377
    :sswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 378
    :sswitch_4
    const/16 v1, 0x10

    goto :goto_0

    .line 379
    :sswitch_5
    const/4 v1, 0x4

    goto :goto_0

    .line 380
    :sswitch_6
    const v1, 0x8000

    goto :goto_0

    .line 383
    :sswitch_7
    const/16 v1, 0x400

    goto :goto_0

    .line 399
    :sswitch_8
    const/16 v1, 0x5810

    .line 401
    goto :goto_0

    .line 409
    :sswitch_9
    const/high16 v1, 0x80000

    goto :goto_0

    .line 372
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xa -> :sswitch_1
        0x23 -> :sswitch_2
        0x24 -> :sswitch_2
        0x3d -> :sswitch_3
        0x43 -> :sswitch_5
        0x44 -> :sswitch_7
        0x48 -> :sswitch_4
        0x4e -> :sswitch_8
        0x4f -> :sswitch_8
        0x50 -> :sswitch_8
        0x51 -> :sswitch_8
        0x52 -> :sswitch_8
        0x53 -> :sswitch_8
        0x54 -> :sswitch_8
        0x55 -> :sswitch_8
        0x57 -> :sswitch_8
        0x58 -> :sswitch_8
        0x59 -> :sswitch_8
        0x5a -> :sswitch_8
        0x5b -> :sswitch_8
        0x5c -> :sswitch_8
        0x5d -> :sswitch_8
        0x62 -> :sswitch_6
        0x65 -> :sswitch_7
        0x66 -> :sswitch_7
        0x67 -> :sswitch_9
        0x68 -> :sswitch_9
        0x69 -> :sswitch_9
        0x6a -> :sswitch_9
        0x6b -> :sswitch_9
        0x6c -> :sswitch_9
        0x6d -> :sswitch_9
        0x6e -> :sswitch_9
    .end sparse-switch
.end method


# virtual methods
.method public getSettingMotionSensitivity(II)I
    .locals 4
    .param p1, "motion"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v1, 0x5

    .line 201
    const/4 v0, 0x0

    .line 202
    .local v0, "setting":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 211
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 212
    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const/4 v3, -0x2

    invoke-static {v2, v0, v1, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    .line 215
    :cond_1
    return v1

    .line 203
    :sswitch_0
    const-string v0, "motion_zooming_sensitivity"

    goto :goto_0

    .line 205
    :sswitch_1
    const/4 v2, 0x1

    if-ne p2, v2, :cond_2

    const-string v0, "motion_panning_sensitivity"

    goto :goto_0

    .line 206
    :cond_2
    const/4 v2, 0x2

    if-ne p2, v2, :cond_0

    const-string v0, "motion_pan_to_browse_image_sensitivity"

    goto :goto_0

    .line 202
    :sswitch_data_0
    .sparse-switch
        0x3d -> :sswitch_1
        0x48 -> :sswitch_0
    .end sparse-switch
.end method

.method public getUsedSensorForEvents()I
    .locals 4

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 137
    .local v0, "motion_sensors":I
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Acquire sensors : Settings = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    invoke-static {v3}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    and-int/2addr v1, v2

    const/high16 v2, 0x80000

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 140
    or-int/lit8 v0, v0, 0x3

    .line 143
    :cond_0
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    const/high16 v2, 0x200000

    and-int/2addr v1, v2

    if-eqz v1, :cond_3

    .line 146
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    and-int/2addr v1, v2

    const v2, 0x13fffb

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    .line 166
    or-int/lit8 v0, v0, 0x1

    .line 170
    :cond_1
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    and-int/2addr v1, v2

    const v2, 0x105ff9

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 184
    or-int/lit8 v0, v0, 0x2

    .line 188
    :cond_2
    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    and-int/2addr v1, v2

    const v2, 0x100c00

    and-int/2addr v1, v2

    if-eqz v1, :cond_3

    .line 192
    or-int/lit8 v0, v0, 0x4

    .line 195
    :cond_3
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Acquired sensors for motion : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    return v0
.end method

.method public isActivatedEvent(I)Z
    .locals 4
    .param p1, "motion"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 425
    const-string v3, "debug.motion.enabled"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-ne v3, v1, :cond_1

    .line 434
    :cond_0
    :goto_0
    return v1

    .line 428
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/motion/MotionRecognitionSettings;->getregisteredMotionforEvents(I)I

    move-result v0

    .line 430
    .local v0, "registeredEvents":I
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    and-int/2addr v3, v0

    if-nez v3, :cond_0

    move v1, v2

    .line 431
    goto :goto_0
.end method

.method public isActivatedMotion(I)Z
    .locals 1
    .param p1, "motionevent"    # I

    .prologue
    .line 440
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 441
    const/4 v0, 0x1

    .line 443
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMotionEngineEnabled()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 353
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    const/high16 v4, 0x200000

    and-int/2addr v3, v4

    if-nez v3, :cond_0

    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    const/high16 v4, 0x80000

    and-int/2addr v3, v4

    if-eqz v3, :cond_2

    :cond_0
    const/4 v1, 0x1

    .line 357
    .local v1, "isEnabled":Z
    :goto_0
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "  .isMotionEngineEnabled : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    const-string v3, "debug.motion.enabled"

    invoke-static {v3, v2}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 359
    .local v0, "debugEnabled":Z
    if-eqz v0, :cond_1

    .line 360
    const-string v2, "MotionRecognitionService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  .isMotionEngineEnabled : isEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ",  debugEnabled="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    move v1, v0

    .line 364
    :cond_1
    return v1

    .end local v0    # "debugEnabled":Z
    .end local v1    # "isEnabled":Z
    :cond_2
    move v1, v2

    .line 353
    goto :goto_0
.end method

.method public isOpenAPIMotion(I)Z
    .locals 1
    .param p1, "motionevent"    # I

    .prologue
    .line 450
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 451
    const/4 v0, 0x1

    .line 453
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSettingMotion(I)Z
    .locals 1
    .param p1, "motionevent"    # I

    .prologue
    .line 460
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 461
    const/4 v0, 0x1

    .line 463
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract onChangeMotionSettings(II)V
.end method

.method public setRegisteredEvents(I)V
    .locals 0
    .param p1, "registeredmotions"    # I

    .prologue
    .line 110
    iput p1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    .line 111
    return-void
.end method

.method public setRegisteredEvents(II)V
    .locals 3
    .param p1, "registeredmotions"    # I
    .param p2, "registeredmotionsforopen"    # I

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotions:I

    .line 116
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    if-eq v0, p2, :cond_0

    .line 117
    iput p2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    .line 118
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    .line 119
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    if-eqz v0, :cond_1

    .line 120
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    const/high16 v1, 0x200000

    or-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    .line 124
    :goto_0
    const-string v0, "MotionRecognitionService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "events for open api = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    invoke-static {v2}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_0
    return-void

    .line 122
    :cond_1
    iget v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    iget v1, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    goto :goto_0
.end method

.method public updateCurrentSettings()V
    .locals 12

    .prologue
    const/high16 v11, 0x200000

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 247
    iget-object v6, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mLock:Ljava/lang/Object;

    monitor-enter v6

    .line 249
    const/4 v3, 0x0

    :try_start_0
    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 253
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v3, 0x15

    if-ge v0, v3, :cond_16

    .line 254
    shl-int v1, v4, v0

    .line 256
    .local v1, "motion_event":I
    const/high16 v3, 0x80000

    if-ne v1, v3, :cond_4

    .line 258
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "smart_scroll"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v3, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eq v3, v4, :cond_1

    .line 253
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 260
    :cond_1
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    shl-int v7, v4, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 335
    :cond_2
    :goto_2
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v7, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v3, v7}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "motion_engine"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v3, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 337
    :cond_3
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    or-int/2addr v3, v11

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto :goto_1

    .line 347
    .end local v0    # "i":I
    .end local v1    # "motion_event":I
    :catchall_0
    move-exception v3

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 261
    .restart local v0    # "i":I
    .restart local v1    # "motion_event":I
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v7, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v3, v7}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "motion_engine"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v3, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-ne v3, v4, :cond_2

    .line 263
    :cond_5
    sparse-switch v1, :sswitch_data_0

    goto :goto_2

    .line 266
    :sswitch_0
    const/4 v2, 0x0

    .line 267
    .local v2, "result":I
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v3

    const-string v7, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v3, v7}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 268
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "motion_merged_mute_pause"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v3, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    iget-object v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_overturn"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v7, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v7

    and-int v2, v3, v7

    .line 270
    const-string v3, "MotionRecognitionService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Palm motion, result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " motion_event =  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_3
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    if-eqz v2, :cond_7

    move v3, v4

    :goto_4
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    .line 272
    :cond_6
    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v7, "motion_overturn"

    const/4 v8, 0x0

    const/4 v9, -0x2

    invoke-static {v3, v7, v8, v9}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v2

    .line 273
    const-string v3, "MotionRecognitionService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Motion,  result = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_7
    move v3, v5

    .line 276
    goto :goto_4

    .line 279
    .end local v2    # "result":I
    :sswitch_1
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_shake"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    :goto_5
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_8
    move v3, v5

    goto :goto_5

    .line 282
    :sswitch_2
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_double_tap"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_9

    move v3, v4

    :goto_6
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_9
    move v3, v5

    goto :goto_6

    .line 287
    :sswitch_3
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_zooming"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_a

    move v3, v4

    :goto_7
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 288
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_tilt_to_list_scrolling"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_b

    move v3, v4

    :goto_8
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_a
    move v3, v5

    .line 287
    goto :goto_7

    :cond_b
    move v3, v5

    .line 288
    goto :goto_8

    .line 291
    :sswitch_4
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_pan_to_browse_image"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_c

    move v3, v4

    :goto_9
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 293
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_peek_view_albums_list"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_d

    move v3, v4

    :goto_a
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 294
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_peek_chapter_preview"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_e

    move v3, v4

    :goto_b
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_c
    move v3, v5

    .line 291
    goto :goto_9

    :cond_d
    move v3, v5

    .line 293
    goto :goto_a

    :cond_e
    move v3, v5

    .line 294
    goto :goto_b

    .line 298
    :sswitch_5
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_panning"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_f

    move v3, v4

    :goto_c
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 300
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_peek_view_albums_list"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_10

    move v3, v4

    :goto_d
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    .line 301
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_peek_chapter_preview"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_11

    move v3, v4

    :goto_e
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_f
    move v3, v5

    .line 298
    goto :goto_c

    :cond_10
    move v3, v5

    .line 300
    goto :goto_d

    :cond_11
    move v3, v5

    .line 301
    goto :goto_e

    .line 306
    :sswitch_6
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_unlock_camera_short_cut"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_12

    move v3, v4

    :goto_f
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_12
    move v3, v5

    goto :goto_f

    .line 310
    :sswitch_7
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_pick_up_to_call_out"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_13

    move v3, v4

    :goto_10
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_13
    move v3, v5

    goto :goto_10

    .line 313
    :sswitch_8
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "lock_motion_tilt_to_unlock"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_14

    move v3, v4

    :goto_11
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_14
    move v3, v5

    goto :goto_11

    .line 317
    :sswitch_9
    iget v7, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mResolver:Landroid/content/ContentResolver;

    const-string v8, "motion_pick_up"

    const/4 v9, 0x0

    const/4 v10, -0x2

    invoke-static {v3, v8, v9, v10}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v3

    if-eqz v3, :cond_15

    move v3, v4

    :goto_12
    shl-int/2addr v3, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    :cond_15
    move v3, v5

    goto :goto_12

    .line 323
    :sswitch_a
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    shl-int v7, v5, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    .line 327
    :sswitch_b
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    shl-int v7, v4, v0

    or-int/2addr v3, v7

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    goto/16 :goto_2

    .line 341
    .end local v1    # "motion_event":I
    :cond_16
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Settings updated : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    invoke-static {v5}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    .line 343
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    if-eqz v3, :cond_17

    .line 344
    iget v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentMotionSettings:I

    iget v4, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mRegisteredMotionsforOpen:I

    or-int/2addr v3, v4

    or-int/2addr v3, v11

    iput v3, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    .line 345
    const-string v3, "MotionRecognitionService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Open API is using motion : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/motion/MotionRecognitionSettings;->mCurrentEnabledMotions:I

    invoke-static {v5}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    :cond_17
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 348
    return-void

    .line 263
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_9
        0x8 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
        0x40 -> :sswitch_5
        0x80 -> :sswitch_6
        0x100 -> :sswitch_6
        0x200 -> :sswitch_a
        0x400 -> :sswitch_7
        0x800 -> :sswitch_8
        0x1000 -> :sswitch_3
        0x2000 -> :sswitch_a
        0x4000 -> :sswitch_3
        0x8000 -> :sswitch_9
        0x10000 -> :sswitch_a
        0x20000 -> :sswitch_0
        0x40000 -> :sswitch_7
        0x100000 -> :sswitch_b
    .end sparse-switch
.end method
