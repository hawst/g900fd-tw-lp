.class Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;
.super Ljava/lang/Object;
.source "MotionRecognitionFlipCover.java"

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/motion/MotionRecognitionFlipCover;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;


# direct methods
.method constructor <init>(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0
    .param p1, "sensor"    # Landroid/hardware/Sensor;
    .param p2, "accuracy"    # I

    .prologue
    .line 177
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 6
    .param p1, "event"    # Landroid/hardware/SensorEvent;

    .prologue
    const/4 v5, 0x0

    .line 180
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    .line 184
    .local v0, "sensor":Landroid/hardware/Sensor;
    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 186
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v2, v2, v5

    float-to-int v2, v2

    # setter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mPostLightVal:I
    invoke-static {v1, v2}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$002(Lcom/samsung/android/motion/MotionRecognitionFlipCover;I)I

    .line 187
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mPostLightVal:I
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$000(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)I

    move-result v1

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    .line 189
    const-string v1, "MotionRecognitionService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "light val = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mPostLightVal:I
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$000(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mProxEnabled:Z
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$100(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 191
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->sensorManager:Landroid/hardware/SensorManager;
    invoke-static {v1}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$300(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)Landroid/hardware/SensorManager;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->sensorProxListener:Landroid/hardware/SensorEventListener;
    invoke-static {v2}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$200(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)Landroid/hardware/SensorEventListener;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->sensorManager:Landroid/hardware/SensorManager;
    invoke-static {v3}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$300(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)Landroid/hardware/SensorManager;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # getter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->sensorManager:Landroid/hardware/SensorManager;
    invoke-static {v4}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$300(Lcom/samsung/android/motion/MotionRecognitionFlipCover;)Landroid/hardware/SensorManager;

    const/4 v4, 0x3

    invoke-virtual {v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    .line 192
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mProxEnabled:Z
    invoke-static {v1, v2}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$102(Lcom/samsung/android/motion/MotionRecognitionFlipCover;Z)Z

    .line 193
    iget-object v1, p0, Lcom/samsung/android/motion/MotionRecognitionFlipCover$2;->this$0:Lcom/samsung/android/motion/MotionRecognitionFlipCover;

    # setter for: Lcom/samsung/android/motion/MotionRecognitionFlipCover;->mProxClosed:Z
    invoke-static {v1, v5}, Lcom/samsung/android/motion/MotionRecognitionFlipCover;->access$402(Lcom/samsung/android/motion/MotionRecognitionFlipCover;Z)Z

    .line 194
    const-string v1, "MotionRecognitionService"

    const-string v2, " Proximity Reg "

    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    return-void
.end method
