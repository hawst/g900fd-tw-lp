.class public Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;
.super Lcom/android/internal/policy/impl/MultiPhoneWindow$PenWindowController;
.source "MultiPhoneWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/MultiPhoneWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "TitleBarController"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;


# direct methods
.method protected constructor <init>(Lcom/android/internal/policy/impl/MultiPhoneWindow;)V
    .locals 0

    .prologue
    .line 3311
    iput-object p1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/MultiPhoneWindow$PenWindowController;-><init>(Lcom/android/internal/policy/impl/MultiPhoneWindow;)V

    return-void
.end method


# virtual methods
.method protected performInflateController()V
    .locals 3

    .prologue
    .line 3314
    iget-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    # getter for: Lcom/android/internal/policy/impl/MultiPhoneWindow;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/internal/policy/impl/MultiPhoneWindow;->access$600(Lcom/android/internal/policy/impl/MultiPhoneWindow;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 3315
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x109008c

    iget-object v2, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    # getter for: Lcom/android/internal/policy/impl/MultiPhoneWindow;->mContentRootContainer:Lcom/android/internal/policy/impl/MultiPhoneWindow$ContentRootContainer;
    invoke-static {v2}, Lcom/android/internal/policy/impl/MultiPhoneWindow;->access$1700(Lcom/android/internal/policy/impl/MultiPhoneWindow;)Lcom/android/internal/policy/impl/MultiPhoneWindow$ContentRootContainer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 3316
    iget-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    # getter for: Lcom/android/internal/policy/impl/MultiPhoneWindow;->mContentRootContainer:Lcom/android/internal/policy/impl/MultiPhoneWindow$ContentRootContainer;
    invoke-static {v1}, Lcom/android/internal/policy/impl/MultiPhoneWindow;->access$1700(Lcom/android/internal/policy/impl/MultiPhoneWindow;)Lcom/android/internal/policy/impl/MultiPhoneWindow$ContentRootContainer;

    move-result-object v1

    const v2, 0x10203d8

    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/MultiPhoneWindow$ContentRootContainer;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    .line 3317
    iget-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    iput-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mMenuContainer:Landroid/view/View;

    .line 3319
    iget-object v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    # getter for: Lcom/android/internal/policy/impl/MultiPhoneWindow;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/internal/policy/impl/MultiPhoneWindow;->access$600(Lcom/android/internal/policy/impl/MultiPhoneWindow;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10501b9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mControllerHeight:I

    .line 3320
    return-void
.end method

.method protected performUpdateBackground()V
    .locals 2

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->this$0:Lcom/android/internal/policy/impl/MultiPhoneWindow;

    # getter for: Lcom/android/internal/policy/impl/MultiPhoneWindow;->mHasStackFocus:Z
    invoke-static {v0}, Lcom/android/internal/policy/impl/MultiPhoneWindow;->access$6400(Lcom/android/internal/policy/impl/MultiPhoneWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3338
    iget-object v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    const v1, 0x108063f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 3342
    :goto_0
    return-void

    .line 3340
    :cond_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    const v1, 0x1080640

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected performUpdateVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 3324
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mIsShowing:Z

    if-ne v0, p1, :cond_0

    .line 3333
    :goto_0
    return-void

    .line 3327
    :cond_0
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mIsShowing:Z

    .line 3328
    if-eqz p1, :cond_1

    .line 3329
    iget-object v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 3331
    :cond_1
    iget-object v0, p0, Lcom/android/internal/policy/impl/MultiPhoneWindow$TitleBarController;->mPenWindowHeader:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateAvailableButtons()V
    .locals 0

    .prologue
    .line 3347
    return-void
.end method
