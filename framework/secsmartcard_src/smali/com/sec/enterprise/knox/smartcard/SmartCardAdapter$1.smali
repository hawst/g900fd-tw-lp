.class Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;
.super Ljava/lang/Object;
.source "SmartCardAdapter.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->bindPinService(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;


# direct methods
.method constructor <init>(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    new-instance v1, Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    invoke-direct {v1, p2}, Lcom/sec/smartcard/pinservice/SmartCardPinManager;-><init>(Landroid/os/IBinder;)V

    # setter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$202(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Lcom/sec/smartcard/pinservice/SmartCardPinManager;)Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    .line 186
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # getter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$100(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # getter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$100(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # getter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;
    invoke-static {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$200(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;->onServiceBound(Lcom/sec/smartcard/pinservice/SmartCardPinManager;)V

    .line 190
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    const/4 v1, 0x0

    .line 175
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # setter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$002(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;

    .line 176
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # getter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$100(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # getter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    invoke-static {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$100(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;->onServiceUnbound()V

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # setter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$102(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    .line 180
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;->this$0:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    # setter for: Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;
    invoke-static {v0, v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->access$202(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Lcom/sec/smartcard/pinservice/SmartCardPinManager;)Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    .line 181
    return-void
.end method
