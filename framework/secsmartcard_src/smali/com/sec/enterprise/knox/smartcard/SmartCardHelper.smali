.class public Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
.super Lcom/sec/smartcard/client/SmartCardHelper;
.source "SmartCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;
    }
.end annotation


# static fields
.field public static final ERROR_CANCELLED_BY_USER:I = 0x4

.field public static final ERROR_CONNECTION_BUSY:I = 0x9

.field public static final ERROR_DEVICE_NOT_CONFIGURED:I = 0x6

.field public static final ERROR_INTERNAL:I = 0xa

.field public static final ERROR_INVALID_PARAMETER:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NO_CONNECTION:I = 0x5

.field public static final ERROR_SECURITY_VIOLATION:I = 0x3

.field public static final ERROR_SERVICE_NOT_PRESENT:I = 0x7

.field public static final ERROR_SERVICE_NOT_READY:I = 0x8

.field public static final ERROR_UNKNOWN:I = 0x1

.field public static final SMARTCARD_SERVICE_VERSION_CODE:I = 0x3

.field static final TAG:Ljava/lang/String; = "SmartCardHelperKNOX"

.field private static mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/smartcard/client/SmartCardHelper;-><init>()V

    .line 112
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 135
    const-class v1, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mContext:Landroid/content/Context;

    .line 137
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    .line 139
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static getPassCode()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 363
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "SmartCardHelper: got call from native!!!!!!!!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 364
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v2, :cond_0

    .line 366
    const-string v2, "SmartCardHelperKNOX"

    const-string v3, "mSmartCardHelper is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    .local v0, "tempPin":[C
    :goto_0
    return-object v1

    .line 370
    .end local v0    # "tempPin":[C
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getPinSync()[C

    move-result-object v0

    .line 372
    .restart local v0    # "tempPin":[C
    if-eqz v0, :cond_1

    .line 374
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    goto :goto_0

    .line 376
    :cond_1
    const-string v2, "SmartCardHelperKNOX"

    const-string v3, "pin is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getPkcs11FunctionName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 437
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "SmartCardHelper: got call from native!!!!!!!!! getPkcs11FunctionName"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 440
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v0, :cond_0

    .line 442
    const-string v0, "SmartCardHelperKNOX"

    const-string v1, "getPkcs11FunctionName C_GetFunctionList"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const-string v0, "C_GetFunctionList"

    .line 446
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getPkcs11externalFunctionName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getPkcs11LibName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 418
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "SmartCardHelper: got call from native!!!!!!!!! getPkcs11LibName"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 421
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v0, :cond_0

    .line 423
    const-string v0, "SmartCardHelperKNOX"

    const-string v1, "getPkcs11LibName libSamsungPkcs11Wrapper.so"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    const-string v0, "libSamsungPkcs11Wrapper.so"

    .line 427
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getPkcs11LibraryName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static getSlotID()J
    .locals 4

    .prologue
    .line 288
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "SmartCardHelper: got call from native!!!!!!!!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 289
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v2, :cond_0

    .line 291
    const-string v2, "SmartCardHelperKNOX"

    const-string v3, "mSmartCardHelper is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const-wide/16 v0, -0x1

    .line 296
    .local v0, "slot_id":J
    :goto_0
    return-wide v0

    .line 295
    .end local v0    # "slot_id":J
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v2}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getSlotID_super()J

    move-result-wide v0

    .line 296
    .restart local v0    # "slot_id":J
    goto :goto_0
.end method

.method public static getSlotID(Ljava/lang/String;)J
    .locals 4
    .param p0, "alias"    # Ljava/lang/String;

    .prologue
    .line 309
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "SmartCardHelper: getSlotID got call from native!!!!!!!!!"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 310
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v2, :cond_0

    .line 312
    const-string v2, "SmartCardHelperKNOX"

    const-string v3, "mSmartCardHelper is null"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    const-wide/16 v0, -0x1

    .line 317
    :goto_0
    return-wide v0

    .line 316
    :cond_0
    sget-object v2, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v2, p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->getSlotIDwithAlias(Ljava/lang/String;)J

    move-result-wide v0

    .line 317
    .local v0, "slot_id":J
    goto :goto_0
.end method

.method public static isCCMEnabled()Z
    .locals 4

    .prologue
    .line 331
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "SmartCardHelper: isCCMEnabled got call from native!!!!!!!!!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 332
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    if-nez v1, :cond_0

    .line 334
    const-string v1, "SmartCardHelperKNOX"

    const-string v2, "mSmartCardHelper is null"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    const/4 v1, 0x0

    .line 340
    .local v0, "retval":Ljava/lang/Boolean;
    :goto_0
    return v1

    .line 338
    .end local v0    # "retval":Ljava/lang/Boolean;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->mSmartCardHelper:Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/knox/smartcard/SmartCardHelper;->isCCMFeatureEnabled()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 339
    .restart local v0    # "retval":Ljava/lang/Boolean;
    const-string v1, "SmartCardHelperKNOX"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCCMEnabled is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 340
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public deinitialize()V
    .locals 0

    .prologue
    .line 198
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->deinitialize()V

    .line 199
    return-void
.end method

.method public finalize()V
    .locals 0

    .prologue
    .line 187
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->deinitialize()V

    .line 188
    return-void
.end method

.method public getPin(Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;

    .prologue
    .line 277
    invoke-super {p0, p1}, Lcom/sec/smartcard/client/SmartCardHelper;->getPin(Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;)V

    .line 278
    return-void
.end method

.method public getSmartCardId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->getSmartCardId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public initialize(Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;)V
    .locals 0
    .param p1, "cb"    # Lcom/sec/enterprise/knox/smartcard/SmartCardHelper$Callback;

    .prologue
    .line 175
    invoke-super {p0, p1}, Lcom/sec/smartcard/client/SmartCardHelper;->initialize(Lcom/sec/smartcard/client/SmartCardHelper$Callback;)V

    .line 176
    return-void
.end method

.method public registerOpenSSLEngine()Z
    .locals 1

    .prologue
    .line 397
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->registerSSLEngine()Z

    move-result v0

    return v0
.end method

.method public registerProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->registerProvider()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public unregisterOpenSSLEngine()Z
    .locals 1

    .prologue
    .line 410
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->unregisterSSLEngine()Z

    move-result v0

    return v0
.end method

.method public unregisterProvider()Z
    .locals 1

    .prologue
    .line 253
    invoke-super {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->unregisterProvider()Z

    move-result v0

    return v0
.end method
