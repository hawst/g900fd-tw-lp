.class public Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
.super Ljava/lang/Object;
.source "SmartCardAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    }
.end annotation


# static fields
.field public static final BIND_PIN_SERVICE:Ljava/lang/String; = "com.sec.smartcard.pinservice.action.BIND_SMART_CARD_PIN_SERVICE"

.field private static final SAMSUNG_DEFAULT_ADAPTER:Ljava/lang/String; = "com.sec.enterprise.mdm.sc.bai"

.field private static final SAMSUNG_SC_PKG_PREFIX:Ljava/lang/String; = "com.sec.enterprise.mdm.sc."

.field private static final TAG:Ljava/lang/String; = "SmartcardAdapter"

.field private static mContext:Landroid/content/Context;

.field private static mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;


# instance fields
.field public mName:Ljava/lang/String;

.field private mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

.field private mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;

.field private mPinServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mName:Ljava/lang/String;

    .line 124
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mName:Ljava/lang/String;

    .line 125
    return-void
.end method

.method static synthetic access$002(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Landroid/content/ServiceConnection;)Landroid/content/ServiceConnection;
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    .param p1, "x1"    # Landroid/content/ServiceConnection;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    .param p1, "x1"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)Lcom/sec/smartcard/pinservice/SmartCardPinManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;Lcom/sec/smartcard/pinservice/SmartCardPinManager;)Lcom/sec/smartcard/pinservice/SmartCardPinManager;
    .locals 0
    .param p0, "x0"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    .param p1, "x1"    # Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;

    return-object p1
.end method

.method public static declared-synchronized getAdapter(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "adapterName"    # Ljava/lang/String;

    .prologue
    .line 336
    const-class v1, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    monitor-enter v1

    :try_start_0
    const-string v0, "SmartcardAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdapter for package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mContext:Landroid/content/Context;

    .line 338
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    const-string v2, "com.sec.enterprise.mdm.sc.bai"

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    .line 341
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 336
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getAvailableAdapters(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    const-class v1, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    monitor-enter v1

    :try_start_0
    const-string v0, "SmartcardAdapter"

    const-string v2, "getAvailableAdapters"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    const/4 v0, 0x0

    monitor-exit v1

    return-object v0

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized getDefaultAdapter(Landroid/content/Context;)Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 297
    const-class v1, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    monitor-enter v1

    :try_start_0
    const-string v0, "SmartcardAdapter"

    const-string v2, "getDefaultAdapter"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mContext:Landroid/content/Context;

    .line 299
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    if-nez v0, :cond_0

    .line 300
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    const-string v2, "com.sec.enterprise.mdm.sc.bai"

    invoke-direct {v0, v2}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;

    .line 302
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mSmartcardAdapter:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public declared-synchronized bindPinService(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;)V
    .locals 4
    .param p1, "pinCallback"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    .prologue
    .line 166
    monitor-enter p0

    if-nez p1, :cond_0

    .line 167
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "PinServiceCallback is null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 170
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinCallback:Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    .line 171
    new-instance v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$1;-><init>(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;

    .line 192
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.smartcard.pinservice.action.BIND_SMART_CARD_PIN_SERVICE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized unbindPinService(Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;)V
    .locals 2
    .param p1, "pinCallback"    # Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter$PinServiceCallback;

    .prologue
    .line 233
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 234
    sget-object v0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/enterprise/knox/smartcard/SmartCardAdapter;->mPinServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_0
    monitor-exit p0

    return-void

    .line 233
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
