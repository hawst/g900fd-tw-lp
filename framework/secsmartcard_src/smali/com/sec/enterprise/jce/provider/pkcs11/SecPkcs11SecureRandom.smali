.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;
.super Ljava/security/SecureRandomSpi;
.source "SecPkcs11SecureRandom.java"


# static fields
.field private static final serialVersionUID:J = 0x71e4819c94414d50L


# instance fields
.field private localSeed:[B

.field private mSecureRandom:Ljava/security/SecureRandom;

.field private final pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;)V
    .locals 4
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    .param p2, "algorithm"    # Ljava/lang/String;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/security/SecureRandomSpi;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 55
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->localSeed:[B

    .line 58
    :try_start_0
    iget-object v1, p1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-object v1, v1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    invoke-static {p2, v1}, Ljava/security/SecureRandom;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/SecureRandom;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->mSecureRandom:Ljava/security/SecureRandom;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_1

    .line 67
    :goto_0
    return-void

    .line 59
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 62
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unsupported SecureRandom algorithm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 65
    .local v0, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v0}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public engineGenerateSeed(I)[B
    .locals 1
    .param p1, "numBytes"    # I

    .prologue
    .line 110
    new-array v0, p1, [B

    .line 111
    .local v0, "b":[B
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->engineNextBytes([B)V

    .line 112
    return-object v0
.end method

.method public engineNextBytes([B)V
    .locals 1
    .param p1, "bytes"    # [B

    .prologue
    .line 93
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->mSecureRandom:Ljava/security/SecureRandom;

    invoke-virtual {v0, p1}, Ljava/security/SecureRandom;->nextBytes([B)V

    goto :goto_0
.end method

.method public declared-synchronized engineSetSeed([B)V
    .locals 3
    .param p1, "seed"    # [B

    .prologue
    .line 74
    monitor-enter p0

    if-nez p1, :cond_0

    .line 75
    :try_start_0
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "cannot have null seed"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 74
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 80
    :cond_0
    :try_start_1
    array-length v1, p1

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->localSeed:[B

    .line 81
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11SecureRandom;->localSeed:[B

    aget-byte v2, p1, v0

    aput-byte v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 84
    :cond_1
    monitor-exit p0

    return-void
.end method
