.class public final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
.super Ljava/lang/Object;
.source "SecPkcs11Configuration.java"


# instance fields
.field public debug:Z

.field public defaultProvider:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public functionListName:Ljava/lang/String;

.field public libraryName:Ljava/lang/String;

.field public moduleName:Ljava/lang/String;

.field public providerName:Ljava/lang/String;

.field public slotId:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    .line 139
    const-string v0, "BC"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    .line 140
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    .line 141
    const-string v0, "SECPkcs11"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->providerName:Ljava/lang/String;

    .line 142
    const-string v0, "SEC PKCS11 Crypto Provider"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->description:Ljava/lang/String;

    .line 143
    const-string v0, "libSamsungPkcs11Wrapper.so"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    .line 144
    const-string v0, "C_GetFunctionList"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    .line 145
    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 3
    .param p1, "slotId"    # J
    .param p3, "libraryName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    .line 169
    iput-wide p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    .line 170
    iput-object p3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    .line 171
    const-string v0, "C_GetFunctionList"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    .line 172
    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    .line 173
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    .line 155
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    .line 156
    const-string v0, "C_GetFunctionList"

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    .line 157
    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    .line 158
    return-void
.end method


# virtual methods
.method getDefaultProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    return-object v0
.end method

.method getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getFunctionListName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    return-object v0
.end method

.method public getLibraryName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    return-object v0
.end method

.method public getModuleName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    return-object v0
.end method

.method getProviderName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->providerName:Ljava/lang/String;

    return-object v0
.end method

.method getSlotId()J
    .locals 2

    .prologue
    .line 189
    iget-wide v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    return-wide v0
.end method

.method isDebug()Z
    .locals 1

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    return v0
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1, "debug"    # Z

    .prologue
    .line 255
    iput-boolean p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->debug:Z

    .line 256
    return-void
.end method

.method public setDefaultProvider(Ljava/lang/String;)V
    .locals 0
    .param p1, "defaultProvider"    # Ljava/lang/String;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    .line 226
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->description:Ljava/lang/String;

    .line 265
    return-void
.end method

.method public setFunctionListName(Ljava/lang/String;)V
    .locals 0
    .param p1, "funcListName"    # Ljava/lang/String;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    .line 123
    return-void
.end method

.method public setLibraryName(Ljava/lang/String;)V
    .locals 0
    .param p1, "libraryName"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setModuleName(Ljava/lang/String;)V
    .locals 0
    .param p1, "moduleName"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setProviderName(Ljava/lang/String;)V
    .locals 0
    .param p1, "providerName"    # Ljava/lang/String;

    .prologue
    .line 245
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->providerName:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public setSlotId(J)V
    .locals 1
    .param p1, "slotId"    # J

    .prologue
    .line 235
    iput-wide p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    .line 236
    return-void
.end method
