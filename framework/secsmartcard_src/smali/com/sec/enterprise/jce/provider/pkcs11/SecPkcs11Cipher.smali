.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;
.super Ljavax/crypto/CipherSpi;
.source "SecPkcs11Cipher.java"


# static fields
.field private static final ANDROID_OPENSSL_PROVIDER:Ljava/lang/String; = "AndroidOpenSSL"

.field public static final MAX_MODLEN:I = 0x4000

.field public static final MIN_MODLEN:I = 0x200

.field private static final M_DECRYPT:I = 0x2

.field private static final M_ENCRYPT:I = 0x1

.field private static final M_SIGN:I = 0x3

.field private static final M_VERIFY:I = 0x4

.field private static final PAD_NONE:I = 0x5

.field private static final PAD_PKCS1:I = 0x2

.field private static final PKCS1_MIN_PADDING_LENGTH:I = 0xb

.field private static final TAG:Ljava/lang/String; = "SecPkcs11Cipher"


# instance fields
.field private bcCipher:Ljavax/crypto/Cipher;

.field private final blockSize:I

.field private buffer:[B

.field private bytesBuffered:I

.field private decrypt:Z

.field private final keyAlgorithm:Ljava/lang/String;

.field private maxInputSize:I

.field private opensslCipher:Ljavax/crypto/Cipher;

.field private outputSize:I

.field private padType:I

.field private paddingType:I

.field private final pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

.field private privKeyHandle:J

.field private pubKeyTemplate:[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field private useCard:Z


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V
    .locals 6
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    .param p2, "algorithm"    # Ljava/lang/String;
    .param p3, "mechanism"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 149
    invoke-direct {p0}, Ljavax/crypto/CipherSpi;-><init>()V

    .line 108
    iput-boolean v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    .line 150
    const-string v2, "SecPkcs11Cipher"

    const-string v3, "SecPkcs11Cipher"

    invoke-static {v2, v3}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 153
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->privKeyHandle:J

    .line 155
    iput-object v5, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->buffer:[B

    .line 157
    const-string v2, "/"

    invoke-virtual {p2, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "algoParts":[Ljava/lang/String;
    aget-object v2, v0, v4

    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->keyAlgorithm:Ljava/lang/String;

    .line 160
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->keyAlgorithm:Ljava/lang/String;

    const-string v3, "AES"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 161
    const/16 v2, 0x10

    iput v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    .line 168
    :goto_0
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->keyAlgorithm:Ljava/lang/String;

    const-string v3, "RSA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 169
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    .line 172
    :cond_0
    iput-object v5, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    .line 174
    :try_start_0
    iget-object v2, p1, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->config:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    iget-object v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->defaultProvider:Ljava/lang/String;

    invoke-static {p2, v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    .line 175
    const-string v2, "AndroidOpenSSL"

    invoke-static {p2, v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    .line 183
    :goto_1
    return-void

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->keyAlgorithm:Ljava/lang/String;

    const-string v3, "RC4"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->keyAlgorithm:Ljava/lang/String;

    const-string v3, "ARCFOUR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 163
    :cond_2
    iput v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    goto :goto_0

    .line 165
    :cond_3
    const/16 v2, 0x8

    iput v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    goto :goto_0

    .line 176
    :catch_0
    move-exception v1

    .line 177
    .local v1, "e":Ljava/security/NoSuchProviderException;
    invoke-virtual {v1}, Ljava/security/NoSuchProviderException;->printStackTrace()V

    goto :goto_1

    .line 178
    .end local v1    # "e":Ljava/security/NoSuchProviderException;
    :catch_1
    move-exception v1

    .line 179
    .local v1, "e":Ljavax/crypto/NoSuchPaddingException;
    invoke-virtual {v1}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V

    goto :goto_1

    .line 180
    .end local v1    # "e":Ljavax/crypto/NoSuchPaddingException;
    :catch_2
    move-exception v1

    .line 181
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v2, Ljava/security/ProviderException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No such algorithm"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private doFinalLength(I)I
    .locals 3
    .param p1, "inLen"    # I

    .prologue
    .line 306
    const-string v1, "SecPkcs11Cipher"

    const-string v2, "doFinalLength"

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    if-gez p1, :cond_1

    .line 308
    const/4 v0, 0x0

    .line 315
    :cond_0
    :goto_0
    return v0

    .line 310
    :cond_1
    iget v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bytesBuffered:I

    add-int v0, p1, v1

    .line 311
    .local v0, "result":I
    iget v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->paddingType:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    .line 313
    iget v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    iget v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    add-int/lit8 v2, v2, -0x1

    and-int/2addr v2, v0

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected engineDoFinal(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
    .locals 2
    .param p1, "inBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "outBuffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 432
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineDoFinal"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2}, Ljavax/crypto/Cipher;->doFinal(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 438
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2}, Ljavax/crypto/Cipher;->doFinal(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I

    move-result v0

    goto :goto_0
.end method

.method protected engineDoFinal([BII[BI)I
    .locals 6
    .param p1, "in"    # [B
    .param p2, "inOfs"    # I
    .param p3, "inLen"    # I
    .param p4, "out"    # [B
    .param p5, "outOfs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;,
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 413
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineDoFinal"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v0

    .line 419
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->doFinal([BII[BI)I

    move-result v0

    goto :goto_0
.end method

.method protected engineDoFinal([BII)[B
    .locals 2
    .param p1, "in"    # [B
    .param p2, "inOfs"    # I
    .param p3, "inLen"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljavax/crypto/BadPaddingException;
        }
    .end annotation

    .prologue
    .line 395
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineDoFinal"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v0

    .line 401
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->doFinal([BII)[B

    move-result-object v0

    goto :goto_0
.end method

.method protected engineGetBlockSize()I
    .locals 2

    .prologue
    .line 201
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineGetBlockSize"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 203
    iget v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->blockSize:I

    .line 205
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getBlockSize()I

    move-result v0

    goto :goto_0
.end method

.method protected engineGetIV()[B
    .locals 2

    .prologue
    .line 221
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineGetIV"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 223
    new-instance v0, Ljava/security/ProviderException;

    const-string v1, "engineGetIV: not supported in BAI"

    invoke-direct {v0, v1}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getIV()[B

    move-result-object v0

    return-object v0
.end method

.method protected engineGetKeySize(Ljava/security/Key;)I
    .locals 2
    .param p1, "key"    # Ljava/security/Key;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 483
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineGetKeySize"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 484
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "engineGetKeySize()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineGetOutputSize(I)I
    .locals 2
    .param p1, "inputLen"    # I

    .prologue
    .line 211
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineGetOutputSize"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-direct {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->doFinalLength(I)I

    move-result v0

    .line 215
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->getOutputSize(I)I

    move-result v0

    goto :goto_0
.end method

.method protected engineGetParameters()Ljava/security/AlgorithmParameters;
    .locals 2

    .prologue
    .line 231
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineGetParameters"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x0

    .line 235
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0}, Ljavax/crypto/Cipher;->getParameters()Ljava/security/AlgorithmParameters;

    move-result-object v0

    goto :goto_0
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V
    .locals 2
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "params"    # Ljava/security/AlgorithmParameters;
    .param p4, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 289
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineInit 3"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V

    .line 297
    :goto_0
    return-void

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;Ljava/security/SecureRandom;)V

    goto :goto_0
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/SecureRandom;)V
    .locals 3
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 244
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineInit 1"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    packed-switch p1, :pswitch_data_0

    .line 253
    new-instance v0, Ljava/security/InvalidKeyException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported mode: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 247
    :pswitch_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    .line 256
    :goto_0
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    .line 262
    :goto_1
    return-void

    .line 250
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    goto :goto_0

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/SecureRandom;)V

    goto :goto_1

    .line 245
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected engineInit(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V
    .locals 2
    .param p1, "opmode"    # I
    .param p2, "key"    # Ljava/security/Key;
    .param p3, "params"    # Ljava/security/spec/AlgorithmParameterSpec;
    .param p4, "random"    # Ljava/security/SecureRandom;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/InvalidAlgorithmParameterException;
        }
    .end annotation

    .prologue
    .line 271
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineInit 2"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 272
    if-eqz p3, :cond_0

    .line 273
    instance-of v0, p3, Ljavax/crypto/spec/IvParameterSpec;

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Ljava/security/InvalidAlgorithmParameterException;

    const-string v1, "Only IvParameterSpec supported"

    invoke-direct {v0, v1}, Ljava/security/InvalidAlgorithmParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_0
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->opensslCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    .line 284
    :goto_0
    return-void

    .line 282
    :cond_1
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3, p4}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;Ljava/security/SecureRandom;)V

    goto :goto_0
.end method

.method protected engineSetMode(Ljava/lang/String;)V
    .locals 3
    .param p1, "mode"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 186
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineSetMode"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v0, Ljava/security/NoSuchAlgorithmException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/NoSuchAlgorithmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineSetPadding(Ljava/lang/String;)V
    .locals 3
    .param p1, "padding"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/NoSuchPaddingException;
        }
    .end annotation

    .prologue
    .line 193
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineSetPadding"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    new-instance v0, Ljavax/crypto/NoSuchPaddingException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported padding "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/crypto/NoSuchPaddingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected engineUnwrap([BLjava/lang/String;I)Ljava/security/Key;
    .locals 2
    .param p1, "wrappedKey"    # [B
    .param p2, "wrappedKeyAlgorithm"    # Ljava/lang/String;
    .param p3, "wrappedKeyType"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/InvalidKeyException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 471
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineUnwrap"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 474
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "engineUnwrap()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->unwrap([BLjava/lang/String;I)Ljava/security/Key;

    move-result-object v0

    return-object v0
.end method

.method protected engineUpdate(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I
    .locals 2
    .param p1, "inBuffer"    # Ljava/nio/ByteBuffer;
    .param p2, "outBuffer"    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;
        }
    .end annotation

    .prologue
    .line 369
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineUpdate"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 372
    new-instance v0, Ljava/security/ProviderException;

    const-string v1, "engineUpdate: not supported in BAI"

    invoke-direct {v0, v1}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 377
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2}, Ljavax/crypto/Cipher;->update(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)I

    move-result v0

    return v0
.end method

.method protected engineUpdate([BII[BI)I
    .locals 6
    .param p1, "in"    # [B
    .param p2, "inOfs"    # I
    .param p3, "inLen"    # I
    .param p4, "out"    # [B
    .param p5, "outOfs"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/ShortBufferException;
        }
    .end annotation

    .prologue
    .line 352
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineUpdate"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 355
    new-instance v0, Ljava/security/ProviderException;

    const-string v1, "engineUpdate: not supportedin BAI"

    invoke-direct {v0, v1}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ljavax/crypto/Cipher;->update([BII[BI)I

    move-result v0

    return v0
.end method

.method protected engineUpdate([BII)[B
    .locals 2
    .param p1, "in"    # [B
    .param p2, "inOfs"    # I
    .param p3, "inLen"    # I

    .prologue
    .line 333
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineUpdate"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 336
    new-instance v0, Ljava/security/ProviderException;

    const-string v1, "engineUpdate: not supported in BAI"

    invoke-direct {v0, v1}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1, p2, p3}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v0

    return-object v0
.end method

.method protected engineWrap(Ljava/security/Key;)[B
    .locals 2
    .param p1, "key"    # Ljava/security/Key;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/crypto/IllegalBlockSizeException;,
            Ljava/security/InvalidKeyException;
        }
    .end annotation

    .prologue
    .line 453
    const-string v0, "SecPkcs11Cipher"

    const-string v1, "engineWrap"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->useCard:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->decrypt:Z

    if-eqz v0, :cond_0

    .line 455
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "engineWrap()"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 457
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Cipher;->bcCipher:Ljavax/crypto/Cipher;

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->wrap(Ljava/security/Key;)[B

    move-result-object v0

    return-object v0
.end method
