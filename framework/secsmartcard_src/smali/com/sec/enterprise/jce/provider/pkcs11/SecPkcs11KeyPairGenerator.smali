.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;
.super Ljava/security/KeyPairGeneratorSpi;
.source "SecPkcs11KeyPairGenerator.java"


# instance fields
.field mKeyPairGenerator:Ljava/security/KeyPairGenerator;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1, "algorithm"    # Ljava/lang/String;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/security/KeyPairGeneratorSpi;-><init>()V

    .line 51
    :try_start_0
    invoke-static {p1}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;->mKeyPairGenerator:Ljava/security/KeyPairGenerator;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    new-instance v1, Ljava/security/ProviderException;

    const-string v2, "No such algorithm"

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public generateKeyPair()Ljava/security/KeyPair;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;->mKeyPairGenerator:Ljava/security/KeyPairGenerator;

    invoke-virtual {v0}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    move-result-object v0

    return-object v0
.end method

.method public initialize(ILjava/security/SecureRandom;)V
    .locals 1
    .param p1, "keySize"    # I
    .param p2, "random"    # Ljava/security/SecureRandom;

    .prologue
    .line 64
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyPairGenerator;->mKeyPairGenerator:Ljava/security/KeyPairGenerator;

    invoke-virtual {v0, p1, p2}, Ljava/security/KeyPairGenerator;->initialize(ILjava/security/SecureRandom;)V

    .line 65
    return-void
.end method
