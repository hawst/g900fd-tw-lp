.class public Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Key;
.super Ljava/lang/Object;
.source "SecPkcs11Key.java"

# interfaces
.implements Ljava/security/Key;


# direct methods
.method constructor <init>(ILcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;JLjava/lang/String;I[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V
    .locals 0
    .param p1, "keyType"    # I
    .param p2, "session"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
    .param p3, "keyHandle"    # J
    .param p5, "keyAlgorithm"    # Ljava/lang/String;
    .param p6, "keyLength"    # I
    .param p7, "keyAttributes"    # [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method


# virtual methods
.method protected destroy()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public getAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return-object v0
.end method

.method public getEncoded()[B
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public getFormat()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return-object v0
.end method

.method public getKeyLength()J
    .locals 2

    .prologue
    .line 86
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public isPrivate()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public isPublic()Z
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public queryAttributes([Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V
    .locals 0
    .param p1, "attributes"    # [Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .prologue
    .line 100
    return-void
.end method
