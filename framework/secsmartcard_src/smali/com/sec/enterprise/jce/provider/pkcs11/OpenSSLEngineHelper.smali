.class public Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;
.super Ljava/lang/Object;
.source "OpenSSLEngineHelper.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private FUNCTION_LIST_NAME:Ljava/lang/String;

.field private LIBRARY_NAME:Ljava/lang/String;

.field private opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

.field private slotID:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "OpenSSLHelper_Pkcs11"

    sput-object v0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    .line 63
    const-string v0, "secopenssl_engine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "libraryName"    # Ljava/lang/String;
    .param p2, "functionListName"    # Ljava/lang/String;
    .param p3, "slot_Id"    # J

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    .line 69
    sget-object v0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v1, "OpenSSLEngineHelper- constructor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->LIBRARY_NAME:Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->FUNCTION_LIST_NAME:Ljava/lang/String;

    .line 72
    iput-wide p3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->slotID:J

    .line 73
    iput-object p0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    .line 74
    return-void
.end method

.method public static getPassCode()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 177
    const/4 v2, 0x0

    .line 178
    .local v2, "pin":[C
    new-instance v1, Ljavax/security/auth/callback/PasswordCallback;

    const-string v4, "Enter CAC Pin"

    invoke-direct {v1, v4, v5}, Ljavax/security/auth/callback/PasswordCallback;-><init>(Ljava/lang/String;Z)V

    .line 179
    .local v1, "pcall":Ljavax/security/auth/callback/PasswordCallback;
    const/4 v4, 0x1

    new-array v0, v4, [Ljavax/security/auth/callback/Callback;

    aput-object v1, v0, v5

    .line 183
    .local v0, "callbacks":[Ljavax/security/auth/callback/Callback;
    :try_start_0
    sget-object v4, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->pHandler:Ljavax/security/auth/callback/CallbackHandler;

    invoke-interface {v4, v0}, Ljavax/security/auth/callback/CallbackHandler;->handle([Ljavax/security/auth/callback/Callback;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_0
    invoke-virtual {v1}, Ljavax/security/auth/callback/PasswordCallback;->getPassword()[C

    move-result-object v2

    .line 189
    invoke-virtual {v1}, Ljavax/security/auth/callback/PasswordCallback;->clearPassword()V

    .line 190
    if-nez v2, :cond_0

    .line 191
    const/4 v3, 0x0

    .line 194
    :goto_1
    return-object v3

    .line 193
    :cond_0
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([C)V

    .line 194
    .local v3, "ret_pin":Ljava/lang/String;
    goto :goto_1

    .line 184
    .end local v3    # "ret_pin":Ljava/lang/String;
    :catch_0
    move-exception v4

    goto :goto_0
.end method


# virtual methods
.method public native deregisterEngine()I
.end method

.method public deregisterSSLEngine()Z
    .locals 3

    .prologue
    .line 96
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "deregister_engine function"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->deregisterEngine()I

    move-result v0

    .line 100
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 101
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "deregister_engine success"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v1, 0x1

    .line 105
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public native getAllCertAliases()[Ljava/lang/String;
.end method

.method public native getAllPrivKeyAliases()[Ljava/lang/String;
.end method

.method public getCertAliasList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 150
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "getCertAliasList function"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 152
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getAllCertAliases()[Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "aliasList":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 155
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "getCertAliasList return null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    :cond_0
    return-object v0
.end method

.method public getCertChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .locals 7
    .param p1, "alias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 129
    sget-object v5, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v6, "getCertificateChain function"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    if-nez p1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-object v1

    .line 132
    :cond_1
    iget-object v5, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v5, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getCertificateChain(Ljava/lang/String;)[B

    move-result-object v4

    .line 133
    .local v4, "certRawBytes":[B
    if-eqz v4, :cond_0

    .line 138
    const-string v5, "X.509"

    invoke-static {v5}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 139
    .local v3, "certFactory":Ljava/security/cert/CertificateFactory;
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 140
    .local v2, "bIs":Ljava/io/ByteArrayInputStream;
    invoke-virtual {v3, v2}, Ljava/security/cert/CertificateFactory;->generateCertificates(Ljava/io/InputStream;)Ljava/util/Collection;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 141
    .local v0, "CertList":Ljava/util/List;, "Ljava/util/List<Ljava/security/cert/Certificate;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/security/cert/Certificate;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/security/cert/Certificate;

    .line 143
    .local v1, "Certs":[Ljava/security/cert/Certificate;
    goto :goto_0
.end method

.method public native getCertificateChain(Ljava/lang/String;)[B
.end method

.method public getPrivKeyAliasList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 164
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "getPrivKeyAliasList function"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getAllPrivKeyAliases()[Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "aliasList":[Ljava/lang/String;
    if-nez v0, :cond_0

    .line 168
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "getPrivKeyAliasList return null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_0
    return-object v0
.end method

.method public getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 112
    sget-object v3, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v4, "getPrivateKey function"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    const/4 v2, 0x0

    .line 114
    .local v2, "pkey":Ljava/security/PrivateKey;
    if-nez p1, :cond_0

    .line 115
    const/4 v3, 0x0

    .line 122
    :goto_0
    return-object v3

    .line 116
    :cond_0
    const-string v3, "secpkcs11"

    invoke-static {v3}, Lcom/android/org/conscrypt/OpenSSLEngine;->getInstance(Ljava/lang/String;)Lcom/android/org/conscrypt/OpenSSLEngine;

    move-result-object v1

    .line 118
    .local v1, "engine":Lcom/android/org/conscrypt/OpenSSLEngine;
    :try_start_0
    invoke-virtual {v1, p1}, Lcom/android/org/conscrypt/OpenSSLEngine;->getPrivateKeyById(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 122
    goto :goto_0

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Ljava/security/InvalidKeyException;
    sget-object v3, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v4, "InvalidKeyException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public native registerEngine(Ljava/lang/String;Ljava/lang/String;J)I
.end method

.method public registerSSLEngine()Z
    .locals 6

    .prologue
    .line 80
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "register_engine function"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->opensslhelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->LIBRARY_NAME:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->FUNCTION_LIST_NAME:Ljava/lang/String;

    iget-wide v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->slotID:J

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->registerEngine(Ljava/lang/String;Ljava/lang/String;J)I

    move-result v0

    .line 84
    .local v0, "ret":I
    if-nez v0, :cond_0

    .line 85
    sget-object v1, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->TAG:Ljava/lang/String;

    const-string v2, "register_engine success"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    const/4 v1, 0x1

    .line 89
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
