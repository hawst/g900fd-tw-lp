.class public final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;
.super Ljava/security/KeyStoreSpi;
.source "SecPkcs11KeyStore.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecPkcs11KeyStore"


# instance fields
.field private final pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;)V
    .locals 0
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/security/KeyStoreSpi;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;)V
    .locals 1
    .param p1, "token"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/security/KeyStoreSpi;-><init>()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 105
    return-void
.end method

.method private isCertificateEntry(Ljava/lang/String;)Z
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 267
    if-nez p1, :cond_1

    .line 274
    :cond_0
    :goto_0
    return v1

    .line 270
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v2}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getCertAliasList()[Ljava/lang/String;

    move-result-object v0

    .line 271
    .local v0, "aliasList":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 274
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized engineAliases()Ljava/util/Enumeration;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Enumeration",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    const-string v4, "SecPkcs11KeyStore"

    const-string v5, "engineAliases"

    invoke-static {v4, v5}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    iget-object v4, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v4, v4, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v4}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getCertAliasList()[Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "aliasList1":[Ljava/lang/String;
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 121
    .local v1, "aliasNames":Ljava/util/Vector;
    if-eqz v0, :cond_1

    .line 122
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v4, v0

    if-ge v3, v4, :cond_1

    .line 123
    const-string v4, "SecPkcs11KeyStore"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "alias1 "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v0, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    aget-object v4, v0, v3

    invoke-virtual {v1, v4}, Ljava/util/Vector;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 125
    aget-object v4, v0, v3

    invoke-virtual {v1, v4}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 138
    .end local v3    # "i":I
    :cond_1
    invoke-virtual {v1}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 140
    .local v2, "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    monitor-exit p0

    return-object v2

    .line 115
    .end local v0    # "aliasList1":[Ljava/lang/String;
    .end local v1    # "aliasNames":Ljava/util/Vector;
    .end local v2    # "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method

.method public declared-synchronized engineContainsAlias(Ljava/lang/String;)Z
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 149
    monitor-enter p0

    :try_start_0
    const-string v3, "SecPkcs11KeyStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "engineContainsAlias - Alias: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    if-nez p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    monitor-exit p0

    return v2

    .line 154
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->engineAliases()Ljava/util/Enumeration;

    move-result-object v0

    .line 155
    .local v0, "aliasSet":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 159
    :cond_2
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 160
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 161
    .local v1, "temp":Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    if-eqz v3, :cond_2

    .line 162
    const/4 v2, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "aliasSet":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v1    # "temp":Ljava/lang/String;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public engineDeleteEntry(Ljava/lang/String;)V
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 174
    const-string v0, "SecPkcs11KeyStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "engineDeleteEntry - Alias: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Delete entry not supported"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public declared-synchronized engineGetCertificate(Ljava/lang/String;)Ljava/security/cert/Certificate;
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 184
    monitor-enter p0

    :try_start_0
    const-string v3, "SecPkcs11KeyStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "engineGetCertificate - Alias: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-direct {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->isCertificateEntry(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_1

    .line 198
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v2

    .line 189
    :cond_1
    const/4 v0, 0x0

    .line 191
    .local v0, "certChain":[Ljava/security/cert/Certificate;
    :try_start_1
    iget-object v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v3, v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v3, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getCertChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    :try_end_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    .line 198
    const/4 v2, 0x0

    :try_start_2
    aget-object v2, v0, v2

    goto :goto_0

    .line 192
    :catch_0
    move-exception v1

    .line 193
    .local v1, "e":Ljava/security/cert/CertificateException;
    new-instance v2, Ljava/security/ProviderException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Certificate exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 184
    .end local v0    # "certChain":[Ljava/security/cert/Certificate;
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public engineGetCertificateAlias(Ljava/security/cert/Certificate;)Ljava/lang/String;
    .locals 2
    .param p1, "cert"    # Ljava/security/cert/Certificate;

    .prologue
    .line 207
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineGetCertificateAlias"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    const/4 v0, 0x0

    return-object v0
.end method

.method public engineGetCertificateChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    .locals 6
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 218
    const-string v3, "SecPkcs11KeyStore"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "engineGetCertificateChain - Alias: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    invoke-direct {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->isCertificateEntry(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    move-object v0, v2

    .line 233
    :cond_0
    :goto_0
    return-object v0

    .line 224
    :cond_1
    const/4 v0, 0x0

    .line 226
    .local v0, "certChain":[Ljava/security/cert/Certificate;
    :try_start_0
    iget-object v3, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v3, v3, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v3, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getCertChain(Ljava/lang/String;)[Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    move-object v0, v2

    .line 231
    goto :goto_0

    .line 227
    :catch_0
    move-exception v1

    .line 228
    .local v1, "e":Ljava/security/cert/CertificateException;
    new-instance v2, Ljava/security/ProviderException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Certificate exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public engineGetCreationDate(Ljava/lang/String;)Ljava/util/Date;
    .locals 3
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 242
    const-string v0, "SecPkcs11KeyStore"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "engineGetCreationDate - Alias: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized engineGetKey(Ljava/lang/String;[C)Ljava/security/Key;
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/UnrecoverableKeyException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 253
    monitor-enter p0

    :try_start_0
    const-string v2, "SecPkcs11KeyStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "engineGetKey - Alias: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->engineIsKeyEntry(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 261
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 257
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v2, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 258
    .local v0, "pkey":Ljava/security/PrivateKey;
    if-nez v0, :cond_0

    move-object v0, v1

    .line 259
    goto :goto_0

    .line 253
    .end local v0    # "pkey":Ljava/security/PrivateKey;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized engineIsCertificateEntry(Ljava/lang/String;)Z
    .locals 4
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 283
    monitor-enter p0

    :try_start_0
    const-string v1, "SecPkcs11KeyStore"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "engineIsCertificateEntry - Alias: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 285
    if-nez p1, :cond_1

    .line 288
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->engineIsKeyEntry(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->isCertificateEntry(Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 283
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized engineIsKeyEntry(Ljava/lang/String;)Z
    .locals 5
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 297
    monitor-enter p0

    :try_start_0
    const-string v2, "SecPkcs11KeyStore"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "engineIsKeyEntry - Alias: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 298
    if-nez p1, :cond_1

    .line 307
    :cond_0
    :goto_0
    monitor-exit p0

    return v1

    .line 303
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    iget-object v2, v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->opensslHelper:Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;

    invoke-virtual {v2}, Lcom/sec/enterprise/jce/provider/pkcs11/OpenSSLEngineHelper;->getPrivKeyAliasList()[Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "aliasList":[Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 307
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    goto :goto_0

    .line 297
    .end local v0    # "aliasList":[Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized engineLoad(Ljava/io/InputStream;[C)V
    .locals 2
    .param p1, "is"    # Ljava/io/InputStream;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineLoad"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    monitor-exit p0

    return-void

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public engineSetCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 327
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineSetCertificateEntry"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Set not supported"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public engineSetKeyEntry(Ljava/lang/String;Ljava/security/Key;[C[Ljava/security/cert/Certificate;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # Ljava/security/Key;
    .param p3, "arg2"    # [C
    .param p4, "arg3"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 351
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineSetKeyEntry"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Set not supported"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public engineSetKeyEntry(Ljava/lang/String;[B[Ljava/security/cert/Certificate;)V
    .locals 2
    .param p1, "arg0"    # Ljava/lang/String;
    .param p2, "arg1"    # [B
    .param p3, "arg2"    # [Ljava/security/cert/Certificate;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/KeyStoreException;
        }
    .end annotation

    .prologue
    .line 339
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineSetKeyEntry"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    new-instance v0, Ljava/security/KeyStoreException;

    const-string v1, "Set not supported"

    invoke-direct {v0, v1}, Ljava/security/KeyStoreException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public engineSize()I
    .locals 4

    .prologue
    .line 361
    const-string v2, "SecPkcs11KeyStore"

    const-string v3, "engineSize"

    invoke-static {v2, v3}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    invoke-virtual {p0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11KeyStore;->engineAliases()Ljava/util/Enumeration;

    move-result-object v0

    .line 364
    .local v0, "aliasSet":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    if-nez v0, :cond_1

    .line 365
    const/4 v1, 0x0

    .line 374
    :cond_0
    return v1

    .line 368
    :cond_1
    const/4 v1, 0x0

    .line 369
    .local v1, "size":I
    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 370
    add-int/lit8 v1, v1, 0x1

    .line 371
    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    goto :goto_0
.end method

.method public engineStore(Ljava/io/OutputStream;[C)V
    .locals 2
    .param p1, "os"    # Ljava/io/OutputStream;
    .param p2, "password"    # [C
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;,
            Ljava/security/cert/CertificateException;
        }
    .end annotation

    .prologue
    .line 384
    const-string v0, "SecPkcs11KeyStore"

    const-string v1, "engineStore"

    invoke-static {v0, v1}, Lcom/sec/generic/util/log/Log;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    new-instance v0, Ljava/io/IOException;

    const-string v1, "engineStore not supported on token"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
