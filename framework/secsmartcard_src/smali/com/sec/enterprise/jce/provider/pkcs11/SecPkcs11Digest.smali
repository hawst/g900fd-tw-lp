.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;
.super Ljava/security/MessageDigestSpi;
.source "SecPkcs11Digest.java"


# static fields
.field private static final DIGEST_BEGIN:I = 0x1

.field private static final DIGEST_BUFFERED:I = 0x2

.field private static final DIGEST_INIT:I = 0x3


# instance fields
.field private algorithmName:Ljava/lang/String;

.field private bcDigest:Ljava/security/MessageDigest;

.field buffer:Ljava/io/ByteArrayOutputStream;

.field private final digestLen:I

.field private mechanism:Lcom/sec/smartcard/pkcs11/CK_MECHANISM;

.field private oneByte:[B

.field private final pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

.field private state:I


# direct methods
.method public constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;Ljava/lang/String;J)V
    .locals 5
    .param p1, "provider"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    .param p2, "alg"    # Ljava/lang/String;
    .param p3, "m"    # J

    .prologue
    const/16 v2, 0x10

    .line 95
    invoke-direct {p0}, Ljava/security/MessageDigestSpi;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->pkcs11Provider:Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    .line 97
    iput-object p2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 99
    new-instance v1, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;

    invoke-direct {v1, p3, p4}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;-><init>(J)V

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->mechanism:Lcom/sec/smartcard/pkcs11/CK_MECHANISM;

    .line 100
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->mechanism:Lcom/sec/smartcard/pkcs11/CK_MECHANISM;

    iput-wide p3, v1, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->mechanism:J

    .line 107
    long-to-int v1, p3

    sparse-switch v1, :sswitch_data_0

    .line 133
    new-instance v1, Ljava/security/ProviderException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown mechanism: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/security/ProviderException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 109
    :sswitch_0
    const-string v1, "MD2"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 110
    iput v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    .line 137
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    invoke-static {v1}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :goto_1
    return-void

    .line 113
    :sswitch_1
    const-string v1, "MD5"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 114
    iput v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    goto :goto_0

    .line 117
    :sswitch_2
    const-string v1, "SHA-1"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 118
    const/16 v1, 0x14

    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    goto :goto_0

    .line 121
    :sswitch_3
    const-string v1, "SHA-256"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 122
    const/16 v1, 0x20

    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    goto :goto_0

    .line 125
    :sswitch_4
    const-string v1, "SHA-384"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 126
    const/16 v1, 0x30

    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    goto :goto_0

    .line 129
    :sswitch_5
    const-string v1, "SHA-512"

    iput-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->algorithmName:Ljava/lang/String;

    .line 130
    const/16 v1, 0x40

    iput v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1

    .line 107
    :sswitch_data_0
    .sparse-switch
        0x200 -> :sswitch_0
        0x210 -> :sswitch_1
        0x220 -> :sswitch_2
        0x250 -> :sswitch_3
        0x260 -> :sswitch_4
        0x270 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method protected engineDigest([BII)I
    .locals 3
    .param p1, "digest"    # [B
    .param p2, "ofs"    # I
    .param p3, "len"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/security/DigestException;
        }
    .end annotation

    .prologue
    .line 184
    iget v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    if-ge p3, v0, :cond_0

    .line 185
    new-instance v0, Ljava/security/DigestException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Length must be at least "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/security/DigestException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->digest([BII)I

    move-result v0

    return v0
.end method

.method public engineDigest()[B
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    return-object v0
.end method

.method protected engineGetDigestLength()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->digestLen:I

    return v0
.end method

.method protected engineReset()V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 160
    return-void
.end method

.method public engineUpdate(B)V
    .locals 1
    .param p1, "in"    # B

    .prologue
    .line 201
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1}, Ljava/security/MessageDigest;->update(B)V

    .line 203
    return-void
.end method

.method protected engineUpdate(Ljava/nio/ByteBuffer;)V
    .locals 2
    .param p1, "bb"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 229
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 230
    .local v0, "len":I
    if-gtz v0, :cond_0

    .line 235
    :goto_0
    return-void

    .line 233
    :cond_0
    iget-object v1, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v1, p1}, Ljava/security/MessageDigest;->update(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public engineUpdate([BII)V
    .locals 1
    .param p1, "in"    # [B
    .param p2, "ofs"    # I
    .param p3, "len"    # I

    .prologue
    .line 216
    if-gtz p3, :cond_0

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Digest;->bcDigest:Ljava/security/MessageDigest;

    invoke-virtual {v0, p1, p2, p3}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0
.end method
