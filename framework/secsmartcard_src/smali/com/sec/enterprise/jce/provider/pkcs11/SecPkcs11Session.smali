.class final Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Session;
.super Ljava/lang/Object;
.source "SecPkcs11Session.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "SecPkcs11Session"


# direct methods
.method constructor <init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;J)V
    .locals 0
    .param p1, "token"    # Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Token;
    .param p2, "id"    # J

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method


# virtual methods
.method protected close()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation

    .prologue
    .line 57
    return-void
.end method

.method protected id()J
    .locals 2

    .prologue
    .line 64
    const-wide/16 v0, 0x0

    return-wide v0
.end method
