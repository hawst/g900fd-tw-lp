.class public interface abstract Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;
.super Ljava/lang/Object;
.source "ISmartcardConnectionAdapterListener.java"


# virtual methods
.method public abstract onConnected(Ljava/lang/String;)V
.end method

.method public abstract onConnectionFailed(Ljava/lang/String;I)V
.end method

.method public abstract onConnectionProgress(Ljava/lang/String;I)V
.end method

.method public abstract onDisconnected(Ljava/lang/String;I)V
.end method
