.class public Lcom/sec/smartcard/adapter/AdapterDependencyInfo;
.super Ljava/lang/Object;
.source "AdapterDependencyInfo.java"


# instance fields
.field public description:Ljava/lang/String;

.field public intent:Landroid/content/Intent;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->name:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->description:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->intent:Landroid/content/Intent;

    .line 32
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->description:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public setIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->intent:Landroid/content/Intent;

    .line 75
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterDependencyInfo;->name:Ljava/lang/String;

    .line 47
    return-void
.end method
