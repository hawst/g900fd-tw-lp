.class public Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
.super Ljava/lang/Object;
.source "SmartcardConnectionInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private details:Ljava/lang/String;

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo$1;

    invoke-direct {v0}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo$1;-><init>()V

    sput-object v0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-object p1, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->name:Ljava/lang/String;

    .line 64
    const-string v0, "N/A"

    iput-object v0, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->details:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "details"    # Ljava/lang/String;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->name:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->details:Ljava/lang/String;

    .line 79
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public getDetails()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->details:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 108
    iget-object v0, p0, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->details:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 110
    return-void
.end method
