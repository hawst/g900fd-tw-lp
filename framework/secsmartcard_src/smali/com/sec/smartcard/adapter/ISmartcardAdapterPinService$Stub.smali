.class public abstract Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;
.super Landroid/os/Binder;
.source "ISmartcardAdapterPinService.java"

# interfaces
.implements Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

.field static final TRANSACTION_getLoginAttemptRemain:I = 0x4

.field static final TRANSACTION_isDeviceConnectedWithCard:I = 0x5

.field static final TRANSACTION_register:I = 0x1

.field static final TRANSACTION_unregister:I = 0x2

.field static final TRANSACTION_verify:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 110
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v5

    :goto_0
    return v5

    .line 42
    :sswitch_0
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->createCharArray()[C

    move-result-object v0

    .line 51
    .local v0, "_arg0":[C
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;

    move-result-object v1

    .line 52
    .local v1, "_arg1":Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;
    invoke-virtual {p0, v0, v1}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->register([CLcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V

    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 58
    .end local v0    # "_arg0":[C
    .end local v1    # "_arg1":Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;
    :sswitch_2
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->createCharArray()[C

    move-result-object v0

    .line 62
    .restart local v0    # "_arg0":[C
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 63
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 69
    .local v1, "_arg1":Landroid/os/Bundle;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;

    move-result-object v2

    .line 70
    .local v2, "_arg2":Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;
    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->unregister([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V

    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 66
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v2    # "_arg2":Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Landroid/os/Bundle;
    goto :goto_1

    .line 76
    .end local v0    # "_arg0":[C
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_3
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->createCharArray()[C

    move-result-object v0

    .line 80
    .restart local v0    # "_arg0":[C
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 81
    sget-object v4, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    .line 87
    .restart local v1    # "_arg1":Landroid/os/Bundle;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;

    move-result-object v2

    .line 88
    .local v2, "_arg2":Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;
    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->verify([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;)V

    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 84
    .end local v1    # "_arg1":Landroid/os/Bundle;
    .end local v2    # "_arg2":Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Landroid/os/Bundle;
    goto :goto_2

    .line 94
    .end local v0    # "_arg0":[C
    .end local v1    # "_arg1":Landroid/os/Bundle;
    :sswitch_4
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback;

    move-result-object v0

    .line 97
    .local v0, "_arg0":Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->getLoginAttemptRemain(Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback;)V

    .line 98
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 103
    .end local v0    # "_arg0":Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback;
    :sswitch_5
    const-string v4, "com.sec.smartcard.adapter.ISmartcardAdapterPinService"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;->isDeviceConnectedWithCard()Z

    move-result v3

    .line 105
    .local v3, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    if-eqz v3, :cond_2

    move v4, v5

    :goto_3
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
