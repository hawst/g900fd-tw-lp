.class public interface abstract Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
.super Ljava/lang/Object;
.source "ICryptoServiceInitCallback.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback$Stub;
    }
.end annotation


# virtual methods
.method public abstract onInitComplete()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onInitFailed()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
