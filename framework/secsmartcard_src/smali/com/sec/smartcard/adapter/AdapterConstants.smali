.class public interface abstract Lcom/sec/smartcard/adapter/AdapterConstants;
.super Ljava/lang/Object;
.source "AdapterConstants.java"


# static fields
.field public static final SC_ADAPTER_ERROR_DEPENDENCY:I = 0x64

.field public static final SC_ADAPTER_ERROR_GENERAL:I = 0x68

.field public static final SC_ADAPTER_ERROR_INIT_RESOURCE:I = 0x65

.field public static final SC_ADAPTER_ERROR_LOST_SERVICE:I = 0x67

.field public static final SC_ADAPTER_ERROR_NONE:I = 0x0

.field public static final SC_ADAPTER_ERROR_SECURITY:I = 0x66

.field public static final SC_CONNECTION_ERROR_GENERIC_ERROR:I = 0x44

.field public static final SC_CONNECTION_ERROR_INVALID_PARAMETER:I = 0x4f

.field public static final SC_CONNECTION_ERROR_IO_FAILURE:I = 0x52

.field public static final SC_CONNECTION_ERROR_LOST_SERVICE:I = 0x50

.field public static final SC_CONNECTION_ERROR_NONE:I = 0x0

.field public static final SC_CONNECTION_ERROR_NO_CONNECTION:I = 0x49

.field public static final SC_CONNECTION_ERROR_SECURITY_VIOLATION:I = 0x41

.field public static final SC_CONNECTION_STATE_CONNECTING:I = 0x1

.field public static final SC_CONNECTION_STATE_IN_USE:I = 0xe

.field public static final SC_CONNECTION_STATE_NONE:I = 0x0

.field public static final SC_CONNECTION_STATE_READY:I = 0x4

.field public static final SC_CONNECTION_STATE_SECURING:I = 0x2

.field public static final SC_CONNECTION_STATE_WAITING_FOR_AUTHORIZATION:I = 0x3
