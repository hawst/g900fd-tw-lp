.class public abstract Lcom/sec/smartcard/adapter/ICryptoService$Stub;
.super Landroid/os/Binder;
.source "ICryptoService.java"

# interfaces
.implements Lcom/sec/smartcard/adapter/ICryptoService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/ICryptoService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/adapter/ICryptoService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.smartcard.adapter.ICryptoService"

.field static final TRANSACTION_connect:I = 0x5

.field static final TRANSACTION_connectAvailableTransport:I = 0x6

.field static final TRANSACTION_deinitialize:I = 0x3

.field static final TRANSACTION_deregisterListener:I = 0x9

.field static final TRANSACTION_disconnect:I = 0x7

.field static final TRANSACTION_getConnectionInfo:I = 0xa

.field static final TRANSACTION_getConnectionState:I = 0xb

.field static final TRANSACTION_getLastUsageTimeStamp:I = 0xc

.field static final TRANSACTION_getSupportedConnections:I = 0x4

.field static final TRANSACTION_getVersion:I = 0x1

.field static final TRANSACTION_initialize:I = 0x2

.field static final TRANSACTION_registerListener:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.smartcard.adapter.ICryptoService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/smartcard/adapter/ICryptoService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/smartcard/adapter/ICryptoService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/smartcard/adapter/ICryptoService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 167
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->getVersion()I

    move-result v2

    .line 49
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 55
    .end local v2    # "_result":I
    :sswitch_2
    const-string v7, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;

    move-result-object v0

    .line 59
    .local v0, "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->initialize(Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;Ljava/lang/String;)Z

    move-result v2

    .line 61
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v2, :cond_0

    move v5, v6

    :cond_0
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 67
    .end local v0    # "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceInitCallback;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v2    # "_result":Z
    :sswitch_3
    const-string v7, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->deinitialize()Z

    move-result v2

    .line 69
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 70
    if-eqz v2, :cond_1

    move v5, v6

    :cond_1
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v2    # "_result":Z
    :sswitch_4
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->getSupportedConnections()Ljava/util/List;

    move-result-object v4

    .line 77
    .local v4, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 78
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto :goto_0

    .line 83
    .end local v4    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_5
    const-string v7, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 86
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->connect(Ljava/lang/String;)Z

    move-result v2

    .line 87
    .restart local v2    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 88
    if-eqz v2, :cond_2

    move v5, v6

    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 93
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_result":Z
    :sswitch_6
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v0

    .line 96
    .local v0, "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->connectAvailableTransport(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Ljava/lang/String;

    move-result-object v2

    .line 97
    .local v2, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 98
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 103
    .end local v0    # "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .end local v2    # "_result":Ljava/lang/String;
    :sswitch_7
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 106
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->disconnect(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 112
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_8
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 116
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    .line 117
    .local v1, "_arg1":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->registerListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 118
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 123
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    :sswitch_9
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 127
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v1

    .line 128
    .restart local v1    # "_arg1":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-virtual {p0, v0, v1}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->deregisterListener(Ljava/lang/String;Lcom/sec/smartcard/adapter/ICryptoServiceListener;)V

    .line 129
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 134
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    :sswitch_a
    const-string v7, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 136
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 137
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->getConnectionInfo(Ljava/lang/String;)Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    move-result-object v2

    .line 138
    .local v2, "_result":Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 139
    if-eqz v2, :cond_3

    .line 140
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    invoke-virtual {v2, p3, v6}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 144
    :cond_3
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 150
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_result":Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    :sswitch_b
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->getConnectionState(Ljava/lang/String;)I

    move-result v2

    .line 154
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 160
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_result":I
    :sswitch_c
    const-string v5, "com.sec.smartcard.adapter.ICryptoService"

    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/smartcard/adapter/ICryptoService$Stub;->getLastUsageTimeStamp()J

    move-result-wide v2

    .line 162
    .local v2, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 163
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
