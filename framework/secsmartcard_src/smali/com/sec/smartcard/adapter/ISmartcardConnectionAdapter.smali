.class public interface abstract Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
.super Ljava/lang/Object;
.source "ISmartcardConnectionAdapter.java"


# virtual methods
.method public abstract connect()Z
.end method

.method public abstract disconnect()V
.end method

.method public abstract getConnectionState()I
.end method

.method public abstract getDetails()Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract registerListener(Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;)V
.end method

.method public abstract unregisterListener(Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapterListener;)V
.end method
