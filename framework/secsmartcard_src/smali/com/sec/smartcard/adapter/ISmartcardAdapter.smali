.class public interface abstract Lcom/sec/smartcard/adapter/ISmartcardAdapter;
.super Ljava/lang/Object;
.source "ISmartcardAdapter.java"


# virtual methods
.method public abstract checkDependency()Z
.end method

.method public abstract getAvailableTransport()Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
.end method

.method public abstract getConnection(Ljava/lang/String;)Lcom/sec/smartcard/adapter/ISmartcardConnectionAdapter;
.end method

.method public abstract getCryptokiInfo()Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;
.end method

.method public abstract getDependencyInfo()[Lcom/sec/smartcard/adapter/AdapterDependencyInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getSupportedConnections()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract onDestroy()Z
.end method

.method public abstract onInit(Landroid/content/Context;Lcom/sec/smartcard/adapter/ISmartcardAdapterCallback;)I
.end method
