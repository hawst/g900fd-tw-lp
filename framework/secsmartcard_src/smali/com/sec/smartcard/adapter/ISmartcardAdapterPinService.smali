.class public interface abstract Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService;
.super Ljava/lang/Object;
.source "ISmartcardAdapterPinService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/adapter/ISmartcardAdapterPinService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getLoginAttemptRemain(Lcom/sec/smartcard/adapter/ISmartcardAdapterInfoCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract isDeviceConnectedWithCard()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract register([CLcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unregister([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterRegisterInfoCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract verify([CLandroid/os/Bundle;Lcom/sec/smartcard/adapter/ISmartcardAdapterVerifyCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
