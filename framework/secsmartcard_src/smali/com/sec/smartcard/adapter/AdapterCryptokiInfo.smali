.class public Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;
.super Ljava/lang/Object;
.source "AdapterCryptokiInfo.java"


# instance fields
.field getunctionListMethod:Ljava/lang/String;

.field libraryPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "libraryPath"    # Ljava/lang/String;
    .param p2, "getunctionListMethod"    # Ljava/lang/String;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->libraryPath:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->getunctionListMethod:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public getGetFunctionListMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->getunctionListMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getLibraryPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->libraryPath:Ljava/lang/String;

    return-object v0
.end method

.method public setGetunctionListMethod(Ljava/lang/String;)V
    .locals 0
    .param p1, "getunctionListMethod"    # Ljava/lang/String;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->getunctionListMethod:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public setLibraryPath(Ljava/lang/String;)V
    .locals 0
    .param p1, "libraryPath"    # Ljava/lang/String;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/smartcard/adapter/AdapterCryptokiInfo;->libraryPath:Ljava/lang/String;

    .line 41
    return-void
.end method
