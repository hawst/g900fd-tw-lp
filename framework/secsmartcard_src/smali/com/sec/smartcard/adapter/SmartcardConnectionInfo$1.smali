.class final Lcom/sec/smartcard/adapter/SmartcardConnectionInfo$1;
.super Ljava/lang/Object;
.source "SmartcardConnectionInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "details":Ljava/lang/String;
    new-instance v2, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    invoke-direct {v2, v1, v0}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 92
    new-array v0, p1, [Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/sec/smartcard/adapter/SmartcardConnectionInfo$1;->newArray(I)[Lcom/sec/smartcard/adapter/SmartcardConnectionInfo;

    move-result-object v0

    return-object v0
.end method
