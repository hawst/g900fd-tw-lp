.class public Lcom/sec/smartcard/pkcs11/PKCS11RuntimeException;
.super Lcom/sec/smartcard/pkcs11/TokenRuntimeException;
.source "PKCS11RuntimeException.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/sec/smartcard/pkcs11/TokenRuntimeException;-><init>()V

    .line 68
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "encapsulatedException"    # Ljava/lang/Exception;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pkcs11/TokenRuntimeException;-><init>(Ljava/lang/Exception;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/smartcard/pkcs11/TokenRuntimeException;-><init>(Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "encapsulatedException"    # Ljava/lang/Exception;

    .prologue
    .line 101
    invoke-direct {p0, p1, p2}, Lcom/sec/smartcard/pkcs11/TokenRuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 102
    return-void
.end method
