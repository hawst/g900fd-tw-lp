.class public Lcom/sec/smartcard/pkcs11/CK_MECHANISM;
.super Ljava/lang/Object;
.source "CK_MECHANISM.java"


# instance fields
.field public mechanism:J

.field public pParameter:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "mechanism"    # J

    .prologue
    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->mechanism:J

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->pParameter:Ljava/lang/Object;

    .line 151
    return-void
.end method

.method public constructor <init>(JLcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "params"    # Lcom/sec/smartcard/pkcs11/CK_ECDH1_DERIVE_PARAMS;

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 204
    return-void
.end method

.method public constructor <init>(JLcom/sec/smartcard/pkcs11/CK_SSL3_KEY_MAT_PARAMS;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "params"    # Lcom/sec/smartcard/pkcs11/CK_SSL3_KEY_MAT_PARAMS;

    .prologue
    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 197
    return-void
.end method

.method public constructor <init>(JLcom/sec/smartcard/pkcs11/CK_SSL3_MASTER_KEY_DERIVE_PARAMS;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "params"    # Lcom/sec/smartcard/pkcs11/CK_SSL3_MASTER_KEY_DERIVE_PARAMS;

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 190
    return-void
.end method

.method public constructor <init>(JLcom/sec/smartcard/pkcs11/CK_VERSION;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "version"    # Lcom/sec/smartcard/pkcs11/CK_VERSION;

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 183
    return-void
.end method

.method public constructor <init>(JLjava/lang/Object;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "pParameter"    # Ljava/lang/Object;

    .prologue
    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 143
    return-void
.end method

.method public constructor <init>(JLjava/math/BigInteger;)V
    .locals 0
    .param p1, "mechanism"    # J
    .param p3, "b"    # Ljava/math/BigInteger;

    .prologue
    .line 172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    return-void
.end method

.method public constructor <init>(J[B)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "pParameter"    # [B

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->init(JLjava/lang/Object;)V

    .line 167
    return-void
.end method

.method private init(JLjava/lang/Object;)V
    .locals 1
    .param p1, "mechanism"    # J
    .param p3, "pParameter"    # Ljava/lang/Object;

    .prologue
    .line 207
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->mechanism:J

    .line 208
    iput-object p3, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->pParameter:Ljava/lang/Object;

    .line 211
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 115
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 117
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 118
    const-string v1, "mechanism: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 119
    iget-wide v2, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->mechanism:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 120
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 122
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 123
    const-string v1, "pParameter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 124
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->pParameter:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 125
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_MECHANISM;->pParameter:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 128
    :goto_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 130
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 131
    const-string v1, "ulParameterLen: ??"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 135
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 127
    :cond_0
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
