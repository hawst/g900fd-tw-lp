.class public Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
.super Ljava/lang/Object;
.source "CK_ATTRIBUTE.java"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field public static final DECRYPT_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final DECRYPT_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final DERIVE_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final ENCRYPT_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final ENCRYPT_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final EXTRACTABLE_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final SENSITIVE_FALSE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final SIGN_RECOVER_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final SIGN_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final TOKEN_FALSE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final UNWRAP_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final UNWRAP_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final VERIFY_RECOVER_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final VERIFY_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final WRAP_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

.field public static final WRAP_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;


# instance fields
.field public pValue:Ljava/lang/Object;

.field public type:J


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x106

    const-wide/16 v8, 0x105

    const-wide/16 v6, 0x104

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 105
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x1

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->TOKEN_FALSE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 110
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x103

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->SENSITIVE_FALSE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 115
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x162

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->EXTRACTABLE_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 120
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v6, v7, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->ENCRYPT_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 125
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v8, v9, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->DECRYPT_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 130
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v10, v11, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->WRAP_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 135
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x107

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->UNWRAP_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 140
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x108

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->SIGN_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 145
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x10a

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->VERIFY_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 150
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x109

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->SIGN_RECOVER_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 155
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x10b

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->VERIFY_RECOVER_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 161
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x10c

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(JZ)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->DERIVE_TRUE:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 166
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v6, v7}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(J)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->ENCRYPT_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 171
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v8, v9}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(J)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->DECRYPT_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 176
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    invoke-direct {v0, v10, v11}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(J)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->WRAP_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    .line 181
    new-instance v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    const-wide/16 v2, 0x107

    invoke-direct {v0, v2, v3}, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;-><init>(J)V

    sput-object v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->UNWRAP_NULL:Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 189
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .param p1, "type"    # J

    .prologue
    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    .line 208
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1
    .param p1, "type"    # J
    .param p3, "value"    # J

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    .line 227
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    .line 228
    return-void
.end method

.method public constructor <init>(JLjava/lang/Object;)V
    .locals 1
    .param p1, "type"    # J
    .param p3, "pValue"    # Ljava/lang/Object;

    .prologue
    .line 196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 197
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    .line 198
    iput-object p3, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    .line 199
    return-void
.end method

.method public constructor <init>(JLjava/math/BigInteger;)V
    .locals 1
    .param p1, "type"    # J
    .param p3, "value"    # Ljava/math/BigInteger;

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 236
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    .line 239
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 1
    .param p1, "type"    # J
    .param p3, "value"    # Z

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 216
    iput-wide p1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    .line 217
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    .line 218
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 309
    const/4 v1, 0x0

    .line 312
    .local v1, "clone":Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;

    move-object v1, v0

    .line 315
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [B

    if-eqz v2, :cond_0

    .line 316
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [B

    check-cast v2, [B

    invoke-virtual {v2}, [B->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    .line 338
    :goto_0
    return-object v1

    .line 317
    :cond_0
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [C

    if-eqz v2, :cond_1

    .line 318
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [C

    check-cast v2, [C

    invoke-virtual {v2}, [C->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 333
    :catch_0
    move-exception v2

    goto :goto_0

    .line 319
    :cond_1
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, Lcom/sec/smartcard/pkcs11/CK_DATE;

    if-eqz v2, :cond_2

    .line 320
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, Lcom/sec/smartcard/pkcs11/CK_DATE;

    invoke-virtual {v2}, Lcom/sec/smartcard/pkcs11/CK_DATE;->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 321
    :cond_2
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [Z

    if-eqz v2, :cond_3

    .line 322
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [Z

    check-cast v2, [Z

    invoke-virtual {v2}, [Z->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 323
    :cond_3
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [I

    if-eqz v2, :cond_4

    .line 324
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [I

    check-cast v2, [I

    invoke-virtual {v2}, [I->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 325
    :cond_4
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [J

    if-eqz v2, :cond_5

    .line 326
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [J

    check-cast v2, [J

    invoke-virtual {v2}, [J->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 327
    :cond_5
    iget-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v2, v2, [Ljava/lang/Object;

    if-eqz v2, :cond_6

    .line 328
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    check-cast v2, [Ljava/lang/Object;

    invoke-virtual {v2}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    goto :goto_0

    .line 331
    :cond_6
    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    iput-object v2, v1, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public getBigInteger()Ljava/math/BigInteger;
    .locals 3

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-nez v0, :cond_0

    .line 248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not a byte[]"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_0
    new-instance v1, Ljava/math/BigInteger;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    invoke-direct {v1, v2, v0}, Ljava/math/BigInteger;-><init>(I[B)V

    return-object v1
.end method

.method public getBoolean()Z
    .locals 3

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a Boolean: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public getByteArray()[B
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v0, v0, [B

    if-nez v0, :cond_0

    .line 284
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not a byte[]"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v0, [B

    check-cast v0, [B

    return-object v0
.end method

.method public getCharArray()[C
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v0, v0, [C

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not a char[]"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v0, [C

    check-cast v0, [C

    return-object v0
.end method

.method public getLong()J
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not a Long: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 298
    :cond_0
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 350
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 352
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 353
    const-string v1, "type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 354
    iget-wide v2, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->type:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    .line 355
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 357
    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 358
    const-string v1, "pValue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 359
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    if-eqz v1, :cond_0

    .line 360
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;->pValue:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 364
    :goto_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Constants;->NEWLINE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 366
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 362
    :cond_0
    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
