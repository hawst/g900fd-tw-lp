.class public Lcom/sec/smartcard/pkcs11/Functions;
.super Ljava/lang/Object;
.source "Functions.java"


# static fields
.field protected static final HEX_DIGITS:[C

.field protected static digestMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static fullEncryptDecryptMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static fullSignVerifyMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static keyDerivationMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static keyGenerationMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static keyPairGenerationMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static mechansimNames_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static signVerifyRecoverMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static singleOperationEncryptDecryptMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static singleOperationSignVerifyMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected static wrapUnwrapMechanisms_:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 152
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->HEX_DIGITS:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static HexStringToLong(Ljava/lang/String;)J
    .locals 6
    .param p0, "value"    # Ljava/lang/String;

    .prologue
    .line 164
    const-wide/16 v0, 0x0

    .line 165
    .local v0, "retValue":J
    move-object v4, p0

    .line 167
    .local v4, "sStr":Ljava/lang/String;
    if-nez p0, :cond_0

    move-wide v2, v0

    .line 174
    .end local v0    # "retValue":J
    .local v2, "retValue":J
    :goto_0
    return-wide v2

    .line 170
    .end local v2    # "retValue":J
    .restart local v0    # "retValue":J
    :cond_0
    const-string v5, "0x"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 171
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    .line 173
    :cond_1
    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    move-wide v2, v0

    .line 174
    .end local v0    # "retValue":J
    .restart local v2    # "retValue":J
    goto :goto_0
.end method

.method public static classTypeToString(J)Ljava/lang/String;
    .locals 4
    .param p0, "classType"    # J

    .prologue
    .line 1050
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-nez v1, :cond_0

    .line 1051
    const-string v0, "CKO_DATA"

    .line 1070
    .local v0, "name":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 1052
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v1, p0, v2

    if-nez v1, :cond_1

    .line 1053
    const-string v0, "CKO_CERTIFICATE"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1054
    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    const-wide/16 v2, 0x2

    cmp-long v1, p0, v2

    if-nez v1, :cond_2

    .line 1055
    const-string v0, "CKO_PUBLIC_KEY"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1056
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    const-wide/16 v2, 0x3

    cmp-long v1, p0, v2

    if-nez v1, :cond_3

    .line 1057
    const-string v0, "CKO_PRIVATE_KEY"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1058
    .end local v0    # "name":Ljava/lang/String;
    :cond_3
    const-wide/16 v2, 0x4

    cmp-long v1, p0, v2

    if-nez v1, :cond_4

    .line 1059
    const-string v0, "CKO_SECRET_KEY"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1060
    .end local v0    # "name":Ljava/lang/String;
    :cond_4
    const-wide/16 v2, 0x5

    cmp-long v1, p0, v2

    if-nez v1, :cond_5

    .line 1061
    const-string v0, "CKO_HW_FEATURE"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1062
    .end local v0    # "name":Ljava/lang/String;
    :cond_5
    const-wide/16 v2, 0x6

    cmp-long v1, p0, v2

    if-nez v1, :cond_6

    .line 1063
    const-string v0, "CKO_DOMAIN_PARAMETERS"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1064
    .end local v0    # "name":Ljava/lang/String;
    :cond_6
    const-wide v2, 0x80000000L

    cmp-long v1, p0, v2

    if-nez v1, :cond_7

    .line 1065
    const-string v0, "CKO_VENDOR_DEFINED"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 1067
    .end local v0    # "name":Ljava/lang/String;
    :cond_7
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR: unknown classType with code: 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0
.end method

.method public static equals(Lcom/sec/smartcard/pkcs11/CK_DATE;Lcom/sec/smartcard/pkcs11/CK_DATE;)Z
    .locals 3
    .param p0, "date1"    # Lcom/sec/smartcard/pkcs11/CK_DATE;
    .param p1, "date2"    # Lcom/sec/smartcard/pkcs11/CK_DATE;

    .prologue
    .line 1201
    const/4 v0, 0x0

    .line 1203
    .local v0, "equal":Z
    if-ne p0, p1, :cond_0

    .line 1204
    const/4 v0, 0x1

    .line 1213
    :goto_0
    return v0

    .line 1205
    :cond_0
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    .line 1206
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    iget-object v2, p1, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    invoke-static {v1, v2}, Lcom/sec/smartcard/pkcs11/Functions;->equals([C[C)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    iget-object v2, p1, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    invoke-static {v1, v2}, Lcom/sec/smartcard/pkcs11/Functions;->equals([C[C)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    iget-object v2, p1, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    invoke-static {v1, v2}, Lcom/sec/smartcard/pkcs11/Functions;->equals([C[C)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1210
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static equals([B[B)Z
    .locals 5
    .param p0, "array1"    # [B
    .param p1, "array2"    # [B

    .prologue
    .line 1087
    const/4 v0, 0x0

    .line 1089
    .local v0, "equal":Z
    if-ne p0, p1, :cond_1

    .line 1090
    const/4 v0, 0x1

    .line 1108
    :cond_0
    :goto_0
    return v0

    .line 1091
    :cond_1
    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    .line 1092
    array-length v2, p0

    .line 1093
    .local v2, "length":I
    array-length v3, p1

    if-ne v2, v3, :cond_3

    .line 1094
    const/4 v0, 0x1

    .line 1095
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1096
    aget-byte v3, p0, v1

    aget-byte v4, p1, v1

    if-eq v3, v4, :cond_2

    .line 1097
    const/4 v0, 0x0

    .line 1098
    goto :goto_0

    .line 1095
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1102
    .end local v1    # "i":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1105
    .end local v2    # "length":I
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static equals([C[C)Z
    .locals 5
    .param p0, "array1"    # [C
    .param p1, "array2"    # [C

    .prologue
    .line 1125
    const/4 v0, 0x0

    .line 1127
    .local v0, "equal":Z
    if-ne p0, p1, :cond_1

    .line 1128
    const/4 v0, 0x1

    .line 1146
    :cond_0
    :goto_0
    return v0

    .line 1129
    :cond_1
    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    .line 1130
    array-length v2, p0

    .line 1131
    .local v2, "length":I
    array-length v3, p1

    if-ne v2, v3, :cond_3

    .line 1132
    const/4 v0, 0x1

    .line 1133
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1134
    aget-char v3, p0, v1

    aget-char v4, p1, v1

    if-eq v3, v4, :cond_2

    .line 1135
    const/4 v0, 0x0

    .line 1136
    goto :goto_0

    .line 1133
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1140
    .end local v1    # "i":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1143
    .end local v2    # "length":I
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static equals([J[J)Z
    .locals 8
    .param p0, "array1"    # [J
    .param p1, "array2"    # [J

    .prologue
    .line 1163
    const/4 v0, 0x0

    .line 1165
    .local v0, "equal":Z
    if-ne p0, p1, :cond_1

    .line 1166
    const/4 v0, 0x1

    .line 1184
    :cond_0
    :goto_0
    return v0

    .line 1167
    :cond_1
    if-eqz p0, :cond_4

    if-eqz p1, :cond_4

    .line 1168
    array-length v2, p0

    .line 1169
    .local v2, "length":I
    array-length v3, p1

    if-ne v2, v3, :cond_3

    .line 1170
    const/4 v0, 0x1

    .line 1171
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1172
    aget-wide v4, p0, v1

    aget-wide v6, p1, v1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_2

    .line 1173
    const/4 v0, 0x0

    .line 1174
    goto :goto_0

    .line 1171
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1178
    .end local v1    # "i":I
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1181
    .end local v2    # "length":I
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getAttributeId(Ljava/lang/String;)J
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1930
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getAttributeName(J)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # J

    .prologue
    .line 1921
    const-string v0, "fix me"

    return-object v0
.end method

.method public static getId(Ljava/util/Map;Ljava/lang/String;)J
    .locals 6
    .param p1, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")J"
        }
    .end annotation

    .prologue
    .line 1865
    .local p0, "idMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1866
    .local v0, "mech":Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 1867
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown name "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1869
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    return-wide v2
.end method

.method public static getKeyId(Ljava/lang/String;)J
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1910
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getKeyName(J)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # J

    .prologue
    .line 1901
    const-string v0, "fix me"

    return-object v0
.end method

.method public static getMechanismId(Ljava/lang/String;)J
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1889
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getMechanismName(J)Ljava/lang/String;
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 1878
    invoke-static {p0, p1}, Lcom/sec/smartcard/pkcs11/Functions;->mechanismCodeToString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getName(Ljava/util/Map;J)Ljava/lang/String;
    .locals 7
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;J)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1850
    .local p0, "nameMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/4 v0, 0x0

    .line 1851
    .local v0, "name":Ljava/lang/String;
    const/16 v1, 0x20

    ushr-long v2, p1, v1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1852
    long-to-int v1, p1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "name":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 1854
    .restart local v0    # "name":Ljava/lang/String;
    :cond_0
    if-nez v0, :cond_1

    .line 1855
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1, p2}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1857
    :cond_1
    return-object v0
.end method

.method public static getObjectClassId(Ljava/lang/String;)J
    .locals 2
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1951
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public static getObjectClassName(J)Ljava/lang/String;
    .locals 1
    .param p0, "id"    # J

    .prologue
    .line 1941
    const-string v0, "fix me"

    return-object v0
.end method

.method public static hashCode(Lcom/sec/smartcard/pkcs11/CK_DATE;)I
    .locals 7
    .param p0, "date"    # Lcom/sec/smartcard/pkcs11/CK_DATE;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0xffff

    .line 1290
    const/4 v0, 0x0

    .line 1292
    .local v0, "hash":I
    if-eqz p0, :cond_2

    .line 1293
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    array-length v1, v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 1294
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    aget-char v1, v1, v4

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    xor-int/2addr v0, v1

    .line 1295
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    aget-char v1, v1, v5

    and-int/2addr v1, v3

    xor-int/2addr v0, v1

    .line 1296
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    aget-char v1, v1, v6

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    xor-int/2addr v0, v1

    .line 1297
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->year:[C

    const/4 v2, 0x3

    aget-char v1, v1, v2

    and-int/2addr v1, v3

    xor-int/2addr v0, v1

    .line 1299
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    array-length v1, v1

    if-ne v1, v6, :cond_1

    .line 1300
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    aget-char v1, v1, v4

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    xor-int/2addr v0, v1

    .line 1301
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->month:[C

    aget-char v1, v1, v5

    and-int/2addr v1, v3

    xor-int/2addr v0, v1

    .line 1303
    :cond_1
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    array-length v1, v1

    if-ne v1, v6, :cond_2

    .line 1304
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    aget-char v1, v1, v4

    and-int/2addr v1, v3

    shl-int/lit8 v1, v1, 0x10

    xor-int/2addr v0, v1

    .line 1305
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/CK_DATE;->day:[C

    aget-char v1, v1, v5

    and-int/2addr v1, v3

    xor-int/2addr v0, v1

    .line 1309
    :cond_2
    return v0
.end method

.method public static hashCode([B)I
    .locals 4
    .param p0, "array"    # [B

    .prologue
    .line 1226
    const/4 v0, 0x0

    .line 1228
    .local v0, "hash":I
    if-eqz p0, :cond_0

    .line 1229
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 1230
    aget-byte v2, p0, v1

    and-int/lit16 v2, v2, 0xff

    rem-int/lit8 v3, v1, 0x4

    shl-int/lit8 v3, v3, 0x3

    shl-int/2addr v2, v3

    xor-int/2addr v0, v2

    .line 1229
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1234
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public static hashCode([C)I
    .locals 3
    .param p0, "array"    # [C

    .prologue
    .line 1246
    const/4 v0, 0x0

    .line 1248
    .local v0, "hash":I
    if-eqz p0, :cond_0

    .line 1249
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 1250
    aget-char v2, p0, v1

    shr-int/lit8 v2, v2, 0x20

    and-int/lit8 v2, v2, -0x1

    xor-int/2addr v0, v2

    .line 1251
    aget-char v2, p0, v1

    and-int/lit8 v2, v2, -0x1

    xor-int/2addr v0, v2

    .line 1249
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1255
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public static hashCode([J)I
    .locals 10
    .param p0, "array"    # [J

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x4

    .line 1268
    const/4 v0, 0x0

    .line 1270
    .local v0, "hash":I
    if-eqz p0, :cond_0

    .line 1271
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    array-length v2, p0

    if-ge v1, v2, :cond_0

    .line 1272
    int-to-long v2, v0

    aget-wide v4, p0, v1

    shr-long/2addr v4, v6

    and-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 1273
    int-to-long v2, v0

    aget-wide v4, p0, v1

    and-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v0, v2

    .line 1271
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1277
    .end local v1    # "i":I
    :cond_0
    return v0
.end method

.method public static isDigestMechanism(J)Z
    .locals 4
    .param p0, "mechanismCode"    # J

    .prologue
    .line 1593
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->digestMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1594
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1595
    .local v0, "digestMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x200

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD2"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1596
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x210

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD5"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1597
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x220

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA_1"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1598
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x230

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1599
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x240

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD160"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1600
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x250

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1601
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x260

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1602
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x270

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1603
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1070

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_FASTHASH"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1604
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->digestMechanisms_:Ljava/util/Hashtable;

    .line 1607
    .end local v0    # "digestMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->digestMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isFullEncryptDecryptMechanism(J)Z
    .locals 10
    .param p0, "mechanismCode"    # J

    .prologue
    const-wide/16 v8, 0x325

    const-wide/16 v6, 0x322

    const-wide/16 v4, 0x321

    .line 1330
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->fullEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1331
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1332
    .local v0, "fullEncryptDecryptMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x101

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1333
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x102

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1334
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x105

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1335
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x111

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC4"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1336
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x121

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1337
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x122

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1338
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x125

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1339
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x132

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1340
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x133

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1341
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x136

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1342
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x141

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1343
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x142

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1344
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x145

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1346
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x150

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_OFB64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1347
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x151

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_OFB8"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1349
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x152

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CFB64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1350
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x153

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CFB8"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1352
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x301

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1353
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x302

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1354
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x305

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1355
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x311

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1356
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x312

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1357
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x315

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1358
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1359
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1360
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1361
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1362
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1363
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1364
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x331

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1365
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x332

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1366
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x335

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1367
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1081

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1368
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1082

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1369
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1085

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1370
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1091

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BLOWFISH_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1371
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1093

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TWOFISH_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1372
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x341

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1373
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x342

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1374
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x345

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1375
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1001

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_ECB64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1376
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1002

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_CBC64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1377
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1003

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_OFB64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1378
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1004

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_CFB64"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1379
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1005

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_CFB32"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1380
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1006

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_CFB16"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1381
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1007

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_CFB8"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1382
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1031

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_ECB128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1383
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1032

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_ECB96"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1384
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1033

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_CBC128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1385
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1034

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_COUNTER"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1386
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1035

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_SHUFFLE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1387
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1061

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_ECB128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1388
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1062

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_CBC128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1389
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1063

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_COUNTER"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1390
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1064

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_SHUFFLE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1391
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->fullEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    .line 1394
    .end local v0    # "fullEncryptDecryptMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->fullEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isFullSignVerifyMechanism(J)Z
    .locals 8
    .param p0, "mechanismCode"    # J

    .prologue
    const-wide/16 v6, 0x324

    const-wide/16 v4, 0x323

    .line 1444
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->fullSignVerifyMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1445
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1446
    .local v0, "fullSignVerifyMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD2_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1447
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x5

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD5_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1448
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x6

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA1_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1449
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x7

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD128_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1450
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x8

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD160_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1451
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0xe

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA1_RSA_PKCS_PSS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1452
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0xc

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA1_RSA_X9_31"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1453
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x12

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DSA_SHA1"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1455
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x40

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1456
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x41

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1457
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x42

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1459
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x43

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256_RSA_PKCS_PSS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1460
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x44

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384_RSA_PKCS_PSS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1461
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x45

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512_RSA_PKCS_PSS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1463
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x103

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1464
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x104

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1465
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x123

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1466
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x124

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1467
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x134

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1468
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x135

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1469
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x143

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1470
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x144

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1471
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x201

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD2_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1472
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x202

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD2_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1473
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x211

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD5_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1474
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x212

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD5_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1475
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x221

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA_1_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1476
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x222

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA_1_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1477
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x231

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD128_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1478
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x232

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD128_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1479
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x241

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD160_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1480
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x242

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RIPEMD160_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1482
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x251

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1483
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x252

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1485
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x261

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1486
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x262

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1488
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x271

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1489
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x272

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512_HMAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1491
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x303

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1492
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x304

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1493
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x313

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1494
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x314

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1495
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1496
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1497
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1498
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1499
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x333

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1500
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x334

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1501
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1083

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1084

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1503
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x343

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1504
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x344

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_MAC_GENERAL"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1505
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x380

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_MD5_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1506
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x381

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_SHA1_MAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1507
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1042

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_ECDSA_SHA1"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1508
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->fullSignVerifyMechanisms_:Ljava/util/Hashtable;

    .line 1511
    .end local v0    # "fullSignVerifyMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->fullSignVerifyMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isKeyDerivationMechanism(J)Z
    .locals 4
    .param p0, "mechanismCode"    # J

    .prologue
    .line 1803
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyDerivationMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1804
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1805
    .local v0, "keyDerivationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x21

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DH_PKCS_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1806
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x360

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CONCATENATE_BASE_AND_KEY"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1807
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x362

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CONCATENATE_BASE_AND_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1808
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x363

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CONCATENATE_DATA_AND_BASE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1809
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x364

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_XOR_BASE_AND_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1810
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x365

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_EXTRACT_KEY_FROM_KEY"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1811
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x371

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_MASTER_KEY_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1812
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x373

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_MASTER_KEY_DERIVE_DH"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1813
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x372

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_KEY_AND_MAC_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1814
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x375

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_MASTER_KEY_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1815
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x377

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_MASTER_KEY_DERIVE_DH"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1816
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x376

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_KEY_AND_MAC_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1818
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x378

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_PRF"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1820
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x390

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD5_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1821
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x391

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_MD2_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1822
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x392

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA1_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1824
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x393

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA256_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1825
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x394

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA384_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1826
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x395

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SHA512_KEY_DERIVATION"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1828
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_MASTER_KEY_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1829
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_WTLS_MASTER_KEY_DERIVE_DH_ECC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d4

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_WTLS_SERVER_KEY_AND_MAC_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1831
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d5

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_WTLS_CLIENT_KEY_AND_MAC_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1832
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_WTLS_PRF"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1011

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_KEA_KEY_DERIVE"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1836
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1100

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_ECB_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1837
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1101

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CBC_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1838
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1102

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_ECB_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1839
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1103

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_CBC_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1840
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1104

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_ECB_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1841
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1105

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_CBC_ENCRYPT_DATA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1843
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->keyDerivationMechanisms_:Ljava/util/Hashtable;

    .line 1846
    .end local v0    # "keyDerivationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyDerivationMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isKeyGenerationMechanism(J)Z
    .locals 10
    .param p0, "mechanismCode"    # J

    .prologue
    const-wide/16 v8, 0x3a5

    const-wide/16 v6, 0x3a4

    const-wide/16 v4, 0x320

    .line 1627
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyGenerationMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1628
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1629
    .local v0, "keyGenerationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x2000

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DSA_PARAMETER_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1630
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x2001

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DH_PKCS_PARAMETER_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1631
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x2002

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_X9_42_DH_PARAMETER_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1632
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x100

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1633
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x110

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC4_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1634
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x120

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1635
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x130

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES2_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1636
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x131

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1637
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x140

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1638
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x300

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1639
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x310

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1640
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1641
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1642
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x330

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1080

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1645
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1090

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BLOWFISH_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1647
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1092

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TWOFISH_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1649
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x340

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1650
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x350

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_GENERIC_SECRET_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1651
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x370

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SSL3_PRE_MASTER_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1652
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x374

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TLS_PRE_MASTER_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1653
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD2_DES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1654
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD5_DES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1655
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD5_CAST_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1656
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD5_CAST3_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1657
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD5_CAST5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1658
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_MD5_CAST128_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1659
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_CAST5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1660
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_CAST128_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1661
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a6

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_RC4_128"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1662
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a7

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_RC4_40"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1663
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a8

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_DES3_EDE_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1664
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3a9

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_DES2_EDE_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1665
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3aa

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_RC2_128_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1666
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3ab

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBE_SHA1_RC2_40_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1667
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3b0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PKCS5_PBKD2"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1668
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3c0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_PBA_SHA1_WITH_SHA1_HMAC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1670
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3d0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_WTLS_PRE_MASTER_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1672
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1000

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1673
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1030

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1674
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1060

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_KEY_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->keyGenerationMechanisms_:Ljava/util/Hashtable;

    .line 1678
    .end local v0    # "keyGenerationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyGenerationMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isKeyPairGenerationMechanism(J)Z
    .locals 8
    .param p0, "mechanismCode"    # J

    .prologue
    const-wide/16 v6, 0x1040

    const-wide/16 v4, 0x20

    .line 1698
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyPairGenerationMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1699
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1700
    .local v0, "keyPairGenerationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x0

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1701
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0xa

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X9_31_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1702
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x10

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DSA_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1703
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DH_PKCS_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1704
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1010

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_KEA_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1705
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_ECDSA_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1706
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_EC_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1707
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DH_PKCS_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1708
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x30

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_X9_42_DH_KEY_PAIR_GEN"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->keyPairGenerationMechanisms_:Ljava/util/Hashtable;

    .line 1712
    .end local v0    # "keyPairGenerationMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->keyPairGenerationMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isSignVerifyRecoverMechanism(J)Z
    .locals 4
    .param p0, "mechanismCode"    # J

    .prologue
    .line 1565
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->signVerifyRecoverMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1566
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1567
    .local v0, "signVerifyRecoverMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1568
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_9796"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1569
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X_509"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1570
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->signVerifyRecoverMechanisms_:Ljava/util/Hashtable;

    .line 1573
    .end local v0    # "signVerifyRecoverMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->signVerifyRecoverMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isSingleOperationEncryptDecryptMechanism(J)Z
    .locals 4
    .param p0, "mechanismCode"    # J

    .prologue
    .line 1415
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1416
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1417
    .local v0, "singleOperationEncryptDecryptMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1418
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x9

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS_OAEP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1419
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X_509"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1420
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    .line 1423
    .end local v0    # "singleOperationEncryptDecryptMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationEncryptDecryptMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isSingleOperationSignVerifyMechanism(J)Z
    .locals 4
    .param p0, "mechanismCode"    # J

    .prologue
    .line 1532
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationSignVerifyMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1533
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1534
    .local v0, "singleOperationSignVerifyMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1535
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0xd

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS_PSS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1536
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x2

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_9796"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1537
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X_509"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1538
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0xb

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X9_31"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1539
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x11

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1540
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1020

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_FORTEZZA_TIMESTAMP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1541
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1041

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_ECDSA"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1542
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationSignVerifyMechanisms_:Ljava/util/Hashtable;

    .line 1545
    .end local v0    # "singleOperationSignVerifyMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->singleOperationSignVerifyMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static isWrapUnwrapMechanism(J)Z
    .locals 10
    .param p0, "mechanismCode"    # J

    .prologue
    const-wide/16 v8, 0x325

    const-wide/16 v6, 0x322

    const-wide/16 v4, 0x321

    .line 1733
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->wrapUnwrapMechanisms_:Ljava/util/Hashtable;

    if-nez v1, :cond_0

    .line 1734
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    .line 1735
    .local v0, "wrapUnwrapMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1736
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x3

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_X_509"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1737
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x9

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RSA_PKCS_OAEP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1738
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x101

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1739
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x102

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x105

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC2_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1741
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x121

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1742
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x122

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1743
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x125

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1744
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x132

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1745
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x133

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1746
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x136

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_DES3_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1747
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x141

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1748
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x142

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1749
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x145

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CDMF_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1750
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x301

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1751
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x302

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1752
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x305

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1753
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x311

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1754
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x312

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1755
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x315

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST3_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1756
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1757
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v4, v5}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1758
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1759
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1760
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST5_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1761
    new-instance v1, Ljava/lang/Long;

    invoke-direct {v1, v8, v9}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_CAST128_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1762
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x331

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x332

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1764
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x335

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_RC5_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1765
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x341

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1766
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x342

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1767
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x345

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_IDEA_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1768
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x400

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_KEY_WRAP_LYNKS"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1769
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x401

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_KEY_WRAP_SET_OAEP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1770
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1008

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_WRAP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1771
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1009

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_PRIVATE_WRAP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1772
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x100a

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_SKIPJACK_RELAYX"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1773
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1036

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BATON_WRAP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1774
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1065

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_JUNIPER_WRAP"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1775
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1081

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_ECB"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1082

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1777
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1085

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_AES_CBC_PAD"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1778
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1091

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_BLOWFISH_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779
    new-instance v1, Ljava/lang/Long;

    const-wide/16 v2, 0x1093

    invoke-direct {v1, v2, v3}, Ljava/lang/Long;-><init>(J)V

    const-string v2, "CKM_TWOFISH_CBC"

    invoke-virtual {v0, v1, v2}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1780
    sput-object v0, Lcom/sec/smartcard/pkcs11/Functions;->wrapUnwrapMechanisms_:Ljava/util/Hashtable;

    .line 1783
    .end local v0    # "wrapUnwrapMechanisms":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    sget-object v1, Lcom/sec/smartcard/pkcs11/Functions;->wrapUnwrapMechanisms_:Ljava/util/Hashtable;

    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static mechanismCodeToString(J)Ljava/lang/String;
    .locals 8
    .param p0, "mechansimCode"    # J

    .prologue
    .line 782
    sget-object v4, Lcom/sec/smartcard/pkcs11/Functions;->mechansimNames_:Ljava/util/Hashtable;

    if-nez v4, :cond_0

    .line 783
    new-instance v3, Ljava/util/Hashtable;

    const/16 v4, 0xc8

    invoke-direct {v3, v4}, Ljava/util/Hashtable;-><init>(I)V

    .line 784
    .local v3, "mechansimNames":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x0

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_PKCS_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 786
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x2

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_9796"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_X_509"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 788
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x4

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD2_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 789
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x5

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD5_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 790
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x6

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA1_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 791
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x7

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD128_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 792
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x8

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD160_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 793
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x9

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_PKCS_OAEP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0xa

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_X9_31_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 795
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0xb

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_X9_31"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 796
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0xc

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA1_RSA_X9_31"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 797
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0xd

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RSA_PKCS_PSS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 798
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0xe

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA1_RSA_PKCS_PSS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 799
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x10

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DSA_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 800
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x11

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DSA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 801
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x12

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DSA_SHA1"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 802
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x20

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DH_PKCS_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 803
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x21

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DH_PKCS_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x30

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_X9_42_DH_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 805
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x31

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_X9_42_DH_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 806
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x32

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_X9_42_DH_HYBRID_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 807
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x33

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_X9_42_MQV_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x40

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x41

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x42

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512_RSA_PKCS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x43

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256_RSA_PKCS_PSS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 814
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x44

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384_RSA_PKCS_PSS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 815
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x45

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512_RSA_PKCS_PSS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 817
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x100

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 818
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x101

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 819
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x102

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 820
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x103

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 821
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x104

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 822
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x105

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC2_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 823
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x110

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC4_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 824
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x111

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC4"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 825
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x120

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x121

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 827
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x122

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 828
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x123

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 829
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x124

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 830
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x125

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 831
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x130

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES2_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 832
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x131

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x132

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x133

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 835
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x134

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 836
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x135

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 837
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x136

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 838
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x140

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 839
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x141

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x142

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 841
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x143

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 842
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x144

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 843
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x145

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CDMF_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 845
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x150

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_OFB64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 846
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x151

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_OFB8"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 848
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x152

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_CFB64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 849
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x153

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_CFB8"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 851
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x200

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD2"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 852
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x201

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD2_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 853
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x202

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD2_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 854
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x210

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD5"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 855
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x211

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD5_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 856
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x212

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD5_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 857
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x220

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA_1"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 858
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x221

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA_1_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 859
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x222

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA_1_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 860
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x230

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 861
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x231

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD128_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 862
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x232

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD128_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x240

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD160"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 864
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x241

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD160_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 865
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x242

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RIPEMD160_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x250

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 868
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x251

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 869
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x252

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 871
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x260

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 872
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x261

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 873
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x262

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 875
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x270

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 876
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x271

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 877
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x272

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512_HMAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 879
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x300

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 880
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x301

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 881
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x302

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 882
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x303

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 883
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x304

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 884
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x305

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 885
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x310

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 886
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x311

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 887
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x312

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 888
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x313

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 889
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x314

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 890
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x315

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST3_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 891
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x320

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 892
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x320

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 893
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x321

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 894
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x321

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 895
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x322

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 896
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x322

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 897
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x323

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 898
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x323

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 899
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x324

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 900
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x324

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 901
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x325

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST5_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 902
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x325

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CAST128_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 903
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x330

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x331

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 905
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x332

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x333

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 907
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x334

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 908
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x335

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_RC5_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 909
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x340

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 910
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x341

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 911
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x342

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 912
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x343

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 913
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x344

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 914
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x345

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_IDEA_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x350

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_GENERIC_SECRET_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 916
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x360

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CONCATENATE_BASE_AND_KEY"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 917
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x362

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CONCATENATE_BASE_AND_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 918
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x363

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CONCATENATE_DATA_AND_BASE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 919
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x364

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_XOR_BASE_AND_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 920
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x365

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_EXTRACT_KEY_FROM_KEY"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 921
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x370

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_PRE_MASTER_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 922
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x371

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_MASTER_KEY_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 923
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x372

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_KEY_AND_MAC_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 924
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x373

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_MASTER_KEY_DERIVE_DH"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 925
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x374

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TLS_PRE_MASTER_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 926
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x375

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TLS_MASTER_KEY_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 927
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x376

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TLS_KEY_AND_MAC_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 928
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x377

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TLS_MASTER_KEY_DERIVE_DH"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 929
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x380

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_MD5_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 930
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x381

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SSL3_SHA1_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x390

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD5_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 932
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x391

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_MD2_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 933
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x392

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA1_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 935
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x393

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA256_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 936
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x394

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA384_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 937
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x395

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SHA512_KEY_DERIVATION"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 939
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a0

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD2_DES_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 940
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a1

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD5_DES_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a2

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD5_CAST_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 942
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a3

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD5_CAST3_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 943
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a4

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD5_CAST5_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a4

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_MD5_CAST128_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 945
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a5

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_CAST5_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 946
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a5

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_CAST128_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 947
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a6

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_RC4_128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 948
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a7

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_RC4_40"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 949
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a8

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_DES3_EDE_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 950
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3a9

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_DES2_EDE_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3aa

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_RC2_128_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 952
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3ab

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBE_SHA1_RC2_40_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 953
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3b0

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PKCS5_PBKD2"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 954
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3c0

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_PBA_SHA1_WITH_SHA1_HMAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 956
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d0

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_PRE_MASTER_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 957
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d1

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_MASTER_KEY_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 958
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d2

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_MASTER_KEY_DERIVE_DH_ECC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 959
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d3

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_PRF"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 960
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d4

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_SERVER_KEY_AND_MAC_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 961
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x3d5

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_WTLS_CLIENT_KEY_AND_MAC_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 963
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x400

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_KEY_WRAP_LYNKS"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 964
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x401

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_KEY_WRAP_SET_OAEP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 966
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x500

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_CMS_SIG"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 968
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1000

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1001

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_ECB64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 970
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1002

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_CBC64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 971
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1003

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_OFB64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 972
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1004

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_CFB64"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 973
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1005

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_CFB32"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 974
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1006

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_CFB16"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 975
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1007

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_CFB8"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 976
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1008

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_WRAP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 977
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1009

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_PRIVATE_WRAP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 978
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x100a

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_SKIPJACK_RELAYX"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 979
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1010

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_KEA_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1011

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_KEA_KEY_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 981
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1020

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_FORTEZZA_TIMESTAMP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 982
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1030

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 983
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1031

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_ECB128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 984
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1032

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_ECB96"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 985
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1033

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_CBC128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 986
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1034

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_COUNTER"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 987
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1035

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_SHUFFLE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 988
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1036

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BATON_WRAP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 989
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1040

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECDSA_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 990
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1040

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_EC_KEY_PAIR_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 991
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1041

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECDSA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 992
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1042

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECDSA_SHA1"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 993
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1050

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECDH1_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 994
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1051

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECDH1_COFACTOR_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 995
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1052

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_ECMQV_DERIVE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 996
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1060

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 997
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1061

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_ECB128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 998
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1062

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_CBC128"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 999
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1063

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_COUNTER"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1000
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1064

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_SHUFFLE"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1001
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1065

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_JUNIPER_WRAP"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1002
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1070

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_FASTHASH"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1003
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1080

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1004
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1081

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_ECB"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1005
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1082

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1006
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1083

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_MAC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1007
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1084

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_MAC_GENERAL"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1008
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1085

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_CBC_PAD"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1010
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1090

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BLOWFISH_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1011
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1091

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_BLOWFISH_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1013
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1092

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TWOFISH_KEY_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1014
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1093

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_TWOFISH_CBC"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1016
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1100

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_ECB_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1017
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1101

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES_CBC_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1018
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1102

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_ECB_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1019
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1103

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DES3_CBC_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1020
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1104

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_ECB_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1021
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x1105

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_AES_CBC_ENCRYPT_DATA"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1023
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x2000

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DSA_PARAMETER_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1024
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x2001

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_DH_PKCS_PARAMETER_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1025
    new-instance v4, Ljava/lang/Long;

    const-wide/16 v6, 0x2002

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_X9_42_DH_PARAMETER_GEN"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1026
    new-instance v4, Ljava/lang/Long;

    const-wide v6, 0x80000000L

    invoke-direct {v4, v6, v7}, Ljava/lang/Long;-><init>(J)V

    const-string v5, "CKM_VENDOR_DEFINED"

    invoke-virtual {v3, v4, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1027
    sput-object v3, Lcom/sec/smartcard/pkcs11/Functions;->mechansimNames_:Ljava/util/Hashtable;

    .line 1030
    .end local v3    # "mechansimNames":Ljava/util/Hashtable;, "Ljava/util/Hashtable<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_0
    new-instance v2, Ljava/lang/Long;

    invoke-direct {v2, p0, p1}, Ljava/lang/Long;-><init>(J)V

    .line 1031
    .local v2, "mechansimCodeObject":Ljava/lang/Long;
    sget-object v4, Lcom/sec/smartcard/pkcs11/Functions;->mechansimNames_:Ljava/util/Hashtable;

    invoke-virtual {v4, v2}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1033
    .local v0, "entry":Ljava/lang/Object;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1037
    .local v1, "mechanismName":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 1033
    .end local v1    # "mechanismName":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknwon mechanism with code: 0x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {p0, p1}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static mechanismInfoFlagsToString(J)Ljava/lang/String;
    .locals 6
    .param p0, "flags"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 572
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 573
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 575
    .local v1, "notFirst":Z
    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 576
    const-string v2, "CKF_HW"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 578
    const/4 v1, 0x1

    .line 581
    :cond_0
    const-wide/16 v2, 0x100

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 582
    if-eqz v1, :cond_1

    .line 583
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 586
    :cond_1
    const-string v2, "CKF_ENCRYPT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 588
    const/4 v1, 0x1

    .line 591
    :cond_2
    const-wide/16 v2, 0x200

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 592
    if-eqz v1, :cond_3

    .line 593
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 596
    :cond_3
    const-string v2, "CKF_DECRYPT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 598
    const/4 v1, 0x1

    .line 601
    :cond_4
    const-wide/16 v2, 0x400

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 602
    if-eqz v1, :cond_5

    .line 603
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 606
    :cond_5
    const-string v2, "CKF_DIGEST"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 608
    const/4 v1, 0x1

    .line 611
    :cond_6
    const-wide/16 v2, 0x800

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 612
    if-eqz v1, :cond_7

    .line 613
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 616
    :cond_7
    const-string v2, "CKF_SIGN"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 618
    const/4 v1, 0x1

    .line 621
    :cond_8
    const-wide/16 v2, 0x1000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 622
    if-eqz v1, :cond_9

    .line 623
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 626
    :cond_9
    const-string v2, "CKF_SIGN_RECOVER"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 628
    const/4 v1, 0x1

    .line 631
    :cond_a
    const-wide/16 v2, 0x2000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 632
    if-eqz v1, :cond_b

    .line 633
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 636
    :cond_b
    const-string v2, "CKF_VERIFY"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 638
    const/4 v1, 0x1

    .line 641
    :cond_c
    const-wide/16 v2, 0x4000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 642
    if-eqz v1, :cond_d

    .line 643
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 646
    :cond_d
    const-string v2, "CKF_VERIFY_RECOVER"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 648
    const/4 v1, 0x1

    .line 651
    :cond_e
    const-wide/32 v2, 0x8000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 652
    if-eqz v1, :cond_f

    .line 653
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 656
    :cond_f
    const-string v2, "CKF_GENERATE"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 658
    const/4 v1, 0x1

    .line 661
    :cond_10
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    .line 662
    if-eqz v1, :cond_11

    .line 663
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 666
    :cond_11
    const-string v2, "CKF_GENERATE_KEY_PAIR"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 668
    const/4 v1, 0x1

    .line 671
    :cond_12
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    .line 672
    if-eqz v1, :cond_13

    .line 673
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 676
    :cond_13
    const-string v2, "CKF_WRAP"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 678
    const/4 v1, 0x1

    .line 681
    :cond_14
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    .line 682
    if-eqz v1, :cond_15

    .line 683
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 686
    :cond_15
    const-string v2, "CKF_UNWRAP"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 688
    const/4 v1, 0x1

    .line 691
    :cond_16
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    .line 692
    if-eqz v1, :cond_17

    .line 693
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 696
    :cond_17
    const-string v2, "CKF_DERIVE"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 698
    const/4 v1, 0x1

    .line 701
    :cond_18
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1a

    .line 702
    if-eqz v1, :cond_19

    .line 703
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 706
    :cond_19
    const-string v2, "CKF_EC_F_P"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 708
    const/4 v1, 0x1

    .line 711
    :cond_1a
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1c

    .line 712
    if-eqz v1, :cond_1b

    .line 713
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 716
    :cond_1b
    const-string v2, "CKF_EC_F_2M"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 718
    const/4 v1, 0x1

    .line 721
    :cond_1c
    const-wide/32 v2, 0x400000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1e

    .line 722
    if-eqz v1, :cond_1d

    .line 723
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 726
    :cond_1d
    const-string v2, "CKF_EC_ECPARAMETERS"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 728
    const/4 v1, 0x1

    .line 731
    :cond_1e
    const-wide/32 v2, 0x800000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_20

    .line 732
    if-eqz v1, :cond_1f

    .line 733
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 736
    :cond_1f
    const-string v2, "CKF_EC_NAMEDCURVE"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 738
    const/4 v1, 0x1

    .line 741
    :cond_20
    const-wide/32 v2, 0x1000000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_22

    .line 742
    if-eqz v1, :cond_21

    .line 743
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 746
    :cond_21
    const-string v2, "CKF_EC_UNCOMPRESS"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 748
    const/4 v1, 0x1

    .line 751
    :cond_22
    const-wide/32 v2, 0x2000000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_24

    .line 752
    if-eqz v1, :cond_23

    .line 753
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 756
    :cond_23
    const-string v2, "CKF_EC_COMPRESS"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 758
    const/4 v1, 0x1

    .line 761
    :cond_24
    const-wide v2, 0x80000000L

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_26

    .line 762
    if-eqz v1, :cond_25

    .line 763
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 766
    :cond_25
    const-string v2, "CKF_EXTENSION"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 768
    const/4 v1, 0x1

    .line 771
    :cond_26
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static sessionInfoFlagsToString(J)Ljava/lang/String;
    .locals 6
    .param p0, "flags"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 517
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 518
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 520
    .local v1, "notFirst":Z
    const-wide/16 v2, 0x2

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 521
    const-string v2, "CKF_RW_SESSION"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 523
    const/4 v1, 0x1

    .line 526
    :cond_0
    const-wide/16 v2, 0x4

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 527
    if-eqz v1, :cond_1

    .line 528
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 531
    :cond_1
    const-string v2, "CKF_SERIAL_SESSION"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 534
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static sessionStateToString(J)Ljava/lang/String;
    .locals 4
    .param p0, "state"    # J

    .prologue
    .line 547
    const-wide/16 v2, 0x0

    cmp-long v1, p0, v2

    if-nez v1, :cond_0

    .line 548
    const-string v0, "CKS_RO_PUBLIC_SESSION"

    .line 561
    .local v0, "name":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 549
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    const-wide/16 v2, 0x1

    cmp-long v1, p0, v2

    if-nez v1, :cond_1

    .line 550
    const-string v0, "CKS_RO_USER_FUNCTIONS"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 551
    .end local v0    # "name":Ljava/lang/String;
    :cond_1
    const-wide/16 v2, 0x2

    cmp-long v1, p0, v2

    if-nez v1, :cond_2

    .line 552
    const-string v0, "CKS_RW_PUBLIC_SESSION"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 553
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    const-wide/16 v2, 0x3

    cmp-long v1, p0, v2

    if-nez v1, :cond_3

    .line 554
    const-string v0, "CKS_RW_USER_FUNCTIONS"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 555
    .end local v0    # "name":Ljava/lang/String;
    :cond_3
    const-wide/16 v2, 0x4

    cmp-long v1, p0, v2

    if-nez v1, :cond_4

    .line 556
    const-string v0, "CKS_RW_SO_FUNCTIONS"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    .line 558
    .end local v0    # "name":Ljava/lang/String;
    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ERROR: unknown session state 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p0, p1}, Lcom/sec/smartcard/pkcs11/Functions;->toFullHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0
.end method

.method public static slotInfoFlagsToString(J)Ljava/lang/String;
    .locals 6
    .param p0, "flags"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 289
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 290
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 292
    .local v1, "notFirst":Z
    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 293
    const-string v2, "CKF_TOKEN_PRESENT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 295
    const/4 v1, 0x1

    .line 298
    :cond_0
    const-wide/16 v2, 0x2

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 299
    if-eqz v1, :cond_1

    .line 300
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 303
    :cond_1
    const-string v2, "CKF_TOKEN_PRESENT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 305
    const/4 v1, 0x1

    .line 308
    :cond_2
    const-wide/16 v2, 0x4

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 309
    if-eqz v1, :cond_3

    .line 310
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 313
    :cond_3
    const-string v2, "CKF_HW_SLOT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 316
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static toBinaryString(J)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 265
    const/4 v0, 0x2

    invoke-static {p0, p1, v0}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toBinaryString([B)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # [B

    .prologue
    .line 276
    new-instance v0, Ljava/math/BigInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p0}, Ljava/math/BigInteger;-><init>(I[B)V

    .line 278
    .local v0, "helpBigInteger":Ljava/math/BigInteger;
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static toFullHexString(I)Ljava/lang/String;
    .locals 6
    .param p0, "value"    # I

    .prologue
    const/16 v5, 0x8

    .line 205
    move v1, p0

    .line 206
    .local v1, "currentValue":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 207
    .local v3, "stringBuffer":Ljava/lang/StringBuffer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_0

    .line 208
    and-int/lit8 v0, v1, 0xf

    .line 209
    .local v0, "currentDigit":I
    sget-object v4, Lcom/sec/smartcard/pkcs11/Functions;->HEX_DIGITS:[C

    aget-char v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 210
    ushr-int/lit8 v1, v1, 0x4

    .line 207
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 213
    .end local v0    # "currentDigit":I
    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->reverse()Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static toFullHexString(J)Ljava/lang/String;
    .locals 8
    .param p0, "value"    # J

    .prologue
    const/16 v6, 0x10

    .line 185
    move-wide v2, p0

    .line 186
    .local v2, "currentValue":J
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v6}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 187
    .local v4, "stringBuffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 188
    long-to-int v5, v2

    and-int/lit8 v0, v5, 0xf

    .line 189
    .local v0, "currentDigit":I
    sget-object v5, Lcom/sec/smartcard/pkcs11/Functions;->HEX_DIGITS:[C

    aget-char v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 190
    const/4 v5, 0x4

    ushr-long/2addr v2, v5

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 193
    .end local v0    # "currentDigit":I
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->reverse()Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static toHexString(J)Ljava/lang/String;
    .locals 2
    .param p0, "value"    # J

    .prologue
    .line 224
    invoke-static {p0, p1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toHexString([B)Ljava/lang/String;
    .locals 5
    .param p0, "value"    # [B

    .prologue
    const/16 v4, 0x10

    .line 237
    if-nez p0, :cond_0

    .line 238
    const/4 v3, 0x0

    .line 254
    :goto_0
    return-object v3

    .line 241
    :cond_0
    new-instance v0, Ljava/lang/StringBuffer;

    array-length v3, p0

    mul-int/lit8 v3, v3, 0x2

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 244
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, p0

    if-ge v1, v3, :cond_2

    .line 245
    aget-byte v3, p0, v1

    and-int/lit16 v2, v3, 0xff

    .line 247
    .local v2, "single":I
    if-ge v2, v4, :cond_1

    .line 248
    const/16 v3, 0x30

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 251
    :cond_1
    invoke-static {v2, v4}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 244
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 254
    .end local v2    # "single":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static tokenInfoFlagsToString(J)Ljava/lang/String;
    .locals 6
    .param p0, "flags"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 327
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 328
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .line 330
    .local v1, "notFirst":Z
    const-wide/16 v2, 0x1

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 331
    const-string v2, "CKF_RNG"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 333
    const/4 v1, 0x1

    .line 336
    :cond_0
    const-wide/16 v2, 0x2

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 337
    if-eqz v1, :cond_1

    .line 338
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 341
    :cond_1
    const-string v2, "CKF_WRITE_PROTECTED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 343
    const/4 v1, 0x1

    .line 346
    :cond_2
    const-wide/16 v2, 0x4

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    .line 347
    if-eqz v1, :cond_3

    .line 348
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 351
    :cond_3
    const-string v2, "CKF_LOGIN_REQUIRED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 353
    const/4 v1, 0x1

    .line 356
    :cond_4
    const-wide/16 v2, 0x8

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    .line 357
    if-eqz v1, :cond_5

    .line 358
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 361
    :cond_5
    const-string v2, "CKF_USER_PIN_INITIALIZED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 363
    const/4 v1, 0x1

    .line 366
    :cond_6
    const-wide/16 v2, 0x20

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    .line 367
    if-eqz v1, :cond_7

    .line 368
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 371
    :cond_7
    const-string v2, "CKF_RESTORE_KEY_NOT_NEEDED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 373
    const/4 v1, 0x1

    .line 376
    :cond_8
    const-wide/16 v2, 0x40

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    .line 377
    if-eqz v1, :cond_9

    .line 378
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 381
    :cond_9
    const-string v2, "CKF_CLOCK_ON_TOKEN"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 383
    const/4 v1, 0x1

    .line 386
    :cond_a
    const-wide/16 v2, 0x100

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    .line 387
    if-eqz v1, :cond_b

    .line 388
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 391
    :cond_b
    const-string v2, "CKF_PROTECTED_AUTHENTICATION_PATH"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 393
    const/4 v1, 0x1

    .line 396
    :cond_c
    const-wide/16 v2, 0x200

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_e

    .line 397
    if-eqz v1, :cond_d

    .line 398
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 401
    :cond_d
    const-string v2, "CKF_DUAL_CRYPTO_OPERATIONS"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 403
    const/4 v1, 0x1

    .line 406
    :cond_e
    const-wide/16 v2, 0x400

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    .line 407
    if-eqz v1, :cond_f

    .line 408
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 411
    :cond_f
    const-string v2, "CKF_TOKEN_INITIALIZED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 413
    const/4 v1, 0x1

    .line 416
    :cond_10
    const-wide/16 v2, 0x800

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    .line 417
    if-eqz v1, :cond_11

    .line 418
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 421
    :cond_11
    const-string v2, "CKF_SECONDARY_AUTHENTICATION"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 423
    const/4 v1, 0x1

    .line 426
    :cond_12
    const-wide/32 v2, 0x10000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    .line 427
    if-eqz v1, :cond_13

    .line 428
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 431
    :cond_13
    const-string v2, "CKF_USER_PIN_COUNT_LOW"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 433
    const/4 v1, 0x1

    .line 436
    :cond_14
    const-wide/32 v2, 0x20000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_16

    .line 437
    if-eqz v1, :cond_15

    .line 438
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 441
    :cond_15
    const-string v2, "CKF_USER_PIN_FINAL_TRY"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 443
    const/4 v1, 0x1

    .line 446
    :cond_16
    const-wide/32 v2, 0x40000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_18

    .line 447
    if-eqz v1, :cond_17

    .line 448
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 451
    :cond_17
    const-string v2, "CKF_USER_PIN_LOCKED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 453
    const/4 v1, 0x1

    .line 456
    :cond_18
    const-wide/32 v2, 0x80000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1a

    .line 457
    if-eqz v1, :cond_19

    .line 458
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 461
    :cond_19
    const-string v2, "CKF_USER_PIN_TO_BE_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 463
    const/4 v1, 0x1

    .line 466
    :cond_1a
    const-wide/32 v2, 0x100000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1c

    .line 467
    if-eqz v1, :cond_1b

    .line 468
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 471
    :cond_1b
    const-string v2, "CKF_SO_PIN_COUNT_LOW"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 473
    const/4 v1, 0x1

    .line 476
    :cond_1c
    const-wide/32 v2, 0x200000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1e

    .line 477
    if-eqz v1, :cond_1d

    .line 478
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 481
    :cond_1d
    const-string v2, "CKF_SO_PIN_FINAL_TRY"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 483
    const/4 v1, 0x1

    .line 486
    :cond_1e
    const-wide/32 v2, 0x400000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_20

    .line 487
    if-eqz v1, :cond_1f

    .line 488
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 491
    :cond_1f
    const-string v2, "CKF_USER_PIN_FINAL_TRY"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 493
    const/4 v1, 0x1

    .line 496
    :cond_20
    const-wide/32 v2, 0x800000

    and-long/2addr v2, p0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_22

    .line 497
    if-eqz v1, :cond_21

    .line 498
    const-string v2, " | "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 501
    :cond_21
    const-string v2, "CKF_USER_PIN_LOCKED"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 503
    const/4 v1, 0x1

    .line 506
    :cond_22
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
