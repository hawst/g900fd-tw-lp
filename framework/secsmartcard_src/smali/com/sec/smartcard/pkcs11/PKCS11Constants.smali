.class public interface abstract Lcom/sec/smartcard/pkcs11/PKCS11Constants;
.super Ljava/lang/Object;
.source "PKCS11Constants.java"


# static fields
.field public static final CKA_AC_ISSUER:J = 0x83L

.field public static final CKA_ALLOWED_MECHANISMS:J = 0x40000600L

.field public static final CKA_ALWAYS_AUTHENTICATE:J = 0x202L

.field public static final CKA_ALWAYS_SENSITIVE:J = 0x165L

.field public static final CKA_APPLICATION:J = 0x10L

.field public static final CKA_ATTR_TYPES:J = 0x85L

.field public static final CKA_AUTH_PIN_FLAGS:J = 0x201L

.field public static final CKA_BASE:J = 0x132L

.field public static final CKA_BITS_PER_PIXEL:J = 0x406L

.field public static final CKA_CERTIFICATE_CATEGORY:J = 0x87L

.field public static final CKA_CERTIFICATE_TYPE:J = 0x80L

.field public static final CKA_CHAR_COLUMNS:J = 0x404L

.field public static final CKA_CHAR_ROWS:J = 0x403L

.field public static final CKA_CHAR_SETS:J = 0x480L

.field public static final CKA_CHECK_VALUE:J = 0x90L

.field public static final CKA_CLASS:J = 0x0L

.field public static final CKA_COEFFICIENT:J = 0x128L

.field public static final CKA_COLOR:J = 0x405L

.field public static final CKA_DECRYPT:J = 0x105L

.field public static final CKA_DEFAULT_CMS_ATTRIBUTES:J = 0x502L

.field public static final CKA_DERIVE:J = 0x10cL

.field public static final CKA_ECDSA_PARAMS:J = 0x180L

.field public static final CKA_EC_PARAMS:J = 0x180L

.field public static final CKA_EC_POINT:J = 0x181L

.field public static final CKA_ENCODING_METHODS:J = 0x481L

.field public static final CKA_ENCRYPT:J = 0x104L

.field public static final CKA_END_DATE:J = 0x111L

.field public static final CKA_EXPONENT_1:J = 0x126L

.field public static final CKA_EXPONENT_2:J = 0x127L

.field public static final CKA_EXTRACTABLE:J = 0x162L

.field public static final CKA_HASH_OF_ISSUER_PUBLIC_KEY:J = 0x8bL

.field public static final CKA_HASH_OF_SUBJECT_PUBLIC_KEY:J = 0x8aL

.field public static final CKA_HAS_RESET:J = 0x302L

.field public static final CKA_HW_FEATURE_TYPE:J = 0x300L

.field public static final CKA_ID:J = 0x102L

.field public static final CKA_ISSUER:J = 0x81L

.field public static final CKA_JAVA_MIDP_SECURITY_DOMAIN:J = 0x88L

.field public static final CKA_KEY_GEN_MECHANISM:J = 0x166L

.field public static final CKA_KEY_TYPE:J = 0x100L

.field public static final CKA_LABEL:J = 0x3L

.field public static final CKA_LOCAL:J = 0x163L

.field public static final CKA_MECHANISM_TYPE:J = 0x500L

.field public static final CKA_MIME_TYPES:J = 0x482L

.field public static final CKA_MODIFIABLE:J = 0x170L

.field public static final CKA_MODULUS:J = 0x120L

.field public static final CKA_MODULUS_BITS:J = 0x121L

.field public static final CKA_NEVER_EXTRACTABLE:J = 0x164L

.field public static final CKA_OBJECT_ID:J = 0x12L

.field public static final CKA_OWNER:J = 0x84L

.field public static final CKA_PIXEL_X:J = 0x400L

.field public static final CKA_PIXEL_Y:J = 0x401L

.field public static final CKA_PRIME:J = 0x130L

.field public static final CKA_PRIME_1:J = 0x124L

.field public static final CKA_PRIME_2:J = 0x125L

.field public static final CKA_PRIME_BITS:J = 0x133L

.field public static final CKA_PRIVATE:J = 0x2L

.field public static final CKA_PRIVATE_EXPONENT:J = 0x123L

.field public static final CKA_PUBLIC_EXPONENT:J = 0x122L

.field public static final CKA_REQUIRED_CMS_ATTRIBUTES:J = 0x501L

.field public static final CKA_RESET_ON_INIT:J = 0x301L

.field public static final CKA_RESOLUTION:J = 0x402L

.field public static final CKA_SECONDARY_AUTH:J = 0x200L

.field public static final CKA_SENSITIVE:J = 0x103L

.field public static final CKA_SERIAL_NUMBER:J = 0x82L

.field public static final CKA_SIGN:J = 0x108L

.field public static final CKA_SIGN_RECOVER:J = 0x109L

.field public static final CKA_START_DATE:J = 0x110L

.field public static final CKA_SUBJECT:J = 0x101L

.field public static final CKA_SUBPRIME:J = 0x131L

.field public static final CKA_SUB_PRIME_BITS:J = 0x134L

.field public static final CKA_SUPPORTED_CMS_ATTRIBUTES:J = 0x503L

.field public static final CKA_TOKEN:J = 0x1L

.field public static final CKA_TRUSTED:J = 0x86L

.field public static final CKA_UNWRAP:J = 0x107L

.field public static final CKA_UNWRAP_TEMPLATE:J = 0x40000212L

.field public static final CKA_URL:J = 0x89L

.field public static final CKA_VALUE:J = 0x11L

.field public static final CKA_VALUE_BITS:J = 0x160L

.field public static final CKA_VALUE_LEN:J = 0x161L

.field public static final CKA_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKA_VERIFY:J = 0x10aL

.field public static final CKA_VERIFY_RECOVER:J = 0x10bL

.field public static final CKA_WRAP:J = 0x106L

.field public static final CKA_WRAP_TEMPLATE:J = 0x40000211L

.field public static final CKA_WRAP_WITH_TRUSTED:J = 0x210L

.field public static final CKC_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKC_WTLS:J = 0x2L

.field public static final CKC_X_509:J = 0x0L

.field public static final CKC_X_509_ATTR_CERT:J = 0x1L

.field public static final CKD_NULL:J = 0x1L

.field public static final CKD_SHA1_KDF:J = 0x2L

.field public static final CKD_SHA1_KDF_ASN1:J = 0x3L

.field public static final CKD_SHA1_KDF_CONCATENATE:J = 0x4L

.field public static final CKF_ARRAY_ATTRIBUTE:J = 0x40000000L

.field public static final CKF_CLOCK_ON_TOKEN:J = 0x40L

.field public static final CKF_DECRYPT:J = 0x200L

.field public static final CKF_DERIVE:J = 0x80000L

.field public static final CKF_DIGEST:J = 0x400L

.field public static final CKF_DONT_BLOCK:J = 0x1L

.field public static final CKF_DUAL_CRYPTO_OPERATIONS:J = 0x200L

.field public static final CKF_EC_COMPRESS:J = 0x2000000L

.field public static final CKF_EC_ECPARAMETERS:J = 0x400000L

.field public static final CKF_EC_F_2M:J = 0x200000L

.field public static final CKF_EC_F_P:J = 0x100000L

.field public static final CKF_EC_NAMEDCURVE:J = 0x800000L

.field public static final CKF_EC_UNCOMPRESS:J = 0x1000000L

.field public static final CKF_ENCRYPT:J = 0x100L

.field public static final CKF_EXTENSION:J = 0x80000000L

.field public static final CKF_GENERATE:J = 0x8000L

.field public static final CKF_GENERATE_KEY_PAIR:J = 0x10000L

.field public static final CKF_HW:J = 0x1L

.field public static final CKF_HW_SLOT:J = 0x4L

.field public static final CKF_LIBRARY_CANT_CREATE_OS_THREADS:J = 0x1L

.field public static final CKF_LOGIN_REQUIRED:J = 0x4L

.field public static final CKF_OS_LOCKING_OK:J = 0x2L

.field public static final CKF_PROTECTED_AUTHENTICATION_PATH:J = 0x100L

.field public static final CKF_REMOVABLE_DEVICE:J = 0x2L

.field public static final CKF_RESTORE_KEY_NOT_NEEDED:J = 0x20L

.field public static final CKF_RNG:J = 0x1L

.field public static final CKF_RW_SESSION:J = 0x2L

.field public static final CKF_SECONDARY_AUTHENTICATION:J = 0x800L

.field public static final CKF_SERIAL_SESSION:J = 0x4L

.field public static final CKF_SIGN:J = 0x800L

.field public static final CKF_SIGN_RECOVER:J = 0x1000L

.field public static final CKF_SO_PIN_COUNT_LOW:J = 0x100000L

.field public static final CKF_SO_PIN_FINAL_TRY:J = 0x200000L

.field public static final CKF_SO_PIN_LOCKED:J = 0x400000L

.field public static final CKF_SO_PIN_TO_BE_CHANGED:J = 0x800000L

.field public static final CKF_TOKEN_INITIALIZED:J = 0x400L

.field public static final CKF_TOKEN_PRESENT:J = 0x1L

.field public static final CKF_UNWRAP:J = 0x40000L

.field public static final CKF_USER_PIN_COUNT_LOW:J = 0x10000L

.field public static final CKF_USER_PIN_FINAL_TRY:J = 0x20000L

.field public static final CKF_USER_PIN_INITIALIZED:J = 0x8L

.field public static final CKF_USER_PIN_LOCKED:J = 0x40000L

.field public static final CKF_USER_PIN_TO_BE_CHANGED:J = 0x80000L

.field public static final CKF_VERIFY:J = 0x2000L

.field public static final CKF_VERIFY_RECOVER:J = 0x4000L

.field public static final CKF_WRAP:J = 0x20000L

.field public static final CKF_WRITE_PROTECTED:J = 0x2L

.field public static final CKG_MGF1_SHA1:J = 0x1L

.field public static final CKG_MGF1_SHA256:J = 0x2L

.field public static final CKG_MGF1_SHA384:J = 0x3L

.field public static final CKG_MGF1_SHA512:J = 0x4L

.field public static final CKH_CLOCK:J = 0x2L

.field public static final CKH_MONOTONIC_COUNTER:J = 0x1L

.field public static final CKH_USER_INTERFACE:J = 0x3L

.field public static final CKH_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKK_AES:J = 0x1fL

.field public static final CKK_BATON:J = 0x1cL

.field public static final CKK_BLOWFISH:J = 0x20L

.field public static final CKK_CAST:J = 0x16L

.field public static final CKK_CAST128:J = 0x18L

.field public static final CKK_CAST3:J = 0x17L

.field public static final CKK_CAST5:J = 0x18L

.field public static final CKK_CDMF:J = 0x1eL

.field public static final CKK_DES:J = 0x13L

.field public static final CKK_DES2:J = 0x14L

.field public static final CKK_DES3:J = 0x15L

.field public static final CKK_DH:J = 0x2L

.field public static final CKK_DSA:J = 0x1L

.field public static final CKK_EC:J = 0x3L

.field public static final CKK_ECDSA:J = 0x3L

.field public static final CKK_GENERIC_SECRET:J = 0x10L

.field public static final CKK_IDEA:J = 0x1aL

.field public static final CKK_JUNIPER:J = 0x1dL

.field public static final CKK_KEA:J = 0x5L

.field public static final CKK_RC2:J = 0x11L

.field public static final CKK_RC4:J = 0x12L

.field public static final CKK_RC5:J = 0x19L

.field public static final CKK_RSA:J = 0x0L

.field public static final CKK_SKIPJACK:J = 0x1bL

.field public static final CKK_TWOFISH:J = 0x21L

.field public static final CKK_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKK_X9_42_DH:J = 0x4L

.field public static final CKM_AES_CBC:J = 0x1082L

.field public static final CKM_AES_CBC_ENCRYPT_DATA:J = 0x1105L

.field public static final CKM_AES_CBC_PAD:J = 0x1085L

.field public static final CKM_AES_ECB:J = 0x1081L

.field public static final CKM_AES_ECB_ENCRYPT_DATA:J = 0x1104L

.field public static final CKM_AES_KEY_GEN:J = 0x1080L

.field public static final CKM_AES_MAC:J = 0x1083L

.field public static final CKM_AES_MAC_GENERAL:J = 0x1084L

.field public static final CKM_BATON_CBC128:J = 0x1033L

.field public static final CKM_BATON_COUNTER:J = 0x1034L

.field public static final CKM_BATON_ECB128:J = 0x1031L

.field public static final CKM_BATON_ECB96:J = 0x1032L

.field public static final CKM_BATON_KEY_GEN:J = 0x1030L

.field public static final CKM_BATON_SHUFFLE:J = 0x1035L

.field public static final CKM_BATON_WRAP:J = 0x1036L

.field public static final CKM_BLOWFISH_CBC:J = 0x1091L

.field public static final CKM_BLOWFISH_KEY_GEN:J = 0x1090L

.field public static final CKM_CAST128_CBC:J = 0x322L

.field public static final CKM_CAST128_CBC_PAD:J = 0x325L

.field public static final CKM_CAST128_ECB:J = 0x321L

.field public static final CKM_CAST128_KEY_GEN:J = 0x320L

.field public static final CKM_CAST128_MAC:J = 0x323L

.field public static final CKM_CAST128_MAC_GENERAL:J = 0x324L

.field public static final CKM_CAST3_CBC:J = 0x312L

.field public static final CKM_CAST3_CBC_PAD:J = 0x315L

.field public static final CKM_CAST3_ECB:J = 0x311L

.field public static final CKM_CAST3_KEY_GEN:J = 0x310L

.field public static final CKM_CAST3_MAC:J = 0x313L

.field public static final CKM_CAST3_MAC_GENERAL:J = 0x314L

.field public static final CKM_CAST5_CBC:J = 0x322L

.field public static final CKM_CAST5_CBC_PAD:J = 0x325L

.field public static final CKM_CAST5_ECB:J = 0x321L

.field public static final CKM_CAST5_KEY_GEN:J = 0x320L

.field public static final CKM_CAST5_MAC:J = 0x323L

.field public static final CKM_CAST5_MAC_GENERAL:J = 0x324L

.field public static final CKM_CAST_CBC:J = 0x302L

.field public static final CKM_CAST_CBC_PAD:J = 0x305L

.field public static final CKM_CAST_ECB:J = 0x301L

.field public static final CKM_CAST_KEY_GEN:J = 0x300L

.field public static final CKM_CAST_MAC:J = 0x303L

.field public static final CKM_CAST_MAC_GENERAL:J = 0x304L

.field public static final CKM_CDMF_CBC:J = 0x142L

.field public static final CKM_CDMF_CBC_PAD:J = 0x145L

.field public static final CKM_CDMF_ECB:J = 0x141L

.field public static final CKM_CDMF_KEY_GEN:J = 0x140L

.field public static final CKM_CDMF_MAC:J = 0x143L

.field public static final CKM_CDMF_MAC_GENERAL:J = 0x144L

.field public static final CKM_CMS_SIG:J = 0x500L

.field public static final CKM_CONCATENATE_BASE_AND_DATA:J = 0x362L

.field public static final CKM_CONCATENATE_BASE_AND_KEY:J = 0x360L

.field public static final CKM_CONCATENATE_DATA_AND_BASE:J = 0x363L

.field public static final CKM_DES2_KEY_GEN:J = 0x130L

.field public static final CKM_DES3_CBC:J = 0x133L

.field public static final CKM_DES3_CBC_ENCRYPT_DATA:J = 0x1103L

.field public static final CKM_DES3_CBC_PAD:J = 0x136L

.field public static final CKM_DES3_ECB:J = 0x132L

.field public static final CKM_DES3_ECB_ENCRYPT_DATA:J = 0x1102L

.field public static final CKM_DES3_KEY_GEN:J = 0x131L

.field public static final CKM_DES3_MAC:J = 0x134L

.field public static final CKM_DES3_MAC_GENERAL:J = 0x135L

.field public static final CKM_DES_CBC:J = 0x122L

.field public static final CKM_DES_CBC_ENCRYPT_DATA:J = 0x1101L

.field public static final CKM_DES_CBC_PAD:J = 0x125L

.field public static final CKM_DES_CFB64:J = 0x152L

.field public static final CKM_DES_CFB8:J = 0x153L

.field public static final CKM_DES_ECB:J = 0x121L

.field public static final CKM_DES_ECB_ENCRYPT_DATA:J = 0x1100L

.field public static final CKM_DES_KEY_GEN:J = 0x120L

.field public static final CKM_DES_MAC:J = 0x123L

.field public static final CKM_DES_MAC_GENERAL:J = 0x124L

.field public static final CKM_DES_OFB64:J = 0x150L

.field public static final CKM_DES_OFB8:J = 0x151L

.field public static final CKM_DH_PKCS_DERIVE:J = 0x21L

.field public static final CKM_DH_PKCS_KEY_PAIR_GEN:J = 0x20L

.field public static final CKM_DH_PKCS_PARAMETER_GEN:J = 0x2001L

.field public static final CKM_DSA:J = 0x11L

.field public static final CKM_DSA_KEY_PAIR_GEN:J = 0x10L

.field public static final CKM_DSA_PARAMETER_GEN:J = 0x2000L

.field public static final CKM_DSA_SHA1:J = 0x12L

.field public static final CKM_ECDH1_COFACTOR_DERIVE:J = 0x1051L

.field public static final CKM_ECDH1_DERIVE:J = 0x1050L

.field public static final CKM_ECDSA:J = 0x1041L

.field public static final CKM_ECDSA_KEY_PAIR_GEN:J = 0x1040L

.field public static final CKM_ECDSA_SHA1:J = 0x1042L

.field public static final CKM_ECMQV_DERIVE:J = 0x1052L

.field public static final CKM_EC_KEY_PAIR_GEN:J = 0x1040L

.field public static final CKM_EXTRACT_KEY_FROM_KEY:J = 0x365L

.field public static final CKM_FASTHASH:J = 0x1070L

.field public static final CKM_FORTEZZA_TIMESTAMP:J = 0x1020L

.field public static final CKM_GENERIC_SECRET_KEY_GEN:J = 0x350L

.field public static final CKM_IDEA_CBC:J = 0x342L

.field public static final CKM_IDEA_CBC_PAD:J = 0x345L

.field public static final CKM_IDEA_ECB:J = 0x341L

.field public static final CKM_IDEA_KEY_GEN:J = 0x340L

.field public static final CKM_IDEA_MAC:J = 0x343L

.field public static final CKM_IDEA_MAC_GENERAL:J = 0x344L

.field public static final CKM_JUNIPER_CBC128:J = 0x1062L

.field public static final CKM_JUNIPER_COUNTER:J = 0x1063L

.field public static final CKM_JUNIPER_ECB128:J = 0x1061L

.field public static final CKM_JUNIPER_KEY_GEN:J = 0x1060L

.field public static final CKM_JUNIPER_SHUFFLE:J = 0x1064L

.field public static final CKM_JUNIPER_WRAP:J = 0x1065L

.field public static final CKM_KEA_KEY_DERIVE:J = 0x1011L

.field public static final CKM_KEA_KEY_PAIR_GEN:J = 0x1010L

.field public static final CKM_KEY_WRAP_LYNKS:J = 0x400L

.field public static final CKM_KEY_WRAP_SET_OAEP:J = 0x401L

.field public static final CKM_MD2:J = 0x200L

.field public static final CKM_MD2_HMAC:J = 0x201L

.field public static final CKM_MD2_HMAC_GENERAL:J = 0x202L

.field public static final CKM_MD2_KEY_DERIVATION:J = 0x391L

.field public static final CKM_MD2_RSA_PKCS:J = 0x4L

.field public static final CKM_MD5:J = 0x210L

.field public static final CKM_MD5_HMAC:J = 0x211L

.field public static final CKM_MD5_HMAC_GENERAL:J = 0x212L

.field public static final CKM_MD5_KEY_DERIVATION:J = 0x390L

.field public static final CKM_MD5_RSA_PKCS:J = 0x5L

.field public static final CKM_PBA_SHA1_WITH_SHA1_HMAC:J = 0x3c0L

.field public static final CKM_PBE_MD2_DES_CBC:J = 0x3a0L

.field public static final CKM_PBE_MD5_CAST128_CBC:J = 0x3a4L

.field public static final CKM_PBE_MD5_CAST3_CBC:J = 0x3a3L

.field public static final CKM_PBE_MD5_CAST5_CBC:J = 0x3a4L

.field public static final CKM_PBE_MD5_CAST_CBC:J = 0x3a2L

.field public static final CKM_PBE_MD5_DES_CBC:J = 0x3a1L

.field public static final CKM_PBE_SHA1_CAST128_CBC:J = 0x3a5L

.field public static final CKM_PBE_SHA1_CAST5_CBC:J = 0x3a5L

.field public static final CKM_PBE_SHA1_DES2_EDE_CBC:J = 0x3a9L

.field public static final CKM_PBE_SHA1_DES3_EDE_CBC:J = 0x3a8L

.field public static final CKM_PBE_SHA1_RC2_128_CBC:J = 0x3aaL

.field public static final CKM_PBE_SHA1_RC2_40_CBC:J = 0x3abL

.field public static final CKM_PBE_SHA1_RC4_128:J = 0x3a6L

.field public static final CKM_PBE_SHA1_RC4_40:J = 0x3a7L

.field public static final CKM_PKCS5_PBKD2:J = 0x3b0L

.field public static final CKM_RC2_CBC:J = 0x102L

.field public static final CKM_RC2_CBC_PAD:J = 0x105L

.field public static final CKM_RC2_ECB:J = 0x101L

.field public static final CKM_RC2_KEY_GEN:J = 0x100L

.field public static final CKM_RC2_MAC:J = 0x103L

.field public static final CKM_RC2_MAC_GENERAL:J = 0x104L

.field public static final CKM_RC4:J = 0x111L

.field public static final CKM_RC4_KEY_GEN:J = 0x110L

.field public static final CKM_RC5_CBC:J = 0x332L

.field public static final CKM_RC5_CBC_PAD:J = 0x335L

.field public static final CKM_RC5_ECB:J = 0x331L

.field public static final CKM_RC5_KEY_GEN:J = 0x330L

.field public static final CKM_RC5_MAC:J = 0x333L

.field public static final CKM_RC5_MAC_GENERAL:J = 0x334L

.field public static final CKM_RIPEMD128:J = 0x230L

.field public static final CKM_RIPEMD128_HMAC:J = 0x231L

.field public static final CKM_RIPEMD128_HMAC_GENERAL:J = 0x232L

.field public static final CKM_RIPEMD128_RSA_PKCS:J = 0x7L

.field public static final CKM_RIPEMD160:J = 0x240L

.field public static final CKM_RIPEMD160_HMAC:J = 0x241L

.field public static final CKM_RIPEMD160_HMAC_GENERAL:J = 0x242L

.field public static final CKM_RIPEMD160_RSA_PKCS:J = 0x8L

.field public static final CKM_RSA_9796:J = 0x2L

.field public static final CKM_RSA_PKCS:J = 0x1L

.field public static final CKM_RSA_PKCS_KEY_PAIR_GEN:J = 0x0L

.field public static final CKM_RSA_PKCS_OAEP:J = 0x9L

.field public static final CKM_RSA_PKCS_PSS:J = 0xdL

.field public static final CKM_RSA_X9_31:J = 0xbL

.field public static final CKM_RSA_X9_31_KEY_PAIR_GEN:J = 0xaL

.field public static final CKM_RSA_X_509:J = 0x3L

.field public static final CKM_SHA1_KEY_DERIVATION:J = 0x392L

.field public static final CKM_SHA1_RSA_PKCS:J = 0x6L

.field public static final CKM_SHA1_RSA_PKCS_PSS:J = 0xeL

.field public static final CKM_SHA1_RSA_X9_31:J = 0xcL

.field public static final CKM_SHA256:J = 0x250L

.field public static final CKM_SHA256_HMAC:J = 0x251L

.field public static final CKM_SHA256_HMAC_GENERAL:J = 0x252L

.field public static final CKM_SHA256_KEY_DERIVATION:J = 0x393L

.field public static final CKM_SHA256_RSA_PKCS:J = 0x40L

.field public static final CKM_SHA256_RSA_PKCS_PSS:J = 0x43L

.field public static final CKM_SHA384:J = 0x260L

.field public static final CKM_SHA384_HMAC:J = 0x261L

.field public static final CKM_SHA384_HMAC_GENERAL:J = 0x262L

.field public static final CKM_SHA384_KEY_DERIVATION:J = 0x394L

.field public static final CKM_SHA384_RSA_PKCS:J = 0x41L

.field public static final CKM_SHA384_RSA_PKCS_PSS:J = 0x44L

.field public static final CKM_SHA512:J = 0x270L

.field public static final CKM_SHA512_HMAC:J = 0x271L

.field public static final CKM_SHA512_HMAC_GENERAL:J = 0x272L

.field public static final CKM_SHA512_KEY_DERIVATION:J = 0x395L

.field public static final CKM_SHA512_RSA_PKCS:J = 0x42L

.field public static final CKM_SHA512_RSA_PKCS_PSS:J = 0x45L

.field public static final CKM_SHA_1:J = 0x220L

.field public static final CKM_SHA_1_HMAC:J = 0x221L

.field public static final CKM_SHA_1_HMAC_GENERAL:J = 0x222L

.field public static final CKM_SKIPJACK_CBC64:J = 0x1002L

.field public static final CKM_SKIPJACK_CFB16:J = 0x1006L

.field public static final CKM_SKIPJACK_CFB32:J = 0x1005L

.field public static final CKM_SKIPJACK_CFB64:J = 0x1004L

.field public static final CKM_SKIPJACK_CFB8:J = 0x1007L

.field public static final CKM_SKIPJACK_ECB64:J = 0x1001L

.field public static final CKM_SKIPJACK_KEY_GEN:J = 0x1000L

.field public static final CKM_SKIPJACK_OFB64:J = 0x1003L

.field public static final CKM_SKIPJACK_PRIVATE_WRAP:J = 0x1009L

.field public static final CKM_SKIPJACK_RELAYX:J = 0x100aL

.field public static final CKM_SKIPJACK_WRAP:J = 0x1008L

.field public static final CKM_SSL3_KEY_AND_MAC_DERIVE:J = 0x372L

.field public static final CKM_SSL3_MASTER_KEY_DERIVE:J = 0x371L

.field public static final CKM_SSL3_MASTER_KEY_DERIVE_DH:J = 0x373L

.field public static final CKM_SSL3_MD5_MAC:J = 0x380L

.field public static final CKM_SSL3_PRE_MASTER_KEY_GEN:J = 0x370L

.field public static final CKM_SSL3_SHA1_MAC:J = 0x381L

.field public static final CKM_TLS_KEY_AND_MAC_DERIVE:J = 0x376L

.field public static final CKM_TLS_MASTER_KEY_DERIVE:J = 0x375L

.field public static final CKM_TLS_MASTER_KEY_DERIVE_DH:J = 0x377L

.field public static final CKM_TLS_PRE_MASTER_KEY_GEN:J = 0x374L

.field public static final CKM_TLS_PRF:J = 0x378L

.field public static final CKM_TWOFISH_CBC:J = 0x1093L

.field public static final CKM_TWOFISH_KEY_GEN:J = 0x1092L

.field public static final CKM_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKM_WTLS_CLIENT_KEY_AND_MAC_DERIVE:J = 0x3d5L

.field public static final CKM_WTLS_MASTER_KEY_DERIVE:J = 0x3d1L

.field public static final CKM_WTLS_MASTER_KEY_DERIVE_DH_ECC:J = 0x3d2L

.field public static final CKM_WTLS_MASTER_KEY_DERVIE_DH_ECC:J = 0x3d2L

.field public static final CKM_WTLS_PRE_MASTER_KEY_GEN:J = 0x3d0L

.field public static final CKM_WTLS_PRF:J = 0x3d3L

.field public static final CKM_WTLS_SERVER_KEY_AND_MAC_DERIVE:J = 0x3d4L

.field public static final CKM_X9_42_DH_DERIVE:J = 0x31L

.field public static final CKM_X9_42_DH_HYBRID_DERIVE:J = 0x32L

.field public static final CKM_X9_42_DH_KEY_PAIR_GEN:J = 0x30L

.field public static final CKM_X9_42_DH_PARAMETER_GEN:J = 0x2002L

.field public static final CKM_X9_42_MQV_DERIVE:J = 0x33L

.field public static final CKM_XOR_BASE_AND_DATA:J = 0x364L

.field public static final CKN_SURRENDER:J = 0x0L

.field public static final CKO_CERTIFICATE:J = 0x1L

.field public static final CKO_DATA:J = 0x0L

.field public static final CKO_DOMAIN_PARAMETERS:J = 0x6L

.field public static final CKO_HW_FEATURE:J = 0x5L

.field public static final CKO_MECHANISM:J = 0x7L

.field public static final CKO_PRIVATE_KEY:J = 0x3L

.field public static final CKO_PUBLIC_KEY:J = 0x2L

.field public static final CKO_SECRET_KEY:J = 0x4L

.field public static final CKO_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKP_PKCS5_PBKD2_HMAC_SHA1:J = 0x1L

.field public static final CKR_ARGUMENTS_BAD:J = 0x7L

.field public static final CKR_ATTRIBUTE_READ_ONLY:J = 0x10L

.field public static final CKR_ATTRIBUTE_SENSITIVE:J = 0x11L

.field public static final CKR_ATTRIBUTE_TYPE_INVALID:J = 0x12L

.field public static final CKR_ATTRIBUTE_VALUE_INVALID:J = 0x13L

.field public static final CKR_BUFFER_TOO_SMALL:J = 0x150L

.field public static final CKR_CANCEL:J = 0x1L

.field public static final CKR_CANT_LOCK:J = 0xaL

.field public static final CKR_CRYPTOKI_ALREADY_INITIALIZED:J = 0x191L

.field public static final CKR_CRYPTOKI_NOT_INITIALIZED:J = 0x190L

.field public static final CKR_DATA_INVALID:J = 0x20L

.field public static final CKR_DATA_LEN_RANGE:J = 0x21L

.field public static final CKR_DEVICE_ERROR:J = 0x30L

.field public static final CKR_DEVICE_MEMORY:J = 0x31L

.field public static final CKR_DEVICE_REMOVED:J = 0x32L

.field public static final CKR_DOMAIN_PARAMS_INVALID:J = 0x130L

.field public static final CKR_ENCRYPTED_DATA_INVALID:J = 0x40L

.field public static final CKR_ENCRYPTED_DATA_LEN_RANGE:J = 0x41L

.field public static final CKR_FUNCTION_CANCELED:J = 0x50L

.field public static final CKR_FUNCTION_FAILED:J = 0x6L

.field public static final CKR_FUNCTION_NOT_PARALLEL:J = 0x51L

.field public static final CKR_FUNCTION_NOT_SUPPORTED:J = 0x54L

.field public static final CKR_FUNCTION_REJECTED:J = 0x200L

.field public static final CKR_GENERAL_ERROR:J = 0x5L

.field public static final CKR_HOST_MEMORY:J = 0x2L

.field public static final CKR_INFORMATION_SENSITIVE:J = 0x170L

.field public static final CKR_KEY_CHANGED:J = 0x65L

.field public static final CKR_KEY_FUNCTION_NOT_PERMITTED:J = 0x68L

.field public static final CKR_KEY_HANDLE_INVALID:J = 0x60L

.field public static final CKR_KEY_INDIGESTIBLE:J = 0x67L

.field public static final CKR_KEY_NEEDED:J = 0x66L

.field public static final CKR_KEY_NOT_NEEDED:J = 0x64L

.field public static final CKR_KEY_NOT_WRAPPABLE:J = 0x69L

.field public static final CKR_KEY_SIZE_RANGE:J = 0x62L

.field public static final CKR_KEY_TYPE_INCONSISTENT:J = 0x63L

.field public static final CKR_KEY_UNEXTRACTABLE:J = 0x6aL

.field public static final CKR_MECHANISM_INVALID:J = 0x70L

.field public static final CKR_MECHANISM_PARAM_INVALID:J = 0x71L

.field public static final CKR_MUTEX_BAD:J = 0x1a0L

.field public static final CKR_MUTEX_NOT_LOCKED:J = 0x1a1L

.field public static final CKR_NEED_TO_CREATE_THREADS:J = 0x9L

.field public static final CKR_NO_EVENT:J = 0x8L

.field public static final CKR_OBJECT_HANDLE_INVALID:J = 0x82L

.field public static final CKR_OK:J = 0x0L

.field public static final CKR_OPERATION_ACTIVE:J = 0x90L

.field public static final CKR_OPERATION_NOT_INITIALIZED:J = 0x91L

.field public static final CKR_PIN_EXPIRED:J = 0xa3L

.field public static final CKR_PIN_INCORRECT:J = 0xa0L

.field public static final CKR_PIN_INVALID:J = 0xa1L

.field public static final CKR_PIN_LEN_RANGE:J = 0xa2L

.field public static final CKR_PIN_LOCKED:J = 0xa4L

.field public static final CKR_RANDOM_NO_RNG:J = 0x121L

.field public static final CKR_RANDOM_SEED_NOT_SUPPORTED:J = 0x120L

.field public static final CKR_SAVED_STATE_INVALID:J = 0x160L

.field public static final CKR_SESSION_CLOSED:J = 0xb0L

.field public static final CKR_SESSION_COUNT:J = 0xb1L

.field public static final CKR_SESSION_EXISTS:J = 0xb6L

.field public static final CKR_SESSION_HANDLE_INVALID:J = 0xb3L

.field public static final CKR_SESSION_PARALLEL_NOT_SUPPORTED:J = 0xb4L

.field public static final CKR_SESSION_READ_ONLY:J = 0xb5L

.field public static final CKR_SESSION_READ_ONLY_EXISTS:J = 0xb7L

.field public static final CKR_SESSION_READ_WRITE_SO_EXISTS:J = 0xb8L

.field public static final CKR_SIGNATURE_INVALID:J = 0xc0L

.field public static final CKR_SIGNATURE_LEN_RANGE:J = 0xc1L

.field public static final CKR_SLOT_ID_INVALID:J = 0x3L

.field public static final CKR_STATE_UNSAVEABLE:J = 0x180L

.field public static final CKR_TEMPLATE_INCOMPLETE:J = 0xd0L

.field public static final CKR_TEMPLATE_INCONSISTENT:J = 0xd1L

.field public static final CKR_TOKEN_NOT_PRESENT:J = 0xe0L

.field public static final CKR_TOKEN_NOT_RECOGNIZED:J = 0xe1L

.field public static final CKR_TOKEN_WRITE_PROTECTED:J = 0xe2L

.field public static final CKR_UNWRAPPING_KEY_HANDLE_INVALID:J = 0xf0L

.field public static final CKR_UNWRAPPING_KEY_SIZE_RANGE:J = 0xf1L

.field public static final CKR_UNWRAPPING_KEY_TYPE_INCONSISTENT:J = 0xf2L

.field public static final CKR_USER_ALREADY_LOGGED_IN:J = 0x100L

.field public static final CKR_USER_ANOTHER_ALREADY_LOGGED_IN:J = 0x104L

.field public static final CKR_USER_NOT_LOGGED_IN:J = 0x101L

.field public static final CKR_USER_PIN_NOT_INITIALIZED:J = 0x102L

.field public static final CKR_USER_TOO_MANY_TYPES:J = 0x105L

.field public static final CKR_USER_TYPE_INVALID:J = 0x103L

.field public static final CKR_VENDOR_DEFINED:J = 0x80000000L

.field public static final CKR_WRAPPED_KEY_INVALID:J = 0x110L

.field public static final CKR_WRAPPED_KEY_LEN_RANGE:J = 0x112L

.field public static final CKR_WRAPPING_KEY_HANDLE_INVALID:J = 0x113L

.field public static final CKR_WRAPPING_KEY_SIZE_RANGE:J = 0x114L

.field public static final CKR_WRAPPING_KEY_TYPE_INCONSISTENT:J = 0x115L

.field public static final CKS_RO_PUBLIC_SESSION:J = 0x0L

.field public static final CKS_RO_USER_FUNCTIONS:J = 0x1L

.field public static final CKS_RW_PUBLIC_SESSION:J = 0x2L

.field public static final CKS_RW_SO_FUNCTIONS:J = 0x4L

.field public static final CKS_RW_USER_FUNCTIONS:J = 0x3L

.field public static final CKU_CONTEXT_SPECIFIC:J = 0x2L

.field public static final CKU_SO:J = 0x0L

.field public static final CKU_USER:J = 0x1L

.field public static final CKZ_DATA_SPECIFIED:J = 0x1L

.field public static final CKZ_SALT_SPECIFIED:J = 0x1L

.field public static final CK_EFFECTIVELY_INFINITE:J = 0x0L

.field public static final CK_INVALID_HANDLE:J = 0x0L

.field public static final CK_UNAVAILABLE_INFORMATION:J = 0xffffffffL

.field public static final FALSE:Z = false

.field public static final NAME_CKM_AES_CBC:Ljava/lang/String; = "CKM_AES_CBC"

.field public static final NAME_CKM_AES_CBC_ENCRYPT_DATA:Ljava/lang/String; = "CKM_AES_CBC_ENCRYPT_DATA"

.field public static final NAME_CKM_AES_CBC_PAD:Ljava/lang/String; = "CKM_AES_CBC_PAD"

.field public static final NAME_CKM_AES_ECB:Ljava/lang/String; = "CKM_AES_ECB"

.field public static final NAME_CKM_AES_ECB_ENCRYPT_DATA:Ljava/lang/String; = "CKM_AES_ECB_ENCRYPT_DATA"

.field public static final NAME_CKM_AES_KEY_GEN:Ljava/lang/String; = "CKM_AES_KEY_GEN"

.field public static final NAME_CKM_AES_MAC:Ljava/lang/String; = "CKM_AES_MAC"

.field public static final NAME_CKM_AES_MAC_GENERAL:Ljava/lang/String; = "CKM_AES_MAC_GENERAL"

.field public static final NAME_CKM_BATON_CBC128:Ljava/lang/String; = "CKM_BATON_CBC128"

.field public static final NAME_CKM_BATON_COUNTER:Ljava/lang/String; = "CKM_BATON_COUNTER"

.field public static final NAME_CKM_BATON_ECB128:Ljava/lang/String; = "CKM_BATON_ECB128"

.field public static final NAME_CKM_BATON_ECB96:Ljava/lang/String; = "CKM_BATON_ECB96"

.field public static final NAME_CKM_BATON_KEY_GEN:Ljava/lang/String; = "CKM_BATON_KEY_GEN"

.field public static final NAME_CKM_BATON_SHUFFLE:Ljava/lang/String; = "CKM_BATON_SHUFFLE"

.field public static final NAME_CKM_BATON_WRAP:Ljava/lang/String; = "CKM_BATON_WRAP"

.field public static final NAME_CKM_BLOWFISH_CBC:Ljava/lang/String; = "CKM_BLOWFISH_CBC"

.field public static final NAME_CKM_BLOWFISH_KEY_GEN:Ljava/lang/String; = "CKM_BLOWFISH_KEY_GEN"

.field public static final NAME_CKM_CAST128_CBC:Ljava/lang/String; = "CKM_CAST128_CBC"

.field public static final NAME_CKM_CAST128_CBC_PAD:Ljava/lang/String; = "CKM_CAST128_CBC_PAD"

.field public static final NAME_CKM_CAST128_ECB:Ljava/lang/String; = "CKM_CAST128_ECB"

.field public static final NAME_CKM_CAST128_KEY_GEN:Ljava/lang/String; = "CKM_CAST128_KEY_GEN"

.field public static final NAME_CKM_CAST128_MAC:Ljava/lang/String; = "CKM_CAST128_MAC"

.field public static final NAME_CKM_CAST128_MAC_GENERAL:Ljava/lang/String; = "CKM_CAST128_MAC_GENERAL"

.field public static final NAME_CKM_CAST3_CBC:Ljava/lang/String; = "CKM_CAST3_CBC"

.field public static final NAME_CKM_CAST3_CBC_PAD:Ljava/lang/String; = "CKM_CAST3_CBC_PAD"

.field public static final NAME_CKM_CAST3_ECB:Ljava/lang/String; = "CKM_CAST3_ECB"

.field public static final NAME_CKM_CAST3_KEY_GEN:Ljava/lang/String; = "CKM_CAST3_KEY_GEN"

.field public static final NAME_CKM_CAST3_MAC:Ljava/lang/String; = "CKM_CAST3_MAC"

.field public static final NAME_CKM_CAST3_MAC_GENERAL:Ljava/lang/String; = "CKM_CAST3_MAC_GENERAL"

.field public static final NAME_CKM_CAST5_CBC:Ljava/lang/String; = "CKM_CAST5_CBC"

.field public static final NAME_CKM_CAST5_CBC_PAD:Ljava/lang/String; = "CKM_CAST5_CBC_PAD"

.field public static final NAME_CKM_CAST5_ECB:Ljava/lang/String; = "CKM_CAST5_ECB"

.field public static final NAME_CKM_CAST5_KEY_GEN:Ljava/lang/String; = "CKM_CAST5_KEY_GEN"

.field public static final NAME_CKM_CAST5_MAC:Ljava/lang/String; = "CKM_CAST5_MAC"

.field public static final NAME_CKM_CAST5_MAC_GENERAL:Ljava/lang/String; = "CKM_CAST5_MAC_GENERAL"

.field public static final NAME_CKM_CAST_CBC:Ljava/lang/String; = "CKM_CAST_CBC"

.field public static final NAME_CKM_CAST_CBC_PAD:Ljava/lang/String; = "CKM_CAST_CBC_PAD"

.field public static final NAME_CKM_CAST_ECB:Ljava/lang/String; = "CKM_CAST_ECB"

.field public static final NAME_CKM_CAST_KEY_GEN:Ljava/lang/String; = "CKM_CAST_KEY_GEN"

.field public static final NAME_CKM_CAST_MAC:Ljava/lang/String; = "CKM_CAST_MAC"

.field public static final NAME_CKM_CAST_MAC_GENERAL:Ljava/lang/String; = "CKM_CAST_MAC_GENERAL"

.field public static final NAME_CKM_CDMF_CBC:Ljava/lang/String; = "CKM_CDMF_CBC"

.field public static final NAME_CKM_CDMF_CBC_PAD:Ljava/lang/String; = "CKM_CDMF_CBC_PAD"

.field public static final NAME_CKM_CDMF_ECB:Ljava/lang/String; = "CKM_CDMF_ECB"

.field public static final NAME_CKM_CDMF_KEY_GEN:Ljava/lang/String; = "CKM_CDMF_KEY_GEN"

.field public static final NAME_CKM_CDMF_MAC:Ljava/lang/String; = "CKM_CDMF_MAC"

.field public static final NAME_CKM_CDMF_MAC_GENERAL:Ljava/lang/String; = "CKM_CDMF_MAC_GENERAL"

.field public static final NAME_CKM_CMS_SIG:Ljava/lang/String; = "CKM_CMS_SIG"

.field public static final NAME_CKM_CONCATENATE_BASE_AND_DATA:Ljava/lang/String; = "CKM_CONCATENATE_BASE_AND_DATA"

.field public static final NAME_CKM_CONCATENATE_BASE_AND_KEY:Ljava/lang/String; = "CKM_CONCATENATE_BASE_AND_KEY"

.field public static final NAME_CKM_CONCATENATE_DATA_AND_BASE:Ljava/lang/String; = "CKM_CONCATENATE_DATA_AND_BASE"

.field public static final NAME_CKM_DES2_KEY_GEN:Ljava/lang/String; = "CKM_DES2_KEY_GEN"

.field public static final NAME_CKM_DES3_CBC:Ljava/lang/String; = "CKM_DES3_CBC"

.field public static final NAME_CKM_DES3_CBC_ENCRYPT_DATA:Ljava/lang/String; = "CKM_DES3_CBC_ENCRYPT_DATA"

.field public static final NAME_CKM_DES3_CBC_PAD:Ljava/lang/String; = "CKM_DES3_CBC_PAD"

.field public static final NAME_CKM_DES3_ECB:Ljava/lang/String; = "CKM_DES3_ECB"

.field public static final NAME_CKM_DES3_ECB_ENCRYPT_DATA:Ljava/lang/String; = "CKM_DES3_ECB_ENCRYPT_DATA"

.field public static final NAME_CKM_DES3_KEY_GEN:Ljava/lang/String; = "CKM_DES3_KEY_GEN"

.field public static final NAME_CKM_DES3_MAC:Ljava/lang/String; = "CKM_DES3_MAC"

.field public static final NAME_CKM_DES3_MAC_GENERAL:Ljava/lang/String; = "CKM_DES3_MAC_GENERAL"

.field public static final NAME_CKM_DES_CBC:Ljava/lang/String; = "CKM_DES_CBC"

.field public static final NAME_CKM_DES_CBC_ENCRYPT_DATA:Ljava/lang/String; = "CKM_DES_CBC_ENCRYPT_DATA"

.field public static final NAME_CKM_DES_CBC_PAD:Ljava/lang/String; = "CKM_DES_CBC_PAD"

.field public static final NAME_CKM_DES_CFB64:Ljava/lang/String; = "CKM_DES_CFB64"

.field public static final NAME_CKM_DES_CFB8:Ljava/lang/String; = "CKM_DES_CFB8"

.field public static final NAME_CKM_DES_ECB:Ljava/lang/String; = "CKM_DES_ECB"

.field public static final NAME_CKM_DES_ECB_ENCRYPT_DATA:Ljava/lang/String; = "CKM_DES_ECB_ENCRYPT_DATA"

.field public static final NAME_CKM_DES_KEY_GEN:Ljava/lang/String; = "CKM_DES_KEY_GEN"

.field public static final NAME_CKM_DES_MAC:Ljava/lang/String; = "CKM_DES_MAC"

.field public static final NAME_CKM_DES_MAC_GENERAL:Ljava/lang/String; = "CKM_DES_MAC_GENERAL"

.field public static final NAME_CKM_DES_OFB64:Ljava/lang/String; = "CKM_DES_OFB64"

.field public static final NAME_CKM_DES_OFB8:Ljava/lang/String; = "CKM_DES_OFB8"

.field public static final NAME_CKM_DH_PKCS_DERIVE:Ljava/lang/String; = "CKM_DH_PKCS_DERIVE"

.field public static final NAME_CKM_DH_PKCS_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_DH_PKCS_KEY_PAIR_GEN"

.field public static final NAME_CKM_DH_PKCS_PARAMETER_GEN:Ljava/lang/String; = "CKM_DH_PKCS_PARAMETER_GEN"

.field public static final NAME_CKM_DSA:Ljava/lang/String; = "CKM_DSA"

.field public static final NAME_CKM_DSA_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_DSA_KEY_PAIR_GEN"

.field public static final NAME_CKM_DSA_PARAMETER_GEN:Ljava/lang/String; = "CKM_DSA_PARAMETER_GEN"

.field public static final NAME_CKM_DSA_SHA1:Ljava/lang/String; = "CKM_DSA_SHA1"

.field public static final NAME_CKM_ECDH1_COFACTOR_DERIVE:Ljava/lang/String; = "CKM_ECDH1_COFACTOR_DERIVE"

.field public static final NAME_CKM_ECDH1_DERIVE:Ljava/lang/String; = "CKM_ECDH1_DERIVE"

.field public static final NAME_CKM_ECDSA:Ljava/lang/String; = "CKM_ECDSA"

.field public static final NAME_CKM_ECDSA_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_ECDSA_KEY_PAIR_GEN"

.field public static final NAME_CKM_ECDSA_SHA1:Ljava/lang/String; = "CKM_ECDSA_SHA1"

.field public static final NAME_CKM_ECMQV_DERIVE:Ljava/lang/String; = "CKM_ECMQV_DERIVE"

.field public static final NAME_CKM_EC_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_EC_KEY_PAIR_GEN"

.field public static final NAME_CKM_EXTRACT_KEY_FROM_KEY:Ljava/lang/String; = "CKM_EXTRACT_KEY_FROM_KEY"

.field public static final NAME_CKM_FASTHASH:Ljava/lang/String; = "CKM_FASTHASH"

.field public static final NAME_CKM_FORTEZZA_TIMESTAMP:Ljava/lang/String; = "CKM_FORTEZZA_TIMESTAMP"

.field public static final NAME_CKM_GENERIC_SECRET_KEY_GEN:Ljava/lang/String; = "CKM_GENERIC_SECRET_KEY_GEN"

.field public static final NAME_CKM_IDEA_CBC:Ljava/lang/String; = "CKM_IDEA_CBC"

.field public static final NAME_CKM_IDEA_CBC_PAD:Ljava/lang/String; = "CKM_IDEA_CBC_PAD"

.field public static final NAME_CKM_IDEA_ECB:Ljava/lang/String; = "CKM_IDEA_ECB"

.field public static final NAME_CKM_IDEA_KEY_GEN:Ljava/lang/String; = "CKM_IDEA_KEY_GEN"

.field public static final NAME_CKM_IDEA_MAC:Ljava/lang/String; = "CKM_IDEA_MAC"

.field public static final NAME_CKM_IDEA_MAC_GENERAL:Ljava/lang/String; = "CKM_IDEA_MAC_GENERAL"

.field public static final NAME_CKM_JUNIPER_CBC128:Ljava/lang/String; = "CKM_JUNIPER_CBC128"

.field public static final NAME_CKM_JUNIPER_COUNTER:Ljava/lang/String; = "CKM_JUNIPER_COUNTER"

.field public static final NAME_CKM_JUNIPER_ECB128:Ljava/lang/String; = "CKM_JUNIPER_ECB128"

.field public static final NAME_CKM_JUNIPER_KEY_GEN:Ljava/lang/String; = "CKM_JUNIPER_KEY_GEN"

.field public static final NAME_CKM_JUNIPER_SHUFFLE:Ljava/lang/String; = "CKM_JUNIPER_SHUFFLE"

.field public static final NAME_CKM_JUNIPER_WRAP:Ljava/lang/String; = "CKM_JUNIPER_WRAP"

.field public static final NAME_CKM_KEA_KEY_DERIVE:Ljava/lang/String; = "CKM_KEA_KEY_DERIVE"

.field public static final NAME_CKM_KEA_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_KEA_KEY_PAIR_GEN"

.field public static final NAME_CKM_KEY_WRAP_LYNKS:Ljava/lang/String; = "CKM_KEY_WRAP_LYNKS"

.field public static final NAME_CKM_KEY_WRAP_SET_OAEP:Ljava/lang/String; = "CKM_KEY_WRAP_SET_OAEP"

.field public static final NAME_CKM_MD2:Ljava/lang/String; = "CKM_MD2"

.field public static final NAME_CKM_MD2_HMAC:Ljava/lang/String; = "CKM_MD2_HMAC"

.field public static final NAME_CKM_MD2_HMAC_GENERAL:Ljava/lang/String; = "CKM_MD2_HMAC_GENERAL"

.field public static final NAME_CKM_MD2_KEY_DERIVATION:Ljava/lang/String; = "CKM_MD2_KEY_DERIVATION"

.field public static final NAME_CKM_MD2_RSA_PKCS:Ljava/lang/String; = "CKM_MD2_RSA_PKCS"

.field public static final NAME_CKM_MD5:Ljava/lang/String; = "CKM_MD5"

.field public static final NAME_CKM_MD5_HMAC:Ljava/lang/String; = "CKM_MD5_HMAC"

.field public static final NAME_CKM_MD5_HMAC_GENERAL:Ljava/lang/String; = "CKM_MD5_HMAC_GENERAL"

.field public static final NAME_CKM_MD5_KEY_DERIVATION:Ljava/lang/String; = "CKM_MD5_KEY_DERIVATION"

.field public static final NAME_CKM_MD5_RSA_PKCS:Ljava/lang/String; = "CKM_MD5_RSA_PKCS"

.field public static final NAME_CKM_PBA_SHA1_WITH_SHA1_HMAC:Ljava/lang/String; = "CKM_PBA_SHA1_WITH_SHA1_HMAC"

.field public static final NAME_CKM_PBE_MD2_DES_CBC:Ljava/lang/String; = "CKM_PBE_MD2_DES_CBC"

.field public static final NAME_CKM_PBE_MD5_CAST128_CBC:Ljava/lang/String; = "CKM_PBE_MD5_CAST128_CBC"

.field public static final NAME_CKM_PBE_MD5_CAST3_CBC:Ljava/lang/String; = "CKM_PBE_MD5_CAST3_CBC"

.field public static final NAME_CKM_PBE_MD5_CAST5_CBC:Ljava/lang/String; = "CKM_PBE_MD5_CAST5_CBC"

.field public static final NAME_CKM_PBE_MD5_CAST_CBC:Ljava/lang/String; = "CKM_PBE_MD5_CAST_CBC"

.field public static final NAME_CKM_PBE_MD5_DES_CBC:Ljava/lang/String; = "CKM_PBE_MD5_DES_CBC"

.field public static final NAME_CKM_PBE_SHA1_CAST128_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_CAST128_CBC"

.field public static final NAME_CKM_PBE_SHA1_CAST5_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_CAST5_CBC"

.field public static final NAME_CKM_PBE_SHA1_DES2_EDE_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_DES2_EDE_CBC"

.field public static final NAME_CKM_PBE_SHA1_DES3_EDE_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_DES3_EDE_CBC"

.field public static final NAME_CKM_PBE_SHA1_RC2_128_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_RC2_128_CBC"

.field public static final NAME_CKM_PBE_SHA1_RC2_40_CBC:Ljava/lang/String; = "CKM_PBE_SHA1_RC2_40_CBC"

.field public static final NAME_CKM_PBE_SHA1_RC4_128:Ljava/lang/String; = "CKM_PBE_SHA1_RC4_128"

.field public static final NAME_CKM_PBE_SHA1_RC4_40:Ljava/lang/String; = "CKM_PBE_SHA1_RC4_40"

.field public static final NAME_CKM_PKCS5_PBKD2:Ljava/lang/String; = "CKM_PKCS5_PBKD2"

.field public static final NAME_CKM_RC2_CBC:Ljava/lang/String; = "CKM_RC2_CBC"

.field public static final NAME_CKM_RC2_CBC_PAD:Ljava/lang/String; = "CKM_RC2_CBC_PAD"

.field public static final NAME_CKM_RC2_ECB:Ljava/lang/String; = "CKM_RC2_ECB"

.field public static final NAME_CKM_RC2_KEY_GEN:Ljava/lang/String; = "CKM_RC2_KEY_GEN"

.field public static final NAME_CKM_RC2_MAC:Ljava/lang/String; = "CKM_RC2_MAC"

.field public static final NAME_CKM_RC2_MAC_GENERAL:Ljava/lang/String; = "CKM_RC2_MAC_GENERAL"

.field public static final NAME_CKM_RC4:Ljava/lang/String; = "CKM_RC4"

.field public static final NAME_CKM_RC4_KEY_GEN:Ljava/lang/String; = "CKM_RC4_KEY_GEN"

.field public static final NAME_CKM_RC5_CBC:Ljava/lang/String; = "CKM_RC5_CBC"

.field public static final NAME_CKM_RC5_CBC_PAD:Ljava/lang/String; = "CKM_RC5_CBC_PAD"

.field public static final NAME_CKM_RC5_ECB:Ljava/lang/String; = "CKM_RC5_ECB"

.field public static final NAME_CKM_RC5_KEY_GEN:Ljava/lang/String; = "CKM_RC5_KEY_GEN"

.field public static final NAME_CKM_RC5_MAC:Ljava/lang/String; = "CKM_RC5_MAC"

.field public static final NAME_CKM_RC5_MAC_GENERAL:Ljava/lang/String; = "CKM_RC5_MAC_GENERAL"

.field public static final NAME_CKM_RIPEMD128:Ljava/lang/String; = "CKM_RIPEMD128"

.field public static final NAME_CKM_RIPEMD128_HMAC:Ljava/lang/String; = "CKM_RIPEMD128_HMAC"

.field public static final NAME_CKM_RIPEMD128_HMAC_GENERAL:Ljava/lang/String; = "CKM_RIPEMD128_HMAC_GENERAL"

.field public static final NAME_CKM_RIPEMD128_RSA_PKCS:Ljava/lang/String; = "CKM_RIPEMD128_RSA_PKCS"

.field public static final NAME_CKM_RIPEMD160:Ljava/lang/String; = "CKM_RIPEMD160"

.field public static final NAME_CKM_RIPEMD160_HMAC:Ljava/lang/String; = "CKM_RIPEMD160_HMAC"

.field public static final NAME_CKM_RIPEMD160_HMAC_GENERAL:Ljava/lang/String; = "CKM_RIPEMD160_HMAC_GENERAL"

.field public static final NAME_CKM_RIPEMD160_RSA_PKCS:Ljava/lang/String; = "CKM_RIPEMD160_RSA_PKCS"

.field public static final NAME_CKM_RSA_9796:Ljava/lang/String; = "CKM_RSA_9796"

.field public static final NAME_CKM_RSA_PKCS:Ljava/lang/String; = "CKM_RSA_PKCS"

.field public static final NAME_CKM_RSA_PKCS_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_RSA_PKCS_KEY_PAIR_GEN"

.field public static final NAME_CKM_RSA_PKCS_OAEP:Ljava/lang/String; = "CKM_RSA_PKCS_OAEP"

.field public static final NAME_CKM_RSA_PKCS_PSS:Ljava/lang/String; = "CKM_RSA_PKCS_PSS"

.field public static final NAME_CKM_RSA_X9_31:Ljava/lang/String; = "CKM_RSA_X9_31"

.field public static final NAME_CKM_RSA_X9_31_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_RSA_X9_31_KEY_PAIR_GEN"

.field public static final NAME_CKM_RSA_X_509:Ljava/lang/String; = "CKM_RSA_X_509"

.field public static final NAME_CKM_SHA1_KEY_DERIVATION:Ljava/lang/String; = "CKM_SHA1_KEY_DERIVATION"

.field public static final NAME_CKM_SHA1_RSA_PKCS:Ljava/lang/String; = "CKM_SHA1_RSA_PKCS"

.field public static final NAME_CKM_SHA1_RSA_PKCS_PSS:Ljava/lang/String; = "CKM_SHA1_RSA_PKCS_PSS"

.field public static final NAME_CKM_SHA1_RSA_X9_31:Ljava/lang/String; = "CKM_SHA1_RSA_X9_31"

.field public static final NAME_CKM_SHA256:Ljava/lang/String; = "CKM_SHA256"

.field public static final NAME_CKM_SHA256_HMAC:Ljava/lang/String; = "CKM_SHA256_HMAC"

.field public static final NAME_CKM_SHA256_HMAC_GENERAL:Ljava/lang/String; = "CKM_SHA256_HMAC_GENERAL"

.field public static final NAME_CKM_SHA256_KEY_DERIVATION:Ljava/lang/String; = "CKM_SHA256_KEY_DERIVATION"

.field public static final NAME_CKM_SHA256_RSA_PKCS:Ljava/lang/String; = "CKM_SHA256_RSA_PKCS"

.field public static final NAME_CKM_SHA256_RSA_PKCS_PSS:Ljava/lang/String; = "CKM_SHA256_RSA_PKCS_PSS"

.field public static final NAME_CKM_SHA384:Ljava/lang/String; = "CKM_SHA384"

.field public static final NAME_CKM_SHA384_HMAC:Ljava/lang/String; = "CKM_SHA384_HMAC"

.field public static final NAME_CKM_SHA384_HMAC_GENERAL:Ljava/lang/String; = "CKM_SHA384_HMAC_GENERAL"

.field public static final NAME_CKM_SHA384_KEY_DERIVATION:Ljava/lang/String; = "CKM_SHA384_KEY_DERIVATION"

.field public static final NAME_CKM_SHA384_RSA_PKCS:Ljava/lang/String; = "CKM_SHA384_RSA_PKCS"

.field public static final NAME_CKM_SHA384_RSA_PKCS_PSS:Ljava/lang/String; = "CKM_SHA384_RSA_PKCS_PSS"

.field public static final NAME_CKM_SHA512:Ljava/lang/String; = "CKM_SHA512"

.field public static final NAME_CKM_SHA512_HMAC:Ljava/lang/String; = "CKM_SHA512_HMAC"

.field public static final NAME_CKM_SHA512_HMAC_GENERAL:Ljava/lang/String; = "CKM_SHA512_HMAC_GENERAL"

.field public static final NAME_CKM_SHA512_KEY_DERIVATION:Ljava/lang/String; = "CKM_SHA512_KEY_DERIVATION"

.field public static final NAME_CKM_SHA512_RSA_PKCS:Ljava/lang/String; = "CKM_SHA512_RSA_PKCS"

.field public static final NAME_CKM_SHA512_RSA_PKCS_PSS:Ljava/lang/String; = "CKM_SHA512_RSA_PKCS_PSS"

.field public static final NAME_CKM_SHA_1:Ljava/lang/String; = "CKM_SHA_1"

.field public static final NAME_CKM_SHA_1_HMAC:Ljava/lang/String; = "CKM_SHA_1_HMAC"

.field public static final NAME_CKM_SHA_1_HMAC_GENERAL:Ljava/lang/String; = "CKM_SHA_1_HMAC_GENERAL"

.field public static final NAME_CKM_SKIPJACK_CBC64:Ljava/lang/String; = "CKM_SKIPJACK_CBC64"

.field public static final NAME_CKM_SKIPJACK_CFB16:Ljava/lang/String; = "CKM_SKIPJACK_CFB16"

.field public static final NAME_CKM_SKIPJACK_CFB32:Ljava/lang/String; = "CKM_SKIPJACK_CFB32"

.field public static final NAME_CKM_SKIPJACK_CFB64:Ljava/lang/String; = "CKM_SKIPJACK_CFB64"

.field public static final NAME_CKM_SKIPJACK_CFB8:Ljava/lang/String; = "CKM_SKIPJACK_CFB8"

.field public static final NAME_CKM_SKIPJACK_ECB64:Ljava/lang/String; = "CKM_SKIPJACK_ECB64"

.field public static final NAME_CKM_SKIPJACK_KEY_GEN:Ljava/lang/String; = "CKM_SKIPJACK_KEY_GEN"

.field public static final NAME_CKM_SKIPJACK_OFB64:Ljava/lang/String; = "CKM_SKIPJACK_OFB64"

.field public static final NAME_CKM_SKIPJACK_PRIVATE_WRAP:Ljava/lang/String; = "CKM_SKIPJACK_PRIVATE_WRAP"

.field public static final NAME_CKM_SKIPJACK_RELAYX:Ljava/lang/String; = "CKM_SKIPJACK_RELAYX"

.field public static final NAME_CKM_SKIPJACK_WRAP:Ljava/lang/String; = "CKM_SKIPJACK_WRAP"

.field public static final NAME_CKM_SSL3_KEY_AND_MAC_DERIVE:Ljava/lang/String; = "CKM_SSL3_KEY_AND_MAC_DERIVE"

.field public static final NAME_CKM_SSL3_MASTER_KEY_DERIVE:Ljava/lang/String; = "CKM_SSL3_MASTER_KEY_DERIVE"

.field public static final NAME_CKM_SSL3_MASTER_KEY_DERIVE_DH:Ljava/lang/String; = "CKM_SSL3_MASTER_KEY_DERIVE_DH"

.field public static final NAME_CKM_SSL3_MD5_MAC:Ljava/lang/String; = "CKM_SSL3_MD5_MAC"

.field public static final NAME_CKM_SSL3_PRE_MASTER_KEY_GEN:Ljava/lang/String; = "CKM_SSL3_PRE_MASTER_KEY_GEN"

.field public static final NAME_CKM_SSL3_SHA1_MAC:Ljava/lang/String; = "CKM_SSL3_SHA1_MAC"

.field public static final NAME_CKM_TLS_KEY_AND_MAC_DERIVE:Ljava/lang/String; = "CKM_TLS_KEY_AND_MAC_DERIVE"

.field public static final NAME_CKM_TLS_MASTER_KEY_DERIVE:Ljava/lang/String; = "CKM_TLS_MASTER_KEY_DERIVE"

.field public static final NAME_CKM_TLS_MASTER_KEY_DERIVE_DH:Ljava/lang/String; = "CKM_TLS_MASTER_KEY_DERIVE_DH"

.field public static final NAME_CKM_TLS_PRE_MASTER_KEY_GEN:Ljava/lang/String; = "CKM_TLS_PRE_MASTER_KEY_GEN"

.field public static final NAME_CKM_TLS_PRF:Ljava/lang/String; = "CKM_TLS_PRF"

.field public static final NAME_CKM_TWOFISH_CBC:Ljava/lang/String; = "CKM_TWOFISH_CBC"

.field public static final NAME_CKM_TWOFISH_KEY_GEN:Ljava/lang/String; = "CKM_TWOFISH_KEY_GEN"

.field public static final NAME_CKM_VENDOR_DEFINED:Ljava/lang/String; = "CKM_VENDOR_DEFINED"

.field public static final NAME_CKM_WTLS_CLIENT_KEY_AND_MAC_DERIVE:Ljava/lang/String; = "CKM_WTLS_CLIENT_KEY_AND_MAC_DERIVE"

.field public static final NAME_CKM_WTLS_MASTER_KEY_DERIVE:Ljava/lang/String; = "CKM_WTLS_MASTER_KEY_DERIVE"

.field public static final NAME_CKM_WTLS_MASTER_KEY_DERIVE_DH_ECC:Ljava/lang/String; = "CKM_WTLS_MASTER_KEY_DERIVE_DH_ECC"

.field public static final NAME_CKM_WTLS_PRE_MASTER_KEY_GEN:Ljava/lang/String; = "CKM_WTLS_PRE_MASTER_KEY_GEN"

.field public static final NAME_CKM_WTLS_PRF:Ljava/lang/String; = "CKM_WTLS_PRF"

.field public static final NAME_CKM_WTLS_SERVER_KEY_AND_MAC_DERIVE:Ljava/lang/String; = "CKM_WTLS_SERVER_KEY_AND_MAC_DERIVE"

.field public static final NAME_CKM_X9_42_DH_DERIVE:Ljava/lang/String; = "CKM_X9_42_DH_DERIVE"

.field public static final NAME_CKM_X9_42_DH_HYBRID_DERIVE:Ljava/lang/String; = "CKM_X9_42_DH_HYBRID_DERIVE"

.field public static final NAME_CKM_X9_42_DH_KEY_PAIR_GEN:Ljava/lang/String; = "CKM_X9_42_DH_KEY_PAIR_GEN"

.field public static final NAME_CKM_X9_42_DH_PARAMETER_GEN:Ljava/lang/String; = "CKM_X9_42_DH_PARAMETER_GEN"

.field public static final NAME_CKM_X9_42_MQV_DERIVE:Ljava/lang/String; = "CKM_X9_42_MQV_DERIVE"

.field public static final NAME_CKM_XOR_BASE_AND_DATA:Ljava/lang/String; = "CKM_XOR_BASE_AND_DATA"

.field public static final NULL_PTR:Ljava/lang/Object;

.field public static final TRUE:Z = true


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/smartcard/pkcs11/PKCS11Constants;->NULL_PTR:Ljava/lang/Object;

    return-void
.end method
