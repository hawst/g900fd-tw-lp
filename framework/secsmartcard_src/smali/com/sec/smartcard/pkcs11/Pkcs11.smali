.class public Lcom/sec/smartcard/pkcs11/Pkcs11;
.super Ljava/lang/Object;
.source "Pkcs11.java"


# static fields
.field private static final PKCS11_LIB_NAME:Ljava/lang/String; = "SamsungPkcs11Wrapper"

.field private static final TAG:Ljava/lang/String; = "Pkcs11"


# instance fields
.field private mIsLoaded:Z

.field private mPkcs11Module:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/smartcard/pkcs11/Pkcs11;->mIsLoaded:Z

    .line 57
    const-string v0, "SamsungPkcs11Wrapper"

    iput-object v0, p0, Lcom/sec/smartcard/pkcs11/Pkcs11;->mPkcs11Module:Ljava/lang/String;

    .line 60
    return-void
.end method

.method protected static synchronized native declared-synchronized finalizeLibrary()V
.end method

.method protected static synchronized native declared-synchronized initializeLibrary()V
.end method


# virtual methods
.method public native C_CancelFunction(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_CloseAllSessions(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_CloseSession(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_CopyObject(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_CreateObject(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Decrypt(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DecryptDigestUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DecryptFinal(J)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DecryptInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DecryptUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DecryptVerifyUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DeriveKey(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DestroyObject(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Digest(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DigestEncryptUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DigestFinal(J)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DigestInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DigestKey(JJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_DigestUpdate(J[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Encrypt(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_EncryptFinal(J)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_EncryptInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_EncryptUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Finalize(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_FindObjects(JJ)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_FindObjectsFinal(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_FindObjectsInit(J[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GenerateKey(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GenerateKeyPair(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GenerateRandom(JJ)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetAttributeValue(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetFunctionStatus(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetInfo()Lcom/sec/smartcard/pkcs11/CK_INFO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetMechanismInfo(JJ)Lcom/sec/smartcard/pkcs11/CK_MECHANISM_INFO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetMechanismList(J)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetObjectSize(JJ)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetOperationState(J)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetSessionInfo(J)Lcom/sec/smartcard/pkcs11/CK_SESSION_INFO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetSlotInfo(J)Lcom/sec/smartcard/pkcs11/CK_SLOT_INFO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetSlotList(Z)[J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_GetTokenInfo(J)Lcom/sec/smartcard/pkcs11/CK_TOKEN_INFO;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_InitPIN(J[C)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_InitToken(J[C[C)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Initialize(Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Login(JJ[C)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Logout(J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_OpenSession(JJLjava/lang/Object;Lcom/sec/smartcard/pkcs11/CK_NOTIFY;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SeedRandom(J[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SetAttributeValue(JJ[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SetOperationState(J[BJJ)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SetPIN(J[C[C)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Sign(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignEncryptUpdate(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignFinal(J)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignRecover(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignRecoverInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_SignUpdate(J[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_UnwrapKey(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J[B[Lcom/sec/smartcard/pkcs11/CK_ATTRIBUTE;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_Verify(J[B[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_VerifyFinal(J[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_VerifyInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_VerifyRecover(J[B)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_VerifyRecoverInit(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;J)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_VerifyUpdate(J[B)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_WaitForSlotEvent(JLjava/lang/Object;)J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public native C_WrapKey(JLcom/sec/smartcard/pkcs11/CK_MECHANISM;JJ)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation
.end method

.method public GetRemainingTokenLoginAttempt(J)J
    .locals 2
    .param p1, "slot"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/smartcard/pkcs11/PKCS11Exception;
        }
    .end annotation

    .prologue
    .line 280
    const-wide/16 v0, 0x0

    .line 281
    .local v0, "attempt":J
    return-wide v0
.end method

.method public Load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "pkcs11LibraryName"    # Ljava/lang/String;
    .param p2, "getFunctionListName"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "isServer"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsatisfiedLinkError;
        }
    .end annotation

    .prologue
    .line 80
    if-nez p1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 83
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/Pkcs11;->mPkcs11Module:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 85
    invoke-static {}, Lcom/sec/smartcard/pkcs11/Pkcs11;->initializeLibrary()V

    .line 87
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/smartcard/pkcs11/Pkcs11;->connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/UnsatisfiedLinkError;

    const-string v2, "PKCS11 Library cannot be loaded"

    invoke-direct {v1, v2}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public Unload()V
    .locals 0

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/sec/smartcard/pkcs11/Pkcs11;->disconnect()V

    .line 100
    invoke-static {}, Lcom/sec/smartcard/pkcs11/Pkcs11;->finalizeLibrary()V

    .line 101
    return-void
.end method

.method public native connect(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public native disconnect()V
.end method
