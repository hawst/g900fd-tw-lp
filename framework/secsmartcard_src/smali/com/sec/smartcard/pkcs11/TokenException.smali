.class public Lcom/sec/smartcard/pkcs11/TokenException;
.super Ljava/lang/Exception;
.source "TokenException.java"


# instance fields
.field protected encapsulatedException_:Ljava/lang/Exception;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 72
    return-void
.end method

.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 0
    .param p1, "encapsulatedException"    # Ljava/lang/Exception;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 93
    iput-object p1, p0, Lcom/sec/smartcard/pkcs11/TokenException;->encapsulatedException_:Ljava/lang/Exception;

    .line 94
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "encapsulatedException"    # Ljava/lang/Exception;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 107
    iput-object p2, p0, Lcom/sec/smartcard/pkcs11/TokenException;->encapsulatedException_:Ljava/lang/Exception;

    .line 108
    return-void
.end method


# virtual methods
.method public getEncapsulatedException()Ljava/lang/Exception;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/smartcard/pkcs11/TokenException;->encapsulatedException_:Ljava/lang/Exception;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-super {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 131
    .local v0, "buffer":Ljava/lang/StringBuffer;
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/TokenException;->encapsulatedException_:Ljava/lang/Exception;

    if-eqz v1, :cond_0

    .line 132
    const-string v1, ", Encasulated Exception: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 133
    iget-object v1, p0, Lcom/sec/smartcard/pkcs11/TokenException;->encapsulatedException_:Ljava/lang/Exception;

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 136
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
