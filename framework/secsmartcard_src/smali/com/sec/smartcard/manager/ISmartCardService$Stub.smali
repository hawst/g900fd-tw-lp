.class public abstract Lcom/sec/smartcard/manager/ISmartCardService$Stub;
.super Landroid/os/Binder;
.source "ISmartCardService.java"

# interfaces
.implements Lcom/sec/smartcard/manager/ISmartCardService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/manager/ISmartCardService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/manager/ISmartCardService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.smartcard.manager.ISmartCardService"

.field static final TRANSACTION_connectCryptoService:I = 0x3

.field static final TRANSACTION_disconnectCryptoService:I = 0x4

.field static final TRANSACTION_getCryptoServiceSocket:I = 0x5

.field static final TRANSACTION_getPin:I = 0x2

.field static final TRANSACTION_getSlotID:I = 0x6

.field static final TRANSACTION_getVersion:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p0, p0, v0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/manager/ISmartCardService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/smartcard/manager/ISmartCardService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/sec/smartcard/manager/ISmartCardService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/sec/smartcard/manager/ISmartCardService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 96
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 42
    :sswitch_0
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->getVersion()I

    move-result v2

    .line 49
    .local v2, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 50
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 55
    .end local v2    # "_result":I
    :sswitch_2
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    move-result-object v0

    .line 58
    .local v0, "_arg0":Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->getPin(Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;)V

    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 64
    .end local v0    # "_arg0":Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;
    :sswitch_3
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    move-result-object v0

    .line 67
    .local v0, "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    invoke-virtual {p0, v0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->connectCryptoService(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z

    move-result v2

    .line 68
    .local v2, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 69
    if-eqz v2, :cond_0

    move v1, v4

    :goto_1
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 74
    .end local v0    # "_arg0":Lcom/sec/smartcard/adapter/ICryptoServiceListener;
    .end local v2    # "_result":Z
    :sswitch_4
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->disconnectCryptoService()V

    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 81
    :sswitch_5
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->getCryptoServiceSocket()Ljava/lang/String;

    move-result-object v2

    .line 83
    .local v2, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    .end local v2    # "_result":Ljava/lang/String;
    :sswitch_6
    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->getSlotID()J

    move-result-wide v2

    .line 91
    .local v2, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    invoke-virtual {p3, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
