.class Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;
.super Ljava/lang/Object;
.source "SmartCardServiceClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/client/SmartCardServiceClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SmartCardInterface"
.end annotation


# instance fields
.field private final cls:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private final intentAction:Ljava/lang/String;

.field private interfaceBinder:Landroid/os/IBinder;

.field private final isSupported:Z

.field private svcConn:Landroid/content/ServiceConnection;

.field final synthetic this$0:Lcom/sec/smartcard/client/SmartCardServiceClient;


# direct methods
.method public constructor <init>(Lcom/sec/smartcard/client/SmartCardServiceClient;Ljava/lang/String;Ljava/lang/Class;Z)V
    .locals 1
    .param p2, "intentAction"    # Ljava/lang/String;
    .param p4, "isSupported"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 141
    .local p3, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->this$0:Lcom/sec/smartcard/client/SmartCardServiceClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-instance v0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;-><init>(Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;)V

    iput-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->svcConn:Landroid/content/ServiceConnection;

    .line 142
    iput-object p2, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->intentAction:Ljava/lang/String;

    .line 143
    iput-boolean p4, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->isSupported:Z

    .line 144
    iput-object p3, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->cls:Ljava/lang/Class;

    .line 145
    return-void
.end method

.method static synthetic access$002(Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->interfaceBinder:Landroid/os/IBinder;

    return-object p1
.end method
