.class Lcom/sec/smartcard/client/SmartCardHelper$1;
.super Ljava/lang/Object;
.source "SmartCardHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/client/SmartCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/client/SmartCardHelper;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/client/SmartCardHelper;)V
    .locals 0

    .prologue
    .line 319
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-static {p2}, Lcom/sec/smartcard/manager/ISmartCardService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/smartcard/manager/ISmartCardService;

    move-result-object v1

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$002(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/manager/ISmartCardService;)Lcom/sec/smartcard/manager/ISmartCardService;

    .line 325
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v0}, Lcom/sec/smartcard/client/SmartCardHelper;->access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 326
    const-string v0, "SmartcardHelper"

    const-string v1, "Connecting to cryptoservie..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # invokes: Lcom/sec/smartcard/client/SmartCardHelper;->connectCryptoService()V
    invoke-static {v0}, Lcom/sec/smartcard/client/SmartCardHelper;->access$100(Lcom/sec/smartcard/client/SmartCardHelper;)V

    .line 329
    :cond_0
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$002(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/manager/ISmartCardService;)Lcom/sec/smartcard/manager/ISmartCardService;

    .line 334
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$1;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$202(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z

    .line 335
    return-void
.end method
