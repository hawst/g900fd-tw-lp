.class public Lcom/sec/smartcard/client/SmartCardServiceClient;
.super Ljava/lang/Object;
.source "SmartCardServiceClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;,
        Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SmartCardServiceClient"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mName:Ljava/lang/String;

.field private mPinCallback:Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;

.field private mPinManager:Lcom/sec/smartcard/pinservice/SmartCardPinManager;

.field private mPinServiceConnection:Landroid/content/ServiceConnection;

.field private mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;

.field private mSmartCardInterfaces:[Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 181
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mName:Ljava/lang/String;

    .line 183
    return-void
.end method

.method public static declared-synchronized getAdapter(Landroid/content/Context;Ljava/lang/String;)Lcom/sec/smartcard/client/SmartCardServiceClient;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "adapterName"    # Ljava/lang/String;

    .prologue
    .line 443
    const-class v0, Lcom/sec/smartcard/client/SmartCardServiceClient;

    monitor-enter v0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1
.end method

.method public static declared-synchronized getAvailableAdapters(Landroid/content/Context;)Ljava/util/List;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 472
    const-class v2, Lcom/sec/smartcard/client/SmartCardServiceClient;

    monitor-enter v2

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    .local v0, "providerList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    monitor-exit v2

    return-object v0

    .line 472
    .end local v0    # "providerList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static declared-synchronized getDefaultAdapter(Landroid/content/Context;)Lcom/sec/smartcard/client/SmartCardServiceClient;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 407
    const-class v2, Lcom/sec/smartcard/client/SmartCardServiceClient;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sput-object v1, Lcom/sec/smartcard/client/SmartCardServiceClient;->mContext:Landroid/content/Context;

    .line 408
    invoke-static {p0}, Lcom/sec/smartcard/client/SmartCardServiceClient;->getAvailableAdapters(Landroid/content/Context;)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 409
    .local v0, "providerList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    monitor-exit v2

    return-object v1

    .line 407
    .end local v0    # "providerList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method


# virtual methods
.method public declared-synchronized bindPinService(Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;)V
    .locals 0
    .param p1, "pinCallback"    # Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;

    .prologue
    .line 304
    monitor-enter p0

    monitor-exit p0

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized getPkcs11()Lcom/sec/smartcard/pkcs11/Pkcs11;
    .locals 1

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mPkcs11:Lcom/sec/smartcard/pkcs11/Pkcs11;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public declared-synchronized unbindPinService(Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;)V
    .locals 2
    .param p1, "pinCallback"    # Lcom/sec/smartcard/client/SmartCardServiceClient$PinServiceCallback;

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mPinServiceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 344
    sget-object v0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardServiceClient;->mPinServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :cond_0
    monitor-exit p0

    return-void

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
