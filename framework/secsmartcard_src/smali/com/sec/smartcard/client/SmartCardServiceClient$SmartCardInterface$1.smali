.class Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;
.super Ljava/lang/Object;
.source "SmartCardServiceClient.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;->this$1:Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "component"    # Landroid/content/ComponentName;
    .param p2, "binder"    # Landroid/os/IBinder;

    .prologue
    .line 132
    const-string v0, "SmartCardServiceClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;->this$1:Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;

    # setter for: Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->interfaceBinder:Landroid/os/IBinder;
    invoke-static {v0, p2}, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->access$002(Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 134
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface$1;->this$1:Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;

    const/4 v1, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->interfaceBinder:Landroid/os/IBinder;
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;->access$002(Lcom/sec/smartcard/client/SmartCardServiceClient$SmartCardInterface;Landroid/os/IBinder;)Landroid/os/IBinder;

    .line 128
    return-void
.end method
