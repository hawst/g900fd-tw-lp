.class Lcom/sec/smartcard/client/SmartCardHelper$2;
.super Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;
.source "SmartCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/client/SmartCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/client/SmartCardHelper;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/client/SmartCardHelper;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-direct {p0}, Lcom/sec/smartcard/adapter/ICryptoServiceListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onSmartCardConnected(Ljava/lang/String;)V
    .locals 6
    .param p1, "identifier"    # Ljava/lang/String;

    .prologue
    .line 359
    const-string v2, "SmartcardHelper"

    const-string v3, "onSmartCardConnected"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 361
    monitor-enter p0

    .line 362
    :try_start_0
    const-string v2, "SmartcardHelper"

    const-string v3, "Executing synchronized block"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    if-nez v2, :cond_0

    .line 364
    const-string v2, "SmartcardHelper"

    const-string v3, "Pkcs11 is null. So creating and loading Pkcs11"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 365
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    new-instance v3, Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-direct {v3}, Lcom/sec/smartcard/pkcs11/Pkcs11;-><init>()V

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2, v3}, Lcom/sec/smartcard/client/SmartCardHelper;->access$302(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/pkcs11/Pkcs11;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 366
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 367
    const-string v2, "SmartcardHelper"

    const-string v3, "Load Pkcs11"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 368
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_0

    .line 370
    :try_start_1
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/smartcard/manager/ISmartCardService;->getCryptoServiceSocket()Ljava/lang/String;

    move-result-object v1

    .line 371
    .local v1, "socket":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    const-string v3, "libSamsungPkcs11Wrapper.so"

    const-string v4, "C_GetFunctionList"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 378
    .end local v1    # "socket":Ljava/lang/String;
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 379
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$400(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/smartcard/client/SmartCardHelper$Callback;->onInitComplete()V

    .line 380
    return-void

    .line 372
    :catch_0
    move-exception v0

    .line 373
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 378
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method public onSmartCardConnectionError(Ljava/lang/String;I)V
    .locals 5
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "reasonCode"    # I

    .prologue
    .line 387
    const-string v2, "SmartcardHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSmartCardConnectionError"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z
    invoke-static {v3}, Lcom/sec/smartcard/client/SmartCardHelper;->access$200(Lcom/sec/smartcard/client/SmartCardHelper;)Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 392
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/smartcard/manager/ISmartCardService;->disconnectCryptoService()V

    .line 395
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 397
    :try_start_1
    const-string v2, "SmartcardHelper"

    const-string v3, "Executing synchronized block"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 399
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Unload()V

    .line 400
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v3, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2, v3}, Lcom/sec/smartcard/client/SmartCardHelper;->access$302(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/pkcs11/Pkcs11;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 402
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 404
    :try_start_2
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-virtual {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->unregisterProvider()Z

    .line 405
    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;
    invoke-static {}, Lcom/sec/smartcard/client/SmartCardHelper;->access$500()Ljava/util/Map;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 406
    .local v1, "err":I
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$400(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/smartcard/client/SmartCardHelper$Callback;->onStatusChanged(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 410
    .end local v1    # "err":I
    :goto_0
    return-void

    .line 402
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    .line 407
    :catch_0
    move-exception v0

    .line 408
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSmartCardConnectionProgress(Ljava/lang/String;I)V
    .locals 3
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "state"    # I

    .prologue
    .line 383
    const-string v0, "SmartcardHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSmartCardConnectionProgress"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    return-void
.end method

.method public onSmartCardDisconnected(Ljava/lang/String;I)V
    .locals 5
    .param p1, "identifier"    # Ljava/lang/String;
    .param p2, "reasonCode"    # I

    .prologue
    .line 413
    const-string v2, "SmartcardHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onSmartCardDisconnected"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 415
    const/4 v2, 0x1

    :try_start_0
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z
    invoke-static {v3}, Lcom/sec/smartcard/client/SmartCardHelper;->access$200(Lcom/sec/smartcard/client/SmartCardHelper;)Z

    move-result v3

    if-ne v2, v3, :cond_0

    .line 418
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/smartcard/manager/ISmartCardService;->disconnectCryptoService()V

    .line 420
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 422
    :try_start_1
    const-string v2, "SmartcardHelper"

    const-string v3, "Executing synchronized block"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 424
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Unload()V

    .line 425
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v3, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;
    invoke-static {v2, v3}, Lcom/sec/smartcard/client/SmartCardHelper;->access$302(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/pkcs11/Pkcs11;)Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 427
    :cond_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 429
    :try_start_2
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-virtual {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->unregisterProvider()Z

    .line 431
    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;
    invoke-static {}, Lcom/sec/smartcard/client/SmartCardHelper;->access$500()Ljava/util/Map;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 432
    .local v1, "err":I
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper$2;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # getter for: Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;
    invoke-static {v2}, Lcom/sec/smartcard/client/SmartCardHelper;->access$400(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/sec/smartcard/client/SmartCardHelper$Callback;->onStatusChanged(I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 436
    .end local v1    # "err":I
    :goto_0
    return-void

    .line 427
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    .line 433
    :catch_0
    move-exception v0

    .line 434
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
