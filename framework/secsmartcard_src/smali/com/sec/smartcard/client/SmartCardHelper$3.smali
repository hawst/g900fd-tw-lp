.class Lcom/sec/smartcard/client/SmartCardHelper$3;
.super Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;
.source "SmartCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/smartcard/client/SmartCardHelper;->getPinSync()[C
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/smartcard/client/SmartCardHelper;

.field final synthetic val$cv:Landroid/os/ConditionVariable;


# direct methods
.method constructor <init>(Lcom/sec/smartcard/client/SmartCardHelper;Landroid/os/ConditionVariable;)V
    .locals 0

    .prologue
    .line 725
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    iput-object p2, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->val$cv:Landroid/os/ConditionVariable;

    invoke-direct {p0}, Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onUserCancelled()V
    .locals 2

    .prologue
    .line 736
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$602(Lcom/sec/smartcard/client/SmartCardHelper;[C)[C

    .line 737
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$702(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z

    .line 738
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->val$cv:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 739
    return-void
.end method

.method public onUserEnteredPin([C)V
    .locals 2
    .param p1, "pin"    # [C

    .prologue
    .line 729
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C
    invoke-static {v0, p1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$602(Lcom/sec/smartcard/client/SmartCardHelper;[C)[C

    .line 730
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$702(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z

    .line 731
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->val$cv:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 732
    return-void
.end method

.method public onUserPinError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 743
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x0

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$602(Lcom/sec/smartcard/client/SmartCardHelper;[C)[C

    .line 744
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->this$0:Lcom/sec/smartcard/client/SmartCardHelper;

    const/4 v1, 0x1

    # setter for: Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z
    invoke-static {v0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->access$702(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z

    .line 745
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper$3;->val$cv:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 746
    return-void
.end method
