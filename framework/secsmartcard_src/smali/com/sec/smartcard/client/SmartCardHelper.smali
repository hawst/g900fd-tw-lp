.class public Lcom/sec/smartcard/client/SmartCardHelper;
.super Ljava/lang/Object;
.source "SmartCardHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;,
        Lcom/sec/smartcard/client/SmartCardHelper$Callback;
    }
.end annotation


# static fields
.field protected static final CCM_CRYPTOKI_LIBRARYNAME:Ljava/lang/String; = "libtlc_tz_ccm.so"

.field protected static final CCM_C_GETFUNCTIONLIST_METHOD:Ljava/lang/String; = "TZ_CCM_C_GetFunctionList"

.field protected static final CCM_IDENTIFIER:Ljava/lang/String; = "ccm"

.field public static final ERROR_CANCELLED_BY_USER:I = 0x4

.field public static final ERROR_CONNECTION_BUSY:I = 0x9

.field public static final ERROR_DEVICE_NOT_CONFIGURED:I = 0x6

.field public static final ERROR_INTERNAL:I = 0xa

.field public static final ERROR_INVALID_PARAMETER:I = 0x2

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NO_CONNECTION:I = 0x5

.field public static final ERROR_SECURITY_VIOLATION:I = 0x3

.field public static final ERROR_SERVICE_NOT_PRESENT:I = 0x7

.field public static final ERROR_SERVICE_NOT_READY:I = 0x8

.field public static final ERROR_UNKNOWN:I = 0x1

.field protected static final OWN_CRYPTOKI_LIBRARYNAME:Ljava/lang/String; = "libSamsungPkcs11Wrapper.so"

.field protected static final OWN_C_GETFUNCTIONLIST_METHOD:Ljava/lang/String; = "C_GetFunctionList"

.field public static final SMARTCARD_SERVICE_VERSION_CODE:I = 0x3

.field static final TAG:Ljava/lang/String; = "SmartCardHelper"

.field private static final TEST_PIN_CODE:Ljava/lang/String; = "77777777"

.field private static final mAdapterSamsungErrorCodeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected static mContext:Landroid/content/Context;


# instance fields
.field private mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

.field private mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;

.field private mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

.field private mIsCCMProfileInitialized:Z

.field private mIsCallbackCalled:Z

.field private mIsSmartCardServiceBounded:Z

.field private mLastError:I

.field private mPin:[C

.field private mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

.field private mProviderName:Ljava/lang/String;

.field private mSmartcardServiceConnection:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x0

    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    .line 186
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x50

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x44

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0xa

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x4f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x49

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x52

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x41

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v1, 0x65

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    const-string v0, "secpkcs11_engine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 198
    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 207
    iput-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 211
    iput-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    .line 316
    iput-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    .line 317
    iput-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    .line 319
    new-instance v0, Lcom/sec/smartcard/client/SmartCardHelper$1;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/client/SmartCardHelper$1;-><init>(Lcom/sec/smartcard/client/SmartCardHelper;)V

    iput-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mSmartcardServiceConnection:Landroid/content/ServiceConnection;

    .line 356
    new-instance v0, Lcom/sec/smartcard/client/SmartCardHelper$2;

    invoke-direct {v0, p0}, Lcom/sec/smartcard/client/SmartCardHelper$2;-><init>(Lcom/sec/smartcard/client/SmartCardHelper;)V

    iput-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    .line 214
    return-void
.end method

.method static synthetic access$000(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/manager/ISmartCardService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/manager/ISmartCardService;)Lcom/sec/smartcard/manager/ISmartCardService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;
    .param p1, "x1"    # Lcom/sec/smartcard/manager/ISmartCardService;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/smartcard/client/SmartCardHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->connectCryptoService()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/smartcard/client/SmartCardHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 75
    iget-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/pkcs11/Pkcs11;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/smartcard/client/SmartCardHelper;Lcom/sec/smartcard/pkcs11/Pkcs11;)Lcom/sec/smartcard/pkcs11/Pkcs11;
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;
    .param p1, "x1"    # Lcom/sec/smartcard/pkcs11/Pkcs11;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/smartcard/client/SmartCardHelper;)Lcom/sec/smartcard/client/SmartCardHelper$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    return-object v0
.end method

.method static synthetic access$500()Ljava/util/Map;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/smartcard/client/SmartCardHelper;[C)[C
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;
    .param p1, "x1"    # [C

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C

    return-object p1
.end method

.method static synthetic access$702(Lcom/sec/smartcard/client/SmartCardHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/smartcard/client/SmartCardHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z

    return p1
.end method

.method private connectCryptoService()V
    .locals 4

    .prologue
    .line 340
    :try_start_0
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCryptoServiceListener:Lcom/sec/smartcard/adapter/ICryptoServiceListener;

    invoke-interface {v2, v3}, Lcom/sec/smartcard/manager/ISmartCardService;->connectCryptoService(Lcom/sec/smartcard/adapter/ICryptoServiceListener;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 341
    sget-object v2, Lcom/sec/smartcard/client/SmartCardHelper;->mAdapterSamsungErrorCodeMap:Ljava/util/Map;

    const/16 v3, 0x49

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 342
    .local v1, "err":I
    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    invoke-interface {v2, v1}, Lcom/sec/smartcard/client/SmartCardHelper$Callback;->onStatusChanged(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    .end local v1    # "err":I
    :cond_0
    :goto_0
    return-void

    .line 344
    :catch_0
    move-exception v0

    .line 345
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private doBindService()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 444
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.smartcard.manager.ISmartCardService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 445
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.sec.smartcard.manager"

    const-string v3, "com.sec.smartcard.manager.CardManagerService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 446
    sget-object v1, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mSmartcardServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v0, v2, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 447
    iput-boolean v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    .line 448
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/sec/smartcard/client/SmartCardHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 242
    const-class v0, Lcom/sec/smartcard/client/SmartCardHelper;

    monitor-enter v0

    const/4 v1, 0x0

    monitor-exit v0

    return-object v1
.end method

.method public static getPassCode()Ljava/lang/String;
    .locals 2

    .prologue
    .line 831
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "SmartCardHelper: got call from native!!!!!!!!!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 849
    const-string v0, "77777777"

    return-object v0
.end method

.method private initProvider()Ljava/lang/String;
    .locals 8

    .prologue
    .line 507
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v3, :cond_0

    .line 508
    const/4 v3, 0x0

    .line 532
    :goto_0
    return-object v3

    .line 510
    :cond_0
    const-string v3, "SECPkcs11"

    invoke-static {v3}, Ljava/security/Security;->getProvider(Ljava/lang/String;)Ljava/security/Provider;

    move-result-object v3

    if-nez v3, :cond_3

    .line 511
    new-instance v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;

    invoke-direct {v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;-><init>()V

    .line 512
    .local v0, "config":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
    invoke-virtual {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->getSmartCardId()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->moduleName:Ljava/lang/String;

    .line 513
    invoke-virtual {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->getSlotID_super()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-ltz v3, :cond_1

    .line 514
    invoke-virtual {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->getSlotID_super()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->slotId:J

    .line 517
    :cond_1
    iget-boolean v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    .line 518
    const-string v3, "SmartcardHelper"

    const-string v4, "initProvider with CCM profile "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    const-string v3, "libtlc_tz_ccm.so"

    iput-object v3, v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->libraryName:Ljava/lang/String;

    .line 520
    const-string v3, "TZ_CCM_C_GetFunctionList"

    iput-object v3, v0, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;->functionListName:Ljava/lang/String;

    .line 522
    :cond_2
    new-instance v2, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;

    invoke-direct {v2, v0}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;-><init>(Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;)V

    .line 523
    .local v2, "secProvider":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    invoke-virtual {v2}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->getName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mProviderName:Ljava/lang/String;

    .line 525
    new-instance v1, Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;

    invoke-direct {v1}, Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;-><init>()V

    .line 526
    .local v1, "jcePinCallback":Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;
    invoke-virtual {v1, p0}, Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;->setPinCallback(Lcom/sec/smartcard/client/SmartCardHelper;)V

    .line 527
    invoke-virtual {v2, v1}, Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;->setCallbackHandler(Ljavax/security/auth/callback/CallbackHandler;)V

    .line 528
    invoke-static {v2}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    .line 532
    .end local v0    # "config":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Configuration;
    .end local v1    # "jcePinCallback":Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;
    .end local v2    # "secProvider":Lcom/sec/enterprise/jce/provider/pkcs11/SecPkcs11Provider;
    :goto_1
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mProviderName:Ljava/lang/String;

    goto :goto_0

    .line 530
    :cond_3
    const-string v3, "SECPkcs11"

    iput-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mProviderName:Ljava/lang/String;

    goto :goto_1
.end method

.method private initWithCCM()V
    .locals 9

    .prologue
    .line 300
    monitor-enter p0

    .line 301
    :try_start_0
    const-string v4, "SmartcardHelper"

    const-string v5, "CCM profile initWithCCM Executing synchronized block"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v1

    .line 303
    .local v1, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    sget-object v4, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v4}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    .line 304
    .local v0, "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    const/4 v4, 0x0

    sget-object v5, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getSlotIdForPackage(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 305
    .local v2, "slot":J
    const-string v4, "SmartcardHelper"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initWithCCM slot alloted: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    iget-object v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-nez v4, :cond_0

    .line 307
    const-string v4, "SmartcardHelper"

    const-string v5, "Pkcs11 is null. So creating and loading Pkcs11"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    new-instance v4, Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-direct {v4}, Lcom/sec/smartcard/pkcs11/Pkcs11;-><init>()V

    iput-object v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 309
    iget-object v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    const-string v5, "libtlc_tz_ccm.so"

    const-string v6, "TZ_CCM_C_GetFunctionList"

    const-string v7, "ccm"

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Load(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 311
    :cond_0
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    .line 312
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    iget-object v4, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    invoke-interface {v4}, Lcom/sec/smartcard/client/SmartCardHelper$Callback;->onInitComplete()V

    .line 314
    return-void

    .line 312
    .end local v0    # "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    .end local v1    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .end local v2    # "slot":J
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method private loadSSLLibs()V
    .locals 1

    .prologue
    .line 352
    const-string v0, "secpkcs11_engine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 354
    return-void
.end method


# virtual methods
.method public deinitialize()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 474
    const-string v1, "SmartcardHelper"

    const-string v2, "deinitialize"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    if-ne v3, v1, :cond_0

    .line 479
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    invoke-interface {v1}, Lcom/sec/smartcard/manager/ISmartCardService;->disconnectCryptoService()V

    .line 480
    sget-object v1, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mSmartcardServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 481
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    .line 483
    :cond_0
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 485
    :try_start_1
    const-string v1, "SmartcardHelper"

    const-string v2, "Executing synchronized block"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 486
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    if-eqz v1, :cond_1

    .line 487
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    invoke-virtual {v1}, Lcom/sec/smartcard/pkcs11/Pkcs11;->Unload()V

    .line 488
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPkcs11Loader:Lcom/sec/smartcard/pkcs11/Pkcs11;

    .line 491
    :cond_1
    iget-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    if-ne v1, v3, :cond_2

    .line 492
    const-string v1, "SmartcardHelper"

    const-string v2, "deinitialize CCM profile ended "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    .line 495
    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 497
    :try_start_2
    invoke-virtual {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->unregisterProvider()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 501
    :goto_0
    return-void

    .line 495
    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v1
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    .line 498
    :catch_0
    move-exception v0

    .line 499
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPin(Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;)V
    .locals 4
    .param p1, "callback"    # Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;

    .prologue
    .line 632
    :try_start_0
    iget-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 633
    const-string v1, "SmartcardHelper"

    const-string v2, "getpin null for CCM "

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 634
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;->onUserEnteredPin([C)V

    .line 643
    :goto_0
    return-void

    .line 636
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    iget-object v2, p1, Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;->mICallback:Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;

    invoke-interface {v1, v2}, Lcom/sec/smartcard/manager/ISmartCardService;->getPin(Lcom/sec/smartcard/pinservice/ISmartCardGetPinCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 639
    :catch_0
    move-exception v0

    .line 641
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SmartCardHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized getPinSync()[C
    .locals 2

    .prologue
    .line 720
    monitor-enter p0

    :try_start_0
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    .line 721
    .local v0, "cv":Landroid/os/ConditionVariable;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C

    .line 723
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z

    .line 725
    new-instance v1, Lcom/sec/smartcard/client/SmartCardHelper$3;

    invoke-direct {v1, p0, v0}, Lcom/sec/smartcard/client/SmartCardHelper$3;-><init>(Lcom/sec/smartcard/client/SmartCardHelper;Landroid/os/ConditionVariable;)V

    invoke-virtual {p0, v1}, Lcom/sec/smartcard/client/SmartCardHelper;->getPin(Lcom/sec/smartcard/pinservice/SmartCardPinManager$PinCallback;)V

    .line 748
    iget-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCallbackCalled:Z

    if-nez v1, :cond_0

    .line 749
    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 751
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mPin:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v1

    .line 720
    .end local v0    # "cv":Landroid/os/ConditionVariable;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method protected getPkcs11LibraryName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 910
    iget-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 912
    const-string v0, "SmartCardHelper"

    const-string v1, "getPkcs11LibraryName libtlc_tz_ccm.so"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 913
    const-string v0, "libtlc_tz_ccm.so"

    .line 917
    :goto_0
    return-object v0

    .line 916
    :cond_0
    const-string v0, "SmartCardHelper"

    const-string v1, "getPkcs11LibraryName libSamsungPkcs11Wrapper.so"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 917
    const-string v0, "libSamsungPkcs11Wrapper.so"

    goto :goto_0
.end method

.method protected getPkcs11externalFunctionName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 929
    iget-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 931
    const-string v0, "SmartCardHelper"

    const-string v1, "getPkcs11externalFunctionName TZ_CCM_C_GetFunctionList"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 932
    const-string v0, "TZ_CCM_C_GetFunctionList"

    .line 936
    :goto_0
    return-object v0

    .line 935
    :cond_0
    const-string v0, "SmartCardHelper"

    const-string v1, "getPkcs11externalFunctionName C_GetFunctionList"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 936
    const-string v0, "C_GetFunctionList"

    goto :goto_0
.end method

.method protected getSlotID_super()J
    .locals 8

    .prologue
    .line 655
    :try_start_0
    iget-boolean v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v6, 0x1

    if-ne v3, v6, :cond_0

    .line 656
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v2

    .line 657
    .local v2, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    sget-object v3, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    .line 658
    .local v0, "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    const/4 v3, 0x0

    sget-object v6, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v3, v6}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getSlotIdForPackage(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 659
    .local v4, "slot":J
    const-string v3, "SmartcardHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSlotID with ccm profile "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    .end local v0    # "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    .end local v2    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .end local v4    # "slot":J
    :goto_0
    return-wide v4

    .line 662
    :cond_0
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    invoke-interface {v3}, Lcom/sec/smartcard/manager/ISmartCardService;->getSlotID()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 665
    :catch_0
    move-exception v1

    .line 667
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "SmartCardHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method protected getSlotIDwithAlias(Ljava/lang/String;)J
    .locals 8
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 684
    :try_start_0
    iget-boolean v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v6, 0x1

    if-ne v3, v6, :cond_0

    .line 685
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getInstance()Lcom/sec/enterprise/knox/EnterpriseKnoxManager;

    move-result-object v2

    .line 686
    .local v2, "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    sget-object v3, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lcom/sec/enterprise/knox/EnterpriseKnoxManager;->getClientCertificateManagerPolicy(Landroid/content/Context;)Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;

    move-result-object v0

    .line 687
    .local v0, "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    sget-object v3, Lcom/sec/smartcard/client/SmartCardHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;->getSlotIdForPackage(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 688
    .local v4, "slot":J
    const-string v3, "SmartcardHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getSlotIDwithAlias with ccm profile alias: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", slot:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    .end local v0    # "ccm":Lcom/sec/enterprise/knox/ccm/ClientCertificateManager;
    .end local v2    # "ekm":Lcom/sec/enterprise/knox/EnterpriseKnoxManager;
    .end local v4    # "slot":J
    :goto_0
    return-wide v4

    .line 691
    :cond_0
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    invoke-interface {v3}, Lcom/sec/smartcard/manager/ISmartCardService;->getSlotID()J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 694
    :catch_0
    move-exception v1

    .line 696
    .local v1, "e":Landroid/os/RemoteException;
    const-string v3, "SmartCardHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Exception : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 699
    const-wide/16 v4, -0x1

    goto :goto_0
.end method

.method public getSmartCardId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 606
    iget-boolean v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 607
    const-string v1, "SmartcardHelper"

    const-string v2, "getSmartCardId with CCM profile: ccm"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 608
    const-string v1, "ccm"

    .line 617
    :goto_0
    return-object v1

    .line 610
    :cond_0
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    if-eqz v1, :cond_1

    .line 612
    :try_start_0
    iget-object v1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    invoke-interface {v1}, Lcom/sec/smartcard/manager/ISmartCardService;->getCryptoServiceSocket()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    .line 617
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public initialize(Lcom/sec/smartcard/client/SmartCardHelper$Callback;)V
    .locals 2
    .param p1, "cb"    # Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mCallback:Lcom/sec/smartcard/client/SmartCardHelper$Callback;

    .line 279
    const-string v0, "SmartcardHelper"

    const-string v1, "initialize"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    iget-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsSmartCardServiceBounded:Z

    if-nez v0, :cond_1

    .line 288
    const-string v0, "SmartcardHelper"

    const-string v1, "dobindservice called"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    invoke-direct {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->doBindService()V

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mBoundSmartCardService:Lcom/sec/smartcard/manager/ISmartCardService;

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "SmartcardHelper"

    const-string v1, "Connecting to cryptoservie..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-direct {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->connectCryptoService()V

    goto :goto_0
.end method

.method protected isCCMFeatureEnabled()Z
    .locals 1

    .prologue
    .line 711
    iget-boolean v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mIsCCMProfileInitialized:Z

    return v0
.end method

.method public declared-synchronized registerProvider()Ljava/lang/String;
    .locals 2

    .prologue
    .line 575
    monitor-enter p0

    :try_start_0
    const-string v0, "SmartcardHelper"

    const-string v1, "registerProvider synchronized"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 576
    invoke-direct {p0}, Lcom/sec/smartcard/client/SmartCardHelper;->initProvider()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 575
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public native registerSSLEngine()Z
.end method

.method public unregisterProvider()Z
    .locals 1

    .prologue
    .line 590
    iget-object v0, p0, Lcom/sec/smartcard/client/SmartCardHelper;->mProviderName:Ljava/lang/String;

    invoke-static {v0}, Ljava/security/Security;->removeProvider(Ljava/lang/String;)V

    .line 591
    const/4 v0, 0x1

    return v0
.end method

.method public native unregisterSSLEngine()Z
.end method
