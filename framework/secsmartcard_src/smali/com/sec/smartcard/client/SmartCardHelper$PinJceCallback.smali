.class Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;
.super Ljava/lang/Object;
.source "SmartCardHelper.java"

# interfaces
.implements Ljavax/security/auth/callback/CallbackHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/smartcard/client/SmartCardHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PinJceCallback"
.end annotation


# instance fields
.field private pinHelper:Lcom/sec/smartcard/client/SmartCardHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 790
    return-void
.end method


# virtual methods
.method public handle([Ljavax/security/auth/callback/Callback;)V
    .locals 6
    .param p1, "callbacks"    # [Ljavax/security/auth/callback/Callback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljavax/security/auth/callback/UnsupportedCallbackException;
        }
    .end annotation

    .prologue
    .line 810
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 811
    aget-object v3, p1, v0

    instance-of v3, v3, Ljavax/security/auth/callback/PasswordCallback;

    if-eqz v3, :cond_0

    .line 813
    aget-object v2, p1, v0

    check-cast v2, Ljavax/security/auth/callback/PasswordCallback;

    .line 815
    .local v2, "pc":Ljavax/security/auth/callback/PasswordCallback;
    iget-object v3, p0, Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;->pinHelper:Lcom/sec/smartcard/client/SmartCardHelper;

    invoke-virtual {v3}, Lcom/sec/smartcard/client/SmartCardHelper;->getPinSync()[C

    move-result-object v1

    .line 817
    .local v1, "input":[C
    invoke-virtual {v2, v1}, Ljavax/security/auth/callback/PasswordCallback;->setPassword([C)V

    .line 810
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 819
    .end local v1    # "input":[C
    .end local v2    # "pc":Ljavax/security/auth/callback/PasswordCallback;
    :cond_0
    new-instance v3, Ljavax/security/auth/callback/UnsupportedCallbackException;

    aget-object v4, p1, v0

    const-string v5, "Unrecognized Callback"

    invoke-direct {v3, v4, v5}, Ljavax/security/auth/callback/UnsupportedCallbackException;-><init>(Ljavax/security/auth/callback/Callback;Ljava/lang/String;)V

    throw v3

    .line 822
    :cond_1
    return-void
.end method

.method public setPinCallback(Lcom/sec/smartcard/client/SmartCardHelper;)V
    .locals 0
    .param p1, "pm"    # Lcom/sec/smartcard/client/SmartCardHelper;

    .prologue
    .line 797
    iput-object p1, p0, Lcom/sec/smartcard/client/SmartCardHelper$PinJceCallback;->pinHelper:Lcom/sec/smartcard/client/SmartCardHelper;

    .line 798
    return-void
.end method
