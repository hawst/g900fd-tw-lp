.class public final Lcom/sec/generic/util/log/Log;
.super Ljava/lang/Object;
.source "Log.java"


# static fields
.field public static final LVL_DEBUG:I = 0x3

.field public static final LVL_ERROR:I = 0x6

.field public static final LVL_INFO:I = 0x4

.field public static final LVL_SENSITIVE:I = 0x1

.field public static final LVL_VERBOSE:I = 0x2

.field public static final LVL_WARNING:I = 0x5

.field private static mLoggerList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/generic/util/log/Logger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method public static declared-synchronized addLogger(Lcom/sec/generic/util/log/Logger;)Z
    .locals 3
    .param p0, "logger"    # Lcom/sec/generic/util/log/Logger;

    .prologue
    const/4 v0, 0x0

    .line 69
    const-class v1, Lcom/sec/generic/util/log/Log;

    monitor-enter v1

    if-nez p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    monitor-exit v1

    return v0

    .line 72
    :cond_1
    :try_start_0
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 73
    sget-object v0, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x3

    .line 131
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 132
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 136
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x3

    .line 223
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 224
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 225
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 228
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x6

    .line 176
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 177
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 181
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x6

    .line 271
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 272
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static declared-synchronized getLogger(Ljava/lang/String;)Lcom/sec/generic/util/log/Logger;
    .locals 5
    .param p0, "loggerName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 97
    const-class v3, Lcom/sec/generic/util/log/Log;

    monitor-enter v3

    if-nez p0, :cond_0

    move-object v1, v2

    .line 105
    :goto_0
    monitor-exit v3

    return-object v1

    .line 100
    :cond_0
    :try_start_0
    sget-object v4, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 101
    .local v1, "logger":Lcom/sec/generic/util/log/Logger;
    iget-object v4, v1, Lcom/sec/generic/util/log/Logger;->loggerName:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    .end local v1    # "logger":Lcom/sec/generic/util/log/Logger;
    :cond_2
    move-object v1, v2

    .line 105
    goto :goto_0

    .line 97
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 4
    .param p0, "tr"    # Ljava/lang/Throwable;

    .prologue
    .line 300
    if-nez p0, :cond_0

    .line 301
    const-string v3, ""

    .line 318
    :goto_0
    return-object v3

    .line 307
    :cond_0
    move-object v2, p0

    .line 308
    .local v2, "t":Ljava/lang/Throwable;
    :goto_1
    if-eqz v2, :cond_2

    .line 309
    instance-of v3, v2, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 310
    const-string v3, ""

    goto :goto_0

    .line 312
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    goto :goto_1

    .line 315
    :cond_2
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 316
    .local v1, "sw":Ljava/io/StringWriter;
    new-instance v0, Ljava/io/PrintWriter;

    invoke-direct {v0, v1}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 317
    .local v0, "pw":Ljava/io/PrintWriter;
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 318
    invoke-virtual {v1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x4

    .line 146
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 147
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 151
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x4

    .line 239
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 240
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 244
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static declared-synchronized removeLogger(Lcom/sec/generic/util/log/Logger;)Z
    .locals 2
    .param p0, "logger"    # Lcom/sec/generic/util/log/Logger;

    .prologue
    .line 84
    const-class v1, Lcom/sec/generic/util/log/Log;

    monitor-enter v1

    if-nez p0, :cond_0

    .line 85
    const/4 v0, 0x0

    .line 88
    :goto_0
    monitor-exit v1

    return v0

    .line 87
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/generic/util/log/Logger;->flush()V

    .line 88
    sget-object v0, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static s(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 191
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 192
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 193
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 196
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static s(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x1

    .line 287
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 288
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 292
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static stringValueOf(I)Ljava/lang/String;
    .locals 1
    .param p0, "logLevel"    # I

    .prologue
    .line 325
    packed-switch p0, :pswitch_data_0

    .line 339
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 327
    :pswitch_0
    const-string v0, "SENSITIVE"

    goto :goto_0

    .line 329
    :pswitch_1
    const-string v0, "VERBOSE"

    goto :goto_0

    .line 331
    :pswitch_2
    const-string v0, "DEBUG"

    goto :goto_0

    .line 333
    :pswitch_3
    const-string v0, "INFO"

    goto :goto_0

    .line 335
    :pswitch_4
    const-string v0, "WARNING"

    goto :goto_0

    .line 337
    :pswitch_5
    const-string v0, "ERROR"

    goto :goto_0

    .line 325
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    .line 116
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 117
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 118
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 121
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x2

    .line 207
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 208
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x5

    .line 161
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 162
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v3}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 163
    invoke-virtual {v1, v3, p0, p1}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 166
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method

.method public static w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p0, "tag"    # Ljava/lang/String;
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "tr"    # Ljava/lang/Throwable;

    .prologue
    const/4 v4, 0x5

    .line 255
    sget-object v2, Lcom/sec/generic/util/log/Log;->mLoggerList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/generic/util/log/Logger;

    .line 256
    .local v1, "ilog":Lcom/sec/generic/util/log/Logger;
    invoke-virtual {v1, v4}, Lcom/sec/generic/util/log/Logger;->isLoggable(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 257
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p2}, Lcom/sec/generic/util/log/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, p0, v2}, Lcom/sec/generic/util/log/Logger;->log(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    .end local v1    # "ilog":Lcom/sec/generic/util/log/Logger;
    :cond_1
    return-void
.end method
