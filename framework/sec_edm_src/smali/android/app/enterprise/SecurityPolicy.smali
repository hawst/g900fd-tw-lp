.class public Landroid/app/enterprise/SecurityPolicy;
.super Ljava/lang/Object;
.source "SecurityPolicy.java"


# static fields
.field public static final CA_CERTIFICATE:Ljava/lang/String; = "CACERT_"

.field public static final DOD_BANNER_PACKAGE:Ljava/lang/String; = "com.samsung.android.mdm"

.field public static final GLOBAL_KEYSTORE_PARAMS:I = 0x2

.field public static final KEYSTORE_DEFAULT:I = 0x1

.field public static final KEYSTORE_FOR_VPN_AND_APPS:I = 0x4

.field public static final KEYSTORE_FOR_WIFI:I = 0x2

.field public static final KEYSTORE_KEY_NOT_FOUND:I = 0x7

.field public static final KEYSTORE_LOCKED:I = 0x2

.field public static final KEYSTORE_NO_ERROR:I = 0x1

.field public static final KEYSTORE_PERMISSION_DENIED:I = 0x6

.field public static final KEYSTORE_SYSTEM_ERROR:I = 0x4

.field public static final KEYSTORE_UNDEFINED_ACTION:I = 0x9

.field public static final KEYSTORE_UNINITIALIZED:I = 0x3

.field public static final KEYSTORE_VALUE_CORRUPTED:I = 0x8

.field public static final KEYSTORE_WRONG_PASSWORD:I = 0xa

.field public static MAXIMUM_CERTIFICATE_NUMBERS:I = 0x0

.field private static TAG:Ljava/lang/String; = null

.field public static final TYPE_CERTIFICATE:Ljava/lang/String; = "CERT"

.field public static final TYPE_PKCS12:Ljava/lang/String; = "PKCS12"

.field public static final USER_CERTIFICATE:Ljava/lang/String; = "USRCERT_"

.field public static final USER_KEYSTORE_PARAMS:I = 0x5

.field public static final WIPE_EXTERNAL_MEMORY:I = 0x2

.field public static final WIPE_INTERNAL_EXTERNAL_MEMORY:I = 0x3

.field public static final WIPE_INTERNAL_MEMORY:I = 0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/ISecurityPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "SecurityPolicy"

    sput-object v0, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    .line 1300
    const/16 v0, 0x1e

    sput v0, Landroid/app/enterprise/SecurityPolicy;->MAXIMUM_CERTIFICATE_NUMBERS:I

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 212
    iput-object p2, p0, Landroid/app/enterprise/SecurityPolicy;->mContext:Landroid/content/Context;

    .line 213
    return-void
.end method

.method private formatInternalStorage(ZZ)Z
    .locals 3
    .param p1, "includeSystemDirectory"    # Z
    .param p2, "includeDataDirectory"    # Z

    .prologue
    .line 636
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 638
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->formatInternalStorage(Landroid/app/enterprise/ContextInfo;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 643
    :goto_0
    return v1

    .line 639
    :catch_0
    move-exception v0

    .line 640
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 643
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private formatSelective([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "fileList"    # [Ljava/lang/String;
    .param p2, "filters"    # [Ljava/lang/String;

    .prologue
    .line 608
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 610
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->formatSelective(Landroid/app/enterprise/ContextInfo;[Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 615
    :goto_0
    return-object v1

    .line 611
    :catch_0
    move-exception v0

    .line 612
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 615
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private formatStorageCard(Z)Z
    .locals 2
    .param p1, "isExternal"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 656
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v1, p1}, Landroid/app/enterprise/ISecurityPolicy;->formatStorageCard(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v0

    .line 659
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static generateToken(II)I
    .locals 3
    .param p0, "min"    # I
    .param p1, "max"    # I

    .prologue
    .line 1303
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 1304
    .local v0, "rand":Ljava/util/Random;
    sub-int v2, p1, p0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int v1, v2, p0

    .line 1306
    .local v1, "randomNum":I
    return v1
.end method

.method private getService()Landroid/app/enterprise/ISecurityPolicy;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    if-nez v0, :cond_0

    .line 217
    const-string v0, "security_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/ISecurityPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    .line 220
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    return-object v0
.end method


# virtual methods
.method public changeCredentialStoragePassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "oldpassword"    # Ljava/lang/String;
    .param p2, "newpassword"    # Ljava/lang/String;

    .prologue
    .line 402
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.changeCredentialStoragePassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 403
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 405
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->changeCredentialStoragePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 410
    :goto_0
    return v1

    .line 406
    :catch_0
    move-exception v0

    .line 407
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed changeCredentialStoragePassword"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 410
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearInstalledCertificates()Z
    .locals 3

    .prologue
    .line 544
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.clearInstalledCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 545
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 547
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->clearInstalledCertificates(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 552
    :goto_0
    return v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed clearInstalledCertificates"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteCertificateFromKeystore(Landroid/app/enterprise/CertificateInfo;I)Z
    .locals 3
    .param p1, "certInfo"    # Landroid/app/enterprise/CertificateInfo;
    .param p2, "keystore"    # I

    .prologue
    .line 1315
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.deleteCertificateFromKeystore"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1316
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1318
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->deleteCertificateFromKeystore(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/CertificateInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1323
    :goto_0
    return v1

    .line 1319
    :catch_0
    move-exception v0

    .line 1320
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1323
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteCertificateFromUserKeystore(Landroid/app/enterprise/CertificateInfo;I)Z
    .locals 2
    .param p1, "certInfo"    # Landroid/app/enterprise/CertificateInfo;
    .param p2, "keystore"    # I

    .prologue
    .line 1351
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "SecurityPolicy.deleteCertificateFromUserKeystore"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1352
    const/4 v0, 0x0

    return v0
.end method

.method public enableRebootBanner(Z)Z
    .locals 3
    .param p1, "isEnrolled"    # Z

    .prologue
    .line 1121
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1123
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->enableRebootBanner(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1128
    :goto_0
    return v1

    .line 1124
    :catch_0
    move-exception v0

    .line 1125
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1128
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableRebootBanner(ZLjava/lang/String;)Z
    .locals 3
    .param p1, "isEnrolled"    # Z
    .param p2, "bannerText"    # Ljava/lang/String;

    .prologue
    .line 1375
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.enableRebootBanner"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1376
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1378
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->enableRebootBannerWithText(Landroid/app/enterprise/ContextInfo;ZLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1383
    :goto_0
    return v1

    .line 1379
    :catch_0
    move-exception v0

    .line 1380
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1383
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCertificatesFromKeystore(I)Ljava/util/List;
    .locals 7
    .param p1, "keystore"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1268
    iget-object v4, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "SecurityPolicy.getCertificatesFromKeystore"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1269
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1270
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1275
    .local v1, "fullList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    const/4 v4, 0x0

    const/16 v5, 0x64

    :try_start_0
    invoke-static {v4, v5}, Landroid/app/enterprise/SecurityPolicy;->generateToken(II)I

    move-result v3

    .line 1276
    .local v3, "token":I
    const/4 v2, 0x0

    .line 1280
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_0
    iget-object v4, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v5, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, v3}, Landroid/app/enterprise/ISecurityPolicy;->getCertificatesFromKeystore(Landroid/app/enterprise/ContextInfo;II)Ljava/util/List;

    move-result-object v2

    .line 1281
    if-nez v2, :cond_1

    .line 1282
    const/4 v1, 0x0

    .line 1291
    .end local v1    # "fullList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "token":I
    :goto_0
    return-object v1

    .line 1284
    .restart local v1    # "fullList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .restart local v3    # "token":I
    :cond_1
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1285
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    sget v5, Landroid/app/enterprise/SecurityPolicy;->MAXIMUM_CERTIFICATE_NUMBERS:I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v4, v5, :cond_0

    goto :goto_0

    .line 1287
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    .end local v3    # "token":I
    :catch_0
    move-exception v0

    .line 1288
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with security policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1291
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "fullList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getCertificatesFromUserKeystore(I)Ljava/util/List;
    .locals 2
    .param p1, "keystore"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1342
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "SecurityPolicy.getCertificatesFromUserKeystore"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1343
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public getCredentialStorageStatus()I
    .locals 3

    .prologue
    .line 422
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 424
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->getCredentialStorageStatus(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 429
    :goto_0
    return v1

    .line 425
    :catch_0
    move-exception v0

    .line 426
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getCredentialStorageStatus"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 429
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public getDeviceLastAccessDate()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1232
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1234
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->getDeviceLastAccessDate(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1239
    :goto_0
    return-object v1

    .line 1235
    :catch_0
    move-exception v0

    .line 1236
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1239
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getFileNamesOnDevice(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 916
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getFileNamesOnDevice"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 917
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 919
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getFileNamesOnDevice(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 924
    :goto_0
    return-object v1

    .line 920
    :catch_0
    move-exception v0

    .line 921
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 924
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getFileNamesWithAttributes(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 947
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getFileNamesWithAttributes"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 948
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 950
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getFileNamesWithAttributes(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 955
    :goto_0
    return-object v1

    .line 951
    :catch_0
    move-exception v0

    .line 952
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 955
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getFileNamesWithAttributesRecursive(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    .line 994
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getFileNamesWithAttributesRecursive"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 995
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 997
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->getFileNamesWithAttributesRecursive(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1002
    :goto_0
    return v1

    .line 998
    :catch_0
    move-exception v0

    .line 999
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1002
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInstalledCertificate(Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    .locals 3
    .param p1, "certificateName"    # Ljava/lang/String;

    .prologue
    .line 328
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getInstalledCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 329
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 331
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getInstalledCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 336
    :goto_0
    return-object v1

    .line 332
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 336
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Landroid/app/enterprise/CertificateInfo;

    invoke-direct {v1}, Landroid/app/enterprise/CertificateInfo;-><init>()V

    goto :goto_0
.end method

.method public getInstalledCertificateNames(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getInstalledCertificateNames"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 354
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 356
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getInstalledCertificateNames(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 361
    :goto_0
    return-object v1

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getInstalledCertificateNames"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getInstalledCertificates()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 312
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getInstalledCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 313
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 315
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->getInstalledCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 320
    :goto_0
    return-object v1

    .line 316
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 320
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getRebootBannerText()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1397
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1399
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->getRebootBannerText(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1404
    :goto_0
    return-object v1

    .line 1400
    :catch_0
    move-exception v0

    .line 1401
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1404
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRequireDeviceEncryption(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 798
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 800
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getRequireDeviceEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 805
    :goto_0
    return v1

    .line 801
    :catch_0
    move-exception v0

    .line 802
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 805
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRequireStorageCardEncryption(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 840
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 842
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->getRequireStorageCardEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 847
    :goto_0
    return v1

    .line 843
    :catch_0
    move-exception v0

    .line 844
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 847
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemCertificates()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 286
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.getSystemCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 287
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 289
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->getSystemCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 294
    :goto_0
    return-object v1

    .line 290
    :catch_0
    move-exception v0

    .line 291
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 294
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public installCertificate(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;

    .prologue
    .line 495
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "SecurityPolicy.installCertificate"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 496
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 498
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/ISecurityPolicy;->installCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 503
    :goto_0
    return v0

    .line 499
    :catch_0
    move-exception v6

    .line 500
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with misc policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 503
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public installCertificateToKeystore(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I)Z
    .locals 8
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "keystore"    # I

    .prologue
    .line 1252
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "SecurityPolicy.installCertificateToKeystore"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1253
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1255
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/app/enterprise/ISecurityPolicy;->installCertificateToKeystore(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1261
    :goto_0
    return v0

    .line 1257
    :catch_0
    move-exception v7

    .line 1258
    .local v7, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with security policy"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1261
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public installCertificateToUserKeystore(Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I)Z
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # [B
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "password"    # Ljava/lang/String;
    .param p5, "keystore"    # I

    .prologue
    .line 1333
    iget-object v0, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "SecurityPolicy.installCertificateToUserKeystore"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1334
    const/4 v0, 0x0

    return v0
.end method

.method public installCertificateWithType(Ljava/lang/String;[B)V
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 241
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.installCertificateWithType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 242
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 244
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->installCertificateWithType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public installCertificatesFromSdCard()V
    .locals 3

    .prologue
    .line 261
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.installCertificatesFromSdCard"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 262
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 264
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->installCertificatesFromSdCard(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 265
    :catch_0
    move-exception v0

    .line 266
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isDodBannerVisible()Z
    .locals 3

    .prologue
    .line 1188
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1190
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->isDodBannerVisible(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1195
    :goto_0
    return v1

    .line 1191
    :catch_0
    move-exception v0

    .line 1192
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1195
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isExternalStorageEncrypted()Z
    .locals 3

    .prologue
    .line 754
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.isExternalStorageEncrypted"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 755
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 757
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->isExternalStorageEncrypted(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 762
    :goto_0
    return v1

    .line 758
    :catch_0
    move-exception v0

    .line 759
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 762
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isInternalStorageEncrypted()Z
    .locals 3

    .prologue
    .line 728
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.isInternalStorageEncrypted"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 729
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 731
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->isInternalStorageEncrypted(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 736
    :goto_0
    return v1

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 736
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRebootBannerEnabled()Z
    .locals 3

    .prologue
    .line 1143
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1145
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->isRebootBannerEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1150
    :goto_0
    return v1

    .line 1146
    :catch_0
    move-exception v0

    .line 1147
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1150
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public lockoutDevice(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1064
    .local p3, "phones":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez p1, :cond_0

    .line 1065
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Lockout password cannot be null!"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1068
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1070
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/ISecurityPolicy;->lockoutDevice(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1075
    :cond_1
    :goto_0
    return-void

    .line 1071
    :catch_0
    move-exception v0

    .line 1072
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public powerOffDevice()V
    .locals 3

    .prologue
    .line 887
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.powerOffDevice"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 888
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 890
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->powerOffDevice(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 895
    :cond_0
    :goto_0
    return-void

    .line 891
    :catch_0
    move-exception v0

    .line 892
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public readFile(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
    .locals 3
    .param p1, "pathOrig"    # Ljava/lang/String;
    .param p2, "output"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 1029
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.readFile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1030
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1032
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->readFile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1037
    :goto_0
    return v1

    .line 1033
    :catch_0
    move-exception v0

    .line 1034
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1037
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAccountsByType(Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 866
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.removeAccountsByType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 867
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 869
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->removeAccountsByType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 874
    :goto_0
    return v1

    .line 870
    :catch_0
    move-exception v0

    .line 871
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 874
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "type"    # Ljava/lang/String;

    .prologue
    .line 521
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.removeCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 522
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 524
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->removeCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 529
    :goto_0
    return v1

    .line 525
    :catch_0
    move-exception v0

    .line 526
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed unlockCredentialStorage"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 529
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeDeviceLockout()V
    .locals 3

    .prologue
    .line 1090
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1092
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->removeDeviceLockout(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1097
    :cond_0
    :goto_0
    return-void

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public resetCredentialStorage()Z
    .locals 3

    .prologue
    .line 468
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.resetCredentialStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 469
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 471
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ISecurityPolicy;->resetCredentialStorage(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 476
    :goto_0
    return v1

    .line 472
    :catch_0
    move-exception v0

    .line 473
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed resetCredentialStorage"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 476
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCredentialStoragePassword(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 377
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.setCredentialStoragePassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 378
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 380
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->setCredentialStoragePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 385
    :goto_0
    return v1

    .line 381
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setCredentialStoragePassword"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 385
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDeviceLastAccessDate(Ljava/lang/String;)Z
    .locals 3
    .param p1, "date"    # Ljava/lang/String;

    .prologue
    .line 1211
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1213
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->setDeviceLastAccessDate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1219
    :goto_0
    return v1

    .line 1214
    :catch_0
    move-exception v0

    .line 1215
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1219
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDodBannerVisibleStatus(Z)Z
    .locals 3
    .param p1, "isVisible"    # Z

    .prologue
    .line 1167
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1169
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->setDodBannerVisibleStatus(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1175
    :goto_0
    return v1

    .line 1170
    :catch_0
    move-exception v0

    .line 1171
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1175
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setExternalStorageEncryption(Z)V
    .locals 3
    .param p1, "isEncrypt"    # Z

    .prologue
    .line 703
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.setExternalStorageEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 704
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 706
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->setExternalStorageEncryption(Landroid/app/enterprise/ContextInfo;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 707
    :catch_0
    move-exception v0

    .line 708
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setInternalStorageEncryption(Z)V
    .locals 3
    .param p1, "isEncrypt"    # Z

    .prologue
    .line 678
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.setInternalStorageEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 679
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 681
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->setInternalStorageEncryption(Landroid/app/enterprise/ContextInfo;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 686
    :cond_0
    :goto_0
    return-void

    .line 682
    :catch_0
    move-exception v0

    .line 683
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRequireDeviceEncryption(Landroid/content/ComponentName;Z)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # Z

    .prologue
    .line 778
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.setRequireDeviceEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 779
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 781
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->setRequireDeviceEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 786
    :cond_0
    :goto_0
    return-void

    .line 782
    :catch_0
    move-exception v0

    .line 783
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRequireStorageCardEncryption(Landroid/content/ComponentName;Z)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # Z

    .prologue
    .line 820
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.setRequireStorageCardEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 821
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 823
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ISecurityPolicy;->setRequireStorageCardEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 824
    :catch_0
    move-exception v0

    .line 825
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unlockCredentialStorage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 445
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SecurityPolicy.unlockCredentialStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 446
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 448
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/SecurityPolicy;->mService:Landroid/app/enterprise/ISecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ISecurityPolicy;->unlockCredentialStorage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 453
    :goto_0
    return v1

    .line 449
    :catch_0
    move-exception v0

    .line 450
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed unlockCredentialStorage"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 453
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public wipeDevice(I)Z
    .locals 6
    .param p1, "flags"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 571
    iget-object v2, p0, Landroid/app/enterprise/SecurityPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "SecurityPolicy.wipeDevice"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 572
    const/4 v1, 0x0

    .line 573
    .local v1, "result":Z
    invoke-direct {p0}, Landroid/app/enterprise/SecurityPolicy;->getService()Landroid/app/enterprise/ISecurityPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 575
    const/4 v2, 0x2

    if-eq p1, v2, :cond_0

    if-ne p1, v5, :cond_1

    .line 577
    :cond_0
    const/4 v2, 0x1

    :try_start_0
    invoke-direct {p0, v2}, Landroid/app/enterprise/SecurityPolicy;->formatStorageCard(Z)Z

    move-result v1

    .line 579
    :cond_1
    if-eq p1, v4, :cond_2

    if-ne p1, v5, :cond_3

    .line 581
    :cond_2
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Landroid/app/enterprise/SecurityPolicy;->formatStorageCard(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 588
    :cond_3
    :goto_0
    return v1

    .line 584
    :catch_0
    move-exception v0

    .line 585
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/SecurityPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with misc info policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
