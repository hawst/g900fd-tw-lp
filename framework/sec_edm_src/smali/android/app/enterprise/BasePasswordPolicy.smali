.class public Landroid/app/enterprise/BasePasswordPolicy;
.super Ljava/lang/Object;
.source "BasePasswordPolicy.java"


# static fields
.field public static final PASSWORD_QUALITY_FINGERPRINT:I = 0x61000

.field public static final PASSWORD_QUALITY_SMARTCARDNUMERIC:I = 0x70000

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IPasswordPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-string v0, "BasePasswordPolicy"

    sput-object v0, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 36
    return-void
.end method

.method private getService()Landroid/app/enterprise/IPasswordPolicy;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    if-nez v0, :cond_0

    .line 40
    const-string v0, "password_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IPasswordPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    .line 43
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    return-object v0
.end method


# virtual methods
.method public getCurrentFailedPasswordAttempts()I
    .locals 3

    .prologue
    .line 500
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 502
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getCurrentFailedPasswordAttempts(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 507
    :goto_0
    return v1

    .line 503
    :catch_0
    move-exception v0

    .line 504
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 507
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 539
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 541
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumFailedPasswordsForWipe(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 546
    :goto_0
    return v1

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 546
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumTimeToLock(Landroid/content/ComponentName;)J
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 586
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 588
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumTimeToLock(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 593
    :goto_0
    return-wide v2

    .line 589
    :catch_0
    move-exception v0

    .line 590
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 593
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getPasswordExpiration(Landroid/content/ComponentName;)J
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 447
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 449
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordExpiration(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 454
    :goto_0
    return-wide v2

    .line 450
    :catch_0
    move-exception v0

    .line 451
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 454
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getPasswordExpirationTimeout(Landroid/content/ComponentName;)J
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 426
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 428
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordExpirationTimeout(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 433
    :goto_0
    return-wide v2

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 433
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getPasswordHistoryLength(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 387
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 389
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordHistoryLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 394
    :goto_0
    return v1

    .line 390
    :catch_0
    move-exception v0

    .line 391
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 394
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMaximumLength(Landroid/content/ComponentName;)J
    .locals 2
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 466
    iget-object v0, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BasePasswordPolicy.getPasswordMaximumLength"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 474
    const-wide/16 v0, 0x10

    return-wide v0
.end method

.method public getPasswordMinimumLength(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 112
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 114
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 119
    :goto_0
    return v1

    .line 115
    :catch_0
    move-exception v0

    .line 116
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 119
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumLetters(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 231
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 233
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumLetters(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 238
    :goto_0
    return v1

    .line 234
    :catch_0
    move-exception v0

    .line 235
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 238
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumLowerCase(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 191
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 193
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumLowerCase(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 199
    :goto_0
    return v1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 199
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumNonLetter(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 348
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 350
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumNonLetter(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 355
    :goto_0
    return v1

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 355
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumNumeric(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 270
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 272
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumNumeric(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 277
    :goto_0
    return v1

    .line 273
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 277
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumSymbols(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 309
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 311
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumSymbols(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 316
    :goto_0
    return v1

    .line 312
    :catch_0
    move-exception v0

    .line 313
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 316
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordMinimumUpperCase(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 149
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 151
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordMinimumUpperCase(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 157
    :goto_0
    return v1

    .line 153
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 157
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordQuality(Landroid/content/ComponentName;)I
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 76
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordQuality(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 81
    :goto_0
    return v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 81
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isActivePasswordSufficient()Z
    .locals 3

    .prologue
    .line 483
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 485
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isActivePasswordSufficient(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 490
    :goto_0
    return v1

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 490
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetPassword(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 555
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 557
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->resetPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 562
    :goto_0
    return v1

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 562
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "num"    # I

    .prologue
    .line 520
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 522
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumFailedPasswordsForWipe(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 527
    :cond_0
    :goto_0
    return-void

    .line 523
    :catch_0
    move-exception v0

    .line 524
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMaximumTimeToLock(Landroid/content/ComponentName;J)V
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "timeMs"    # J

    .prologue
    .line 571
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 573
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumTimeToLock(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 578
    :cond_0
    :goto_0
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 575
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordExpirationTimeout(Landroid/content/ComponentName;J)V
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "timeout"    # J

    .prologue
    .line 407
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 409
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordExpirationTimeout(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 410
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordHistoryLength(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 368
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 370
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordHistoryLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 371
    :catch_0
    move-exception v0

    .line 372
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLength(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 93
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLetters(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 212
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 214
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLetters(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 216
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLowerCase(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 171
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 173
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLowerCase(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumNonLetter(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 329
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 331
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumNonLetter(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :cond_0
    :goto_0
    return-void

    .line 332
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumNumeric(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 251
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 253
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumNumeric(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumSymbols(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 290
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 292
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumSymbols(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 294
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumUpperCase(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 132
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 134
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumUpperCase(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 136
    :catch_0
    move-exception v0

    .line 137
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordQuality(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "quality"    # I

    .prologue
    .line 54
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BasePasswordPolicy.setPasswordQuality"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Landroid/app/enterprise/BasePasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 57
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BasePasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BasePasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordQuality(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BasePasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
