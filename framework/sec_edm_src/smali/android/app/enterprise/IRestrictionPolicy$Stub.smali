.class public abstract Landroid/app/enterprise/IRestrictionPolicy$Stub;
.super Landroid/os/Binder;
.source "IRestrictionPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IRestrictionPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IRestrictionPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IRestrictionPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IRestrictionPolicy"

.field static final TRANSACTION_addNewAdminActivationAppWhiteList:I = 0x87

.field static final TRANSACTION_allowActivationLock:I = 0x75

.field static final TRANSACTION_allowAirplaneMode:I = 0x6e

.field static final TRANSACTION_allowAndroidBeam:I = 0x57

.field static final TRANSACTION_allowAudioRecord:I = 0x41

.field static final TRANSACTION_allowBackgroundProcessLimit:I = 0x49

.field static final TRANSACTION_allowClipboardShare:I = 0x4f

.field static final TRANSACTION_allowDeveloperMode:I = 0x6c

.field static final TRANSACTION_allowFactoryReset:I = 0x25

.field static final TRANSACTION_allowFastEncryption:I = 0x7b

.field static final TRANSACTION_allowFirmwareAutoUpdate:I = 0x73

.field static final TRANSACTION_allowFirmwareRecovery:I = 0x69

.field static final TRANSACTION_allowGoogleAccountsAutoSync:I = 0x70

.field static final TRANSACTION_allowGoogleCrashReport:I = 0x38

.field static final TRANSACTION_allowKillingActivitiesOnLeave:I = 0x4b

.field static final TRANSACTION_allowLockScreenView:I = 0x65

.field static final TRANSACTION_allowOTAUpgrade:I = 0x34

.field static final TRANSACTION_allowPowerOff:I = 0x3f

.field static final TRANSACTION_allowSBeam:I = 0x55

.field static final TRANSACTION_allowSDCardMove:I = 0x79

.field static final TRANSACTION_allowSDCardWrite:I = 0x36

.field static final TRANSACTION_allowSVoice:I = 0x52

.field static final TRANSACTION_allowSafeMode:I = 0x63

.field static final TRANSACTION_allowSettingsChanges:I = 0x2d

.field static final TRANSACTION_allowShareList:I = 0x5b

.field static final TRANSACTION_allowSmartClipMode:I = 0x8b

.field static final TRANSACTION_allowStatusBarExpansion:I = 0x3d

.field static final TRANSACTION_allowStopSystemApp:I = 0x45

.field static final TRANSACTION_allowUsbHostStorage:I = 0x59

.field static final TRANSACTION_allowUserMobileDataLimit:I = 0x4d

.field static final TRANSACTION_allowVideoRecord:I = 0x43

.field static final TRANSACTION_allowVpn:I = 0x32

.field static final TRANSACTION_allowWallpaperChange:I = 0x3b

.field static final TRANSACTION_allowWifiDirect:I = 0x47

.field static final TRANSACTION_clearNewAdminActivationAppWhiteList:I = 0x86

.field static final TRANSACTION_enableODETrustedBootVerification:I = 0x80

.field static final TRANSACTION_getNewAdminActivationAppWhiteList:I = 0x88

.field static final TRANSACTION_isActivationLockAllowed:I = 0x76

.field static final TRANSACTION_isAirplaneModeAllowed:I = 0x6f

.field static final TRANSACTION_isAndroidBeamAllowed:I = 0x58

.field static final TRANSACTION_isAudioRecordAllowed:I = 0x42

.field static final TRANSACTION_isBackgroundDataEnabled:I = 0x2a

.field static final TRANSACTION_isBackgroundProcessLimitAllowed:I = 0x4a

.field static final TRANSACTION_isBackupAllowed:I = 0x21

.field static final TRANSACTION_isBluetoothTetheringEnabled:I = 0xc

.field static final TRANSACTION_isCCModeEnabled:I = 0x7e

.field static final TRANSACTION_isCCModeSupported:I = 0x7f

.field static final TRANSACTION_isCameraEnabled:I = 0x2

.field static final TRANSACTION_isCellularDataAllowed:I = 0x2c

.field static final TRANSACTION_isClipboardAllowed:I = 0x23

.field static final TRANSACTION_isClipboardAllowedAsUser:I = 0x24

.field static final TRANSACTION_isClipboardShareAllowed:I = 0x50

.field static final TRANSACTION_isClipboardShareAllowedAsUser:I = 0x51

.field static final TRANSACTION_isDeveloperModeAllowed:I = 0x6d

.field static final TRANSACTION_isFactoryResetAllowed:I = 0x26

.field static final TRANSACTION_isFastEncryptionAllowed:I = 0x7c

.field static final TRANSACTION_isFirmwareAutoUpdateAllowed:I = 0x74

.field static final TRANSACTION_isFirmwareRecoveryAllowed:I = 0x6a

.field static final TRANSACTION_isGoogleAccountsAutoSyncAllowed:I = 0x71

.field static final TRANSACTION_isGoogleAccountsAutoSyncAllowedAsUser:I = 0x72

.field static final TRANSACTION_isGoogleCrashReportAllowed:I = 0x39

.field static final TRANSACTION_isGoogleCrashReportAllowedAsUser:I = 0x3a

.field static final TRANSACTION_isHeadphoneEnabled:I = 0x78

.field static final TRANSACTION_isHomeKeyEnabled:I = 0x28

.field static final TRANSACTION_isKillingActivitiesOnLeaveAllowed:I = 0x4c

.field static final TRANSACTION_isLockScreenEnabled:I = 0x68

.field static final TRANSACTION_isLockScreenViewAllowed:I = 0x66

.field static final TRANSACTION_isMicrophoneEnabled:I = 0x4

.field static final TRANSACTION_isMicrophoneEnabledAsUser:I = 0x5

.field static final TRANSACTION_isMockLocationEnabled:I = 0x1f

.field static final TRANSACTION_isNFCEnabled:I = 0x7

.field static final TRANSACTION_isNFCEnabledWithMsg:I = 0x8

.field static final TRANSACTION_isNewAdminActivationEnabled:I = 0x85

.field static final TRANSACTION_isNewAdminInstallationEnabled:I = 0x83

.field static final TRANSACTION_isNonMarketAppAllowed:I = 0x31

.field static final TRANSACTION_isNonTrustedAppInstallBlocked:I = 0x61

.field static final TRANSACTION_isNonTrustedAppInstallBlockedAsUser:I = 0x62

.field static final TRANSACTION_isODETrustedBootVerificationEnabled:I = 0x81

.field static final TRANSACTION_isOTAUpgradeAllowed:I = 0x35

.field static final TRANSACTION_isPowerOffAllowed:I = 0x40

.field static final TRANSACTION_isSBeamAllowed:I = 0x56

.field static final TRANSACTION_isSDCardMoveAllowed:I = 0x7a

.field static final TRANSACTION_isSDCardWriteAllowed:I = 0x37

.field static final TRANSACTION_isSVoiceAllowed:I = 0x53

.field static final TRANSACTION_isSVoiceAllowedAsUser:I = 0x54

.field static final TRANSACTION_isSafeModeAllowed:I = 0x64

.field static final TRANSACTION_isScreenCaptureEnabled:I = 0x1c

.field static final TRANSACTION_isScreenCaptureEnabledInternal:I = 0x1d

.field static final TRANSACTION_isSdCardEnabled:I = 0xa

.field static final TRANSACTION_isSettingsChangesAllowed:I = 0x2e

.field static final TRANSACTION_isSettingsChangesAllowedAsUser:I = 0x2f

.field static final TRANSACTION_isShareListAllowed:I = 0x5c

.field static final TRANSACTION_isShareListAllowedAsUser:I = 0x5d

.field static final TRANSACTION_isSmartClipModeAllowed:I = 0x89

.field static final TRANSACTION_isSmartClipModeAllowedInternal:I = 0x8a

.field static final TRANSACTION_isStatusBarExpansionAllowed:I = 0x3e

.field static final TRANSACTION_isStatusBarExpansionAllowedAsUser:I = 0x6b

.field static final TRANSACTION_isStopSystemAppAllowed:I = 0x46

.field static final TRANSACTION_isTetheringEnabled:I = 0x12

.field static final TRANSACTION_isUsbDebuggingEnabled:I = 0x14

.field static final TRANSACTION_isUsbHostStorageAllowed:I = 0x5a

.field static final TRANSACTION_isUsbKiesAvailable:I = 0x18

.field static final TRANSACTION_isUsbMassStorageEnabled:I = 0x16

.field static final TRANSACTION_isUsbMediaPlayerAvailable:I = 0x1a

.field static final TRANSACTION_isUsbTetheringEnabled:I = 0xe

.field static final TRANSACTION_isUseSecureKeypadEnabled:I = 0x5f

.field static final TRANSACTION_isUserMobileDataLimitAllowed:I = 0x4e

.field static final TRANSACTION_isVideoRecordAllowed:I = 0x44

.field static final TRANSACTION_isVpnAllowed:I = 0x33

.field static final TRANSACTION_isWallpaperChangeAllowed:I = 0x3c

.field static final TRANSACTION_isWifiDirectAllowed:I = 0x48

.field static final TRANSACTION_isWifiTetheringEnabled:I = 0x10

.field static final TRANSACTION_preventNewAdminActivation:I = 0x84

.field static final TRANSACTION_preventNewAdminInstallation:I = 0x82

.field static final TRANSACTION_setAllowNonMarketApps:I = 0x30

.field static final TRANSACTION_setBackgroundData:I = 0x29

.field static final TRANSACTION_setBackup:I = 0x20

.field static final TRANSACTION_setBluetoothTethering:I = 0xb

.field static final TRANSACTION_setCCMode:I = 0x7d

.field static final TRANSACTION_setCamera:I = 0x1

.field static final TRANSACTION_setCellularData:I = 0x2b

.field static final TRANSACTION_setClipboardEnabled:I = 0x22

.field static final TRANSACTION_setEnableNFC:I = 0x6

.field static final TRANSACTION_setHeadphoneState:I = 0x77

.field static final TRANSACTION_setHomeKeyState:I = 0x27

.field static final TRANSACTION_setLockScreenState:I = 0x67

.field static final TRANSACTION_setMicrophoneState:I = 0x3

.field static final TRANSACTION_setMockLocation:I = 0x1e

.field static final TRANSACTION_setNonTrustedAppInstallBlock:I = 0x60

.field static final TRANSACTION_setScreenCapture:I = 0x1b

.field static final TRANSACTION_setSdCardState:I = 0x9

.field static final TRANSACTION_setTethering:I = 0x11

.field static final TRANSACTION_setUsbDebuggingEnabled:I = 0x13

.field static final TRANSACTION_setUsbKiesAvailability:I = 0x17

.field static final TRANSACTION_setUsbMassStorage:I = 0x15

.field static final TRANSACTION_setUsbMediaPlayerAvailability:I = 0x19

.field static final TRANSACTION_setUsbTethering:I = 0xd

.field static final TRANSACTION_setUseSecureKeypad:I = 0x5e

.field static final TRANSACTION_setWifiTethering:I = 0xf


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "android.app.enterprise.IRestrictionPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IRestrictionPolicy;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Landroid/app/enterprise/IRestrictionPolicy;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Landroid/app/enterprise/IRestrictionPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IRestrictionPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 2260
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v7, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    .line 50
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 56
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2

    move v1, v6

    .line 57
    .local v1, "_arg1":Z
    :goto_2
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setCamera(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 58
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    if-eqz v4, :cond_0

    move v7, v6

    :cond_0
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    :cond_2
    move v1, v7

    .line 56
    goto :goto_2

    .line 64
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4

    .line 67
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 73
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5

    move v1, v6

    .line 74
    .restart local v1    # "_arg1":Z
    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isCameraEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 75
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    if-eqz v4, :cond_3

    move v7, v6

    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 70
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    :cond_5
    move v1, v7

    .line 73
    goto :goto_4

    .line 81
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7

    .line 84
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 90
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8

    move v1, v6

    .line 91
    .restart local v1    # "_arg1":Z
    :goto_6
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setMicrophoneState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 92
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 93
    if-eqz v4, :cond_6

    move v7, v6

    :cond_6
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 87
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    :cond_8
    move v1, v7

    .line 90
    goto :goto_6

    .line 98
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a

    .line 101
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 107
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b

    move v1, v6

    .line 108
    .restart local v1    # "_arg1":Z
    :goto_8
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isMicrophoneEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 109
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    if-eqz v4, :cond_9

    move v7, v6

    :cond_9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 104
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    :cond_b
    move v1, v7

    .line 107
    goto :goto_8

    .line 115
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d

    move v0, v6

    .line 119
    .local v0, "_arg0":Z
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 120
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isMicrophoneEnabledAsUser(ZI)Z

    move-result v4

    .line 121
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    if-eqz v4, :cond_c

    move v7, v6

    :cond_c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_d
    move v0, v7

    .line 117
    goto :goto_9

    .line 127
    :sswitch_6
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f

    .line 130
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 136
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10

    move v1, v6

    .line 137
    .local v1, "_arg1":Z
    :goto_b
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setEnableNFC(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 138
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 139
    if-eqz v4, :cond_e

    move v7, v6

    :cond_e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 133
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    :cond_10
    move v1, v7

    .line 136
    goto :goto_b

    .line 144
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNFCEnabled()Z

    move-result v4

    .line 146
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 147
    if-eqz v4, :cond_11

    move v7, v6

    :cond_11
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 152
    .end local v4    # "_result":Z
    :sswitch_8
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 154
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13

    move v0, v6

    .line 155
    .local v0, "_arg0":Z
    :goto_c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNFCEnabledWithMsg(Z)Z

    move-result v4

    .line 156
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 157
    if-eqz v4, :cond_12

    move v7, v6

    :cond_12
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":Z
    :cond_13
    move v0, v7

    .line 154
    goto :goto_c

    .line 162
    :sswitch_9
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15

    .line 165
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 171
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_16

    move v1, v6

    .line 172
    .restart local v1    # "_arg1":Z
    :goto_e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setSdCardState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 173
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    if-eqz v4, :cond_14

    move v7, v6

    :cond_14
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 168
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_15
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    :cond_16
    move v1, v7

    .line 171
    goto :goto_e

    .line 179
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_18

    .line 182
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 187
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSdCardEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 188
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 189
    if-eqz v4, :cond_17

    move v7, v6

    :cond_17
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 185
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_18
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 194
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1a

    .line 197
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 203
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1b

    move v1, v6

    .line 204
    .restart local v1    # "_arg1":Z
    :goto_11
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setBluetoothTethering(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 205
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 206
    if-eqz v4, :cond_19

    move v7, v6

    :cond_19
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 200
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_1a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    :cond_1b
    move v1, v7

    .line 203
    goto :goto_11

    .line 211
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 213
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1d

    .line 214
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 219
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isBluetoothTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 220
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 221
    if-eqz v4, :cond_1c

    move v7, v6

    :cond_1c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 217
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 226
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1f

    .line 229
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 235
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_20

    move v1, v6

    .line 236
    .restart local v1    # "_arg1":Z
    :goto_14
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUsbTethering(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 237
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 238
    if-eqz v4, :cond_1e

    move v7, v6

    :cond_1e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 232
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    :cond_20
    move v1, v7

    .line 235
    goto :goto_14

    .line 243
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_22

    .line 246
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 251
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 252
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 253
    if-eqz v4, :cond_21

    move v7, v6

    :cond_21
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 249
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_22
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 258
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_24

    .line 261
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 267
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_25

    move v1, v6

    .line 268
    .restart local v1    # "_arg1":Z
    :goto_17
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setWifiTethering(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 269
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 270
    if-eqz v4, :cond_23

    move v7, v6

    :cond_23
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 264
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_24
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    :cond_25
    move v1, v7

    .line 267
    goto :goto_17

    .line 275
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_27

    .line 278
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 283
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isWifiTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 284
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 285
    if-eqz v4, :cond_26

    move v7, v6

    :cond_26
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 281
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_27
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 290
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_29

    .line 293
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 299
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2a

    move v1, v6

    .line 300
    .restart local v1    # "_arg1":Z
    :goto_1a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setTethering(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 301
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 302
    if-eqz v4, :cond_28

    move v7, v6

    :cond_28
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 296
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_29
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    :cond_2a
    move v1, v7

    .line 299
    goto :goto_1a

    .line 307
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2c

    .line 310
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 315
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 316
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    if-eqz v4, :cond_2b

    move v7, v6

    :cond_2b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 313
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_2c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 322
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 324
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2e

    .line 325
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 331
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2f

    move v1, v6

    .line 332
    .restart local v1    # "_arg1":Z
    :goto_1d
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUsbDebuggingEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 333
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 334
    if-eqz v4, :cond_2d

    move v7, v6

    :cond_2d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 328
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_2e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    :cond_2f
    move v1, v7

    .line 331
    goto :goto_1d

    .line 339
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_31

    .line 342
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 347
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbDebuggingEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 348
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 349
    if-eqz v4, :cond_30

    move v7, v6

    :cond_30
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 345
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_31
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 354
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 356
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_33

    .line 357
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 363
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_34

    move v1, v6

    .line 364
    .restart local v1    # "_arg1":Z
    :goto_20
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUsbMassStorage(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 365
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 366
    if-eqz v4, :cond_32

    move v7, v6

    :cond_32
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 360
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_33
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    :cond_34
    move v1, v7

    .line 363
    goto :goto_20

    .line 371
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 373
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_36

    .line 374
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 380
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_37

    move v1, v6

    .line 381
    .restart local v1    # "_arg1":Z
    :goto_22
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbMassStorageEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 382
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 383
    if-eqz v4, :cond_35

    move v7, v6

    :cond_35
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 377
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    :cond_37
    move v1, v7

    .line 380
    goto :goto_22

    .line 388
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_39

    .line 391
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 397
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3a

    move v1, v6

    .line 398
    .restart local v1    # "_arg1":Z
    :goto_24
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUsbKiesAvailability(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 399
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 400
    if-eqz v4, :cond_38

    move v7, v6

    :cond_38
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 394
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_39
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_23

    :cond_3a
    move v1, v7

    .line 397
    goto :goto_24

    .line 405
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 407
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3c

    .line 408
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 414
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3d

    move v1, v6

    .line 415
    .restart local v1    # "_arg1":Z
    :goto_26
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbKiesAvailable(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 416
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 417
    if-eqz v4, :cond_3b

    move v7, v6

    :cond_3b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 411
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_3c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    :cond_3d
    move v1, v7

    .line 414
    goto :goto_26

    .line 422
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 424
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3f

    .line 425
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 431
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_40

    move v1, v6

    .line 432
    .restart local v1    # "_arg1":Z
    :goto_28
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUsbMediaPlayerAvailability(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 433
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 434
    if-eqz v4, :cond_3e

    move v7, v6

    :cond_3e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 428
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_3f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    :cond_40
    move v1, v7

    .line 431
    goto :goto_28

    .line 439
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 441
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_42

    .line 442
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 448
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_43

    move v1, v6

    .line 449
    .restart local v1    # "_arg1":Z
    :goto_2a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbMediaPlayerAvailable(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 450
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 451
    if-eqz v4, :cond_41

    move v7, v6

    :cond_41
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 445
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_42
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    :cond_43
    move v1, v7

    .line 448
    goto :goto_2a

    .line 456
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 458
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_45

    .line 459
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 465
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_46

    move v1, v6

    .line 466
    .restart local v1    # "_arg1":Z
    :goto_2c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setScreenCapture(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 467
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 468
    if-eqz v4, :cond_44

    move v7, v6

    :cond_44
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 462
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_45
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    :cond_46
    move v1, v7

    .line 465
    goto :goto_2c

    .line 473
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 475
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_48

    .line 476
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 482
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_49

    move v1, v6

    .line 483
    .restart local v1    # "_arg1":Z
    :goto_2e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isScreenCaptureEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 484
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 485
    if-eqz v4, :cond_47

    move v7, v6

    :cond_47
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 479
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_48
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    :cond_49
    move v1, v7

    .line 482
    goto :goto_2e

    .line 490
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 492
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4b

    move v0, v6

    .line 493
    .local v0, "_arg0":Z
    :goto_2f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isScreenCaptureEnabledInternal(Z)Z

    move-result v4

    .line 494
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 495
    if-eqz v4, :cond_4a

    move v7, v6

    :cond_4a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":Z
    :cond_4b
    move v0, v7

    .line 492
    goto :goto_2f

    .line 500
    :sswitch_1e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4d

    .line 503
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 509
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4e

    move v1, v6

    .line 510
    .restart local v1    # "_arg1":Z
    :goto_31
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setMockLocation(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 511
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 512
    if-eqz v4, :cond_4c

    move v7, v6

    :cond_4c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 506
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_4d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    :cond_4e
    move v1, v7

    .line 509
    goto :goto_31

    .line 517
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_50

    .line 520
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 525
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isMockLocationEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 526
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 527
    if-eqz v4, :cond_4f

    move v7, v6

    :cond_4f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 523
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_50
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 532
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 534
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_52

    .line 535
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 541
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_33
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_53

    move v1, v6

    .line 542
    .restart local v1    # "_arg1":Z
    :goto_34
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setBackup(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 543
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 544
    if-eqz v4, :cond_51

    move v7, v6

    :cond_51
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 538
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_52
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_33

    :cond_53
    move v1, v7

    .line 541
    goto :goto_34

    .line 549
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 551
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_55

    .line 552
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 558
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_56

    move v1, v6

    .line 559
    .restart local v1    # "_arg1":Z
    :goto_36
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isBackupAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 560
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 561
    if-eqz v4, :cond_54

    move v7, v6

    :cond_54
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 555
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_55
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    :cond_56
    move v1, v7

    .line 558
    goto :goto_36

    .line 566
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 568
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_58

    .line 569
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 575
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_59

    move v1, v6

    .line 576
    .restart local v1    # "_arg1":Z
    :goto_38
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setClipboardEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 577
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 578
    if-eqz v4, :cond_57

    move v7, v6

    :cond_57
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 572
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_58
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    :cond_59
    move v1, v7

    .line 575
    goto :goto_38

    .line 583
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 585
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5b

    .line 586
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 592
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5c

    move v1, v6

    .line 593
    .restart local v1    # "_arg1":Z
    :goto_3a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isClipboardAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 594
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 595
    if-eqz v4, :cond_5a

    move v7, v6

    :cond_5a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 589
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_5b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    :cond_5c
    move v1, v7

    .line 592
    goto :goto_3a

    .line 600
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_24
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 602
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5e

    move v0, v6

    .line 604
    .local v0, "_arg0":Z
    :goto_3b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 605
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isClipboardAllowedAsUser(ZI)Z

    move-result v4

    .line 606
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 607
    if-eqz v4, :cond_5d

    move v7, v6

    :cond_5d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_5e
    move v0, v7

    .line 602
    goto :goto_3b

    .line 612
    :sswitch_25
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_60

    .line 615
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 621
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_61

    move v1, v6

    .line 622
    .local v1, "_arg1":Z
    :goto_3d
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowFactoryReset(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 623
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 624
    if-eqz v4, :cond_5f

    move v7, v6

    :cond_5f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 618
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_60
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3c

    :cond_61
    move v1, v7

    .line 621
    goto :goto_3d

    .line 629
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_26
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_63

    .line 632
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 637
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isFactoryResetAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 638
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 639
    if-eqz v4, :cond_62

    move v7, v6

    :cond_62
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 635
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_63
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 644
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 646
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_65

    .line 647
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 653
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_66

    move v1, v6

    .line 654
    .restart local v1    # "_arg1":Z
    :goto_40
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setHomeKeyState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 655
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 656
    if-eqz v4, :cond_64

    move v7, v6

    :cond_64
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 650
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_65
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    :cond_66
    move v1, v7

    .line 653
    goto :goto_40

    .line 661
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_28
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 663
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_68

    .line 664
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 670
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_69

    move v1, v6

    .line 671
    .restart local v1    # "_arg1":Z
    :goto_42
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isHomeKeyEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 672
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 673
    if-eqz v4, :cond_67

    move v7, v6

    :cond_67
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 667
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_68
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    :cond_69
    move v1, v7

    .line 670
    goto :goto_42

    .line 678
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_29
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 680
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_6b

    .line 681
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 687
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_43
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_6c

    move v1, v6

    .line 688
    .restart local v1    # "_arg1":Z
    :goto_44
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setBackgroundData(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 689
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 690
    if-eqz v4, :cond_6a

    move v7, v6

    :cond_6a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 684
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_6b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_43

    :cond_6c
    move v1, v7

    .line 687
    goto :goto_44

    .line 695
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 697
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_6e

    .line 698
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 703
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_45
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isBackgroundDataEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 704
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 705
    if-eqz v4, :cond_6d

    move v7, v6

    :cond_6d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 701
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_6e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_45

    .line 710
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 712
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_70

    .line 713
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 719
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_46
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_71

    move v1, v6

    .line 720
    .restart local v1    # "_arg1":Z
    :goto_47
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setCellularData(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 721
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 722
    if-eqz v4, :cond_6f

    move v7, v6

    :cond_6f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 716
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_70
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_46

    :cond_71
    move v1, v7

    .line 719
    goto :goto_47

    .line 727
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 729
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_73

    .line 730
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 735
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_48
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isCellularDataAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 736
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 737
    if-eqz v4, :cond_72

    move v7, v6

    :cond_72
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 733
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_73
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_48

    .line 742
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 744
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_75

    .line 745
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 751
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_76

    move v1, v6

    .line 752
    .restart local v1    # "_arg1":Z
    :goto_4a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSettingsChanges(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 753
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 754
    if-eqz v4, :cond_74

    move v7, v6

    :cond_74
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 748
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_75
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_49

    :cond_76
    move v1, v7

    .line 751
    goto :goto_4a

    .line 759
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 761
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_78

    .line 762
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 768
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_79

    move v1, v6

    .line 769
    .restart local v1    # "_arg1":Z
    :goto_4c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSettingsChangesAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 770
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 771
    if-eqz v4, :cond_77

    move v7, v6

    :cond_77
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 765
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_78
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4b

    :cond_79
    move v1, v7

    .line 768
    goto :goto_4c

    .line 776
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 778
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7b

    move v0, v6

    .line 780
    .local v0, "_arg0":Z
    :goto_4d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 781
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSettingsChangesAllowedAsUser(ZI)Z

    move-result v4

    .line 782
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 783
    if-eqz v4, :cond_7a

    move v7, v6

    :cond_7a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_7b
    move v0, v7

    .line 778
    goto :goto_4d

    .line 788
    :sswitch_30
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 790
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7d

    .line 791
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 797
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7e

    move v1, v6

    .line 798
    .local v1, "_arg1":Z
    :goto_4f
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setAllowNonMarketApps(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 799
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 800
    if-eqz v4, :cond_7c

    move v7, v6

    :cond_7c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 794
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_7d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4e

    :cond_7e
    move v1, v7

    .line 797
    goto :goto_4f

    .line 805
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_31
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 807
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_80

    .line 808
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 813
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_50
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNonMarketAppAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 814
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 815
    if-eqz v4, :cond_7f

    move v7, v6

    :cond_7f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 811
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_80
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_50

    .line 820
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_32
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 822
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_82

    .line 823
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 829
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_83

    move v1, v6

    .line 830
    .restart local v1    # "_arg1":Z
    :goto_52
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowVpn(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 831
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 832
    if-eqz v4, :cond_81

    move v7, v6

    :cond_81
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 826
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_82
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_51

    :cond_83
    move v1, v7

    .line 829
    goto :goto_52

    .line 837
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_33
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 839
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_85

    .line 840
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 845
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_53
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isVpnAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 846
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 847
    if-eqz v4, :cond_84

    move v7, v6

    :cond_84
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 843
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_85
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_53

    .line 852
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_34
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 854
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_87

    .line 855
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 861
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_88

    move v1, v6

    .line 862
    .restart local v1    # "_arg1":Z
    :goto_55
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowOTAUpgrade(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 863
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 864
    if-eqz v4, :cond_86

    move v7, v6

    :cond_86
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 858
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_87
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_54

    :cond_88
    move v1, v7

    .line 861
    goto :goto_55

    .line 869
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_35
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 871
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8a

    .line 872
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 877
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_56
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isOTAUpgradeAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 878
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 879
    if-eqz v4, :cond_89

    move v7, v6

    :cond_89
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 875
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_8a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_56

    .line 884
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_36
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 886
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8c

    .line 887
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 893
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8d

    move v1, v6

    .line 894
    .restart local v1    # "_arg1":Z
    :goto_58
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSDCardWrite(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 895
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 896
    if-eqz v4, :cond_8b

    move v7, v6

    :cond_8b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 890
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_8c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_57

    :cond_8d
    move v1, v7

    .line 893
    goto :goto_58

    .line 901
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_37
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 903
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8f

    .line 904
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 909
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_59
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSDCardWriteAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 910
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 911
    if-eqz v4, :cond_8e

    move v7, v6

    :cond_8e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 907
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_8f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_59

    .line 916
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_38
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 918
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_91

    .line 919
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 925
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_92

    move v1, v6

    .line 926
    .restart local v1    # "_arg1":Z
    :goto_5b
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowGoogleCrashReport(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 927
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 928
    if-eqz v4, :cond_90

    move v7, v6

    :cond_90
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 922
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_91
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5a

    :cond_92
    move v1, v7

    .line 925
    goto :goto_5b

    .line 933
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_39
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 935
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_94

    .line 936
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 941
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isGoogleCrashReportAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 942
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 943
    if-eqz v4, :cond_93

    move v7, v6

    :cond_93
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 939
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_94
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5c

    .line 948
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 950
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 951
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isGoogleCrashReportAllowedAsUser(I)Z

    move-result v4

    .line 952
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 953
    if-eqz v4, :cond_95

    move v7, v6

    :cond_95
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 958
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Z
    :sswitch_3b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 960
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_97

    .line 961
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 967
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_98

    move v1, v6

    .line 968
    .restart local v1    # "_arg1":Z
    :goto_5e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowWallpaperChange(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 969
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 970
    if-eqz v4, :cond_96

    move v7, v6

    :cond_96
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 964
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_97
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5d

    :cond_98
    move v1, v7

    .line 967
    goto :goto_5e

    .line 975
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 977
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9a

    .line 978
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 984
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9b

    move v1, v6

    .line 985
    .restart local v1    # "_arg1":Z
    :goto_60
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isWallpaperChangeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 986
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 987
    if-eqz v4, :cond_99

    move v7, v6

    :cond_99
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 981
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_9a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5f

    :cond_9b
    move v1, v7

    .line 984
    goto :goto_60

    .line 992
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 994
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9d

    .line 995
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1001
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_61
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9e

    move v1, v6

    .line 1002
    .restart local v1    # "_arg1":Z
    :goto_62
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowStatusBarExpansion(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1003
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1004
    if-eqz v4, :cond_9c

    move v7, v6

    :cond_9c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 998
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_9d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_61

    :cond_9e
    move v1, v7

    .line 1001
    goto :goto_62

    .line 1009
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1011
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a0

    .line 1012
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1018
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_63
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a1

    move v1, v6

    .line 1019
    .restart local v1    # "_arg1":Z
    :goto_64
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isStatusBarExpansionAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1020
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1021
    if-eqz v4, :cond_9f

    move v7, v6

    :cond_9f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1015
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_63

    :cond_a1
    move v1, v7

    .line 1018
    goto :goto_64

    .line 1026
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1028
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a3

    .line 1029
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1035
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a4

    move v1, v6

    .line 1036
    .restart local v1    # "_arg1":Z
    :goto_66
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowPowerOff(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1037
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1038
    if-eqz v4, :cond_a2

    move v7, v6

    :cond_a2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1032
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_65

    :cond_a4
    move v1, v7

    .line 1035
    goto :goto_66

    .line 1043
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_40
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1045
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a6

    .line 1046
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1052
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a7

    move v1, v6

    .line 1053
    .restart local v1    # "_arg1":Z
    :goto_68
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isPowerOffAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1054
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1055
    if-eqz v4, :cond_a5

    move v7, v6

    :cond_a5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1049
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_67

    :cond_a7
    move v1, v7

    .line 1052
    goto :goto_68

    .line 1060
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_41
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1062
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a9

    .line 1063
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1069
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_aa

    move v1, v6

    .line 1070
    .restart local v1    # "_arg1":Z
    :goto_6a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowAudioRecord(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1071
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1072
    if-eqz v4, :cond_a8

    move v7, v6

    :cond_a8
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1066
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_69

    :cond_aa
    move v1, v7

    .line 1069
    goto :goto_6a

    .line 1077
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_42
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1079
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ac

    .line 1080
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1086
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ad

    move v1, v6

    .line 1087
    .restart local v1    # "_arg1":Z
    :goto_6c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isAudioRecordAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1088
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1089
    if-eqz v4, :cond_ab

    move v7, v6

    :cond_ab
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1083
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_ac
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6b

    :cond_ad
    move v1, v7

    .line 1086
    goto :goto_6c

    .line 1094
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_43
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1096
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_af

    .line 1097
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1103
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b0

    move v1, v6

    .line 1104
    .restart local v1    # "_arg1":Z
    :goto_6e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowVideoRecord(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1105
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1106
    if-eqz v4, :cond_ae

    move v7, v6

    :cond_ae
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1100
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_af
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6d

    :cond_b0
    move v1, v7

    .line 1103
    goto :goto_6e

    .line 1111
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_44
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b2

    .line 1114
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1120
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b3

    move v1, v6

    .line 1121
    .restart local v1    # "_arg1":Z
    :goto_70
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isVideoRecordAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1122
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1123
    if-eqz v4, :cond_b1

    move v7, v6

    :cond_b1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1117
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_b2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6f

    :cond_b3
    move v1, v7

    .line 1120
    goto :goto_70

    .line 1128
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_45
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b5

    .line 1131
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1137
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b6

    move v1, v6

    .line 1138
    .restart local v1    # "_arg1":Z
    :goto_72
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowStopSystemApp(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1139
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1140
    if-eqz v4, :cond_b4

    move v7, v6

    :cond_b4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1134
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_b5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_71

    :cond_b6
    move v1, v7

    .line 1137
    goto :goto_72

    .line 1145
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_46
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1147
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b8

    .line 1148
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1153
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_73
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isStopSystemAppAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1154
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1155
    if-eqz v4, :cond_b7

    move v7, v6

    :cond_b7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1151
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_b8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_73

    .line 1160
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_47
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ba

    .line 1163
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1169
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_74
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_bb

    move v1, v6

    .line 1170
    .restart local v1    # "_arg1":Z
    :goto_75
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowWifiDirect(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1171
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1172
    if-eqz v4, :cond_b9

    move v7, v6

    :cond_b9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1166
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_ba
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_74

    :cond_bb
    move v1, v7

    .line 1169
    goto :goto_75

    .line 1177
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_48
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_bd

    .line 1180
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1186
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_be

    move v1, v6

    .line 1187
    .restart local v1    # "_arg1":Z
    :goto_77
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isWifiDirectAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1188
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1189
    if-eqz v4, :cond_bc

    move v7, v6

    :cond_bc
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1183
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_bd
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_76

    :cond_be
    move v1, v7

    .line 1186
    goto :goto_77

    .line 1194
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_49
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1196
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c0

    .line 1197
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1203
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c1

    move v1, v6

    .line 1204
    .restart local v1    # "_arg1":Z
    :goto_79
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowBackgroundProcessLimit(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1205
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1206
    if-eqz v4, :cond_bf

    move v7, v6

    :cond_bf
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1200
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_c0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_78

    :cond_c1
    move v1, v7

    .line 1203
    goto :goto_79

    .line 1211
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1213
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c3

    .line 1214
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1219
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isBackgroundProcessLimitAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1220
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1221
    if-eqz v4, :cond_c2

    move v7, v6

    :cond_c2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1217
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_c3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7a

    .line 1226
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1228
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c5

    .line 1229
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1235
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c6

    move v1, v6

    .line 1236
    .restart local v1    # "_arg1":Z
    :goto_7c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowKillingActivitiesOnLeave(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1237
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1238
    if-eqz v4, :cond_c4

    move v7, v6

    :cond_c4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1232
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_c5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7b

    :cond_c6
    move v1, v7

    .line 1235
    goto :goto_7c

    .line 1243
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1245
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c8

    .line 1246
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1251
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isKillingActivitiesOnLeaveAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1252
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1253
    if-eqz v4, :cond_c7

    move v7, v6

    :cond_c7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1249
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_c8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7d

    .line 1258
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1260
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ca

    .line 1261
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1267
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_cb

    move v1, v6

    .line 1268
    .restart local v1    # "_arg1":Z
    :goto_7f
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowUserMobileDataLimit(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1269
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1270
    if-eqz v4, :cond_c9

    move v7, v6

    :cond_c9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1264
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_ca
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7e

    :cond_cb
    move v1, v7

    .line 1267
    goto :goto_7f

    .line 1275
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_cd

    .line 1278
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1283
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_80
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUserMobileDataLimitAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1284
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1285
    if-eqz v4, :cond_cc

    move v7, v6

    :cond_cc
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1281
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_cd
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_80

    .line 1290
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1292
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_cf

    .line 1293
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1299
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d0

    move v1, v6

    .line 1300
    .restart local v1    # "_arg1":Z
    :goto_82
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowClipboardShare(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1301
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1302
    if-eqz v4, :cond_ce

    move v7, v6

    :cond_ce
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1296
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_cf
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_81

    :cond_d0
    move v1, v7

    .line 1299
    goto :goto_82

    .line 1307
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_50
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1309
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d2

    .line 1310
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1315
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_83
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isClipboardShareAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1316
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1317
    if-eqz v4, :cond_d1

    move v7, v6

    :cond_d1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1313
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_d2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_83

    .line 1322
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_51
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1324
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1325
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isClipboardShareAllowedAsUser(I)Z

    move-result v4

    .line 1326
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1327
    if-eqz v4, :cond_d3

    move v7, v6

    :cond_d3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1332
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Z
    :sswitch_52
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d5

    .line 1335
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1341
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d6

    move v1, v6

    .line 1342
    .restart local v1    # "_arg1":Z
    :goto_85
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSVoice(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1343
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1344
    if-eqz v4, :cond_d4

    move v7, v6

    :cond_d4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1338
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_d5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_84

    :cond_d6
    move v1, v7

    .line 1341
    goto :goto_85

    .line 1349
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_53
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1351
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d8

    .line 1352
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1358
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d9

    move v1, v6

    .line 1359
    .restart local v1    # "_arg1":Z
    :goto_87
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSVoiceAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1360
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1361
    if-eqz v4, :cond_d7

    move v7, v6

    :cond_d7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1355
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_d8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_86

    :cond_d9
    move v1, v7

    .line 1358
    goto :goto_87

    .line 1366
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_54
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1368
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_db

    move v0, v6

    .line 1370
    .local v0, "_arg0":Z
    :goto_88
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1371
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSVoiceAllowedAsUser(ZI)Z

    move-result v4

    .line 1372
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1373
    if-eqz v4, :cond_da

    move v7, v6

    :cond_da
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_db
    move v0, v7

    .line 1368
    goto :goto_88

    .line 1378
    :sswitch_55
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1380
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_dd

    .line 1381
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1387
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_de

    move v1, v6

    .line 1388
    .local v1, "_arg1":Z
    :goto_8a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSBeam(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1389
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1390
    if-eqz v4, :cond_dc

    move v7, v6

    :cond_dc
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1384
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_dd
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_89

    :cond_de
    move v1, v7

    .line 1387
    goto :goto_8a

    .line 1395
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_56
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1397
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e0

    .line 1398
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1404
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e1

    move v1, v6

    .line 1405
    .restart local v1    # "_arg1":Z
    :goto_8c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSBeamAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1406
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1407
    if-eqz v4, :cond_df

    move v7, v6

    :cond_df
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1401
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_e0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8b

    :cond_e1
    move v1, v7

    .line 1404
    goto :goto_8c

    .line 1412
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_57
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1414
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e3

    .line 1415
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1421
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e4

    move v1, v6

    .line 1422
    .restart local v1    # "_arg1":Z
    :goto_8e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowAndroidBeam(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1423
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1424
    if-eqz v4, :cond_e2

    move v7, v6

    :cond_e2
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1418
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_e3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8d

    :cond_e4
    move v1, v7

    .line 1421
    goto :goto_8e

    .line 1429
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_58
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1431
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e6

    .line 1432
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1438
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e7

    move v1, v6

    .line 1439
    .restart local v1    # "_arg1":Z
    :goto_90
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isAndroidBeamAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1440
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1441
    if-eqz v4, :cond_e5

    move v7, v6

    :cond_e5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1435
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_e6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8f

    :cond_e7
    move v1, v7

    .line 1438
    goto :goto_90

    .line 1446
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_59
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1448
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e9

    .line 1449
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1455
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ea

    move v1, v6

    .line 1456
    .restart local v1    # "_arg1":Z
    :goto_92
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowUsbHostStorage(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1457
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1458
    if-eqz v4, :cond_e8

    move v7, v6

    :cond_e8
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1452
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_e9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_91

    :cond_ea
    move v1, v7

    .line 1455
    goto :goto_92

    .line 1463
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1465
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ec

    .line 1466
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1472
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ed

    move v1, v6

    .line 1473
    .restart local v1    # "_arg1":Z
    :goto_94
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUsbHostStorageAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1474
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1475
    if-eqz v4, :cond_eb

    move v7, v6

    :cond_eb
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1469
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_ec
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_93

    :cond_ed
    move v1, v7

    .line 1472
    goto :goto_94

    .line 1480
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1482
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ef

    .line 1483
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1489
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_95
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f0

    move v1, v6

    .line 1490
    .restart local v1    # "_arg1":Z
    :goto_96
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowShareList(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1491
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1492
    if-eqz v4, :cond_ee

    move v7, v6

    :cond_ee
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1486
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_ef
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_95

    :cond_f0
    move v1, v7

    .line 1489
    goto :goto_96

    .line 1497
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1499
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f2

    .line 1500
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1506
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f3

    move v1, v6

    .line 1507
    .restart local v1    # "_arg1":Z
    :goto_98
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isShareListAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1508
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1509
    if-eqz v4, :cond_f1

    move v7, v6

    :cond_f1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1503
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_f2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_97

    :cond_f3
    move v1, v7

    .line 1506
    goto :goto_98

    .line 1514
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1516
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1518
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f5

    move v1, v6

    .line 1519
    .restart local v1    # "_arg1":Z
    :goto_99
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isShareListAllowedAsUser(IZ)Z

    move-result v4

    .line 1520
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1521
    if-eqz v4, :cond_f4

    move v7, v6

    :cond_f4
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_f5
    move v1, v7

    .line 1518
    goto :goto_99

    .line 1526
    .end local v0    # "_arg0":I
    :sswitch_5e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1528
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f7

    .line 1529
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1535
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f8

    move v1, v6

    .line 1536
    .restart local v1    # "_arg1":Z
    :goto_9b
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setUseSecureKeypad(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1537
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1538
    if-eqz v4, :cond_f6

    move v7, v6

    :cond_f6
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1532
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_f7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9a

    :cond_f8
    move v1, v7

    .line 1535
    goto :goto_9b

    .line 1543
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1545
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_fa

    .line 1546
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1551
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isUseSecureKeypadEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1552
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1553
    if-eqz v4, :cond_f9

    move v7, v6

    :cond_f9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1549
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_fa
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9c

    .line 1558
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_60
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1560
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_fc

    .line 1561
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1567
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_fd

    move v1, v6

    .line 1568
    .restart local v1    # "_arg1":Z
    :goto_9e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setNonTrustedAppInstallBlock(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1569
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1570
    if-eqz v4, :cond_fb

    move v7, v6

    :cond_fb
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1564
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_fc
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9d

    :cond_fd
    move v1, v7

    .line 1567
    goto :goto_9e

    .line 1575
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_61
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1577
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_ff

    .line 1578
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1583
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNonTrustedAppInstallBlocked(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1584
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1585
    if-eqz v4, :cond_fe

    move v7, v6

    :cond_fe
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1581
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_ff
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9f

    .line 1590
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_62
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1592
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1593
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNonTrustedAppInstallBlockedAsUser(I)Z

    move-result v4

    .line 1594
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1595
    if-eqz v4, :cond_100

    move v7, v6

    :cond_100
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1600
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Z
    :sswitch_63
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1602
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_102

    .line 1603
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1609
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_103

    move v1, v6

    .line 1610
    .restart local v1    # "_arg1":Z
    :goto_a1
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSafeMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1611
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1612
    if-eqz v4, :cond_101

    move v7, v6

    :cond_101
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1606
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_102
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a0

    :cond_103
    move v1, v7

    .line 1609
    goto :goto_a1

    .line 1617
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_64
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1619
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_105

    .line 1620
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1625
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSafeModeAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1626
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1627
    if-eqz v4, :cond_104

    move v7, v6

    :cond_104
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1623
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_105
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a2

    .line 1632
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_65
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1634
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_107

    .line 1635
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1641
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1643
    .local v1, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_108

    move v3, v6

    .line 1644
    .local v3, "_arg2":Z
    :goto_a4
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowLockScreenView(Landroid/app/enterprise/ContextInfo;IZ)Z

    move-result v4

    .line 1645
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1646
    if-eqz v4, :cond_106

    move v7, v6

    :cond_106
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1638
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v3    # "_arg2":Z
    .end local v4    # "_result":Z
    :cond_107
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a3

    .restart local v1    # "_arg1":I
    :cond_108
    move v3, v7

    .line 1643
    goto :goto_a4

    .line 1651
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    :sswitch_66
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1653
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10a

    .line 1654
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1660
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1661
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isLockScreenViewAllowed(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v4

    .line 1662
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1663
    if-eqz v4, :cond_109

    move v7, v6

    :cond_109
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1657
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_10a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a5

    .line 1668
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_67
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1670
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10c

    .line 1671
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1677
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10d

    move v1, v6

    .line 1678
    .local v1, "_arg1":Z
    :goto_a7
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setLockScreenState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1679
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1680
    if-eqz v4, :cond_10b

    move v7, v6

    :cond_10b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1674
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_10c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a6

    :cond_10d
    move v1, v7

    .line 1677
    goto :goto_a7

    .line 1685
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_68
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1687
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10f

    .line 1688
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1694
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_110

    move v1, v6

    .line 1695
    .restart local v1    # "_arg1":Z
    :goto_a9
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isLockScreenEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1696
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1697
    if-eqz v4, :cond_10e

    move v7, v6

    :cond_10e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1691
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_10f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a8

    :cond_110
    move v1, v7

    .line 1694
    goto :goto_a9

    .line 1702
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_69
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1704
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_112

    .line 1705
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1711
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_aa
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_113

    move v1, v6

    .line 1712
    .restart local v1    # "_arg1":Z
    :goto_ab
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowFirmwareRecovery(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1713
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1714
    if-eqz v4, :cond_111

    move v7, v6

    :cond_111
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1708
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_112
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_aa

    :cond_113
    move v1, v7

    .line 1711
    goto :goto_ab

    .line 1719
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1721
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_115

    .line 1722
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1728
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ac
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_116

    move v1, v6

    .line 1729
    .restart local v1    # "_arg1":Z
    :goto_ad
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isFirmwareRecoveryAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1730
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1731
    if-eqz v4, :cond_114

    move v7, v6

    :cond_114
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1725
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_115
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ac

    :cond_116
    move v1, v7

    .line 1728
    goto :goto_ad

    .line 1736
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1738
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_118

    move v0, v6

    .line 1740
    .local v0, "_arg0":Z
    :goto_ae
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1741
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isStatusBarExpansionAllowedAsUser(ZI)Z

    move-result v4

    .line 1742
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1743
    if-eqz v4, :cond_117

    move v7, v6

    :cond_117
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_118
    move v0, v7

    .line 1738
    goto :goto_ae

    .line 1748
    :sswitch_6c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1750
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_11a

    .line 1751
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1757
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_af
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_11b

    move v1, v6

    .line 1758
    .local v1, "_arg1":Z
    :goto_b0
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowDeveloperMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1759
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1760
    if-eqz v4, :cond_119

    move v7, v6

    :cond_119
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1754
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_11a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_af

    :cond_11b
    move v1, v7

    .line 1757
    goto :goto_b0

    .line 1765
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1767
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_11d

    .line 1768
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1774
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_11e

    move v1, v6

    .line 1775
    .restart local v1    # "_arg1":Z
    :goto_b2
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isDeveloperModeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1776
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1777
    if-eqz v4, :cond_11c

    move v7, v6

    :cond_11c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1771
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_11d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b1

    :cond_11e
    move v1, v7

    .line 1774
    goto :goto_b2

    .line 1782
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1784
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_120

    .line 1785
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1791
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_121

    move v1, v6

    .line 1792
    .restart local v1    # "_arg1":Z
    :goto_b4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowAirplaneMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1793
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1794
    if-eqz v4, :cond_11f

    move v7, v6

    :cond_11f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1788
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_120
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b3

    :cond_121
    move v1, v7

    .line 1791
    goto :goto_b4

    .line 1799
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1801
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_123

    move v0, v6

    .line 1802
    .local v0, "_arg0":Z
    :goto_b5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isAirplaneModeAllowed(Z)Z

    move-result v4

    .line 1803
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1804
    if-eqz v4, :cond_122

    move v7, v6

    :cond_122
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":Z
    :cond_123
    move v0, v7

    .line 1801
    goto :goto_b5

    .line 1809
    :sswitch_70
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1811
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_125

    .line 1812
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1818
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_126

    move v1, v6

    .line 1819
    .restart local v1    # "_arg1":Z
    :goto_b7
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowGoogleAccountsAutoSync(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1820
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1821
    if-eqz v4, :cond_124

    move v7, v6

    :cond_124
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1815
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_125
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b6

    :cond_126
    move v1, v7

    .line 1818
    goto :goto_b7

    .line 1826
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_71
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1828
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_128

    .line 1829
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1834
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isGoogleAccountsAutoSyncAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 1835
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1836
    if-eqz v4, :cond_127

    move v7, v6

    :cond_127
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1832
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_128
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b8

    .line 1841
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_72
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1843
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1844
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isGoogleAccountsAutoSyncAllowedAsUser(I)Z

    move-result v4

    .line 1845
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1846
    if-eqz v4, :cond_129

    move v7, v6

    :cond_129
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1851
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Z
    :sswitch_73
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1853
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12b

    .line 1854
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1860
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12c

    move v1, v6

    .line 1861
    .restart local v1    # "_arg1":Z
    :goto_ba
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowFirmwareAutoUpdate(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1862
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1863
    if-eqz v4, :cond_12a

    move v7, v6

    :cond_12a
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1857
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_12b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b9

    :cond_12c
    move v1, v7

    .line 1860
    goto :goto_ba

    .line 1868
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_74
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1870
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12e

    .line 1871
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1877
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bb
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12f

    move v1, v6

    .line 1878
    .restart local v1    # "_arg1":Z
    :goto_bc
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isFirmwareAutoUpdateAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1879
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1880
    if-eqz v4, :cond_12d

    move v7, v6

    :cond_12d
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1874
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_12e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bb

    :cond_12f
    move v1, v7

    .line 1877
    goto :goto_bc

    .line 1885
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_75
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1887
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_131

    .line 1888
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1894
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bd
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_132

    move v1, v6

    .line 1895
    .restart local v1    # "_arg1":Z
    :goto_be
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowActivationLock(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1896
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1897
    if-eqz v4, :cond_130

    move v7, v6

    :cond_130
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1891
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_131
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bd

    :cond_132
    move v1, v7

    .line 1894
    goto :goto_be

    .line 1902
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_76
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1904
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_134

    .line 1905
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1911
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bf
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_135

    move v1, v6

    .line 1912
    .restart local v1    # "_arg1":Z
    :goto_c0
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isActivationLockAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1913
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1914
    if-eqz v4, :cond_133

    move v7, v6

    :cond_133
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1908
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_134
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bf

    :cond_135
    move v1, v7

    .line 1911
    goto :goto_c0

    .line 1919
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_77
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1921
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_137

    .line 1922
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1928
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_138

    move v1, v6

    .line 1929
    .restart local v1    # "_arg1":Z
    :goto_c2
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setHeadphoneState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1930
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1931
    if-eqz v4, :cond_136

    move v7, v6

    :cond_136
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1925
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_137
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c1

    :cond_138
    move v1, v7

    .line 1928
    goto :goto_c2

    .line 1936
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_78
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1938
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13a

    .line 1939
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1945
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13b

    move v1, v6

    .line 1946
    .restart local v1    # "_arg1":Z
    :goto_c4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isHeadphoneEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1947
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1948
    if-eqz v4, :cond_139

    move v7, v6

    :cond_139
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1942
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_13a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c3

    :cond_13b
    move v1, v7

    .line 1945
    goto :goto_c4

    .line 1953
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_79
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1955
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13d

    .line 1956
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1962
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13e

    move v1, v6

    .line 1963
    .restart local v1    # "_arg1":Z
    :goto_c6
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSDCardMove(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1964
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1965
    if-eqz v4, :cond_13c

    move v7, v6

    :cond_13c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1959
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_13d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c5

    :cond_13e
    move v1, v7

    .line 1962
    goto :goto_c6

    .line 1970
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1972
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_140

    .line 1973
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1979
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_141

    move v1, v6

    .line 1980
    .restart local v1    # "_arg1":Z
    :goto_c8
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSDCardMoveAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1981
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1982
    if-eqz v4, :cond_13f

    move v7, v6

    :cond_13f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1976
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_140
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c7

    :cond_141
    move v1, v7

    .line 1979
    goto :goto_c8

    .line 1987
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1989
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_143

    .line 1990
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 1996
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_144

    move v1, v6

    .line 1997
    .restart local v1    # "_arg1":Z
    :goto_ca
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowFastEncryption(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 1998
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1999
    if-eqz v4, :cond_142

    move v7, v6

    :cond_142
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1993
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_143
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c9

    :cond_144
    move v1, v7

    .line 1996
    goto :goto_ca

    .line 2004
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7c
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2006
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_146

    .line 2007
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2013
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_cb
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_147

    move v1, v6

    .line 2014
    .restart local v1    # "_arg1":Z
    :goto_cc
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isFastEncryptionAllowed(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2015
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2016
    if-eqz v4, :cond_145

    move v7, v6

    :cond_145
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2010
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_146
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_cb

    :cond_147
    move v1, v7

    .line 2013
    goto :goto_cc

    .line 2021
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7d
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2023
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_149

    .line 2024
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2030
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_cd
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_14a

    move v1, v6

    .line 2031
    .restart local v1    # "_arg1":Z
    :goto_ce
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->setCCMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2032
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2033
    if-eqz v4, :cond_148

    move v7, v6

    :cond_148
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2027
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_149
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_cd

    :cond_14a
    move v1, v7

    .line 2030
    goto :goto_ce

    .line 2038
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7e
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2040
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_14c

    .line 2041
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2047
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_cf
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_14d

    move v1, v6

    .line 2048
    .restart local v1    # "_arg1":Z
    :goto_d0
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isCCModeEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2049
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2050
    if-eqz v4, :cond_14b

    move v7, v6

    :cond_14b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2044
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_14c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_cf

    :cond_14d
    move v1, v7

    .line 2047
    goto :goto_d0

    .line 2055
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7f
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2057
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_14f

    .line 2058
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2064
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_150

    move v1, v6

    .line 2065
    .restart local v1    # "_arg1":Z
    :goto_d2
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isCCModeSupported(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2066
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2067
    if-eqz v4, :cond_14e

    move v7, v6

    :cond_14e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2061
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_14f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d1

    :cond_150
    move v1, v7

    .line 2064
    goto :goto_d2

    .line 2072
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_80
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2074
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_152

    .line 2075
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2081
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_153

    move v1, v6

    .line 2082
    .restart local v1    # "_arg1":Z
    :goto_d4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->enableODETrustedBootVerification(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2083
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2084
    if-eqz v4, :cond_151

    move v7, v6

    :cond_151
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2078
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_152
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d3

    :cond_153
    move v1, v7

    .line 2081
    goto :goto_d4

    .line 2089
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_81
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2091
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_155

    .line 2092
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2097
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isODETrustedBootVerificationEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 2098
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2099
    if-eqz v4, :cond_154

    move v7, v6

    :cond_154
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2095
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_155
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d5

    .line 2104
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_82
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2106
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_157

    .line 2107
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2113
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_158

    move v1, v6

    .line 2114
    .restart local v1    # "_arg1":Z
    :goto_d7
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->preventNewAdminInstallation(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2115
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2116
    if-eqz v4, :cond_156

    move v7, v6

    :cond_156
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2110
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_157
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d6

    :cond_158
    move v1, v7

    .line 2113
    goto :goto_d7

    .line 2121
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_83
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15a

    .line 2124
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2130
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d8
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15b

    move v1, v6

    .line 2131
    .restart local v1    # "_arg1":Z
    :goto_d9
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNewAdminInstallationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2132
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2133
    if-eqz v4, :cond_159

    move v7, v6

    :cond_159
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2127
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_15a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d8

    :cond_15b
    move v1, v7

    .line 2130
    goto :goto_d9

    .line 2138
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_84
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15d

    .line 2141
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2147
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_da
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15e

    move v1, v6

    .line 2148
    .restart local v1    # "_arg1":Z
    :goto_db
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->preventNewAdminActivation(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2149
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2150
    if-eqz v4, :cond_15c

    move v7, v6

    :cond_15c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2144
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_15d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_da

    :cond_15e
    move v1, v7

    .line 2147
    goto :goto_db

    .line 2155
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_85
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_160

    .line 2158
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2164
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_dc
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_161

    move v1, v6

    .line 2165
    .restart local v1    # "_arg1":Z
    :goto_dd
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isNewAdminActivationEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2166
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2167
    if-eqz v4, :cond_15f

    move v7, v6

    :cond_15f
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2161
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_160
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_dc

    :cond_161
    move v1, v7

    .line 2164
    goto :goto_dd

    .line 2172
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_86
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_163

    .line 2175
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2180
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_de
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->clearNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 2181
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2182
    if-eqz v4, :cond_162

    move v7, v6

    :cond_162
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2178
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_163
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_de

    .line 2187
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_87
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2189
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_165

    .line 2190
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2196
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_df
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 2197
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->addNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 2198
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2199
    if-eqz v4, :cond_164

    move v7, v6

    :cond_164
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2193
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_165
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_df

    .line 2204
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_88
    const-string v7, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2206
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_166

    .line 2207
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2212
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e0
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->getNewAdminActivationAppWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 2213
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2214
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 2210
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_166
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e0

    .line 2219
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_89
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2221
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_168

    .line 2222
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2227
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSmartClipModeAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 2228
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2229
    if-eqz v4, :cond_167

    move v7, v6

    :cond_167
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2225
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_168
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e1

    .line 2234
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8a
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2236
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_16a

    move v0, v6

    .line 2237
    .local v0, "_arg0":Z
    :goto_e2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->isSmartClipModeAllowedInternal(Z)Z

    move-result v4

    .line 2238
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2239
    if-eqz v4, :cond_169

    move v7, v6

    :cond_169
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":Z
    :cond_16a
    move v0, v7

    .line 2236
    goto :goto_e2

    .line 2244
    :sswitch_8b
    const-string v8, "android.app.enterprise.IRestrictionPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2246
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_16c

    .line 2247
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 2253
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_16d

    move v1, v6

    .line 2254
    .restart local v1    # "_arg1":Z
    :goto_e4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->allowSmartClipMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 2255
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2256
    if-eqz v4, :cond_16b

    move v7, v6

    :cond_16b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 2250
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_16c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e3

    :cond_16d
    move v1, v7

    .line 2253
    goto :goto_e4

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5c -> :sswitch_5c
        0x5d -> :sswitch_5d
        0x5e -> :sswitch_5e
        0x5f -> :sswitch_5f
        0x60 -> :sswitch_60
        0x61 -> :sswitch_61
        0x62 -> :sswitch_62
        0x63 -> :sswitch_63
        0x64 -> :sswitch_64
        0x65 -> :sswitch_65
        0x66 -> :sswitch_66
        0x67 -> :sswitch_67
        0x68 -> :sswitch_68
        0x69 -> :sswitch_69
        0x6a -> :sswitch_6a
        0x6b -> :sswitch_6b
        0x6c -> :sswitch_6c
        0x6d -> :sswitch_6d
        0x6e -> :sswitch_6e
        0x6f -> :sswitch_6f
        0x70 -> :sswitch_70
        0x71 -> :sswitch_71
        0x72 -> :sswitch_72
        0x73 -> :sswitch_73
        0x74 -> :sswitch_74
        0x75 -> :sswitch_75
        0x76 -> :sswitch_76
        0x77 -> :sswitch_77
        0x78 -> :sswitch_78
        0x79 -> :sswitch_79
        0x7a -> :sswitch_7a
        0x7b -> :sswitch_7b
        0x7c -> :sswitch_7c
        0x7d -> :sswitch_7d
        0x7e -> :sswitch_7e
        0x7f -> :sswitch_7f
        0x80 -> :sswitch_80
        0x81 -> :sswitch_81
        0x82 -> :sswitch_82
        0x83 -> :sswitch_83
        0x84 -> :sswitch_84
        0x85 -> :sswitch_85
        0x86 -> :sswitch_86
        0x87 -> :sswitch_87
        0x88 -> :sswitch_88
        0x89 -> :sswitch_89
        0x8a -> :sswitch_8a
        0x8b -> :sswitch_8b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
