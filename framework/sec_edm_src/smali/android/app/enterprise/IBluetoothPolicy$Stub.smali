.class public abstract Landroid/app/enterprise/IBluetoothPolicy$Stub;
.super Landroid/os/Binder;
.source "IBluetoothPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IBluetoothPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IBluetoothPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IBluetoothPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IBluetoothPolicy"

.field static final TRANSACTION_activateBluetoothDeviceRestriction:I = 0x24

.field static final TRANSACTION_activateBluetoothUUIDRestriction:I = 0x1b

.field static final TRANSACTION_addBluetoothDevicesToBlackList:I = 0x1c

.field static final TRANSACTION_addBluetoothDevicesToWhiteList:I = 0x20

.field static final TRANSACTION_addBluetoothUUIDsToBlackList:I = 0x13

.field static final TRANSACTION_addBluetoothUUIDsToWhiteList:I = 0x17

.field static final TRANSACTION_allowBluetooth:I = 0x4

.field static final TRANSACTION_allowCallerIDDisplay:I = 0x32

.field static final TRANSACTION_allowOutgoingCalls:I = 0x9

.field static final TRANSACTION_bluetoothLog:I = 0x31

.field static final TRANSACTION_clearBluetoothDevicesFromBlackList:I = 0x1e

.field static final TRANSACTION_clearBluetoothDevicesFromWhiteList:I = 0x22

.field static final TRANSACTION_clearBluetoothUUIDsFromBlackList:I = 0x15

.field static final TRANSACTION_clearBluetoothUUIDsFromWhiteList:I = 0x19

.field static final TRANSACTION_getAllBluetoothDevicesBlackLists:I = 0x1f

.field static final TRANSACTION_getAllBluetoothDevicesWhiteLists:I = 0x23

.field static final TRANSACTION_getAllBluetoothUUIDsBlackLists:I = 0x16

.field static final TRANSACTION_getAllBluetoothUUIDsWhiteLists:I = 0x1a

.field static final TRANSACTION_getAllowBluetoothDataTransfer:I = 0x2

.field static final TRANSACTION_getBluetoothLog:I = 0x30

.field static final TRANSACTION_getEffectiveBluetoothDevicesBlackLists:I = 0x2b

.field static final TRANSACTION_getEffectiveBluetoothDevicesWhiteLists:I = 0x2c

.field static final TRANSACTION_getEffectiveBluetoothUUIDsBlackLists:I = 0x29

.field static final TRANSACTION_getEffectiveBluetoothUUIDsWhiteLists:I = 0x2a

.field static final TRANSACTION_isBluetoothDeviceAllowed:I = 0x28

.field static final TRANSACTION_isBluetoothDeviceRestrictionActive:I = 0x26

.field static final TRANSACTION_isBluetoothEnabled:I = 0x5

.field static final TRANSACTION_isBluetoothEnabledWithMsg:I = 0x6

.field static final TRANSACTION_isBluetoothLogEnabled:I = 0x2f

.field static final TRANSACTION_isBluetoothUUIDAllowed:I = 0x27

.field static final TRANSACTION_isBluetoothUUIDRestrictionActive:I = 0x25

.field static final TRANSACTION_isCallerIDDisplayAllowed:I = 0x33

.field static final TRANSACTION_isDesktopConnectivityEnabled:I = 0x12

.field static final TRANSACTION_isDiscoverableEnabled:I = 0x10

.field static final TRANSACTION_isLimitedDiscoverableEnabled:I = 0xc

.field static final TRANSACTION_isOutgoingCallsAllowed:I = 0xa

.field static final TRANSACTION_isPairingEnabled:I = 0x8

.field static final TRANSACTION_isProfileEnabled:I = 0xe

.field static final TRANSACTION_isProfileEnabledInternal:I = 0x2e

.field static final TRANSACTION_removeBluetoothDevicesFromBlackList:I = 0x1d

.field static final TRANSACTION_removeBluetoothDevicesFromWhiteList:I = 0x21

.field static final TRANSACTION_removeBluetoothUUIDsFromBlackList:I = 0x14

.field static final TRANSACTION_removeBluetoothUUIDsFromWhiteList:I = 0x18

.field static final TRANSACTION_setAllowBluetoothDataTransfer:I = 0x1

.field static final TRANSACTION_setBluetooth:I = 0x3

.field static final TRANSACTION_setBluetoothLogEnabled:I = 0x2d

.field static final TRANSACTION_setDesktopConnectivityState:I = 0x11

.field static final TRANSACTION_setDiscoverableState:I = 0xf

.field static final TRANSACTION_setLimitedDiscoverableState:I = 0xb

.field static final TRANSACTION_setPairingState:I = 0x7

.field static final TRANSACTION_setProfileState:I = 0xd


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "android.app.enterprise.IBluetoothPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IBluetoothPolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Landroid/app/enterprise/IBluetoothPolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Landroid/app/enterprise/IBluetoothPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IBluetoothPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 862
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 45
    :sswitch_0
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1

    .line 53
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2

    move v1, v7

    .line 60
    .local v1, "_arg1":Z
    :goto_2
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setAllowBluetoothDataTransfer(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 61
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v4, :cond_0

    move v8, v7

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    :cond_2
    move v1, v8

    .line 59
    goto :goto_2

    .line 67
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4

    .line 70
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 76
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5

    move v1, v7

    .line 77
    .restart local v1    # "_arg1":Z
    :goto_4
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getAllowBluetoothDataTransfer(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 78
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 79
    if-eqz v4, :cond_3

    move v8, v7

    :cond_3
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 73
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    :cond_5
    move v1, v8

    .line 76
    goto :goto_4

    .line 84
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_7

    .line 87
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 93
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_8

    move v1, v7

    .line 94
    .restart local v1    # "_arg1":Z
    :goto_6
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setBluetooth(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 95
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 96
    if-eqz v4, :cond_6

    move v8, v7

    :cond_6
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 90
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    :cond_8
    move v1, v8

    .line 93
    goto :goto_6

    .line 101
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_a

    .line 104
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 110
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_b

    move v1, v7

    .line 111
    .restart local v1    # "_arg1":Z
    :goto_8
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->allowBluetooth(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 112
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 113
    if-eqz v4, :cond_9

    move v8, v7

    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 107
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    :cond_b
    move v1, v8

    .line 110
    goto :goto_8

    .line 118
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_d

    .line 121
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 126
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 127
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 128
    if-eqz v4, :cond_c

    move v8, v7

    :cond_c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 124
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 133
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_f

    move v0, v7

    .line 136
    .local v0, "_arg0":Z
    :goto_a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothEnabledWithMsg(Z)Z

    move-result v4

    .line 137
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v4, :cond_e

    move v8, v7

    :cond_e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":Z
    :cond_f
    move v0, v8

    .line 135
    goto :goto_a

    .line 143
    :sswitch_7
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_11

    .line 146
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 152
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_12

    move v1, v7

    .line 153
    .restart local v1    # "_arg1":Z
    :goto_c
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setPairingState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 154
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v4, :cond_10

    move v8, v7

    :cond_10
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 149
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    :cond_12
    move v1, v8

    .line 152
    goto :goto_c

    .line 160
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_14

    .line 163
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 168
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isPairingEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 169
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 170
    if-eqz v4, :cond_13

    move v8, v7

    :cond_13
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 166
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 175
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_16

    .line 178
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 184
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_17

    move v1, v7

    .line 185
    .restart local v1    # "_arg1":Z
    :goto_f
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->allowOutgoingCalls(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 186
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    if-eqz v4, :cond_15

    move v8, v7

    :cond_15
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 181
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_16
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    :cond_17
    move v1, v8

    .line 184
    goto :goto_f

    .line 192
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_19

    .line 195
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 200
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isOutgoingCallsAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 201
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 202
    if-eqz v4, :cond_18

    move v8, v7

    :cond_18
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 198
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_19
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 207
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1b

    .line 210
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 216
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1c

    move v1, v7

    .line 217
    .restart local v1    # "_arg1":Z
    :goto_12
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setLimitedDiscoverableState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 218
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 219
    if-eqz v4, :cond_1a

    move v8, v7

    :cond_1a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 213
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    :cond_1c
    move v1, v8

    .line 216
    goto :goto_12

    .line 224
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1e

    .line 227
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 232
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isLimitedDiscoverableEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 233
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 234
    if-eqz v4, :cond_1d

    move v8, v7

    :cond_1d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 230
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_1e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 239
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_20

    .line 242
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 248
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_21

    move v1, v7

    .line 250
    .restart local v1    # "_arg1":Z
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 251
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setProfileState(Landroid/app/enterprise/ContextInfo;ZI)Z

    move-result v4

    .line 252
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 253
    if-eqz v4, :cond_1f

    move v8, v7

    :cond_1f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 245
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":I
    .end local v4    # "_result":Z
    :cond_20
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_14

    :cond_21
    move v1, v8

    .line 248
    goto :goto_15

    .line 258
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_23

    .line 261
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 267
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 268
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isProfileEnabled(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v4

    .line 269
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 270
    if-eqz v4, :cond_22

    move v8, v7

    :cond_22
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 264
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_23
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 275
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_25

    .line 278
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 284
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_26

    move v1, v7

    .line 285
    .local v1, "_arg1":Z
    :goto_18
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setDiscoverableState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 286
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 287
    if-eqz v4, :cond_24

    move v8, v7

    :cond_24
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 281
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_25
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    :cond_26
    move v1, v8

    .line 284
    goto :goto_18

    .line 292
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 294
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_28

    .line 295
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 300
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isDiscoverableEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 301
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 302
    if-eqz v4, :cond_27

    move v8, v7

    :cond_27
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 298
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 307
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 309
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2a

    .line 310
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 316
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2b

    move v1, v7

    .line 317
    .restart local v1    # "_arg1":Z
    :goto_1b
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setDesktopConnectivityState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 318
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 319
    if-eqz v4, :cond_29

    move v8, v7

    :cond_29
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 313
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_2a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1a

    :cond_2b
    move v1, v8

    .line 316
    goto :goto_1b

    .line 324
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 326
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2d

    .line 327
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 332
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isDesktopConnectivityEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 333
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 334
    if-eqz v4, :cond_2c

    move v8, v7

    :cond_2c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 330
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_2d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 339
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 341
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2f

    .line 342
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 348
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 349
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->addBluetoothUUIDsToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 350
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 351
    if-eqz v4, :cond_2e

    move v8, v7

    :cond_2e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 345
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_2f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    .line 356
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 358
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_31

    .line 359
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 365
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 366
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->removeBluetoothUUIDsFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 367
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 368
    if-eqz v4, :cond_30

    move v8, v7

    :cond_30
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 362
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_31
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 373
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 375
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_33

    .line 376
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 381
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->clearBluetoothUUIDsFromBlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 382
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 383
    if-eqz v4, :cond_32

    move v8, v7

    :cond_32
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 379
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_33
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .line 388
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_34

    .line 391
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 396
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getAllBluetoothUUIDsBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 397
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 398
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 394
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    :cond_34
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 403
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_36

    .line 406
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 412
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 413
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->addBluetoothUUIDsToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 414
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 415
    if-eqz v4, :cond_35

    move v8, v7

    :cond_35
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 409
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 420
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 422
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_38

    .line 423
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 429
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 430
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->removeBluetoothUUIDsFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 431
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 432
    if-eqz v4, :cond_37

    move v8, v7

    :cond_37
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 426
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_38
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 437
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3a

    .line 440
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 445
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_23
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->clearBluetoothUUIDsFromWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 446
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 447
    if-eqz v4, :cond_39

    move v8, v7

    :cond_39
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 443
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_3a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_23

    .line 452
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3b

    .line 455
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 460
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getAllBluetoothUUIDsWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 461
    .restart local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 462
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 458
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    :cond_3b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 467
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 469
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3d

    .line 470
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 476
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3e

    move v1, v7

    .line 477
    .restart local v1    # "_arg1":Z
    :goto_26
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->activateBluetoothUUIDRestriction(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 478
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 479
    if-eqz v4, :cond_3c

    move v8, v7

    :cond_3c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 473
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_3d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    :cond_3e
    move v1, v8

    .line 476
    goto :goto_26

    .line 484
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 486
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_40

    .line 487
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 493
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 494
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->addBluetoothDevicesToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 495
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 496
    if-eqz v4, :cond_3f

    move v8, v7

    :cond_3f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 490
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_40
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 501
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 503
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_42

    .line 504
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 510
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 511
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->removeBluetoothDevicesFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 512
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 513
    if-eqz v4, :cond_41

    move v8, v7

    :cond_41
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 507
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_42
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 518
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1e
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 520
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_44

    .line 521
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 526
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->clearBluetoothDevicesFromBlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 527
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 528
    if-eqz v4, :cond_43

    move v8, v7

    :cond_43
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 524
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_44
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 533
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 535
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_45

    .line 536
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 541
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getAllBluetoothDevicesBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 542
    .restart local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 543
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 539
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    :cond_45
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2a

    .line 548
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 550
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_47

    .line 551
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 557
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 558
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->addBluetoothDevicesToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 559
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 560
    if-eqz v4, :cond_46

    move v8, v7

    :cond_46
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 554
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_47
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 565
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 567
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_49

    .line 568
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 574
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 575
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->removeBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 576
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 577
    if-eqz v4, :cond_48

    move v8, v7

    :cond_48
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 571
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_result":Z
    :cond_49
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 582
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 584
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4b

    .line 585
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 590
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->clearBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 591
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 592
    if-eqz v4, :cond_4a

    move v8, v7

    :cond_4a
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 588
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_4b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 597
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 599
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4c

    .line 600
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 605
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getAllBluetoothDevicesWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 606
    .restart local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 607
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 603
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothControlInfo;>;"
    :cond_4c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 612
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_24
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 614
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4e

    .line 615
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 621
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4f

    move v1, v7

    .line 622
    .restart local v1    # "_arg1":Z
    :goto_30
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->activateBluetoothDeviceRestriction(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 623
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 624
    if-eqz v4, :cond_4d

    move v8, v7

    :cond_4d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 618
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_4e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2f

    :cond_4f
    move v1, v8

    .line 621
    goto :goto_30

    .line 629
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_25
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_51

    .line 632
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 637
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_31
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothUUIDRestrictionActive(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 638
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 639
    if-eqz v4, :cond_50

    move v8, v7

    :cond_50
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 635
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_51
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_31

    .line 644
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_26
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 646
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_53

    .line 647
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 652
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothDeviceRestrictionActive(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 653
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 654
    if-eqz v4, :cond_52

    move v8, v7

    :cond_52
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 650
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_53
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 659
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 661
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_55

    .line 662
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 668
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_33
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 669
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothUUIDAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v4

    .line 670
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 671
    if-eqz v4, :cond_54

    move v8, v7

    :cond_54
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 665
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_55
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_33

    .line 676
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_28
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 678
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_57

    .line 679
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 685
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 686
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothDeviceAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v4

    .line 687
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 688
    if-eqz v4, :cond_56

    move v8, v7

    :cond_56
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 682
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_57
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 693
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_29
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 695
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_58

    .line 696
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 701
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getEffectiveBluetoothUUIDsBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 702
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 703
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 699
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_58
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 708
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2a
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 710
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_59

    .line 711
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 716
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_36
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getEffectiveBluetoothUUIDsWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 717
    .restart local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 718
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 714
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_59
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_36

    .line 723
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2b
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 725
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5a

    .line 726
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 731
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getEffectiveBluetoothDevicesBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 732
    .restart local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 733
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 729
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 738
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2c
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 740
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5b

    .line 741
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 746
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_38
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getEffectiveBluetoothDevicesWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 747
    .restart local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 748
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 744
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_38

    .line 753
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2d
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 755
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5d

    .line 756
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 762
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5e

    move v1, v7

    .line 763
    .local v1, "_arg1":Z
    :goto_3a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->setBluetoothLogEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 764
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 765
    if-eqz v4, :cond_5c

    move v8, v7

    :cond_5c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 759
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_5d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    :cond_5e
    move v1, v8

    .line 762
    goto :goto_3a

    .line 770
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 772
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 774
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_60

    move v1, v7

    .line 775
    .restart local v1    # "_arg1":Z
    :goto_3b
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isProfileEnabledInternal(IZ)Z

    move-result v4

    .line 776
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 777
    if-eqz v4, :cond_5f

    move v8, v7

    :cond_5f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_60
    move v1, v8

    .line 774
    goto :goto_3b

    .line 782
    .end local v0    # "_arg0":I
    :sswitch_2f
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 784
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_62

    .line 785
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 790
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isBluetoothLogEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 791
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 792
    if-eqz v4, :cond_61

    move v8, v7

    :cond_61
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 788
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_62
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3c

    .line 797
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_30
    const-string v8, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_63

    .line 800
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 805
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->getBluetoothLog(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v6

    .line 806
    .restart local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 807
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 803
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_63
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3d

    .line 812
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_31
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 814
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_65

    .line 815
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 821
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 823
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 824
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->bluetoothLog(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    .line 825
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 826
    if-eqz v4, :cond_64

    move v8, v7

    :cond_64
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 818
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_65
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 831
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_32
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 833
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_67

    .line 834
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 840
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_68

    move v1, v7

    .line 841
    .local v1, "_arg1":Z
    :goto_40
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->allowCallerIDDisplay(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 842
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 843
    if-eqz v4, :cond_66

    move v8, v7

    :cond_66
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 837
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_67
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    :cond_68
    move v1, v8

    .line 840
    goto :goto_40

    .line 848
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_33
    const-string v9, "android.app.enterprise.IBluetoothPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 850
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_6a

    .line 851
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 856
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->isCallerIDDisplayAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 857
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 858
    if-eqz v4, :cond_69

    move v8, v7

    :cond_69
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 854
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_6a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
