.class public Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
.super Ljava/lang/Object;
.source "KnoxCustomManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/knoxcustom/KnoxCustomManager$1;,
        Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;
    }
.end annotation


# static fields
.field public static final ALARM:I = 0x4

.field public static final BACKUP:I = 0x1

.field public static final BUSY:I = -0x5

.field public static final CLASS_STRING:I = 0xde

.field public static final DEFAULT:I = 0x4

.field public static final DESTINATION_ADDRESS:I = 0x14c

.field public static final DISABLE:I = 0x0

.field public static final ENABLE:I = 0x1

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field public static final FAIL:I = -0x1

.field public static final HIDE:I = 0x3

.field public static final INVALID_ADDRESS:I = -0x24

.field public static final INVALID_DEVICE:I = -0x2f

.field public static final INVALID_EMERGENCY_NUMBER:I = -0x31

.field public static final INVALID_INPUT_TYPE:I = -0x30

.field public static final INVALID_LENGTH:I = -0x33

.field public static final INVALID_LOCALE:I = -0x2c

.field public static final INVALID_MODE_TYPE:I = -0x2b

.field public static final INVALID_PACKAGE:I = -0x21

.field public static final INVALID_PASSCODE:I = -0x20

.field public static final INVALID_PERCENT_VALUE:I = -0x2a

.field public static final INVALID_PERMISSION:I = -0x25

.field public static final INVALID_RING_TONE_TYPE:I = -0x22

.field public static final INVALID_ROTATION_TYPE:I = -0x27

.field public static final INVALID_SOUND_TYPE:I = -0x26

.field public static final INVALID_STRING:I = -0x28

.field public static final INVALID_STRING_TYPE:I = -0x29

.field public static final INVALID_TIMEOUT:I = -0x2d

.field public static final INVALID_UID:I = -0x2e

.field public static final INVALID_VALUE:I = -0x32

.field public static final MEDIA_PLAYBACK:I = 0x3

.field public static final MOTION:I = 0x1

.field public static final MULTI_WINDOW_FIXED_STATE:I = 0x1b9

.field public static final MULTI_WINDOW_PERCENTAGE:I = 0x1ba

.field public static final NOTIFICATIONS:I = 0x5

.field public static final NOTIFICATIONS_ALL:I = 0x1f

.field public static final NOTIFICATIONS_BATTERY_FULL:I = 0x2

.field public static final NOTIFICATIONS_BATTERY_LOW:I = 0x1

.field public static final NOTIFICATIONS_NITZ_SET_TIME:I = 0x10

.field public static final NOTIFICATIONS_NONE:I = 0x0

.field public static final NOTIFICATIONS_SAFE_VOLUME:I = 0x4

.field public static final NOTIFICATIONS_STATUS_BAR:I = 0x8

.field public static final NOT_SUPPORTED:I = -0x6

.field public static final PACKAGE_STRING:I = 0xdd

.field public static final PALM_MOTION:I = 0x2

.field public static final PERMISSION_DENIED:I = -0x4

.field public static final POLICY_RESTRICTED:I = -0x7

.field public static final POWER_DIALOG_ACCESSIBILITY:I = 0x1

.field public static final POWER_DIALOG_AIRPLANEMODE:I = 0x20

.field public static final POWER_DIALOG_ALL:I = 0x3ff

.field public static final POWER_DIALOG_BUGREPORT:I = 0x100

.field public static final POWER_DIALOG_DATAMODETOGGLE:I = 0x10

.field public static final POWER_DIALOG_EMERGENCY:I = 0x80

.field public static final POWER_DIALOG_NONE:I = 0x0

.field public static final POWER_DIALOG_POWEROFF:I = 0x4

.field public static final POWER_DIALOG_RESTART:I = 0x40

.field public static final POWER_DIALOG_SILENTMODE:I = 0x200

.field public static final POWER_DIALOG_SLEEP:I = 0x8

.field public static final POWER_DIALOG_TALKBACK:I = 0x2

.field public static final RECENT_LONGPRESS_GLOBAL:I = 0x2

.field public static final RECENT_LONGPRESS_HOME:I = 0x1

.field public static final RECENT_LONGPRESS_OFF:I = 0x0

.field public static final RESTORE:I = 0x2

.field public static final RINGER:I = 0x2

.field public static final RING_TONE_NOT_FOUND:I = -0x23

.field public static final ROTATION_0:I = 0x0

.field public static final ROTATION_180:I = 0x2

.field public static final ROTATION_270:I = 0x3

.field public static final ROTATION_90:I = 0x1

.field public static final ROTATION_CURRENT:I = -0x1

.field public static final SEALED_MODE_ACTIVE:I = -0x3

.field public static final SEALED_MODE_NOT_ACTIVE:I = -0x2

.field public static final SEALED_OFF_STRING:I = 0x71

.field public static final SEALED_ON_STRING:I = 0x70

.field public static final SEALED_OPTION_STRING:I = 0x6f

.field public static final SETTINGS_ALL:I = 0x1ff

.field public static final SETTINGS_BACKUP_RESET:I = 0x40

.field public static final SETTINGS_BLUETOOTH:I = 0x2

.field public static final SETTINGS_DEVELOPER:I = 0x100

.field public static final SETTINGS_FLIGHT_MODE:I = 0x4

.field public static final SETTINGS_LANGUAGE:I = 0x20

.field public static final SETTINGS_LOCK_SCREEN:I = 0x10

.field public static final SETTINGS_MULTI_WINDOW:I = 0x8

.field public static final SETTINGS_USERS:I = 0x80

.field public static final SETTINGS_WIFI:I = 0x1

.field public static final SHOW:I = 0x2

.field public static final SOURCE_ADDRESS:I = 0x14b

.field public static final SUCCESS:I = 0x0

.field public static final SYSTEM_SOUNDS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "KnoxCustomManager"

.field public static final VOICE_CALL:I

.field private static gKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private gContentResolver:Landroid/content/ContentResolver;

.field private mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gContentResolver:Landroid/content/ContentResolver;

    .line 383
    return-void
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 10

    .prologue
    .line 412
    iget-object v8, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gContentResolver:Landroid/content/ContentResolver;

    if-nez v8, :cond_0

    .line 414
    :try_start_0
    const-string v8, "android.app.ActivityThread"

    invoke-static {v8}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 415
    .local v1, "atClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const-string v8, "currentActivityThread"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v1, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 416
    .local v5, "getInstance":Ljava/lang/reflect/Method;
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v8, v9}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    .line 417
    .local v7, "obj":Ljava/lang/Object;
    const-string v8, "getSystemContext"

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Class;

    invoke-virtual {v1, v8, v9}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 418
    .local v6, "getSystemContext":Ljava/lang/reflect/Method;
    if-eqz v6, :cond_0

    .line 419
    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 420
    .local v3, "ctx":Ljava/lang/Object;
    move-object v0, v3

    check-cast v0, Landroid/content/Context;

    move-object v2, v0

    .line 421
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iput-object v8, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gContentResolver:Landroid/content/ContentResolver;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 427
    .end local v1    # "atClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "context":Landroid/content/Context;
    .end local v3    # "ctx":Ljava/lang/Object;
    .end local v5    # "getInstance":Ljava/lang/reflect/Method;
    .end local v6    # "getSystemContext":Ljava/lang/reflect/Method;
    .end local v7    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    iget-object v8, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gContentResolver:Landroid/content/ContentResolver;

    return-object v8

    .line 423
    :catch_0
    move-exception v4

    .line 424
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    .locals 2

    .prologue
    .line 403
    sget-object v1, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 404
    :try_start_0
    sget-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    if-nez v0, :cond_0

    .line 405
    new-instance v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    invoke-direct {v0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;-><init>()V

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    .line 407
    :cond_0
    sget-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->gKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    monitor-exit v1

    return-object v0

    .line 408
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    if-nez v0, :cond_0

    .line 386
    const-string v0, "knoxcustom"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    .line 388
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    return-object v0
.end method


# virtual methods
.method public checkEnterprisePermission(Ljava/lang/String;)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 1438
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1440
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->checkEnterprisePermission(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1445
    :goto_0
    return v1

    .line 1441
    :catch_0
    move-exception v0

    .line 1442
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1445
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dialEmergencyNumber(Ljava/lang/String;)I
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 2733
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2735
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->dialEmergencyNumber(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2740
    :goto_0
    return v1

    .line 2736
    :catch_0
    move-exception v0

    .line 2737
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2740
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAutoRotationState()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1686
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public getBackupRestoreState(I)Z
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3100
    if-ne p1, v0, :cond_2

    .line 3102
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "backup_enabled"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 3107
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 3102
    goto :goto_0

    .line 3103
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    .line 3105
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "backup_auto_restore"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 3107
    goto :goto_0
.end method

.method public getBluetoothVisibilityTimeout()I
    .locals 3

    .prologue
    .line 2852
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "bluetooth_discoverable_timeout"

    const/16 v2, 0x78

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getCheckCoverPopupState()Z
    .locals 3

    .prologue
    .line 3334
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3336
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getCheckCoverPopupState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3341
    :goto_0
    return v1

    .line 3337
    :catch_0
    move-exception v0

    .line 3338
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3341
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCpuPowerSavingState()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2085
    const/4 v1, 0x0

    .line 2087
    .local v1, "globalSetting":I
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "powersaving_switch"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2091
    if-nez v1, :cond_0

    .line 2095
    :goto_0
    return v3

    .line 2094
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "psm_cpu_clock"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2095
    .local v0, "cpuSetting":I
    if-ne v0, v2, :cond_1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public getExtendedCallInfoState()Z
    .locals 3

    .prologue
    .line 2703
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2705
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getExtendedCallInfoState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2710
    :goto_0
    return v1

    .line 2706
    :catch_0
    move-exception v0

    .line 2707
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2710
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getKnoxCustomSdkVer()Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;
    .locals 1

    .prologue
    .line 490
    sget-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->KNOX_CUSTOM_SDK_VERSION_2:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    return-object v0
.end method

.method public getMotionControlState(I)Z
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 3164
    if-ne p1, v1, :cond_2

    .line 3165
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_pick_up"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_pick_up_to_call_out"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 3171
    :cond_1
    :goto_0
    return v0

    .line 3167
    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    .line 3168
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "motion_merged_mute_pause"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "surface_palm_swipe"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public getPackageVerifierState()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 3046
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "package_verifier_enable"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getRecentLongPressActivity()Ljava/lang/String;
    .locals 3

    .prologue
    .line 3401
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3403
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getRecentLongPressActivity()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3408
    :goto_0
    return-object v1

    .line 3404
    :catch_0
    move-exception v0

    .line 3405
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3408
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRecentLongPressMode()I
    .locals 3

    .prologue
    .line 3474
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3476
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getRecentLongPressMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3481
    :goto_0
    return v1

    .line 3477
    :catch_0
    move-exception v0

    .line 3478
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3481
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getScreenPowerSavingState()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2015
    const/4 v0, 0x0

    .line 2017
    .local v0, "globalSetting":I
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "powersaving_switch"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2022
    if-nez v0, :cond_0

    .line 2026
    :goto_0
    return v3

    .line 2025
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "psm_display"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 2026
    .local v1, "screenSetting":I
    if-ne v1, v2, :cond_1

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method public getScreenTimeout()I
    .locals 4

    .prologue
    .line 2144
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "screen_off_timeout"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 2145
    .local v0, "val":I
    div-int/lit16 v1, v0, 0x3e8

    return v1
.end method

.method public getSealedExitUI(I)Ljava/lang/String;
    .locals 3
    .param p1, "stringType"    # I

    .prologue
    .line 695
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 697
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedExitUI(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 702
    :goto_0
    return-object v1

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 702
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedHideNotificationMessages()I
    .locals 3

    .prologue
    .line 3274
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3276
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedHideNotificationMessages()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3281
    :goto_0
    return v1

    .line 3277
    :catch_0
    move-exception v0

    .line 3278
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3281
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSealedHomeActivity()Ljava/lang/String;
    .locals 3

    .prologue
    .line 934
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 936
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedHomeActivity()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 941
    :goto_0
    return-object v1

    .line 937
    :catch_0
    move-exception v0

    .line 938
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 941
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedInputMethodRestrictionState()Z
    .locals 3

    .prologue
    .line 1950
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1952
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedInputMethodRestrictionState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1957
    :goto_0
    return v1

    .line 1953
    :catch_0
    move-exception v0

    .line 1954
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1957
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedModeString(I)Ljava/lang/String;
    .locals 3
    .param p1, "stringType"    # I

    .prologue
    .line 858
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 860
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedModeString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 865
    :goto_0
    return-object v1

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 865
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedMultiWindowFixedState(I)I
    .locals 3
    .param p1, "paramType"    # I

    .prologue
    .line 1850
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1852
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedMultiWindowFixedState(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1857
    :goto_0
    return v1

    .line 1853
    :catch_0
    move-exception v0

    .line 1854
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1857
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSealedNotificationMessagesState()Z
    .locals 3

    .prologue
    .line 1744
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1746
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedNotificationMessagesState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1751
    :goto_0
    return v1

    .line 1747
    :catch_0
    move-exception v0

    .line 1748
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1751
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getSealedPowerDialogCustomItems()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2609
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2611
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedPowerDialogCustomItems()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2616
    :goto_0
    return-object v1

    .line 2612
    :catch_0
    move-exception v0

    .line 2613
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2616
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedPowerDialogCustomItemsState()Z
    .locals 3

    .prologue
    .line 2511
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2513
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedPowerDialogCustomItemsState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2518
    :goto_0
    return v1

    .line 2514
    :catch_0
    move-exception v0

    .line 2515
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2518
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedPowerDialogItems()I
    .locals 3

    .prologue
    .line 2385
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2387
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedPowerDialogItems()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2392
    :goto_0
    return v1

    .line 2388
    :catch_0
    move-exception v0

    .line 2389
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2392
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSealedPowerDialogOptionMode()I
    .locals 3

    .prologue
    .line 780
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 782
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedPowerDialogOptionMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 787
    :goto_0
    return v1

    .line 783
    :catch_0
    move-exception v0

    .line 784
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 787
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public getSealedState()Z
    .locals 3

    .prologue
    .line 568
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 570
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 575
    :goto_0
    return v1

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedStatusBarClockState()Z
    .locals 3

    .prologue
    .line 1068
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1070
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedStatusBarClockState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1075
    :goto_0
    return v1

    .line 1071
    :catch_0
    move-exception v0

    .line 1072
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1075
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getSealedStatusBarIconsState()Z
    .locals 3

    .prologue
    .line 1136
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1138
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedStatusBarIconsState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1143
    :goto_0
    return v1

    .line 1139
    :catch_0
    move-exception v0

    .line 1140
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1143
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedStatusBarMode()I
    .locals 3

    .prologue
    .line 1001
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1003
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedStatusBarMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1008
    :goto_0
    return v1

    .line 1004
    :catch_0
    move-exception v0

    .line 1005
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1008
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public getSealedUsbMassStorageState()Z
    .locals 3

    .prologue
    .line 1211
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1213
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedUsbMassStorageState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1218
    :goto_0
    return v1

    .line 1214
    :catch_0
    move-exception v0

    .line 1215
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1218
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getSealedUsbNetAddress(I)Ljava/lang/String;
    .locals 3
    .param p1, "addressType"    # I

    .prologue
    .line 1360
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1362
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedUsbNetAddress(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1367
    :goto_0
    return-object v1

    .line 1363
    :catch_0
    move-exception v0

    .line 1364
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSealedUsbNetState()Z
    .locals 3

    .prologue
    .line 1286
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1288
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSealedUsbNetState()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1293
    :goto_0
    return v1

    .line 1289
    :catch_0
    move-exception v0

    .line 1290
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1293
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2355
    const-string v3, "ril.serialnumber"

    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2356
    .local v0, "rilSerial":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "00000000000"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const-string v1, "ro.serialno"

    .line 2357
    .local v1, "serial":Ljava/lang/String;
    :goto_0
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2358
    .local v2, "serialNum":Ljava/lang/String;
    return-object v2

    .line 2356
    .end local v1    # "serial":Ljava/lang/String;
    .end local v2    # "serialNum":Ljava/lang/String;
    :cond_1
    const-string v1, "ril.serialnumber"

    goto :goto_0
.end method

.method public getSettingsHiddenState()I
    .locals 3

    .prologue
    .line 2795
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2797
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getSettingsHiddenState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2802
    :goto_0
    return v1

    .line 2798
    :catch_0
    move-exception v0

    .line 2799
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2802
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUserInactivityTimeout()I
    .locals 3

    .prologue
    .line 2201
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2203
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->getUserInactivityTimeout()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2208
    :goto_0
    return v1

    .line 2204
    :catch_0
    move-exception v0

    .line 2205
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2208
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getWifiConnectionMonitorState()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2935
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_watchdog_poor_network_test_enabled"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public removeLockScreen()I
    .locals 3

    .prologue
    .line 2232
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2234
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->removeLockScreen()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2239
    :goto_0
    return v1

    .line 2235
    :catch_0
    move-exception v0

    .line 2236
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2239
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setAdbState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1469
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1471
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setAdbState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1476
    :goto_0
    return v1

    .line 1472
    :catch_0
    move-exception v0

    .line 1473
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1476
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setAudioVolume(II)I
    .locals 3
    .param p1, "audioStream"    # I
    .param p2, "volumeLevel"    # I

    .prologue
    .line 2265
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2267
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setAudioVolume(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2272
    :goto_0
    return v1

    .line 2268
    :catch_0
    move-exception v0

    .line 2269
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2272
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setAutoRotationState(ZI)I
    .locals 3
    .param p1, "state"    # Z
    .param p2, "rotation"    # I

    .prologue
    .line 1660
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1662
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setAutoRotationState(ZI)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1667
    :goto_0
    return v1

    .line 1663
    :catch_0
    move-exception v0

    .line 1664
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1667
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setBackupRestoreState(IZ)I
    .locals 3
    .param p1, "type"    # I
    .param p2, "state"    # Z

    .prologue
    .line 3073
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3075
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setBackupRestoreState(IZ)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3080
    :goto_0
    return v1

    .line 3076
    :catch_0
    move-exception v0

    .line 3077
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3080
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setBluetoothState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1532
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1534
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setBluetoothState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1539
    :goto_0
    return v1

    .line 1535
    :catch_0
    move-exception v0

    .line 1536
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1539
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setBluetoothVisibilityTimeout(I)I
    .locals 3
    .param p1, "timeout"    # I

    .prologue
    .line 2828
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2830
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setBluetoothVisibilityTimeout(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2835
    :goto_0
    return v1

    .line 2831
    :catch_0
    move-exception v0

    .line 2832
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2835
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setCheckCoverPopupState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 3306
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3308
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setCheckCoverPopupState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3313
    :goto_0
    return v1

    .line 3309
    :catch_0
    move-exception v0

    .line 3310
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3313
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setCpuPowerSavingState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2056
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2058
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setCpuPowerSavingState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2063
    :goto_0
    return v1

    .line 2059
    :catch_0
    move-exception v0

    .line 2060
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2063
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setDeveloperOptionsHidden()I
    .locals 3

    .prologue
    .line 2641
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2643
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setDeveloperOptionsHidden()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2648
    :goto_0
    return v1

    .line 2644
    :catch_0
    move-exception v0

    .line 2645
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2648
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setExtendedCallInfoState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2676
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2678
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setExtendedCallInfoState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2683
    :goto_0
    return v1

    .line 2679
    :catch_0
    move-exception v0

    .line 2680
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2683
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setGpsState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1596
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1598
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setGpsState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1603
    :goto_0
    return v1

    .line 1599
    :catch_0
    move-exception v0

    .line 1600
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1603
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setInputMethod(Ljava/lang/String;Z)I
    .locals 3
    .param p1, "inputMethodClassName"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 1887
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1889
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setInputMethod(Ljava/lang/String;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1894
    :goto_0
    return v1

    .line 1890
    :catch_0
    move-exception v0

    .line 1891
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1894
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setMobileDataRoamingState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2877
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2879
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setMobileDataRoamingState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2884
    :goto_0
    return v1

    .line 2880
    :catch_0
    move-exception v0

    .line 2881
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2884
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setMobileDataState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1626
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1628
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setMobileDataState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1633
    :goto_0
    return v1

    .line 1629
    :catch_0
    move-exception v0

    .line 1630
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1633
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setMotionControlState(IZ)I
    .locals 3
    .param p1, "type"    # I
    .param p2, "state"    # Z

    .prologue
    .line 3134
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3136
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setMotionControlState(IZ)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3141
    :goto_0
    return v1

    .line 3137
    :catch_0
    move-exception v0

    .line 3138
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3141
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setMultiWindowState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1778
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1780
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setMultiWindowState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1785
    :goto_0
    return v1

    .line 1781
    :catch_0
    move-exception v0

    .line 1782
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1785
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setPackageVerifierState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 3020
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3022
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setPackageVerifierState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3027
    :goto_0
    return v1

    .line 3023
    :catch_0
    move-exception v0

    .line 3024
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3027
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setRecentLongPressActivity(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3374
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3376
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setRecentLongPressActivity(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3381
    :goto_0
    return v1

    .line 3377
    :catch_0
    move-exception v0

    .line 3378
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3381
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setRecentLongPressMode(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 3442
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3444
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setRecentLongPressMode(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3449
    :goto_0
    return v1

    .line 3445
    :catch_0
    move-exception v0

    .line 3446
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3449
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setScreenPowerSavingState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1986
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1988
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setScreenPowerSavingState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1993
    :goto_0
    return v1

    .line 1989
    :catch_0
    move-exception v0

    .line 1990
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1993
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setScreenTimeout(I)I
    .locals 3
    .param p1, "timeout"    # I

    .prologue
    .line 2121
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2123
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setScreenTimeout(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2128
    :goto_0
    return v1

    .line 2124
    :catch_0
    move-exception v0

    .line 2125
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2128
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedExitUI(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "exitUIPackage"    # Ljava/lang/String;
    .param p2, "exitUIClass"    # Ljava/lang/String;

    .prologue
    .line 662
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 664
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedExitUI(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 669
    :goto_0
    return v1

    .line 665
    :catch_0
    move-exception v0

    .line 666
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 669
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedHideNotificationMessages(I)I
    .locals 3
    .param p1, "mask"    # I

    .prologue
    .line 3242
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3244
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedHideNotificationMessages(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3249
    :goto_0
    return v1

    .line 3245
    :catch_0
    move-exception v0

    .line 3246
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3249
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedHomeActivity(Ljava/lang/String;)I
    .locals 3
    .param p1, "homePackage"    # Ljava/lang/String;

    .prologue
    .line 903
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 905
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedHomeActivity(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 910
    :goto_0
    return v1

    .line 906
    :catch_0
    move-exception v0

    .line 907
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 910
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedInputMethodRestrictionState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1921
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1923
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedInputMethodRestrictionState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1928
    :goto_0
    return v1

    .line 1924
    :catch_0
    move-exception v0

    .line 1925
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1928
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedModeString(ILjava/lang/String;)I
    .locals 3
    .param p1, "stringType"    # I
    .param p2, "stringValue"    # Ljava/lang/String;

    .prologue
    .line 825
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 827
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedModeString(ILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 832
    :goto_0
    return v1

    .line 828
    :catch_0
    move-exception v0

    .line 829
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 832
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedMultiWindowFixedState(II)I
    .locals 3
    .param p1, "fixed"    # I
    .param p2, "percentageSplit"    # I

    .prologue
    .line 1818
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1820
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedMultiWindowFixedState(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1825
    :goto_0
    return v1

    .line 1821
    :catch_0
    move-exception v0

    .line 1822
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1825
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedNotificationMessagesState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1714
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1716
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedNotificationMessagesState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1721
    :goto_0
    return v1

    .line 1717
    :catch_0
    move-exception v0

    .line 1718
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1721
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedPassCode(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "currentPassCode"    # Ljava/lang/String;
    .param p2, "passCode"    # Ljava/lang/String;

    .prologue
    .line 613
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 615
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedPassCode(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 620
    :goto_0
    return v1

    .line 616
    :catch_0
    move-exception v0

    .line 617
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 620
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedPowerDialogCustomItems(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 2577
    .local p1, "item":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;>;"
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2579
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedPowerDialogCustomItems(Ljava/util/List;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2584
    :goto_0
    return v1

    .line 2580
    :catch_0
    move-exception v0

    .line 2581
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2584
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedPowerDialogCustomItemsState(Z)I
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 2478
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2480
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedPowerDialogCustomItemsState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2485
    :goto_0
    return v1

    .line 2481
    :catch_0
    move-exception v0

    .line 2482
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2485
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedPowerDialogItems(I)I
    .locals 3
    .param p1, "value"    # I

    .prologue
    .line 2439
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2441
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedPowerDialogItems(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2446
    :goto_0
    return v1

    .line 2442
    :catch_0
    move-exception v0

    .line 2443
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2446
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedPowerDialogOptionMode(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 742
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 744
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedPowerDialogOptionMode(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 749
    :goto_0
    return v1

    .line 745
    :catch_0
    move-exception v0

    .line 746
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 749
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedState(ZLjava/lang/String;)I
    .locals 3
    .param p1, "status"    # Z
    .param p2, "passCode"    # Ljava/lang/String;

    .prologue
    .line 533
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 535
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedState(ZLjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 540
    :goto_0
    return v1

    .line 536
    :catch_0
    move-exception v0

    .line 537
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 540
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedStatusBarClockState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1036
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1038
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedStatusBarClockState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1043
    :goto_0
    return v1

    .line 1039
    :catch_0
    move-exception v0

    .line 1040
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1043
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedStatusBarIconsState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1104
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1106
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedStatusBarIconsState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1111
    :goto_0
    return v1

    .line 1107
    :catch_0
    move-exception v0

    .line 1108
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1111
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedStatusBarMode(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 971
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 973
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedStatusBarMode(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 978
    :goto_0
    return v1

    .line 974
    :catch_0
    move-exception v0

    .line 975
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 978
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedUsbMassStorageState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1179
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1181
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedUsbMassStorageState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1186
    :goto_0
    return v1

    .line 1182
    :catch_0
    move-exception v0

    .line 1183
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1186
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedUsbNetAddresses(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "sourceAddress"    # Ljava/lang/String;
    .param p2, "destinationAddress"    # Ljava/lang/String;

    .prologue
    .line 1329
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1331
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedUsbNetAddresses(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1336
    :goto_0
    return v1

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1336
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSealedUsbNetState(Z)I
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1253
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1255
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSealedUsbNetState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1260
    :goto_0
    return v1

    .line 1256
    :catch_0
    move-exception v0

    .line 1257
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1260
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSettingsHiddenState(ZI)I
    .locals 3
    .param p1, "state"    # Z
    .param p2, "elements"    # I

    .prologue
    .line 2767
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2769
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSettingsHiddenState(ZI)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2774
    :goto_0
    return v1

    .line 2770
    :catch_0
    move-exception v0

    .line 2771
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2774
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setStayAwakeState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 3196
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3198
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setStayAwakeState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3203
    :goto_0
    return v1

    .line 3199
    :catch_0
    move-exception v0

    .line 3200
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3203
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSystemLocale(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "localeLanguage"    # Ljava/lang/String;
    .param p2, "localeCountry"    # Ljava/lang/String;

    .prologue
    .line 2297
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2299
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSystemLocale(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2304
    :goto_0
    return v1

    .line 2300
    :catch_0
    move-exception v0

    .line 2301
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2304
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSystemRingtone(ILjava/lang/String;)I
    .locals 3
    .param p1, "ringToneType"    # I
    .param p2, "ringToneName"    # Ljava/lang/String;

    .prologue
    .line 2333
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2335
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSystemRingtone(ILjava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2340
    :goto_0
    return v1

    .line 2336
    :catch_0
    move-exception v0

    .line 2337
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2340
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSystemSoundsSilent()I
    .locals 3

    .prologue
    .line 2989
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2991
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setSystemSoundsSilent()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2996
    :goto_0
    return v1

    .line 2992
    :catch_0
    move-exception v0

    .line 2993
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2996
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setUnknownSourcesState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 1501
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1503
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setUnknownSourcesState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1508
    :goto_0
    return v1

    .line 1504
    :catch_0
    move-exception v0

    .line 1505
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1508
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setUsbDeviceDefaultPackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;I)I
    .locals 3
    .param p1, "usbDevice"    # Landroid/hardware/usb/UsbDevice;
    .param p2, "applicationPackage"    # Ljava/lang/String;
    .param p3, "applicationUid"    # I

    .prologue
    .line 1408
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1410
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setUsbDeviceDefaultPackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1415
    :goto_0
    return v1

    .line 1411
    :catch_0
    move-exception v0

    .line 1412
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1415
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setUserInactivityTimeout(I)I
    .locals 3
    .param p1, "timeout"    # I

    .prologue
    .line 2176
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2178
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setUserInactivityTimeout(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2183
    :goto_0
    return v1

    .line 2179
    :catch_0
    move-exception v0

    .line 2180
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2183
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setWifiConnectionMonitorState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2909
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2911
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setWifiConnectionMonitorState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2916
    :goto_0
    return v1

    .line 2912
    :catch_0
    move-exception v0

    .line 2913
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2916
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setWifiNetworkNotificationState(Z)I
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2959
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2961
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setWifiNetworkNotificationState(Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2966
    :goto_0
    return v1

    .line 2962
    :catch_0
    move-exception v0

    .line 2963
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2966
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setWifiState(ZLjava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "state"    # Z
    .param p2, "ssid"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 1565
    invoke-direct {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getService()Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1567
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->mService:Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;->setWifiState(ZLjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1572
    :goto_0
    return v1

    .line 1568
    :catch_0
    move-exception v0

    .line 1569
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxCustomManager"

    const-string v2, "Failed talking with KnoxCustomManager service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1572
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
