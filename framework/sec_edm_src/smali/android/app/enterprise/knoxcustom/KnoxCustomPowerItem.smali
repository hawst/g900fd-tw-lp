.class public Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;
.super Ljava/lang/Object;
.source "KnoxCustomPowerItem.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_SEND_BROADCAST:I = 0x1

.field public static final ACTION_SEND_STICKY_BROADCAST:I = 0x2

.field public static final ACTION_START_ACTIVITY:I = 0x4

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mIcon:Landroid/graphics/drawable/BitmapDrawable;

.field private final mIcon_KEY:Ljava/lang/String;

.field private mId:I

.field private final mId_KEY:Ljava/lang/String;

.field private mIntent:Landroid/content/Intent;

.field private mIntentAction:I

.field private final mIntentAction_KEY:Ljava/lang/String;

.field private final mIntent_KEY:Ljava/lang/String;

.field private mText:Ljava/lang/String;

.field private final mText_KEY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem$1;

    invoke-direct {v0}, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/graphics/drawable/BitmapDrawable;Landroid/content/Intent;ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "icon"    # Landroid/graphics/drawable/BitmapDrawable;
    .param p3, "intent"    # Landroid/content/Intent;
    .param p4, "intentAction"    # I
    .param p5, "text"    # Ljava/lang/String;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v0, "KnoxCustomPowerItem"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->TAG:Ljava/lang/String;

    .line 73
    const-string v0, "ID"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId_KEY:Ljava/lang/String;

    .line 75
    const-string v0, "NAME"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon_KEY:Ljava/lang/String;

    .line 78
    const-string v0, "INTENT"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent_KEY:Ljava/lang/String;

    .line 80
    const-string v0, "INTENT_ACTION"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction_KEY:Ljava/lang/String;

    .line 83
    const-string v0, "TEXT"

    iput-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText_KEY:Ljava/lang/String;

    .line 118
    iput p1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId:I

    .line 119
    iput-object p2, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 120
    iput-object p3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent:Landroid/content/Intent;

    .line 121
    iput p4, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction:I

    .line 122
    iput-object p5, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText:Ljava/lang/String;

    .line 123
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-string v3, "KnoxCustomPowerItem"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->TAG:Ljava/lang/String;

    .line 73
    const-string v3, "ID"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId_KEY:Ljava/lang/String;

    .line 75
    const-string v3, "NAME"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon_KEY:Ljava/lang/String;

    .line 78
    const-string v3, "INTENT"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent_KEY:Ljava/lang/String;

    .line 80
    const-string v3, "INTENT_ACTION"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction_KEY:Ljava/lang/String;

    .line 83
    const-string v3, "TEXT"

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText_KEY:Ljava/lang/String;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 255
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "intent"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent:Landroid/content/Intent;

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction:I

    .line 261
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    iput v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId:I

    .line 264
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText:Ljava/lang/String;

    .line 267
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 268
    .local v1, "size":I
    new-array v2, v1, [B

    .line 271
    .local v2, "temp":[B
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->readByteArray([B)V

    .line 272
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    const/4 v4, 0x0

    array-length v5, v2

    invoke-static {v2, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    .line 273
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem$1;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x0

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId:I

    return v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method public getIntentAction()I
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction:I

    return v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "descr:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->describeContents()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " icon:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " intent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " intentAction:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 6
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 212
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 213
    .local v2, "bundle":Landroid/os/Bundle;
    const-string v4, "intent"

    iget-object v5, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntent:Landroid/content/Intent;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 214
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 217
    iget v4, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIntentAction:I

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 220
    iget v4, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mId:I

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    iget-object v4, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mText:Ljava/lang/String;

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 226
    iget-object v4, p0, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->mIcon:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 227
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 228
    .local v3, "stream":Ljava/io/ByteArrayOutputStream;
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x5a

    invoke-virtual {v1, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 229
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 230
    .local v0, "b":[B
    array-length v4, v0

    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 234
    return-void
.end method
