.class public abstract Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;
.super Landroid/os/Binder;
.source "IKnoxCustomManager.java"

# interfaces
.implements Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.knoxcustom.IKnoxCustomManager"

.field static final TRANSACTION_checkEnterprisePermission:I = 0x19

.field static final TRANSACTION_dialEmergencyNumber:I = 0x3b

.field static final TRANSACTION_getCheckCoverPopupState:I = 0x4a

.field static final TRANSACTION_getExtendedCallInfoState:I = 0x3a

.field static final TRANSACTION_getRecentLongPressActivity:I = 0x4c

.field static final TRANSACTION_getRecentLongPressMode:I = 0x4e

.field static final TRANSACTION_getSealedExitUI:I = 0x5

.field static final TRANSACTION_getSealedHideNotificationMessages:I = 0x48

.field static final TRANSACTION_getSealedHomeActivity:I = 0xb

.field static final TRANSACTION_getSealedInputMethodRestrictionState:I = 0x28

.field static final TRANSACTION_getSealedModeString:I = 0x9

.field static final TRANSACTION_getSealedMultiWindowFixedState:I = 0x25

.field static final TRANSACTION_getSealedNotificationMessagesState:I = 0x22

.field static final TRANSACTION_getSealedPowerDialogCustomItems:I = 0x37

.field static final TRANSACTION_getSealedPowerDialogCustomItemsState:I = 0x35

.field static final TRANSACTION_getSealedPowerDialogItems:I = 0x32

.field static final TRANSACTION_getSealedPowerDialogOptionMode:I = 0x7

.field static final TRANSACTION_getSealedState:I = 0x2

.field static final TRANSACTION_getSealedStatusBarClockState:I = 0xf

.field static final TRANSACTION_getSealedStatusBarIconsState:I = 0x11

.field static final TRANSACTION_getSealedStatusBarMode:I = 0xd

.field static final TRANSACTION_getSealedUsbMassStorageState:I = 0x13

.field static final TRANSACTION_getSealedUsbNetAddress:I = 0x17

.field static final TRANSACTION_getSealedUsbNetState:I = 0x15

.field static final TRANSACTION_getSettingsHiddenState:I = 0x3d

.field static final TRANSACTION_getUserInactivityTimeout:I = 0x2d

.field static final TRANSACTION_removeLockScreen:I = 0x2e

.field static final TRANSACTION_setAdbState:I = 0x1a

.field static final TRANSACTION_setAudioVolume:I = 0x2f

.field static final TRANSACTION_setAutoRotationState:I = 0x20

.field static final TRANSACTION_setBackupRestoreState:I = 0x44

.field static final TRANSACTION_setBluetoothState:I = 0x1c

.field static final TRANSACTION_setBluetoothVisibilityTimeout:I = 0x3e

.field static final TRANSACTION_setCheckCoverPopupState:I = 0x49

.field static final TRANSACTION_setCpuPowerSavingState:I = 0x2a

.field static final TRANSACTION_setDeveloperOptionsHidden:I = 0x38

.field static final TRANSACTION_setExtendedCallInfoState:I = 0x39

.field static final TRANSACTION_setGpsState:I = 0x1e

.field static final TRANSACTION_setInputMethod:I = 0x26

.field static final TRANSACTION_setMobileDataRoamingState:I = 0x3f

.field static final TRANSACTION_setMobileDataState:I = 0x1f

.field static final TRANSACTION_setMotionControlState:I = 0x45

.field static final TRANSACTION_setMultiWindowState:I = 0x23

.field static final TRANSACTION_setPackageVerifierState:I = 0x43

.field static final TRANSACTION_setRecentLongPressActivity:I = 0x4b

.field static final TRANSACTION_setRecentLongPressMode:I = 0x4d

.field static final TRANSACTION_setScreenPowerSavingState:I = 0x29

.field static final TRANSACTION_setScreenTimeout:I = 0x2b

.field static final TRANSACTION_setSealedExitUI:I = 0x4

.field static final TRANSACTION_setSealedHideNotificationMessages:I = 0x47

.field static final TRANSACTION_setSealedHomeActivity:I = 0xa

.field static final TRANSACTION_setSealedInputMethodRestrictionState:I = 0x27

.field static final TRANSACTION_setSealedModeString:I = 0x8

.field static final TRANSACTION_setSealedMultiWindowFixedState:I = 0x24

.field static final TRANSACTION_setSealedNotificationMessagesState:I = 0x21

.field static final TRANSACTION_setSealedPassCode:I = 0x3

.field static final TRANSACTION_setSealedPowerDialogCustomItems:I = 0x36

.field static final TRANSACTION_setSealedPowerDialogCustomItemsState:I = 0x34

.field static final TRANSACTION_setSealedPowerDialogItems:I = 0x33

.field static final TRANSACTION_setSealedPowerDialogOptionMode:I = 0x6

.field static final TRANSACTION_setSealedState:I = 0x1

.field static final TRANSACTION_setSealedStatusBarClockState:I = 0xe

.field static final TRANSACTION_setSealedStatusBarIconsState:I = 0x10

.field static final TRANSACTION_setSealedStatusBarMode:I = 0xc

.field static final TRANSACTION_setSealedUsbMassStorageState:I = 0x12

.field static final TRANSACTION_setSealedUsbNetAddresses:I = 0x16

.field static final TRANSACTION_setSealedUsbNetState:I = 0x14

.field static final TRANSACTION_setSettingsHiddenState:I = 0x3c

.field static final TRANSACTION_setStayAwakeState:I = 0x46

.field static final TRANSACTION_setSystemLocale:I = 0x30

.field static final TRANSACTION_setSystemRingtone:I = 0x31

.field static final TRANSACTION_setSystemSoundsSilent:I = 0x42

.field static final TRANSACTION_setUnknownSourcesState:I = 0x1b

.field static final TRANSACTION_setUsbDeviceDefaultPackage:I = 0x18

.field static final TRANSACTION_setUserInactivityTimeout:I = 0x2c

.field static final TRANSACTION_setWifiConnectionMonitorState:I = 0x40

.field static final TRANSACTION_setWifiNetworkNotificationState:I = 0x41

.field static final TRANSACTION_setWifiState:I = 0x1d


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v1, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 826
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 47
    :sswitch_0
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :sswitch_1
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_0

    move v0, v6

    .line 56
    .local v0, "_arg0":Z
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedState(ZLjava/lang/String;)I

    move-result v4

    .line 58
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .end local v0    # "_arg0":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :cond_0
    move v0, v7

    .line 54
    goto :goto_1

    .line 64
    :sswitch_2
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedState()Z

    move-result v4

    .line 66
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    if-eqz v4, :cond_1

    move v7, v6

    :cond_1
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 72
    .end local v4    # "_result":Z
    :sswitch_3
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 76
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 77
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedPassCode(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 78
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 79
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 84
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_4
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 88
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 89
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedExitUI(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 90
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 96
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_5
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 99
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedExitUI(I)Ljava/lang/String;

    move-result-object v4

    .line 100
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_6
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 109
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedPowerDialogOptionMode(I)I

    move-result v4

    .line 110
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 111
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 116
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_7
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedPowerDialogOptionMode()I

    move-result v4

    .line 118
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 119
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 124
    .end local v4    # "_result":I
    :sswitch_8
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 128
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 129
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedModeString(ILjava/lang/String;)I

    move-result v4

    .line 130
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 131
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 136
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_9
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 139
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedModeString(I)Ljava/lang/String;

    move-result-object v4

    .line 140
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 141
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_a
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 149
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedHomeActivity(Ljava/lang/String;)I

    move-result v4

    .line 150
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 156
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_b
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedHomeActivity()Ljava/lang/String;

    move-result-object v4

    .line 158
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 159
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 164
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_c
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 167
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedStatusBarMode(I)I

    move-result v4

    .line 168
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 169
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 174
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_d
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedStatusBarMode()I

    move-result v4

    .line 176
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 182
    .end local v4    # "_result":I
    :sswitch_e
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2

    move v0, v6

    .line 185
    .local v0, "_arg0":Z
    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedStatusBarClockState(Z)I

    move-result v4

    .line 186
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_2
    move v0, v7

    .line 184
    goto :goto_2

    .line 192
    :sswitch_f
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedStatusBarClockState()Z

    move-result v4

    .line 194
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 195
    if-eqz v4, :cond_3

    move v7, v6

    :cond_3
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 200
    .end local v4    # "_result":Z
    :sswitch_10
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4

    move v0, v6

    .line 203
    .restart local v0    # "_arg0":Z
    :goto_3
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedStatusBarIconsState(Z)I

    move-result v4

    .line 204
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 205
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_4
    move v0, v7

    .line 202
    goto :goto_3

    .line 210
    :sswitch_11
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedStatusBarIconsState()Z

    move-result v4

    .line 212
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 213
    if-eqz v4, :cond_5

    move v7, v6

    :cond_5
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 218
    .end local v4    # "_result":Z
    :sswitch_12
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_6

    move v0, v6

    .line 221
    .restart local v0    # "_arg0":Z
    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedUsbMassStorageState(Z)I

    move-result v4

    .line 222
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 223
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_6
    move v0, v7

    .line 220
    goto :goto_4

    .line 228
    :sswitch_13
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedUsbMassStorageState()Z

    move-result v4

    .line 230
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v4, :cond_7

    move v7, v6

    :cond_7
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 236
    .end local v4    # "_result":Z
    :sswitch_14
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8

    move v0, v6

    .line 239
    .restart local v0    # "_arg0":Z
    :goto_5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedUsbNetState(Z)I

    move-result v4

    .line 240
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 241
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_8
    move v0, v7

    .line 238
    goto :goto_5

    .line 246
    :sswitch_15
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 247
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedUsbNetState()Z

    move-result v4

    .line 248
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 249
    if-eqz v4, :cond_9

    move v7, v6

    :cond_9
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 254
    .end local v4    # "_result":Z
    :sswitch_16
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 258
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 259
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedUsbNetAddresses(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 260
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 261
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 266
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_17
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 269
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedUsbNetAddress(I)Ljava/lang/String;

    move-result-object v4

    .line 270
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 271
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 276
    .end local v0    # "_arg0":I
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_18
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 278
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    .line 279
    sget-object v7, Landroid/hardware/usb/UsbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbDevice;

    .line 285
    .local v0, "_arg0":Landroid/hardware/usb/UsbDevice;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 287
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 288
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setUsbDeviceDefaultPackage(Landroid/hardware/usb/UsbDevice;Ljava/lang/String;I)I

    move-result v4

    .line 289
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 290
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 282
    .end local v0    # "_arg0":Landroid/hardware/usb/UsbDevice;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v4    # "_result":I
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/hardware/usb/UsbDevice;
    goto :goto_6

    .line 295
    .end local v0    # "_arg0":Landroid/hardware/usb/UsbDevice;
    :sswitch_19
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 298
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->checkEnterprisePermission(Ljava/lang/String;)Z

    move-result v4

    .line 299
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 300
    if-eqz v4, :cond_b

    move v7, v6

    :cond_b
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 305
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":Z
    :sswitch_1a
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_c

    move v0, v6

    .line 308
    .local v0, "_arg0":Z
    :goto_7
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setAdbState(Z)I

    move-result v4

    .line 309
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 310
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_c
    move v0, v7

    .line 307
    goto :goto_7

    .line 315
    :sswitch_1b
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d

    move v0, v6

    .line 318
    .restart local v0    # "_arg0":Z
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setUnknownSourcesState(Z)I

    move-result v4

    .line 319
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 320
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_d
    move v0, v7

    .line 317
    goto :goto_8

    .line 325
    :sswitch_1c
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_e

    move v0, v6

    .line 328
    .restart local v0    # "_arg0":Z
    :goto_9
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setBluetoothState(Z)I

    move-result v4

    .line 329
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_e
    move v0, v7

    .line 327
    goto :goto_9

    .line 335
    :sswitch_1d
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_f

    move v0, v6

    .line 339
    .restart local v0    # "_arg0":Z
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 341
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 342
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setWifiState(ZLjava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 343
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 344
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_result":I
    :cond_f
    move v0, v7

    .line 337
    goto :goto_a

    .line 349
    :sswitch_1e
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 351
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10

    move v0, v6

    .line 352
    .restart local v0    # "_arg0":Z
    :goto_b
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setGpsState(Z)I

    move-result v4

    .line 353
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 354
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_10
    move v0, v7

    .line 351
    goto :goto_b

    .line 359
    :sswitch_1f
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_11

    move v0, v6

    .line 362
    .restart local v0    # "_arg0":Z
    :goto_c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setMobileDataState(Z)I

    move-result v4

    .line 363
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_11
    move v0, v7

    .line 361
    goto :goto_c

    .line 369
    :sswitch_20
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12

    move v0, v6

    .line 373
    .restart local v0    # "_arg0":Z
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 374
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setAutoRotationState(ZI)I

    move-result v4

    .line 375
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 376
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :cond_12
    move v0, v7

    .line 371
    goto :goto_d

    .line 381
    :sswitch_21
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_13

    move v0, v6

    .line 384
    .restart local v0    # "_arg0":Z
    :goto_e
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedNotificationMessagesState(Z)I

    move-result v4

    .line 385
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 386
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_13
    move v0, v7

    .line 383
    goto :goto_e

    .line 391
    :sswitch_22
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 392
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedNotificationMessagesState()Z

    move-result v4

    .line 393
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 394
    if-eqz v4, :cond_14

    move v7, v6

    :cond_14
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 399
    .end local v4    # "_result":Z
    :sswitch_23
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_15

    move v0, v6

    .line 402
    .restart local v0    # "_arg0":Z
    :goto_f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setMultiWindowState(Z)I

    move-result v4

    .line 403
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 404
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_15
    move v0, v7

    .line 401
    goto :goto_f

    .line 409
    :sswitch_24
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 411
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 413
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 414
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedMultiWindowFixedState(II)I

    move-result v4

    .line 415
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 416
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 421
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :sswitch_25
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 424
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedMultiWindowFixedState(I)I

    move-result v4

    .line 425
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 426
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 431
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_26
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 433
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 435
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_16

    move v2, v6

    .line 436
    .local v2, "_arg1":Z
    :goto_10
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setInputMethod(Ljava/lang/String;Z)I

    move-result v4

    .line 437
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 438
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    .end local v4    # "_result":I
    :cond_16
    move v2, v7

    .line 435
    goto :goto_10

    .line 443
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_27
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 445
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_17

    move v0, v6

    .line 446
    .local v0, "_arg0":Z
    :goto_11
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedInputMethodRestrictionState(Z)I

    move-result v4

    .line 447
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 448
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_17
    move v0, v7

    .line 445
    goto :goto_11

    .line 453
    :sswitch_28
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 454
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedInputMethodRestrictionState()Z

    move-result v4

    .line 455
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 456
    if-eqz v4, :cond_18

    move v7, v6

    :cond_18
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 461
    .end local v4    # "_result":Z
    :sswitch_29
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 463
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_19

    move v0, v6

    .line 464
    .restart local v0    # "_arg0":Z
    :goto_12
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setScreenPowerSavingState(Z)I

    move-result v4

    .line 465
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 466
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_19
    move v0, v7

    .line 463
    goto :goto_12

    .line 471
    :sswitch_2a
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 473
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1a

    move v0, v6

    .line 474
    .restart local v0    # "_arg0":Z
    :goto_13
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setCpuPowerSavingState(Z)I

    move-result v4

    .line 475
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 476
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_1a
    move v0, v7

    .line 473
    goto :goto_13

    .line 481
    :sswitch_2b
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 483
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 484
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setScreenTimeout(I)I

    move-result v4

    .line 485
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 486
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 491
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_2c
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 493
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 494
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setUserInactivityTimeout(I)I

    move-result v4

    .line 495
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 496
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 501
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_2d
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getUserInactivityTimeout()I

    move-result v4

    .line 503
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 504
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 509
    .end local v4    # "_result":I
    :sswitch_2e
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->removeLockScreen()I

    move-result v4

    .line 511
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 512
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 517
    .end local v4    # "_result":I
    :sswitch_2f
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 521
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 522
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setAudioVolume(II)I

    move-result v4

    .line 523
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 524
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 529
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :sswitch_30
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 531
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 533
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 534
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSystemLocale(Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 535
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 536
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 541
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_31
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 543
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 545
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 546
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSystemRingtone(ILjava/lang/String;)I

    move-result v4

    .line 547
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 548
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 553
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_32
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedPowerDialogItems()I

    move-result v4

    .line 555
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 556
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 561
    .end local v4    # "_result":I
    :sswitch_33
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 563
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 564
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedPowerDialogItems(I)I

    move-result v4

    .line 565
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 566
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 571
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_34
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 573
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1b

    move v0, v6

    .line 574
    .local v0, "_arg0":Z
    :goto_14
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedPowerDialogCustomItemsState(Z)I

    move-result v4

    .line 575
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 576
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_1b
    move v0, v7

    .line 573
    goto :goto_14

    .line 581
    :sswitch_35
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 582
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedPowerDialogCustomItemsState()Z

    move-result v4

    .line 583
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 584
    if-eqz v4, :cond_1c

    move v7, v6

    :cond_1c
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 589
    .end local v4    # "_result":Z
    :sswitch_36
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 591
    sget-object v7, Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 592
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;>;"
    invoke-virtual {p0, v1}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedPowerDialogCustomItems(Ljava/util/List;)I

    move-result v4

    .line 593
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 594
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 599
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;>;"
    .end local v4    # "_result":I
    :sswitch_37
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 600
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedPowerDialogCustomItems()Ljava/util/List;

    move-result-object v5

    .line 601
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 602
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 607
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/knoxcustom/KnoxCustomPowerItem;>;"
    :sswitch_38
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 608
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setDeveloperOptionsHidden()I

    move-result v4

    .line 609
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 610
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 615
    .end local v4    # "_result":I
    :sswitch_39
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 617
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1d

    move v0, v6

    .line 618
    .restart local v0    # "_arg0":Z
    :goto_15
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setExtendedCallInfoState(Z)I

    move-result v4

    .line 619
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 620
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_1d
    move v0, v7

    .line 617
    goto :goto_15

    .line 625
    :sswitch_3a
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 626
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getExtendedCallInfoState()Z

    move-result v4

    .line 627
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 628
    if-eqz v4, :cond_1e

    move v7, v6

    :cond_1e
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 633
    .end local v4    # "_result":Z
    :sswitch_3b
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 635
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 636
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->dialEmergencyNumber(Ljava/lang/String;)I

    move-result v4

    .line 637
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 638
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 643
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_3c
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 645
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1f

    move v0, v6

    .line 647
    .local v0, "_arg0":Z
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 648
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSettingsHiddenState(ZI)I

    move-result v4

    .line 649
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 650
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v2    # "_arg1":I
    .end local v4    # "_result":I
    :cond_1f
    move v0, v7

    .line 645
    goto :goto_16

    .line 655
    :sswitch_3d
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 656
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSettingsHiddenState()I

    move-result v4

    .line 657
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 658
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 663
    .end local v4    # "_result":I
    :sswitch_3e
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 665
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 666
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setBluetoothVisibilityTimeout(I)I

    move-result v4

    .line 667
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 668
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 673
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_3f
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_20

    move v0, v6

    .line 676
    .local v0, "_arg0":Z
    :goto_17
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setMobileDataRoamingState(Z)I

    move-result v4

    .line 677
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 678
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_20
    move v0, v7

    .line 675
    goto :goto_17

    .line 683
    :sswitch_40
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 685
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_21

    move v0, v6

    .line 686
    .restart local v0    # "_arg0":Z
    :goto_18
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setWifiConnectionMonitorState(Z)I

    move-result v4

    .line 687
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 688
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_21
    move v0, v7

    .line 685
    goto :goto_18

    .line 693
    :sswitch_41
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 695
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_22

    move v0, v6

    .line 696
    .restart local v0    # "_arg0":Z
    :goto_19
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setWifiNetworkNotificationState(Z)I

    move-result v4

    .line 697
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 698
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_22
    move v0, v7

    .line 695
    goto :goto_19

    .line 703
    :sswitch_42
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 704
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSystemSoundsSilent()I

    move-result v4

    .line 705
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 706
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 711
    .end local v4    # "_result":I
    :sswitch_43
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 713
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_23

    move v0, v6

    .line 714
    .restart local v0    # "_arg0":Z
    :goto_1a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setPackageVerifierState(Z)I

    move-result v4

    .line 715
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 716
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_23
    move v0, v7

    .line 713
    goto :goto_1a

    .line 721
    :sswitch_44
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 723
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 725
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_24

    move v2, v6

    .line 726
    .local v2, "_arg1":Z
    :goto_1b
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setBackupRestoreState(IZ)I

    move-result v4

    .line 727
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 728
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    .end local v4    # "_result":I
    :cond_24
    move v2, v7

    .line 725
    goto :goto_1b

    .line 733
    .end local v0    # "_arg0":I
    :sswitch_45
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 735
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 737
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_25

    move v2, v6

    .line 738
    .restart local v2    # "_arg1":Z
    :goto_1c
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setMotionControlState(IZ)I

    move-result v4

    .line 739
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 740
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    .end local v4    # "_result":I
    :cond_25
    move v2, v7

    .line 737
    goto :goto_1c

    .line 745
    .end local v0    # "_arg0":I
    :sswitch_46
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 747
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_26

    move v0, v6

    .line 748
    .local v0, "_arg0":Z
    :goto_1d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setStayAwakeState(Z)I

    move-result v4

    .line 749
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 750
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_26
    move v0, v7

    .line 747
    goto :goto_1d

    .line 755
    :sswitch_47
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 757
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 758
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setSealedHideNotificationMessages(I)I

    move-result v4

    .line 759
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 760
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 765
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_48
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 766
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getSealedHideNotificationMessages()I

    move-result v4

    .line 767
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 768
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 773
    .end local v4    # "_result":I
    :sswitch_49
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 775
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_27

    move v0, v6

    .line 776
    .local v0, "_arg0":Z
    :goto_1e
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setCheckCoverPopupState(Z)I

    move-result v4

    .line 777
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 778
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v4    # "_result":I
    :cond_27
    move v0, v7

    .line 775
    goto :goto_1e

    .line 783
    :sswitch_4a
    const-string v8, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 784
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getCheckCoverPopupState()Z

    move-result v4

    .line 785
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 786
    if-eqz v4, :cond_28

    move v7, v6

    :cond_28
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 791
    .end local v4    # "_result":Z
    :sswitch_4b
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 793
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 794
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setRecentLongPressActivity(Ljava/lang/String;)I

    move-result v4

    .line 795
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 796
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 801
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v4    # "_result":I
    :sswitch_4c
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 802
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getRecentLongPressActivity()Ljava/lang/String;

    move-result-object v4

    .line 803
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 804
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 809
    .end local v4    # "_result":Ljava/lang/String;
    :sswitch_4d
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 811
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 812
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->setRecentLongPressMode(I)I

    move-result v4

    .line 813
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 814
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 819
    .end local v0    # "_arg0":I
    .end local v4    # "_result":I
    :sswitch_4e
    const-string v7, "android.app.enterprise.knoxcustom.IKnoxCustomManager"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 820
    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/IKnoxCustomManager$Stub;->getRecentLongPressMode()I

    move-result v4

    .line 821
    .restart local v4    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 822
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
