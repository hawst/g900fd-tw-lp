.class public final enum Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;
.super Ljava/lang/Enum;
.source "KnoxCustomManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "KnoxCustomSdkVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

.field public static final enum KNOX_CUSTOM_SDK_VERSION_1:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

.field public static final enum KNOX_CUSTOM_SDK_VERSION_2:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 439
    new-instance v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    const-string v1, "KNOX_CUSTOM_SDK_VERSION_1"

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->KNOX_CUSTOM_SDK_VERSION_1:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    .line 445
    new-instance v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    const-string v1, "KNOX_CUSTOM_SDK_VERSION_2"

    invoke-direct {v0, v1, v3}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->KNOX_CUSTOM_SDK_VERSION_2:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    .line 435
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    sget-object v1, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->KNOX_CUSTOM_SDK_VERSION_1:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    aput-object v1, v0, v2

    sget-object v1, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->KNOX_CUSTOM_SDK_VERSION_2:Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    aput-object v1, v0, v3

    sput-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->$VALUES:[Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 435
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 435
    const-class v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    return-object v0
.end method

.method public static values()[Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;
    .locals 1

    .prologue
    .line 435
    sget-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->$VALUES:[Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    invoke-virtual {v0}, [Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;

    return-object v0
.end method


# virtual methods
.method public getKnoxCustomInternalSdkVer()Ljava/lang/String;
    .locals 2

    .prologue
    .line 463
    sget-object v0, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$1;->$SwitchMap$android$app$enterprise$knoxcustom$KnoxCustomManager$KnoxCustomSdkVersion:[I

    invoke-virtual {p0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager$KnoxCustomSdkVersion;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 469
    const-string v0, "N/A"

    :goto_0
    return-object v0

    .line 465
    :pswitch_0
    const-string v0, "1.0.0"

    goto :goto_0

    .line 467
    :pswitch_1
    const-string v0, "2.0.0"

    goto :goto_0

    .line 463
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
