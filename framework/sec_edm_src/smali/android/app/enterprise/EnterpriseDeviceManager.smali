.class public Landroid/app/enterprise/EnterpriseDeviceManager;
.super Ljava/lang/Object;
.source "EnterpriseDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/EnterpriseDeviceManager$1;,
        Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;,
        Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseKeyVersion;
    }
.end annotation


# static fields
.field public static final ACTION_ADD_DEVICE_ADMIN:Ljava/lang/String; = "android.app.action.ADD_DEVICE_ADMIN"

.field public static final ACTION_CALL_STATE_CHANGED:Ljava/lang/String; = "com.sec.action.CALL_STATE_CHANGED"

.field public static final ACTION_CHECK_REENROLLMENT:Ljava/lang/String; = "edm.intent.action.sec.CHECK_REENROLLMENT"

.field public static final ACTION_EDM_BOOT_COMPLETED:Ljava/lang/String; = "edm.intent.action.ACTION_EDM_BOOT_COMPLETED"

.field public static final ACTION_ENTERPRISE_MIGRATION:Ljava/lang/String; = "android.intent.action.sec.ENTERPRISE_MIGRATION"

.field public static final ACTION_NOTIFY_STORAGE_CARD:Ljava/lang/String; = "secedm.src.android.app.enterprise.action.NOTIFY_STORAGE_CARD"

.field public static final ACTION_NO_USER_ACTIVITY:Ljava/lang/String; = "com.sec.action.NO_USER_ACTIVITY"

.field public static final ACTION_USER_ACTIVITY:Ljava/lang/String; = "com.sec.action.USER_ACTIVITY"

.field public static final APN_SETTINGS_POLICY_SERVICE:Ljava/lang/String; = "apn_settings_policy"

.field public static final APPLICATION_POLICY_SERVICE:Ljava/lang/String; = "application_policy"

.field public static final APPPERMISSION_POLICY_SERVICE:Ljava/lang/String; = "apppermission_control_policy"

.field public static final AUDIT_LOG:Ljava/lang/String; = "auditlog"

.field public static final BLUETOOTH_POLICY_SERVICE:Ljava/lang/String; = "bluetooth_policy"

.field public static final BROWSER_SETTINGS_POLICY_SERVICE:Ljava/lang/String; = "browser_policy"

.field public static final BT_SECURE_MODE_POLICY_SERVICE:Ljava/lang/String; = "bluetooth_secure_mode_policy"

.field public static final CALLING_POLICY_SERVICE:Ljava/lang/String; = "calling_policy"

.field public static final CERTIFICATE_POLICY_SERVICE:Ljava/lang/String; = "certificate_policy"

.field public static final DATA_FADE_POLICY_SERVICE:Ljava/lang/String; = "data_fade_policy"

.field public static final DATE_TIME_POLICY_SERVICE:Ljava/lang/String; = "date_time_policy"

.field public static final DEFAULT_USER_ACTIVITY_TIMEOUT:I = 0x0

.field public static final DEVICE_ACCOUNT_POLICY_SERVICE:Ljava/lang/String; = "device_account_policy"

.field public static final DEVICE_INVENTORY_SERVICE:Ljava/lang/String; = "device_info"

.field public static final DO_KEYGUARD:Ljava/lang/String; = "secedm.src.android.app.enterprise.action.DO_KEYGUARD"

.field public static final DUALSIM_POLICY_SERVICE:Ljava/lang/String; = "dualsim_policy"

.field public static final EAS_ACCOUNT_POLICY_SERVICE:Ljava/lang/String; = "eas_account_policy"

.field public static final EMAIL_ACCOUNT_POLICY_SERVICE:Ljava/lang/String; = "email_account_policy"

.field public static final EMAIL_POLICY_SERVICE:Ljava/lang/String; = "email_policy"

.field public static final ENTERPRISE_BILLING_POLICY_SERVICE:Ljava/lang/String; = "enterprise_billing_policy"

.field public static final ENTERPRISE_CONTAINER_POLICY_SERVICE:Ljava/lang/String; = "enterprise_container_policy"

.field public static final ENTERPRISE_ISL_POLICY_SERVICE:Ljava/lang/String; = "enterprise_isl_policy"

.field public static final ENTERPRISE_LICENSE_POLICY_SERVICE:Ljava/lang/String; = "enterprise_license_policy"

.field public static final ENTERPRISE_POLICY_SERVICE:Ljava/lang/String; = "enterprise_policy"

.field public static final ENTERPRISE_PREMIUM_VPN_POLICY_SERVICE:Ljava/lang/String; = "enterprise_premium_vpn_policy"

.field public static final ENTERPRISE_SSO_POLICY_SERVICE:Ljava/lang/String; = "enterprise_sso_policy"

.field public static final ENTERPRISE_USER_SPACE_SSO_POLICY_SERVICE:Ljava/lang/String; = "enterprise_user_space_sso_policy"

.field public static final ENTERPRISE_VPN_POLICY_SERVICE:Ljava/lang/String; = "enterprise_vpn_policy"

.field public static final ERROR_CRYPTO_CHECK_FAILURE:I = -0x5

.field public static final ERROR_INVALID_FILE:I = -0x3

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_ACTIVE_ADMIN:I = -0x2

.field public static final ERROR_PACKAGE_NAME_MISMATCH:I = -0x4

.field public static final ERROR_UNKNOWN:I = -0x1

.field public static final EXTRA_ADD_EXPLANATION:Ljava/lang/String; = "android.app.extra.ADD_EXPLANATION"

.field public static final EXTRA_CALL_STATE:Ljava/lang/String; = "com.sec.intent.extra.CALL_STATE"

.field public static final EXTRA_CURRENT_VERSION:Ljava/lang/String; = "currentVersion"

.field public static final EXTRA_DEVICE_ADMIN:Ljava/lang/String; = "android.app.extra.DEVICE_ADMIN"

.field public static final EXTRA_MIGRATION_RESULT:Ljava/lang/String; = "migrationResult"

.field public static final EXTRA_PHONE_STATE:Ljava/lang/String; = "com.sec.intent.extra.PHONE_STATE"

.field public static final FIREWALL_POLICY_SERVICE:Ljava/lang/String; = "firewall_policy"

.field public static final GENERIC_SSO_SERVICE:Ljava/lang/String; = "genericssoservice"

.field public static final GENERIC_VPN_POLICY_SERVICE:Ljava/lang/String; = "generic_vpn_policy"

.field public static final GEOFENCING:Ljava/lang/String; = "geofencing"

.field public static final KIOSKMODE:Ljava/lang/String; = "kioskmode"

.field public static final KNOX_CCM_POLICY_SERVICE:Ljava/lang/String; = "knox_ccm_policy"

.field public static final KNOX_CUSTOM_MANAGER_SERVICE:Ljava/lang/String; = "knoxcustom"

.field public static final KNOX_SCEP_POLICY_SERVICE:Ljava/lang/String; = "knox_scep_policy"

.field public static final KNOX_TIMAKEYSTORE_POLICY_SERVICE:Ljava/lang/String; = "knox_timakeystore_policy"

.field public static final KNOX_TRUSTED_PINPAD_POLICY_SERVICE:Ljava/lang/String; = "knox_pinpad_service"

.field public static final KNOX_VPN_POLICY_SERVICE:Ljava/lang/String; = "knox_vpn_policy"

.field public static final LDAP_ACCOUNT_POLICY_SERVICE:Ljava/lang/String; = "ldap_account_policy"

.field public static final LICENSE_LOG_SERVICE:Ljava/lang/String; = "license_log_service"

.field public static final LOCATION_POLICY_SERVICE:Ljava/lang/String; = "location_policy"

.field public static final LOG_MANAGER_SERVICE:Ljava/lang/String; = "log_manager_service"

.field public static final MISC_POLICY_SERVICE:Ljava/lang/String; = "misc_policy"

.field public static final MTP_BLOCKED:Ljava/lang/String; = "secedm.src.android.app.enterprise.action.MTP_BLOCKED"

.field public static final MULTI_USER_MANAGER_SERVICE:Ljava/lang/String; = "multi_user_manager_service"

.field public static final MUM_CONTAINER_POLICY_SERVICE:Ljava/lang/String; = "mum_container_policy"

.field public static final MUM_CONTAINER_RCP_POLICY_SERVICE:Ljava/lang/String; = "mum_container_rcp_policy"

.field public static final PASSWORD_POLICY_SERVICE:Ljava/lang/String; = "password_policy"

.field public static final PASSWORD_QUALITY_ALPHABETIC:I = 0x40000

.field public static final PASSWORD_QUALITY_ALPHANUMERIC:I = 0x50000

.field public static final PASSWORD_QUALITY_NUMERIC:I = 0x20000

.field public static final PASSWORD_QUALITY_SMARTCARDNUMERIC:I = 0x70000

.field public static final PASSWORD_QUALITY_SOMETHING:I = 0x10000

.field public static final PASSWORD_QUALITY_UNSPECIFIED:I = 0x0

.field public static final PHONE_RESTRICTION_POLICY_SERVICE:Ljava/lang/String; = "phone_restriction_policy"

.field public static final REMOTE_INJECTION_SERVICE:Ljava/lang/String; = "remoteinjection"

.field public static final RESET_PASSWORD_REQUIRE_ENTRY:I = 0x1

.field public static final RESTRICTION_POLICY_SERVICE:Ljava/lang/String; = "restriction_policy"

.field public static final ROAMING_POLICY_SERVICE:Ljava/lang/String; = "roaming_policy"

.field public static final SEANDROID_POLICY:Ljava/lang/String; = "seandroid_policy"

.field public static final SECURITY_POLICY_SERVICE:Ljava/lang/String; = "security_policy"

.field public static final SMARTCARD_ACCESS_POLICY_SERVICE:Ljava/lang/String; = "smartcard_access_policy"

.field public static final SMARTCARD_BROWSER_POLICY_SERVICE:Ljava/lang/String; = "smartcard_browser_policy"

.field public static final SMARTCARD_EMAIL_POLICY_SERVICE:Ljava/lang/String; = "smartcard_email_policy"

.field public static final SMARTCARD_LOCKSCREEN_POLICY_SERVICE:Ljava/lang/String; = "smartcard_lockscreen_policy"

.field public static final SMARTCARD_VPN_POLICY_SERVICE:Ljava/lang/String; = "smartcard_vpn_policy"

.field private static TAG:Ljava/lang/String; = null

.field public static final VPN_POLICY_SERVICE:Ljava/lang/String; = "vpn_policy"

.field public static final WIFI_POLICY_SERVICE:Ljava/lang/String; = "wifi_policy"

.field public static final WIPE_EXTERNAL_STORAGE:I = 0x1


# instance fields
.field private mApnSettingsPolicy:Landroid/app/enterprise/ApnSettingsPolicy;

.field private mApnSettingsPolicyCreated:Z

.field private mAppPermissionControlPolicy:Landroid/app/enterprise/ApplicationPermissionControlPolicy;

.field private mAppPermissionControlPolicyCreated:Z

.field private mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

.field private mApplicationPolicyCreated:Z

.field private mBTSecureModePolicy:Landroid/app/enterprise/BluetoothSecureModePolicy;

.field private mBTSecureModePolicyCreated:Z

.field private mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

.field private mBasePasswordPolicyCreated:Z

.field private mBluetoothPolicy:Landroid/app/enterprise/BluetoothPolicy;

.field private mBluetoothPolicyCreated:Z

.field private mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

.field private mBrowserPolicyCreated:Z

.field private final mContext:Landroid/content/Context;

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mDPM:Landroid/app/admin/DevicePolicyManager;

.field private mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

.field private mDateTimePolicyCreated:Z

.field private mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

.field private mDeviceAccountPolicyCreated:Z

.field private mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

.field private mDeviceInventoryCreated:Z

.field private mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

.field private mEasAccountPolicyCreated:Z

.field private mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

.field private mEmailAccountPolicyCreated:Z

.field private mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

.field private mEmailPolicyCreated:Z

.field private mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

.field private mEnterpriseVpnPolicyCreated:Z

.field private mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

.field private mFirewallPolicyCreated:Z

.field private mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

.field private mGenericSSOPolicyCreated:Z

.field private mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

.field private mLDAPAccountPolicyCreated:Z

.field private mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

.field private mLocationPolicyCreated:Z

.field private mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

.field private mMiscPolicyCreated:Z

.field private mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

.field private mPasswordPolicyCreated:Z

.field private mPhonePolicy:Landroid/app/enterprise/PhoneRestrictionPolicy;

.field private mPhonePolicyCreated:Z

.field private mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

.field private mRestrictionPolicyCreated:Z

.field private mRoamingPolicy:Landroid/app/enterprise/RoamingPolicy;

.field private mRoamingPolicyCreated:Z

.field private mSSOPolicy:Landroid/app/enterprise/SSOPolicy;

.field private mSSOPolicyCreated:Z

.field private mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

.field private mSecurityPolicyCreated:Z

.field private mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

.field private mVpnPolicy:Landroid/app/enterprise/VpnPolicy;

.field private mVpnPolicyCreated:Z

.field private mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

.field private mWifiPolicyCreated:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 253
    const-string v0, "EnterpriseDeviceManager"

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 385
    const/4 v0, 0x0

    const/4 v1, 0x1

    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V

    .line 387
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 392
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0, p2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V

    .line 394
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;

    .prologue
    .line 417
    const/4 v0, 0x1

    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {p0, p1, p2, v0, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V

    .line 419
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "checkForContainerId"    # Z

    .prologue
    .line 403
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V

    .line 405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "handler"    # Landroid/os/Handler;
    .param p3, "checkForContainerId"    # Z
    .param p4, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    const/4 v2, 0x0

    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2538
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicyCreated:Z

    .line 2540
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicyCreated:Z

    .line 2542
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicyCreated:Z

    .line 2544
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicyCreated:Z

    .line 2546
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicyCreated:Z

    .line 2548
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventoryCreated:Z

    .line 2550
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicyCreated:Z

    .line 2552
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicyCreated:Z

    .line 2554
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicyCreated:Z

    .line 2556
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicyCreated:Z

    .line 2558
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicyCreated:Z

    .line 2560
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicyCreated:Z

    .line 2562
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicyCreated:Z

    .line 2564
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicyCreated:Z

    .line 2566
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicyCreated:Z

    .line 2568
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicyCreated:Z

    .line 2570
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicyCreated:Z

    .line 2572
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicyCreated:Z

    .line 2574
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicyCreated:Z

    .line 2576
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicyCreated:Z

    .line 2578
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicyCreated:Z

    .line 2580
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicyCreated:Z

    .line 2582
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicyCreated:Z

    .line 2584
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicyCreated:Z

    .line 2589
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicyCreated:Z

    .line 2591
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicyCreated:Z

    .line 2594
    iput-boolean v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSOPolicyCreated:Z

    .line 426
    iput-object p1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    .line 439
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p2}, Landroid/app/admin/DevicePolicyManager;->create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/admin/DevicePolicyManager;

    move-result-object v2

    iput-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    .line 442
    if-eqz p3, :cond_1

    .line 444
    iget v0, p4, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    .line 445
    .local v0, "callerUid":I
    iget v1, p4, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 447
    .local v1, "containerId":I
    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_0

    .line 449
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 454
    :cond_0
    new-instance p4, Landroid/app/enterprise/ContextInfo;

    .end local p4    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    invoke-direct {p4, v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    .line 460
    .end local v0    # "callerUid":I
    .end local v1    # "containerId":I
    .restart local p4    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_1
    iput-object p4, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 462
    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/os/Handler;)Landroid/app/enterprise/EnterpriseDeviceManager;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 484
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    const/4 v1, 0x1

    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {v0, p0, p1, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/os/Handler;ZLandroid/app/enterprise/ContextInfo;)V

    .line 488
    .local v0, "me":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-direct {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .end local v0    # "me":Landroid/app/enterprise/EnterpriseDeviceManager;
    :goto_0
    return-object v0

    .restart local v0    # "me":Landroid/app/enterprise/EnterpriseDeviceManager;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static getContainerId(I)I
    .locals 1
    .param p0, "appUid"    # I

    .prologue
    .line 527
    const/4 v0, 0x0

    return v0
.end method

.method static getContainerType(I)I
    .locals 1
    .param p0, "containerId"    # I

    .prologue
    .line 568
    const/4 v0, -0x1

    return v0
.end method

.method private getService()Landroid/app/enterprise/IEnterpriseDeviceManager;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    if-nez v0, :cond_0

    .line 469
    const-string v0, "enterprise_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    .line 475
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    return-object v0
.end method


# virtual methods
.method public activateAdminForUser(Landroid/content/ComponentName;ZI)V
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;
    .param p2, "refreshing"    # Z
    .param p3, "userHandle"    # I

    .prologue
    .line 3836
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3840
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IEnterpriseDeviceManager;->activateAdminForUser(Landroid/content/ComponentName;ZI)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3850
    :cond_0
    :goto_0
    return-void

    .line 3842
    :catch_0
    move-exception v0

    .line 3844
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public activateDevicePermissions(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3918
    .local p1, "devicePermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3922
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->activateDevicePermissions(Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3932
    :goto_0
    return v1

    .line 3924
    :catch_0
    move-exception v0

    .line 3926
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3932
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V
    .locals 3
    .param p1, "proxyAdmin"    # Landroid/app/admin/ProxyDeviceAdminInfo;
    .param p2, "proxyUid"    # I
    .param p3, "adminComponentName"    # Landroid/content/ComponentName;
    .param p4, "adminUid"    # I

    .prologue
    .line 3751
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3755
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/enterprise/IEnterpriseDeviceManager;->addProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3767
    :cond_0
    :goto_0
    return-void

    .line 3759
    :catch_0
    move-exception v0

    .line 3761
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public configureContainerAdminForMigration(Z)Z
    .locals 4
    .param p1, "blockAdminConnection"    # Z

    .prologue
    .line 4023
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4027
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->configureContainerAdminForMigration(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4041
    :goto_0
    return v1

    .line 4029
    :catch_0
    move-exception v0

    .line 4031
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4041
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4033
    :catch_1
    move-exception v0

    .line 4035
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public deactivateAdminForUser(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "userHandle"    # I

    .prologue
    .line 3864
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3868
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->deactivateAdminForUser(Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3878
    :cond_0
    :goto_0
    return-void

    .line 3870
    :catch_0
    move-exception v0

    .line 3872
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public disableConstrainedState()Z
    .locals 4

    .prologue
    .line 4109
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4111
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->disableConstrainedState(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4118
    :goto_0
    return v1

    .line 4112
    :catch_0
    move-exception v0

    .line 4113
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4118
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4114
    :catch_1
    move-exception v0

    .line 4115
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public enableConstrainedState(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4
    .param p1, "Comment"    # Ljava/lang/String;
    .param p2, "DownloadUrl"    # Ljava/lang/String;
    .param p3, "PolicyBitMask"    # I

    .prologue
    .line 4095
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4097
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enableConstrainedState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4104
    :goto_0
    return v1

    .line 4098
    :catch_0
    move-exception v0

    .line 4099
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4104
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4100
    :catch_1
    move-exception v0

    .line 4101
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public enforceActiveAdminPermission(Ljava/lang/String;)V
    .locals 3
    .param p1, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1547
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1551
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceActiveAdminPermission(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1561
    :cond_0
    :goto_0
    return-void

    .line 1553
    :catch_0
    move-exception v0

    .line 1555
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforceActiveAdminPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1576
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1580
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceActiveAdminPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 1590
    .end local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    :goto_0
    return-object p1

    .line 1582
    .restart local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catch_0
    move-exception v0

    .line 1584
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforceContainerOwnerShipPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1605
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1609
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceContainerOwnerShipPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 1619
    .end local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    :goto_0
    return-object p1

    .line 1611
    .restart local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catch_0
    move-exception v0

    .line 1613
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforceOwnerOnlyAndActiveAdminPermission(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1640
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1644
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceOwnerOnlyAndActiveAdminPermission(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 1654
    .end local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    :goto_0
    return-object p1

    .line 1646
    .restart local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catch_0
    move-exception v0

    .line 1648
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforceOwnerOnlyPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1666
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1668
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforceOwnerOnlyPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 1674
    .end local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    :goto_0
    return-object p1

    .line 1669
    .restart local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catch_0
    move-exception v0

    .line 1670
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enforcePermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    .locals 3
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "reqPermission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/SecurityException;
        }
    .end annotation

    .prologue
    .line 1689
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1693
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->enforcePermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 1703
    .end local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    :goto_0
    return-object p1

    .line 1695
    .restart local p1    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catch_0
    move-exception v0

    .line 1697
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getActiveAdminComponent()Landroid/content/ComponentName;
    .locals 3

    .prologue
    .line 1300
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1304
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getActiveAdminComponent()Landroid/content/ComponentName;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1314
    :goto_0
    return-object v1

    .line 1306
    :catch_0
    move-exception v0

    .line 1308
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1314
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getActiveAdmins(I)Ljava/util/List;
    .locals 3
    .param p1, "userHandle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1333
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1337
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getActiveAdmins(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1347
    :goto_0
    return-object v1

    .line 1339
    :catch_0
    move-exception v0

    .line 1341
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1347
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getActiveAdminsInfo(I)Ljava/util/List;
    .locals 3
    .param p1, "userId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/EnterpriseDeviceAdminInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3892
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3896
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getActiveAdminsInfo(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3906
    :goto_0
    return-object v1

    .line 3898
    :catch_0
    move-exception v0

    .line 3900
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3906
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAdminRemovable()Z
    .locals 4

    .prologue
    .line 1861
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1867
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getAdminRemovable(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1877
    :goto_0
    return v1

    .line 1869
    :catch_0
    move-exception v0

    .line 1871
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1877
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAdminRemovable(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1832
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1836
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getAdminRemovable(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1846
    :goto_0
    return v1

    .line 1838
    :catch_0
    move-exception v0

    .line 1840
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1846
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApnSettingsPolicy()Landroid/app/enterprise/ApnSettingsPolicy;
    .locals 3

    .prologue
    .line 3003
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3005
    const-class v1, Landroid/app/enterprise/ApnSettingsPolicy;

    monitor-enter v1

    .line 3007
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3009
    new-instance v0, Landroid/app/enterprise/ApnSettingsPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ApnSettingsPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicy:Landroid/app/enterprise/ApnSettingsPolicy;

    .line 3011
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicyCreated:Z

    .line 3015
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3019
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApnSettingsPolicy:Landroid/app/enterprise/ApnSettingsPolicy;

    return-object v0

    .line 3015
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getApplicationPermissionControlPolicy()Landroid/app/enterprise/ApplicationPermissionControlPolicy;
    .locals 3

    .prologue
    .line 3372
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3374
    const-class v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;

    monitor-enter v1

    .line 3376
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3378
    new-instance v0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicy:Landroid/app/enterprise/ApplicationPermissionControlPolicy;

    .line 3380
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicyCreated:Z

    .line 3384
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3388
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mAppPermissionControlPolicy:Landroid/app/enterprise/ApplicationPermissionControlPolicy;

    return-object v0

    .line 3384
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getApplicationPolicy()Landroid/app/enterprise/ApplicationPolicy;
    .locals 3

    .prologue
    .line 2632
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2634
    const-class v1, Landroid/app/enterprise/ApplicationPolicy;

    monitor-enter v1

    .line 2636
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2638
    new-instance v0, Landroid/app/enterprise/ApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ApplicationPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    .line 2640
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicyCreated:Z

    .line 2644
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2648
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mApplicationPolicy:Landroid/app/enterprise/ApplicationPolicy;

    return-object v0

    .line 2644
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;
    .locals 3

    .prologue
    .line 2929
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2931
    const-class v1, Landroid/app/enterprise/BasePasswordPolicy;

    monitor-enter v1

    .line 2933
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2935
    new-instance v0, Landroid/app/enterprise/BasePasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/BasePasswordPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

    .line 2937
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicyCreated:Z

    .line 2941
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2945
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBasePasswordPolicy:Landroid/app/enterprise/BasePasswordPolicy;

    return-object v0

    .line 2941
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;
    .locals 3

    .prologue
    .line 3188
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3190
    const-class v1, Landroid/app/enterprise/BluetoothPolicy;

    monitor-enter v1

    .line 3192
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3194
    new-instance v0, Landroid/app/enterprise/BluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/BluetoothPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicy:Landroid/app/enterprise/BluetoothPolicy;

    .line 3196
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicyCreated:Z

    .line 3200
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3204
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBluetoothPolicy:Landroid/app/enterprise/BluetoothPolicy;

    return-object v0

    .line 3200
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBluetoothSecureModePolicy()Landroid/app/enterprise/BluetoothSecureModePolicy;
    .locals 4

    .prologue
    .line 3403
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicyCreated:Z

    if-nez v0, :cond_1

    .line 3405
    const-class v1, Landroid/app/enterprise/BluetoothSecureModePolicy;

    monitor-enter v1

    .line 3407
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicyCreated:Z

    if-nez v0, :cond_0

    .line 3409
    new-instance v0, Landroid/app/enterprise/BluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/BluetoothSecureModePolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicy:Landroid/app/enterprise/BluetoothSecureModePolicy;

    .line 3411
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicyCreated:Z

    .line 3415
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3419
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBTSecureModePolicy:Landroid/app/enterprise/BluetoothSecureModePolicy;

    return-object v0

    .line 3415
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getBrowserPolicy()Landroid/app/enterprise/BrowserPolicy;
    .locals 3

    .prologue
    .line 3225
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3227
    const-class v1, Landroid/app/enterprise/BrowserPolicy;

    monitor-enter v1

    .line 3229
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3231
    new-instance v0, Landroid/app/enterprise/BrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/BrowserPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    .line 3233
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicyCreated:Z

    .line 3237
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3241
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mBrowserPolicy:Landroid/app/enterprise/BrowserPolicy;

    return-object v0

    .line 3237
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getConstrainedState()I
    .locals 4

    .prologue
    .line 4137
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4139
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getConstrainedState()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4146
    :goto_0
    return v1

    .line 4140
    :catch_0
    move-exception v0

    .line 4141
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4146
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4142
    :catch_1
    move-exception v0

    .line 4143
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getCurrentFailedPasswordAttempts()I
    .locals 1

    .prologue
    .line 813
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->getCurrentFailedPasswordAttempts()I

    move-result v0

    return v0
.end method

.method public getDateTimePolicy()Landroid/app/enterprise/DateTimePolicy;
    .locals 3

    .prologue
    .line 3336
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicyCreated:Z

    if-nez v0, :cond_1

    .line 3338
    const-class v1, Landroid/app/enterprise/DateTimePolicy;

    monitor-enter v1

    .line 3340
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicyCreated:Z

    if-nez v0, :cond_0

    .line 3342
    new-instance v0, Landroid/app/enterprise/DateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/DateTimePolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

    .line 3344
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicyCreated:Z

    .line 3348
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3352
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDateTimePolicy:Landroid/app/enterprise/DateTimePolicy;

    return-object v0

    .line 3348
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDeviceAccountPolicy()Landroid/app/enterprise/DeviceAccountPolicy;
    .locals 3

    .prologue
    .line 3649
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3651
    const-class v1, Landroid/app/enterprise/DeviceAccountPolicy;

    monitor-enter v1

    .line 3653
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3655
    new-instance v0, Landroid/app/enterprise/DeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/DeviceAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    .line 3657
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicyCreated:Z

    .line 3661
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3665
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceAccountPolicy:Landroid/app/enterprise/DeviceAccountPolicy;

    return-object v0

    .line 3661
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getDeviceInventory()Landroid/app/enterprise/DeviceInventory;
    .locals 4

    .prologue
    .line 2966
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventoryCreated:Z

    if-nez v0, :cond_1

    .line 2968
    const-class v1, Landroid/app/enterprise/DeviceInventory;

    monitor-enter v1

    .line 2970
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventoryCreated:Z

    if-nez v0, :cond_0

    .line 2972
    new-instance v0, Landroid/app/enterprise/DeviceInventory;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/DeviceInventory;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

    .line 2974
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventoryCreated:Z

    .line 2978
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2982
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDeviceInventory:Landroid/app/enterprise/DeviceInventory;

    return-object v0

    .line 2978
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEmailAccountPolicy()Landroid/app/enterprise/EmailAccountPolicy;
    .locals 3

    .prologue
    .line 2854
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2856
    const-class v1, Landroid/app/enterprise/EmailAccountPolicy;

    monitor-enter v1

    .line 2858
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2860
    new-instance v0, Landroid/app/enterprise/EmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EmailAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    .line 2862
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicyCreated:Z

    .line 2866
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2870
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailAccountPolicy:Landroid/app/enterprise/EmailAccountPolicy;

    return-object v0

    .line 2866
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEmailPolicy()Landroid/app/enterprise/EmailPolicy;
    .locals 3

    .prologue
    .line 3570
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3572
    const-class v1, Landroid/app/enterprise/EmailPolicy;

    monitor-enter v1

    .line 3574
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3576
    new-instance v0, Landroid/app/enterprise/EmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EmailPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    .line 3578
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicyCreated:Z

    .line 3582
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3586
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEmailPolicy:Landroid/app/enterprise/EmailPolicy;

    return-object v0

    .line 3582
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getEnterpriseKeyVer()Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseKeyVersion;
    .locals 1

    .prologue
    .line 2083
    sget-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseKeyVersion;->ENTERPRISE_KEY_VERSION_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseKeyVersion;

    return-object v0
.end method

.method public getEnterpriseSdkVer()Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;
    .locals 2

    .prologue
    .line 2177
    const-string v1, "11"

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2178
    .local v0, "mdm_config_version":I
    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 2179
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2183
    :goto_0
    return-object v1

    .line 2180
    :cond_0
    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 2181
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    goto :goto_0

    .line 2183
    :cond_1
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    goto :goto_0
.end method

.method public getEnterpriseVpnPolicy()Landroid/app/enterprise/EnterpriseVpnPolicy;
    .locals 3

    .prologue
    .line 3262
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3264
    const-class v1, Landroid/app/enterprise/EnterpriseVpnPolicy;

    monitor-enter v1

    .line 3266
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3268
    new-instance v0, Landroid/app/enterprise/EnterpriseVpnPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/EnterpriseVpnPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

    .line 3270
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicyCreated:Z

    .line 3274
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3278
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEnterpriseVpnPolicy:Landroid/app/enterprise/EnterpriseVpnPolicy;

    return-object v0

    .line 3274
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getExchangeAccountPolicy()Landroid/app/enterprise/ExchangeAccountPolicy;
    .locals 3

    .prologue
    .line 2669
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2671
    const-class v1, Landroid/app/enterprise/ExchangeAccountPolicy;

    monitor-enter v1

    .line 2673
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2675
    new-instance v0, Landroid/app/enterprise/ExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/ExchangeAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    .line 2677
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicyCreated:Z

    .line 2681
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2685
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mEasAccountPolicy:Landroid/app/enterprise/ExchangeAccountPolicy;

    return-object v0

    .line 2681
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getFirewallPolicy()Landroid/app/enterprise/FirewallPolicy;
    .locals 4

    .prologue
    .line 3151
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3153
    const-class v1, Landroid/app/enterprise/FirewallPolicy;

    monitor-enter v1

    .line 3155
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3157
    new-instance v0, Landroid/app/enterprise/FirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/FirewallPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

    .line 3159
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicyCreated:Z

    .line 3163
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3167
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mFirewallPolicy:Landroid/app/enterprise/FirewallPolicy;

    return-object v0

    .line 3163
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getGenericSSO()Landroid/app/enterprise/sso/GenericSSO;
    .locals 3

    .prologue
    .line 3983
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSOPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3984
    const-class v1, Landroid/app/enterprise/sso/GenericSSO;

    monitor-enter v1

    .line 3985
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSOPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3986
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/app/enterprise/sso/GenericSSO;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/sso/GenericSSO;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

    .line 3988
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSOPolicyCreated:Z

    .line 3990
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3992
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mGenericSSO:Landroid/app/enterprise/sso/GenericSSO;

    return-object v0

    .line 3990
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getLDAPAccountPolicy()Landroid/app/enterprise/LDAPAccountPolicy;
    .locals 4

    .prologue
    .line 3612
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3614
    const-class v1, Landroid/app/enterprise/LDAPAccountPolicy;

    monitor-enter v1

    .line 3616
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3618
    new-instance v0, Landroid/app/enterprise/LDAPAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/LDAPAccountPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

    .line 3620
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicyCreated:Z

    .line 3624
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3628
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLDAPAccountPolicy:Landroid/app/enterprise/LDAPAccountPolicy;

    return-object v0

    .line 3624
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getLocationPolicy()Landroid/app/enterprise/LocationPolicy;
    .locals 3

    .prologue
    .line 3077
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3079
    const-class v1, Landroid/app/enterprise/LocationPolicy;

    monitor-enter v1

    .line 3081
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3083
    new-instance v0, Landroid/app/enterprise/LocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/LocationPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

    .line 3085
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicyCreated:Z

    .line 3089
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3093
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mLocationPolicy:Landroid/app/enterprise/LocationPolicy;

    return-object v0

    .line 3089
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getMaximumFailedPasswordsForWipe()I
    .locals 2

    .prologue
    .line 845
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getMaximumTimeToLock()J
    .locals 2

    .prologue
    .line 911
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getMaximumTimeToLock(Landroid/content/ComponentName;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMinPasswordComplexChars(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 1191
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getMinPasswordComplexChars(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getMiscPolicy()Landroid/app/enterprise/MiscPolicy;
    .locals 4

    .prologue
    .line 2743
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2745
    const-class v1, Landroid/app/enterprise/MiscPolicy;

    monitor-enter v1

    .line 2747
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2749
    new-instance v0, Landroid/app/enterprise/MiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/MiscPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

    .line 2751
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicyCreated:Z

    .line 2755
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2759
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mMiscPolicy:Landroid/app/enterprise/MiscPolicy;

    return-object v0

    .line 2755
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getMyKnoxAdmin()Ljava/lang/String;
    .locals 3

    .prologue
    .line 4169
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4171
    :try_start_0
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "getMyKnoxAdmin"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4172
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getMyKnoxAdmin(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4178
    :goto_0
    return-object v1

    .line 4173
    :catch_0
    move-exception v0

    .line 4174
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4178
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPassword(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 1216
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getPassword(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPasswordExpires(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 1029
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getPasswordExpires(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordHistory(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 1109
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getPasswordHistory(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordMaximumLength(I)I
    .locals 1
    .param p1, "quality"    # I

    .prologue
    .line 787
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMaximumLength(I)I

    move-result v0

    return v0
.end method

.method public getPasswordMinimumLength()I
    .locals 2

    .prologue
    .line 772
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordMinimumLength(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordPolicy()Landroid/app/enterprise/PasswordPolicy;
    .locals 4

    .prologue
    .line 2891
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2893
    const-class v1, Landroid/app/enterprise/PasswordPolicy;

    monitor-enter v1

    .line 2895
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2897
    new-instance v0, Landroid/app/enterprise/PasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/PasswordPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

    .line 2899
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicyCreated:Z

    .line 2903
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2907
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPasswordPolicy:Landroid/app/enterprise/PasswordPolicy;

    return-object v0

    .line 2903
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPasswordQuality()I
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getPasswordQuality(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPhoneRestrictionPolicy()Landroid/app/enterprise/PhoneRestrictionPolicy;
    .locals 3

    .prologue
    .line 3299
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicyCreated:Z

    if-nez v0, :cond_1

    .line 3301
    const-class v1, Landroid/app/enterprise/PhoneRestrictionPolicy;

    monitor-enter v1

    .line 3303
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicyCreated:Z

    if-nez v0, :cond_0

    .line 3305
    new-instance v0, Landroid/app/enterprise/PhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/PhoneRestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicy:Landroid/app/enterprise/PhoneRestrictionPolicy;

    .line 3307
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicyCreated:Z

    .line 3311
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3315
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mPhonePolicy:Landroid/app/enterprise/PhoneRestrictionPolicy;

    return-object v0

    .line 3311
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getProxyAdmins(I)Ljava/util/List;
    .locals 3
    .param p1, "userHandle"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/admin/ProxyDeviceAdminInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3780
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3784
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getProxyAdmins(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3794
    :goto_0
    return-object v1

    .line 3786
    :catch_0
    move-exception v0

    .line 3788
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3794
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method public getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "result"    # Landroid/os/RemoteCallback;

    .prologue
    .line 1516
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1520
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1530
    :cond_0
    :goto_0
    return-void

    .line 1522
    :catch_0
    move-exception v0

    .line 1524
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;
    .locals 4

    .prologue
    .line 2780
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2782
    const-class v1, Landroid/app/enterprise/RestrictionPolicy;

    monitor-enter v1

    .line 2784
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2786
    new-instance v0, Landroid/app/enterprise/RestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/RestrictionPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

    .line 2788
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicyCreated:Z

    .line 2792
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2796
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRestrictionPolicy:Landroid/app/enterprise/RestrictionPolicy;

    return-object v0

    .line 2792
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getRoamingPolicy()Landroid/app/enterprise/RoamingPolicy;
    .locals 3

    .prologue
    .line 2706
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2708
    const-class v1, Landroid/app/enterprise/RoamingPolicy;

    monitor-enter v1

    .line 2710
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2712
    new-instance v0, Landroid/app/enterprise/RoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/RoamingPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicy:Landroid/app/enterprise/RoamingPolicy;

    .line 2714
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicyCreated:Z

    .line 2718
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2722
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mRoamingPolicy:Landroid/app/enterprise/RoamingPolicy;

    return-object v0

    .line 2718
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSSOPolicy(Ljava/lang/String;)Landroid/app/enterprise/SSOPolicy;
    .locals 3
    .param p1, "solutionPackageName"    # Ljava/lang/String;

    .prologue
    .line 3953
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3955
    const-class v1, Landroid/app/enterprise/SSOPolicy;

    monitor-enter v1

    .line 3957
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3959
    new-instance v0, Landroid/app/enterprise/SSOPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2, p1}, Landroid/app/enterprise/SSOPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicy:Landroid/app/enterprise/SSOPolicy;

    .line 3961
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicyCreated:Z

    .line 3965
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3969
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSSOPolicy:Landroid/app/enterprise/SSOPolicy;

    return-object v0

    .line 3965
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;
    .locals 4

    .prologue
    .line 2817
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicyCreated:Z

    if-nez v0, :cond_1

    .line 2819
    const-class v1, Landroid/app/enterprise/SecurityPolicy;

    monitor-enter v1

    .line 2821
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicyCreated:Z

    if-nez v0, :cond_0

    .line 2823
    new-instance v0, Landroid/app/enterprise/SecurityPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/SecurityPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

    .line 2825
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicyCreated:Z

    .line 2829
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2833
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mSecurityPolicy:Landroid/app/enterprise/SecurityPolicy;

    return-object v0

    .line 2829
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getVpnPolicy()Landroid/app/enterprise/VpnPolicy;
    .locals 3

    .prologue
    .line 3040
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3042
    const-class v1, Landroid/app/enterprise/VpnPolicy;

    monitor-enter v1

    .line 3044
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3046
    new-instance v0, Landroid/app/enterprise/VpnPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/VpnPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicy:Landroid/app/enterprise/VpnPolicy;

    .line 3048
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicyCreated:Z

    .line 3052
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3056
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mVpnPolicy:Landroid/app/enterprise/VpnPolicy;

    return-object v0

    .line 3052
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getWifiPolicy()Landroid/app/enterprise/WifiPolicy;
    .locals 3

    .prologue
    .line 3114
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicyCreated:Z

    if-nez v0, :cond_1

    .line 3116
    const-class v1, Landroid/app/enterprise/WifiPolicy;

    monitor-enter v1

    .line 3118
    :try_start_0
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicyCreated:Z

    if-nez v0, :cond_0

    .line 3120
    new-instance v0, Landroid/app/enterprise/WifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-direct {v0, v2}, Landroid/app/enterprise/WifiPolicy;-><init>(Landroid/app/enterprise/ContextInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

    .line 3122
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicyCreated:Z

    .line 3126
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3130
    :cond_1
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mWifiPolicy:Landroid/app/enterprise/WifiPolicy;

    return-object v0

    .line 3126
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public hasAnyActiveAdmin()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3446
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v2

    if-nez v2, :cond_0

    .line 3448
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v3, "No EnterpriseDeviceManager service"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3462
    :goto_0
    return v1

    .line 3456
    :cond_0
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->hasAnyActiveAdmin()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 3458
    :catch_0
    move-exception v0

    .line 3460
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to get hasAnyActiveAdmin"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public hasGrantedPolicy(Landroid/content/ComponentName;I)Z
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "usesPolicy"    # I

    .prologue
    .line 1413
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1417
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->hasGrantedPolicy(Landroid/content/ComponentName;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1427
    :goto_0
    return v1

    .line 1419
    :catch_0
    move-exception v0

    .line 1421
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1427
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isActivePasswordSufficient()Z
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->isActivePasswordSufficient()Z

    move-result v0

    return v0
.end method

.method public isAdminActive(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "who"    # Landroid/content/ComponentName;

    .prologue
    .line 1267
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1271
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isAdminActive(Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1281
    :goto_0
    return v1

    .line 1273
    :catch_0
    move-exception v0

    .line 1275
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1281
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAdminRemovable(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;

    .prologue
    .line 3531
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3535
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isAdminRemovable(Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3545
    :goto_0
    return v1

    .line 3537
    :catch_0
    move-exception v0

    .line 3539
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3545
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isMigrationStateNOK()Z
    .locals 4

    .prologue
    .line 4071
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4075
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isMigrationStateNOK()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4089
    :goto_0
    return v1

    .line 4077
    :catch_0
    move-exception v0

    .line 4079
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4089
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4081
    :catch_1
    move-exception v0

    .line 4083
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public isRestrictedByConstrainedState(I)Z
    .locals 4
    .param p1, "PolicyBitMask"    # I

    .prologue
    .line 4123
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4125
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->isRestrictedByConstrainedState(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4132
    :goto_0
    return v1

    .line 4126
    :catch_0
    move-exception v0

    .line 4127
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4132
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4128
    :catch_1
    move-exception v0

    .line 4129
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public lockNow()V
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V

    .line 929
    return-void
.end method

.method public migrateApplicationDisablePolicy(I)Z
    .locals 4
    .param p1, "newContainerId"    # I

    .prologue
    .line 4047
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4051
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->migrateApplicationDisablePolicy(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4065
    :goto_0
    return v1

    .line 4053
    :catch_0
    move-exception v0

    .line 4055
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4065
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4057
    :catch_1
    move-exception v0

    .line 4059
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public migrateEnterpriseContainer(IZ)Z
    .locals 4
    .param p1, "newContainerId"    # I
    .param p2, "isB2B"    # Z

    .prologue
    .line 3999
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4003
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->migrateEnterpriseContainer(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 4017
    :goto_0
    return v1

    .line 4005
    :catch_0
    move-exception v0

    .line 4007
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4017
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 4009
    :catch_1
    move-exception v0

    .line 4011
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security exception occured! "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public packageHasActiveAdmins(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 3477
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3481
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->packageHasActiveAdmins(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3491
    :goto_0
    return v1

    .line 3483
    :catch_0
    move-exception v0

    .line 3485
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3491
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public packageHasActiveAdminsAsUser(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userID"    # I

    .prologue
    .line 3503
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3507
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->packageHasActiveAdminsAsUser(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3517
    :goto_0
    return v1

    .line 3509
    :catch_0
    move-exception v0

    .line 3511
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3517
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public reboot(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 1239
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EnterpriseDeviceManager.reboot"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1241
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->reboot(Ljava/lang/String;)V

    .line 1243
    return-void
.end method

.method public removeActiveAdmin(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;

    .prologue
    .line 1462
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1466
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->removeActiveAdmin(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1476
    :cond_0
    :goto_0
    return-void

    .line 1468
    :catch_0
    move-exception v0

    .line 1470
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeActiveAdminFromDpm(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;

    .prologue
    .line 1489
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1493
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v2

    invoke-interface {v1, p1, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->removeActiveAdminFromDpm(Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1503
    :cond_0
    :goto_0
    return-void

    .line 1495
    :catch_0
    move-exception v0

    .line 1497
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeProxyAdmin(I)V
    .locals 3
    .param p1, "proxyUid"    # I

    .prologue
    .line 3808
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3812
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->removeProxyAdmin(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3822
    :cond_0
    :goto_0
    return-void

    .line 3814
    :catch_0
    move-exception v0

    .line 3816
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public resetPassword(Ljava/lang/String;I)Z
    .locals 1
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "flags"    # I

    .prologue
    .line 870
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/admin/DevicePolicyManager;->resetPassword(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public selfUpdateAdmin(Ljava/lang/String;)I
    .locals 4
    .param p1, "apkFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, -0x3

    .line 2007
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2011
    if-nez p1, :cond_0

    .line 2047
    :goto_0
    return v1

    .line 2017
    :cond_0
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContext:Landroid/content/Context;

    invoke-static {v2, p1}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 2021
    if-nez p1, :cond_1

    .line 2023
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v3, "Failed to copy file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2033
    :cond_1
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->selfUpdateAdmin(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 2035
    :catch_0
    move-exception v0

    .line 2037
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2043
    invoke-static {p1}, Landroid/app/enterprise/lso/LSOUtils;->deleteFile(Ljava/lang/String;)V

    .line 2047
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setActiveAdmin(Landroid/content/ComponentName;Z)V
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;
    .param p2, "refreshing"    # Z

    .prologue
    .line 1366
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1370
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setActiveAdmin(Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1380
    :cond_0
    :goto_0
    return-void

    .line 1372
    :catch_0
    move-exception v0

    .line 1374
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setActiveAdminSilent(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "adminReceiver"    # Landroid/content/ComponentName;

    .prologue
    .line 3685
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3689
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setActiveAdminSilent(Landroid/content/ComponentName;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3699
    :cond_0
    :goto_0
    return-void

    .line 3691
    :catch_0
    move-exception v0

    .line 3693
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setAdminRemovable(Z)Z
    .locals 4
    .param p1, "removable"    # Z

    .prologue
    .line 1775
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1779
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseDeviceManager.setAdminRemovable"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1781
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setAdminRemovable(Landroid/app/enterprise/ContextInfo;ZLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 1797
    :goto_0
    return v1

    .line 1783
    :catch_0
    move-exception v0

    .line 1785
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1797
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1787
    :catch_1
    move-exception v0

    .line 1789
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Can NOT Found PackageName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1791
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Can NOT Find PackageName"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setAdminRemovable(ZLjava/lang/String;)Z
    .locals 3
    .param p1, "removable"    # Z
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1742
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1746
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setAdminRemovable(Landroid/app/enterprise/ContextInfo;ZLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    .line 1762
    :goto_0
    return v1

    .line 1748
    :catch_0
    move-exception v0

    .line 1750
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1762
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1752
    :catch_1
    move-exception v0

    .line 1754
    .local v0, "e":Ljava/lang/SecurityException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Can NOT Found PackageName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1756
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Can NOT Find PackageName"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setMaximumFailedPasswordsForWipe(I)V
    .locals 2
    .param p1, "num"    # I

    .prologue
    .line 826
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getActiveAdminComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/app/admin/DevicePolicyManager;->setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)V

    .line 828
    return-void
.end method

.method public setMaximumTimeToLock(J)V
    .locals 3
    .param p1, "timeMs"    # J

    .prologue
    .line 889
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getActiveAdminComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Landroid/app/admin/DevicePolicyManager;->setMaximumTimeToLock(Landroid/content/ComponentName;J)V

    .line 891
    return-void
.end method

.method public setMinPasswordComplexChars(Landroid/content/ComponentName;I)V
    .locals 2
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "size"    # I

    .prologue
    .line 1158
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EnterpriseDeviceManager.setMinPasswordComplexChars"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1160
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/admin/DevicePolicyManager;->setMinPasswordComplexChars(Landroid/content/ComponentName;I)V

    .line 1162
    return-void
.end method

.method public setMyKnoxAdmin(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 4153
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4155
    :try_start_0
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "setMyKnoxAdmin"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4156
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEnterpriseDeviceManager;->setMyKnoxAdmin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4162
    :goto_0
    return v1

    .line 4157
    :catch_0
    move-exception v0

    .line 4158
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4162
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPasswordExpires(Landroid/content/ComponentName;I)V
    .locals 2
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 996
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EnterpriseDeviceManager.setPasswordExpires"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 998
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/admin/DevicePolicyManager;->setPasswordExpires(Landroid/content/ComponentName;I)V

    .line 1000
    return-void
.end method

.method public setPasswordHistory(Landroid/content/ComponentName;I)V
    .locals 2
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 1076
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EnterpriseDeviceManager.setPasswordHistory"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1078
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1, p2}, Landroid/app/admin/DevicePolicyManager;->setPasswordHistory(Landroid/content/ComponentName;I)V

    .line 1080
    return-void
.end method

.method public setPasswordMinimumLength(I)V
    .locals 2
    .param p1, "length"    # I

    .prologue
    .line 753
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getActiveAdminComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/app/admin/DevicePolicyManager;->setPasswordMinimumLength(Landroid/content/ComponentName;I)V

    .line 755
    return-void
.end method

.method public setPasswordQuality(I)V
    .locals 2
    .param p1, "quality"    # I

    .prologue
    .line 720
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getActiveAdminComponent()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/app/admin/DevicePolicyManager;->setPasswordQuality(Landroid/content/ComponentName;I)V

    .line 722
    return-void
.end method

.method public updateProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V
    .locals 3
    .param p1, "proxyAdmin"    # Landroid/app/admin/ProxyDeviceAdminInfo;
    .param p2, "proxyUid"    # I
    .param p3, "adminComponentName"    # Landroid/content/ComponentName;
    .param p4, "adminUid"    # I

    .prologue
    .line 3719
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getService()Landroid/app/enterprise/IEnterpriseDeviceManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3723
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mService:Landroid/app/enterprise/IEnterpriseDeviceManager;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/enterprise/IEnterpriseDeviceManager;->updateProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3735
    :cond_0
    :goto_0
    return-void

    .line 3727
    :catch_0
    move-exception v0

    .line 3729
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with enterprise policy service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public wipeData(I)V
    .locals 1
    .param p1, "flags"    # I

    .prologue
    .line 949
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceManager;->mDPM:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->wipeData(I)V

    .line 951
    return-void
.end method
