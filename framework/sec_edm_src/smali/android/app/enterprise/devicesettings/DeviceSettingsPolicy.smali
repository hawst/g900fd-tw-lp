.class public Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
.super Ljava/lang/Object;
.source "DeviceSettingsPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mContainerId:I

.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IMiscPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mSync:Ljava/lang/Object;

    .line 61
    const-string v0, "DeviceSettingsPolicy"

    sput-object v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    iput-object p2, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContext:Landroid/content/Context;

    .line 72
    iput-object p1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 73
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    new-instance v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    sget-object v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 115
    new-instance v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    .line 117
    :cond_0
    sget-object v0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    monitor-exit v1

    return-object v0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    sget-object v2, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 101
    :try_start_0
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    if-nez v1, :cond_0

    if-eqz p0, :cond_0

    .line 102
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 103
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    .line 105
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mDeviceSettingsPolicy:Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;

    monitor-exit v2

    return-object v1

    .line 106
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Landroid/app/enterprise/IMiscPolicy;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    if-nez v0, :cond_0

    .line 77
    const-string v0, "misc_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IMiscPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IMiscPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    .line 80
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    return-object v0
.end method


# virtual methods
.method public allowNFCStateChange(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 165
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceSettingsPolicy.allowNFCStateChange"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 166
    invoke-direct {p0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 168
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IMiscPolicy;->allowNFCStateChange(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 173
    :goto_0
    return v1

    .line 169
    :catch_0
    move-exception v0

    .line 170
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with MiscPolicy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 173
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNFCStarted()Z
    .locals 3

    .prologue
    .line 298
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceSettingsPolicy.isNFCStarted"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 299
    invoke-direct {p0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 301
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IMiscPolicy;->isNFCStarted()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 306
    :goto_0
    return v1

    .line 302
    :catch_0
    move-exception v0

    .line 303
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with MiscPolicy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 306
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNFCStateChangeAllowed()Z
    .locals 3

    .prologue
    .line 208
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceSettingsPolicy.isNFCStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 209
    invoke-direct {p0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 211
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IMiscPolicy;->isNFCStateChangeAllowed()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 216
    :goto_0
    return v1

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with MiscPolicy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public startNFC(Z)Z
    .locals 3
    .param p1, "start"    # Z

    .prologue
    .line 255
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceSettingsPolicy.startNFC"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 256
    invoke-direct {p0}, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 258
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IMiscPolicy;->startNFC(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 263
    :goto_0
    return v1

    .line 259
    :catch_0
    move-exception v0

    .line 260
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/devicesettings/DeviceSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with MiscPolicy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 263
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
