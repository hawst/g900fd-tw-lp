.class public Landroid/app/enterprise/DeviceInventory;
.super Ljava/lang/Object;
.source "DeviceInventory.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "DeviceInventory"

    sput-object v0, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "cxt"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 77
    iput-object p2, p0, Landroid/app/enterprise/DeviceInventory;->mContext:Landroid/content/Context;

    .line 78
    return-void
.end method

.method private externalSdCardAvailable()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 244
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getExternalSdCardDirectory()Ljava/lang/String;

    move-result-object v0

    .line 245
    .local v0, "externalSdCardPath":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 253
    :cond_0
    :goto_0
    return v3

    .line 248
    :cond_1
    iget-object v4, p0, Landroid/app/enterprise/DeviceInventory;->mContext:Landroid/content/Context;

    const-string v5, "storage"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 249
    .local v1, "sm":Landroid/os/storage/StorageManager;
    invoke-virtual {v1, v0}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "volState":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 253
    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_0
.end method

.method private getAvailableMemorySize(Landroid/os/StatFs;)J
    .locals 8
    .param p1, "stat"    # Landroid/os/StatFs;

    .prologue
    .line 176
    const-wide/16 v2, 0x0

    .line 177
    .local v2, "sdSize":J
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 179
    invoke-virtual {p1}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v6

    mul-long v0, v4, v6

    .line 181
    .local v0, "sdAvailSize":J
    move-wide v2, v0

    .line 189
    .end local v0    # "sdAvailSize":J
    :goto_0
    return-wide v2

    .line 184
    :cond_0
    invoke-virtual {p1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-double v4, v4

    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-double v6, v6

    mul-double v0, v4, v6

    .line 186
    .local v0, "sdAvailSize":D
    double-to-long v2, v0

    goto :goto_0
.end method

.method private getExternalSdCardDirectory()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 224
    iget-object v5, p0, Landroid/app/enterprise/DeviceInventory;->mContext:Landroid/content/Context;

    const-string v6, "storage"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 225
    .local v1, "sm":Landroid/os/storage/StorageManager;
    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    .line 226
    .local v3, "storageVolumes":[Landroid/os/storage/StorageVolume;
    if-nez v3, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-object v4

    .line 229
    :cond_1
    array-length v0, v3

    .line 233
    .local v0, "length":I
    if-le v0, v7, :cond_0

    aget-object v5, v3, v7

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 234
    aget-object v2, v3, v7

    .line 235
    .local v2, "storageVolume":Landroid/os/storage/StorageVolume;
    sget-object v4, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Subsystem : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getSubSystem()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    sget-object v4, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    invoke-virtual {v2}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private getInternalSdCardPath()Ljava/lang/String;
    .locals 5

    .prologue
    .line 257
    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContext:Landroid/content/Context;

    const-string v4, "storage"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    .line 258
    .local v1, "sm":Landroid/os/storage/StorageManager;
    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v2

    .line 259
    .local v2, "storageVolumes":[Landroid/os/storage/StorageVolume;
    array-length v0, v2

    .line 263
    .local v0, "length":I
    if-lez v0, :cond_0

    .line 264
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 266
    :goto_0
    return-object v3

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private getService()Landroid/app/enterprise/IDeviceInfo;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    if-nez v0, :cond_0

    .line 82
    const-string v0, "device_info"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IDeviceInfo$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IDeviceInfo;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    .line 85
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    return-object v0
.end method

.method private getTotalMemorySize(Landroid/os/StatFs;)J
    .locals 8
    .param p1, "stat"    # Landroid/os/StatFs;

    .prologue
    .line 159
    const-wide/16 v2, 0x0

    .line 160
    .local v2, "sdSize":J
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_0

    .line 162
    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v6

    mul-long v0, v4, v6

    .line 164
    .local v0, "sdAvailSize":J
    move-wide v2, v0

    .line 172
    .end local v0    # "sdAvailSize":J
    :goto_0
    return-wide v2

    .line 167
    :cond_0
    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v4

    int-to-double v4, v4

    invoke-virtual {p1}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-double v6, v6

    mul-double v0, v4, v6

    .line 169
    .local v0, "sdAvailSize":D
    double-to-long v2, v0

    goto :goto_0
.end method


# virtual methods
.method public clearCallingLog()Z
    .locals 3

    .prologue
    .line 1014
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.clearCallingLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1015
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1017
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->clearCallingLog(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1022
    :goto_0
    return v1

    .line 1018
    :catch_0
    move-exception v0

    .line 1019
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1022
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearMMSLog()Z
    .locals 3

    .prologue
    .line 1900
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1902
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->clearMMSLog(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1907
    :goto_0
    return v1

    .line 1903
    :catch_0
    move-exception v0

    .line 1904
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1907
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearSMSLog()Z
    .locals 3

    .prologue
    .line 860
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.clearSMSLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 861
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 863
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->clearSMSLog(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 868
    :goto_0
    return v1

    .line 864
    :catch_0
    move-exception v0

    .line 865
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 868
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public dataUsageTimerActivation()V
    .locals 3

    .prologue
    .line 1300
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1302
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->dataUsageTimerActivation(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1307
    :cond_0
    :goto_0
    return-void

    .line 1303
    :catch_0
    move-exception v0

    .line 1304
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public enableCallingCapture(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 888
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.enableCallingCapture"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 889
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 891
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->enableCallingCapture(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 896
    :goto_0
    return v1

    .line 892
    :catch_0
    move-exception v0

    .line 893
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 896
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableMMSCapture(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1647
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1649
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->enableMMSCapture(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1654
    :goto_0
    return v1

    .line 1650
    :catch_0
    move-exception v0

    .line 1651
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1654
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableSMSCapture(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 740
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.enableSMSCapture"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 741
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 743
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->enableSMSCapture(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 748
    :goto_0
    return v1

    .line 744
    :catch_0
    move-exception v0

    .line 745
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 748
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAvailableCapacityExternal()J
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 209
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->externalSdCardAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 210
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getExternalSdCardDirectory()Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 220
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v4

    .line 214
    .restart local v1    # "path":Ljava/lang/String;
    :cond_1
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 215
    .local v2, "stat":Landroid/os/StatFs;
    invoke-direct {p0, v2}, Landroid/app/enterprise/DeviceInventory;->getAvailableMemorySize(Landroid/os/StatFs;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 217
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAvailableCapacityExternal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableCapacityInternal()J
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 315
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getInternalSdCardPath()Ljava/lang/String;

    move-result-object v1

    .line 316
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 324
    .end local v1    # "path":Ljava/lang/String;
    :goto_0
    return-wide v4

    .line 319
    .restart local v1    # "path":Ljava/lang/String;
    :cond_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 320
    .local v2, "stat":Landroid/os/StatFs;
    invoke-direct {p0, v2}, Landroid/app/enterprise/DeviceInventory;->getAvailableMemorySize(Landroid/os/StatFs;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 321
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getAvailableCapacityInternal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAvailableRamMemory()J
    .locals 4

    .prologue
    .line 712
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 714
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getAvailableRamMemory(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 719
    :goto_0
    return-wide v2

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 719
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getBytesReceivedNetwork()J
    .locals 4

    .prologue
    .line 1208
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getBytesReceivedNetwork"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1209
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1211
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getBytesReceivedNetwork(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1216
    :goto_0
    return-wide v2

    .line 1212
    :catch_0
    move-exception v0

    .line 1213
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1216
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getBytesReceivedWiFi()J
    .locals 4

    .prologue
    .line 1162
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getBytesReceivedWiFi"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1163
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1165
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getBytesReceivedWiFi(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1170
    :goto_0
    return-wide v2

    .line 1166
    :catch_0
    move-exception v0

    .line 1167
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1170
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getBytesSentNetwork()J
    .locals 4

    .prologue
    .line 1185
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getBytesSentNetwork"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1186
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1188
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getBytesSentNetwork(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1193
    :goto_0
    return-wide v2

    .line 1189
    :catch_0
    move-exception v0

    .line 1190
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1193
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getBytesSentWiFi()J
    .locals 4

    .prologue
    .line 1139
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getBytesSentWiFi"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1140
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1142
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getBytesSentWiFi(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1147
    :goto_0
    return-wide v2

    .line 1143
    :catch_0
    move-exception v0

    .line 1144
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1147
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getCellTowerCID()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1458
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getCellTowerCID"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1459
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1461
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getCellTowerCID(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1466
    :goto_0
    return-object v1

    .line 1462
    :catch_0
    move-exception v0

    .line 1463
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1466
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCellTowerLAC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1490
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getCellTowerLAC"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1491
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1493
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getCellTowerLAC(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1498
    :goto_0
    return-object v1

    .line 1494
    :catch_0
    move-exception v0

    .line 1495
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1498
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCellTowerPSC()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1522
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getCellTowerPSC"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1523
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1525
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getCellTowerPSC(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1530
    :goto_0
    return-object v1

    .line 1526
    :catch_0
    move-exception v0

    .line 1527
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1530
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCellTowerRSSI()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1554
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getCellTowerRSSI"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1555
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1557
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getCellTowerRSSI(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1562
    :goto_0
    return-object v1

    .line 1558
    :catch_0
    move-exception v0

    .line 1559
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1562
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/16 v1, 0x63

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getDataCallLog(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "time"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceInventory.getDataCallLog"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1422
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1423
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1425
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IDeviceInfo;->getDataCallLog(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1430
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 1426
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 1427
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with phone restriction policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getDataCallLoggingEnabled()Z
    .locals 3

    .prologue
    .line 1361
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getDataCallLoggingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1362
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1364
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDataCallLoggingEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1369
    :goto_0
    return v1

    .line 1365
    :catch_0
    move-exception v0

    .line 1366
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1369
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDataCallStatisticsEnabled()Z
    .locals 3

    .prologue
    .line 1117
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1119
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDataCallStatisticsEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1124
    :goto_0
    return v1

    .line 1120
    :catch_0
    move-exception v0

    .line 1121
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed to getDataCallStatisticsEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDataUsageTimer()I
    .locals 3

    .prologue
    .line 1282
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1284
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDataUsageTimer(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1289
    :goto_0
    return v1

    .line 1285
    :catch_0
    move-exception v0

    .line 1286
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1289
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceMaker()Ljava/lang/String;
    .locals 3

    .prologue
    .line 491
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 493
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceMaker(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 498
    :goto_0
    return-object v1

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 498
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 374
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 376
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 381
    :goto_0
    return-object v1

    .line 377
    :catch_0
    move-exception v0

    .line 378
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 381
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceOS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 510
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 512
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceOS(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 517
    :goto_0
    return-object v1

    .line 513
    :catch_0
    move-exception v0

    .line 514
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 517
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceOSVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 529
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 531
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceOSVersion(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 536
    :goto_0
    return-object v1

    .line 532
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 536
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDevicePlatform()Ljava/lang/String;
    .locals 3

    .prologue
    .line 548
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 550
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDevicePlatform(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 555
    :goto_0
    return-object v1

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 555
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceProcessorSpeed()Ljava/lang/String;
    .locals 3

    .prologue
    .line 587
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 589
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceProcessorSpeed(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 594
    :goto_0
    return-object v1

    .line 590
    :catch_0
    move-exception v0

    .line 591
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 594
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceProcessorType()Ljava/lang/String;
    .locals 3

    .prologue
    .line 568
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 570
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDeviceProcessorType(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 575
    :goto_0
    return-object v1

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDroppedCallsCount()I
    .locals 3

    .prologue
    .line 606
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getDroppedCallsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 607
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    if-eqz v1, :cond_0

    .line 609
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getDroppedCallsCount(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 614
    :goto_0
    return v1

    .line 610
    :catch_0
    move-exception v0

    .line 611
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 614
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getInboundMMSCaptured()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1822
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1824
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getInboundMMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1829
    :goto_0
    return-object v1

    .line 1825
    :catch_0
    move-exception v0

    .line 1826
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1829
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInboundSMSCaptured()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 812
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceInventory.getInboundSMSCaptured"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 813
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 814
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 816
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IDeviceInfo;->getInboundSMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 821
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 817
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 818
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with device info policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getIncomingCallingCaptured()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 990
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceInventory.getIncomingCallingCaptured"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 991
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 992
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 994
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IDeviceInfo;->getIncomingCallingCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 999
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 995
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 996
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with device inventory policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getMissedCallsCount()I
    .locals 3

    .prologue
    .line 625
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getMissedCallsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 626
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 628
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getMissedCallsCount(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 633
    :goto_0
    return v1

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 633
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getModelName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 336
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 338
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getModelName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 343
    :goto_0
    return-object v1

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 343
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getModelNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 355
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 357
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getModelNumber(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 362
    :goto_0
    return-object v1

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getModemFirmware()Ljava/lang/String;
    .locals 3

    .prologue
    .line 434
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 436
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getModemFirmware(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 441
    :goto_0
    return-object v1

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 441
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOutboundMMSCaptured()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1758
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1760
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getOutboundMMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1765
    :goto_0
    return-object v1

    .line 1761
    :catch_0
    move-exception v0

    .line 1762
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1765
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOutboundSMSCaptured()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 785
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceInventory.getOutboundSMSCaptured"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 786
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 787
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 789
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IDeviceInfo;->getOutboundSMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 794
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 790
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 791
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with device info policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getOutgoingCallingCaptured()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 963
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceInventory.getOutgoingCallingCaptured"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 964
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 965
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 967
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v3, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IDeviceInfo;->getOutgoingCallingCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 972
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 968
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 969
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with device inventory policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPlatformSDK()I
    .locals 3

    .prologue
    .line 453
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 455
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getPlatformSDK(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 460
    :goto_0
    return v1

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 460
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPlatformVersion()Ljava/lang/String;
    .locals 3

    .prologue
    .line 472
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 474
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getPlatformVersion(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 479
    :goto_0
    return-object v1

    .line 475
    :catch_0
    move-exception v0

    .line 476
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 479
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPlatformVersionName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 415
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 417
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getPlatformVersionName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 422
    :goto_0
    return-object v1

    .line 418
    :catch_0
    move-exception v0

    .line 419
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 422
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSerialNumber()Ljava/lang/String;
    .locals 3

    .prologue
    .line 396
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 398
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getSerialNumber(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 403
    :goto_0
    return-object v1

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 403
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSuccessCallsCount()I
    .locals 3

    .prologue
    .line 644
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.getSuccessCallsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 645
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 647
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getSuccessCallsCount(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 652
    :goto_0
    return v1

    .line 648
    :catch_0
    move-exception v0

    .line 649
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 652
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getTotalCapacityExternal()J
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 144
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->externalSdCardAvailable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getExternalSdCardDirectory()Ljava/lang/String;

    move-result-object v1

    .line 146
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 155
    .end local v1    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-wide v4

    .line 149
    .restart local v1    # "path":Ljava/lang/String;
    :cond_1
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 150
    .local v2, "stat":Landroid/os/StatFs;
    invoke-direct {p0, v2}, Landroid/app/enterprise/DeviceInventory;->getTotalMemorySize(Landroid/os/StatFs;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 152
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getTotalCapacityExternal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getTotalCapacityInternal()J
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    .line 286
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getInternalSdCardPath()Ljava/lang/String;

    move-result-object v1

    .line 287
    .local v1, "path":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 295
    .end local v1    # "path":Ljava/lang/String;
    :goto_0
    return-wide v4

    .line 290
    .restart local v1    # "path":Ljava/lang/String;
    :cond_0
    new-instance v2, Landroid/os/StatFs;

    invoke-direct {v2, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 291
    .local v2, "stat":Landroid/os/StatFs;
    invoke-direct {p0, v2}, Landroid/app/enterprise/DeviceInventory;->getTotalMemorySize(Landroid/os/StatFs;)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    goto :goto_0

    .line 292
    .end local v1    # "path":Ljava/lang/String;
    .end local v2    # "stat":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getTotalCapacityInternal"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getTotalRamMemory()J
    .locals 4

    .prologue
    .line 688
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 690
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getTotalRamMemory(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 695
    :goto_0
    return-wide v2

    .line 691
    :catch_0
    move-exception v0

    .line 692
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 695
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getWifiStatisticEnabled()Z
    .locals 3

    .prologue
    .line 1063
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1065
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->getWifiStatisticEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1070
    :goto_0
    return v1

    .line 1066
    :catch_0
    move-exception v0

    .line 1067
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1070
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCallingCaptureEnabled()Z
    .locals 3

    .prologue
    .line 907
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.isCallingCaptureEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 908
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 910
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isCallingCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 915
    :goto_0
    return v1

    .line 911
    :catch_0
    move-exception v0

    .line 912
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 915
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeviceLocked()Z
    .locals 3

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 118
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isDeviceLocked(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 123
    :goto_0
    return v1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeviceRooted()Z
    .locals 3

    .prologue
    .line 1588
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.isDeviceRooted"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1589
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1591
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isDeviceRooted(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1596
    :goto_0
    return v1

    .line 1592
    :catch_0
    move-exception v0

    .line 1593
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1596
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeviceSecure()Z
    .locals 3

    .prologue
    .line 96
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.isDeviceSecure"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 97
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 99
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isDeviceSecure(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 104
    :goto_0
    return v1

    .line 100
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 104
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMMSCaptureEnabled()Z
    .locals 3

    .prologue
    .line 1694
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1696
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isMMSCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1701
    :goto_0
    return v1

    .line 1697
    :catch_0
    move-exception v0

    .line 1698
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1701
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSMSCaptureEnabled()Z
    .locals 3

    .prologue
    .line 759
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.isSMSCaptureEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 760
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 762
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->isSMSCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 767
    :goto_0
    return v1

    .line 763
    :catch_0
    move-exception v0

    .line 764
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 767
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetCallsCount()Z
    .locals 3

    .prologue
    .line 663
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.resetCallsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 664
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 666
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->resetCallsCount(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 671
    :goto_0
    return v1

    .line 667
    :catch_0
    move-exception v0

    .line 668
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 671
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetDataCallLogging(Ljava/lang/String;)Z
    .locals 3
    .param p1, "time"    # Ljava/lang/String;

    .prologue
    .line 1391
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.resetDataCallLogging"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1392
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1394
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->resetDataCallLogging(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1399
    :goto_0
    return v1

    .line 1395
    :catch_0
    move-exception v0

    .line 1396
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed to resetDataCallLogging (String time)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1399
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetDataUsage()V
    .locals 3

    .prologue
    .line 1229
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.resetDataUsage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1230
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1232
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDeviceInfo;->resetDataUsage(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1237
    :cond_0
    :goto_0
    return-void

    .line 1233
    :catch_0
    move-exception v0

    .line 1234
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setDataCallLoggingEnabled(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1332
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.setDataCallLoggingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1333
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1335
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->setDataCallLoggingEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1340
    :goto_0
    return v1

    .line 1336
    :catch_0
    move-exception v0

    .line 1337
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device inventory policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1340
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDataCallStatisticsEnabled(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1091
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.setDataCallStatisticsEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1092
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1094
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->setDataCallStatisticsEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1099
    :goto_0
    return v1

    .line 1095
    :catch_0
    move-exception v0

    .line 1096
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed to setDataCallLoggingStatisticsEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1099
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDataUsageTimer(I)Z
    .locals 3
    .param p1, "seconds"    # I

    .prologue
    .line 1258
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.setDataUsageTimer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1259
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1261
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->setDataUsageTimer(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1266
    :goto_0
    return v1

    .line 1262
    :catch_0
    move-exception v0

    .line 1263
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1266
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifiStatisticEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1043
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceInventory.setWifiStatisticEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1044
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1046
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    iget-object v2, p0, Landroid/app/enterprise/DeviceInventory;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceInfo;->setWifiStatisticEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1051
    :goto_0
    return v1

    .line 1047
    :catch_0
    move-exception v0

    .line 1048
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1051
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public storeCalling(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 7
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/String;
    .param p3, "duration"    # Ljava/lang/String;
    .param p4, "status"    # Ljava/lang/String;
    .param p5, "isIncoming"    # Z

    .prologue
    .line 936
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 938
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IDeviceInfo;->storeCalling(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 940
    const/4 v0, 0x1

    .line 945
    :goto_0
    return v0

    .line 941
    :catch_0
    move-exception v6

    .line 942
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with device inventory policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 945
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public storeMMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "isInbound"    # Z

    .prologue
    .line 1848
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1850
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/enterprise/IDeviceInfo;->storeMMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1855
    :cond_0
    :goto_0
    return-void

    .line 1851
    :catch_0
    move-exception v0

    .line 1852
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public storeSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "timeStamp"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "isInbound"    # Z

    .prologue
    .line 839
    invoke-direct {p0}, Landroid/app/enterprise/DeviceInventory;->getService()Landroid/app/enterprise/IDeviceInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 841
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceInventory;->mService:Landroid/app/enterprise/IDeviceInfo;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/enterprise/IDeviceInfo;->storeSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 846
    :cond_0
    :goto_0
    return-void

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceInventory;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
