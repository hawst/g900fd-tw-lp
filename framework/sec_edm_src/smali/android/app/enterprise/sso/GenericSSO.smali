.class public Landroid/app/enterprise/sso/GenericSSO;
.super Ljava/lang/Object;
.source "GenericSSO.java"


# static fields
.field private static final IDP_SDK_VERSION:Ljava/lang/String; = "2.1.1"

.field private static final ISV_SDK_VERSION:Ljava/lang/String; = "2.0.1"

.field private static final LogoSizeLimit:I = 0x100000

.field private static final TAG:Ljava/lang/String; = "GenericSSO"

.field private static mGenericSSOService:Landroid/app/enterprise/sso/IGenericSSO;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/GenericSSO;->mSync:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContext:Landroid/content/Context;

    .line 89
    iput-object p1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 90
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/sso/GenericSSO;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 153
    new-instance v0, Landroid/app/enterprise/sso/GenericSSO;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/app/enterprise/sso/GenericSSO;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/sso/GenericSSO;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    sget-object v1, Landroid/app/enterprise/sso/GenericSSO;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 140
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    monitor-exit v1

    .line 142
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/app/enterprise/sso/GenericSSO;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/sso/GenericSSO;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v1

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/sso/GenericSSO;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    sget-object v2, Landroid/app/enterprise/sso/GenericSSO;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 125
    if-nez p0, :cond_0

    .line 126
    const/4 v1, 0x0

    :try_start_0
    monitor-exit v2

    .line 129
    :goto_0
    return-object v1

    .line 128
    :cond_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 129
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/sso/GenericSSO;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Landroid/app/enterprise/sso/GenericSSO;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v2

    goto :goto_0

    .line 131
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getService()Landroid/app/enterprise/sso/IGenericSSO;
    .locals 1

    .prologue
    .line 103
    sget-object v0, Landroid/app/enterprise/sso/GenericSSO;->mGenericSSOService:Landroid/app/enterprise/sso/IGenericSSO;

    if-nez v0, :cond_0

    .line 104
    const-string v0, "genericssoservice"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/sso/IGenericSSO$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/sso/GenericSSO;->mGenericSSOService:Landroid/app/enterprise/sso/IGenericSSO;

    .line 107
    :cond_0
    sget-object v0, Landroid/app/enterprise/sso/GenericSSO;->mGenericSSOService:Landroid/app/enterprise/sso/IGenericSSO;

    return-object v0
.end method


# virtual methods
.method public addAppTokenToGenericSSO(Ljava/lang/String;Landroid/app/enterprise/sso/TokenInfo;)I
    .locals 3
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "token"    # Landroid/app/enterprise/sso/TokenInfo;

    .prologue
    .line 418
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 420
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/sso/IGenericSSO;->addAppTokenToGenericSSO(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/app/enterprise/sso/TokenInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 426
    :goto_0
    return v1

    .line 422
    :catch_0
    move-exception v0

    .line 423
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in addAppTokenToGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public addUserAndDeviceCertToGenericSSO(Landroid/app/enterprise/sso/TokenInfo;)I
    .locals 3
    .param p1, "token"    # Landroid/app/enterprise/sso/TokenInfo;

    .prologue
    .line 430
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 432
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->addUserAndDeviceCertToGenericSSO(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/sso/TokenInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 438
    :goto_0
    return v1

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in addUserAndDeviceCertToGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public addWhiteListPackages(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WhiteListPackage;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 304
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WhiteListPackage;>;"
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.addWhiteListPackages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 306
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 308
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->addWhiteListPackages(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 314
    :goto_0
    return v1

    .line 310
    :catch_0
    move-exception v0

    .line 311
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in addWhiteListPackage"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public configureSSOByFile([B)I
    .locals 3
    .param p1, "configData"    # [B

    .prologue
    .line 273
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.configureSSOByFile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 275
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->configureSSOByFile(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 283
    :goto_0
    return v1

    .line 279
    :catch_0
    move-exception v0

    .line 280
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in pushKnoxSSOConfigData"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public deleteWhiteListPackages(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 332
    .local p1, "packages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.deleteWhiteListPackages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 334
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 336
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->deleteWhiteListPackages(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 342
    :goto_0
    return v1

    .line 338
    :catch_0
    move-exception v0

    .line 339
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in deleteWhiteListPackage"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 342
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public enrollSSOVendor(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)I
    .locals 7
    .param p1, "servicePackageName"    # Ljava/lang/String;
    .param p2, "servicePackageCert"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 194
    .local p3, "ssoConfig":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v5, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "GenericSSO.enrollSSOVendor"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 196
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 198
    const/4 v0, 0x0

    .line 199
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p3, :cond_2

    .line 200
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 201
    .end local v0    # "bundle":Landroid/os/Bundle;
    .local v1, "bundle":Landroid/os/Bundle;
    :try_start_1
    invoke-interface {p3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 202
    .local v4, "key":Ljava/lang/String;
    invoke-interface {p3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 207
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 208
    .end local v1    # "bundle":Landroid/os/Bundle;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .local v2, "e":Landroid/os/RemoteException;
    :goto_1
    const-string v5, "GenericSSO"

    const-string v6, "Exception in enRollSSOVendor"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v5, -0x1

    :goto_2
    return v5

    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_1
    move-object v0, v1

    .line 205
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "i$":Ljava/util/Iterator;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_2
    :try_start_2
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    iget-object v6, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1, p2, v0}, Landroid/app/enterprise/sso/IGenericSSO;->enrollSSOVendor(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v5

    goto :goto_2

    .line 207
    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public forceAuthenticate()I
    .locals 3

    .prologue
    .line 215
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.forceAuthenticate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 217
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 219
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->forceAuthenticate(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 224
    :goto_0
    return v1

    .line 220
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in forceAuthenticate"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAppTokenFromGenericSSO(Ljava/lang/String;)Landroid/app/enterprise/sso/TokenInfo;
    .locals 3
    .param p1, "appPkgName"    # Ljava/lang/String;

    .prologue
    .line 457
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 459
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->getAppTokenFromGenericSSO(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/sso/TokenInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 465
    :goto_0
    return-object v1

    .line 461
    :catch_0
    move-exception v0

    .line 462
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getAppTokenFromGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCustomerBrandInfo()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 505
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 507
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    iget-object v6, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6}, Landroid/app/enterprise/sso/IGenericSSO;->getCustomerBrandInfo(Landroid/app/enterprise/ContextInfo;)Landroid/os/Bundle;

    move-result-object v3

    .line 508
    .local v3, "ret":Landroid/os/Bundle;
    const/4 v4, 0x0

    .line 509
    .local v4, "retMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    if-eqz v3, :cond_1

    .line 510
    new-instance v4, Ljava/util/HashMap;

    .end local v4    # "retMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 511
    .restart local v4    # "retMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 512
    .local v2, "key":Ljava/lang/String;
    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v5

    invoke-interface {v4, v2, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 516
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "key":Ljava/lang/String;
    .end local v3    # "ret":Landroid/os/Bundle;
    .end local v4    # "retMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "GenericSSO"

    const-string v6, "Exception in getCustomerBrandInfo"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v4, 0x0

    :cond_1
    return-object v4
.end method

.method public getEnrolledSSOVendor()Ljava/lang/String;
    .locals 3

    .prologue
    .line 162
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.getEnrolledSSOVendor"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 164
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 166
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->getEnrolledSSOVendor(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 171
    :goto_0
    return-object v1

    .line 167
    :catch_0
    move-exception v0

    .line 168
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getSSOIDs"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getToken(ZLandroid/app/enterprise/sso/GenericSSOCallback;)V
    .locals 3
    .param p1, "getFromLocalCache"    # Z
    .param p2, "callback"    # Landroid/app/enterprise/sso/GenericSSOCallback;

    .prologue
    .line 554
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 556
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/sso/IGenericSSO;->getToken(Landroid/app/enterprise/ContextInfo;ZLandroid/app/enterprise/sso/IGenericSSOCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    :cond_0
    :goto_0
    return-void

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getToken"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getUserAndDeviceCertFromGenericSSO()Landroid/app/enterprise/sso/TokenInfo;
    .locals 3

    .prologue
    .line 469
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 471
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->getUserAndDeviceCertFromGenericSSO(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/sso/TokenInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 477
    :goto_0
    return-object v1

    .line 473
    :catch_0
    move-exception v0

    .line 474
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getUserAndDeviceCertFromGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUserInfo(Landroid/app/enterprise/sso/GenericSSOCallback;)V
    .locals 3
    .param p1, "callback"    # Landroid/app/enterprise/sso/GenericSSOCallback;

    .prologue
    .line 574
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 576
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->getUserInfo(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/sso/IGenericSSOCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 577
    :catch_0
    move-exception v0

    .line 578
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getUserInfo"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getWhiteListPackages()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.getWhiteListPackages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 359
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 361
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->getWhiteListPackages(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 366
    :goto_0
    return-object v1

    .line 362
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in getWhitelistedPackages"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public get_IDP_SDK_Version()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    const-string v0, "2.1.1"

    return-object v0
.end method

.method public get_ISV_SDK_Version()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    const-string v0, "2.0.1"

    return-object v0
.end method

.method public processWebServiceRequest(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    .locals 32
    .param p1, "httpUriRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    .line 596
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v29

    if-eqz v29, :cond_1

    .line 598
    :try_start_0
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v24

    .line 599
    .local v24, "uri":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v16

    .line 600
    .local v16, "method":Ljava/lang/String;
    const/16 v27, 0x0

    .line 601
    .local v27, "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    const-string v29, "GET"

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_0

    .line 602
    new-instance v27, Landroid/app/enterprise/sso/WebServiceRequest;

    .end local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    sget-object v29, Landroid/app/enterprise/sso/WebServiceRequest$RequestType;->GET:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/sso/WebServiceRequest;-><init>(Landroid/app/enterprise/sso/WebServiceRequest$RequestType;Ljava/lang/String;)V

    .line 604
    .restart local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    const-string v29, "GenericSSO"

    const-string v30, "GET"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :goto_0
    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v11

    .line 614
    .local v11, "headers":[Lorg/apache/http/Header;
    move-object v4, v11

    .local v4, "arr$":[Lorg/apache/http/Header;
    array-length v14, v4

    .local v14, "len$":I
    const/4 v13, 0x0

    .local v13, "i$":I
    :goto_1
    if-ge v13, v14, :cond_4

    aget-object v10, v4, v13

    .line 616
    .local v10, "header":Lorg/apache/http/Header;
    new-instance v25, Landroid/app/enterprise/sso/WebServiceHeader;

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/sso/WebServiceHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    .local v25, "webHeader":Landroid/app/enterprise/sso/WebServiceHeader;
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Header Name value"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-interface {v10}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-interface {v10}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    move-object/from16 v0, v27

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/app/enterprise/sso/WebServiceRequest;->addHeader(Landroid/app/enterprise/sso/WebServiceHeader;)V

    .line 614
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 605
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v10    # "header":Lorg/apache/http/Header;
    .end local v11    # "headers":[Lorg/apache/http/Header;
    .end local v13    # "i$":I
    .end local v14    # "len$":I
    .end local v25    # "webHeader":Landroid/app/enterprise/sso/WebServiceHeader;
    :cond_0
    const-string v29, "POST"

    move-object/from16 v0, v16

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 606
    new-instance v27, Landroid/app/enterprise/sso/WebServiceRequest;

    .end local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    sget-object v29, Landroid/app/enterprise/sso/WebServiceRequest$RequestType;->POST:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    move-object/from16 v2, v24

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/sso/WebServiceRequest;-><init>(Landroid/app/enterprise/sso/WebServiceRequest$RequestType;Ljava/lang/String;)V

    .line 608
    .restart local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    const-string v29, "GenericSSO"

    const-string v30, "POST"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 678
    .end local v16    # "method":Ljava/lang/String;
    .end local v24    # "uri":Ljava/lang/String;
    .end local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    :catch_0
    move-exception v5

    .line 679
    .local v5, "e":Landroid/os/RemoteException;
    const-string v29, "GenericSSO"

    const-string v30, "Exception in getWebServiceInfo"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    .end local v5    # "e":Landroid/os/RemoteException;
    :cond_1
    :goto_2
    const/4 v12, 0x0

    :cond_2
    :goto_3
    return-object v12

    .line 610
    .restart local v16    # "method":Ljava/lang/String;
    .restart local v24    # "uri":Ljava/lang/String;
    .restart local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    :cond_3
    const/4 v12, 0x0

    goto :goto_3

    .line 625
    .restart local v4    # "arr$":[Lorg/apache/http/Header;
    .restart local v11    # "headers":[Lorg/apache/http/Header;
    .restart local v13    # "i$":I
    .restart local v14    # "len$":I
    :cond_4
    :try_start_1
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "URI"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-interface/range {p1 .. p1}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    move-object/from16 v0, p1

    instance-of v0, v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    move/from16 v29, v0

    if-eqz v29, :cond_5

    .line 627
    const-string v29, "GenericSSO"

    const-string v30, "HttpEnclosingRequest"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    move-object/from16 v0, p1

    check-cast v0, Lorg/apache/http/HttpEntityEnclosingRequest;

    move-object v6, v0

    .line 629
    .local v6, "enclosingRequest":Lorg/apache/http/HttpEntityEnclosingRequest;
    invoke-interface {v6}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v7

    .line 630
    .local v7, "entity":Lorg/apache/http/HttpEntity;
    if-eqz v7, :cond_5

    .line 631
    new-instance v18, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 632
    .local v18, "out":Ljava/io/ByteArrayOutputStream;
    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    .line 634
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 635
    invoke-virtual/range {v18 .. v18}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v29

    move-object/from16 v0, v27

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/app/enterprise/sso/WebServiceRequest;->setEntity([B)V

    .line 639
    .end local v6    # "enclosingRequest":Lorg/apache/http/HttpEntityEnclosingRequest;
    .end local v7    # "entity":Lorg/apache/http/HttpEntity;
    .end local v18    # "out":Ljava/io/ByteArrayOutputStream;
    :cond_5
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v29

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v30, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    move-object/from16 v2, v27

    invoke-interface {v0, v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->processWebServiceRequest(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/sso/WebServiceRequest;)Landroid/app/enterprise/sso/WebServiceResponse;

    move-result-object v28

    .line 642
    .local v28, "webResponse":Landroid/app/enterprise/sso/WebServiceResponse;
    if-nez v28, :cond_6

    .line 643
    const-string v29, "GenericSSO"

    const-string v30, "webResponse is null"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    const/4 v12, 0x0

    goto :goto_3

    .line 646
    :cond_6
    new-instance v9, Lorg/apache/http/impl/DefaultHttpResponseFactory;

    invoke-direct {v9}, Lorg/apache/http/impl/DefaultHttpResponseFactory;-><init>()V

    .line 648
    .local v9, "factory":Lorg/apache/http/HttpResponseFactory;
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getProtocol()Ljava/lang/String;

    move-result-object v20

    .line 649
    .local v20, "protocol":Ljava/lang/String;
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getMajor()I

    move-result v15

    .line 650
    .local v15, "major":I
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getMinor()I

    move-result v17

    .line 651
    .local v17, "minor":I
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getReasonPhrase()Ljava/lang/String;

    move-result-object v21

    .line 652
    .local v21, "reasonPhrase":Ljava/lang/String;
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "ReasonPhrase"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getStatusCode()I

    move-result v22

    .line 654
    .local v22, "statusCode":I
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Status code"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    new-instance v23, Lorg/apache/http/message/BasicStatusLine;

    new-instance v29, Lorg/apache/http/ProtocolVersion;

    move-object/from16 v0, v29

    move-object/from16 v1, v20

    move/from16 v2, v17

    invoke-direct {v0, v1, v15, v2}, Lorg/apache/http/ProtocolVersion;-><init>(Ljava/lang/String;II)V

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    move/from16 v2, v22

    move-object/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/http/message/BasicStatusLine;-><init>(Lorg/apache/http/ProtocolVersion;ILjava/lang/String;)V

    .line 659
    .local v23, "statusLine":Lorg/apache/http/StatusLine;
    const/16 v29, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-interface {v9, v0, v1}, Lorg/apache/http/HttpResponseFactory;->newHttpResponse(Lorg/apache/http/StatusLine;Lorg/apache/http/protocol/HttpContext;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 661
    .local v12, "httpResponse":Lorg/apache/http/HttpResponse;
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getHeaders()Ljava/util/List;

    move-result-object v26

    .line 662
    .local v26, "webHeaders":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WebServiceHeader;>;"
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Header size"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 663
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->size()I

    move-result v29

    if-lez v29, :cond_7

    .line 664
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_7

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/app/enterprise/sso/WebServiceHeader;

    .line 665
    .restart local v25    # "webHeader":Landroid/app/enterprise/sso/WebServiceHeader;
    const-string v29, "GenericSSO"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Name "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v25 .. v25}, Landroid/app/enterprise/sso/WebServiceHeader;->getName()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, "value"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v25 .. v25}, Landroid/app/enterprise/sso/WebServiceHeader;->getValue()Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    invoke-virtual/range {v25 .. v25}, Landroid/app/enterprise/sso/WebServiceHeader;->getName()Ljava/lang/String;

    move-result-object v29

    invoke-virtual/range {v25 .. v25}, Landroid/app/enterprise/sso/WebServiceHeader;->getValue()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    invoke-interface {v12, v0, v1}, Lorg/apache/http/HttpResponse;->addHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    .line 680
    .end local v4    # "arr$":[Lorg/apache/http/Header;
    .end local v9    # "factory":Lorg/apache/http/HttpResponseFactory;
    .end local v11    # "headers":[Lorg/apache/http/Header;
    .end local v12    # "httpResponse":Lorg/apache/http/HttpResponse;
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v14    # "len$":I
    .end local v15    # "major":I
    .end local v16    # "method":Ljava/lang/String;
    .end local v17    # "minor":I
    .end local v20    # "protocol":Ljava/lang/String;
    .end local v21    # "reasonPhrase":Ljava/lang/String;
    .end local v22    # "statusCode":I
    .end local v23    # "statusLine":Lorg/apache/http/StatusLine;
    .end local v24    # "uri":Ljava/lang/String;
    .end local v25    # "webHeader":Landroid/app/enterprise/sso/WebServiceHeader;
    .end local v26    # "webHeaders":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WebServiceHeader;>;"
    .end local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    .end local v28    # "webResponse":Landroid/app/enterprise/sso/WebServiceResponse;
    :catch_1
    move-exception v5

    .line 681
    .local v5, "e":Ljava/io/IOException;
    const-string v29, "GenericSSO"

    const-string v30, "IO Exception"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 672
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v4    # "arr$":[Lorg/apache/http/Header;
    .restart local v9    # "factory":Lorg/apache/http/HttpResponseFactory;
    .restart local v11    # "headers":[Lorg/apache/http/Header;
    .restart local v12    # "httpResponse":Lorg/apache/http/HttpResponse;
    .restart local v14    # "len$":I
    .restart local v15    # "major":I
    .restart local v16    # "method":Ljava/lang/String;
    .restart local v17    # "minor":I
    .restart local v20    # "protocol":Ljava/lang/String;
    .restart local v21    # "reasonPhrase":Ljava/lang/String;
    .restart local v22    # "statusCode":I
    .restart local v23    # "statusLine":Lorg/apache/http/StatusLine;
    .restart local v24    # "uri":Ljava/lang/String;
    .restart local v26    # "webHeaders":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WebServiceHeader;>;"
    .restart local v27    # "webRequest":Landroid/app/enterprise/sso/WebServiceRequest;
    .restart local v28    # "webResponse":Landroid/app/enterprise/sso/WebServiceResponse;
    :cond_7
    :try_start_2
    invoke-virtual/range {v28 .. v28}, Landroid/app/enterprise/sso/WebServiceResponse;->getEntity()[B

    move-result-object v8

    .line 673
    .local v8, "entityData":[B
    if-eqz v8, :cond_2

    .line 674
    new-instance v19, Lorg/apache/http/entity/ByteArrayEntity;

    move-object/from16 v0, v19

    invoke-direct {v0, v8}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    .line 675
    .local v19, "postEntity":Lorg/apache/http/entity/ByteArrayEntity;
    move-object/from16 v0, v19

    invoke-interface {v12, v0}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3
.end method

.method public removeAppTokenFromGenericSSO(Ljava/lang/String;)I
    .locals 3
    .param p1, "appPkgName"    # Ljava/lang/String;

    .prologue
    .line 481
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 483
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->removeAppTokenFromGenericSSO(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 489
    :goto_0
    return v1

    .line 485
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in removeAppTokenFromGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public removeUserAndDeviceCertFromGenericSSO()I
    .locals 3

    .prologue
    .line 493
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 495
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/sso/IGenericSSO;->removeUserAndDeviceCertFromGenericSSO(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 501
    :goto_0
    return v1

    .line 497
    :catch_0
    move-exception v0

    .line 498
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in removeUserAndDeviceCertFromGenericSSO"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setCustomerBrandInfo(Ljava/util/Map;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "[B>;)I"
        }
    .end annotation

    .prologue
    .local p1, "brandInfo":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;[B>;"
    const/16 v6, -0xe

    .line 373
    iget-object v5, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "GenericSSO.setCustomerBrandInfo"

    invoke-static {v5, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 375
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 377
    const/4 v0, 0x0

    .line 378
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz p1, :cond_3

    .line 379
    :try_start_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V
    :try_end_0
    .catch Landroid/os/TransactionTooLargeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 380
    .end local v0    # "bundle":Landroid/os/Bundle;
    .local v1, "bundle":Landroid/os/Bundle;
    :try_start_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 381
    .local v4, "key":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 383
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    const/high16 v7, 0x100000

    if-le v5, v7, :cond_1

    move v5, v6

    .line 396
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    :goto_1
    return v5

    .line 385
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "key":Ljava/lang/String;
    :cond_1
    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V
    :try_end_1
    .catch Landroid/os/TransactionTooLargeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 389
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "key":Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 390
    .end local v1    # "bundle":Landroid/os/Bundle;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .local v2, "e":Landroid/os/TransactionTooLargeException;
    :goto_2
    const-string v5, "GenericSSO"

    const-string v7, "TranscationTooLarge Exception in setCustomerBrandInfo"

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 391
    goto :goto_1

    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "e":Landroid/os/TransactionTooLargeException;
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_2
    move-object v0, v1

    .line 388
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "i$":Ljava/util/Iterator;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    :cond_3
    :try_start_2
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v5

    iget-object v7, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v7, v0}, Landroid/app/enterprise/sso/IGenericSSO;->setCustomerBrandInfo(Landroid/app/enterprise/ContextInfo;Landroid/os/Bundle;)I
    :try_end_2
    .catch Landroid/os/TransactionTooLargeException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v5

    goto :goto_1

    .line 392
    :catch_1
    move-exception v2

    .line 393
    .local v2, "e":Landroid/os/RemoteException;
    :goto_3
    const-string v5, "GenericSSO"

    const-string v6, "Exception in setCustomerBrandInfo"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_4
    const/4 v5, -0x1

    goto :goto_1

    .line 392
    .restart local v1    # "bundle":Landroid/os/Bundle;
    :catch_2
    move-exception v2

    move-object v0, v1

    .end local v1    # "bundle":Landroid/os/Bundle;
    .restart local v0    # "bundle":Landroid/os/Bundle;
    goto :goto_3

    .line 389
    :catch_3
    move-exception v2

    goto :goto_2
.end method

.method public unenrollSSOVendor(Ljava/lang/String;)I
    .locals 3
    .param p1, "authenticatorPkgName"    # Ljava/lang/String;

    .prologue
    .line 243
    iget-object v1, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "GenericSSO.unenrollSSOVendor"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 245
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 247
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/sso/GenericSSO;->getService()Landroid/app/enterprise/sso/IGenericSSO;

    move-result-object v1

    iget-object v2, p0, Landroid/app/enterprise/sso/GenericSSO;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/sso/IGenericSSO;->unenrollSSOVendor(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 253
    :goto_0
    return v1

    .line 249
    :catch_0
    move-exception v0

    .line 250
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "GenericSSO"

    const-string v2, "Exception in unEnrollSSOVendor"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
