.class public abstract Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;
.super Landroid/os/Binder;
.source "GenericSSOSupportSolution.java"

# interfaces
.implements Landroid/app/enterprise/sso/GenericSSOSupportSolution;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/sso/GenericSSOSupportSolution;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.sso.GenericSSOSupportSolution"

.field static final TRANSACTION_acquireToken:I = 0x1

.field static final TRANSACTION_acquireTokenByRefreshToken:I = 0x4

.field static final TRANSACTION_acquireUserInfo:I = 0x2

.field static final TRANSACTION_acquireWebService:I = 0x3

.field static final TRANSACTION_forceAuthenticate:I = 0x6

.field static final TRANSACTION_pushAuthenticatorConfig:I = 0x7

.field static final TRANSACTION_unregister:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 26
    const-string v0, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/sso/GenericSSOSupportSolution;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 34
    if-nez p0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0

    .line 37
    :cond_0
    const-string v1, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 38
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/sso/GenericSSOSupportSolution;

    if-eqz v1, :cond_1

    .line 39
    check-cast v0, Landroid/app/enterprise/sso/GenericSSOSupportSolution;

    goto :goto_0

    .line 41
    :cond_1
    new-instance v0, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 45
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 49
    sparse-switch p1, :sswitch_data_0

    .line 186
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 53
    :sswitch_0
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :sswitch_1
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    sget-object v3, Landroid/app/enterprise/sso/AuthenticationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/AuthenticationRequest;

    .line 66
    .local v0, "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    :goto_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->acquireToken(Landroid/app/enterprise/sso/AuthenticationRequest;)Landroid/app/enterprise/sso/TokenInfo;

    move-result-object v1

    .line 67
    .local v1, "_result":Landroid/app/enterprise/sso/TokenInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 70
    invoke-virtual {v1, p3, v2}, Landroid/app/enterprise/sso/TokenInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 64
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    goto :goto_1

    .line 73
    .restart local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :cond_1
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :sswitch_2
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 82
    sget-object v3, Landroid/app/enterprise/sso/AuthenticationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/AuthenticationRequest;

    .line 87
    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->acquireUserInfo(Landroid/app/enterprise/sso/AuthenticationRequest;)Landroid/app/enterprise/sso/UserInfo;

    move-result-object v1

    .line 88
    .local v1, "_result":Landroid/app/enterprise/sso/UserInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 89
    if-eqz v1, :cond_3

    .line 90
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 91
    invoke-virtual {v1, p3, v2}, Landroid/app/enterprise/sso/UserInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 85
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/UserInfo;
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    goto :goto_2

    .line 94
    .restart local v1    # "_result":Landroid/app/enterprise/sso/UserInfo;
    :cond_3
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 100
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/UserInfo;
    :sswitch_3
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 103
    sget-object v3, Landroid/app/enterprise/sso/AuthenticationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/AuthenticationRequest;

    .line 108
    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    :goto_3
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->acquireWebService(Landroid/app/enterprise/sso/AuthenticationRequest;)Landroid/app/enterprise/sso/WebServiceResponse;

    move-result-object v1

    .line 109
    .local v1, "_result":Landroid/app/enterprise/sso/WebServiceResponse;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    if-eqz v1, :cond_5

    .line 111
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    invoke-virtual {v1, p3, v2}, Landroid/app/enterprise/sso/WebServiceResponse;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 106
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/WebServiceResponse;
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    goto :goto_3

    .line 115
    .restart local v1    # "_result":Landroid/app/enterprise/sso/WebServiceResponse;
    :cond_5
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 121
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/WebServiceResponse;
    :sswitch_4
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_6

    .line 124
    sget-object v3, Landroid/app/enterprise/sso/AuthenticationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/AuthenticationRequest;

    .line 129
    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->acquireTokenByRefreshToken(Landroid/app/enterprise/sso/AuthenticationRequest;)Landroid/app/enterprise/sso/TokenInfo;

    move-result-object v1

    .line 130
    .local v1, "_result":Landroid/app/enterprise/sso/TokenInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 131
    if-eqz v1, :cond_7

    .line 132
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    invoke-virtual {v1, p3, v2}, Landroid/app/enterprise/sso/TokenInfo;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 127
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    goto :goto_4

    .line 136
    .restart local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :cond_7
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 142
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/AuthenticationRequest;
    .end local v1    # "_result":Landroid/app/enterprise/sso/TokenInfo;
    :sswitch_5
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_8

    .line 145
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 150
    .local v0, "_arg0":Landroid/os/Bundle;
    :goto_5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->unregister(Landroid/os/Bundle;)I

    move-result v1

    .line 151
    .local v1, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 152
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 148
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_result":I
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_5

    .line 157
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_6
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_9

    .line 160
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 165
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_6
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->forceAuthenticate(Landroid/os/Bundle;)I

    move-result v1

    .line 166
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 163
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_result":I
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_6

    .line 172
    .end local v0    # "_arg0":Landroid/os/Bundle;
    :sswitch_7
    const-string v3, "android.app.enterprise.sso.GenericSSOSupportSolution"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_a

    .line 175
    sget-object v3, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v3, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 180
    .restart local v0    # "_arg0":Landroid/os/Bundle;
    :goto_7
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/GenericSSOSupportSolution$Stub;->pushAuthenticatorConfig(Landroid/os/Bundle;)I

    move-result v1

    .line 181
    .restart local v1    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 182
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 178
    .end local v0    # "_arg0":Landroid/os/Bundle;
    .end local v1    # "_result":I
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/os/Bundle;
    goto :goto_7

    .line 49
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
