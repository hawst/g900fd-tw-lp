.class public Landroid/app/enterprise/sso/WebServiceRequest;
.super Ljava/lang/Object;
.source "WebServiceRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/sso/WebServiceRequest$RequestType;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/sso/WebServiceRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEntity:[B

.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

.field private mUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Landroid/app/enterprise/sso/WebServiceRequest$1;

    invoke-direct {v0}, Landroid/app/enterprise/sso/WebServiceRequest$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/WebServiceRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/sso/WebServiceRequest$RequestType;Ljava/lang/String;)V
    .locals 0
    .param p1, "requestType"    # Landroid/app/enterprise/sso/WebServiceRequest$RequestType;
    .param p2, "Uri"    # Ljava/lang/String;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p2, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mUri:Ljava/lang/String;

    .line 60
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/WebServiceRequest;->readFromParcel(Landroid/os/Parcel;)V

    .line 90
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/sso/WebServiceRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/sso/WebServiceRequest$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/WebServiceRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mUri:Ljava/lang/String;

    .line 114
    const-class v1, Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 117
    .local v0, "size":I
    if-eqz v0, :cond_0

    .line 118
    new-array v1, v0, [B

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    .line 119
    iget-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 121
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    .line 122
    iget-object v1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    sget-object v2, Landroid/app/enterprise/sso/WebServiceHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 123
    return-void
.end method


# virtual methods
.method public addHeader(Landroid/app/enterprise/sso/WebServiceHeader;)V
    .locals 1
    .param p1, "header"    # Landroid/app/enterprise/sso/WebServiceHeader;

    .prologue
    .line 64
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    .line 67
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public getEntity()[B
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    return-object v0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method public getRequestType()Landroid/app/enterprise/sso/WebServiceRequest$RequestType;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    return-object v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mUri:Ljava/lang/String;

    return-object v0
.end method

.method public setEntity([B)V
    .locals 0
    .param p1, "entity"    # [B

    .prologue
    .line 47
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    .line 48
    return-void
.end method

.method public setHeaders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WebServiceHeader;>;"
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    .line 40
    return-void
.end method

.method public setRequestType(Landroid/app/enterprise/sso/WebServiceRequest$RequestType;)V
    .locals 0
    .param p1, "requestType"    # Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    .prologue
    .line 55
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    .line 56
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 31
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mUri:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 101
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mUri:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mRequestType:Landroid/app/enterprise/sso/WebServiceRequest$RequestType;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 103
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    if-nez v0, :cond_0

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    :goto_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mHeaders:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 110
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceRequest;->mEntity:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
