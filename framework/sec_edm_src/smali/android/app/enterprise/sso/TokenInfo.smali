.class public Landroid/app/enterprise/sso/TokenInfo;
.super Ljava/lang/Object;
.source "TokenInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/sso/TokenInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private protocolType:I

.field private responseBundle:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    new-instance v0, Landroid/app/enterprise/sso/TokenInfo$1;

    invoke-direct {v0}, Landroid/app/enterprise/sso/TokenInfo$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/TokenInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    return-void
.end method

.method public constructor <init>(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput p1, p0, Landroid/app/enterprise/sso/TokenInfo;->protocolType:I

    .line 84
    iput-object p2, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    .line 85
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/TokenInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 108
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/sso/TokenInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/sso/TokenInfo$1;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/TokenInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/sso/TokenInfo;->protocolType:I

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    .line 125
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public getProtocolType()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Landroid/app/enterprise/sso/TokenInfo;->protocolType:I

    return v0
.end method

.method public getResponseBundle()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    return-object v0
.end method

.method public setProtocolType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 59
    iput p1, p0, Landroid/app/enterprise/sso/TokenInfo;->protocolType:I

    .line 60
    return-void
.end method

.method public setResponseBundle(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 63
    iput-object p1, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    .line 64
    return-void
.end method

.method public setResponseBundle(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 73
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    .line 75
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method public setResponseBundle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 67
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    .line 69
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 118
    iget v0, p0, Landroid/app/enterprise/sso/TokenInfo;->protocolType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    iget-object v0, p0, Landroid/app/enterprise/sso/TokenInfo;->responseBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method
