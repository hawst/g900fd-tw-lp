.class public abstract Landroid/app/enterprise/sso/IGenericSSOCallback$Stub;
.super Landroid/os/Binder;
.source "IGenericSSOCallback.java"

# interfaces
.implements Landroid/app/enterprise/sso/IGenericSSOCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/sso/IGenericSSOCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/sso/IGenericSSOCallback$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.sso.IGenericSSOCallback"

.field static final TRANSACTION_onFail:I = 0x3

.field static final TRANSACTION_tokenInfoSuccess:I = 0x1

.field static final TRANSACTION_userInfoSuccess:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/sso/IGenericSSOCallback;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/sso/IGenericSSOCallback;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/sso/IGenericSSOCallback;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 87
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 46
    :sswitch_0
    const-string v2, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v2, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 54
    sget-object v2, Landroid/app/enterprise/sso/TokenInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/TokenInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/sso/TokenInfo;
    :goto_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub;->tokenInfoSuccess(Landroid/app/enterprise/sso/TokenInfo;)V

    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/TokenInfo;
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/TokenInfo;
    goto :goto_1

    .line 65
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/TokenInfo;
    :sswitch_2
    const-string v2, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    sget-object v2, Landroid/app/enterprise/sso/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v2, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/UserInfo;

    .line 73
    .local v0, "_arg0":Landroid/app/enterprise/sso/UserInfo;
    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub;->userInfoSuccess(Landroid/app/enterprise/sso/UserInfo;)V

    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 71
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/UserInfo;
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/sso/UserInfo;
    goto :goto_2

    .line 79
    .end local v0    # "_arg0":Landroid/app/enterprise/sso/UserInfo;
    :sswitch_3
    const-string v2, "android.app.enterprise.sso.IGenericSSOCallback"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 82
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/sso/IGenericSSOCallback$Stub;->onFail(I)V

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
