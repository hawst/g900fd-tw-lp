.class public Landroid/app/enterprise/sso/AuthenticationRequest;
.super Ljava/lang/Object;
.source "AuthenticationRequest.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/sso/AuthenticationRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAuthenticatorPkgName:Ljava/lang/String;

.field private mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

.field private mRequestConfig:Landroid/os/Bundle;

.field private mToken:Landroid/app/enterprise/sso/TokenInfo;

.field private mWhiteListPkgName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 114
    new-instance v0, Landroid/app/enterprise/sso/AuthenticationRequest$1;

    invoke-direct {v0}, Landroid/app/enterprise/sso/AuthenticationRequest$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/AuthenticationRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/AuthenticationRequest;->readFromParcel(Landroid/os/Parcel;)V

    .line 134
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/sso/AuthenticationRequest$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/sso/AuthenticationRequest$1;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/AuthenticationRequest;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/sso/TokenInfo;Landroid/os/Bundle;Landroid/app/enterprise/sso/WebServiceRequest;)V
    .locals 0
    .param p1, "authenPkgName"    # Ljava/lang/String;
    .param p2, "whitelistpkg"    # Ljava/lang/String;
    .param p3, "token"    # Landroid/app/enterprise/sso/TokenInfo;
    .param p4, "config"    # Landroid/os/Bundle;
    .param p5, "httprequest"    # Landroid/app/enterprise/sso/WebServiceRequest;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mAuthenticatorPkgName:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mWhiteListPkgName:Ljava/lang/String;

    .line 68
    iput-object p3, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mToken:Landroid/app/enterprise/sso/TokenInfo;

    .line 69
    iput-object p4, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mRequestConfig:Landroid/os/Bundle;

    .line 70
    iput-object p5, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

    .line 71
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mAuthenticatorPkgName:Ljava/lang/String;

    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mWhiteListPkgName:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mRequestConfig:Landroid/os/Bundle;

    .line 155
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/TokenInfo;

    iput-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mToken:Landroid/app/enterprise/sso/TokenInfo;

    .line 156
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/sso/WebServiceRequest;

    iput-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

    .line 157
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthenticatorPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mAuthenticatorPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestConfig()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mRequestConfig:Landroid/os/Bundle;

    return-object v0
.end method

.method public getTokenInfo()Landroid/app/enterprise/sso/TokenInfo;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mToken:Landroid/app/enterprise/sso/TokenInfo;

    return-object v0
.end method

.method public getWebServiceRequest()Landroid/app/enterprise/sso/WebServiceRequest;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

    return-object v0
.end method

.method public getWhiteListPkgName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mWhiteListPkgName:Ljava/lang/String;

    return-object v0
.end method

.method public setAuthenticatorPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1, "authPkgName"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mAuthenticatorPkgName:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public setRequestConfig(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "config"    # Landroid/os/Bundle;

    .prologue
    .line 102
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mRequestConfig:Landroid/os/Bundle;

    .line 103
    return-void
.end method

.method public setTokenInfo(Landroid/app/enterprise/sso/TokenInfo;)V
    .locals 0
    .param p1, "token"    # Landroid/app/enterprise/sso/TokenInfo;

    .prologue
    .line 106
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mToken:Landroid/app/enterprise/sso/TokenInfo;

    .line 107
    return-void
.end method

.method public setWebServiceRequest(Landroid/app/enterprise/sso/WebServiceRequest;)V
    .locals 0
    .param p1, "request"    # Landroid/app/enterprise/sso/WebServiceRequest;

    .prologue
    .line 110
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

    .line 111
    return-void
.end method

.method public setWhiteListPkgName(Ljava/lang/String;)V
    .locals 0
    .param p1, "whitelistPkgName"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mWhiteListPkgName:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 144
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mAuthenticatorPkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mWhiteListPkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mRequestConfig:Landroid/os/Bundle;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeBundle(Landroid/os/Bundle;)V

    .line 147
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mToken:Landroid/app/enterprise/sso/TokenInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 148
    iget-object v0, p0, Landroid/app/enterprise/sso/AuthenticationRequest;->mHttpRequest:Landroid/app/enterprise/sso/WebServiceRequest;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 149
    return-void
.end method
