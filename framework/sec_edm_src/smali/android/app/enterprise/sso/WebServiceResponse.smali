.class public Landroid/app/enterprise/sso/WebServiceResponse;
.super Ljava/lang/Object;
.source "WebServiceResponse.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/sso/WebServiceResponse;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mEntity:[B

.field private mHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;"
        }
    .end annotation
.end field

.field private mMajor:I

.field private mMinor:I

.field private mProtocol:Ljava/lang/String;

.field private mReasonPhrase:Ljava/lang/String;

.field private mStatusCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Landroid/app/enterprise/sso/WebServiceResponse$1;

    invoke-direct {v0}, Landroid/app/enterprise/sso/WebServiceResponse$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/WebServiceResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 2
    .param p1, "statusCode"    # I
    .param p2, "reasonPhrase"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const-string v0, "HTTP"

    iput-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    .line 87
    iput v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    .line 88
    iput v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMinor:I

    .line 89
    iput p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    .line 90
    iput-object p2, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    .line 91
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/WebServiceResponse;->readFromParcel(Landroid/os/Parcel;)V

    .line 129
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/sso/WebServiceResponse$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/sso/WebServiceResponse$1;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/WebServiceResponse;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIILjava/lang/String;)V
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "major"    # I
    .param p3, "minor"    # I
    .param p4, "statusCode"    # I
    .param p5, "reasonPhrase"    # Ljava/lang/String;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    .line 96
    iput p2, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    .line 97
    iput p3, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMinor:I

    .line 98
    iput p4, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    .line 99
    iput-object p5, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    .line 100
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMinor:I

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 161
    .local v0, "size":I
    if-eqz v0, :cond_0

    .line 162
    new-array v1, v0, [B

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    .line 163
    iget-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 165
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    .line 166
    iget-object v1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    sget-object v2, Landroid/app/enterprise/sso/WebServiceHeader;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v1, v2}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 167
    return-void
.end method


# virtual methods
.method public addHeader(Landroid/app/enterprise/sso/WebServiceHeader;)V
    .locals 1
    .param p1, "header"    # Landroid/app/enterprise/sso/WebServiceHeader;

    .prologue
    .line 103
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    .line 106
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public getEntity()[B
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    return-object v0
.end method

.method public getHeaders()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    return-object v0
.end method

.method public getMajor()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    return v0
.end method

.method public getMinor()I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    return v0
.end method

.method public getProtocol()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    return-object v0
.end method

.method public getReasonPhrase()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    return-object v0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    return v0
.end method

.method public setEntity([B)V
    .locals 0
    .param p1, "entity"    # [B

    .prologue
    .line 78
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    .line 79
    return-void
.end method

.method public setHeaders(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/sso/WebServiceHeader;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "headers":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/sso/WebServiceHeader;>;"
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    .line 71
    return-void
.end method

.method public setMajor(I)V
    .locals 0
    .param p1, "major"    # I

    .prologue
    .line 38
    iput p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    .line 39
    return-void
.end method

.method public setMinor(I)V
    .locals 0
    .param p1, "minor"    # I

    .prologue
    .line 46
    iput p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMinor:I

    .line 47
    return-void
.end method

.method public setProtocol(Ljava/lang/String;)V
    .locals 0
    .param p1, "protocol"    # Ljava/lang/String;

    .prologue
    .line 30
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public setReasonPhrase(Ljava/lang/String;)V
    .locals 0
    .param p1, "reasonPhrase"    # Ljava/lang/String;

    .prologue
    .line 62
    iput-object p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public setStatusCode(I)V
    .locals 0
    .param p1, "statusCode"    # I

    .prologue
    .line 54
    iput p1, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    .line 55
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 140
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mProtocol:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 141
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMajor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 142
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mMinor:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 143
    iget v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mStatusCode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 144
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mReasonPhrase:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    if-nez v0, :cond_0

    .line 146
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 151
    :goto_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mHeaders:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 152
    return-void

    .line 148
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    iget-object v0, p0, Landroid/app/enterprise/sso/WebServiceResponse;->mEntity:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
