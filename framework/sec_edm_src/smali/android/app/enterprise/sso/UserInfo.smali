.class public Landroid/app/enterprise/sso/UserInfo;
.super Ljava/lang/Object;
.source "UserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/sso/UserInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mAuthenticationType:Ljava/lang/String;

.field private mEmailAddress:Ljava/lang/String;

.field private mFirstName:Ljava/lang/String;

.field private mIdentityProvider:Ljava/lang/String;

.field private mLastAuthenMillis:Ljava/lang/String;

.field private mLastName:Ljava/lang/String;

.field private mPhoneNumber:Ljava/lang/String;

.field private mTenantId:Ljava/lang/String;

.field private mUserId:Ljava/lang/String;

.field private userInfoJsonString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Landroid/app/enterprise/sso/UserInfo$1;

    invoke-direct {v0}, Landroid/app/enterprise/sso/UserInfo$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/sso/UserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 224
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/UserInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 226
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/sso/UserInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/sso/UserInfo$1;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/app/enterprise/sso/UserInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "jsonStr"    # Ljava/lang/String;

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    .line 170
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "userid"    # Ljava/lang/String;
    .param p2, "firstName"    # Ljava/lang/String;
    .param p3, "lastName"    # Ljava/lang/String;
    .param p4, "identityProvider"    # Ljava/lang/String;
    .param p5, "tenantId"    # Ljava/lang/String;
    .param p6, "address"    # Ljava/lang/String;
    .param p7, "emailAddress"    # Ljava/lang/String;
    .param p8, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    .line 180
    iput-object p3, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    .line 181
    iput-object p4, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 182
    iput-object p5, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    .line 183
    iput-object p6, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    .line 184
    iput-object p7, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    .line 185
    iput-object p8, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "jsonStr"    # Ljava/lang/String;
    .param p2, "userid"    # Ljava/lang/String;
    .param p3, "firstName"    # Ljava/lang/String;
    .param p4, "lastName"    # Ljava/lang/String;
    .param p5, "identityProvider"    # Ljava/lang/String;
    .param p6, "tenantId"    # Ljava/lang/String;
    .param p7, "address"    # Ljava/lang/String;
    .param p8, "emailAddress"    # Ljava/lang/String;
    .param p9, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    .line 195
    iput-object p2, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    .line 196
    iput-object p3, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    .line 197
    iput-object p4, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    .line 198
    iput-object p5, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 199
    iput-object p6, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    .line 200
    iput-object p7, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    .line 201
    iput-object p8, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    .line 202
    iput-object p9, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    .line 203
    return-void
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    .line 251
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    .line 252
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    .line 253
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    .line 254
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 255
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAuthenticationType:Ljava/lang/String;

    .line 256
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    .line 257
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    .line 258
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    .line 259
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    .line 260
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastAuthenMillis:Ljava/lang/String;

    .line 261
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthenticationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAuthenticationType:Ljava/lang/String;

    return-object v0
.end method

.method public getEmailAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    return-object v0
.end method

.method public getIdentityProvider()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    return-object v0
.end method

.method public getJsonString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    return-object v0
.end method

.method public getLastAuthenMillis()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastAuthenMillis:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    return-object v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTenantId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    return-object v0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 149
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public setAuthenticationType(Ljava/lang/String;)V
    .locals 0
    .param p1, "authType"    # Ljava/lang/String;

    .prologue
    .line 141
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mAuthenticationType:Ljava/lang/String;

    .line 142
    return-void
.end method

.method public setEmailAddress(Ljava/lang/String;)V
    .locals 0
    .param p1, "email"    # Ljava/lang/String;

    .prologue
    .line 153
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    .line 154
    return-void
.end method

.method public setFirstName(Ljava/lang/String;)V
    .locals 0
    .param p1, "firstName"    # Ljava/lang/String;

    .prologue
    .line 129
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    .line 130
    return-void
.end method

.method public setIdentityProvider(Ljava/lang/String;)V
    .locals 0
    .param p1, "idp"    # Ljava/lang/String;

    .prologue
    .line 137
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    .line 138
    return-void
.end method

.method public setJsonString(Ljava/lang/String;)V
    .locals 0
    .param p1, "json"    # Ljava/lang/String;

    .prologue
    .line 121
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    .line 122
    return-void
.end method

.method public setLastAuthenMillis(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastAuth"    # Ljava/lang/String;

    .prologue
    .line 161
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mLastAuthenMillis:Ljava/lang/String;

    .line 162
    return-void
.end method

.method public setLastName(Ljava/lang/String;)V
    .locals 0
    .param p1, "lastName"    # Ljava/lang/String;

    .prologue
    .line 133
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    .line 134
    return-void
.end method

.method public setPhoneNumber(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 157
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public setTenantId(Ljava/lang/String;)V
    .locals 0
    .param p1, "tenantId"    # Ljava/lang/String;

    .prologue
    .line 145
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    .line 146
    return-void
.end method

.method public setUserId(Ljava/lang/String;)V
    .locals 0
    .param p1, "userId"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 236
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->userInfoJsonString:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 237
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mUserId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 238
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mFirstName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 239
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 240
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mIdentityProvider:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAuthenticationType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 242
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mTenantId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 244
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mPhoneNumber:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 245
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Landroid/app/enterprise/sso/UserInfo;->mLastAuthenMillis:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 247
    return-void
.end method
