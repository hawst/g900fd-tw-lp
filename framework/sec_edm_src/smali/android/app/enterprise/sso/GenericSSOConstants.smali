.class public Landroid/app/enterprise/sso/GenericSSOConstants;
.super Ljava/lang/Object;
.source "GenericSSOConstants.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/sso/GenericSSOConstants$KerberosRequestKeys;,
        Landroid/app/enterprise/sso/GenericSSOConstants$SAMLResponseKeys;,
        Landroid/app/enterprise/sso/GenericSSOConstants$SAMLRequestKeys;,
        Landroid/app/enterprise/sso/GenericSSOConstants$OAuthResponseKeys;,
        Landroid/app/enterprise/sso/GenericSSOConstants$OAuthRequestKeys;
    }
.end annotation


# static fields
.field public static final ACTION_GENERICSSO_TOKEN_CLEAR:Ljava/lang/String; = "genericsso.INTENT.ACTION.tokens_cleared"

.field public static final BIND_ACTION_GENERICSSO:Ljava/lang/String; = ".genericssoconnection"

.field public static final CUSTOMER_BRAND_LOGO:Ljava/lang/String; = "customer_brand_logo"

.field public static final CUSTOMER_BRAND_NAME:Ljava/lang/String; = "customer_brand_name"

.field public static final DEBUG:Z

.field public static final ERROR_CLIENT_ID_ABSENT:I = -0xb

.field public static final ERROR_DATA_OVERSIZE:I = -0xe

.field public static final ERROR_FAIL:I = -0x1

.field public static final ERROR_FILE_NOT_FOUND:I = -0x5

.field public static final ERROR_INVALID_PARAMETER:I = -0x3

.field public static final ERROR_LICENSE_CHECK_FAILED:I = -0x8

.field public static final ERROR_NETWORK_NOT_AVAILABLE:I = -0xf

.field public static final ERROR_NOT_AUTHORIZED:I = -0x2

.field public static final ERROR_NOT_SAME_MDM:I = -0xa

.field public static final ERROR_PACKAGE_NOT_WHITELISTED:I = -0x7

.field public static final ERROR_REMOTE_PROCESSING:I = -0x10

.field public static final ERROR_SECOND_SSOPROVIDER_REJECTED:I = -0x9

.field public static final ERROR_SERVICE_NOT_BOUND:I = -0x4

.field public static final ERROR_SSOAUTHENTICATOR_NOT_INSTALLED:I = -0xc

.field public static final ERROR_SSOAUTHENTICATOR_SIGNATURE_NOT_MATCHED:I = -0xd

.field public static final ERROR_SSOPROVIDER_NOT_ENROLLED:I = -0x6

.field public static final PROTOCOL_KERBEROS:I = 0x3

.field public static final PROTOCOL_OAUTH:I = 0x2

.field public static final PROTOCOL_OPENID:I = 0x4

.field public static final PROTOCOL_SAML:I = 0x1

.field public static final SSO_PROVIDER_PACKAGE_NAME:Ljava/lang/String; = "servicepackagename"

.field public static final SSO_PROVIDER_PACKAGE_SIGNATURE:Ljava/lang/String; = "servicepackagesignature"

.field public static final SSO_PROVIDER_PROTOCAL_TYPE:Ljava/lang/String; = "protocoltype"

.field public static final SSO_PROVIDER_SETBY_MDMUID:Ljava/lang/String; = "mdmuid"

.field public static final SUCCESS:I = 0x0

.field public static final WHITELIST_PACKAGE_NAME:Ljava/lang/String; = "clientpackagename"

.field public static final WHITELIST_PACKAGE_SIGNATURE:Ljava/lang/String; = "clientpackagesignature"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 104
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v1

    if-ne v1, v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    sput-boolean v0, Landroid/app/enterprise/sso/GenericSSOConstants;->DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    return-void
.end method
