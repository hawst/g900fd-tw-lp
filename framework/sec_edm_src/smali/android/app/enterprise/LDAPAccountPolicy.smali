.class public Landroid/app/enterprise/LDAPAccountPolicy;
.super Ljava/lang/Object;
.source "LDAPAccountPolicy.java"


# static fields
.field public static final ACTION_LDAP_CREATE_ACCT:Ljava/lang/String; = "edm.intent.action.ldap.createacct.internal"

.field public static final ACTION_LDAP_CREATE_ACCT_RESULT:Ljava/lang/String; = "edm.intent.action.ldap.createacct.result"

.field public static final ERROR_LDAP_ALREADY_EXISTS:I = -0x1

.field public static final ERROR_LDAP_CONNECT_ERROR:I = -0x7

.field public static final ERROR_LDAP_DOES_NOT_EXIST:I = -0x2

.field public static final ERROR_LDAP_INVALID_CREDENTIALS:I = -0x3

.field public static final ERROR_LDAP_NONE:I = 0x0

.field public static final ERROR_LDAP_SERVER_BUSY:I = -0x4

.field public static final ERROR_LDAP_SERVER_DOWN:I = -0x5

.field public static final ERROR_LDAP_TIMEOUT:I = -0x6

.field public static final ERROR_LDAP_UNKNOWN:I = -0x8

.field public static final EXTRA_LDAP_ACCT_ID:Ljava/lang/String; = "edm.intent.extra.ldap.acct.id"

.field public static final EXTRA_LDAP_RESULT:Ljava/lang/String; = "edm.intent.extra.ldap.result"

.field public static final EXTRA_LDAP_USER_ID:Ljava/lang/String; = "edm.intent.extra.ldap.user.id"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private lService:Landroid/app/enterprise/ILDAPAccountPolicy;

.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "LDAPAccountPolicy"

    sput-object v0, Landroid/app/enterprise/LDAPAccountPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p2, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContext:Landroid/content/Context;

    .line 176
    iput-object p1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 177
    return-void
.end method

.method private getService()Landroid/app/enterprise/ILDAPAccountPolicy;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    if-nez v0, :cond_0

    .line 181
    const-string v0, "ldap_account_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/ILDAPAccountPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/ILDAPAccountPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    .line 184
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    return-object v0
.end method


# virtual methods
.method public createLDAPAccount(Landroid/app/enterprise/LDAPAccount;)V
    .locals 3
    .param p1, "ldap"    # Landroid/app/enterprise/LDAPAccount;

    .prologue
    .line 227
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LDAPAccountPolicy.createLDAPAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 229
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/LDAPAccountPolicy;->getService()Landroid/app/enterprise/ILDAPAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ILDAPAccountPolicy;->createLDAPAccount(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/LDAPAccount;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 233
    :catch_0
    move-exception v0

    .line 234
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LDAPAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to LDAP Settings service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deleteLDAPAccount(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 271
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LDAPAccountPolicy.deleteLDAPAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 273
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/LDAPAccountPolicy;->getService()Landroid/app/enterprise/ILDAPAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 274
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ILDAPAccountPolicy;->deleteLDAPAccount(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 279
    :goto_0
    return v1

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LDAPAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to LDAP Settings service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 279
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllLDAPAccounts()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/LDAPAccount;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LDAPAccountPolicy.getAllLDAPAccounts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 356
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/LDAPAccountPolicy;->getService()Landroid/app/enterprise/ILDAPAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ILDAPAccountPolicy;->getAllLDAPAccounts(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 362
    :goto_0
    return-object v1

    .line 359
    :catch_0
    move-exception v0

    .line 360
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LDAPAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to LDAP Settings service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLDAPAccount(J)Landroid/app/enterprise/LDAPAccount;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 314
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LDAPAccountPolicy.getLDAPAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 316
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/LDAPAccountPolicy;->getService()Landroid/app/enterprise/ILDAPAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 317
    iget-object v1, p0, Landroid/app/enterprise/LDAPAccountPolicy;->lService:Landroid/app/enterprise/ILDAPAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LDAPAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ILDAPAccountPolicy;->getLDAPAccount(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/LDAPAccount;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 322
    :goto_0
    return-object v1

    .line 319
    :catch_0
    move-exception v0

    .line 320
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LDAPAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to LDAP Settings service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 322
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
