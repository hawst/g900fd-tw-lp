.class public Landroid/app/enterprise/ApplicationPolicy;
.super Ljava/lang/Object;
.source "ApplicationPolicy.java"


# static fields
.field public static final ACTION_EDM_BACKUP_RESULT:Ljava/lang/String; = "edm.intent.action.backup.result"

.field public static final ACTION_EDM_BACKUP_SERVICE_AVAILABLE:Ljava/lang/String; = "edm.intent.action.backup.service.available"

.field public static final ACTION_EDM_DO_BACKUP:Ljava/lang/String; = "edm.intent.action.do.backup"

.field public static final ACTION_EDM_DO_RESTORE:Ljava/lang/String; = "edm.intent.action.do.restore"

.field public static final ACTION_EDM_FORCE_LAUNCHER_REFRESH:Ljava/lang/String; = "edm.intent.action.FORCE_LAUNCHER_REFRESH"

.field public static final ACTION_EDM_RESTORE_RESULT:Ljava/lang/String; = "edm.intent.action.restore.result"

.field public static final ACTION_PREVENT_APPLICATION_START:Ljava/lang/String; = "edm.intent.application.action.prevent.start"

.field public static final ACTION_PREVENT_APPLICATION_STOP:Ljava/lang/String; = "edm.intent.application.action.prevent.stop"

.field public static final APPLICATION_INSTALLATION_MODE_ALLOW:I = 0x1

.field public static final APPLICATION_INSTALLATION_MODE_DISALLOW:I = 0x0

.field public static final APPLICATION_UNINSTALLATION_MODE_ALLOW:I = 0x1

.field public static final APPLICATION_UNINSTALLATION_MODE_DISALLOW:I = 0x0

.field public static final BACKUP_APPLICATION_BUSY_SERVICE:I = -0x3

.field public static final BACKUP_APPLICATION_FAILED:I = -0x2

.field public static final BACKUP_APPLICATION_SERVICE_ERROR:I = -0x1

.field public static final BACKUP_APPLICATION_SUCCESS:I = 0x0

.field public static final EXTRA_ADMIN_PACKAGE_NAME:Ljava/lang/String; = "adminPkgName"

.field public static final EXTRA_APPLICATION_PACKAGE_NAME:Ljava/lang/String; = "application_package_name"

.field public static final EXTRA_BACKUP_DATA:Ljava/lang/String; = "backupData"

.field public static final EXTRA_BACKUP_RESULT:Ljava/lang/String; = "backupResult"

.field public static final EXTRA_ERROR_CLASS:Ljava/lang/String; = "error_class"

.field public static final EXTRA_ERROR_REASON:Ljava/lang/String; = "error_reason"

.field public static final EXTRA_ERROR_TYPE:Ljava/lang/String; = "error_type"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "pkgName"

.field public static final EXTRA_RESTORE_DATA:Ljava/lang/String; = "restoreData"

.field public static final EXTRA_RESTORE_RESULT:Ljava/lang/String; = "restoreResult"

.field public static final EXTRA_USER_ID:Ljava/lang/String; = "user_id"

.field public static final KEY_NON_SYSTEM_UID:Ljava/lang/String; = "edm:nonSystemUid"

.field public static final KEY_STARTED_BY_ADMIN:Ljava/lang/String; = "edm:startedByAdmin"

.field public static final NOTIFICATION_MODE_BLOCK_ALL:I = 0x2

.field public static final NOTIFICATION_MODE_BLOCK_TEXT:I = 0x3

.field public static final NOTIFICATION_MODE_BLOCK_TEXT_AND_SOUND:I = 0x4

.field public static final PKGINFO_SIGNATURE:Ljava/lang/String; = "packageAppSignature"

.field public static final PKGINFO_VERSION:Ljava/lang/String; = "packageAppVersionCode"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IApplicationPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-string v0, "ApplicationPolicy"

    sput-object v0, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    iput-object p1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 265
    return-void
.end method

.method private checkPathAccessSecured(Ljava/lang/String;)V
    .locals 9
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 570
    :try_start_0
    const-string v0, "/sdcard/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 571
    const/4 v0, 0x4

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Application installed from "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "a insecure file path"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V

    .line 589
    :cond_0
    :goto_0
    return-void

    .line 578
    :cond_1
    sget-object v0, Llibcore/io/Libcore;->os:Llibcore/io/Os;

    invoke-interface {v0, p1}, Llibcore/io/Os;->stat(Ljava/lang/String;)Landroid/system/StructStat;

    move-result-object v7

    .line 579
    .local v7, "stat":Landroid/system/StructStat;
    sget v6, Landroid/system/OsConstants;->S_IWOTH:I

    .line 580
    .local v6, "accessBits":I
    iget v0, v7, Landroid/system/StructStat;->st_mode:I

    invoke-static {v0}, Landroid/system/OsConstants;->S_ISREG(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, v7, Landroid/system/StructStat;->st_mode:I

    and-int/2addr v0, v6

    if-ne v0, v6, :cond_0

    .line 581
    const/4 v0, 0x4

    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Application installed from "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "a insecure file path"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Landroid/sec/enterprise/auditlog/AuditLog;->log(IIZILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 586
    .end local v6    # "accessBits":I
    .end local v7    # "stat":Landroid/system/StructStat;
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private getAllUniqueAdminPackageNames(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2473
    .local p1, "infoList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    .local p2, "currentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2475
    .local v3, "tempSet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-nez p2, :cond_0

    .line 2476
    new-instance p2, Ljava/util/ArrayList;

    .end local p2    # "currentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .restart local p2    # "currentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v0, p2

    .line 2493
    .end local p2    # "currentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v0, "currentList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v0

    .line 2480
    .end local v0    # "currentList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local p2    # "currentList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    if-nez p1, :cond_1

    move-object v0, p2

    .line 2481
    .restart local v0    # "currentList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 2484
    .end local v0    # "currentList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v3, p2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2486
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/AppControlInfo;

    .line 2487
    .local v2, "obj":Landroid/app/enterprise/AppControlInfo;
    iget-object v4, v2, Landroid/app/enterprise/AppControlInfo;->entries:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 2488
    iget-object v4, v2, Landroid/app/enterprise/AppControlInfo;->entries:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 2492
    .end local v2    # "obj":Landroid/app/enterprise/AppControlInfo;
    :cond_3
    invoke-interface {p2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object v0, p2

    .line 2493
    .restart local v0    # "currentList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0
.end method

.method private getService()Landroid/app/enterprise/IApplicationPolicy;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    if-nez v0, :cond_0

    .line 271
    const-string v0, "application_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    .line 274
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    return-object v0
.end method


# virtual methods
.method public addAppPackageNameToBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2257
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addAppPackageNameToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2258
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2260
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppPackageNameToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2265
    :goto_0
    return v1

    .line 2261
    :catch_0
    move-exception v0

    .line 2262
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2265
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAppPackageNameToWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2349
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addAppPackageNameToWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2350
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2352
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppPackageNameToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2357
    :goto_0
    return v1

    .line 2353
    :catch_0
    move-exception v0

    .line 2354
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2357
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAppPackageNameToWhiteList(Ljava/lang/String;Z)Z
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "defaultBlackList"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2388
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addAppPackageNameToWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2390
    const/4 v2, 0x1

    .line 2391
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 2393
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addAppPackageNameToWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2394
    const-string v1, ".*"

    .line 2395
    .local v1, "myStr":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addAppPackageNameToBlackList(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2397
    const/4 v2, 0x0

    .line 2398
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addAppPackageNameToWhiteList: failed to add .*"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2402
    .end local v1    # "myStr":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2404
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppPackageNameToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 2409
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 2404
    goto :goto_0

    .line 2405
    :catch_0
    move-exception v0

    .line 2406
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 2409
    goto :goto_0
.end method

.method public addAppPermissionToBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appPermission"    # Ljava/lang/String;

    .prologue
    .line 1858
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addAppPermissionToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1859
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1861
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppPermissionToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1866
    :goto_0
    return v1

    .line 1862
    :catch_0
    move-exception v0

    .line 1863
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1866
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAppSignatureToBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appSignature"    # Ljava/lang/String;

    .prologue
    .line 1974
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addAppSignatureToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1975
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1977
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppSignatureToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1982
    :goto_0
    return v1

    .line 1978
    :catch_0
    move-exception v0

    .line 1979
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1982
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAppSignatureToWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appSignature"    # Ljava/lang/String;

    .prologue
    .line 4486
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addAppSignatureToWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4487
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4489
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppSignatureToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4494
    :goto_0
    return v1

    .line 4490
    :catch_0
    move-exception v0

    .line 4491
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4494
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAppSignatureToWhiteList(Ljava/lang/String;Z)Z
    .locals 7
    .param p1, "appSignature"    # Ljava/lang/String;
    .param p2, "defaultBlackList"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 4523
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addAppSignatureToWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4525
    const/4 v2, 0x1

    .line 4526
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 4528
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addAppSignatureToWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4530
    const-string v1, "*"

    .line 4531
    .local v1, "myStr":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addAppSignatureToBlackList(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 4533
    const/4 v2, 0x0

    .line 4534
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addAppSignatureToWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4538
    .end local v1    # "myStr":Ljava/lang/String;
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 4540
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppSignatureToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 4545
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 4540
    goto :goto_0

    .line 4541
    :catch_0
    move-exception v0

    .line 4542
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 4545
    goto :goto_0
.end method

.method public addHomeShortcut(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "homePkgName"    # Ljava/lang/String;

    .prologue
    .line 3923
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addHomeShortcut"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3924
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3926
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->addHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3931
    :goto_0
    return v1

    .line 3927
    :catch_0
    move-exception v0

    .line 3928
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed addHomeShorcut!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3931
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToClearCacheBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5078
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToClearCacheBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5079
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5081
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5086
    :goto_0
    return v1

    .line 5082
    :catch_0
    move-exception v0

    .line 5083
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5086
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToClearCacheWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5246
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToClearCacheWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5247
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5249
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5254
    :goto_0
    return v1

    .line 5250
    :catch_0
    move-exception v0

    .line 5251
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5254
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToClearDataBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4719
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToClearDataBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4720
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4722
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4727
    :goto_0
    return v1

    .line 4723
    :catch_0
    move-exception v0

    .line 4724
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4727
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToClearDataWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4887
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToClearDataWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4888
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4890
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4895
    :goto_0
    return v1

    .line 4891
    :catch_0
    move-exception v0

    .line 4892
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4895
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToDisableClipboardBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5905
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToDisableClipboardBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5906
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5908
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5913
    :goto_0
    return v1

    .line 5909
    :catch_0
    move-exception v0

    .line 5910
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5913
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToDisableClipboardWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5971
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToDisableClipboardWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5972
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5974
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5979
    :goto_0
    return v1

    .line 5975
    :catch_0
    move-exception v0

    .line 5976
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5979
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToDisableClipboardWhiteList(Ljava/util/List;Z)Z
    .locals 7
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5996
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToDisableClipboardWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5998
    const/4 v2, 0x1

    .line 5999
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 6001
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToDisableClipboardWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6002
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 6003
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6004
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToDisableClipboardBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 6006
    const/4 v2, 0x0

    .line 6007
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addPackagesToDisableClipboardWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6011
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 6013
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 6018
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 6013
    goto :goto_0

    .line 6014
    :catch_0
    move-exception v0

    .line 6015
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 6018
    goto :goto_0
.end method

.method public addPackagesToDisableUpdateBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5420
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToDisableUpdateBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5421
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5423
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5428
    :goto_0
    return v1

    .line 5424
    :catch_0
    move-exception v0

    .line 5425
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5428
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToDisableUpdateWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5468
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToDisableUpdateWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5469
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5471
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5476
    :goto_0
    return v1

    .line 5472
    :catch_0
    move-exception v0

    .line 5473
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5476
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToDisableUpdateWhiteList(Ljava/util/List;Z)Z
    .locals 7
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 5494
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToDisableUpdateWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5496
    const/4 v2, 0x1

    .line 5497
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 5499
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPermissionControlPolicy.addPackagesToPermissionWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5500
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 5501
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5502
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToDisableUpdateBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 5504
    const/4 v2, 0x0

    .line 5505
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addPackagesToDisableUpdateWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5509
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 5511
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 5516
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 5511
    goto :goto_0

    .line 5512
    :catch_0
    move-exception v0

    .line 5513
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 5516
    goto :goto_0
.end method

.method public addPackagesToForceStopBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4362
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToForceStopBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4363
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4365
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToForceStopBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4370
    :goto_0
    return v1

    .line 4366
    :catch_0
    move-exception v0

    .line 4367
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4370
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToForceStopWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2710
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToForceStopWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2711
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2713
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToForceStopWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2718
    :goto_0
    return v1

    .line 2714
    :catch_0
    move-exception v0

    .line 2715
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2718
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToForceStopWhiteList(Ljava/util/List;Z)Z
    .locals 7
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2771
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToForceStopWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2773
    const/4 v2, 0x1

    .line 2775
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 2777
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToPermissionWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2778
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2779
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2780
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToForceStopBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2782
    const/4 v2, 0x0

    .line 2783
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addPackagesToForceStopWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2787
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2789
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToForceStopWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 2794
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 2789
    goto :goto_0

    .line 2790
    :catch_0
    move-exception v0

    .line 2791
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 2794
    goto :goto_0
.end method

.method public addPackagesToNotificationBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3321
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToNotificationBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3322
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3324
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3329
    :goto_0
    return v1

    .line 3325
    :catch_0
    move-exception v0

    .line 3326
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3329
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToNotificationWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3476
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToNotificationWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3477
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3479
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3484
    :goto_0
    return v1

    .line 3480
    :catch_0
    move-exception v0

    .line 3481
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3484
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToNotificationWhiteList(Ljava/util/List;Z)Z
    .locals 7
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 3537
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToNotificationWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3539
    const/4 v2, 0x1

    .line 3540
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 3542
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToNotificationWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3543
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 3544
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3545
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToNotificationBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 3547
    const/4 v2, 0x0

    .line 3548
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addPackagesToNotificationWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3552
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 3554
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 3559
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 3554
    goto :goto_0

    .line 3555
    :catch_0
    move-exception v0

    .line 3556
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 3559
    goto :goto_0
.end method

.method public addPackagesToPreventStartBlackList(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5727
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToPreventStartBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5728
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5730
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToPreventStartBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5735
    :goto_0
    return-object v1

    .line 5731
    :catch_0
    move-exception v0

    .line 5732
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5735
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToWidgetBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3122
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToWidgetBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3123
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3125
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToWidgetBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3130
    :goto_0
    return v1

    .line 3126
    :catch_0
    move-exception v0

    .line 3127
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3130
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToWidgetWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2913
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addPackagesToWidgetWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2914
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2916
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToWidgetWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2921
    :goto_0
    return v1

    .line 2917
    :catch_0
    move-exception v0

    .line 2918
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2921
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToWidgetWhiteList(Ljava/util/List;Z)Z
    .locals 7
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2972
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToWidgetWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2974
    const/4 v2, 0x1

    .line 2975
    .local v2, "r1":Z
    if-ne p2, v3, :cond_0

    .line 2977
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.addPackagesToWidgetWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2978
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2979
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2980
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ApplicationPolicy;->addPackagesToWidgetBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2982
    const/4 v2, 0x0

    .line 2983
    sget-object v5, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPolicy.addPackagesToWidgetWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2987
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2989
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToWidgetWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 2994
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 2989
    goto :goto_0

    .line 2990
    :catch_0
    move-exception v0

    .line 2991
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 2994
    goto :goto_0
.end method

.method public addUsbDevicesForDefaultAccess(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/UsbDeviceConfig;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 6236
    .local p2, "deviceConfig":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.addUsbDevicesForDefaultAccess"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6238
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6239
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->addUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6244
    :goto_0
    return v1

    .line 6241
    :catch_0
    move-exception v0

    .line 6242
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6244
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public backupApplicationData(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 4130
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.backupApplicationData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4131
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4133
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->backupApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4138
    :goto_0
    return v1

    .line 4134
    :catch_0
    move-exception v0

    .line 4135
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed backup App Data"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4138
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public changeApplicationIcon(Ljava/lang/String;[B)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "imageData"    # [B

    .prologue
    const/4 v1, 0x0

    .line 1769
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeApplicationIcon:packageName "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1770
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.changeApplicationIcon"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1771
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1774
    if-eqz p2, :cond_1

    :try_start_0
    array-length v2, p2

    const/high16 v3, 0x100000

    if-le v2, v3, :cond_1

    .line 1775
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeApplicationIcon: Icon size is too big :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, p2

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1784
    :cond_0
    :goto_0
    return v1

    .line 1779
    :cond_1
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->changeApplicationIcon(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1780
    :catch_0
    move-exception v0

    .line 1781
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with application policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public changeApplicationName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "newName"    # Ljava/lang/String;

    .prologue
    .line 5385
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.changeApplicationName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5386
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5388
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->changeApplicationName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5393
    :goto_0
    return v1

    .line 5389
    :catch_0
    move-exception v0

    .line 5390
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5393
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearAppPackageNameFromList()Z
    .locals 11

    .prologue
    const/4 v8, 0x0

    .line 2431
    iget-object v9, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v10, "ApplicationPolicy.clearAppPackageNameFromList"

    invoke-static {v9, v10}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2433
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getAppPackageNamesAllBlackLists()Ljava/util/List;

    move-result-object v3

    .line 2434
    .local v3, "myList1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getAppPackageNamesAllWhiteLists()Ljava/util/List;

    move-result-object v4

    .line 2436
    .local v4, "myList2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2437
    .local v1, "blackListObj":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2439
    .local v7, "whiteListObj":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x1

    .line 2440
    .local v5, "r1":Z
    const/4 v6, 0x1

    .line 2442
    .local v6, "r2":Z
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 2462
    :cond_0
    :goto_0
    return v8

    .line 2445
    :cond_1
    invoke-direct {p0, v3, v1}, Landroid/app/enterprise/ApplicationPolicy;->getAllUniqueAdminPackageNames(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 2446
    invoke-direct {p0, v4, v7}, Landroid/app/enterprise/ApplicationPolicy;->getAllUniqueAdminPackageNames(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    .line 2448
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2451
    .local v0, "Obj":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/ApplicationPolicy;->removeAppPackageNameFromBlackList(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 2452
    const/4 v5, 0x0

    goto :goto_1

    .line 2455
    .end local v0    # "Obj":Ljava/lang/String;
    :cond_3
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2458
    .restart local v0    # "Obj":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/ApplicationPolicy;->removeAppPackageNameFromWhiteList(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 2459
    const/4 v6, 0x0

    goto :goto_2

    .line 2462
    .end local v0    # "Obj":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    const/4 v8, 0x1

    goto :goto_0
.end method

.method public clearAppSignatureFromList()Z
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 4563
    iget-object v10, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v11, "ApplicationPermissionControlPolicy.clearAppSignatureFromList "

    invoke-static {v10, v11}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4565
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getAppSignaturesBlackList()[Ljava/lang/String;

    move-result-object v1

    .line 4566
    .local v1, "blackList":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getAppSignaturesWhiteList()[Ljava/lang/String;

    move-result-object v7

    .line 4568
    .local v7, "whiteList":[Ljava/lang/String;
    const/4 v5, 0x1

    .line 4569
    .local v5, "r1":Z
    const/4 v6, 0x1

    .line 4571
    .local v6, "r2":Z
    if-eqz v1, :cond_0

    if-nez v7, :cond_1

    .line 4584
    :cond_0
    :goto_0
    return v9

    .line 4574
    :cond_1
    move-object v0, v1

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v2, v0, v3

    .line 4575
    .local v2, "blackListObj":Ljava/lang/String;
    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->removeAppSignatureFromBlackList(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 4576
    const/4 v5, 0x0

    .line 4574
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 4579
    .end local v2    # "blackListObj":Ljava/lang/String;
    :cond_3
    move-object v0, v7

    array-length v4, v0

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v4, :cond_5

    aget-object v8, v0, v3

    .line 4580
    .local v8, "whiteListObj":Ljava/lang/String;
    invoke-virtual {p0, v8}, Landroid/app/enterprise/ApplicationPolicy;->removeAppSignatureFromWhiteList(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_4

    .line 4581
    const/4 v6, 0x0

    .line 4579
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 4584
    .end local v8    # "whiteListObj":Ljava/lang/String;
    :cond_5
    if-eqz v5, :cond_0

    if-eqz v6, :cond_0

    const/4 v9, 0x1

    goto :goto_0
.end method

.method public clearDisableClipboardBlackList()Z
    .locals 3

    .prologue
    .line 6089
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearDisableClipboardBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6090
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6092
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->clearDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6097
    :goto_0
    return v1

    .line 6093
    :catch_0
    move-exception v0

    .line 6094
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6097
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearDisableClipboardWhiteList()Z
    .locals 3

    .prologue
    .line 6108
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearDisableClipboardWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6109
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6111
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->clearDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6116
    :goto_0
    return v1

    .line 6112
    :catch_0
    move-exception v0

    .line 6113
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6116
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearDisableUpdateBlackList()Z
    .locals 3

    .prologue
    .line 5573
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearDisableUpdateBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5574
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5576
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->clearDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5581
    :goto_0
    return v1

    .line 5577
    :catch_0
    move-exception v0

    .line 5578
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5581
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearDisableUpdateWhiteList()Z
    .locals 3

    .prologue
    .line 5589
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearDisableUpdateWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5590
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5592
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->clearDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5597
    :goto_0
    return v1

    .line 5593
    :catch_0
    move-exception v0

    .line 5594
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5597
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearPackagesFromDisableClipboardList()Z
    .locals 4

    .prologue
    .line 6030
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.clearPackagesFromDisableClipboardList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6032
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->clearDisableClipboardBlackList()Z

    move-result v0

    .line 6033
    .local v0, "r1":Z
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->clearDisableClipboardWhiteList()Z

    move-result v1

    .line 6034
    .local v1, "r2":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearPackagesFromDisableUpdateList()Z
    .locals 4

    .prologue
    .line 5528
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.clearPackagesFromDisableUpdateList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5530
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->clearDisableUpdateBlackList()Z

    move-result v0

    .line 5531
    .local v0, "r1":Z
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->clearDisableUpdateWhiteList()Z

    move-result v1

    .line 5533
    .local v1, "r2":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearPackagesFromForceStopList()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 2806
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPolicy.clearPackagesFromForceStopList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2808
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromForceStopBlackList()Ljava/util/List;

    move-result-object v0

    .line 2809
    .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromForceStopWhiteList()Ljava/util/List;

    move-result-object v3

    .line 2811
    .local v3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    .line 2816
    :cond_0
    :goto_0
    return v4

    .line 2814
    :cond_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromForceStopBlackList(Ljava/util/List;)Z

    move-result v1

    .line 2815
    .local v1, "r1":Z
    invoke-virtual {p0, v3}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromForceStopWhiteList(Ljava/util/List;)Z

    move-result v2

    .line 2816
    .local v2, "r2":Z
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public clearPackagesFromNotificationList()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 3573
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPermissionControlPolicy.clearPackagesFromNotificationList "

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3575
    const/4 v1, 0x1

    .line 3576
    .local v1, "r1":Z
    const/4 v2, 0x1

    .line 3577
    .local v2, "r2":Z
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromNotificationBlackList()Ljava/util/List;

    move-result-object v0

    .line 3578
    .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromNotificationWhiteList()Ljava/util/List;

    move-result-object v3

    .line 3580
    .local v3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    .line 3589
    :cond_0
    :goto_0
    return v4

    .line 3583
    :cond_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromNotificationBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 3584
    const/4 v1, 0x0

    .line 3586
    :cond_2
    invoke-virtual {p0, v3}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromNotificationWhiteList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3587
    const/4 v2, 0x0

    .line 3589
    :cond_3
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public clearPackagesFromWidgetList()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 3008
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPermissionControlPolicy.clearPackagesFromWidgetList "

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3010
    const/4 v1, 0x1

    .line 3011
    .local v1, "r1":Z
    const/4 v2, 0x1

    .line 3013
    .local v2, "r2":Z
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromWidgetBlackList()Ljava/util/List;

    move-result-object v0

    .line 3014
    .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPolicy;->getPackagesFromWidgetWhiteList()Ljava/util/List;

    move-result-object v3

    .line 3016
    .local v3, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    .line 3025
    :cond_0
    :goto_0
    return v4

    .line 3019
    :cond_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromWidgetBlackList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 3020
    const/4 v1, 0x0

    .line 3022
    :cond_2
    invoke-virtual {p0, v3}, Landroid/app/enterprise/ApplicationPolicy;->removePackagesFromWidgetWhiteList(Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3023
    const/4 v2, 0x0

    .line 3025
    :cond_3
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    const/4 v4, 0x1

    goto :goto_0
.end method

.method public clearPreventStartBlackList()Z
    .locals 3

    .prologue
    .line 5862
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearPreventStartBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5863
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5865
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->clearPreventStartBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5870
    :goto_0
    return v1

    .line 5866
    :catch_0
    move-exception v0

    .line 5867
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5870
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearUsbDevicesForDefaultAccess(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 6289
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.clearUsbDevicesForDefaultAccess"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6291
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6292
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->clearUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6297
    :goto_0
    return v1

    .line 6294
    :catch_0
    move-exception v0

    .line 6295
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6297
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteHomeShortcut(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "homePkgName"    # Ljava/lang/String;

    .prologue
    .line 3866
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.deleteHomeShortcut"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3867
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3869
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->deleteHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3874
    :goto_0
    return v1

    .line 3870
    :catch_0
    move-exception v0

    .line 3871
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed deleteHomeShorcut!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3874
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteManagedAppInfo(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 1098
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.deleteManagedAppInfo"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1099
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1101
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->deleteManagedAppInfo(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1106
    :goto_0
    return v1

    .line 1102
    :catch_0
    move-exception v0

    .line 1103
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1106
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableAndroidBrowser()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1629
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.disableAndroidBrowser"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1630
    const-string v0, "com.android.browser"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1631
    const-string v0, "com.sec.webbrowserminiapp"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1632
    const-string v0, "com.android.chrome"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1633
    const-string v0, "com.sec.android.app.sbrowser"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1634
    const-string v0, "com.android.browser.provider"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1635
    sget-object v0, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v1, "set all browser as disable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1636
    return-void
.end method

.method public disableAndroidMarket()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1426
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.disableAndroidMarket"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1427
    const-string v0, "com.android.vending"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1428
    const-string v0, "com.google.android.finsky"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1429
    return-void
.end method

.method public disableVoiceDialer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1673
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.disableVoiceDialer"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1674
    const-string v0, "com.android.voicedialer"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1675
    const-string v0, "com.vlingo.client.samsung"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1676
    const-string v0, "com.vlingo.midas"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1677
    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1678
    return-void
.end method

.method public disableYouTube()V
    .locals 2

    .prologue
    .line 1595
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.disableYouTube"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1596
    const-string v0, "com.google.android.youtube"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1597
    return-void
.end method

.method public enableAndroidBrowser()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1651
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.enableAndroidBrowser"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1652
    const-string v0, "com.android.browser"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1653
    const-string v0, "com.sec.webbrowserminiapp"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1654
    const-string v0, "com.android.chrome"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1655
    const-string v0, "com.sec.android.app.sbrowser"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1656
    const-string v0, "com.android.browser.provider"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1657
    sget-object v0, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v1, "set all browser as enable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1658
    return-void
.end method

.method public enableAndroidMarket()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1447
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.enableAndroidMarket"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1448
    const-string v2, "com.android.vending"

    invoke-virtual {p0, v2, v4}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1449
    const-string v2, "com.google.android.finsky"

    invoke-virtual {p0, v2, v4}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1452
    :try_start_0
    const-string v2, "persona"

    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/os/IPersonaManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IPersonaManager;

    move-result-object v1

    .line 1454
    .local v1, "persona":Landroid/os/IPersonaManager;
    if-eqz v1, :cond_1

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-interface {v1, v2}, Landroid/os/IPersonaManager;->exists(I)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v2, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v2}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/os/IPersonaManager;->exists(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1457
    :cond_0
    const-string v2, "com.google.android.gm"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1480
    .end local v1    # "persona":Landroid/os/IPersonaManager;
    :cond_1
    :goto_0
    return-void

    .line 1459
    .restart local v1    # "persona":Landroid/os/IPersonaManager;
    :cond_2
    const-string v2, "com.google.android.gms"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1461
    const-string v2, "com.android.vending"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1463
    const-string v2, "com.google.android.gsf.login"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1465
    const-string v2, "com.google.android.setupwizard"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1467
    const-string v2, "com.google.android.gsf"

    invoke-virtual {p0, v2}, Landroid/app/enterprise/ApplicationPolicy;->isApplicationInstalled(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1470
    const-string v2, "com.google.android.gm"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1471
    const-string v2, "com.google.android.gms"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1472
    const-string v2, "com.google.android.gsf.login"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1473
    const-string v2, "com.google.android.setupwizard"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1474
    const-string v2, "com.google.android.gsf"

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1476
    .end local v1    # "persona":Landroid/os/IPersonaManager;
    :catch_0
    move-exception v0

    .line 1477
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Remote exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public enableVoiceDialer()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1693
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.enableVoiceDialer"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1694
    const-string v0, "com.android.voicedialer"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1695
    const-string v0, "com.vlingo.client.samsung"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1696
    const-string v0, "com.vlingo.midas"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1697
    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1698
    return-void
.end method

.method public enableYouTube()V
    .locals 2

    .prologue
    .line 1612
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ApplicationPolicy.enableYouTube"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1613
    const-string v0, "com.google.android.youtube"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/ApplicationPolicy;->setApplicationState(Ljava/lang/String;Z)Z

    .line 1614
    return-void
.end method

.method public getAddHomeShorcutRequested()Z
    .locals 3

    .prologue
    .line 6410
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6412
    :try_start_0
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "getAddHomeShorcutRequested"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6413
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IApplicationPolicy;->getAddHomeShorcutRequested()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6419
    :goto_0
    return v1

    .line 6414
    :catch_0
    move-exception v0

    .line 6415
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6419
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllAppLastUsage()[Landroid/app/enterprise/AppInfoLastUsage;
    .locals 3

    .prologue
    .line 1739
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAllAppLastUsage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1740
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1742
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAllAppLastUsage(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/AppInfoLastUsage;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1747
    :goto_0
    return-object v1

    .line 1743
    :catch_0
    move-exception v0

    .line 1744
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1747
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Landroid/app/enterprise/AppInfoLastUsage;

    goto :goto_0
.end method

.method public getAllWidgets(Ljava/lang/String;)Ljava/util/Map;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 4178
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAllWidgets"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4179
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4181
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getAllWidgets(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4186
    :goto_0
    return-object v1

    .line 4182
    :catch_0
    move-exception v0

    .line 4183
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4186
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    goto :goto_0
.end method

.method public getAppInstallToSdCard()Z
    .locals 3

    .prologue
    .line 4283
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4285
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppInstallToSdCard(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4290
    :goto_0
    return v1

    .line 4286
    :catch_0
    move-exception v0

    .line 4287
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4290
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAppPackageNamesAllBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2312
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppPackageNamesAllBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2313
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2315
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppPackageNamesAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2320
    :goto_0
    return-object v1

    .line 2316
    :catch_0
    move-exception v0

    .line 2317
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2320
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAppPackageNamesAllWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2539
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppPackageNamesAllWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2540
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2542
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppPackageNamesAllWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2547
    :goto_0
    return-object v1

    .line 2543
    :catch_0
    move-exception v0

    .line 2544
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2547
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAppPermissionsAllBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1942
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppPermissionsAllBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1943
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1945
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppPermissionsAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1950
    :goto_0
    return-object v1

    .line 1946
    :catch_0
    move-exception v0

    .line 1947
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1950
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAppPermissionsBlackList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1915
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1917
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppPermissionsBlackList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1922
    :goto_0
    return-object v1

    .line 1918
    :catch_0
    move-exception v0

    .line 1919
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1922
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getAppSignaturesAllBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2053
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppSignaturesAllBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2054
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2056
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppSignaturesAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2061
    :goto_0
    return-object v1

    .line 2057
    :catch_0
    move-exception v0

    .line 2058
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2061
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAppSignaturesAllWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4651
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAppSignaturesAllWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4652
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4654
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppSignaturesAllWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4659
    :goto_0
    return-object v1

    .line 4655
    :catch_0
    move-exception v0

    .line 4656
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4659
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAppSignaturesBlackList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 2028
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2030
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppSignatureBlackList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2035
    :goto_0
    return-object v1

    .line 2031
    :catch_0
    move-exception v0

    .line 2032
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2035
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getAppSignaturesWhiteList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 4627
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4629
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppSignaturesWhiteList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4634
    :goto_0
    return-object v1

    .line 4630
    :catch_0
    move-exception v0

    .line 4631
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4634
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getApplicationCacheSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1350
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationCacheSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1351
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1353
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationCacheSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1358
    :goto_0
    return-wide v2

    .line 1354
    :catch_0
    move-exception v0

    .line 1355
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1358
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationCodeSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1308
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationCodeSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1309
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1311
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationCodeSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1316
    :goto_0
    return-wide v2

    .line 1312
    :catch_0
    move-exception v0

    .line 1313
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1316
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationComponentState(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "compName"    # Landroid/content/ComponentName;

    .prologue
    .line 5662
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5664
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationComponentState(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5669
    :goto_0
    return v1

    .line 5665
    :catch_0
    move-exception v0

    .line 5666
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5669
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationCpuUsage(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1400
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationCpuUsage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1401
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1403
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationCpuUsage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1408
    :goto_0
    return-wide v2

    .line 1404
    :catch_0
    move-exception v0

    .line 1405
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1408
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationDataSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1329
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationDataSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1330
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1332
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationDataSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1337
    :goto_0
    return-wide v2

    .line 1333
    :catch_0
    move-exception v0

    .line 1334
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationIconFromDb(Ljava/lang/String;)[B
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1793
    const/4 v1, 0x0

    .line 1794
    .local v1, "imageData":[B
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1796
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationIconFromDb(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[B
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    move-object v2, v1

    .line 1804
    :goto_0
    return-object v2

    .line 1797
    :catch_0
    move-exception v0

    .line 1798
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getCustomApplicationIcon: Failed talking with Application control policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1801
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getApplicationInstallationEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 1122
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1124
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationInstallationEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1129
    :goto_0
    return v1

    .line 1125
    :catch_0
    move-exception v0

    .line 1126
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1129
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationInstallationMode()I
    .locals 3

    .prologue
    .line 861
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 863
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAppInstallationMode(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 868
    :goto_0
    return v1

    .line 864
    :catch_0
    move-exception v0

    .line 865
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 868
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationMemoryUsage(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1375
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationMemoryUsage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1376
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1378
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationMemoryUsage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1383
    :goto_0
    return-wide v2

    .line 1379
    :catch_0
    move-exception v0

    .line 1380
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1383
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1194
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1195
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1197
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1202
    :goto_0
    return-object v1

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1202
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getApplicationNameFromDb(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 5401
    const/4 v1, 0x0

    .line 5402
    .local v1, "name":Ljava/lang/String;
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 5404
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationNameFromDb(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    move-object v2, v1

    .line 5412
    :goto_0
    return-object v2

    .line 5405
    :catch_0
    move-exception v0

    .line 5406
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getCustomApplicationName: Failed talking with Application control policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5409
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getApplicationNetworkStats()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/NetworkStats;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1822
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationNetworkStats"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1823
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1825
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getNetworkStats(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1830
    :goto_0
    return-object v1

    .line 1826
    :catch_0
    move-exception v0

    .line 1827
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1830
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getApplicationNotificationMode()I
    .locals 4

    .prologue
    .line 3773
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationNotificationMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3774
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3776
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationNotificationMode(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3781
    :goto_0
    return v1

    .line 3777
    :catch_0
    move-exception v0

    .line 3778
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3781
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getApplicationNotificationModeAsUser(I)I
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 3791
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3793
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationNotificationModeAsUser(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3798
    :goto_0
    return v1

    .line 3794
    :catch_0
    move-exception v0

    .line 3795
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3798
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getApplicationStateEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1064
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1066
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationStateEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1071
    :goto_0
    return v1

    .line 1067
    :catch_0
    move-exception v0

    .line 1068
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1071
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationStateList(Z)[Ljava/lang/String;
    .locals 3
    .param p1, "state"    # Z

    .prologue
    .line 2168
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationStateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2169
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2171
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationStateList(Landroid/app/enterprise/ContextInfo;Z)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2176
    :goto_0
    return-object v1

    .line 2172
    :catch_0
    move-exception v0

    .line 2173
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2176
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getApplicationTotalSize(Ljava/lang/String;)J
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1287
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationTotalSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1288
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1290
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationTotalSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1295
    :goto_0
    return-wide v2

    .line 1291
    :catch_0
    move-exception v0

    .line 1292
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1295
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getApplicationUid(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1217
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationUid"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1218
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1220
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationUid(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1225
    :goto_0
    return v1

    .line 1221
    :catch_0
    move-exception v0

    .line 1222
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1225
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getApplicationUninstallationEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 1146
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationUninstallationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1147
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1149
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationUninstallationEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1154
    :goto_0
    return v1

    .line 1150
    :catch_0
    move-exception v0

    .line 1151
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1154
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationUninstallationMode()I
    .locals 3

    .prologue
    .line 2600
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationUninstallationMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2601
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2603
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationUninstallationMode(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2609
    :goto_0
    return v1

    .line 2604
    :catch_0
    move-exception v0

    .line 2605
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2609
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getApplicationVersion(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1241
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationVersion"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1242
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1244
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationVersion(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1249
    :goto_0
    return-object v1

    .line 1245
    :catch_0
    move-exception v0

    .line 1246
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1249
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getApplicationVersionCode(Ljava/lang/String;)I
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1265
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getApplicationVersionCode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1266
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1268
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationVersionCode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1273
    :goto_0
    return v1

    .line 1269
    :catch_0
    move-exception v0

    .line 1270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1273
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getAvgNoAppUsagePerMonth()[Landroid/app/enterprise/AppInfoLastUsage;
    .locals 3

    .prologue
    .line 1713
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getAvgNoAppUsagePerMonth"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1714
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1716
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getAvgNoAppUsagePerMonth(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/AppInfoLastUsage;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1721
    :goto_0
    return-object v1

    .line 1717
    :catch_0
    move-exception v0

    .line 1718
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1721
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Landroid/app/enterprise/AppInfoLastUsage;

    goto :goto_0
.end method

.method public getDisabledPackages(I)Ljava/util/List;
    .locals 3
    .param p1, "ownerUid"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6358
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getDisabledPackages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6359
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6361
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IApplicationPolicy;->getDisabledPackages(I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 6367
    :goto_0
    return-object v1

    .line 6362
    :catch_0
    move-exception v0

    .line 6363
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getHomeShortcuts(Ljava/lang/String;Z)Ljava/util/List;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "includeHotSeat"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4226
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getHomeShortcuts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4227
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4229
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->getHomeShortcuts(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4234
    :goto_0
    return-object v1

    .line 4230
    :catch_0
    move-exception v0

    .line 4231
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4234
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getInstalledApplicationsIDList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1170
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getInstalledApplicationsIDList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1171
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1173
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getInstalledApplicationsIDList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1178
    :goto_0
    return-object v1

    .line 1174
    :catch_0
    move-exception v0

    .line 1175
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1178
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getInstalledManagedApplicationsList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 765
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getInstalledManagedApplicationsList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 766
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 768
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getInstalledManagedApplicationsList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 773
    :goto_0
    return-object v1

    .line 769
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 773
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getManagedApplicationStatus(Ljava/lang/String;)[Landroid/app/enterprise/ManagedAppInfo;
    .locals 3
    .param p1, "pkg"    # Ljava/lang/String;

    .prologue
    .line 800
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getManagedApplicationStatus"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 801
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 803
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationsList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[Landroid/app/enterprise/ManagedAppInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 808
    :goto_0
    return-object v1

    .line 804
    :catch_0
    move-exception v0

    .line 805
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 808
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Landroid/app/enterprise/ManagedAppInfo;

    goto :goto_0
.end method

.method public getMostCpuUsageApps(IZ)Ljava/util/List;
    .locals 3
    .param p1, "appCount"    # I
    .param p2, "showAllProcess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1504
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getMostCpuUsageApps"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1505
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1507
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->getTopNCPUUsageApp(Landroid/app/enterprise/ContextInfo;IZ)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1513
    :goto_0
    return-object v1

    .line 1509
    :catch_0
    move-exception v0

    .line 1510
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1513
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getMostDataUsageApps(I)Ljava/util/List;
    .locals 3
    .param p1, "appCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1571
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getMostDataUsageApps"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1572
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1574
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getTopNDataUsageApp(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1579
    :goto_0
    return-object v1

    .line 1575
    :catch_0
    move-exception v0

    .line 1576
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1579
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getMostMemoryUsageApps(IZ)Ljava/util/List;
    .locals 3
    .param p1, "appCount"    # I
    .param p2, "showAllProcess"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1540
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getMostMemoryUsageApps"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1541
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1543
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->getTopNMemoryUsageApp(Landroid/app/enterprise/ContextInfo;IZ)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1548
    :goto_0
    return-object v1

    .line 1544
    :catch_0
    move-exception v0

    .line 1545
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1548
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromClearCacheBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5135
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromClearCacheBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5136
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5138
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5143
    :goto_0
    return-object v1

    .line 5139
    :catch_0
    move-exception v0

    .line 5140
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5143
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromClearCacheWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5298
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromClearCacheWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5299
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5301
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5306
    :goto_0
    return-object v1

    .line 5302
    :catch_0
    move-exception v0

    .line 5303
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5306
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromClearDataBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4776
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromClearDataBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4777
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4779
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4784
    :goto_0
    return-object v1

    .line 4780
    :catch_0
    move-exception v0

    .line 4781
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4784
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromClearDataWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4939
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromClearDataWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4940
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4942
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 4947
    :goto_0
    return-object v1

    .line 4943
    :catch_0
    move-exception v0

    .line 4944
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4947
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromDisableClipboardBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5948
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromDisableClipboardBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5949
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5951
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5956
    :goto_0
    return-object v1

    .line 5952
    :catch_0
    move-exception v0

    .line 5953
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5956
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromDisableClipboardWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6070
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromDisableClipboardWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6071
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6073
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 6078
    :goto_0
    return-object v1

    .line 6074
    :catch_0
    move-exception v0

    .line 6075
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6078
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromDisableUpdateBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5436
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromDisableUpdateBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5437
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5439
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5444
    :goto_0
    return-object v1

    .line 5440
    :catch_0
    move-exception v0

    .line 5441
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5444
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromDisableUpdateWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5541
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromDisableUpdateWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5542
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5544
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5549
    :goto_0
    return-object v1

    .line 5545
    :catch_0
    move-exception v0

    .line 5546
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5549
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromForceStopBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2650
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromForceStopBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2651
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2653
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromForceStopBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2658
    :goto_0
    return-object v1

    .line 2654
    :catch_0
    move-exception v0

    .line 2655
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2658
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromForceStopWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2856
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromForceStopWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2857
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2859
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromForceStopWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2864
    :goto_0
    return-object v1

    .line 2860
    :catch_0
    move-exception v0

    .line 2861
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2864
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromNotificationBlackList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3418
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromNotificationBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3419
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3421
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IApplicationPolicy;->getAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3426
    :goto_0
    return-object v1

    .line 3422
    :catch_0
    move-exception v0

    .line 3423
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3426
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromNotificationWhiteList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3679
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromNotificationWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3680
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3682
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IApplicationPolicy;->getAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3687
    :goto_0
    return-object v1

    .line 3683
    :catch_0
    move-exception v0

    .line 3684
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3687
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromPreventStartBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 5775
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromPreventStartBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5776
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5778
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromPreventStartBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 5783
    :goto_0
    return-object v1

    .line 5779
    :catch_0
    move-exception v0

    .line 5780
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5783
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPackagesFromWidgetBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3168
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromWidgetBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3169
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3171
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromWidgetBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3176
    :goto_0
    return-object v1

    .line 3172
    :catch_0
    move-exception v0

    .line 3173
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3176
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromWidgetWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3063
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getPackagesFromWidgetWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3064
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3066
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromWidgetWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3071
    :goto_0
    return-object v1

    .line 3067
    :catch_0
    move-exception v0

    .line 3068
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3071
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getUsbDevicesForDefaultAccess(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/UsbDeviceConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 6175
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.getUsbDevicesForDefaultAccess"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6177
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6178
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->getUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 6183
    :goto_0
    return-object v1

    .line 6180
    :catch_0
    move-exception v0

    .line 6181
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6183
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public installApplication(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 6316
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.installApplication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6317
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6319
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->installExistingApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6325
    :goto_0
    return v1

    .line 6321
    :catch_0
    move-exception v0

    .line 6322
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6325
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public installApplication(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "apkFilePath"    # Ljava/lang/String;
    .param p2, "installOnSDCard"    # Z

    .prologue
    .line 526
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.installApplication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 527
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 529
    :try_start_0
    invoke-direct {p0, p1}, Landroid/app/enterprise/ApplicationPolicy;->checkPathAccessSecured(Ljava/lang/String;)V

    .line 530
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->installApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 535
    :goto_0
    return v1

    .line 531
    :catch_0
    move-exception v0

    .line 532
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 535
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isApplicationClearCacheDisabled(Ljava/lang/String;IZ)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "showMsg"    # Z

    .prologue
    .line 5370
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5372
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationClearCacheDisabled(Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5377
    :goto_0
    return v1

    .line 5373
    :catch_0
    move-exception v0

    .line 5374
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5377
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isApplicationClearDataDisabled(Ljava/lang/String;IZ)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "showMsg"    # Z

    .prologue
    .line 5011
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5013
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationClearDataDisabled(Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5018
    :goto_0
    return v1

    .line 5014
    :catch_0
    move-exception v0

    .line 5015
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5018
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isApplicationForceStopDisabled(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "errorType"    # Ljava/lang/String;
    .param p4, "errorClass"    # Ljava/lang/String;
    .param p5, "errorReason"    # Ljava/lang/String;
    .param p6, "showMsg"    # Z

    .prologue
    .line 4303
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 4305
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationForceStopDisabled(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 4311
    :goto_0
    return v0

    .line 4307
    :catch_0
    move-exception v7

    .line 4308
    .local v7, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with application policy"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4311
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isApplicationInstalled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 466
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 468
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationInstalled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 473
    :goto_0
    return v1

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 473
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isApplicationRunning(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 494
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 496
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationRunning(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 501
    :goto_0
    return v1

    .line 497
    :catch_0
    move-exception v0

    .line 498
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 501
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isApplicationStartDisabledAsUser(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 5883
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5885
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationStartDisabledAsUser(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5890
    :goto_0
    return v1

    .line 5886
    :catch_0
    move-exception v0

    .line 5887
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5890
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isIntentDisabled(Landroid/content/Intent;)Z
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2219
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2221
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IApplicationPolicy;->isIntentDisabled(Landroid/content/Intent;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2226
    :goto_0
    return v1

    .line 2222
    :catch_0
    move-exception v0

    .line 2223
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2226
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMetaKeyEventRequested(Landroid/content/ComponentName;)Z
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    .line 6393
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.isMetaKeyEventRequested"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6394
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6396
    :try_start_0
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "isMetaKeyEventRequested"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6397
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->isMetaKeyEventRequested(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6403
    :goto_0
    return v1

    .line 6398
    :catch_0
    move-exception v0

    .line 6399
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6403
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPackageClipboardAllowed(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 6124
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6126
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->isPackageClipboardAllowed(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6131
    :goto_0
    return v1

    .line 6127
    :catch_0
    move-exception v0

    .line 6128
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6131
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPackageUpdateAllowed(Ljava/lang/String;Z)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "showMsg"    # Z

    .prologue
    .line 5604
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 5605
    .local v0, "callingUid":I
    const/16 v2, 0x3e8

    if-eq v0, v2, :cond_0

    .line 5606
    new-instance v2, Ljava/lang/SecurityException;

    const-string v3, "Calling uid does not have permission to do this operation"

    invoke-direct {v2, v3}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5609
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 5611
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->isPackageUpdateAllowed(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 5616
    :goto_0
    return v2

    .line 5612
    :catch_0
    move-exception v1

    .line 5613
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with application policy"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5616
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public isStatusBarNotificationAllowedAsUser(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I

    .prologue
    .line 3810
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3812
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->isStatusBarNotificationAllowedAsUser(Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3817
    :goto_0
    return v1

    .line 3813
    :catch_0
    move-exception v0

    .line 3814
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3817
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeAppPackageNameFromBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2286
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removeAppPackageNameFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2287
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2289
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppPackageNameFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2294
    :goto_0
    return v1

    .line 2290
    :catch_0
    move-exception v0

    .line 2291
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2294
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAppPackageNameFromWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2513
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removeAppPackageNameFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2514
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2516
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppPackageNameFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2521
    :goto_0
    return v1

    .line 2517
    :catch_0
    move-exception v0

    .line 2518
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAppPermissionFromBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appPermission"    # Ljava/lang/String;

    .prologue
    .line 1889
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removeAppPermissionFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1890
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1892
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppPermissionFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1897
    :goto_0
    return v1

    .line 1893
    :catch_0
    move-exception v0

    .line 1894
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1897
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAppSignatureFromBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appSignature"    # Ljava/lang/String;

    .prologue
    .line 2003
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removeAppSignatureFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2004
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2006
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppSignatureFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2011
    :goto_0
    return v1

    .line 2007
    :catch_0
    move-exception v0

    .line 2008
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2011
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAppSignatureFromWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "appSignature"    # Ljava/lang/String;

    .prologue
    .line 4604
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removeAppSignatureFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4605
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4607
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppSignatureFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4612
    :goto_0
    return v1

    .line 4608
    :catch_0
    move-exception v0

    .line 4609
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4612
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromClearCacheBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5183
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromClearCacheBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5184
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5186
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5191
    :goto_0
    return v1

    .line 5187
    :catch_0
    move-exception v0

    .line 5188
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5191
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromClearCacheWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5348
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromClearCacheWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5349
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5351
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5356
    :goto_0
    return v1

    .line 5352
    :catch_0
    move-exception v0

    .line 5353
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5356
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromClearDataBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4824
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromClearDataBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4825
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4827
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4832
    :goto_0
    return v1

    .line 4828
    :catch_0
    move-exception v0

    .line 4829
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4832
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromClearDataWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4989
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromClearDataWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4990
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4992
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4997
    :goto_0
    return v1

    .line 4993
    :catch_0
    move-exception v0

    .line 4994
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4997
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromDisableClipboardBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5927
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromDisableClipboardBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5928
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5930
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5935
    :goto_0
    return v1

    .line 5931
    :catch_0
    move-exception v0

    .line 5932
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5935
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromDisableClipboardWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 6048
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromDisableClipboardWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6049
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6051
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6056
    :goto_0
    return v1

    .line 6052
    :catch_0
    move-exception v0

    .line 6053
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6056
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromDisableUpdateBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5452
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromDisableUpdateBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5453
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5455
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5460
    :goto_0
    return v1

    .line 5456
    :catch_0
    move-exception v0

    .line 5457
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5460
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromDisableUpdateWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5557
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromDisableUpdateWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5558
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5560
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5565
    :goto_0
    return v1

    .line 5561
    :catch_0
    move-exception v0

    .line 5562
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5565
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromForceStopBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4406
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromForceStopBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4407
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4409
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromForceStopBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4414
    :goto_0
    return v1

    .line 4410
    :catch_0
    move-exception v0

    .line 4411
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4414
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromForceStopWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 4450
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromForceStopWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4451
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4453
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromForceStopWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4458
    :goto_0
    return v1

    .line 4454
    :catch_0
    move-exception v0

    .line 4455
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4458
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromNotificationBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3373
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromNotificationBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3374
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3376
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3381
    :goto_0
    return v1

    .line 3377
    :catch_0
    move-exception v0

    .line 3378
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3381
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromNotificationWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3633
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromNotificationWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3634
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3636
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3641
    :goto_0
    return v1

    .line 3637
    :catch_0
    move-exception v0

    .line 3638
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3641
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromPreventStartBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 5821
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromPreventStartBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5822
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5824
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromPreventStartBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5829
    :goto_0
    return v1

    .line 5825
    :catch_0
    move-exception v0

    .line 5826
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5829
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromWidgetBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3260
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromWidgetBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3261
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3263
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromWidgetBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3268
    :goto_0
    return v1

    .line 3264
    :catch_0
    move-exception v0

    .line 3265
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3268
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromWidgetWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3214
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.removePackagesFromWidgetWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3215
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3217
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromWidgetWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3222
    :goto_0
    return v1

    .line 3218
    :catch_0
    move-exception v0

    .line 3219
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3222
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestMetaKeyEvent(Landroid/content/ComponentName;Z)V
    .locals 3
    .param p1, "componentName"    # Landroid/content/ComponentName;
    .param p2, "request"    # Z

    .prologue
    .line 6377
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.requestMetaKeyEvent"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6378
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6380
    :try_start_0
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "requestMetaKeyEvent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 6381
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->requestMetaKeyEvent(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6387
    :cond_0
    :goto_0
    return-void

    .line 6382
    :catch_0
    move-exception v0

    .line 6383
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public restoreApplicationData(Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "data"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 4019
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.restoreApplicationData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4020
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4022
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->restoreApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4027
    :goto_0
    return v1

    .line 4023
    :catch_0
    move-exception v0

    .line 4024
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed restore App Data"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4027
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setAppInstallToSdCard(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 4258
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setAppInstallToSdCard"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 4259
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 4261
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->setAppInstallToSdCard(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 4266
    :goto_0
    return v1

    .line 4262
    :catch_0
    move-exception v0

    .line 4263
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 4266
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationComponentState(Landroid/content/ComponentName;Z)Z
    .locals 3
    .param p1, "compName"    # Landroid/content/ComponentName;
    .param p2, "enable"    # Z

    .prologue
    .line 5638
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationComponentState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 5639
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 5641
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationComponentState(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 5646
    :goto_0
    return v1

    .line 5642
    :catch_0
    move-exception v0

    .line 5643
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 5646
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationInstallationDisabled(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 920
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationInstallationDisabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 921
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 923
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationInstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 928
    :cond_0
    :goto_0
    return-void

    .line 924
    :catch_0
    move-exception v0

    .line 925
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationInstallationDisabled(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "disableAppInstallation"    # Z

    .prologue
    .line 888
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 890
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationInstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 896
    :cond_0
    :goto_0
    return-void

    .line 892
    :catch_0
    move-exception v0

    .line 893
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationInstallationEnabled(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 951
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationInstallationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 952
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 954
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationInstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 959
    :cond_0
    :goto_0
    return-void

    .line 955
    :catch_0
    move-exception v0

    .line 956
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationInstallationMode(I)Z
    .locals 3
    .param p1, "installationMode"    # I

    .prologue
    .line 838
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationInstallationMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 839
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 841
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->setAppInstallationMode(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 846
    :goto_0
    return v1

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 846
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationNotificationMode(I)Z
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 3727
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationNotificationMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3728
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3730
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationNotificationMode(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3735
    :goto_0
    return v1

    .line 3731
    :catch_0
    move-exception v0

    .line 3732
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3735
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationState(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enableApp"    # Z

    .prologue
    .line 645
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 647
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 652
    :goto_0
    return v1

    .line 648
    :catch_0
    move-exception v0

    .line 649
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 652
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationState(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "enableApp"    # Z
    .param p3, "appControlStateDisableCause"    # Ljava/lang/String;
    .param p4, "appControlStateDisableCauseMetadata"    # Ljava/lang/String;

    .prologue
    .line 677
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 679
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 684
    :goto_0
    return v1

    .line 680
    :catch_0
    move-exception v0

    .line 681
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 684
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setApplicationStateList([Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 3
    .param p1, "pkgList"    # [Ljava/lang/String;
    .param p2, "state"    # Z

    .prologue
    .line 2199
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationStateList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2200
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2202
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationStateList(Landroid/app/enterprise/ContextInfo;[Ljava/lang/String;Z)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2207
    :goto_0
    return-object v1

    .line 2203
    :catch_0
    move-exception v0

    .line 2204
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2207
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method public setApplicationUninstallationDisabled(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1010
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationUninstallationDisabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1011
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1013
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationUninstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 1014
    :catch_0
    move-exception v0

    .line 1015
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationUninstallationDisabled(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "disableAppUninstallation"    # Z

    .prologue
    .line 978
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 980
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationUninstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 986
    :cond_0
    :goto_0
    return-void

    .line 982
    :catch_0
    move-exception v0

    .line 983
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationUninstallationEnabled(Ljava/lang/String;)V
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1041
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationUninstallationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1042
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1044
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationUninstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1050
    :cond_0
    :goto_0
    return-void

    .line 1046
    :catch_0
    move-exception v0

    .line 1047
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setApplicationUninstallationMode(I)Z
    .locals 3
    .param p1, "uninstallationMode"    # I

    .prologue
    .line 2578
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setApplicationUninstallationMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2579
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2581
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationUninstallationMode(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2586
    :goto_0
    return v1

    .line 2582
    :catch_0
    move-exception v0

    .line 2583
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2586
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAsManagedApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 2086
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setAsManagedApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2087
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2089
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->setAsManagedApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2094
    :goto_0
    return v1

    .line 2090
    :catch_0
    move-exception v0

    .line 2091
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2094
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDisableApplication(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 707
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.setDisableApplication"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 708
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 710
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 715
    :cond_0
    :goto_0
    return v1

    .line 711
    :catch_0
    move-exception v0

    .line 712
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with application policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setEnableApplication(Ljava/lang/String;)Z
    .locals 4
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 736
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.setEnableApplication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 737
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 739
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 744
    :goto_0
    return v1

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 744
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 2139
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.startApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2140
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2142
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->startApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2147
    :goto_0
    return v1

    .line 2143
    :catch_0
    move-exception v0

    .line 2144
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2147
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 2111
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.stopApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2112
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2114
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->stopApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2119
    :goto_0
    return v1

    .line 2115
    :catch_0
    move-exception v0

    .line 2116
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2119
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public uninstallApplication(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "keepDataAndCache"    # Z

    .prologue
    .line 613
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.uninstallApplication"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 614
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 616
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->uninstallApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 622
    :goto_0
    return v1

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 622
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public uninstallApplications(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 407
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.uninstallApplications"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 408
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 410
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->removeManagedApplications(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 415
    :goto_0
    return-object v1

    .line 411
    :catch_0
    move-exception v0

    .line 412
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 415
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public updateApplication(Ljava/lang/String;)Z
    .locals 5
    .param p1, "apkFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 555
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApplicationPolicy.updateApplication"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 556
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 558
    :try_start_0
    invoke-direct {p0, p1}, Landroid/app/enterprise/ApplicationPolicy;->checkPathAccessSecured(Ljava/lang/String;)V

    .line 559
    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IApplicationPolicy;->installApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 564
    :cond_0
    :goto_0
    return v1

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with application policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public updateApplicationTable(III)Z
    .locals 3
    .param p1, "containerId"    # I
    .param p2, "oldOwnerUid"    # I
    .param p3, "newOwnerUid"    # I

    .prologue
    .line 6338
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.updateApplicationTable"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 6339
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 6341
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IApplicationPolicy;->updateApplicationTable(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 6347
    :goto_0
    return v1

    .line 6342
    :catch_0
    move-exception v0

    .line 6343
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking to ApplicationPolicy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 6347
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public wipeApplicationData(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 436
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPolicy.wipeApplicationData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPolicy;->getService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 439
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPolicy;->mService:Landroid/app/enterprise/IApplicationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPolicy;->wipeApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 444
    :goto_0
    return v1

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 444
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
