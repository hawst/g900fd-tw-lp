.class public Landroid/app/enterprise/PasswordPolicy;
.super Ljava/lang/Object;
.source "PasswordPolicy.java"


# static fields
.field public static final BIOMETRIC_AUTHENTICATION_FINGERPRINT:I = 0x1

.field public static final BIOMETRIC_AUTHENTICATION_IRIS:I = 0x2

.field public static final PWD_CHANGE_ENFORCED:I = 0x1

.field public static final PWD_CHANGE_ENFORCED_CANCELLED:I = 0x2

.field public static final PWD_CHANGE_ENFORCED_INCALL:I = -0x2

.field public static final PWD_CHANGE_ENFORCED_INCALL_CANCELLED:I = -0x3

.field public static final PWD_CHANGE_ENFORCED_INCALL_NEW_PASSWORD:I = -0x4

.field public static final PWD_CHANGE_ENFORCED_NEW_PASSWORD:I = 0x3

.field public static final PWD_CHANGE_ENFORCE_CANCELLING:I = -0x1

.field public static final PWD_CHANGE_NOT_ENFORCED:I = 0x0

.field public static final PWD_CHANGE_TIMEOUT:Ljava/lang/String; = "com.android.server.enterprise.PWD_CHANGE_TIMEOUT"

.field private static TAG:Ljava/lang/String;

.field public static final enforcePwdExceptions:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mDpm:Landroid/app/admin/DevicePolicyManager;

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mService:Landroid/app/enterprise/IPasswordPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 65
    const-string v0, "PasswordPolicy"

    sput-object v0, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    .line 138
    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.android.settings.SubSettings"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.android.settings.ChooseLockPassword"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.android.settings.ChooseLockPattern"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.google.android.gsf.update.SystemUpdateInstallDialog"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.google.android.gsf.update.SystemUpdateDownloadDialog"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.android.phone.EmergencyDialer"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.android.phone.OutgoingCallBroadcaster"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.phone.EmergencyOutgoingCallBroadcaster"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.android.phone.InCallScreen"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.android.internal.policy.impl.LockScreen"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.android.internal.policy.impl.PatternUnlockScreen"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.internal.policy.impl.PasswordUnlockScreen"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.android.incallui.InCallActivity"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.android.server.telecom.EmergencyCallActivity"

    aput-object v2, v0, v1

    sput-object v0, Landroid/app/enterprise/PasswordPolicy;->enforcePwdExceptions:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 2
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p2, p0, Landroid/app/enterprise/PasswordPolicy;->mContext:Landroid/content/Context;

    .line 156
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mContext:Landroid/content/Context;

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mDpm:Landroid/app/admin/DevicePolicyManager;

    .line 158
    iput-object p1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 159
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p1, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 160
    return-void
.end method

.method private getService()Landroid/app/enterprise/IPasswordPolicy;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    if-nez v0, :cond_0

    .line 164
    const-string v0, "password_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IPasswordPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    .line 167
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    return-object v0
.end method


# virtual methods
.method public addRequiredPasswordPattern(Ljava/lang/String;)Z
    .locals 3
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 378
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.addRequiredPasswordPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 379
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 381
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->addRequiredPasswordPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 386
    :goto_0
    return v1

    .line 382
    :catch_0
    move-exception v0

    .line 383
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 386
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteAllRestrictions()Z
    .locals 3

    .prologue
    .line 404
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.deleteAllRestrictions"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 405
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 407
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->deleteAllRestrictions(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 412
    :goto_0
    return v1

    .line 408
    :catch_0
    move-exception v0

    .line 409
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 412
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enforcePwdChange()Z
    .locals 3

    .prologue
    .line 614
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.enforcePwdChange"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 615
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 617
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->enforcePwdChange(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 622
    :goto_0
    return v1

    .line 618
    :catch_0
    move-exception v0

    .line 619
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 622
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public excludeExternalStorageForFailedPasswordsWipe(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1355
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.excludeExternalStorageForFailedPasswordsWipe"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1356
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1358
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->excludeExternalStorageForFailedPasswordsWipe(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1363
    :goto_0
    return v1

    .line 1359
    :catch_0
    move-exception v0

    .line 1360
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1363
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForbiddenStrings(Z)Ljava/util/List;
    .locals 3
    .param p1, "allAdmins"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1081
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.getForbiddenStrings"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1082
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1084
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getForbiddenStrings(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1089
    :goto_0
    return-object v1

    .line 1085
    :catch_0
    move-exception v0

    .line 1086
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getDataForbidden!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1089
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumCharacterOccurences()I
    .locals 3

    .prologue
    .line 1165
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1167
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumCharacterOccurences(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1172
    :goto_0
    return v1

    .line 1168
    :catch_0
    move-exception v0

    .line 1169
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getMaxRepeatedCharacters!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1172
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumCharacterSequenceLength()I
    .locals 3

    .prologue
    .line 904
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 906
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumCharacterSequenceLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 911
    :goto_0
    return v1

    .line 907
    :catch_0
    move-exception v0

    .line 908
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getMaximumCharacterSequenceLength!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 911
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumFailedPasswordsForDeviceDisable()I
    .locals 3

    .prologue
    .line 733
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.getMaximumFailedPasswordsForDeviceDisable"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 734
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 736
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumFailedPasswordsForDisable(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 741
    :goto_0
    return v1

    .line 737
    :catch_0
    move-exception v0

    .line 738
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getMaximumFailedPasswordsForDeviceDisable!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 741
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaximumNumericSequenceLength()I
    .locals 3

    .prologue
    .line 818
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 820
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getMaximumNumericSequenceLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 825
    :goto_0
    return v1

    .line 821
    :catch_0
    move-exception v0

    .line 822
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getNumericSequencesForbidden!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 825
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMinPasswordComplexChars(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 293
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BasePasswordPolicy;->getPasswordMinimumNonLetter(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getMinimumCharacterChangeLength()I
    .locals 3

    .prologue
    .line 993
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 995
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getMinimumCharacterChangeLength(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1000
    :goto_0
    return v1

    .line 996
    :catch_0
    move-exception v0

    .line 997
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getMinCharacterChangeLength!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1000
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPassword(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 306
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mDpm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->getPassword(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPasswordChangeTimeout()I
    .locals 3

    .prologue
    .line 578
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 580
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordChangeTimeout(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 585
    :goto_0
    return v1

    .line 581
    :catch_0
    move-exception v0

    .line 582
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 585
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPasswordExpires(Landroid/content/ComponentName;)I
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 204
    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/enterprise/BasePasswordPolicy;->getPasswordExpirationTimeout(Landroid/content/ComponentName;)J

    move-result-wide v0

    .line 205
    .local v0, "timeout":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 206
    const-wide/32 v2, 0x5265c00

    div-long v2, v0, v2

    long-to-int v2, v2

    .line 208
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPasswordHistory(Landroid/content/ComponentName;)I
    .locals 1
    .param p1, "admin"    # Landroid/content/ComponentName;

    .prologue
    .line 249
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BasePasswordPolicy;->getPasswordHistoryLength(Landroid/content/ComponentName;)I

    move-result v0

    return v0
.end method

.method public getPasswordLockDelay()I
    .locals 3

    .prologue
    .line 691
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 693
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getPasswordLockDelay(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 698
    :goto_0
    return v1

    .line 694
    :catch_0
    move-exception v0

    .line 695
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getPasswordLockDelay!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 698
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getRequiredPwdPatternRestrictions(Z)Ljava/lang/String;
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 426
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.getRequiredPwdPatternRestrictions"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 427
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 429
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->getRequiredPwdPatternRestrictions(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 434
    :goto_0
    return-object v1

    .line 430
    :catch_0
    move-exception v0

    .line 431
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 434
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSupportedBiometricAuthentications()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1715
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1717
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->getSupportedBiometricAuthentications(Landroid/app/enterprise/ContextInfo;)Ljava/util/Map;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1722
    :goto_0
    return-object v1

    .line 1718
    :catch_0
    move-exception v0

    .line 1719
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1722
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasForbiddenCharacterSequence(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 461
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 463
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenCharacterSequence(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 468
    :goto_0
    return v1

    .line 464
    :catch_0
    move-exception v0

    .line 465
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 468
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasForbiddenData(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 496
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 498
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 503
    :goto_0
    return v1

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 503
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasForbiddenNumericSequence(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 444
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 446
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenNumericSequence(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 451
    :goto_0
    return v1

    .line 447
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 451
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasForbiddenStringDistance(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 479
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 481
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->hasForbiddenStringDistance(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 486
    :goto_0
    return v1

    .line 482
    :catch_0
    move-exception v0

    .line 483
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 486
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasMaxRepeatedCharacters(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 514
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->hasMaxRepeatedCharacters(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 521
    :goto_0
    return v1

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBiometricAuthenticationEnabled(I)Z
    .locals 6
    .param p1, "bioAuth"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1592
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1594
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v5, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Landroid/app/enterprise/IPasswordPolicy;->isBiometricAuthenticationEnabled(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1611
    :cond_0
    :goto_0
    return v2

    .line 1595
    :catch_0
    move-exception v0

    .line 1596
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1600
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    sget-object v4, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "PasswordPolicy.isBiometricAuthenticationEnabled : getService() == null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1601
    if-ne p1, v3, :cond_0

    .line 1602
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Security_ConfigLockLevelODE"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1603
    .local v1, "mCscFeature":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "NoFingerprint"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1605
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "Fingerprint"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1606
    :cond_3
    sget-object v2, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "isBiometricAuthenticationEnabled: return true (CSC)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 1607
    goto :goto_0
.end method

.method public isBiometricAuthenticationEnabledAsUser(II)Z
    .locals 6
    .param p1, "userId"    # I
    .param p2, "bioAuth"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1618
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1620
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    invoke-interface {v4, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->isBiometricAuthenticationEnabledAsUser(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1637
    :cond_0
    :goto_0
    return v2

    .line 1621
    :catch_0
    move-exception v0

    .line 1622
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v4, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with password policy"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1626
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    sget-object v4, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v5, "PasswordPolicy.isBiometricAuthenticationEnabled : getService() == null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1627
    if-ne p2, v3, :cond_0

    .line 1628
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v4

    const-string v5, "CscFeature_Security_ConfigLockLevelODE"

    invoke-virtual {v4, v5}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1629
    .local v1, "mCscFeature":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "NoFingerprint"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1631
    :cond_2
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "Fingerprint"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1632
    :cond_3
    sget-object v2, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "isBiometricAuthenticationEnabled: return true (CSC)"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 1633
    goto :goto_0
.end method

.method public isChangeRequested()I
    .locals 3

    .prologue
    .line 592
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 594
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isChangeRequested(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 599
    :goto_0
    return v1

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 599
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isExternalStorageForFailedPasswordsWipeExcluded()Z
    .locals 3

    .prologue
    .line 1383
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1385
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isExternalStorageForFailedPasswordsWipeExcluded(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1390
    :goto_0
    return v1

    .line 1386
    :catch_0
    move-exception v0

    .line 1387
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1390
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPasswordPatternMatched(Ljava/lang/String;)Z
    .locals 3
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 532
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 534
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->isPasswordPatternMatched(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 539
    :goto_0
    return v1

    .line 535
    :catch_0
    move-exception v0

    .line 536
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 539
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPasswordTableExist()Z
    .locals 3

    .prologue
    .line 1648
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1650
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isPasswordTableExist(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1655
    :goto_0
    return v1

    .line 1651
    :catch_0
    move-exception v0

    .line 1652
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1655
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPasswordVisibilityEnabled()Z
    .locals 3

    .prologue
    .line 1309
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1311
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isPasswordVisibilityEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1316
    :goto_0
    return v1

    .line 1312
    :catch_0
    move-exception v0

    .line 1313
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1316
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isScreenLockPatternVisibilityEnabled()Z
    .locals 3

    .prologue
    .line 1235
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.isScreenLockPatternVisibilityEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1236
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1238
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPasswordPolicy;->isScreenLockPatternVisibilityEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1243
    :goto_0
    return v1

    .line 1239
    :catch_0
    move-exception v0

    .line 1240
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1243
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isServiceRunning()Z
    .locals 2

    .prologue
    .line 1665
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1666
    sget-object v0, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v1, "isServiceRunning()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1667
    const/4 v0, 0x1

    .line 1669
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reboot(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 317
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PasswordPolicy.reboot"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 318
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mDpm:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0, p1}, Landroid/app/admin/DevicePolicyManager;->reboot(Ljava/lang/String;)V

    .line 319
    return-void
.end method

.method public setBiometricAuthenticationEnabled(IZ)Z
    .locals 3
    .param p1, "bioAuth"    # I
    .param p2, "enable"    # Z

    .prologue
    .line 1539
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setBiometricAuthenticationEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1540
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1542
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setBiometricAuthenticationEnabled(Landroid/app/enterprise/ContextInfo;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1547
    :goto_0
    return v1

    .line 1543
    :catch_0
    move-exception v0

    .line 1544
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1547
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setForbiddenStrings(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1049
    .local p1, "forbiddenStrings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setForbiddenStrings"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1050
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1052
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setForbiddenStrings(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1058
    :goto_0
    return v1

    .line 1053
    :catch_0
    move-exception v0

    .line 1054
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setDataForbidden!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1058
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumCharacterOccurrences(I)Z
    .locals 3
    .param p1, "count"    # I

    .prologue
    .line 1134
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMaximumCharacterOccurrences"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1135
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1137
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumCharacterOccurrences(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1143
    :goto_0
    return v1

    .line 1138
    :catch_0
    move-exception v0

    .line 1139
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setMaxRepeatedCharacters!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1143
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumCharacterSequenceLength(I)Z
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 872
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMaximumCharacterSequenceLength"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 873
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 875
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumCharacterSequenceLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 881
    :goto_0
    return v1

    .line 876
    :catch_0
    move-exception v0

    .line 877
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setMaximumCharacterSequenceLength!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 881
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumFailedPasswordsForDeviceDisable(I)Z
    .locals 3
    .param p1, "num"    # I

    .prologue
    .line 714
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMaximumFailedPasswordsForDeviceDisable"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 715
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 717
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumFailedPasswordsForDisable(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 722
    :goto_0
    return v1

    .line 718
    :catch_0
    move-exception v0

    .line 719
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setMaximumFailedPasswordsForDeviceDisable"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 722
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaximumFailedPasswordsForWipe(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "num"    # I

    .prologue
    .line 1423
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMaximumFailedPasswordsForWipe"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1424
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1426
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumFailedPasswordsForWipe(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1431
    :cond_0
    :goto_0
    return-void

    .line 1427
    :catch_0
    move-exception v0

    .line 1428
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setMaximumNumericSequenceLength(I)Z
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 787
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMaximumNumericSequenceLength"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 790
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMaximumNumericSequenceLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 796
    :goto_0
    return v1

    .line 791
    :catch_0
    move-exception v0

    .line 792
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setNumericSequencesForbidden!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 796
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMinPasswordComplexChars(Landroid/content/ComponentName;I)V
    .locals 4
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "size"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 269
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PasswordPolicy.setMinPasswordComplexChars"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordMinimumNonLetter(Landroid/content/ComponentName;I)V

    .line 272
    const/4 v0, 0x4

    if-lt p2, v0, :cond_0

    .line 273
    sget-object v0, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v1, "length is bigger than 4! set Upper & lower case"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordMinimumUpperCase(Landroid/content/ComponentName;I)V

    .line 275
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, v3}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordMinimumLowerCase(Landroid/content/ComponentName;I)V

    .line 280
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordMinimumUpperCase(Landroid/content/ComponentName;I)V

    .line 278
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, v2}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordMinimumLowerCase(Landroid/content/ComponentName;I)V

    goto :goto_0
.end method

.method public setMinimumCharacterChangeLength(I)Z
    .locals 3
    .param p1, "length"    # I

    .prologue
    .line 960
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setMinimumCharacterChangeLength"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 961
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 963
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setMinimumCharacterChangeLength(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 969
    :goto_0
    return v1

    .line 964
    :catch_0
    move-exception v0

    .line 965
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setMinCharacterChangeLength!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 969
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPasswordChangeTimeout(I)Z
    .locals 3
    .param p1, "timeout"    # I

    .prologue
    .line 557
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordChangeTimeout"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 558
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 560
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordChangeTimeout(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 565
    :goto_0
    return v1

    .line 561
    :catch_0
    move-exception v0

    .line 562
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 565
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPasswordExpires(Landroid/content/ComponentName;I)V
    .locals 6
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 188
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PasswordPolicy.setPasswordExpires"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    int-to-long v2, p2

    const-wide/32 v4, 0x5265c00

    mul-long/2addr v2, v4

    invoke-virtual {v0, p1, v2, v3}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordExpirationTimeout(Landroid/content/ComponentName;J)V

    .line 190
    return-void
.end method

.method public setPasswordHistory(Landroid/content/ComponentName;I)V
    .locals 2
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "value"    # I

    .prologue
    .line 229
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PasswordPolicy.setPasswordHistory"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 231
    if-gez p2, :cond_0

    .line 235
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/PasswordPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBasePasswordPolicy()Landroid/app/enterprise/BasePasswordPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/app/enterprise/BasePasswordPolicy;->setPasswordHistoryLength(Landroid/content/ComponentName;I)V

    goto :goto_0
.end method

.method public setPasswordLockDelay(I)Z
    .locals 5
    .param p1, "time"    # I

    .prologue
    const/4 v2, 0x0

    .line 661
    iget-object v3, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "PasswordPolicy.setPasswordLockDelay"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 662
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v0

    .line 663
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "2.0"

    const-string v4, "version"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v3, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    const/16 v4, 0x64

    if-lt v3, v4, :cond_1

    .line 673
    :cond_0
    :goto_0
    return v2

    .line 666
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 668
    :try_start_0
    iget-object v3, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v4, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordLockDelay(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    .line 669
    :catch_0
    move-exception v1

    .line 670
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed setPasswordLockDelay"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLength(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 1441
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordMinimumLength"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1442
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1444
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLength(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1450
    :cond_0
    :goto_0
    return-void

    .line 1445
    :catch_0
    move-exception v0

    .line 1446
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumLetters(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 1461
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordMinimumLetters"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1462
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1464
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumLetters(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1469
    :cond_0
    :goto_0
    return-void

    .line 1465
    :catch_0
    move-exception v0

    .line 1466
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordMinimumNonLetter(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "length"    # I

    .prologue
    .line 1480
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordMinimumNonLetter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1481
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1483
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordMinimumNonLetter(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1488
    :cond_0
    :goto_0
    return-void

    .line 1484
    :catch_0
    move-exception v0

    .line 1485
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordQuality(Landroid/content/ComponentName;I)V
    .locals 3
    .param p1, "admin"    # Landroid/content/ComponentName;
    .param p2, "quality"    # I

    .prologue
    .line 1403
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordQuality"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1404
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1406
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordQuality(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1412
    :cond_0
    :goto_0
    return-void

    .line 1407
    :catch_0
    move-exception v0

    .line 1408
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setPasswordVisibilityEnabled(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1282
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setPasswordVisibilityEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1283
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1285
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPasswordVisibilityEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1290
    :goto_0
    return v1

    .line 1286
    :catch_0
    move-exception v0

    .line 1287
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1290
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPwdChangeRequested(I)Z
    .locals 3
    .param p1, "flag"    # I

    .prologue
    .line 633
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 635
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setPwdChangeRequested(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 640
    :goto_0
    return v1

    .line 636
    :catch_0
    move-exception v0

    .line 637
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 640
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setRequiredPasswordPattern(Ljava/lang/String;)Z
    .locals 3
    .param p1, "regex"    # Ljava/lang/String;

    .prologue
    .line 345
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setRequiredPasswordPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setRequiredPasswordPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 353
    :goto_0
    return v1

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 353
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setScreenLockPatternVisibilityEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1209
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PasswordPolicy.setScreenLockPatternVisibilityEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1210
    invoke-direct {p0}, Landroid/app/enterprise/PasswordPolicy;->getService()Landroid/app/enterprise/IPasswordPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1212
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PasswordPolicy;->mService:Landroid/app/enterprise/IPasswordPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PasswordPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPasswordPolicy;->setScreenLockPatternVisibilityEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1217
    :goto_0
    return v1

    .line 1213
    :catch_0
    move-exception v0

    .line 1214
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PasswordPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with password policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1217
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
