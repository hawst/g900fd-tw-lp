.class public Landroid/app/enterprise/RestrictionPolicy;
.super Ljava/lang/Object;
.source "RestrictionPolicy.java"


# static fields
.field public static final ALLOW_SETTINGS_CHANGES:Ljava/lang/String; = "edm.intent.action.allow.settings"

.field public static final EXTERNAL_STORAGE_PATH_SD:Ljava/lang/String; = "/storage/extSdCard"

.field public static final GOOGLE_ACCOUNT_TYPE:Ljava/lang/String; = "com.google"

.field public static final KEY_VOICE_INPUT_CONTROL:Ljava/lang/String; = "voice_input_control"

.field public static final LOCKSCREEN_MULTIPLE_WIDGET_VIEW:I = 0x1

.field public static final LOCKSCREEN_SHORTCUTS_VIEW:I = 0x2

.field public static final MTP_DISABLED:Ljava/lang/String; = "edm.intent.action.disable.mtp"

.field public static final MTP_DISABLED_PERMISSION:Ljava/lang/String; = "com.sec.restriction.permission.MTP_DISABLED"

.field public static final SVOICE_PACKAGE:Ljava/lang/String; = "com.vlingo.midas"

.field private static TAG:Ljava/lang/String; = null

.field public static TASK_MANAGER_PKGNAME:Ljava/lang/String; = null

.field public static final USB_HOST_STORAGE_PATH:Ljava/lang/String; = "/storage/UsbDrive"

.field public static final WFD_DISABLE:Ljava/lang/String; = "com.android.server.enterprise.WFD_DISABLE"

.field public static final settingsExceptions:[Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mService:Landroid/app/enterprise/IRestrictionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 63
    const-string v0, "RestrictionPolicy"

    sput-object v0, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    .line 94
    const/16 v0, 0x17

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.android.settings.ActivityPicker"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.android.settings.AppWidgetPickActivity"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.android.settings.widget.SettingsAppWidgetProvider"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.android.settings.CryptKeeper"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.android.settings.CryptKeeperConfirm"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.android.settings.CryptKeeperSettings"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.android.settings.ChooseLockAdditionalPin"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.android.settings.ChooseLockFaceWarning"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.android.settings.ChooseLockGeneric"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.android.settings.ChooseLockMotion"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "com.android.settings.ChooseLockPassword"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "com.android.settings.ChooseLockPattern"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "com.android.settings.ConfirmLockPassword"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "com.android.settings.ConfirmLockPattern"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "com.android.settings.DeviceAdminAdd"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "com.android.settings.bluetooth.DevicePickerActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "com.android.settings.wifi.p2p.WifiP2pDeviceList"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "com.android.settings.Settings$WifiP2pDevicePickerActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "com.android.settings.wfd.WfdPickerActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "com.android.settings.bluetooth.BluetoothPairingDialog"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "com.android.settings.bluetooth.CheckBluetoothStateActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "com.android.settings.bluetooth.BluetoothEnableActivity"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "com.android.settings.bluetooth.BluetoothEnablingActivity"

    aput-object v2, v0, v1

    sput-object v0, Landroid/app/enterprise/RestrictionPolicy;->settingsExceptions:[Ljava/lang/String;

    .line 125
    const-string v0, "com.sec.android.app.controlpanel"

    sput-object v0, Landroid/app/enterprise/RestrictionPolicy;->TASK_MANAGER_PKGNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 2
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContext:Landroid/content/Context;

    .line 153
    iput-object p1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 154
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p1, v1}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 155
    return-void
.end method

.method private getService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 159
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    .line 162
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    return-object v0
.end method


# virtual methods
.method public allowActivationLock(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3096
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowActivationLock"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3097
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3099
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowActivationLock(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3104
    :goto_0
    return v1

    .line 3100
    :catch_0
    move-exception v0

    .line 3101
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3104
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowAirplaneMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2946
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowAirplaneMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2947
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2949
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowAirplaneMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2954
    :goto_0
    return v1

    .line 2950
    :catch_0
    move-exception v0

    .line 2951
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2954
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowAndroidBeam(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2510
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowAndroidBeam"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2511
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2513
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowAndroidBeam(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2518
    :goto_0
    return v1

    .line 2514
    :catch_0
    move-exception v0

    .line 2515
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2518
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowAudioRecord(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2005
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowAudioRecord"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2006
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2008
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowAudioRecord(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2013
    :goto_0
    return v1

    .line 2009
    :catch_0
    move-exception v0

    .line 2010
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2013
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowBackgroundProcessLimit(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2255
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowBackgroundProcessLimit"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2256
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2258
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowBackgroundProcessLimit(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2263
    :goto_0
    return v1

    .line 2259
    :catch_0
    move-exception v0

    .line 2260
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2263
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowBluetooth(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 388
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "RestrictionPolicy.allowBluetooth"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 389
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BluetoothPolicy;->allowBluetooth(Z)Z

    move-result v0

    return v0
.end method

.method public allowClipboardShare(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2365
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowClipboardShare"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2366
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2368
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowClipboardShare(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2373
    :goto_0
    return v1

    .line 2369
    :catch_0
    move-exception v0

    .line 2370
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2373
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowDeveloperMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2909
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowDeveloperMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2910
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2912
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowDeveloperMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2917
    :goto_0
    return v1

    .line 2913
    :catch_0
    move-exception v0

    .line 2914
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2917
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowFactoryReset(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1279
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowFactoryReset"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1280
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1282
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowFactoryReset(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1287
    :goto_0
    return v1

    .line 1283
    :catch_0
    move-exception v0

    .line 1284
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1287
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowFastEncryption(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3349
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowFastEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3350
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3352
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowFastEncryption(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3357
    :goto_0
    return v1

    .line 3353
    :catch_0
    move-exception v0

    .line 3354
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3357
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowFirmwareRecovery(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 3393
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowFirmwareRecovery"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3394
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3396
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowFirmwareRecovery(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3401
    :goto_0
    return v1

    .line 3397
    :catch_0
    move-exception v0

    .line 3398
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3401
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowGoogleAccountsAutoSync(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2983
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowGoogleAccountsAutoSync"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2984
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2986
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowGoogleAccountsAutoSync(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2991
    :goto_0
    return v1

    .line 2987
    :catch_0
    move-exception v0

    .line 2988
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2991
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowGoogleCrashReport(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1612
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowGoogleCrashReport"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1613
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1615
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowGoogleCrashReport(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1620
    :goto_0
    return v1

    .line 1616
    :catch_0
    move-exception v0

    .line 1617
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1620
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowKillingActivitiesOnLeave(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2292
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowKillingActivitiesOnLeave"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2293
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2295
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowKillingActivitiesOnLeave(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2300
    :goto_0
    return v1

    .line 2296
    :catch_0
    move-exception v0

    .line 2297
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2300
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowLockScreenView(IZ)Z
    .locals 3
    .param p1, "view"    # I
    .param p2, "allow"    # Z

    .prologue
    .line 2870
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowLockScreenView"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2871
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2873
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IRestrictionPolicy;->allowLockScreenView(Landroid/app/enterprise/ContextInfo;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2878
    :goto_0
    return v1

    .line 2874
    :catch_0
    move-exception v0

    .line 2875
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2878
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowOTAUpgrade(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1471
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowOTAUpgrade"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1472
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1474
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowOTAUpgrade(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1479
    :goto_0
    return v1

    .line 1475
    :catch_0
    move-exception v0

    .line 1476
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1479
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowPowerOff(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1907
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowPowerOff"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1908
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1910
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowPowerOff(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1915
    :goto_0
    return v1

    .line 1911
    :catch_0
    move-exception v0

    .line 1912
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1915
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSBeam(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2471
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSBeam"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2472
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2474
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSBeam(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2479
    :goto_0
    return v1

    .line 2475
    :catch_0
    move-exception v0

    .line 2476
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2479
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSDCardMove(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3265
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSDCardMove"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3266
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3268
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSDCardMove(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3273
    :goto_0
    return v1

    .line 3269
    :catch_0
    move-exception v0

    .line 3270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3273
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSDCardWrite(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1542
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSDCardWrite"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1543
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1545
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSDCardWrite(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1550
    :goto_0
    return v1

    .line 1546
    :catch_0
    move-exception v0

    .line 1547
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1550
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSVoice(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2425
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSVoice"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2426
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2428
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSVoice(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2433
    :goto_0
    return v1

    .line 2429
    :catch_0
    move-exception v0

    .line 2430
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2433
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSafeMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2795
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSafeMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2796
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2798
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSafeMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2803
    :goto_0
    return v1

    .line 2799
    :catch_0
    move-exception v0

    .line 2800
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2803
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSettingsChanges(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 898
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSettingsChanges"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 899
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 901
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSettingsChanges(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 906
    :goto_0
    return v1

    .line 902
    :catch_0
    move-exception v0

    .line 903
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 906
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowShareList(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2618
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2620
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowShareList(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2625
    :goto_0
    return v1

    .line 2621
    :catch_0
    move-exception v0

    .line 2622
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2625
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSmartClipMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 3467
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowSmartClipMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3468
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3470
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowSmartClipMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3475
    :goto_0
    return v1

    .line 3471
    :catch_0
    move-exception v0

    .line 3472
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3475
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowStatusBarExpansion(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1828
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowStatusBarExpansion"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1829
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1831
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowStatusBarExpansion(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1836
    :goto_0
    return v1

    .line 1832
    :catch_0
    move-exception v0

    .line 1833
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1836
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowStopSystemApp(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2162
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowStopSystemApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2163
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2165
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowStopSystemApp(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2170
    :goto_0
    return v1

    .line 2166
    :catch_0
    move-exception v0

    .line 2167
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2170
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowUsbHostStorage(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2550
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowUsbHostStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2551
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2553
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowUsbHostStorage(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2558
    :goto_0
    return v1

    .line 2554
    :catch_0
    move-exception v0

    .line 2555
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2558
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowUserMobileDataLimit(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2329
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowUserMobileDataLimit"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2330
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2332
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowUserMobileDataLimit(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2337
    :goto_0
    return v1

    .line 2333
    :catch_0
    move-exception v0

    .line 2334
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowVideoRecord(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2083
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowVideoRecord"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2084
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2086
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowVideoRecord(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2091
    :goto_0
    return v1

    .line 2087
    :catch_0
    move-exception v0

    .line 2088
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2091
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowVpn(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1399
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowVpn"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1400
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1402
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowVpn(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1407
    :goto_0
    return v1

    .line 1403
    :catch_0
    move-exception v0

    .line 1404
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1407
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowWallpaperChange(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1712
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowWallpaperChange"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1713
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1715
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowWallpaperChange(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1720
    :goto_0
    return v1

    .line 1716
    :catch_0
    move-exception v0

    .line 1717
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1720
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowWiFi(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 370
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "RestrictionPolicy.allowWiFi"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 371
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getWifiPolicy()Landroid/app/enterprise/WifiPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/WifiPolicy;->setWifiAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public allowWifiDirect(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2209
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.allowWifiDirect"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2210
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2212
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->allowWifiDirect(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2217
    :goto_0
    return v1

    .line 2213
    :catch_0
    move-exception v0

    .line 2214
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2217
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isActivationLockAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3126
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3128
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isActivationLockAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3133
    :goto_0
    return v1

    .line 3129
    :catch_0
    move-exception v0

    .line 3130
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3133
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAirplaneModeAllowed()Z
    .locals 1

    .prologue
    .line 2962
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isAirplaneModeAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isAirplaneModeAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2969
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2971
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isAirplaneModeAllowed(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2976
    :goto_0
    return v1

    .line 2972
    :catch_0
    move-exception v0

    .line 2973
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2976
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAndroidBeamAllowed()Z
    .locals 1

    .prologue
    .line 2526
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isAndroidBeamAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isAndroidBeamAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2535
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2537
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isAndroidBeamAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2542
    :goto_0
    return v1

    .line 2538
    :catch_0
    move-exception v0

    .line 2539
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2542
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAudioRecordAllowed()Z
    .locals 1

    .prologue
    .line 2044
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isAudioRecordAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isAudioRecordAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2020
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2022
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isAudioRecordAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2027
    :goto_0
    return v1

    .line 2023
    :catch_0
    move-exception v0

    .line 2024
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2027
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBackgroundDataEnabled()Z
    .locals 3

    .prologue
    .line 1189
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1191
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isBackgroundDataEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1196
    :goto_0
    return v1

    .line 1192
    :catch_0
    move-exception v0

    .line 1193
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1196
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBackgroundProcessLimitAllowed()Z
    .locals 3

    .prologue
    .line 2274
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2276
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isBackgroundProcessLimitAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2281
    :goto_0
    return v1

    .line 2277
    :catch_0
    move-exception v0

    .line 2278
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2281
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBackupAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 804
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 806
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isBackupAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 811
    :goto_0
    return v1

    .line 807
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 811
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBluetoothEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 244
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BluetoothPolicy;->isBluetoothEnabled(Z)Z

    move-result v0

    return v0
.end method

.method public isBluetoothTetheringEnabled()Z
    .locals 3

    .prologue
    .line 521
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 523
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isBluetoothTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 528
    :goto_0
    return v1

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 528
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isCameraEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 291
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isCameraEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 298
    :goto_0
    return v1

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with device info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 298
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isCellularDataAllowed()Z
    .locals 3

    .prologue
    .line 849
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 851
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isCellularDataAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 856
    :goto_0
    return v1

    .line 852
    :catch_0
    move-exception v0

    .line 853
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 856
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isClipboardAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 987
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 989
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isClipboardAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 994
    :goto_0
    return v1

    .line 990
    :catch_0
    move-exception v0

    .line 991
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 994
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isClipboardAllowedAsUser(ZI)Z
    .locals 3
    .param p1, "showMsg"    # Z
    .param p2, "userId"    # I

    .prologue
    .line 1002
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1004
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IRestrictionPolicy;->isClipboardAllowedAsUser(ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1009
    :goto_0
    return v1

    .line 1005
    :catch_0
    move-exception v0

    .line 1006
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1009
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isClipboardShareAllowed()Z
    .locals 3

    .prologue
    .line 2383
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2385
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isClipboardShareAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2390
    :goto_0
    return v1

    .line 2386
    :catch_0
    move-exception v0

    .line 2387
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2390
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isClipboardShareAllowedAsUser(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 2398
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2400
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isClipboardShareAllowedAsUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2405
    :goto_0
    return v1

    .line 2401
    :catch_0
    move-exception v0

    .line 2402
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2405
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isDeveloperModeAllowed()Z
    .locals 1

    .prologue
    .line 2925
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isDeveloperModeAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isDeveloperModeAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2932
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2934
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isDeveloperModeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2939
    :goto_0
    return v1

    .line 2935
    :catch_0
    move-exception v0

    .line 2936
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2939
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isFactoryResetAllowed()Z
    .locals 3

    .prologue
    .line 1303
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1305
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isFactoryResetAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1310
    :goto_0
    return v1

    .line 1306
    :catch_0
    move-exception v0

    .line 1307
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1310
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isFastEncryptionAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3379
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3381
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isFastEncryptionAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3386
    :goto_0
    return v1

    .line 3382
    :catch_0
    move-exception v0

    .line 3383
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3386
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFirmwareRecoveryAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3409
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3411
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isFirmwareRecoveryAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3417
    :goto_0
    return v1

    .line 3412
    :catch_0
    move-exception v0

    .line 3413
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3417
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isGoogleAccountsAutoSyncAllowed()Z
    .locals 3

    .prologue
    .line 2999
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3001
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isGoogleAccountsAutoSyncAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3006
    :goto_0
    return v1

    .line 3002
    :catch_0
    move-exception v0

    .line 3003
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3006
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isGoogleAccountsAutoSyncAllowedAsUser(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 3011
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3013
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isGoogleAccountsAutoSyncAllowedAsUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3018
    :goto_0
    return v1

    .line 3014
    :catch_0
    move-exception v0

    .line 3015
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3018
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isGoogleCrashReportAllowed()Z
    .locals 3

    .prologue
    .line 1639
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1641
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isGoogleCrashReportAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1646
    :goto_0
    return v1

    .line 1642
    :catch_0
    move-exception v0

    .line 1643
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1646
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isGoogleCrashReportAllowedAsUser(I)Z
    .locals 3
    .param p1, "userId"    # I

    .prologue
    .line 1653
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1655
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isGoogleCrashReportAllowedAsUser(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1661
    :goto_0
    return v1

    .line 1656
    :catch_0
    move-exception v0

    .line 1657
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1661
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isHeadphoneEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3210
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3212
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isHeadphoneEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3217
    :goto_0
    return v1

    .line 3213
    :catch_0
    move-exception v0

    .line 3214
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3217
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isHomeKeyEnabled()Z
    .locals 1

    .prologue
    .line 1346
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isHomeKeyEnabled(Z)Z

    move-result v0

    return v0
.end method

.method public isHomeKeyEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1351
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1353
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isHomeKeyEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1358
    :goto_0
    return v1

    .line 1354
    :catch_0
    move-exception v0

    .line 1355
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1358
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isKillingActivitiesOnLeaveAllowed()Z
    .locals 3

    .prologue
    .line 2311
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2313
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isKillingActivitiesOnLeaveAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2318
    :goto_0
    return v1

    .line 2314
    :catch_0
    move-exception v0

    .line 2315
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2318
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isLockScreenEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3042
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3044
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isLockScreenEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3049
    :goto_0
    return v1

    .line 3045
    :catch_0
    move-exception v0

    .line 3046
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3049
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isLockScreenViewAllowed(I)Z
    .locals 3
    .param p1, "view"    # I

    .prologue
    .line 2895
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2897
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isLockScreenViewAllowed(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2902
    :goto_0
    return v1

    .line 2898
    :catch_0
    move-exception v0

    .line 2899
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2902
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isMicrophoneEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 346
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isMicrophoneEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 353
    :goto_0
    return v1

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 353
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isMockLocationEnabled()Z
    .locals 3

    .prologue
    .line 757
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 759
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isMockLocationEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 764
    :goto_0
    return v1

    .line 760
    :catch_0
    move-exception v0

    .line 761
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 764
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNFCEnabled()Z
    .locals 3

    .prologue
    .line 1233
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1235
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IRestrictionPolicy;->isNFCEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1240
    :goto_0
    return v1

    .line 1236
    :catch_0
    move-exception v0

    .line 1237
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1240
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNFCEnabledWithMsg(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1253
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1255
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isNFCEnabledWithMsg(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1260
    :goto_0
    return v1

    .line 1256
    :catch_0
    move-exception v0

    .line 1257
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1260
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNonMarketAppAllowed()Z
    .locals 3

    .prologue
    .line 1142
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1144
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isNonMarketAppAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1149
    :goto_0
    return v1

    .line 1145
    :catch_0
    move-exception v0

    .line 1146
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1149
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNonTrustedAppInstallBlocked()Z
    .locals 1

    .prologue
    .line 2764
    const/4 v0, 0x0

    return v0
.end method

.method public isOTAUpgradeAllowed()Z
    .locals 3

    .prologue
    .line 1498
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1500
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isOTAUpgradeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1505
    :goto_0
    return v1

    .line 1501
    :catch_0
    move-exception v0

    .line 1502
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1505
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPowerOffAllowed()Z
    .locals 1

    .prologue
    .line 1948
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isPowerOffAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isPowerOffAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1956
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1958
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isPowerOffAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1963
    :goto_0
    return v1

    .line 1959
    :catch_0
    move-exception v0

    .line 1960
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1963
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSBeamAllowed()Z
    .locals 1

    .prologue
    .line 2487
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isSBeamAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isSBeamAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2496
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2498
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isSBeamAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2503
    :goto_0
    return v1

    .line 2499
    :catch_0
    move-exception v0

    .line 2500
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2503
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSDCardMoveAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3295
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3297
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isSDCardMoveAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3302
    :goto_0
    return v1

    .line 3298
    :catch_0
    move-exception v0

    .line 3299
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3302
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSDCardWriteAllowed()Z
    .locals 3

    .prologue
    .line 1569
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1571
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isSDCardWriteAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1576
    :goto_0
    return v1

    .line 1572
    :catch_0
    move-exception v0

    .line 1573
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1576
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSVoiceAllowed()Z
    .locals 1

    .prologue
    .line 2450
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isSVoiceAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isSVoiceAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2457
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2459
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isSVoiceAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2464
    :goto_0
    return v1

    .line 2460
    :catch_0
    move-exception v0

    .line 2461
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2464
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSafeModeAllowed()Z
    .locals 3

    .prologue
    .line 2820
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2822
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isSafeModeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2827
    :goto_0
    return v1

    .line 2823
    :catch_0
    move-exception v0

    .line 2824
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2827
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isScreenCaptureEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 565
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isScreenCaptureEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 566
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 568
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isScreenCaptureEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 573
    :goto_0
    return v1

    .line 569
    :catch_0
    move-exception v0

    .line 570
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 573
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isScreenCaptureEnabledInternal(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 577
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isScreenCaptureEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 578
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 580
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isScreenCaptureEnabledInternal(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 585
    :goto_0
    return v1

    .line 581
    :catch_0
    move-exception v0

    .line 582
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 585
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSdCardEnabled()Z
    .locals 3

    .prologue
    .line 919
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 921
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isSdCardEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 926
    :goto_0
    return v1

    .line 922
    :catch_0
    move-exception v0

    .line 923
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 926
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSettingsChangesAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 940
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 942
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isSettingsChangesAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 947
    :goto_0
    return v1

    .line 943
    :catch_0
    move-exception v0

    .line 944
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 947
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isShareListAllowed()Z
    .locals 4

    .prologue
    .line 2643
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2645
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IRestrictionPolicy;->isShareListAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2650
    :goto_0
    return v1

    .line 2646
    :catch_0
    move-exception v0

    .line 2647
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2650
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isShareListAllowed(Z)Z
    .locals 3
    .param p1, "showMg"    # Z

    .prologue
    .line 2657
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isShareListAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2658
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2660
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isShareListAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2665
    :goto_0
    return v1

    .line 2661
    :catch_0
    move-exception v0

    .line 2662
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2665
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isShareListAllowedAsUser(IZ)Z
    .locals 3
    .param p1, "userId"    # I
    .param p2, "showMsg"    # Z

    .prologue
    .line 2669
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isShareListAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2670
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2672
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IRestrictionPolicy;->isShareListAllowedAsUser(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2677
    :goto_0
    return v1

    .line 2673
    :catch_0
    move-exception v0

    .line 2674
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2677
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSmartClipModeAllowed()Z
    .locals 3

    .prologue
    .line 3430
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isSmartClipModeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3431
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3433
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isSmartClipModeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3438
    :goto_0
    return v1

    .line 3434
    :catch_0
    move-exception v0

    .line 3435
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3438
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSmartClipModeAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 3449
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3451
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isSmartClipModeAllowedInternal(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3456
    :goto_0
    return v1

    .line 3452
    :catch_0
    move-exception v0

    .line 3453
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3456
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isStatusBarExpansionAllowed()Z
    .locals 1

    .prologue
    .line 1855
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isStatusBarExpansionAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isStatusBarExpansionAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1862
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1864
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isStatusBarExpansionAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1869
    :goto_0
    return v1

    .line 1865
    :catch_0
    move-exception v0

    .line 1866
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1869
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isStopSystemAppAllowed()Z
    .locals 3

    .prologue
    .line 2188
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2190
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isStopSystemAppAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2195
    :goto_0
    return v1

    .line 2191
    :catch_0
    move-exception v0

    .line 2192
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2195
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isTetheringEnabled()Z
    .locals 3

    .prologue
    .line 665
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 667
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 672
    :goto_0
    return v1

    .line 668
    :catch_0
    move-exception v0

    .line 669
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 672
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbDebuggingEnabled()Z
    .locals 3

    .prologue
    .line 622
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 624
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbDebuggingEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 629
    :goto_0
    return v1

    .line 625
    :catch_0
    move-exception v0

    .line 626
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 629
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbHostStorageAllowed()Z
    .locals 1

    .prologue
    .line 2581
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isUsbHostStorageAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUsbHostStorageAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2565
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2567
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbHostStorageAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2572
    :goto_0
    return v1

    .line 2568
    :catch_0
    move-exception v0

    .line 2569
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2572
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbKiesAvailable(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1048
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1050
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbKiesAvailable(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1055
    :goto_0
    return v1

    .line 1051
    :catch_0
    move-exception v0

    .line 1052
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1055
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbMassStorageEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 712
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 714
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbMassStorageEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 719
    :goto_0
    return v1

    .line 715
    :catch_0
    move-exception v0

    .line 716
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 719
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbMediaPlayerAvailable(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1098
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1100
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbMediaPlayerAvailable(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1105
    :goto_0
    return v1

    .line 1101
    :catch_0
    move-exception v0

    .line 1102
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1105
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUsbTetheringEnabled()Z
    .locals 3

    .prologue
    .line 431
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 433
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isUsbTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 438
    :goto_0
    return v1

    .line 434
    :catch_0
    move-exception v0

    .line 435
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 438
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUseSecureKeypadEnabled()Z
    .locals 3

    .prologue
    .line 2739
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.isUseSecureKeypadEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2740
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2742
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isUseSecureKeypadEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2750
    :goto_0
    return v1

    .line 2743
    :catch_0
    move-exception v0

    .line 2744
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2747
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v1, v1, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-lez v1, :cond_1

    .line 2748
    const/4 v1, 0x1

    goto :goto_0

    .line 2750
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isUserMobileDataLimitAllowed()Z
    .locals 3

    .prologue
    .line 2348
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2350
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isUserMobileDataLimitAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2355
    :goto_0
    return v1

    .line 2351
    :catch_0
    move-exception v0

    .line 2352
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2355
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isVideoRecordAllowed()Z
    .locals 1

    .prologue
    .line 2122
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isVideoRecordAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isVideoRecordAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2098
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2100
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isVideoRecordAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2105
    :goto_0
    return v1

    .line 2101
    :catch_0
    move-exception v0

    .line 2102
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2105
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isVpnAllowed()Z
    .locals 3

    .prologue
    .line 1425
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1427
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isVpnAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1432
    :goto_0
    return v1

    .line 1428
    :catch_0
    move-exception v0

    .line 1429
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1432
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWallpaperChangeAllowed()Z
    .locals 4

    .prologue
    .line 1756
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1758
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IRestrictionPolicy;->isWallpaperChangeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1763
    :goto_0
    return v1

    .line 1759
    :catch_0
    move-exception v0

    .line 1760
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1763
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWallpaperChangeAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1771
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1773
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isWallpaperChangeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1778
    :goto_0
    return v1

    .line 1774
    :catch_0
    move-exception v0

    .line 1775
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1778
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWiFiEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 203
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getWifiPolicy()Landroid/app/enterprise/WifiPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/WifiPolicy;->isWifiAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isWifiDirectAllowed()Z
    .locals 2

    .prologue
    .line 2227
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "RestrictionPolicy.isWifiDirectAllowed"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2228
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/RestrictionPolicy;->isWifiDirectAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isWifiDirectAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 2237
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2239
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->isWifiDirectAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2244
    :goto_0
    return v1

    .line 2240
    :catch_0
    move-exception v0

    .line 2241
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2244
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWifiTetheringEnabled()Z
    .locals 3

    .prologue
    .line 478
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 480
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRestrictionPolicy;->isWifiTetheringEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 485
    :goto_0
    return v1

    .line 481
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 485
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setAllowNonMarketApps(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1122
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setAllowNonMarketApps"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1123
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1125
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setAllowNonMarketApps(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1130
    :goto_0
    return v1

    .line 1126
    :catch_0
    move-exception v0

    .line 1127
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1130
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBackgroundData(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1168
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setBackgroundData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1169
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1171
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setBackgroundData(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1176
    :goto_0
    return v1

    .line 1172
    :catch_0
    move-exception v0

    .line 1173
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1176
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBackup(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 781
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setBackup"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 782
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 784
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setBackup(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 789
    :goto_0
    return v1

    .line 785
    :catch_0
    move-exception v0

    .line 786
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 789
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBluetoothState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 226
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "RestrictionPolicy.setBluetoothState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BluetoothPolicy;->setBluetoothState(Z)Z

    move-result v0

    return v0
.end method

.method public setBluetoothTethering(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 501
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setBluetoothTethering"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 502
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 504
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setBluetoothTethering(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 509
    :goto_0
    return v1

    .line 505
    :catch_0
    move-exception v0

    .line 506
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 509
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCameraState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 266
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setCameraState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 267
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 269
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setCamera(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 274
    :goto_0
    return v1

    .line 270
    :catch_0
    move-exception v0

    .line 271
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 274
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCellularData(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 828
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setCellularData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 829
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 831
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setCellularData(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 836
    :goto_0
    return v1

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 836
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setClipboardEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 964
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setClipboardEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 965
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 967
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setClipboardEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 972
    :goto_0
    return v1

    .line 968
    :catch_0
    move-exception v0

    .line 969
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 972
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEnableNFC(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1213
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setEnableNFC"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1214
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1216
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setEnableNFC(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1221
    :goto_0
    return v1

    .line 1217
    :catch_0
    move-exception v0

    .line 1218
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1221
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setHeadphoneState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3180
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setHeadphoneState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3181
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3183
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setHeadphoneState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3188
    :goto_0
    return v1

    .line 3184
    :catch_0
    move-exception v0

    .line 3185
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3188
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setHomeKeyState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1327
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setHomeKeyState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1328
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1330
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setHomeKeyState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1335
    :goto_0
    return v1

    .line 1331
    :catch_0
    move-exception v0

    .line 1332
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1335
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLockScreenState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 3026
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy. setLockScreenState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3027
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3029
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setLockScreenState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3034
    :goto_0
    return v1

    .line 3030
    :catch_0
    move-exception v0

    .line 3031
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3034
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMicrophoneState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 321
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setMicrophoneState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 322
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 324
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setMicrophoneState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 329
    :goto_0
    return v1

    .line 325
    :catch_0
    move-exception v0

    .line 326
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 329
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMockLocation(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 736
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setMockLocation"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 737
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 739
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setMockLocation(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 744
    :goto_0
    return v1

    .line 740
    :catch_0
    move-exception v0

    .line 741
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 744
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNonTrustedAppInstallBlock(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 2757
    const/4 v0, 0x0

    return v0
.end method

.method public setScreenCapture(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 545
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 547
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setScreenCapture(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 552
    :goto_0
    return v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 552
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSdCardState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 873
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setSdCardState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 874
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 876
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setSdCardState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 881
    :goto_0
    return v1

    .line 877
    :catch_0
    move-exception v0

    .line 878
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 881
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTethering(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 645
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setTethering"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 646
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 648
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setTethering(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 653
    :goto_0
    return v1

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 653
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsbDebuggingEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 602
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUsbDebuggingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 603
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 605
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUsbDebuggingEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 610
    :goto_0
    return v1

    .line 606
    :catch_0
    move-exception v0

    .line 607
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 610
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsbKiesAvailability(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1026
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUsbKiesAvailability"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1027
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1029
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUsbKiesAvailability(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1034
    :goto_0
    return v1

    .line 1030
    :catch_0
    move-exception v0

    .line 1031
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1034
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsbMassStorage(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 689
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUsbMassStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 690
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 692
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUsbMassStorage(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 697
    :goto_0
    return v1

    .line 693
    :catch_0
    move-exception v0

    .line 694
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsbMediaPlayerAvailability(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1072
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUsbMediaPlayerAvailability"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1073
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1075
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUsbMediaPlayerAvailability(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1080
    :goto_0
    return v1

    .line 1076
    :catch_0
    move-exception v0

    .line 1077
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1080
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsbTethering(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 411
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUsbTethering"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 412
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 414
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUsbTethering(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 419
    :goto_0
    return v1

    .line 415
    :catch_0
    move-exception v0

    .line 416
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 419
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUseSecureKeypad(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2714
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setUseSecureKeypad"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2715
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2717
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setUseSecureKeypad(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2722
    :goto_0
    return v1

    .line 2718
    :catch_0
    move-exception v0

    .line 2719
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2722
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWiFiState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 185
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "RestrictionPolicy.setWiFiState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 186
    iget-object v0, p0, Landroid/app/enterprise/RestrictionPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getWifiPolicy()Landroid/app/enterprise/WifiPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/WifiPolicy;->setWifi(Z)Z

    move-result v0

    return v0
.end method

.method public setWifiTethering(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 458
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RestrictionPolicy.setWifiTethering"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 459
    invoke-direct {p0}, Landroid/app/enterprise/RestrictionPolicy;->getService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 461
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RestrictionPolicy;->mService:Landroid/app/enterprise/IRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRestrictionPolicy;->setWifiTethering(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 466
    :goto_0
    return v1

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 466
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
