.class public abstract Landroid/app/enterprise/IEmailAccountPolicy$Stub;
.super Landroid/os/Binder;
.source "IEmailAccountPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IEmailAccountPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IEmailAccountPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IEmailAccountPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IEmailAccountPolicy"

.field static final TRANSACTION_addNewAccount:I = 0x1

.field static final TRANSACTION_addNewAccount_ex:I = 0x2

.field static final TRANSACTION_addNewAccount_new:I = 0x21

.field static final TRANSACTION_deleteAccount:I = 0x1d

.field static final TRANSACTION_getAccountDetails:I = 0x1c

.field static final TRANSACTION_getAccountId:I = 0x1b

.field static final TRANSACTION_getAllEmailAccounts:I = 0x1f

.field static final TRANSACTION_getSecurityInComingServerPassword:I = 0x23

.field static final TRANSACTION_getSecurityOutGoingServerPassword:I = 0x22

.field static final TRANSACTION_removePendingAccount:I = 0x20

.field static final TRANSACTION_sendAccountsChangedBroadcast:I = 0x1e

.field static final TRANSACTION_setAccountName:I = 0x3

.field static final TRANSACTION_setAlwaysVibrateOnEmailNotification:I = 0x8

.field static final TRANSACTION_setAsDefaultAccount:I = 0x1a

.field static final TRANSACTION_setEmailAddress:I = 0x4

.field static final TRANSACTION_setInComingProtocol:I = 0xa

.field static final TRANSACTION_setInComingServerAcceptAllCertificates:I = 0xe

.field static final TRANSACTION_setInComingServerAddress:I = 0xb

.field static final TRANSACTION_setInComingServerLogin:I = 0xf

.field static final TRANSACTION_setInComingServerPassword:I = 0x10

.field static final TRANSACTION_setInComingServerPathPrefix:I = 0x11

.field static final TRANSACTION_setInComingServerPort:I = 0xc

.field static final TRANSACTION_setInComingServerSSL:I = 0xd

.field static final TRANSACTION_setOutGoingProtocol:I = 0x12

.field static final TRANSACTION_setOutGoingServerAcceptAllCertificates:I = 0x16

.field static final TRANSACTION_setOutGoingServerAddress:I = 0x13

.field static final TRANSACTION_setOutGoingServerLogin:I = 0x17

.field static final TRANSACTION_setOutGoingServerPassword:I = 0x18

.field static final TRANSACTION_setOutGoingServerPathPrefix:I = 0x19

.field static final TRANSACTION_setOutGoingServerPort:I = 0x14

.field static final TRANSACTION_setOutGoingServerSSL:I = 0x15

.field static final TRANSACTION_setSecurityInComingServerPassword:I = 0x25

.field static final TRANSACTION_setSecurityOutGoingServerPassword:I = 0x24

.field static final TRANSACTION_setSenderName:I = 0x6

.field static final TRANSACTION_setSignature:I = 0x7

.field static final TRANSACTION_setSilentVibrateOnEmailNotification:I = 0x9

.field static final TRANSACTION_setSyncInterval:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.IEmailAccountPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEmailAccountPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.IEmailAccountPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IEmailAccountPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/IEmailAccountPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/IEmailAccountPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IEmailAccountPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 32
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 794
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 46
    :sswitch_0
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v4, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 64
    .local v7, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 68
    .local v9, "_arg4":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 70
    .local v10, "_arg5":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 72
    .local v11, "_arg6":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    .line 74
    .local v12, "_arg7":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    .line 76
    .local v13, "_arg8":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 78
    .local v14, "_arg9":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    .line 80
    .local v15, "_arg10":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    .local v16, "_arg11":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 81
    invoke-virtual/range {v4 .. v16}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->addNewAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)J

    move-result-wide v30

    .line 82
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 83
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 84
    const/4 v4, 0x1

    goto :goto_0

    .line 57
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":I
    .end local v10    # "_arg5":Ljava/lang/String;
    .end local v11    # "_arg6":Ljava/lang/String;
    .end local v12    # "_arg7":Ljava/lang/String;
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":I
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Ljava/lang/String;
    .end local v30    # "_result":J
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 88
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 91
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 97
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 99
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 101
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 103
    .restart local v8    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    .line 105
    .restart local v9    # "_arg4":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 107
    .restart local v10    # "_arg5":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 109
    .restart local v11    # "_arg6":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    .line 111
    .restart local v12    # "_arg7":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    .line 113
    .restart local v13    # "_arg8":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v14

    .line 115
    .restart local v14    # "_arg9":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    .line 117
    .restart local v15    # "_arg10":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v16

    .line 119
    .restart local v16    # "_arg11":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    const/16 v17, 0x1

    .line 121
    .local v17, "_arg12":Z
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3

    const/16 v18, 0x1

    .line 123
    .local v18, "_arg13":Z
    :goto_4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    const/16 v19, 0x1

    .line 125
    .local v19, "_arg14":Z
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5

    const/16 v20, 0x1

    .line 127
    .local v20, "_arg15":Z
    :goto_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6

    const/16 v21, 0x1

    .line 129
    .local v21, "_arg16":Z
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7

    const/16 v22, 0x1

    .line 131
    .local v22, "_arg17":Z
    :goto_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v23

    .line 133
    .local v23, "_arg18":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8

    const/16 v24, 0x1

    .local v24, "_arg19":Z
    :goto_9
    move-object/from16 v4, p0

    .line 134
    invoke-virtual/range {v4 .. v24}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->addNewAccount_ex(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Z)J

    move-result-wide v30

    .line 135
    .restart local v30    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 136
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 137
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 94
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":I
    .end local v10    # "_arg5":Ljava/lang/String;
    .end local v11    # "_arg6":Ljava/lang/String;
    .end local v12    # "_arg7":Ljava/lang/String;
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":I
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Ljava/lang/String;
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Z
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    .end local v21    # "_arg16":Z
    .end local v22    # "_arg17":Z
    .end local v23    # "_arg18":Ljava/lang/String;
    .end local v24    # "_arg19":Z
    .end local v30    # "_result":J
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 119
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v7    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_arg3":Ljava/lang/String;
    .restart local v9    # "_arg4":I
    .restart local v10    # "_arg5":Ljava/lang/String;
    .restart local v11    # "_arg6":Ljava/lang/String;
    .restart local v12    # "_arg7":Ljava/lang/String;
    .restart local v13    # "_arg8":Ljava/lang/String;
    .restart local v14    # "_arg9":I
    .restart local v15    # "_arg10":Ljava/lang/String;
    .restart local v16    # "_arg11":Ljava/lang/String;
    :cond_2
    const/16 v17, 0x0

    goto :goto_3

    .line 121
    .restart local v17    # "_arg12":Z
    :cond_3
    const/16 v18, 0x0

    goto :goto_4

    .line 123
    .restart local v18    # "_arg13":Z
    :cond_4
    const/16 v19, 0x0

    goto :goto_5

    .line 125
    .restart local v19    # "_arg14":Z
    :cond_5
    const/16 v20, 0x0

    goto :goto_6

    .line 127
    .restart local v20    # "_arg15":Z
    :cond_6
    const/16 v21, 0x0

    goto :goto_7

    .line 129
    .restart local v21    # "_arg16":Z
    :cond_7
    const/16 v22, 0x0

    goto :goto_8

    .line 133
    .restart local v22    # "_arg17":Z
    .restart local v23    # "_arg18":Ljava/lang/String;
    :cond_8
    const/16 v24, 0x0

    goto :goto_9

    .line 141
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":I
    .end local v10    # "_arg5":Ljava/lang/String;
    .end local v11    # "_arg6":Ljava/lang/String;
    .end local v12    # "_arg7":Ljava/lang/String;
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":I
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Ljava/lang/String;
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Z
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    .end local v21    # "_arg16":Z
    .end local v22    # "_arg17":Z
    .end local v23    # "_arg18":Ljava/lang/String;
    :sswitch_3
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 143
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9

    .line 144
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 150
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 152
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 153
    .local v28, "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setAccountName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 154
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v30, :cond_a

    const/4 v4, 0x1

    :goto_b
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 147
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_9
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 155
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_a
    const/4 v4, 0x0

    goto :goto_b

    .line 160
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_4
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 162
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_b

    .line 163
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 169
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 171
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 172
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setEmailAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J

    move-result-wide v30

    .line 173
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 175
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 166
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":J
    :cond_b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 179
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_c

    .line 182
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 188
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 190
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 191
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSyncInterval(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v30

    .line 192
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 193
    if-eqz v30, :cond_d

    const/4 v4, 0x1

    :goto_e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 194
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 185
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 193
    .restart local v6    # "_arg1":I
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_d
    const/4 v4, 0x0

    goto :goto_e

    .line 198
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_6
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 200
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_e

    .line 201
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 207
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 209
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 210
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSenderName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 211
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 212
    if-eqz v30, :cond_f

    const/4 v4, 0x1

    :goto_10
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 213
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 204
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 212
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_f
    const/4 v4, 0x0

    goto :goto_10

    .line 217
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_7
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 219
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_10

    .line 220
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 226
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 228
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 229
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSignature(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 230
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v30, :cond_11

    const/4 v4, 0x1

    :goto_12
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 232
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 223
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_10
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 231
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_11
    const/4 v4, 0x0

    goto :goto_12

    .line 236
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_8
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_12

    .line 239
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 245
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_13

    const/4 v6, 0x1

    .line 247
    .local v6, "_arg1":Z
    :goto_14
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 248
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setAlwaysVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 249
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 250
    if-eqz v30, :cond_14

    const/4 v4, 0x1

    :goto_15
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 251
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 242
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_12
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 245
    :cond_13
    const/4 v6, 0x0

    goto :goto_14

    .line 250
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_14
    const/4 v4, 0x0

    goto :goto_15

    .line 255
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_9
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_15

    .line 258
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 264
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_16

    const/4 v6, 0x1

    .line 266
    .restart local v6    # "_arg1":Z
    :goto_17
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 267
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSilentVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 268
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 269
    if-eqz v30, :cond_17

    const/4 v4, 0x1

    :goto_18
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 270
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 261
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_15
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 264
    :cond_16
    const/4 v6, 0x0

    goto :goto_17

    .line 269
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_17
    const/4 v4, 0x0

    goto :goto_18

    .line 274
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_a
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 276
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_18

    .line 277
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 283
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 285
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 286
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingProtocol(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 287
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 288
    if-eqz v30, :cond_19

    const/4 v4, 0x1

    :goto_1a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 289
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 280
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_18
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 288
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_19
    const/4 v4, 0x0

    goto :goto_1a

    .line 293
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_b
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 295
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1a

    .line 296
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 302
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 304
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 305
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J

    move-result-wide v30

    .line 306
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 307
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 308
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 299
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":J
    :cond_1a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 312
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 314
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1b

    .line 315
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 321
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 323
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 324
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerPort(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v30

    .line 325
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 326
    if-eqz v30, :cond_1c

    const/4 v4, 0x1

    :goto_1d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 327
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 318
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_1b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 326
    .restart local v6    # "_arg1":I
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_1c
    const/4 v4, 0x0

    goto :goto_1d

    .line 331
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_d
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 333
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1d

    .line 334
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 340
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1e

    const/4 v6, 0x1

    .line 342
    .local v6, "_arg1":Z
    :goto_1f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 343
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 344
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 345
    if-eqz v30, :cond_1f

    const/4 v4, 0x1

    :goto_20
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 337
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_1d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 340
    :cond_1e
    const/4 v6, 0x0

    goto :goto_1f

    .line 345
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_1f
    const/4 v4, 0x0

    goto :goto_20

    .line 350
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_e
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 352
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_20

    .line 353
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 359
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_21

    const/4 v6, 0x1

    .line 361
    .restart local v6    # "_arg1":Z
    :goto_22
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 362
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 363
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    if-eqz v30, :cond_22

    const/4 v4, 0x1

    :goto_23
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 365
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 356
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_20
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 359
    :cond_21
    const/4 v6, 0x0

    goto :goto_22

    .line 364
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_22
    const/4 v4, 0x0

    goto :goto_23

    .line 369
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_f
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 371
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_23

    .line 372
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 378
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 380
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 381
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerLogin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J

    move-result-wide v30

    .line 382
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 383
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 384
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 375
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":J
    :cond_23
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 388
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 390
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_24

    .line 391
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 397
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 399
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 400
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 401
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 402
    if-eqz v30, :cond_25

    const/4 v4, 0x1

    :goto_26
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 403
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 394
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_24
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    .line 402
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_25
    const/4 v4, 0x0

    goto :goto_26

    .line 407
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_11
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 409
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_26

    .line 410
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 416
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 418
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 419
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setInComingServerPathPrefix(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 420
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 421
    if-eqz v30, :cond_27

    const/4 v4, 0x1

    :goto_28
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 422
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 413
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_26
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 421
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_27
    const/4 v4, 0x0

    goto :goto_28

    .line 426
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_12
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 428
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_28

    .line 429
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 435
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 437
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 438
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingProtocol(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 439
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 440
    if-eqz v30, :cond_29

    const/4 v4, 0x1

    :goto_2a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 441
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 432
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_28
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 440
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_29
    const/4 v4, 0x0

    goto :goto_2a

    .line 445
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_13
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 447
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2a

    .line 448
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 454
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 456
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 457
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J

    move-result-wide v30

    .line 458
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 459
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 460
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 451
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":J
    :cond_2a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 464
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 466
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2b

    .line 467
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 473
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 475
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 476
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerPort(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v30

    .line 477
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 478
    if-eqz v30, :cond_2c

    const/4 v4, 0x1

    :goto_2d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 479
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 470
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_2b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 478
    .restart local v6    # "_arg1":I
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_2c
    const/4 v4, 0x0

    goto :goto_2d

    .line 483
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_15
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 485
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2d

    .line 486
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 492
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2e

    const/4 v6, 0x1

    .line 494
    .local v6, "_arg1":Z
    :goto_2f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 495
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 496
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 497
    if-eqz v30, :cond_2f

    const/4 v4, 0x1

    :goto_30
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 498
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 489
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_2d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 492
    :cond_2e
    const/4 v6, 0x0

    goto :goto_2f

    .line 497
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_2f
    const/4 v4, 0x0

    goto :goto_30

    .line 502
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_16
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 504
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_30

    .line 505
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 511
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_31
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_31

    const/4 v6, 0x1

    .line 513
    .restart local v6    # "_arg1":Z
    :goto_32
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 514
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v30

    .line 515
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 516
    if-eqz v30, :cond_32

    const/4 v4, 0x1

    :goto_33
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 517
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 508
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_30
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_31

    .line 511
    :cond_31
    const/4 v6, 0x0

    goto :goto_32

    .line 516
    .restart local v6    # "_arg1":Z
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_32
    const/4 v4, 0x0

    goto :goto_33

    .line 521
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_17
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 523
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_33

    .line 524
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 530
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 532
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 533
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerLogin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J

    move-result-wide v30

    .line 534
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 535
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 536
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 527
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":J
    :cond_33
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 540
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 542
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_34

    .line 543
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 549
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 551
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 552
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 553
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 554
    if-eqz v30, :cond_35

    const/4 v4, 0x1

    :goto_36
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 555
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 546
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_34
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 554
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_35
    const/4 v4, 0x0

    goto :goto_36

    .line 559
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_19
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 561
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_36

    .line 562
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 568
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 570
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v28

    .line 571
    .restart local v28    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v28

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setOutGoingServerPathPrefix(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v30

    .line 572
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 573
    if-eqz v30, :cond_37

    const/4 v4, 0x1

    :goto_38
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 574
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 565
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :cond_36
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 573
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v28    # "_arg2":J
    .restart local v30    # "_result":Z
    :cond_37
    const/4 v4, 0x0

    goto :goto_38

    .line 578
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v28    # "_arg2":J
    .end local v30    # "_result":Z
    :sswitch_1a
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 580
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_38

    .line 581
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 587
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v26

    .line 588
    .local v26, "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setAsDefaultAccount(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v30

    .line 589
    .restart local v30    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 590
    if-eqz v30, :cond_39

    const/4 v4, 0x1

    :goto_3a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 591
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 584
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Z
    :cond_38
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 590
    .restart local v26    # "_arg1":J
    .restart local v30    # "_result":Z
    :cond_39
    const/4 v4, 0x0

    goto :goto_3a

    .line 595
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Z
    :sswitch_1b
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 597
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3a

    .line 598
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 604
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 606
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 608
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 609
    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->getAccountId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v30

    .line 610
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 611
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 612
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 601
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v30    # "_result":J
    :cond_3a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3b

    .line 616
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 618
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3b

    .line 619
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 625
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v26

    .line 626
    .restart local v26    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->getAccountDetails(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/Account;

    move-result-object v30

    .line 627
    .local v30, "_result":Landroid/app/enterprise/Account;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 628
    if-eqz v30, :cond_3c

    .line 629
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 630
    const/4 v4, 0x1

    move-object/from16 v0, v30

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Landroid/app/enterprise/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 635
    :goto_3d
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 622
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Landroid/app/enterprise/Account;
    :cond_3b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3c

    .line 633
    .restart local v26    # "_arg1":J
    .restart local v30    # "_result":Landroid/app/enterprise/Account;
    :cond_3c
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3d

    .line 639
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Landroid/app/enterprise/Account;
    :sswitch_1d
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 641
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3d

    .line 642
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 648
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v26

    .line 649
    .restart local v26    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->deleteAccount(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v30

    .line 650
    .local v30, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 651
    if-eqz v30, :cond_3e

    const/4 v4, 0x1

    :goto_3f
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 652
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 645
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Z
    :cond_3d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 651
    .restart local v26    # "_arg1":J
    .restart local v30    # "_result":Z
    :cond_3e
    const/4 v4, 0x0

    goto :goto_3f

    .line 656
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Z
    :sswitch_1e
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 658
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3f

    .line 659
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 664
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_40
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->sendAccountsChangedBroadcast(Landroid/app/enterprise/ContextInfo;)V

    .line 665
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 666
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 662
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_3f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_40

    .line 670
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 672
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_40

    .line 673
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 678
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->getAllEmailAccounts(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/Account;

    move-result-object v30

    .line 679
    .local v30, "_result":[Landroid/app/enterprise/Account;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 680
    const/4 v4, 0x1

    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-virtual {v0, v1, v4}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 681
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 676
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v30    # "_result":[Landroid/app/enterprise/Account;
    :cond_40
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 685
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 687
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_41

    .line 688
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 694
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_42
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 696
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 698
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 699
    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->removePendingAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 701
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 691
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    :cond_41
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_42

    .line 705
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 707
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_42

    .line 708
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 714
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_43
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_43

    .line 715
    sget-object v4, Landroid/app/enterprise/EmailAccount;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/enterprise/EmailAccount;

    .line 720
    .local v6, "_arg1":Landroid/app/enterprise/EmailAccount;
    :goto_44
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->addNewAccount_new(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/EmailAccount;)J

    move-result-wide v30

    .line 721
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 722
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 723
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 711
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Landroid/app/enterprise/EmailAccount;
    .end local v30    # "_result":J
    :cond_42
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_43

    .line 718
    :cond_43
    const/4 v6, 0x0

    .restart local v6    # "_arg1":Landroid/app/enterprise/EmailAccount;
    goto :goto_44

    .line 727
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Landroid/app/enterprise/EmailAccount;
    :sswitch_22
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 729
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_44

    .line 730
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 736
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_45
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v26

    .line 737
    .restart local v26    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->getSecurityOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;

    move-result-object v30

    .line 738
    .local v30, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 739
    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 740
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 733
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Ljava/lang/String;
    :cond_44
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_45

    .line 744
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 746
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_45

    .line 747
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 753
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_46
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v26

    .line 754
    .restart local v26    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v26

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->getSecurityInComingServerPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;

    move-result-object v30

    .line 755
    .restart local v30    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 756
    move-object/from16 v0, p3

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 757
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 750
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v26    # "_arg1":J
    .end local v30    # "_result":Ljava/lang/String;
    :cond_45
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_46

    .line 761
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_24
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 763
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_46

    .line 764
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 770
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_47
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 771
    .local v6, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSecurityOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v30

    .line 772
    .local v30, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 773
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 774
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 767
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v30    # "_result":J
    :cond_46
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_47

    .line 778
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_25
    const-string v4, "android.app.enterprise.IEmailAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 780
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_47

    .line 781
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 787
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_48
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 788
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->setSecurityInComingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v30

    .line 789
    .restart local v30    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 790
    move-object/from16 v0, p3

    move-wide/from16 v1, v30

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 791
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 784
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v30    # "_result":J
    :cond_47
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_48

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
