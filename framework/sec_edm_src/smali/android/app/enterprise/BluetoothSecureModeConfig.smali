.class public Landroid/app/enterprise/BluetoothSecureModeConfig;
.super Ljava/lang/Object;
.source "BluetoothSecureModeConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/BluetoothSecureModeConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a2dpEnable:Z

.field public ftpEnable:Z

.field public gattEnable:Z

.field public hdpEnable:Z

.field public hfpEnable:Z

.field public hidEnable:Z

.field public mapEnable:Z

.field public oppEnable:Z

.field public pairingMode:Z

.field public panEnable:Z

.field public pbapEnable:Z

.field public sapEnable:Z

.field public scanMode:Z

.field public whitelistEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 135
    new-instance v0, Landroid/app/enterprise/BluetoothSecureModeConfig$1;

    invoke-direct {v0}, Landroid/app/enterprise/BluetoothSecureModeConfig$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/BluetoothSecureModeConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    invoke-virtual {p0, p1}, Landroid/app/enterprise/BluetoothSecureModeConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 157
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/BluetoothSecureModeConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/BluetoothSecureModeConfig$1;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/app/enterprise/BluetoothSecureModeConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 203
    if-nez p1, :cond_0

    .line 235
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->scanMode:Z

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->pairingMode:Z

    .line 211
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hfpEnable:Z

    .line 213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->a2dpEnable:Z

    .line 215
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hidEnable:Z

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hdpEnable:Z

    .line 219
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->panEnable:Z

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->oppEnable:Z

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->pbapEnable:Z

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->gattEnable:Z

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->mapEnable:Z

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->ftpEnable:Z

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    iput-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->sapEnable:Z

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_e

    :goto_e
    iput-boolean v1, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->whitelistEnable:Z

    goto :goto_0

    :cond_1
    move v0, v2

    .line 207
    goto :goto_1

    :cond_2
    move v0, v2

    .line 209
    goto :goto_2

    :cond_3
    move v0, v2

    .line 211
    goto :goto_3

    :cond_4
    move v0, v2

    .line 213
    goto :goto_4

    :cond_5
    move v0, v2

    .line 215
    goto :goto_5

    :cond_6
    move v0, v2

    .line 217
    goto :goto_6

    :cond_7
    move v0, v2

    .line 219
    goto :goto_7

    :cond_8
    move v0, v2

    .line 221
    goto :goto_8

    :cond_9
    move v0, v2

    .line 223
    goto :goto_9

    :cond_a
    move v0, v2

    .line 225
    goto :goto_a

    :cond_b
    move v0, v2

    .line 227
    goto :goto_b

    :cond_c
    move v0, v2

    .line 229
    goto :goto_c

    :cond_d
    move v0, v2

    .line 231
    goto :goto_d

    :cond_e
    move v1, v2

    .line 233
    goto :goto_e
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163
    if-nez p1, :cond_0

    .line 195
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->scanMode:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->pairingMode:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hfpEnable:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 173
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->a2dpEnable:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 175
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hidEnable:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 177
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->hdpEnable:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->panEnable:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 181
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->oppEnable:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 183
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->pbapEnable:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->gattEnable:Z

    if-eqz v0, :cond_a

    move v0, v1

    :goto_a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->mapEnable:Z

    if-eqz v0, :cond_b

    move v0, v1

    :goto_b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->ftpEnable:Z

    if-eqz v0, :cond_c

    move v0, v1

    :goto_c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->sapEnable:Z

    if-eqz v0, :cond_d

    move v0, v1

    :goto_d
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    iget-boolean v0, p0, Landroid/app/enterprise/BluetoothSecureModeConfig;->whitelistEnable:Z

    if-eqz v0, :cond_e

    :goto_e
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 167
    goto :goto_1

    :cond_2
    move v0, v2

    .line 169
    goto :goto_2

    :cond_3
    move v0, v2

    .line 171
    goto :goto_3

    :cond_4
    move v0, v2

    .line 173
    goto :goto_4

    :cond_5
    move v0, v2

    .line 175
    goto :goto_5

    :cond_6
    move v0, v2

    .line 177
    goto :goto_6

    :cond_7
    move v0, v2

    .line 179
    goto :goto_7

    :cond_8
    move v0, v2

    .line 181
    goto :goto_8

    :cond_9
    move v0, v2

    .line 183
    goto :goto_9

    :cond_a
    move v0, v2

    .line 185
    goto :goto_a

    :cond_b
    move v0, v2

    .line 187
    goto :goto_b

    :cond_c
    move v0, v2

    .line 189
    goto :goto_c

    :cond_d
    move v0, v2

    .line 191
    goto :goto_d

    :cond_e
    move v1, v2

    .line 193
    goto :goto_e
.end method
