.class public abstract Landroid/app/enterprise/IDeviceInfo$Stub;
.super Landroid/os/Binder;
.source "IDeviceInfo.java"

# interfaces
.implements Landroid/app/enterprise/IDeviceInfo;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IDeviceInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IDeviceInfo$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IDeviceInfo"

.field static final TRANSACTION_clearCallingLog:I = 0x26

.field static final TRANSACTION_clearMMSLog:I = 0x2b

.field static final TRANSACTION_clearSMSLog:I = 0x1e

.field static final TRANSACTION_dataUsageTimerActivation:I = 0x3c

.field static final TRANSACTION_enableCallingCapture:I = 0x21

.field static final TRANSACTION_enableMMSCapture:I = 0x27

.field static final TRANSACTION_enableSMSCapture:I = 0x19

.field static final TRANSACTION_getAvailableCapacityExternal:I = 0x4

.field static final TRANSACTION_getAvailableCapacityInternal:I = 0x6

.field static final TRANSACTION_getAvailableRamMemory:I = 0x1f

.field static final TRANSACTION_getBytesReceivedNetwork:I = 0x38

.field static final TRANSACTION_getBytesReceivedWiFi:I = 0x36

.field static final TRANSACTION_getBytesSentNetwork:I = 0x37

.field static final TRANSACTION_getBytesSentWiFi:I = 0x35

.field static final TRANSACTION_getCellTowerCID:I = 0x3d

.field static final TRANSACTION_getCellTowerLAC:I = 0x3e

.field static final TRANSACTION_getCellTowerPSC:I = 0x3f

.field static final TRANSACTION_getCellTowerRSSI:I = 0x40

.field static final TRANSACTION_getDataCallLog:I = 0x30

.field static final TRANSACTION_getDataCallLoggingEnabled:I = 0x2e

.field static final TRANSACTION_getDataCallStatisticsEnabled:I = 0x32

.field static final TRANSACTION_getDataUsageTimer:I = 0x3b

.field static final TRANSACTION_getDeviceMaker:I = 0xf

.field static final TRANSACTION_getDeviceName:I = 0x9

.field static final TRANSACTION_getDeviceOS:I = 0x10

.field static final TRANSACTION_getDeviceOSVersion:I = 0x11

.field static final TRANSACTION_getDevicePlatform:I = 0x12

.field static final TRANSACTION_getDeviceProcessorSpeed:I = 0x14

.field static final TRANSACTION_getDeviceProcessorType:I = 0x13

.field static final TRANSACTION_getDroppedCallsCount:I = 0x15

.field static final TRANSACTION_getInboundMMSCaptured:I = 0x2a

.field static final TRANSACTION_getInboundSMSCaptured:I = 0x1c

.field static final TRANSACTION_getIncomingCallingCaptured:I = 0x24

.field static final TRANSACTION_getMissedCallsCount:I = 0x16

.field static final TRANSACTION_getModelName:I = 0x7

.field static final TRANSACTION_getModelNumber:I = 0x8

.field static final TRANSACTION_getModemFirmware:I = 0xc

.field static final TRANSACTION_getOutboundMMSCaptured:I = 0x29

.field static final TRANSACTION_getOutboundSMSCaptured:I = 0x1b

.field static final TRANSACTION_getOutgoingCallingCaptured:I = 0x23

.field static final TRANSACTION_getPlatformSDK:I = 0xd

.field static final TRANSACTION_getPlatformVersion:I = 0xe

.field static final TRANSACTION_getPlatformVersionName:I = 0xb

.field static final TRANSACTION_getSerialNumber:I = 0xa

.field static final TRANSACTION_getSuccessCallsCount:I = 0x17

.field static final TRANSACTION_getTotalCapacityExternal:I = 0x3

.field static final TRANSACTION_getTotalCapacityInternal:I = 0x5

.field static final TRANSACTION_getTotalRamMemory:I = 0x20

.field static final TRANSACTION_getWifiStatisticEnabled:I = 0x34

.field static final TRANSACTION_isCallingCaptureEnabled:I = 0x22

.field static final TRANSACTION_isDeviceLocked:I = 0x2

.field static final TRANSACTION_isDeviceRooted:I = 0x41

.field static final TRANSACTION_isDeviceSecure:I = 0x1

.field static final TRANSACTION_isMMSCaptureEnabled:I = 0x28

.field static final TRANSACTION_isSMSCaptureEnabled:I = 0x1a

.field static final TRANSACTION_resetCallsCount:I = 0x18

.field static final TRANSACTION_resetDataCallLogging:I = 0x2f

.field static final TRANSACTION_resetDataUsage:I = 0x39

.field static final TRANSACTION_setDataCallLoggingEnabled:I = 0x2d

.field static final TRANSACTION_setDataCallStatisticsEnabled:I = 0x31

.field static final TRANSACTION_setDataUsageTimer:I = 0x3a

.field static final TRANSACTION_setWifiStatisticEnabled:I = 0x33

.field static final TRANSACTION_storeCalling:I = 0x25

.field static final TRANSACTION_storeMMS:I = 0x2c

.field static final TRANSACTION_storeSMS:I = 0x1d


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IDeviceInfo$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IDeviceInfo;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "android.app.enterprise.IDeviceInfo"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IDeviceInfo;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Landroid/app/enterprise/IDeviceInfo;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Landroid/app/enterprise/IDeviceInfo$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IDeviceInfo$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 1042
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1

    .line 53
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 58
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isDeviceSecure(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 59
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 60
    if-eqz v6, :cond_0

    move v0, v9

    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 56
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 65
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3

    .line 68
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 73
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isDeviceLocked(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 74
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 75
    if-eqz v6, :cond_2

    move v0, v9

    :cond_2
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 71
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_3
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 80
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 83
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 88
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getTotalCapacityExternal(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 89
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 90
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 86
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 95
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 103
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getAvailableCapacityExternal(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 104
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 105
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 101
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 110
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 113
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 118
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getTotalCapacityInternal(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 119
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 116
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 125
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 128
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 133
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getAvailableCapacityInternal(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 134
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 131
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 140
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 143
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 148
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getModelName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 149
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 146
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 155
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 158
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 163
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getModelNumber(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 164
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 165
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 161
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 170
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 173
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 178
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 179
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 180
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 176
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 185
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 188
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 193
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getSerialNumber(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 194
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 195
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 191
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 200
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 203
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 208
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getPlatformVersionName(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 209
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 210
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 206
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 215
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 218
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 223
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getModemFirmware(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 224
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 225
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 221
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 230
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 232
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_e

    .line 233
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 238
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getPlatformSDK(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 239
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 240
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 236
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 245
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 247
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 248
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 253
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getPlatformVersion(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 254
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 255
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 251
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 260
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 262
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 263
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 268
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceMaker(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 269
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 270
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 266
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_10
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 275
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 278
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 283
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceOS(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 284
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 285
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 281
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_11
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 290
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 293
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 298
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceOSVersion(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 299
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 300
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 296
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_12
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 305
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 308
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 313
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDevicePlatform(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 314
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 315
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 311
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_13
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 320
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    .line 323
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 328
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceProcessorType(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 329
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 326
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_14
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 335
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    .line 338
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 343
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_14
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDeviceProcessorSpeed(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 344
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 345
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 341
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_15
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_14

    .line 350
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    .line 353
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 358
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDroppedCallsCount(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 359
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 360
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 356
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_16
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 365
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 367
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 368
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 373
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getMissedCallsCount(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 374
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 375
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 371
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_17
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 380
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_18

    .line 383
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 388
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getSuccessCallsCount(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 389
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 390
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 386
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_18
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 395
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 397
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1a

    .line 398
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 403
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->resetCallsCount(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 404
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 405
    if-eqz v6, :cond_19

    move v0, v9

    :cond_19
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 401
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 410
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 412
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1c

    .line 413
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 419
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1d

    move v2, v9

    .line 420
    .local v2, "_arg1":Z
    :goto_1a
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->enableSMSCapture(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 421
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 422
    if-eqz v6, :cond_1b

    move v0, v9

    :cond_1b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 416
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_1c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    :cond_1d
    move v2, v0

    .line 419
    goto :goto_1a

    .line 427
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1f

    .line 430
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 435
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isSMSCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 436
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 437
    if-eqz v6, :cond_1e

    move v0, v9

    :cond_1e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 433
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 442
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 444
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_20

    .line 445
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 450
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getOutboundSMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 451
    .local v8, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 452
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 448
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_20
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 457
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 459
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    .line 460
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 465
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getInboundSMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 466
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 467
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 463
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_21
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    .line 472
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 476
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 478
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 480
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_22

    move v4, v9

    .line 481
    .local v4, "_arg3":Z
    :goto_1e
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/app/enterprise/IDeviceInfo$Stub;->storeSMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 482
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v4    # "_arg3":Z
    :cond_22
    move v4, v0

    .line 480
    goto :goto_1e

    .line 487
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_1e
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 489
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_24

    .line 490
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 495
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->clearSMSLog(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 496
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 497
    if-eqz v6, :cond_23

    move v0, v9

    :cond_23
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 493
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_24
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .line 502
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 504
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_25

    .line 505
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 510
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getAvailableRamMemory(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 511
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 512
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 508
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_25
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 517
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 519
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_26

    .line 520
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 525
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getTotalRamMemory(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 526
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 527
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 523
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_26
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 532
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 534
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_28

    .line 535
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 541
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_29

    move v2, v9

    .line 542
    .local v2, "_arg1":Z
    :goto_23
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->enableCallingCapture(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 543
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 544
    if-eqz v6, :cond_27

    move v0, v9

    :cond_27
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 538
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_28
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    :cond_29
    move v2, v0

    .line 541
    goto :goto_23

    .line 549
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 551
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2b

    .line 552
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 557
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isCallingCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 558
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 559
    if-eqz v6, :cond_2a

    move v0, v9

    :cond_2a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 555
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_2b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 564
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 566
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 567
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 572
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getOutgoingCallingCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 573
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 574
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 570
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    .line 579
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_24
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 581
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2d

    .line 582
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 587
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_26
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getIncomingCallingCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 588
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 589
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 585
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_26

    .line 594
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_25
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 596
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 598
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 600
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 602
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 604
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2e

    move v5, v9

    .local v5, "_arg4":Z
    :goto_27
    move-object v0, p0

    .line 605
    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/IDeviceInfo$Stub;->storeCalling(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 606
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v5    # "_arg4":Z
    :cond_2e
    move v5, v0

    .line 604
    goto :goto_27

    .line 611
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    :sswitch_26
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 613
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_30

    .line 614
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 619
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->clearCallingLog(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 620
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 621
    if-eqz v6, :cond_2f

    move v0, v9

    :cond_2f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 617
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_30
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 626
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 628
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_32

    .line 629
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 635
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_33

    move v2, v9

    .line 636
    .local v2, "_arg1":Z
    :goto_2a
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->enableMMSCapture(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 637
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 638
    if-eqz v6, :cond_31

    move v0, v9

    :cond_31
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 632
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_32
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    :cond_33
    move v2, v0

    .line 635
    goto :goto_2a

    .line 643
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_28
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 645
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_35

    .line 646
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 651
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isMMSCaptureEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 652
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 653
    if-eqz v6, :cond_34

    move v0, v9

    :cond_34
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 649
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_35
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 658
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_29
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 660
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_36

    .line 661
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 666
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getOutboundMMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 667
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 668
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 664
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_36
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 673
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2a
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_37

    .line 676
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 681
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getInboundMMSCaptured(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v8

    .line 682
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 683
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 679
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_37
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 688
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2b
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 690
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_39

    .line 691
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 696
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->clearMMSLog(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 697
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 698
    if-eqz v6, :cond_38

    move v0, v9

    :cond_38
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 694
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_39
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 703
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2c
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 705
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 707
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 709
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 711
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3a

    move v4, v9

    .line 712
    .local v4, "_arg3":Z
    :goto_2f
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/app/enterprise/IDeviceInfo$Stub;->storeMMS(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 713
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v4    # "_arg3":Z
    :cond_3a
    move v4, v0

    .line 711
    goto :goto_2f

    .line 718
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :sswitch_2d
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 720
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3c

    .line 721
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 727
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3d

    move v2, v9

    .line 728
    .local v2, "_arg1":Z
    :goto_31
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->setDataCallLoggingEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 729
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 730
    if-eqz v6, :cond_3b

    move v0, v9

    :cond_3b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 724
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_3c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    :cond_3d
    move v2, v0

    .line 727
    goto :goto_31

    .line 735
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 737
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3f

    .line 738
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 743
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDataCallLoggingEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 744
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 745
    if-eqz v6, :cond_3e

    move v0, v9

    :cond_3e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 741
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_3f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 750
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2f
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 752
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_41

    .line 753
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 759
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_33
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 760
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->resetDataCallLogging(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 761
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 762
    if-eqz v6, :cond_40

    move v0, v9

    :cond_40
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 756
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_41
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_33

    .line 767
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_30
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 769
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_42

    .line 770
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 776
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 777
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDataCallLog(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    .line 778
    .restart local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 779
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 773
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_42
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 784
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_31
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 786
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_44

    .line 787
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 793
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_45

    move v2, v9

    .line 794
    .local v2, "_arg1":Z
    :goto_36
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->setDataCallStatisticsEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 795
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 796
    if-eqz v6, :cond_43

    move v0, v9

    :cond_43
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 790
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_44
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    :cond_45
    move v2, v0

    .line 793
    goto :goto_36

    .line 801
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_32
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 803
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_47

    .line 804
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 809
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDataCallStatisticsEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 810
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 811
    if-eqz v6, :cond_46

    move v0, v9

    :cond_46
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 807
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_47
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 816
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_33
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 818
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_49

    .line 819
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 825
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_38
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_4a

    move v2, v9

    .line 826
    .restart local v2    # "_arg1":Z
    :goto_39
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->setWifiStatisticEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 827
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 828
    if-eqz v6, :cond_48

    move v0, v9

    :cond_48
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 822
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_49
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_38

    :cond_4a
    move v2, v0

    .line 825
    goto :goto_39

    .line 833
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_34
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 835
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_4c

    .line 836
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 841
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3a
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getWifiStatisticEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 842
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 843
    if-eqz v6, :cond_4b

    move v0, v9

    :cond_4b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 839
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_4c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3a

    .line 848
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_35
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 850
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4d

    .line 851
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 856
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3b
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getBytesSentWiFi(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 857
    .local v6, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 858
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 854
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_4d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3b

    .line 863
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_36
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 865
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4e

    .line 866
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 871
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getBytesReceivedWiFi(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 872
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 873
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 869
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_4e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3c

    .line 878
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_37
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 880
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4f

    .line 881
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 886
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getBytesSentNetwork(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 887
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 888
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 884
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_4f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3d

    .line 893
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_38
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 895
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_50

    .line 896
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 901
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getBytesReceivedNetwork(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v6

    .line 902
    .restart local v6    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 903
    invoke-virtual {p3, v6, v7}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 899
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":J
    :cond_50
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 908
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_39
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 910
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_51

    .line 911
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 916
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->resetDataUsage(Landroid/app/enterprise/ContextInfo;)V

    .line 917
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 914
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_51
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 922
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3a
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 924
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_53

    .line 925
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 931
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_40
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 932
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/IDeviceInfo$Stub;->setDataUsageTimer(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v6

    .line 933
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 934
    if-eqz v6, :cond_52

    move v0, v9

    :cond_52
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 928
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_53
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_40

    .line 939
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3b
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 941
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_54

    .line 942
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 947
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getDataUsageTimer(Landroid/app/enterprise/ContextInfo;)I

    move-result v6

    .line 948
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 949
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 945
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":I
    :cond_54
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 954
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3c
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 956
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_55

    .line 957
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 962
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_42
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->dataUsageTimerActivation(Landroid/app/enterprise/ContextInfo;)V

    .line 963
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 960
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_55
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_42

    .line 968
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3d
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 970
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_56

    .line 971
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 976
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_43
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getCellTowerCID(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 977
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 978
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 974
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_56
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_43

    .line 983
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3e
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 985
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_57

    .line 986
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 991
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_44
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getCellTowerLAC(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 992
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 993
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 989
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_57
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_44

    .line 998
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3f
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1000
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_58

    .line 1001
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 1006
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_45
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getCellTowerPSC(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 1007
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1008
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1004
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_58
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_45

    .line 1013
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_40
    const-string v0, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1015
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_59

    .line 1016
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 1021
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_46
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->getCellTowerRSSI(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v6

    .line 1022
    .restart local v6    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1023
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1019
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Ljava/lang/String;
    :cond_59
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_46

    .line 1028
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_41
    const-string v10, "android.app.enterprise.IDeviceInfo"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1030
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_5b

    .line 1031
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 1036
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_47
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IDeviceInfo$Stub;->isDeviceRooted(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 1037
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1038
    if-eqz v6, :cond_5a

    move v0, v9

    :cond_5a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 1034
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_5b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_47

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
