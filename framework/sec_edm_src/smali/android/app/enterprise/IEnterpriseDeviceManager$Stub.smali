.class public abstract Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseDeviceManager.java"

# interfaces
.implements Landroid/app/enterprise/IEnterpriseDeviceManager;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IEnterpriseDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IEnterpriseDeviceManager$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IEnterpriseDeviceManager"

.field static final TRANSACTION_activateAdminForUser:I = 0x1d

.field static final TRANSACTION_activateDevicePermissions:I = 0x1f

.field static final TRANSACTION_addProxyAdmin:I = 0x19

.field static final TRANSACTION_configureContainerAdminForMigration:I = 0x21

.field static final TRANSACTION_deactivateAdminForUser:I = 0x1e

.field static final TRANSACTION_disableConstrainedState:I = 0x26

.field static final TRANSACTION_enableConstrainedState:I = 0x25

.field static final TRANSACTION_enforceActiveAdminPermission:I = 0xa

.field static final TRANSACTION_enforceActiveAdminPermissionByContext:I = 0x12

.field static final TRANSACTION_enforceContainerOwnerShipPermissionByContext:I = 0x14

.field static final TRANSACTION_enforceOwnerOnlyAndActiveAdminPermission:I = 0x15

.field static final TRANSACTION_enforceOwnerOnlyPermissionByContext:I = 0x16

.field static final TRANSACTION_enforcePermissionByContext:I = 0x13

.field static final TRANSACTION_getActiveAdminComponent:I = 0x2

.field static final TRANSACTION_getActiveAdmins:I = 0x3

.field static final TRANSACTION_getActiveAdminsInfo:I = 0x18

.field static final TRANSACTION_getAdminRemovable:I = 0x8

.field static final TRANSACTION_getConstrainedState:I = 0x28

.field static final TRANSACTION_getMyKnoxAdmin:I = 0x2b

.field static final TRANSACTION_getProxyAdmins:I = 0x1b

.field static final TRANSACTION_getRemoveWarning:I = 0x9

.field static final TRANSACTION_hasAnyActiveAdmin:I = 0xc

.field static final TRANSACTION_hasGrantedPolicy:I = 0x5

.field static final TRANSACTION_isAdminActive:I = 0x1

.field static final TRANSACTION_isAdminRemovable:I = 0xe

.field static final TRANSACTION_isAdminRemovableInternal:I = 0xf

.field static final TRANSACTION_isMigrationStateNOK:I = 0x23

.field static final TRANSACTION_isRestrictedByConstrainedState:I = 0x27

.field static final TRANSACTION_migrateApplicationDisablePolicy:I = 0x22

.field static final TRANSACTION_migrateEnterpriseContainer:I = 0x20

.field static final TRANSACTION_packageHasActiveAdmins:I = 0xd

.field static final TRANSACTION_packageHasActiveAdminsAsUser:I = 0x24

.field static final TRANSACTION_removeActiveAdmin:I = 0x6

.field static final TRANSACTION_removeActiveAdminFromDpm:I = 0xb

.field static final TRANSACTION_removeProxyAdmin:I = 0x1c

.field static final TRANSACTION_selfUpdateAdmin:I = 0x10

.field static final TRANSACTION_sendIntent:I = 0x29

.field static final TRANSACTION_setActiveAdmin:I = 0x4

.field static final TRANSACTION_setActiveAdminSilent:I = 0x17

.field static final TRANSACTION_setAdminRemovable:I = 0x7

.field static final TRANSACTION_setB2BMode:I = 0x11

.field static final TRANSACTION_setMyKnoxAdmin:I = 0x2a

.field static final TRANSACTION_updateProxyAdmin:I = 0x1a


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseDeviceManager;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IEnterpriseDeviceManager;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/IEnterpriseDeviceManager;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 687
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 46
    :sswitch_0
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v9, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_0

    .line 54
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 59
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_1
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->isAdminActive(Landroid/content/ComponentName;)Z

    move-result v5

    .line 60
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 61
    if-eqz v5, :cond_1

    const/4 v9, 0x1

    :goto_2
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 62
    const/4 v9, 0x1

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v5    # "_result":Z
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_1

    .line 61
    .restart local v5    # "_result":Z
    :cond_1
    const/4 v9, 0x0

    goto :goto_2

    .line 66
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v5    # "_result":Z
    :sswitch_2
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getActiveAdminComponent()Landroid/content/ComponentName;

    move-result-object v5

    .line 68
    .local v5, "_result":Landroid/content/ComponentName;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 69
    if-eqz v5, :cond_2

    .line 70
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/content/ComponentName;->writeToParcel(Landroid/os/Parcel;I)V

    .line 76
    :goto_3
    const/4 v9, 0x1

    goto :goto_0

    .line 74
    :cond_2
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 80
    .end local v5    # "_result":Landroid/content/ComponentName;
    :sswitch_3
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getActiveAdmins(I)Ljava/util/List;

    move-result-object v8

    .line 84
    .local v8, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 86
    const/4 v9, 0x1

    goto :goto_0

    .line 90
    .end local v0    # "_arg0":I
    .end local v8    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    :sswitch_4
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_3

    .line 93
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 99
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_4

    const/4 v2, 0x1

    .line 100
    .local v2, "_arg1":Z
    :goto_5
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->setActiveAdmin(Landroid/content/ComponentName;Z)V

    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 102
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 96
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_4

    .line 99
    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    .line 106
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_5
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_5

    .line 109
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 115
    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 116
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->hasGrantedPolicy(Landroid/content/ComponentName;I)Z

    move-result v5

    .line 117
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    if-eqz v5, :cond_6

    const/4 v9, 0x1

    :goto_7
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 119
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 112
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    .end local v5    # "_result":Z
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_6

    .line 118
    .restart local v2    # "_arg1":I
    .restart local v5    # "_result":Z
    :cond_6
    const/4 v9, 0x0

    goto :goto_7

    .line 123
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    .end local v5    # "_result":Z
    :sswitch_6
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_7

    .line 126
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 131
    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->removeActiveAdmin(Landroid/content/ComponentName;)V

    .line 132
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 133
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 129
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_8

    .line 137
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_7
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_8

    .line 140
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 146
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_9

    const/4 v2, 0x1

    .line 148
    .local v2, "_arg1":Z
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 149
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->setAdminRemovable(Landroid/app/enterprise/ContextInfo;ZLjava/lang/String;)Z

    move-result v5

    .line 150
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v5, :cond_a

    const/4 v9, 0x1

    :goto_b
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 143
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v5    # "_result":Z
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 146
    :cond_9
    const/4 v2, 0x0

    goto :goto_a

    .line 151
    .restart local v2    # "_arg1":Z
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_result":Z
    :cond_a
    const/4 v9, 0x0

    goto :goto_b

    .line 156
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v5    # "_result":Z
    :sswitch_8
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_b

    .line 159
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 165
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 166
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getAdminRemovable(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v5

    .line 167
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 168
    if-eqz v5, :cond_c

    const/4 v9, 0x1

    :goto_d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 169
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 162
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Z
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 168
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Z
    :cond_c
    const/4 v9, 0x0

    goto :goto_d

    .line 173
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Z
    :sswitch_9
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 175
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_d

    .line 176
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 182
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_e

    .line 183
    sget-object v9, Landroid/os/RemoteCallback;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/RemoteCallback;

    .line 188
    .local v2, "_arg1":Landroid/os/RemoteCallback;
    :goto_f
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getRemoveWarning(Landroid/content/ComponentName;Landroid/os/RemoteCallback;)V

    .line 189
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 190
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 179
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":Landroid/os/RemoteCallback;
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_e

    .line 186
    :cond_e
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/os/RemoteCallback;
    goto :goto_f

    .line 194
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":Landroid/os/RemoteCallback;
    :sswitch_a
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 196
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforceActiveAdminPermission(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 199
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 203
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_b
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_f

    .line 206
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 212
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 213
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->removeActiveAdminFromDpm(Landroid/content/ComponentName;I)V

    .line 214
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 215
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 209
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    :cond_f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_10

    .line 219
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_c
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->hasAnyActiveAdmin()Z

    move-result v5

    .line 221
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 222
    if-eqz v5, :cond_10

    const/4 v9, 0x1

    :goto_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 222
    :cond_10
    const/4 v9, 0x0

    goto :goto_11

    .line 227
    .end local v5    # "_result":Z
    :sswitch_d
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->packageHasActiveAdmins(Ljava/lang/String;)Z

    move-result v5

    .line 231
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 232
    if-eqz v5, :cond_11

    const/4 v9, 0x1

    :goto_12
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 232
    :cond_11
    const/4 v9, 0x0

    goto :goto_12

    .line 237
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v5    # "_result":Z
    :sswitch_e
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 239
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_12

    .line 240
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 245
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_13
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->isAdminRemovable(Landroid/content/ComponentName;)Z

    move-result v5

    .line 246
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 247
    if-eqz v5, :cond_13

    const/4 v9, 0x1

    :goto_14
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 248
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 243
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v5    # "_result":Z
    :cond_12
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_13

    .line 247
    .restart local v5    # "_result":Z
    :cond_13
    const/4 v9, 0x0

    goto :goto_14

    .line 252
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v5    # "_result":Z
    :sswitch_f
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_14

    .line 255
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 261
    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 262
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->isAdminRemovableInternal(Landroid/content/ComponentName;I)Z

    move-result v5

    .line 263
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 264
    if-eqz v5, :cond_15

    const/4 v9, 0x1

    :goto_16
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 265
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 258
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    .end local v5    # "_result":Z
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_15

    .line 264
    .restart local v2    # "_arg1":I
    .restart local v5    # "_result":Z
    :cond_15
    const/4 v9, 0x0

    goto :goto_16

    .line 269
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    .end local v5    # "_result":Z
    :sswitch_10
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 271
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->selfUpdateAdmin(Ljava/lang/String;)I

    move-result v5

    .line 273
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 274
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 279
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v5    # "_result":I
    :sswitch_11
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_16

    const/4 v0, 0x1

    .line 282
    .local v0, "_arg0":Z
    :goto_17
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->setB2BMode(Z)I

    move-result v5

    .line 283
    .restart local v5    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 284
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 281
    .end local v0    # "_arg0":Z
    .end local v5    # "_result":I
    :cond_16
    const/4 v0, 0x0

    goto :goto_17

    .line 289
    :sswitch_12
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_17

    .line 292
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 298
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 299
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforceActiveAdminPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;

    move-result-object v5

    .line 300
    .local v5, "_result":Landroid/app/enterprise/ContextInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 301
    if-eqz v5, :cond_18

    .line 302
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/ContextInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 308
    :goto_19
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 295
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_17
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 306
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_18
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_19

    .line 312
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 314
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_19

    .line 315
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 321
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 322
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforcePermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;

    move-result-object v5

    .line 323
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 324
    if-eqz v5, :cond_1a

    .line 325
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 326
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/ContextInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 331
    :goto_1b
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 318
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_19
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1a

    .line 329
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1a
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1b

    .line 335
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1b

    .line 338
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 344
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 345
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforceContainerOwnerShipPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;

    move-result-object v5

    .line 346
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 347
    if-eqz v5, :cond_1c

    .line 348
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 349
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/ContextInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 354
    :goto_1d
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 341
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 352
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1c
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1d

    .line 358
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1d

    .line 361
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 367
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 368
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforceOwnerOnlyAndActiveAdminPermission(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;

    move-result-object v5

    .line 369
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 370
    if-eqz v5, :cond_1e

    .line 371
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 372
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/ContextInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 377
    :goto_1f
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 364
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 375
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1e
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1f

    .line 381
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_1f

    .line 384
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 390
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 391
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enforceOwnerOnlyPermissionByContext(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/ContextInfo;

    move-result-object v5

    .line 392
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 393
    if-eqz v5, :cond_20

    .line 394
    const/4 v9, 0x1

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 395
    const/4 v9, 0x1

    invoke-virtual {v5, p3, v9}, Landroid/app/enterprise/ContextInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 400
    :goto_21
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 387
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 398
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :cond_20
    const/4 v9, 0x0

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_21

    .line 404
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 406
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_21

    .line 407
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 412
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_22
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->setActiveAdminSilent(Landroid/content/ComponentName;)V

    .line 413
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 414
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 410
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :cond_21
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_22

    .line 418
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_18
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 420
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 421
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getActiveAdminsInfo(I)Ljava/util/List;

    move-result-object v7

    .line 422
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/EnterpriseDeviceAdminInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 423
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 424
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 428
    .end local v0    # "_arg0":I
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/EnterpriseDeviceAdminInfo;>;"
    :sswitch_19
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_22

    .line 431
    sget-object v9, Landroid/app/admin/ProxyDeviceAdminInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 437
    .local v0, "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    :goto_23
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 439
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_23

    .line 440
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    .line 446
    .local v3, "_arg2":Landroid/content/ComponentName;
    :goto_24
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 447
    .local v4, "_arg3":I
    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->addProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V

    .line 448
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 449
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 434
    .end local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Landroid/content/ComponentName;
    .end local v4    # "_arg3":I
    :cond_22
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    goto :goto_23

    .line 443
    .restart local v2    # "_arg1":I
    :cond_23
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/content/ComponentName;
    goto :goto_24

    .line 453
    .end local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Landroid/content/ComponentName;
    :sswitch_1a
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_24

    .line 456
    sget-object v9, Landroid/app/admin/ProxyDeviceAdminInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/ProxyDeviceAdminInfo;

    .line 462
    .restart local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    :goto_25
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 464
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_25

    .line 465
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    .line 471
    .restart local v3    # "_arg2":Landroid/content/ComponentName;
    :goto_26
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 472
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->updateProxyAdmin(Landroid/app/admin/ProxyDeviceAdminInfo;ILandroid/content/ComponentName;I)V

    .line 473
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 474
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 459
    .end local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Landroid/content/ComponentName;
    .end local v4    # "_arg3":I
    :cond_24
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    goto :goto_25

    .line 468
    .restart local v2    # "_arg1":I
    :cond_25
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/content/ComponentName;
    goto :goto_26

    .line 478
    .end local v0    # "_arg0":Landroid/app/admin/ProxyDeviceAdminInfo;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Landroid/content/ComponentName;
    :sswitch_1b
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 480
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 481
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getProxyAdmins(I)Ljava/util/List;

    move-result-object v6

    .line 482
    .local v6, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/admin/ProxyDeviceAdminInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 483
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 484
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 488
    .end local v0    # "_arg0":I
    .end local v6    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/admin/ProxyDeviceAdminInfo;>;"
    :sswitch_1c
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 491
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->removeProxyAdmin(I)V

    .line 492
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 493
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 497
    .end local v0    # "_arg0":I
    :sswitch_1d
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 499
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_26

    .line 500
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 506
    .local v0, "_arg0":Landroid/content/ComponentName;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_27

    const/4 v2, 0x1

    .line 508
    .local v2, "_arg1":Z
    :goto_28
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 509
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->activateAdminForUser(Landroid/content/ComponentName;ZI)V

    .line 510
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 511
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 503
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":I
    :cond_26
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_27

    .line 506
    :cond_27
    const/4 v2, 0x0

    goto :goto_28

    .line 515
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_1e
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 517
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_28

    .line 518
    sget-object v9, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 524
    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 525
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->deactivateAdminForUser(Landroid/content/ComponentName;I)V

    .line 526
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 527
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 521
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    .end local v2    # "_arg1":I
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/content/ComponentName;
    goto :goto_29

    .line 531
    .end local v0    # "_arg0":Landroid/content/ComponentName;
    :sswitch_1f
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 533
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 534
    .local v1, "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v1}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->activateDevicePermissions(Ljava/util/List;)Z

    move-result v5

    .line 535
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 536
    if-eqz v5, :cond_29

    const/4 v9, 0x1

    :goto_2a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 537
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 536
    :cond_29
    const/4 v9, 0x0

    goto :goto_2a

    .line 541
    .end local v1    # "_arg0":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v5    # "_result":Z
    :sswitch_20
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 543
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 545
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2a

    const/4 v2, 0x1

    .line 546
    .local v2, "_arg1":Z
    :goto_2b
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->migrateEnterpriseContainer(IZ)Z

    move-result v5

    .line 547
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 548
    if-eqz v5, :cond_2b

    const/4 v9, 0x1

    :goto_2c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 549
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 545
    .end local v2    # "_arg1":Z
    .end local v5    # "_result":Z
    :cond_2a
    const/4 v2, 0x0

    goto :goto_2b

    .line 548
    .restart local v2    # "_arg1":Z
    .restart local v5    # "_result":Z
    :cond_2b
    const/4 v9, 0x0

    goto :goto_2c

    .line 553
    .end local v0    # "_arg0":I
    .end local v2    # "_arg1":Z
    .end local v5    # "_result":Z
    :sswitch_21
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 555
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_2c

    const/4 v0, 0x1

    .line 556
    .local v0, "_arg0":Z
    :goto_2d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->configureContainerAdminForMigration(Z)Z

    move-result v5

    .line 557
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 558
    if-eqz v5, :cond_2d

    const/4 v9, 0x1

    :goto_2e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 559
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 555
    .end local v0    # "_arg0":Z
    .end local v5    # "_result":Z
    :cond_2c
    const/4 v0, 0x0

    goto :goto_2d

    .line 558
    .restart local v0    # "_arg0":Z
    .restart local v5    # "_result":Z
    :cond_2d
    const/4 v9, 0x0

    goto :goto_2e

    .line 563
    .end local v0    # "_arg0":Z
    .end local v5    # "_result":Z
    :sswitch_22
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 565
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 566
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->migrateApplicationDisablePolicy(I)Z

    move-result v5

    .line 567
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 568
    if-eqz v5, :cond_2e

    const/4 v9, 0x1

    :goto_2f
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 569
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 568
    :cond_2e
    const/4 v9, 0x0

    goto :goto_2f

    .line 573
    .end local v0    # "_arg0":I
    .end local v5    # "_result":Z
    :sswitch_23
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 574
    invoke-virtual {p0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->isMigrationStateNOK()Z

    move-result v5

    .line 575
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 576
    if-eqz v5, :cond_2f

    const/4 v9, 0x1

    :goto_30
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 577
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 576
    :cond_2f
    const/4 v9, 0x0

    goto :goto_30

    .line 581
    .end local v5    # "_result":Z
    :sswitch_24
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 583
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 585
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 586
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->packageHasActiveAdminsAsUser(Ljava/lang/String;I)Z

    move-result v5

    .line 587
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 588
    if-eqz v5, :cond_30

    const/4 v9, 0x1

    :goto_31
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 589
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 588
    :cond_30
    const/4 v9, 0x0

    goto :goto_31

    .line 593
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v5    # "_result":Z
    :sswitch_25
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 595
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_31

    .line 596
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 602
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 604
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 606
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 607
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v0, v2, v3, v4}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->enableConstrainedState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v5

    .line 608
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 609
    if-eqz v5, :cond_32

    const/4 v9, 0x1

    :goto_33
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 610
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 599
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    .end local v5    # "_result":Z
    :cond_31
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 609
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v4    # "_arg3":I
    .restart local v5    # "_result":Z
    :cond_32
    const/4 v9, 0x0

    goto :goto_33

    .line 614
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":I
    .end local v5    # "_result":Z
    :sswitch_26
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 616
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_33

    .line 617
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 622
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->disableConstrainedState(Landroid/app/enterprise/ContextInfo;)Z

    move-result v5

    .line 623
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 624
    if-eqz v5, :cond_34

    const/4 v9, 0x1

    :goto_35
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 625
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 620
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Z
    :cond_33
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 624
    .restart local v5    # "_result":Z
    :cond_34
    const/4 v9, 0x0

    goto :goto_35

    .line 629
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Z
    :sswitch_27
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 632
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->isRestrictedByConstrainedState(I)Z

    move-result v5

    .line 633
    .restart local v5    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 634
    if-eqz v5, :cond_35

    const/4 v9, 0x1

    :goto_36
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 635
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 634
    :cond_35
    const/4 v9, 0x0

    goto :goto_36

    .line 639
    .end local v0    # "_arg0":I
    .end local v5    # "_result":Z
    :sswitch_28
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 640
    invoke-virtual {p0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getConstrainedState()I

    move-result v5

    .line 641
    .local v5, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 642
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 643
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 647
    .end local v5    # "_result":I
    :sswitch_29
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 649
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 650
    .restart local v0    # "_arg0":I
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->sendIntent(I)V

    .line 651
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 652
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 656
    .end local v0    # "_arg0":I
    :sswitch_2a
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 658
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_36

    .line 659
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 665
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 666
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->setMyKnoxAdmin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v5

    .line 667
    .local v5, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 668
    if-eqz v5, :cond_37

    const/4 v9, 0x1

    :goto_38
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 669
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 662
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Z
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 668
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_result":Z
    :cond_37
    const/4 v9, 0x0

    goto :goto_38

    .line 673
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v5    # "_result":Z
    :sswitch_2b
    const-string v9, "android.app.enterprise.IEnterpriseDeviceManager"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 675
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v9

    if-eqz v9, :cond_38

    .line 676
    sget-object v9, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v9, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 681
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IEnterpriseDeviceManager$Stub;->getMyKnoxAdmin(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v5

    .line 682
    .local v5, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 683
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 684
    const/4 v9, 0x1

    goto/16 :goto_0

    .line 679
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/lang/String;
    :cond_38
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
