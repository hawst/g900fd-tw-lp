.class public Landroid/app/enterprise/LocationPolicy;
.super Ljava/lang/Object;
.source "LocationPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/ILocationPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-string v0, "LocationPolicy"

    sput-object v0, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 55
    return-void
.end method

.method private getService()Landroid/app/enterprise/ILocationPolicy;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    if-nez v0, :cond_0

    .line 59
    const-string v0, "location_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/ILocationPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/ILocationPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    .line 62
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    return-object v0
.end method


# virtual methods
.method public getAllLocationProviders()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 250
    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "LocationPolicy.getAllLocationProviders"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 251
    sget-object v2, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v3, ">>> getAllLocationProviders"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 253
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 255
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v3, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/ILocationPolicy;->getAllLocationProviders(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 264
    .end local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :goto_0
    return-object v1

    .line 256
    .restart local v1    # "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 257
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getAllLocationProviders - Failed talking with Location service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getLocationProviderState(Ljava/lang/String;)Z
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;

    .prologue
    .line 174
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.getLocationProviderState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 175
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 177
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ILocationPolicy;->getIndividualLocationProvider(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 186
    :goto_0
    return v1

    .line 178
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "setIndividualLocationPolicy - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 186
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGPSOn()Z
    .locals 3

    .prologue
    .line 426
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.isGPSOn"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 427
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, ">>> isGPSOn"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 428
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 430
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ILocationPolicy;->isGPSOn(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 435
    :goto_0
    return v1

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "isGPSOn - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 435
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGPSStateChangeAllowed()Z
    .locals 3

    .prologue
    .line 347
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.isGPSStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 348
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, ">>> isGPSStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 351
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/ILocationPolicy;->isGPSStateChangeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 356
    :goto_0
    return v1

    .line 352
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "isGPSStateChangeAllowed - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 356
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isLocationProviderBlocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "SProvider"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 201
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/ILocationPolicy;->isLocationProviderBlocked(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 206
    :goto_0
    return v1

    .line 202
    :catch_0
    move-exception v0

    .line 203
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "isLocationPolicyEnabled - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setGPSStateChangeAllowed(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 322
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.setGPSStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 323
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, ">>> setGPSStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 326
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ILocationPolicy;->setGPSStateChangeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 331
    :goto_0
    return v1

    .line 327
    :catch_0
    move-exception v0

    .line 328
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "setGPSStateChangeAllowed - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 331
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLocationProviderState(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "provider"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 114
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.setLocationProviderState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 115
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 117
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/ILocationPolicy;->setIndividualLocationProvider(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 126
    :goto_0
    return v1

    .line 118
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "setLocationProviderState - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 126
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startGPS(Z)Z
    .locals 3
    .param p1, "start"    # Z

    .prologue
    .line 399
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LocationPolicy.startGPS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 400
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, ">>> startGPS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    invoke-direct {p0}, Landroid/app/enterprise/LocationPolicy;->getService()Landroid/app/enterprise/ILocationPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 403
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/LocationPolicy;->mService:Landroid/app/enterprise/ILocationPolicy;

    iget-object v2, p0, Landroid/app/enterprise/LocationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/ILocationPolicy;->startGPS(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 408
    :goto_0
    return v1

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LocationPolicy;->TAG:Ljava/lang/String;

    const-string v2, "startGPS - Failed talking with Location service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 408
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
