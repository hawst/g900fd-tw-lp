.class public Landroid/app/enterprise/ExchangeAccount;
.super Ljava/lang/Object;
.source "ExchangeAccount.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/ExchangeAccount;",
            ">;"
        }
    .end annotation
.end field

.field public static final SET_SMIME_CERTIFICATE_ALL:I = 0x1

.field public static final SET_SMIME_CERTIFICATE_BY_ECRYPTION:I = 0x2

.field public static final SET_SMIME_CERTIFICATE_BY_SIGNING:I = 0x3


# instance fields
.field public mAcceptAllCertificates:Z

.field public mCertificate_data:[B

.field public mCertificate_password:Ljava/lang/String;

.field public mDisplayName:Ljava/lang/String;

.field public mEasDomain:Ljava/lang/String;

.field public mEasUser:Ljava/lang/String;

.field public mEmailAddress:Ljava/lang/String;

.field public mEmailNotificationVibrateAlways:Z

.field public mEmailNotificationVibrateWhenSilent:Z

.field public mIsDefault:Z

.field public mIsNotify:Z

.field public mOffPeak:I

.field public mPeakDays:I

.field public mPeakEndtime:I

.field public mPeakStarttime:I

.field public mPeriodCalendar:I

.field public mProtocolVersion:Ljava/lang/String;

.field public mRetrivalSize:I

.field public mRoamingSchedule:I

.field public mSMIMECertificareMode:I

.field public mSMIMECertificatePassWord:Ljava/lang/String;

.field public mSMIMECertificatePath:Ljava/lang/String;

.field public mSenderName:Ljava/lang/String;

.field public mServerAddress:Ljava/lang/String;

.field public mServerPassword:Ljava/lang/String;

.field public mServerPathPrefix:Ljava/lang/String;

.field public mSignature:Ljava/lang/String;

.field public mSyncCalendar:I

.field public mSyncContacts:I

.field public mSyncInterval:I

.field public mSyncLookback:I

.field public mUseSSL:Z

.field public mUseTLS:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 348
    new-instance v0, Landroid/app/enterprise/ExchangeAccount$1;

    invoke-direct {v0}, Landroid/app/enterprise/ExchangeAccount$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/ExchangeAccount;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 254
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    invoke-virtual {p0, p1}, Landroid/app/enterprise/ExchangeAccount;->readFromParcel(Landroid/os/Parcel;)V

    .line 256
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "easUser"    # Ljava/lang/String;
    .param p3, "easDomain"    # Ljava/lang/String;
    .param p4, "serverAddress"    # Ljava/lang/String;
    .param p5, "serverPassword"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 368
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mDisplayName:Ljava/lang/String;

    .line 369
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailAddress:Ljava/lang/String;

    .line 370
    iput-object p2, p0, Landroid/app/enterprise/ExchangeAccount;->mEasUser:Ljava/lang/String;

    .line 371
    iput-object p3, p0, Landroid/app/enterprise/ExchangeAccount;->mEasDomain:Ljava/lang/String;

    .line 372
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mSenderName:Ljava/lang/String;

    .line 373
    const-string v0, "2.5"

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mProtocolVersion:Ljava/lang/String;

    .line 374
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mSignature:Ljava/lang/String;

    .line 375
    iput-object p4, p0, Landroid/app/enterprise/ExchangeAccount;->mServerAddress:Ljava/lang/String;

    .line 376
    iput-object p5, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPassword:Ljava/lang/String;

    .line 377
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPathPrefix:Ljava/lang/String;

    .line 378
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_password:Ljava/lang/String;

    .line 379
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_data:[B

    .line 380
    iput v2, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncLookback:I

    .line 381
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncInterval:I

    .line 382
    const/16 v0, 0x1e0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakStarttime:I

    .line 383
    const/16 v0, 0x3fc

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakEndtime:I

    .line 384
    const/16 v0, 0x3e

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakDays:I

    .line 385
    const/4 v0, -0x2

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mOffPeak:I

    .line 386
    iput v3, p0, Landroid/app/enterprise/ExchangeAccount;->mRoamingSchedule:I

    .line 387
    const/4 v0, 0x3

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mRetrivalSize:I

    .line 388
    const/4 v0, 0x4

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeriodCalendar:I

    .line 389
    iput v2, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncContacts:I

    .line 390
    iput v2, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncCalendar:I

    .line 391
    iput-boolean v3, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateAlways:Z

    .line 392
    iput-boolean v3, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateWhenSilent:Z

    .line 393
    iput-boolean v2, p0, Landroid/app/enterprise/ExchangeAccount;->mUseSSL:Z

    .line 394
    iput-boolean v3, p0, Landroid/app/enterprise/ExchangeAccount;->mUseTLS:Z

    .line 395
    iput-boolean v2, p0, Landroid/app/enterprise/ExchangeAccount;->mAcceptAllCertificates:Z

    .line 396
    iput-boolean v3, p0, Landroid/app/enterprise/ExchangeAccount;->mIsDefault:Z

    .line 397
    iput-boolean v2, p0, Landroid/app/enterprise/ExchangeAccount;->mIsNotify:Z

    .line 398
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePath:Ljava/lang/String;

    .line 399
    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePassWord:Ljava/lang/String;

    .line 400
    iput v2, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificareMode:I

    .line 401
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 343
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 301
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mDisplayName:Ljava/lang/String;

    .line 302
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailAddress:Ljava/lang/String;

    .line 303
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEasUser:Ljava/lang/String;

    .line 304
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEasDomain:Ljava/lang/String;

    .line 305
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSenderName:Ljava/lang/String;

    .line 306
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mProtocolVersion:Ljava/lang/String;

    .line 307
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSignature:Ljava/lang/String;

    .line 308
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerAddress:Ljava/lang/String;

    .line 309
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPassword:Ljava/lang/String;

    .line 310
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPathPrefix:Ljava/lang/String;

    .line 311
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_password:Ljava/lang/String;

    .line 313
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_data:[B

    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncLookback:I

    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncInterval:I

    .line 317
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakStarttime:I

    .line 318
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakEndtime:I

    .line 319
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakDays:I

    .line 320
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mOffPeak:I

    .line 321
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mRoamingSchedule:I

    .line 322
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mRetrivalSize:I

    .line 323
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeriodCalendar:I

    .line 324
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncContacts:I

    .line 325
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncCalendar:I

    .line 327
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateAlways:Z

    .line 328
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateWhenSilent:Z

    .line 329
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mUseSSL:Z

    .line 330
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mUseTLS:Z

    .line 331
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mAcceptAllCertificates:Z

    .line 332
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mIsDefault:Z

    .line 333
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    :goto_6
    iput-boolean v1, p0, Landroid/app/enterprise/ExchangeAccount;->mIsNotify:Z

    .line 334
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificareMode:I

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePath:Ljava/lang/String;

    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePassWord:Ljava/lang/String;

    .line 337
    return-void

    :cond_0
    move v0, v2

    .line 327
    goto :goto_0

    :cond_1
    move v0, v2

    .line 328
    goto :goto_1

    :cond_2
    move v0, v2

    .line 329
    goto :goto_2

    :cond_3
    move v0, v2

    .line 330
    goto :goto_3

    :cond_4
    move v0, v2

    .line 331
    goto :goto_4

    :cond_5
    move v0, v2

    .line 332
    goto :goto_5

    :cond_6
    move v1, v2

    .line 333
    goto :goto_6
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 263
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 264
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEasUser:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEasDomain:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 266
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSenderName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 267
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mProtocolVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 268
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSignature:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 269
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 270
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mServerPathPrefix:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mCertificate_data:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 274
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncLookback:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncInterval:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakStarttime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakEndtime:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 278
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeakDays:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 279
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mOffPeak:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mRoamingSchedule:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 281
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mRetrivalSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 282
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mPeriodCalendar:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 283
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncContacts:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 284
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSyncCalendar:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateAlways:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 286
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mEmailNotificationVibrateWhenSilent:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 287
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mUseSSL:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 288
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mUseTLS:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 289
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mAcceptAllCertificates:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mIsDefault:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 291
    iget-boolean v0, p0, Landroid/app/enterprise/ExchangeAccount;->mIsNotify:Z

    if-eqz v0, :cond_6

    :goto_6
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 292
    iget v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificareMode:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 293
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccount;->mSMIMECertificatePassWord:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 295
    return-void

    :cond_0
    move v0, v2

    .line 285
    goto :goto_0

    :cond_1
    move v0, v2

    .line 286
    goto :goto_1

    :cond_2
    move v0, v2

    .line 287
    goto :goto_2

    :cond_3
    move v0, v2

    .line 288
    goto :goto_3

    :cond_4
    move v0, v2

    .line 289
    goto :goto_4

    :cond_5
    move v0, v2

    .line 290
    goto :goto_5

    :cond_6
    move v1, v2

    .line 291
    goto :goto_6
.end method
