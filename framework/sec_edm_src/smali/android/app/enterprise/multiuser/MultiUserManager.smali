.class public Landroid/app/enterprise/multiuser/MultiUserManager;
.super Ljava/lang/Object;
.source "MultiUserManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MultiUserManager"

.field private static gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

.field private static isMuSupportInfoReady:Z

.field private static isMuSupported:Z

.field private static mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->mSync:Ljava/lang/Object;

    .line 443
    sput-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupported:Z

    .line 445
    sput-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupportInfoReady:Z

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-string v0, "multi_user_manager_service"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/multiuser/IMultiUserManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    .line 67
    iput-object p2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContext:Landroid/content/Context;

    .line 68
    iput-object p1, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 69
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->getMuSupportInfo()Z

    .line 70
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/multiuser/MultiUserManager;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    new-instance v0, Landroid/app/enterprise/multiuser/MultiUserManager;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/multiuser/MultiUserManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method private enforceMultiUserSupport()V
    .locals 2

    .prologue
    .line 437
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->getMuSupportInfo()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This device does not support multiple users"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_0
    return-void
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/multiuser/MultiUserManager;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    sget-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 91
    :try_start_0
    sget-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    if-nez v0, :cond_0

    .line 92
    new-instance v0, Landroid/app/enterprise/multiuser/MultiUserManager;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/multiuser/MultiUserManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    .line 94
    :cond_0
    sget-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    monitor-exit v1

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/multiuser/MultiUserManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 78
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 79
    :try_start_0
    sget-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    if-nez v1, :cond_0

    .line 80
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 81
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/multiuser/MultiUserManager;

    invoke-direct {v1, v0, p0}, Landroid/app/enterprise/multiuser/MultiUserManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    .line 83
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->gMultiUserMgrInst:Landroid/app/enterprise/multiuser/MultiUserManager;

    monitor-exit v2

    return-object v1

    .line 84
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private declared-synchronized getMuSupportInfo()Z
    .locals 4

    .prologue
    .line 455
    monitor-enter p0

    :try_start_0
    sget-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupportInfoReady:Z

    if-nez v1, :cond_0

    .line 457
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    if-eqz v1, :cond_0

    .line 459
    :try_start_1
    sget-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/multiuser/IMultiUserManager;->multipleUsersSupported(Landroid/app/enterprise/ContextInfo;)Z

    move-result v1

    sput-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupported:Z

    .line 460
    const/4 v1, 0x1

    sput-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupportInfoReady:Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 468
    :cond_0
    :goto_0
    :try_start_2
    sget-boolean v1, Landroid/app/enterprise/multiuser/MultiUserManager;->isMuSupported:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return v1

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_3
    const-string v1, "MultiUserManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed talking with multi user service. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 455
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private static getService()Landroid/app/enterprise/multiuser/IMultiUserManager;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    if-nez v0, :cond_0

    .line 108
    const-string v0, "multi_user_manager_service"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/multiuser/IMultiUserManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    .line 111
    :cond_0
    sget-object v0, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    return-object v0
.end method


# virtual methods
.method public allowMultipleUsers(Z)Z
    .locals 6
    .param p1, "allow"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 127
    iget-object v4, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "MultiUserManager.allowMultipleUsers"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 128
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 130
    :try_start_0
    sget-object v4, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v5, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->allowMultipleUsers(Landroid/app/enterprise/ContextInfo;Z)I

    move-result v1

    .line 131
    .local v1, "ret":I
    const/4 v4, -0x1

    if-ne v4, v1, :cond_2

    .line 132
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Not Supported in this device"

    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    .end local v1    # "ret":I
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed talking with multi user service"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    move v2, v3

    .line 139
    :cond_1
    :goto_0
    return v2

    .line 134
    .restart local v1    # "ret":I
    :cond_2
    if-eq v1, v2, :cond_1

    move v2, v3

    goto :goto_0
.end method

.method public allowUserCreation(Z)Z
    .locals 5
    .param p1, "allow"    # Z

    .prologue
    .line 301
    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MultiUserManager.allowUserCreation"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 303
    const/4 v1, 0x0

    .line 305
    .local v1, "ret":Z
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 307
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 308
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->allowUserCreation(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 314
    :cond_0
    :goto_0
    return v1

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public allowUserRemoval(Z)Z
    .locals 5
    .param p1, "allow"    # Z

    .prologue
    .line 376
    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MultiUserManager.allowUserRemoval"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 378
    const/4 v1, 0x0

    .line 380
    .local v1, "ret":Z
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 382
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 383
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->allowUserRemoval(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 389
    :cond_0
    :goto_0
    return v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public createUser(Ljava/lang/String;)I
    .locals 5
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 210
    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MultiUserManager.createUser"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 212
    const/4 v1, -0x1

    .line 214
    .local v1, "userId":I
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 216
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 217
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->createUser(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 223
    :cond_0
    :goto_0
    return v1

    .line 218
    :catch_0
    move-exception v0

    .line 219
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getUsers()[I
    .locals 5

    .prologue
    .line 270
    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MultiUserManager.getUsers"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 271
    const/4 v1, 0x0

    .line 273
    .local v1, "users":[I
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 275
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 276
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/multiuser/IMultiUserManager;->getUsers(Landroid/app/enterprise/ContextInfo;)[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 282
    :cond_0
    :goto_0
    return-object v1

    .line 277
    :catch_0
    move-exception v0

    .line 278
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isUserCreationAllowed()Z
    .locals 1

    .prologue
    .line 330
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/multiuser/MultiUserManager;->isUserCreationAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUserCreationAllowed(Z)Z
    .locals 5
    .param p1, "showMsg"    # Z

    .prologue
    .line 347
    const/4 v1, 0x1

    .line 349
    .local v1, "ret":Z
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 351
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 352
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->isUserCreationAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 358
    :cond_0
    :goto_0
    return v1

    .line 353
    :catch_0
    move-exception v0

    .line 354
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isUserRemovalAllowed()Z
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/multiuser/MultiUserManager;->isUserRemovalAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUserRemovalAllowed(Z)Z
    .locals 5
    .param p1, "showMsg"    # Z

    .prologue
    .line 422
    const/4 v1, 0x1

    .line 424
    .local v1, "ret":Z
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 426
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 427
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->isUserRemovalAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 433
    :cond_0
    :goto_0
    return v1

    .line 428
    :catch_0
    move-exception v0

    .line 429
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public multipleUsersAllowed()Z
    .locals 2

    .prologue
    .line 152
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not Supported in this device"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/multiuser/MultiUserManager;->multipleUsersAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public multipleUsersAllowed(Z)Z
    .locals 6
    .param p1, "showMsg"    # Z

    .prologue
    const/4 v2, 0x1

    .line 162
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 164
    :try_start_0
    sget-object v3, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v4, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->multipleUsersAllowed(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 165
    .local v1, "ret":I
    if-ne v1, v2, :cond_1

    .line 170
    .end local v1    # "ret":I
    :cond_0
    :goto_0
    return v2

    .line 165
    .restart local v1    # "ret":I
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 166
    .end local v1    # "ret":I
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/os/RemoteException;
    const-string v3, "MultiUserManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed talking with multi user service"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public multipleUsersSupported()Z
    .locals 4

    .prologue
    .line 184
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 186
    :try_start_0
    sget-object v1, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/multiuser/IMultiUserManager;->multipleUsersSupported(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 191
    :goto_0
    return v1

    .line 187
    :catch_0
    move-exception v0

    .line 188
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MultiUserManager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed talking with multi user service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeUser(I)Z
    .locals 5
    .param p1, "userId"    # I

    .prologue
    .line 242
    iget-object v2, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MultiUserManager.removeUser"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 244
    const/4 v1, 0x0

    .line 246
    .local v1, "ret":Z
    invoke-static {}, Landroid/app/enterprise/multiuser/MultiUserManager;->getService()Landroid/app/enterprise/multiuser/IMultiUserManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 248
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/multiuser/MultiUserManager;->enforceMultiUserSupport()V

    .line 249
    sget-object v2, Landroid/app/enterprise/multiuser/MultiUserManager;->mService:Landroid/app/enterprise/multiuser/IMultiUserManager;

    iget-object v3, p0, Landroid/app/enterprise/multiuser/MultiUserManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/multiuser/IMultiUserManager;->removeUser(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 255
    :cond_0
    :goto_0
    return v1

    .line 250
    :catch_0
    move-exception v0

    .line 251
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "MultiUserManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed talking with multi user service. "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
