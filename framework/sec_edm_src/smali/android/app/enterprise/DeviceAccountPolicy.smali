.class public Landroid/app/enterprise/DeviceAccountPolicy;
.super Ljava/lang/Object;
.source "DeviceAccountPolicy.java"


# static fields
.field private static final ALL_ACCOUNTS:Ljava/lang/String; = ".*"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IDeviceAccountPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "DeviceAccountPolicy"

    sput-object v0, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 78
    return-void
.end method

.method private getService()Landroid/app/enterprise/IDeviceAccountPolicy;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    if-nez v0, :cond_0

    .line 64
    const-string v0, "device_account_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IDeviceAccountPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    .line 67
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    return-object v0
.end method


# virtual methods
.method public addAccountsToAdditionBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 748
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.addAccountsToAdditionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 749
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 751
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->addAccountsToAdditionBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 756
    :goto_0
    return v1

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 756
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAccountsToAdditionWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 965
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.addAccountsToAdditionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 966
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 968
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->addAccountsToAdditionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 973
    :goto_0
    return v1

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 973
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAccountsToAdditionWhiteList(Ljava/lang/String;Ljava/util/List;Z)Z
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p3, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 1031
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceAccountPolicy.addAccountsToAdditionWhiteList(String type, List<String> accounts, boolean defaultBlackList)"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1032
    const/4 v1, 0x1

    .line 1033
    .local v1, "ret":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1034
    .local v0, "allList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, ".*"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1036
    if-eqz p3, :cond_0

    .line 1037
    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/DeviceAccountPolicy;->addAccountsToAdditionBlackList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1038
    sget-object v2, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to update wildCard in Blacklist, Wildcard may be already present!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1039
    const/4 v1, 0x0

    .line 1043
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/app/enterprise/DeviceAccountPolicy;->addAccountsToAdditionWhiteList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public addAccountsToRemovalBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 139
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.addAccountsToRemovalBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 140
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 142
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->addAccountsToRemovalBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 147
    :goto_0
    return v1

    .line 143
    :catch_0
    move-exception v0

    .line 144
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 147
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAccountsToRemovalWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 356
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.addAccountsToRemovalWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 357
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 359
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->addAccountsToRemovalWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 364
    :goto_0
    return v1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 364
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addAccountsToRemovalWhiteList(Ljava/lang/String;Ljava/util/List;Z)Z
    .locals 4
    .param p1, "type"    # Ljava/lang/String;
    .param p3, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 422
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceAccountPolicy.addAccountsToRemovalWhiteList(String type, List<String> accounts, boolean defaultBlackList)"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 423
    const/4 v1, 0x1

    .line 424
    .local v1, "ret":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 425
    .local v0, "allList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, ".*"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    if-eqz p3, :cond_0

    .line 428
    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/DeviceAccountPolicy;->addAccountsToRemovalBlackList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 429
    sget-object v2, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to update wildCard in Blacklist, Wildcard may be already present!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    const/4 v1, 0x0

    .line 434
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/app/enterprise/DeviceAccountPolicy;->addAccountsToRemovalWhiteList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromAdditionBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 899
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.clearAccountsFromAdditionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 900
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 902
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->clearAccountsFromAdditionBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 907
    :goto_0
    return v1

    .line 903
    :catch_0
    move-exception v0

    .line 904
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 907
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromAdditionList(Ljava/lang/String;)Z
    .locals 4
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1235
    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceAccountPolicy.clearAccountsFromAdditionList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1237
    invoke-virtual {p0, p1}, Landroid/app/enterprise/DeviceAccountPolicy;->clearAccountsFromAdditionWhiteList(Ljava/lang/String;)Z

    move-result v1

    .line 1238
    .local v1, "retAdditionWhite":Z
    invoke-virtual {p0, p1}, Landroid/app/enterprise/DeviceAccountPolicy;->clearAccountsFromAdditionBlackList(Ljava/lang/String;)Z

    move-result v0

    .line 1239
    .local v0, "retAdditionBlack":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromAdditionWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 1186
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.clearAccountsFromAdditionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1187
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1189
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->clearAccountsFromAdditionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1194
    :goto_0
    return v1

    .line 1190
    :catch_0
    move-exception v0

    .line 1191
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1194
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromRemovalBlackList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 290
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.clearAccountsFromRemovalBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 291
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->clearAccountsFromRemovalBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 298
    :goto_0
    return v1

    .line 294
    :catch_0
    move-exception v0

    .line 295
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 298
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromRemovalList(Ljava/lang/String;)Z
    .locals 4
    .param p1, "tpye"    # Ljava/lang/String;

    .prologue
    .line 626
    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DeviceAccountPolicy.clearAccountsFromRemovalList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 628
    invoke-virtual {p0, p1}, Landroid/app/enterprise/DeviceAccountPolicy;->clearAccountsFromRemovalWhiteList(Ljava/lang/String;)Z

    move-result v1

    .line 629
    .local v1, "retRemovalWhite":Z
    invoke-virtual {p0, p1}, Landroid/app/enterprise/DeviceAccountPolicy;->clearAccountsFromRemovalBlackList(Ljava/lang/String;)Z

    move-result v0

    .line 630
    .local v0, "retRemovalBlack":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearAccountsFromRemovalWhiteList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 577
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.clearAccountsFromRemovalWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 578
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 580
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->clearAccountsFromRemovalWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 585
    :goto_0
    return v1

    .line 581
    :catch_0
    move-exception v0

    .line 582
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 585
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountsFromAdditionBlackLists(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AccountControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 850
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.getAccountsFromAdditionBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 851
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 853
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->getAccountsFromAdditionBlackLists(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 858
    :goto_0
    return-object v1

    .line 854
    :catch_0
    move-exception v0

    .line 855
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 858
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAccountsFromAdditionWhiteLists(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AccountControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1137
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.getAccountsFromAdditionWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1138
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1140
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->getAccountsFromAdditionWhiteLists(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1145
    :goto_0
    return-object v1

    .line 1141
    :catch_0
    move-exception v0

    .line 1142
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1145
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAccountsFromRemovalBlackLists(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AccountControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.getAccountsFromRemovalBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 242
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 244
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->getAccountsFromRemovalBlackLists(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 249
    :goto_0
    return-object v1

    .line 245
    :catch_0
    move-exception v0

    .line 246
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 249
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getAccountsFromRemovalWhiteLists(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AccountControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 528
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.getAccountsFromRemovalWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 529
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 531
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDeviceAccountPolicy;->getAccountsFromRemovalWhiteLists(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 536
    :goto_0
    return-object v1

    .line 532
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 536
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getSupportedAccountTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 646
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 648
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IDeviceAccountPolicy;->getSupportedAccountTypes()Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 653
    :goto_0
    return-object v1

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 653
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public isAccountAdditionAllowed(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "showMsg"    # Z

    .prologue
    .line 1256
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1258
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IDeviceAccountPolicy;->isAccountAdditionAllowed(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1263
    :goto_0
    return v1

    .line 1259
    :catch_0
    move-exception v0

    .line 1260
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1263
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAccountRemovalAllowed(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "showMsg"    # Z

    .prologue
    .line 670
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 672
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/IDeviceAccountPolicy;->isAccountRemovalAllowed(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 677
    :goto_0
    return v1

    .line 673
    :catch_0
    move-exception v0

    .line 674
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 677
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAccountRemovalAllowedAsUser(Ljava/lang/String;Ljava/lang/String;ZI)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "showMsg"    # Z
    .param p4, "userId"    # I

    .prologue
    .line 682
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 684
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/app/enterprise/IDeviceAccountPolicy;->isAccountRemovalAllowedAsUser(Ljava/lang/String;Ljava/lang/String;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 689
    :goto_0
    return v1

    .line 685
    :catch_0
    move-exception v0

    .line 686
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 689
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeAccountsFromAdditionBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 802
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.removeAccountsFromAdditionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 803
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 805
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->removeAccountsFromAdditionBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 810
    :goto_0
    return v1

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 810
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAccountsFromAdditionWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1089
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.removeAccountsFromAdditionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1090
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1092
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->removeAccountsFromAdditionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1097
    :goto_0
    return v1

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1097
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAccountsFromRemovalBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 193
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.removeAccountsFromRemovalBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 194
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 196
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->removeAccountsFromRemovalBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 201
    :goto_0
    return v1

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 201
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeAccountsFromRemovalWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 480
    .local p2, "accounts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DeviceAccountPolicy.removeAccountsFromRemovalWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 481
    invoke-direct {p0}, Landroid/app/enterprise/DeviceAccountPolicy;->getService()Landroid/app/enterprise/IDeviceAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 483
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mService:Landroid/app/enterprise/IDeviceAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/DeviceAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IDeviceAccountPolicy;->removeAccountsFromRemovalWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 488
    :goto_0
    return v1

    .line 484
    :catch_0
    move-exception v0

    .line 485
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DeviceAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Device Account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 488
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
