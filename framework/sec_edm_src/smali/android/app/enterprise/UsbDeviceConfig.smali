.class public Landroid/app/enterprise/UsbDeviceConfig;
.super Ljava/lang/Object;
.source "UsbDeviceConfig.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/UsbDeviceConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public productId:I

.field public vendorId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Landroid/app/enterprise/UsbDeviceConfig$1;

    invoke-direct {v0}, Landroid/app/enterprise/UsbDeviceConfig$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/UsbDeviceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "vendorId"    # I
    .param p2, "productId"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput p1, p0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    .line 58
    iput p2, p0, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    .line 59
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    invoke-virtual {p0, p1}, Landroid/app/enterprise/UsbDeviceConfig;->readFromParcel(Landroid/os/Parcel;)V

    .line 64
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/UsbDeviceConfig$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/UsbDeviceConfig$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/app/enterprise/UsbDeviceConfig;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 83
    if-nez p1, :cond_0

    move v0, v1

    .line 97
    .end local p1    # "obj":Ljava/lang/Object;
    :goto_0
    return v0

    .line 86
    .restart local p1    # "obj":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Landroid/app/enterprise/UsbDeviceConfig;

    if-nez v0, :cond_1

    move v0, v1

    .line 87
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 89
    check-cast v0, Landroid/app/enterprise/UsbDeviceConfig;

    iget v0, v0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    if-lez v0, :cond_2

    move-object v0, p1

    check-cast v0, Landroid/app/enterprise/UsbDeviceConfig;

    iget v0, v0, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    if-gtz v0, :cond_3

    :cond_2
    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_3
    iget v2, p0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    move-object v0, p1

    check-cast v0, Landroid/app/enterprise/UsbDeviceConfig;

    iget v0, v0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    if-ne v2, v0, :cond_4

    iget v0, p0, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    check-cast p1, Landroid/app/enterprise/UsbDeviceConfig;

    .end local p1    # "obj":Ljava/lang/Object;
    iget v2, p1, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    if-ne v0, v2, :cond_4

    .line 95
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    .line 78
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 70
    iget v0, p0, Landroid/app/enterprise/UsbDeviceConfig;->vendorId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    iget v0, p0, Landroid/app/enterprise/UsbDeviceConfig;->productId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 72
    return-void
.end method
