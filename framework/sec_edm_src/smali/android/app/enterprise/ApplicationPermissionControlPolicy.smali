.class public Landroid/app/enterprise/ApplicationPermissionControlPolicy;
.super Ljava/lang/Object;
.source "ApplicationPermissionControlPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "ApplicationPermissionControlPolicy"

    sput-object v0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 67
    return-void
.end method

.method private getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    if-nez v0, :cond_0

    .line 72
    const-string v0, "apppermission_control_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApplicationPermissionControlPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    .line 75
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    return-object v0
.end method


# virtual methods
.method public addPackagesToPermissionBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "pkgNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.addPackagesToPermissionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 84
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 86
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->addPackagesToPermissionBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 91
    :goto_0
    return v1

    .line 87
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToPermissionWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 147
    .local p2, "pkgNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.addPackagesToPermissionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 148
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 150
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->addPackagesToPermissionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 155
    :goto_0
    return v1

    .line 151
    :catch_0
    move-exception v0

    .line 152
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 155
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addPackagesToPermissionWhiteList(Ljava/lang/String;Ljava/util/List;Z)Z
    .locals 7
    .param p1, "permission"    # Ljava/lang/String;
    .param p3, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .local p2, "pkgNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 181
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPermissionControlPolicy.addPackagesToPermissionWhiteList with defaultBlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 183
    const/4 v2, 0x1

    .line 184
    .local v2, "r1":Z
    if-ne p3, v3, :cond_0

    .line 186
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "ApplicationPermissionControlPolicy.addPackagesToPermissionWhiteList -> Adding Star in BlackList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 187
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v1, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v5, "*"

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    invoke-virtual {p0, p1, v1}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->addPackagesToPermissionBlackList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 191
    const/4 v2, 0x0

    .line 192
    sget-object v5, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v6, "ApplicationPermissionControlPolicy.addPackagesToPermissionWhiteList: failed to add *"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    .end local v1    # "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 198
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v6, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v5, v6, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->addPackagesToPermissionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    .line 203
    :goto_0
    return v3

    :cond_1
    move v3, v4

    .line 198
    goto :goto_0

    .line 199
    :catch_0
    move-exception v0

    .line 200
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application permission control policy"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    move v3, v4

    .line 203
    goto :goto_0
.end method

.method public clearPackagesFromPermissionBlackList()Z
    .locals 3

    .prologue
    .line 131
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.clearPackagesFromPermissionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 132
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 134
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->clearPackagesFromPermissionBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 139
    :goto_0
    return v1

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 139
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearPackagesFromPermissionList(Ljava/lang/String;)Z
    .locals 14
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 217
    iget-object v12, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v13, "ApplicationPermissionControlPolicy.clearPackagesFromPermissionList"

    invoke-static {v12, v13}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getPackagesFromPermissionBlackList()Ljava/util/List;

    move-result-object v5

    .line 220
    .local v5, "myList1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppPermissionControlInfo;>;"
    invoke-virtual {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getPackagesFromPermissionWhiteList()Ljava/util/List;

    move-result-object v6

    .line 222
    .local v6, "myList2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppPermissionControlInfo;>;"
    const/4 v0, 0x0

    .line 223
    .local v0, "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 225
    .local v9, "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x1

    .line 226
    .local v7, "r1":Z
    const/4 v8, 0x1

    .line 232
    .local v8, "r2":Z
    if-eqz v5, :cond_1

    .line 233
    :try_start_0
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    move-object v1, v0

    .end local v0    # "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v1, "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    :try_start_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/AppPermissionControlInfo;

    .line 235
    .local v2, "blackListObj":Landroid/app/enterprise/AppPermissionControlInfo;
    new-instance v0, Ljava/util/ArrayList;

    iget-object v12, v2, Landroid/app/enterprise/AppPermissionControlInfo;->mapEntries:Ljava/util/Map;

    invoke-interface {v12, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Collection;

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 236
    .restart local v0    # "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_2
    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->removePackagesFromPermissionBlackList(Ljava/lang/String;Ljava/util/List;)Z

    .end local v1    # "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    move-result v7

    .line 237
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 238
    const/4 v0, 0x0

    move-object v1, v0

    .line 239
    .restart local v1    # "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .end local v0    # "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v2    # "blackListObj":Landroid/app/enterprise/AppPermissionControlInfo;
    :cond_0
    move-object v0, v1

    .line 241
    .end local v1    # "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    if-eqz v6, :cond_3

    .line 242
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    move-object v10, v9

    .end local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v10, "whiteList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_1
    :try_start_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/app/enterprise/AppPermissionControlInfo;

    .line 244
    .local v11, "whiteListObj":Landroid/app/enterprise/AppPermissionControlInfo;
    new-instance v9, Ljava/util/ArrayList;

    iget-object v12, v11, Landroid/app/enterprise/AppPermissionControlInfo;->mapEntries:Ljava/util/Map;

    invoke-interface {v12, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/Collection;

    invoke-direct {v9, v12}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 245
    .restart local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_4
    invoke-virtual {p0, p1, v9}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->removePackagesFromPermissionWhiteList(Ljava/lang/String;Ljava/util/List;)Z

    .end local v10    # "whiteList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    move-result v8

    .line 246
    invoke-interface {v9}, Ljava/util/List;->clear()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 247
    const/4 v9, 0x0

    move-object v10, v9

    .line 248
    .restart local v10    # "whiteList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_1

    .end local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v11    # "whiteListObj":Landroid/app/enterprise/AppPermissionControlInfo;
    :cond_2
    move-object v9, v10

    .line 250
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v10    # "whiteList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    if-eqz v7, :cond_4

    if-eqz v8, :cond_4

    const/4 v12, 0x1

    .line 254
    :goto_2
    return v12

    .line 250
    :cond_4
    const/4 v12, 0x0

    goto :goto_2

    .line 251
    .restart local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 252
    .local v3, "e":Ljava/lang/Exception;
    :goto_3
    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    .line 254
    const/4 v12, 0x0

    goto :goto_2

    .line 251
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v1    # "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v3

    move-object v0, v1

    .restart local v0    # "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_3

    .end local v0    # "blackList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "blackList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v10    # "whiteList":Ljava/lang/Object;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_2
    move-exception v3

    move-object v9, v10

    .restart local v9    # "whiteList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_3
.end method

.method public clearPackagesFromPermissionWhiteList()Z
    .locals 3

    .prologue
    .line 294
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.clearPackagesFromPermissionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 295
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 297
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->clearPackagesFromPermissionWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 302
    :goto_0
    return v1

    .line 298
    :catch_0
    move-exception v0

    .line 299
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 302
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getApplicationGrantedPermissions(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.getApplicationGrantedPermissions"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 326
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 328
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->getApplicationGrantedPermissions(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 333
    :goto_0
    return-object v1

    .line 329
    :catch_0
    move-exception v0

    .line 330
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 333
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromPermissionBlackList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppPermissionControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 118
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->getPackagesFromPermissionBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 123
    :goto_0
    return-object v1

    .line 119
    :catch_0
    move-exception v0

    .line 120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPackagesFromPermissionWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/AppPermissionControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 281
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->getPackagesFromPermissionWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 286
    :goto_0
    return-object v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 286
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getPermissionBlockedList(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 3
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 310
    .local p2, "PermissionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 312
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->getPermissionBlockedList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 317
    :goto_0
    return-object v1

    .line 313
    :catch_0
    move-exception v0

    .line 314
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 317
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public removePackagesFromPermissionBlackList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 99
    .local p2, "pkgNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.removePackagesFromPermissionBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 100
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 102
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->removePackagesFromPermissionBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 107
    :goto_0
    return v1

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 107
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackagesFromPermissionWhiteList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "permission"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 262
    .local p2, "pkgNameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ApplicationPermissionControlPolicy.removePackagesFromPermissionWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 263
    invoke-direct {p0}, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->getService()Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 265
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mService:Landroid/app/enterprise/IApplicationPermissionControlPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IApplicationPermissionControlPolicy;->removePackagesFromPermissionWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 270
    :goto_0
    return v1

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ApplicationPermissionControlPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application permission control policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 270
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
