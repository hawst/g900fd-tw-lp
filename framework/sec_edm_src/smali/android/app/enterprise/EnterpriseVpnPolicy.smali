.class public Landroid/app/enterprise/EnterpriseVpnPolicy;
.super Ljava/lang/Object;
.source "EnterpriseVpnPolicy.java"


# static fields
.field public static final KEY_USER_ID:Ljava/lang/String; = "user_id"

.field public static final SERVICE_CONNECTED:I = 0x0

.field public static final SERVICE_CONNECTING:I = 0x1

.field public static final SERVICE_FAILED:I = -0x1

.field protected static TAG:Ljava/lang/String; = null

.field public static final VPN_CERT_TYPE_AUTOMATIC:Ljava/lang/String; = "Automatic"

.field public static final VPN_CERT_TYPE_DISABLED:Ljava/lang/String; = "Disabled"

.field public static final VPN_CERT_TYPE_MANUAL:Ljava/lang/String; = "Manual"

.field public static final VPN_TYPE_ANYCONNECT:Ljava/lang/String; = "anyconnect"


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field protected mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/enterprise/EnterpriseResponseData",
            "<*>;"
        }
    .end annotation
.end field

.field protected mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "EnterpriseVpnPolicy"

    sput-object v0, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 147
    return-void
.end method

.method private getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    if-nez v0, :cond_0

    .line 151
    const-string v0, "enterprise_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    .line 154
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    return-object v0
.end method


# virtual methods
.method public getAllEnterpriseVpnConnections()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/EnterpriseVpnConnection;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 385
    const/4 v1, 0x0

    .line 387
    .local v1, "connectionList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/EnterpriseVpnConnection;>;"
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 388
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "EnterpriseVpnPolicy.getAllEnterpriseVpnConnections"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 389
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v4, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "anyconnect"

    invoke-interface {v3, v4, v5}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->getAllEnterpriseVpnConnections(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 390
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 391
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 400
    :cond_0
    :goto_0
    return-object v1

    .line 392
    :cond_1
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 393
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 396
    :catch_0
    move-exception v2

    .line 397
    .local v2, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at EnterpriseVpnpolicy API getAllEnterpriseVpnConnections-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getClientCertificates(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 492
    const/4 v2, 0x0

    .line 494
    .local v2, "getClientCertificatesValue":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 495
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "EnterpriseVpnPolicy.getClientCertificates"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 496
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v4, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->getClientCertificates(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 497
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 498
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v2, v0

    .line 506
    :cond_0
    :goto_0
    return-object v2

    .line 500
    :cond_1
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 501
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 503
    :catch_0
    move-exception v1

    .line 504
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at EnterpriseVpnpolicy API getClientCertificates-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseVpnConnection;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 198
    const/4 v2, 0x0

    .line 200
    .local v2, "getEnterpriseVpnConnectionValue":Landroid/app/enterprise/EnterpriseVpnConnection;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 201
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "EnterpriseVpnPolicy.getEnterpriseVpnConnection"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 202
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v4, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1, p2}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->getEnterpriseVpnConnection(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 203
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 204
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/app/enterprise/EnterpriseVpnConnection;

    move-object v2, v0

    .line 212
    :cond_0
    :goto_0
    return-object v2

    .line 206
    :cond_1
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 207
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :catch_0
    move-exception v1

    .line 210
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed at EnterpriseVpnpolicy API getEnterpriseVpnConnection-Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public installClientCertificate(Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "pkcs12Blob"    # [B
    .param p3, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 449
    const/4 v1, 0x0

    .line 451
    .local v1, "installClientCertificateValue":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 452
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseVpnPolicy.installClientCertificate"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 453
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2, p3}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->installClientCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 455
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 456
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 463
    :cond_0
    :goto_0
    return v1

    .line 457
    :cond_1
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 458
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed at EnterpriseVpnpolicy API installCertificate-Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 326
    const/4 v1, 0x0

    .line 328
    .local v1, "removeEnterpriseVpnConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 329
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseVpnPolicy.removeEnterpriseVpnConnection"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 330
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->removeEnterpriseVpnConnection(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 331
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 332
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 340
    :cond_0
    :goto_0
    return v1

    .line 334
    :cond_1
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 335
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed at EnterpriseVpnpolicy API removeEnterpriseVpnConnection-Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setEnterpriseVpnConnection(Landroid/app/enterprise/EnterpriseVpnConnection;Ljava/lang/String;)Z
    .locals 5
    .param p1, "vpn"    # Landroid/app/enterprise/EnterpriseVpnConnection;
    .param p2, "oldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 269
    const/4 v1, 0x0

    .line 271
    .local v1, "setEnterpriseVpnConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/EnterpriseVpnPolicy;->getService()Landroid/app/enterprise/IEnterpriseVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 272
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EnterpriseVpnPolicy.setEnterpriseVpnConnection"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 273
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mService:Landroid/app/enterprise/IEnterpriseVpnPolicy;

    iget-object v3, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IEnterpriseVpnPolicy;->setEnterpriseVpnConnection(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/EnterpriseVpnConnection;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 274
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 275
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 283
    :cond_0
    :goto_0
    return v1

    .line 277
    :cond_1
    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/app/enterprise/EnterpriseVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 278
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Landroid/app/enterprise/EnterpriseVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed at EnterpriseVpnpolicy API setEnterpriseVpnConnection-Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
