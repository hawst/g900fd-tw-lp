.class public Landroid/app/enterprise/BrowserPolicy;
.super Ljava/lang/Object;
.source "BrowserPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IBrowserPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "BrowserPolicy"

    sput-object v0, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const-string v0, "browser_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IBrowserPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    .line 77
    iput-object p1, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 78
    return-void
.end method

.method private getService()Landroid/app/enterprise/IBrowserPolicy;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    if-nez v0, :cond_0

    .line 64
    const-string v0, "browser_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IBrowserPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    .line 66
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    return-object v0
.end method


# virtual methods
.method public clearHttpProxy()Z
    .locals 3

    .prologue
    .line 505
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BrowserPolicy.clearHttpProxy"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 507
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 509
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBrowserPolicy;->clearHttpProxy(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 515
    :goto_0
    return v1

    .line 510
    :catch_0
    move-exception v0

    .line 511
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 515
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAutoFillSetting()Z
    .locals 1

    .prologue
    .line 288
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Landroid/app/enterprise/BrowserPolicy;->getBrowserSettingStatus(I)Z

    move-result v0

    return v0
.end method

.method public getBrowserSettingStatus(I)Z
    .locals 3
    .param p1, "setting"    # I

    .prologue
    .line 463
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-nez v1, :cond_0

    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 464
    const/4 v1, 0x0

    .line 473
    :goto_0
    return v1

    .line 466
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 468
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBrowserPolicy;->getBrowserSettingStatus(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 469
    :catch_0
    move-exception v0

    .line 470
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 473
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getCookiesSetting()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/enterprise/BrowserPolicy;->getBrowserSettingStatus(I)Z

    move-result v0

    return v0
.end method

.method public getForceFraudWarningSetting()Z
    .locals 1

    .prologue
    .line 362
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/app/enterprise/BrowserPolicy;->getBrowserSettingStatus(I)Z

    move-result v0

    return v0
.end method

.method public getHttpProxy()Ljava/lang/String;
    .locals 3

    .prologue
    .line 529
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 531
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBrowserPolicy;->getHttpProxy(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 537
    :goto_0
    return-object v1

    .line 532
    :catch_0
    move-exception v0

    .line 533
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 537
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getJavaScriptSetting()Z
    .locals 1

    .prologue
    .line 431
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Landroid/app/enterprise/BrowserPolicy;->getBrowserSettingStatus(I)Z

    move-result v0

    return v0
.end method

.method public getPopupsSetting()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/enterprise/BrowserPolicy;->getBrowserSettingStatus(I)Z

    move-result v0

    return v0
.end method

.method public setAutoFillSetting(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 266
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BrowserPolicy.setAutoFillSetting"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 267
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/BrowserPolicy;->setBrowserSettingStatus(ZI)Z

    move-result v0

    return v0
.end method

.method public setBrowserSettingStatus(ZI)Z
    .locals 3
    .param p1, "enable"    # Z
    .param p2, "setting"    # I

    .prologue
    .line 444
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 446
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IBrowserPolicy;->setBrowserSettingStatus(Landroid/app/enterprise/ContextInfo;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 451
    :goto_0
    return v1

    .line 447
    :catch_0
    move-exception v0

    .line 448
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 451
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCookiesSetting(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 194
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BrowserPolicy.setCookiesSetting"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 195
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/BrowserPolicy;->setBrowserSettingStatus(ZI)Z

    move-result v0

    return v0
.end method

.method public setForceFraudWarningSetting(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 340
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BrowserPolicy.setForceFraudWarningSetting"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 341
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/BrowserPolicy;->setBrowserSettingStatus(ZI)Z

    move-result v0

    return v0
.end method

.method public setHttpProxy(Ljava/lang/String;)Z
    .locals 3
    .param p1, "proxySetting"    # Ljava/lang/String;

    .prologue
    .line 484
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BrowserPolicy.setHttpProxy"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 486
    invoke-direct {p0}, Landroid/app/enterprise/BrowserPolicy;->getService()Landroid/app/enterprise/IBrowserPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 488
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BrowserPolicy;->mService:Landroid/app/enterprise/IBrowserPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBrowserPolicy;->setHttpProxy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 494
    :goto_0
    return v1

    .line 489
    :catch_0
    move-exception v0

    .line 490
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BrowserPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with application policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 494
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setJavaScriptSetting(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 409
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BrowserPolicy.setJavaScriptSetting"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 410
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/BrowserPolicy;->setBrowserSettingStatus(ZI)Z

    move-result v0

    return v0
.end method

.method public setPopupsSetting(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 122
    iget-object v0, p0, Landroid/app/enterprise/BrowserPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "BrowserPolicy.setPopupsSetting"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 123
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/BrowserPolicy;->setBrowserSettingStatus(ZI)Z

    move-result v0

    return v0
.end method
