.class public Landroid/app/enterprise/NetworkStats;
.super Ljava/lang/Object;
.source "NetworkStats.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/NetworkStats;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mobileRxBytes:J

.field public mobileTxBytes:J

.field public uid:I

.field public wifiRxBytes:J

.field public wifiTxBytes:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 79
    new-instance v0, Landroid/app/enterprise/NetworkStats$1;

    invoke-direct {v0}, Landroid/app/enterprise/NetworkStats$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/NetworkStats;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Landroid/app/enterprise/NetworkStats;->uid:I

    .line 58
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->wifiRxBytes:J

    .line 64
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->wifiTxBytes:J

    .line 70
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->mobileRxBytes:J

    .line 76
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->mobileTxBytes:J

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide/16 v2, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Landroid/app/enterprise/NetworkStats;->uid:I

    .line 58
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->wifiRxBytes:J

    .line 64
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->wifiTxBytes:J

    .line 70
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->mobileRxBytes:J

    .line 76
    iput-wide v2, p0, Landroid/app/enterprise/NetworkStats;->mobileTxBytes:J

    .line 98
    invoke-virtual {p0, p1}, Landroid/app/enterprise/NetworkStats;->readFromParcel(Landroid/os/Parcel;)V

    .line 99
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/NetworkStats;->uid:I

    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/NetworkStats;->wifiRxBytes:J

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/NetworkStats;->wifiTxBytes:J

    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/NetworkStats;->mobileRxBytes:J

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/NetworkStats;->mobileTxBytes:J

    .line 128
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 108
    iget v0, p0, Landroid/app/enterprise/NetworkStats;->uid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget-wide v0, p0, Landroid/app/enterprise/NetworkStats;->wifiRxBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 110
    iget-wide v0, p0, Landroid/app/enterprise/NetworkStats;->wifiTxBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 111
    iget-wide v0, p0, Landroid/app/enterprise/NetworkStats;->mobileRxBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 112
    iget-wide v0, p0, Landroid/app/enterprise/NetworkStats;->mobileTxBytes:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 114
    return-void
.end method
