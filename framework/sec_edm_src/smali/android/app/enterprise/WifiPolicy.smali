.class public Landroid/app/enterprise/WifiPolicy;
.super Ljava/lang/Object;
.source "WifiPolicy.java"


# static fields
.field public static final ACTION_ENABLE_NETWORK:Ljava/lang/String; = "edm.intent.action.enable"

.field public static final ACTION_LOCK_KEYSTORE:Ljava/lang/String; = "edm.intent.action.lock"

.field public static final CERTIFICATE_SECURITY_LEVEL_HIGH:I = 0x1

.field public static final CERTIFICATE_SECURITY_LEVEL_LOW:I = 0x0

.field public static final ENGINE_ID_SECPKCS11:Ljava/lang/String; = "secpkcs11"

.field public static final SECURITY_LEVEL_EAP_FAST:I = 0x4

.field public static final SECURITY_LEVEL_EAP_LEAP:I = 0x3

.field public static final SECURITY_LEVEL_EAP_PEAP:I = 0x5

.field public static final SECURITY_LEVEL_EAP_TLS:I = 0x7

.field public static final SECURITY_LEVEL_EAP_TTLS:I = 0x6

.field public static final SECURITY_LEVEL_OPEN:I = 0x0

.field public static final SECURITY_LEVEL_WEP:I = 0x1

.field public static final SECURITY_LEVEL_WPA:I = 0x2

.field public static final SECURITY_TYPE_OPEN:Ljava/lang/String; = "Open"

.field public static final SECURITY_TYPE_WPA_PSK:Ljava/lang/String; = "WPA_PSK"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IWifiPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "WifiPolicy"

    sput-object v0, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput-object p1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 148
    return-void
.end method

.method public static getLinkSecurity(Landroid/net/wifi/WifiConfiguration;)I
    .locals 9
    .param p0, "config"    # Landroid/net/wifi/WifiConfiguration;

    .prologue
    const/4 v8, 0x7

    const/4 v7, 0x3

    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v3, 0x1

    .line 2318
    const/4 v1, 0x0

    .line 2319
    .local v1, "sec":I
    if-eqz p0, :cond_1

    .line 2320
    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v3}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2323
    :cond_0
    const/4 v1, 0x2

    .line 2346
    :cond_1
    :goto_0
    return v1

    .line 2324
    :cond_2
    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v7}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    invoke-virtual {v4, v8}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->allowedKeyManagement:Ljava/util/BitSet;

    const/16 v5, 0x9

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2328
    :cond_3
    iget-object v2, p0, Landroid/net/wifi/WifiConfiguration;->enterpriseConfig:Landroid/net/wifi/WifiEnterpriseConfig;

    invoke-virtual {v2}, Landroid/net/wifi/WifiEnterpriseConfig;->getEapMethod()I

    move-result v0

    .line 2329
    .local v0, "eap":I
    if-nez v0, :cond_4

    .line 2330
    const/4 v1, 0x5

    goto :goto_0

    .line 2331
    :cond_4
    if-ne v0, v6, :cond_5

    .line 2332
    const/4 v1, 0x6

    goto :goto_0

    .line 2333
    :cond_5
    const/16 v2, 0x8

    if-ne v0, v2, :cond_6

    .line 2334
    const/4 v1, 0x3

    goto :goto_0

    .line 2335
    :cond_6
    if-ne v0, v8, :cond_7

    .line 2336
    const/4 v1, 0x4

    goto :goto_0

    .line 2338
    :cond_7
    const/4 v1, 0x7

    goto :goto_0

    .line 2341
    .end local v0    # "eap":I
    :cond_8
    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v4, v4, v2

    if-nez v4, :cond_9

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v4, v4, v3

    if-nez v4, :cond_9

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v4, v4, v6

    if-nez v4, :cond_9

    iget-object v4, p0, Landroid/net/wifi/WifiConfiguration;->wepKeys:[Ljava/lang/String;

    aget-object v4, v4, v7

    if-eqz v4, :cond_a

    :cond_9
    move v1, v3

    :goto_1
    goto :goto_0

    :cond_a
    move v1, v2

    goto :goto_1
.end method

.method private getService()Landroid/app/enterprise/IWifiPolicy;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    if-nez v0, :cond_0

    .line 152
    const-string v0, "wifi_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IWifiPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IWifiPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    .line 155
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    return-object v0
.end method


# virtual methods
.method public activateWifiSsidRestriction(Z)Z
    .locals 3
    .param p1, "activate"    # Z

    .prologue
    .line 3226
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.activateWifiSsidRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3227
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3229
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->activateWifiSsidRestriction(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3234
    :goto_0
    return v1

    .line 3230
    :catch_0
    move-exception v0

    .line 3231
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with wifi policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3234
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBlockedNetwork(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1410
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.addBlockedNetwork"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1411
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1413
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->addBlockedNetwork(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1420
    :goto_0
    return v1

    .line 1414
    :catch_0
    move-exception v0

    .line 1415
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "addBlockedNetwork - Failed talking with Wifi service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1420
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addUrlForNetworkProxyBypass(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;

    .prologue
    .line 3451
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.addUrlForNetworkProxyBypass"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3452
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3454
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->addUrlForNetworkProxyBypass(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3459
    :goto_0
    return v1

    .line 3455
    :catch_0
    move-exception v0

    .line 3456
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with addUrlForNetworkProxyBypass"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3459
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addWifiSsidsToBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2683
    .local p1, "ssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.addWifiSsidsToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2684
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2686
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->addWifiSsidToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2691
    :goto_0
    return v1

    .line 2687
    :catch_0
    move-exception v0

    .line 2688
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2691
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addWifiSsidsToWhiteList(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "ssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 2907
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "WifiPolicy.addWifiSsidsToWhiteList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2908
    if-nez p1, :cond_1

    .line 2919
    :cond_0
    :goto_0
    return v1

    .line 2912
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2914
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v3, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IWifiPolicy;->addWifiSsidToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 2915
    :catch_0
    move-exception v0

    .line 2916
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with Wifi policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public addWifiSsidsToWhiteList(Ljava/util/List;Z)Z
    .locals 4
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 2979
    .local p1, "ssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "WifiPolicy.addWifiSsidsToWhiteList(ssid, defaultBlackList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2980
    const/4 v1, 0x1

    .line 2981
    .local v1, "ret":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2982
    .local v0, "allList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "*"

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2984
    if-eqz p2, :cond_0

    .line 2985
    invoke-virtual {p0, v0}, Landroid/app/enterprise/WifiPolicy;->addWifiSsidsToBlackList(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2986
    sget-object v2, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to update wildCard in Blacklist, Wildcard may be already present!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2987
    const/4 v1, 0x0

    .line 2991
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/enterprise/WifiPolicy;->addWifiSsidsToWhiteList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public allowOpenWifiAp(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2565
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.allowOpenWifiAp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2566
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2568
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->allowOpenWifiAp(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2573
    :goto_0
    return v1

    .line 2569
    :catch_0
    move-exception v0

    .line 2570
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2573
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowWifiApSettingUserModification(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2487
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.allowWifiApSettingUserModification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2488
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2490
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->allowWifiApSettingUserModification(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2495
    :goto_0
    return v1

    .line 2491
    :catch_0
    move-exception v0

    .line 2492
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2495
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearUrlsFromNetworkProxyBypassList(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 3481
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.clearUrlsFromNetworkProxyBypassList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3482
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3484
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->clearUrlsFromNetworkProxyBypassList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3489
    :goto_0
    return v1

    .line 3485
    :catch_0
    move-exception v0

    .line 3486
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with clearUrlsFromNetworkProxyBypassList"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3489
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearWifiSsidsFromBlackList()Z
    .locals 3

    .prologue
    .line 2793
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.clearWifiSsidsFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2794
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2796
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->clearWifiSsidBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2801
    :goto_0
    return v1

    .line 2797
    :catch_0
    move-exception v0

    .line 2798
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2801
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearWifiSsidsFromList()Z
    .locals 4

    .prologue
    .line 3193
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "WifiPolicy.clearWifiSsidsFromList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3195
    invoke-virtual {p0}, Landroid/app/enterprise/WifiPolicy;->clearWifiSsidsFromWhiteList()Z

    move-result v1

    .line 3196
    .local v1, "retSsidsWhite":Z
    invoke-virtual {p0}, Landroid/app/enterprise/WifiPolicy;->clearWifiSsidsFromBlackList()Z

    move-result v0

    .line 3197
    .local v0, "retSsidsBlack":Z
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearWifiSsidsFromWhiteList()Z
    .locals 3

    .prologue
    .line 3095
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.clearWifiSsidsFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3096
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3098
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->clearWifiSsidWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3103
    :goto_0
    return v1

    .line 3099
    :catch_0
    move-exception v0

    .line 3100
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with wifi policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3103
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public edmAddOrUpdate(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    .locals 3
    .param p1, "config"    # Landroid/net/wifi/WifiConfiguration;
    .param p2, "netSSID"    # Ljava/lang/String;

    .prologue
    .line 2308
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2309
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->edmAddOrUpdate(Landroid/net/wifi/WifiConfiguration;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2314
    :cond_0
    :goto_0
    return-void

    .line 2311
    :catch_0
    move-exception v0

    .line 2312
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API edmAddOrUpdate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getAllowUserPolicyChanges()Z
    .locals 3

    .prologue
    .line 2027
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2029
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getAllowUserPolicyChanges(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2034
    :goto_0
    return v1

    .line 2030
    :catch_0
    move-exception v0

    .line 2031
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getAllowUserPolicyChanges"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2034
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAllowUserProfiles(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 1920
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/WifiPolicy;->getAllowUserProfilesInternal(ZI)Z

    move-result v0

    return v0
.end method

.method public getAllowUserProfilesInternal(ZI)Z
    .locals 3
    .param p1, "showMsg"    # Z
    .param p2, "userId"    # I

    .prologue
    .line 1927
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getAllowUserProfilesInternal"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1928
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1930
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->getAllowUserProfiles(Landroid/app/enterprise/ContextInfo;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1935
    :goto_0
    return v1

    .line 1931
    :catch_0
    move-exception v0

    .line 1932
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getAllowUserProfiles"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1935
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAutomaticConnectionToWifi()Z
    .locals 3

    .prologue
    .line 1976
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1978
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getAutomaticConnectionToWifi(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1983
    :goto_0
    return v1

    .line 1979
    :catch_0
    move-exception v0

    .line 1980
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getAutomaticConnectionToWifi"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1983
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBlockedNetworks()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1462
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getBlockedNetworks"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1463
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1465
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getBlockedNetworks(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1474
    :goto_0
    return-object v1

    .line 1466
    :catch_0
    move-exception v0

    .line 1467
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "getBlockedNetworks - Failed talking with Wifi service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1474
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getDHCPEnabled()Z
    .locals 3

    .prologue
    .line 1537
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDHCPEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1538
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1540
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDHCPEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1545
    :goto_0
    return v1

    .line 1541
    :catch_0
    move-exception v0

    .line 1542
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "- Failed talking with setDHCPEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1545
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultGateway()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1645
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDefaultGateway"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1646
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1648
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDefaultGateway(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1653
    :goto_0
    return-object v1

    .line 1649
    :catch_0
    move-exception v0

    .line 1650
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultGateway"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1653
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultIp()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1591
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDefaultIp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1592
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1594
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDefaultIp(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1599
    :goto_0
    return-object v1

    .line 1595
    :catch_0
    move-exception v0

    .line 1596
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultIp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1599
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultPrimaryDNS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1700
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDefaultPrimaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1701
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1703
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDefaultPrimaryDNS(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1708
    :goto_0
    return-object v1

    .line 1704
    :catch_0
    move-exception v0

    .line 1705
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getDefaultPrimaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1708
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultSecondaryDNS()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1754
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDefaultSecondaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1755
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1757
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDefaultSecondaryDNS(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1762
    :goto_0
    return-object v1

    .line 1758
    :catch_0
    move-exception v0

    .line 1759
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getDefaultSecondaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1762
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDefaultSubnetMask()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1808
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getDefaultSubnetMask"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1809
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1811
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getDefaultSubnetMask(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1816
    :goto_0
    return-object v1

    .line 1812
    :catch_0
    move-exception v0

    .line 1813
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getDefaultSubnetMask"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1816
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMinimumRequiredSecurity()I
    .locals 3

    .prologue
    .line 2146
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2148
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getMinimumRequiredSecurity(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2153
    :goto_0
    return v1

    .line 2149
    :catch_0
    move-exception v0

    .line 2150
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getSupportedSecurity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2153
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkAnonymousIdValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 995
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkAnonymousIdValue"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 997
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 998
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkAnonymousIdValue(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1004
    :goto_0
    return-object v1

    .line 1000
    :catch_0
    move-exception v0

    .line 1001
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkAnonymousIdValue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1004
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkCaCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 869
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkCaCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 871
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 872
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkCaCertification(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 877
    :goto_0
    return-object v1

    .line 874
    :catch_0
    move-exception v0

    .line 875
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkCaCertification"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 877
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkClientCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 890
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkClientCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 892
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 893
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkClientCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 898
    :goto_0
    return-object v1

    .line 895
    :catch_0
    move-exception v0

    .line 896
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkClientCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 898
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkDHCPEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1044
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkDHCPEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1045
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1047
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkDHCPEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1052
    :goto_0
    return v1

    .line 1048
    :catch_0
    move-exception v0

    .line 1049
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "- Failed talking with getNetworkDHCPEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1052
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkEAPType(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 827
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkEAPType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 829
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 830
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkEAPType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 835
    :goto_0
    return-object v1

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkEAPType"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 835
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkGateway(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1160
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkGateway"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1161
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1163
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkGateway(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1168
    :goto_0
    return-object v1

    .line 1164
    :catch_0
    move-exception v0

    .line 1165
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkGateway"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1168
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkIdentityValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 974
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkIdentityValue"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 976
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 977
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkIdentityValue(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 982
    :goto_0
    return-object v1

    .line 979
    :catch_0
    move-exception v0

    .line 980
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Wi-Fi policy API getNetworkIdentityValue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 982
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkIp(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1102
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkIp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1103
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1105
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkIp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1110
    :goto_0
    return-object v1

    .line 1106
    :catch_0
    move-exception v0

    .line 1107
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkIp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1110
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkKeystoreEngineId(Ljava/lang/String;)I
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 3513
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkKeystoreEngineId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3514
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3516
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkKeystoreEngineId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3521
    :goto_0
    return v1

    .line 3517
    :catch_0
    move-exception v0

    .line 3518
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkKeystoreEngineId"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getNetworkLinkSecurity(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 290
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkLinkSecurity"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 292
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkLinkSecurity(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 298
    :goto_0
    return-object v1

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkLinkSecurity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 298
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkPSK(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 913
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 914
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkPSK(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 919
    :goto_0
    return-object v1

    .line 916
    :catch_0
    move-exception v0

    .line 917
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkPrivateKey"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 919
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 953
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 955
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 956
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 961
    :goto_0
    return-object v1

    .line 958
    :catch_0
    move-exception v0

    .line 959
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkPassword"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 961
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkPhase2(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 848
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkPhase2"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 850
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 851
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkPhase2(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 856
    :goto_0
    return-object v1

    .line 853
    :catch_0
    move-exception v0

    .line 854
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkPhase2"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 856
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkPrimaryDNS(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1218
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkPrimaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1219
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1221
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkPrimaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1226
    :goto_0
    return-object v1

    .line 1222
    :catch_0
    move-exception v0

    .line 1223
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkPrimaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1226
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkPrivateKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 932
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 934
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 935
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkPrivateKey(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 940
    :goto_0
    return-object v1

    .line 937
    :catch_0
    move-exception v0

    .line 938
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkPrivateKey"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 940
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkProxyEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 3376
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkProxyEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3377
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3379
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkProxyEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3384
    :goto_0
    return v1

    .line 3380
    :catch_0
    move-exception v0

    .line 3381
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkProxyEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3384
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkProxyHostName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 3406
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkProxyHostName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3407
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3409
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkProxyHostName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3414
    :goto_0
    return-object v1

    .line 3410
    :catch_0
    move-exception v0

    .line 3411
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkProxyHostName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3414
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkProxyPort(Ljava/lang/String;)I
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 3436
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkProxyPort"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3437
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3439
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkProxyPort(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3444
    :goto_0
    return v1

    .line 3440
    :catch_0
    move-exception v0

    .line 3441
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkProxyPort"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3444
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkSSIDList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkSSIDList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 205
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getNetworkSSIDList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 211
    :goto_0
    return-object v1

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkSSID"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getNetworkSecondaryDNS(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1276
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkSecondaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1277
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1279
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkSecondaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1284
    :goto_0
    return-object v1

    .line 1280
    :catch_0
    move-exception v0

    .line 1281
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkSecondaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1284
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkSubnetMask(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1334
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkSubnetMask"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1335
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1337
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkSubnetMask(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1342
    :goto_0
    return-object v1

    .line 1338
    :catch_0
    move-exception v0

    .line 1339
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getNetworkSubnetMask"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1342
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkWEPKey1(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 464
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 465
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkWEPKey1(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 470
    :goto_0
    return-object v1

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkWEPKey1"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 470
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkWEPKey2(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 489
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 490
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkWEPKey2(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 495
    :goto_0
    return-object v1

    .line 492
    :catch_0
    move-exception v0

    .line 493
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkWEPKey2"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 495
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkWEPKey3(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 514
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 515
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkWEPKey3(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 520
    :goto_0
    return-object v1

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkWEPKey3"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 520
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkWEPKey4(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 539
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 540
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkWEPKey4(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 545
    :goto_0
    return-object v1

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkWEPKey4"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 545
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetworkWEPKeyId(Ljava/lang/String;)I
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 806
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getNetworkWEPKeyId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 808
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 809
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getNetworkWEPKeyId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 814
    :goto_0
    return v1

    .line 811
    :catch_0
    move-exception v0

    .line 812
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getNetworkWEPKeyId"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 814
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getPasswordHidden()Z
    .locals 3

    .prologue
    .line 2273
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2275
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getPasswordHidden(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2280
    :goto_0
    return v1

    .line 2276
    :catch_0
    move-exception v0

    .line 2277
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getPasswordHidden"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2280
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPromptCredentialsEnabled()Z
    .locals 3

    .prologue
    .line 2077
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2079
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getPromptCredentialsEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2084
    :goto_0
    return v1

    .line 2080
    :catch_0
    move-exception v0

    .line 2081
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getPromptCredentialsEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2084
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getTlsCertificateSecurityLevel()I
    .locals 3

    .prologue
    .line 2203
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getTlsCertificateSecurityLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2204
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2206
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getTlsCertificateSecurityLevel(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2211
    :goto_0
    return v1

    .line 2207
    :catch_0
    move-exception v0

    .line 2208
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getTlsCertificateSecurityLevel"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2211
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUrlsFromNetworkProxyBypassList(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3496
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getUrlsFromNetworkProxyBypassList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3497
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3499
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getUrlsFromNetworkProxyBypassList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3504
    :goto_0
    return-object v1

    .line 3500
    :catch_0
    move-exception v0

    .line 3501
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getUrlsFromNetworkProxyBypassList"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3504
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWifiApSetting()Landroid/net/wifi/WifiConfiguration;
    .locals 3

    .prologue
    .line 2452
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getWifiApSetting"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2453
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2455
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getWifiApSetting(Landroid/app/enterprise/ContextInfo;)Landroid/net/wifi/WifiConfiguration;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2460
    :goto_0
    return-object v1

    .line 2456
    :catch_0
    move-exception v0

    .line 2457
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2460
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWifiProfile(Ljava/lang/String;)Landroid/app/enterprise/WifiAdminProfile;
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1379
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getWifiProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1381
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1382
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getWifiProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/WifiAdminProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1387
    :goto_0
    return-object v1

    .line 1384
    :catch_0
    move-exception v0

    .line 1385
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API getWifiProfile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1387
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWifiSsidRestrictionList(I)Ljava/util/List;
    .locals 3
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3344
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getWifiSsidRestrictionList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3345
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3347
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->getWifiSsidRestrictionList(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3352
    :goto_0
    return-object v1

    .line 3348
    :catch_0
    move-exception v0

    .line 3349
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with getWifiSsidRestrictionList"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3352
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getWifiSsidsFromBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/WifiControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2839
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getWifiSsidsFromBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2840
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2842
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getAllWifiSsidBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2847
    :goto_0
    return-object v1

    .line 2843
    :catch_0
    move-exception v0

    .line 2844
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2847
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getWifiSsidsFromWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/WifiControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3142
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.getWifiSsidsFromWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3143
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3145
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->getAllWifiSsidWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 3150
    :goto_0
    return-object v1

    .line 3146
    :catch_0
    move-exception v0

    .line 3147
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with wifi policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3150
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isEnterpriseNetwork(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 2292
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2293
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IWifiPolicy;->isEnterpriseNetwork(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2298
    :goto_0
    return v1

    .line 2295
    :catch_0
    move-exception v0

    .line 2296
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API isEnterpriseNetwork"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2298
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNetworkBlocked(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "showMsg"    # Z

    .prologue
    .line 1490
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1492
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->isNetworkBlocked(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1499
    :goto_0
    return v1

    .line 1493
    :catch_0
    move-exception v0

    .line 1494
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "isNetworkBlocked - Failed talking with Wifi service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1499
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isNetworkSecure(IZ)Z
    .locals 3
    .param p1, "netId"    # I
    .param p2, "showMsg"    # Z

    .prologue
    .line 2226
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2228
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->isNetworkSecure(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2233
    :goto_0
    return v1

    .line 2229
    :catch_0
    move-exception v0

    .line 2230
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with isNetworkSecure"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2233
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isOpenWifiApAllowed()Z
    .locals 3

    .prologue
    .line 2608
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2610
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->isOpenWifiApAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2615
    :goto_0
    return v1

    .line 2611
    :catch_0
    move-exception v0

    .line 2612
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2615
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWifiAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1869
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1871
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->isWifiAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1876
    :goto_0
    return v1

    .line 1872
    :catch_0
    move-exception v0

    .line 1873
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with isWifiEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1876
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWifiApSettingUserModificationAllowed()Z
    .locals 3

    .prologue
    .line 2531
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2533
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->isWifiApSettingUserModificationAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2538
    :goto_0
    return v1

    .line 2534
    :catch_0
    move-exception v0

    .line 2535
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2538
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isWifiSsidRestrictionActive()Z
    .locals 3

    .prologue
    .line 3259
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.isWifiSsidRestrictionActive"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3260
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3262
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->isWifiSsidRestrictionActive(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3267
    :goto_0
    return v1

    .line 3263
    :catch_0
    move-exception v0

    .line 3264
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with wifi policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3267
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWifiStateChangeAllowed()Z
    .locals 3

    .prologue
    .line 3327
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3329
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IWifiPolicy;->isWifiStateChangeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3334
    :goto_0
    return v1

    .line 3330
    :catch_0
    move-exception v0

    .line 3331
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with isWifiEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3334
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeBlockedNetwork(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 1439
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.removeBlockedNetwork"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1440
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1442
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->removeBlockedNetwork(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1451
    :goto_0
    return v1

    .line 1443
    :catch_0
    move-exception v0

    .line 1444
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "removeBlockedNetwork - Failed talking with Wifi service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1451
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeNetworkConfiguration(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.removeNetworkConfiguration"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 235
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 236
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->removeNetworkConfiguration(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 242
    :goto_0
    return v1

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API removeNetworkConfiguration"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 242
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeUrlFromNetworkProxyBypassList(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 3466
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.removeUrlFromNetworkProxyBypassList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3467
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3469
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->removeUrlFromNetworkProxyBypassList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3474
    :goto_0
    return v1

    .line 3470
    :catch_0
    move-exception v0

    .line 3471
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with removeUrlFromNetworkProxyBypassList"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3474
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeWifiSsidsFromBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2742
    .local p1, "ssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.removeWifiSsidsFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2743
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2745
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->removeWifiSsidFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2750
    :goto_0
    return v1

    .line 2746
    :catch_0
    move-exception v0

    .line 2747
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2750
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeWifiSsidsFromWhiteList(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "ssid":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 3040
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "WifiPolicy.removeWifiSsidsFromWhiteList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3041
    if-nez p1, :cond_1

    .line 3052
    :cond_0
    :goto_0
    return v1

    .line 3045
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 3047
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v3, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IWifiPolicy;->removeWifiSsidFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 3048
    :catch_0
    move-exception v0

    .line 3049
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with wifi policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setAllowUserPolicyChanges(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2006
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setAllowUserPolicyChanges"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2007
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2009
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setAllowUserPolicyChanges(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2014
    :goto_0
    return v1

    .line 2010
    :catch_0
    move-exception v0

    .line 2011
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setAllowUserPolicyChanges"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2014
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAllowUserProfiles(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1897
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setAllowUserProfiles"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1898
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1900
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setAllowUserProfiles(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1905
    :goto_0
    return v1

    .line 1901
    :catch_0
    move-exception v0

    .line 1902
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setAllowUserProfiles"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1905
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAutomaticConnectionToWifi(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1955
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setAutomaticConnectionToWifi"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1956
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1958
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setAutomaticConnectionToWifi(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1963
    :goto_0
    return v1

    .line 1959
    :catch_0
    move-exception v0

    .line 1960
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setAutomaticConnectionToWifi"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1963
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDHCPEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1514
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDHCPEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1515
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1517
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDHCPEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1522
    :goto_0
    return v1

    .line 1518
    :catch_0
    move-exception v0

    .line 1519
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "- Failed talking with setDHCPEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1522
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDefaultGateway(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1618
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDefaultGateway"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1619
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1621
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDefaultGateway(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1626
    :goto_0
    return v1

    .line 1622
    :catch_0
    move-exception v0

    .line 1623
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultGateway"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1626
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDefaultIp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1564
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDefaultIp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1565
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1567
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDefaultIp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1572
    :goto_0
    return v1

    .line 1568
    :catch_0
    move-exception v0

    .line 1569
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultIp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1572
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDefaultPrimaryDNS(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1672
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDefaultPrimaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1673
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1675
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDefaultPrimaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1680
    :goto_0
    return v1

    .line 1676
    :catch_0
    move-exception v0

    .line 1677
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultPrimaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1680
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDefaultSecondaryDNS(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1727
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDefaultSecondaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1728
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1730
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDefaultSecondaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1735
    :goto_0
    return v1

    .line 1731
    :catch_0
    move-exception v0

    .line 1732
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultSecondaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1735
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDefaultSubnetMask(Ljava/lang/String;)Z
    .locals 3
    .param p1, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1781
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setDefaultSubnetMask"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1782
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1784
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setDefaultSubnetMask(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1789
    :goto_0
    return v1

    .line 1785
    :catch_0
    move-exception v0

    .line 1786
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setDefaultSubnetMask"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1789
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMinimumRequiredSecurity(I)Z
    .locals 3
    .param p1, "secType"    # I

    .prologue
    .line 2115
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setMinimumRequiredSecurity"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2116
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2118
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setMinimumRequiredSecurity(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2123
    :goto_0
    return v1

    .line 2119
    :catch_0
    move-exception v0

    .line 2120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setMinimumRequiredSecurity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2123
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkAnonymousIdValue(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 662
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkAnonymousIdValue"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 664
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 665
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkAnonymousIdValue(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 671
    :goto_0
    return v1

    .line 667
    :catch_0
    move-exception v0

    .line 668
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkAnonymousIdValue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 671
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkCaCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 755
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkCaCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 757
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 758
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkCaCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 763
    :goto_0
    return v1

    .line 760
    :catch_0
    move-exception v0

    .line 761
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 763
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkClientCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 723
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkClientCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 725
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 726
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkClientCertification(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 733
    :goto_0
    return v1

    .line 728
    :catch_0
    move-exception v0

    .line 729
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkClientCertification"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 733
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkDHCPEnabled(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "enable"    # Z

    .prologue
    .line 1020
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkDHCPEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1021
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1023
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkDHCPEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1028
    :goto_0
    return v1

    .line 1024
    :catch_0
    move-exception v0

    .line 1025
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "- Failed talking with setNetworkDHCPEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1028
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkGateway(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1131
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkGateway"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1132
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1134
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkGateway(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1139
    :goto_0
    return v1

    .line 1135
    :catch_0
    move-exception v0

    .line 1136
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkGateway"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1139
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkIdentityValue(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 630
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkIdentityValue"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 632
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 633
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkIdentityValue(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 638
    :goto_0
    return v1

    .line 635
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkIdentityValue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 638
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkIp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1073
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkIp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1074
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1076
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkIp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1081
    :goto_0
    return v1

    .line 1077
    :catch_0
    move-exception v0

    .line 1078
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkIp"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1081
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkLinkSecurity(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 264
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkLinkSecurity"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 266
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkLinkSecurity(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 272
    :goto_0
    return v1

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkLinkSecurity"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 272
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkPSK(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "psk"    # Ljava/lang/String;

    .prologue
    .line 568
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkPSK"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 570
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 571
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkPSK(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 576
    :goto_0
    return v1

    .line 573
    :catch_0
    move-exception v0

    .line 574
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkPSK"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 576
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 599
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 601
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 602
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 607
    :goto_0
    return v1

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkPassword"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 607
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkPhase2(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 693
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkPhase2"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 695
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 696
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkPhase2(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 701
    :goto_0
    return v1

    .line 698
    :catch_0
    move-exception v0

    .line 699
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkPhase2"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 701
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkPrimaryDNS(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1189
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkPrimaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1190
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1192
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkPrimaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1197
    :goto_0
    return v1

    .line 1193
    :catch_0
    move-exception v0

    .line 1194
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkPrimaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1197
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkPrivateKey(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 785
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkPrivateKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 787
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 788
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkPrivateKey(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 793
    :goto_0
    return v1

    .line 790
    :catch_0
    move-exception v0

    .line 791
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkPrivateKey"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 793
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkProxyEnabled(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 3361
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkProxyEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3362
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3364
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkProxyEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3369
    :goto_0
    return v1

    .line 3365
    :catch_0
    move-exception v0

    .line 3366
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkProxyEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3369
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkProxyHostName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "hostName"    # Ljava/lang/String;

    .prologue
    .line 3391
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkProxyHostName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3392
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3394
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkProxyHostName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3399
    :goto_0
    return v1

    .line 3395
    :catch_0
    move-exception v0

    .line 3396
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkProxyHostName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3399
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkProxyPort(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "port"    # I

    .prologue
    .line 3421
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkProxyPort"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3422
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3424
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkProxyPort(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3429
    :goto_0
    return v1

    .line 3425
    :catch_0
    move-exception v0

    .line 3426
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkProxyPort"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3429
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkSSID(Ljava/lang/String;)Z
    .locals 5
    .param p1, "ssid"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 175
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "WifiPolicy.setNetworkSSID"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 177
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v1

    .line 182
    :cond_1
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 183
    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v3, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IWifiPolicy;->setNetworkSSID(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 185
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at WiFi policy API setNetworkSSID"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setNetworkSecondaryDNS(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1247
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkSecondaryDNS"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1248
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1250
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkSecondaryDNS(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1255
    :goto_0
    return v1

    .line 1251
    :catch_0
    move-exception v0

    .line 1252
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkSecondaryDNS"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1255
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkSubnetMask(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "ipAddr"    # Ljava/lang/String;

    .prologue
    .line 1305
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkSubnetMask"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1306
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1308
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkSubnetMask(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1313
    :goto_0
    return v1

    .line 1309
    :catch_0
    move-exception v0

    .line 1310
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setNetworkSubnetMask"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1313
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkWEPKey1(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 350
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkWEPKey1"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 352
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 353
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkWEPKey1(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 358
    :goto_0
    return v1

    .line 355
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkWEPKey1"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 358
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkWEPKey2(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 379
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkWEPKey2"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 381
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 382
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkWEPKey2(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 387
    :goto_0
    return v1

    .line 384
    :catch_0
    move-exception v0

    .line 385
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkWEPKey2"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 387
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkWEPKey3(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 408
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkWEPKey3"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 410
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 411
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkWEPKey3(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 416
    :goto_0
    return v1

    .line 413
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkWEPKey3"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 416
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkWEPKey4(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 437
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkWEPKey4"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 439
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 440
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkWEPKey4(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 445
    :goto_0
    return v1

    .line 442
    :catch_0
    move-exception v0

    .line 443
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkWEPKey4"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 445
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setNetworkWEPKeyId(Ljava/lang/String;I)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "keyId"    # I

    .prologue
    .line 321
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setNetworkWEPKeyId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 323
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 324
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IWifiPolicy;->setNetworkWEPKeyId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 329
    :goto_0
    return v1

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setNetworkWEPKeyId"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 329
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPasswordHidden(Z)Z
    .locals 3
    .param p1, "passHidden"    # Z

    .prologue
    .line 2253
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setPasswordHidden"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2254
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2256
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setPasswordHidden(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2261
    :goto_0
    return v1

    .line 2257
    :catch_0
    move-exception v0

    .line 2258
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setPasswordHidden"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2261
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPromptCredentialsEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2056
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setPromptCredentialsEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2057
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2059
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setPromptCredentialsEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2064
    :goto_0
    return v1

    .line 2060
    :catch_0
    move-exception v0

    .line 2061
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setPromptCredentialsEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2064
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTlsCertificateSecurityLevel(I)Z
    .locals 3
    .param p1, "level"    # I

    .prologue
    .line 2176
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setTlsCertificateSecurityLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2177
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2179
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setTlsCertificateSecurityLevel(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2188
    :goto_0
    return v1

    .line 2180
    :catch_0
    move-exception v0

    .line 2181
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setTlsCertificateSecurityLevel"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2188
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifi(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1830
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1832
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setWifi(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1837
    :goto_0
    return v1

    .line 1833
    :catch_0
    move-exception v0

    .line 1834
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setWifiEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1837
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifiAllowed(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1849
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1851
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setWifiAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1856
    :goto_0
    return v1

    .line 1852
    :catch_0
    move-exception v0

    .line 1853
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setWifiEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1856
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifiApSetting(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "ssid"    # Ljava/lang/String;
    .param p2, "securityType"    # Ljava/lang/String;
    .param p3, "password"    # Ljava/lang/String;

    .prologue
    .line 2401
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setWifiApSetting"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2402
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2404
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IWifiPolicy;->setWifiApSetting(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2409
    :goto_0
    return v1

    .line 2405
    :catch_0
    move-exception v0

    .line 2406
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Wifi Policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2409
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifiProfile(Landroid/app/enterprise/WifiAdminProfile;)Z
    .locals 3
    .param p1, "profile"    # Landroid/app/enterprise/WifiAdminProfile;

    .prologue
    .line 1357
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setWifiProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1359
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1360
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setWifiProfile(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/WifiAdminProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1365
    :goto_0
    return v1

    .line 1362
    :catch_0
    move-exception v0

    .line 1363
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at WiFi policy API setWifiProfile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1365
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWifiStateChangeAllowed(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 3304
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "WifiPolicy.setWifiStateChangeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 3305
    invoke-direct {p0}, Landroid/app/enterprise/WifiPolicy;->getService()Landroid/app/enterprise/IWifiPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3307
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/WifiPolicy;->mService:Landroid/app/enterprise/IWifiPolicy;

    iget-object v2, p0, Landroid/app/enterprise/WifiPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IWifiPolicy;->setWifiStateChangeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 3312
    :goto_0
    return v1

    .line 3308
    :catch_0
    move-exception v0

    .line 3309
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/WifiPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with setWifiEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 3312
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
