.class public Landroid/app/enterprise/RoamingPolicy;
.super Ljava/lang/Object;
.source "RoamingPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IRoamingPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "RoamingPolicy"

    sput-object v0, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 61
    return-void
.end method

.method private getService()Landroid/app/enterprise/IRoamingPolicy;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    if-nez v0, :cond_0

    .line 65
    const-string v0, "roaming_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRoamingPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    .line 68
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    return-object v0
.end method


# virtual methods
.method public isRoamingDataEnabled()Z
    .locals 3

    .prologue
    .line 213
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 215
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRoamingPolicy;->isRoamingDataEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 220
    :goto_0
    return v1

    .line 216
    :catch_0
    move-exception v0

    .line 217
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 220
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRoamingPushEnabled()Z
    .locals 3

    .prologue
    .line 163
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 165
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRoamingPolicy;->isRoamingPushEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 170
    :goto_0
    return v1

    .line 166
    :catch_0
    move-exception v0

    .line 167
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 170
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRoamingSyncEnabled()Z
    .locals 3

    .prologue
    .line 111
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 113
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRoamingPolicy;->isRoamingSyncEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 118
    :goto_0
    return v1

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 118
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRoamingVoiceCallsEnabled()Z
    .locals 3

    .prologue
    .line 279
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 281
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IRoamingPolicy;->isRoamingVoiceCallsEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 286
    :goto_0
    return v1

    .line 282
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 286
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setRoamingData(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 190
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RoamingPolicy.setRoamingData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 191
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 193
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRoamingPolicy;->setRoamingData(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRoamingPush(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 138
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RoamingPolicy.setRoamingPush"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 139
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 141
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRoamingPolicy;->setRoamingPush(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 142
    :catch_0
    move-exception v0

    .line 143
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRoamingSync(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 89
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RoamingPolicy.setRoamingSync"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 92
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRoamingPolicy;->setRoamingSync(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 94
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRoamingVoiceCalls(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 253
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "RoamingPolicy.setRoamingVoiceCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 254
    invoke-direct {p0}, Landroid/app/enterprise/RoamingPolicy;->getService()Landroid/app/enterprise/IRoamingPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 256
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/RoamingPolicy;->mService:Landroid/app/enterprise/IRoamingPolicy;

    iget-object v2, p0, Landroid/app/enterprise/RoamingPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IRoamingPolicy;->setRoamingVoiceCalls(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v0

    .line 258
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/RoamingPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with roaming policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
