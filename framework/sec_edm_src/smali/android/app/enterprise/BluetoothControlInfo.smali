.class public Landroid/app/enterprise/BluetoothControlInfo;
.super Landroid/app/enterprise/ControlInfo;
.source "BluetoothControlInfo.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/BluetoothControlInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Landroid/app/enterprise/BluetoothControlInfo$1;

    invoke-direct {v0}, Landroid/app/enterprise/BluetoothControlInfo$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/BluetoothControlInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/enterprise/ControlInfo;-><init>()V

    .line 56
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/enterprise/ControlInfo;-><init>()V

    .line 59
    invoke-virtual {p0, p1}, Landroid/app/enterprise/BluetoothControlInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/BluetoothControlInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/BluetoothControlInfo$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/app/enterprise/BluetoothControlInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method
