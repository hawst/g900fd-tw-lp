.class public Landroid/app/enterprise/EnterpriseResponseData;
.super Ljava/lang/Object;
.source "EnterpriseResponseData.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final APINOTSUPPORTED:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/EnterpriseResponseData;",
            ">;"
        }
    .end annotation
.end field

.field public static final ERROR:I = -0x1

.field public static final EXCEPTIONFAILURE:I = 0x3

.field public static final FAILURE:I = 0x1

.field public static final INSTALL_FAILURE:I = 0x6

.field public static final INVALID_ADMIN:I = 0x8

.field public static final INVALID_CONTAINER_ID:I = 0xb

.field public static final INVALID_PARAMETER:I = 0x9

.field public static final INVALID_VENDOR:I = 0x7

.field public static final INVALID_VPN_STATE:I = 0xc

.field public static final NOERROR:I = 0x0

.field public static final NULLPACKAGE:I = 0x4

.field public static final NULLPROFILE:I = 0x2

.field public static final SERVICE_NOT_STARTED:I = 0xa

.field public static final SUCCESS:I = 0x0

.field public static final SYSTEM_UID_FAILURE:I = 0x5


# instance fields
.field public mData:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public mFailureState:I

.field public mStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Landroid/app/enterprise/EnterpriseResponseData$1;

    invoke-direct {v0}, Landroid/app/enterprise/EnterpriseResponseData$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/EnterpriseResponseData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    .line 89
    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    .line 103
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    .line 89
    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    .line 106
    invoke-virtual {p0, p1}, Landroid/app/enterprise/EnterpriseResponseData;->readFromParcel(Landroid/os/Parcel;)V

    .line 107
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/EnterpriseResponseData$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/EnterpriseResponseData$1;

    .prologue
    .line 42
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    invoke-direct {p0, p1}, Landroid/app/enterprise/EnterpriseResponseData;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 131
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mData:Ljava/lang/Object;

    return-object v0
.end method

.method public getFailureState()I
    .locals 1

    .prologue
    .line 127
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    iget v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    return v0
.end method

.method public getStatus()I
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    iget v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 143
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mData:Ljava/lang/Object;

    .line 144
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    .line 145
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    .line 147
    return-void
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    .local p1, "data":Ljava/lang/Object;, "TT;"
    iput-object p1, p0, Landroid/app/enterprise/EnterpriseResponseData;->mData:Ljava/lang/Object;

    .line 111
    return-void
.end method

.method public setStatus(II)V
    .locals 0
    .param p1, "status"    # I
    .param p2, "failureState"    # I

    .prologue
    .line 118
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    iput p1, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    .line 119
    iput p2, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    .line 120
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 135
    .local p0, "this":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<TT;>;"
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mData:Ljava/lang/Object;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 136
    iget v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 137
    iget v0, p0, Landroid/app/enterprise/EnterpriseResponseData;->mFailureState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    return-void
.end method
