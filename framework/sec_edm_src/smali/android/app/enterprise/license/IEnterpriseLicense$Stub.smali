.class public abstract Landroid/app/enterprise/license/IEnterpriseLicense$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseLicense.java"

# interfaces
.implements Landroid/app/enterprise/license/IEnterpriseLicense;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/license/IEnterpriseLicense;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/license/IEnterpriseLicense$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.license.IEnterpriseLicense"

.field static final TRANSACTION_activateKnoxLicense:I = 0x13

.field static final TRANSACTION_activateKnoxLicenseForUMC:I = 0x14

.field static final TRANSACTION_activateLicense:I = 0x7

.field static final TRANSACTION_activateLicenseForUMC:I = 0xa

.field static final TRANSACTION_deActivateKnoxLicense:I = 0x18

.field static final TRANSACTION_deActivateKnoxLicenseForUMC:I = 0x19

.field static final TRANSACTION_deleteApiCallData:I = 0x3

.field static final TRANSACTION_deleteLicense:I = 0xf

.field static final TRANSACTION_deleteLicenseByAdmin:I = 0x10

.field static final TRANSACTION_getAllLicenseInfo:I = 0x5

.field static final TRANSACTION_getApiCallData:I = 0x2

.field static final TRANSACTION_getApiCallDataByAdmin:I = 0x4

.field static final TRANSACTION_getELMLicenseKey:I = 0x15

.field static final TRANSACTION_getELMPermissions:I = 0x1a

.field static final TRANSACTION_getKLMLicenseKey:I = 0x16

.field static final TRANSACTION_getKLMLicenseKeyForDeactivation:I = 0x17

.field static final TRANSACTION_getLicenseInfo:I = 0x6

.field static final TRANSACTION_getRightsObject:I = 0x1

.field static final TRANSACTION_log:I = 0xc

.field static final TRANSACTION_processKnoxLicenseResponse:I = 0x11

.field static final TRANSACTION_processKnoxLicenseResponseForUMC:I = 0x12

.field static final TRANSACTION_processLicenseActivationResponse:I = 0x8

.field static final TRANSACTION_processLicenseActivationResponseForUMC:I = 0xb

.field static final TRANSACTION_processLicenseValidationResult:I = 0x9

.field static final TRANSACTION_resetLicense:I = 0xd

.field static final TRANSACTION_resetLicenseByAdmin:I = 0xe


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/license/IEnterpriseLicense;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/license/IEnterpriseLicense;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/license/IEnterpriseLicense;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/license/IEnterpriseLicense$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 492
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 46
    :sswitch_0
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;

    move-result-object v8

    .line 55
    .local v8, "_result":Landroid/app/enterprise/license/RightsObject;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 56
    if-eqz v8, :cond_0

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Landroid/app/enterprise/license/RightsObject;->writeToParcel(Landroid/os/Parcel;I)V

    .line 63
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1

    .line 67
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Landroid/app/enterprise/license/RightsObject;
    :sswitch_2
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 70
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getApiCallData(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 71
    .local v8, "_result":Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 72
    if-eqz v8, :cond_1

    .line 73
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 79
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 83
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Landroid/os/Bundle;
    :sswitch_3
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 87
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/license/Error;

    .line 95
    .local v3, "_arg2":Landroid/app/enterprise/license/Error;
    :goto_3
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)Z

    move-result v8

    .line 96
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 97
    if-eqz v8, :cond_3

    const/4 v0, 0x1

    :goto_4
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    const/4 v0, 0x1

    goto :goto_0

    .line 93
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v8    # "_result":Z
    :cond_2
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    goto :goto_3

    .line 97
    .restart local v8    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_4

    .line 102
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v8    # "_result":Z
    :sswitch_4
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 104
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 105
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 111
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 112
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getApiCallDataByAdmin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    .line 113
    .local v8, "_result":Landroid/os/Bundle;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 114
    if-eqz v8, :cond_5

    .line 115
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 116
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Landroid/os/Bundle;->writeToParcel(Landroid/os/Parcel;I)V

    .line 121
    :goto_6
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 108
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Landroid/os/Bundle;
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 119
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Landroid/os/Bundle;
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 125
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Landroid/os/Bundle;
    :sswitch_5
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getAllLicenseInfo()[Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v8

    .line 127
    .local v8, "_result":[Landroid/app/enterprise/license/LicenseInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 128
    const/4 v0, 0x1

    invoke-virtual {p3, v8, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 129
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 133
    .end local v8    # "_result":[Landroid/app/enterprise/license/LicenseInfo;
    :sswitch_6
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getLicenseInfo(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;

    move-result-object v8

    .line 137
    .local v8, "_result":Landroid/app/enterprise/license/LicenseInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v8, :cond_6

    .line 139
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Landroid/app/enterprise/license/LicenseInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 145
    :goto_7
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 143
    :cond_6
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_7

    .line 149
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Landroid/app/enterprise/license/LicenseInfo;
    :sswitch_7
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 152
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 158
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 160
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 161
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->activateLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 163
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 155
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_7
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 167
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 171
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 173
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 175
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 177
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 178
    sget-object v0, Landroid/app/enterprise/license/RightsObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/license/RightsObject;

    .line 184
    .local v5, "_arg4":Landroid/app/enterprise/license/RightsObject;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 185
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/enterprise/license/Error;

    .line 191
    .local v6, "_arg5":Landroid/app/enterprise/license/Error;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .local v7, "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 192
    invoke-virtual/range {v0 .. v7}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->processLicenseActivationResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    move-result v8

    .line 193
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v8, :cond_a

    const/4 v0, 0x1

    :goto_b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 195
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 181
    .end local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    .end local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_8
    const/4 v5, 0x0

    .restart local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    goto :goto_9

    .line 188
    :cond_9
    const/4 v6, 0x0

    .restart local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    goto :goto_a

    .line 194
    .restart local v7    # "_arg6":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    goto :goto_b

    .line 199
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    .end local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_9
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 203
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 205
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 206
    sget-object v0, Landroid/app/enterprise/license/RightsObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/license/RightsObject;

    .line 212
    .local v3, "_arg2":Landroid/app/enterprise/license/RightsObject;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 213
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/enterprise/license/Error;

    .line 219
    .local v4, "_arg3":Landroid/app/enterprise/license/Error;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 220
    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    move-result v8

    .line 221
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 222
    if-eqz v8, :cond_d

    const/4 v0, 0x1

    :goto_e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 223
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 209
    .end local v3    # "_arg2":Landroid/app/enterprise/license/RightsObject;
    .end local v4    # "_arg3":Landroid/app/enterprise/license/Error;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_b
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/app/enterprise/license/RightsObject;
    goto :goto_c

    .line 216
    :cond_c
    const/4 v4, 0x0

    .restart local v4    # "_arg3":Landroid/app/enterprise/license/Error;
    goto :goto_d

    .line 222
    .restart local v5    # "_arg4":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    goto :goto_e

    .line 227
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/app/enterprise/license/RightsObject;
    .end local v4    # "_arg3":Landroid/app/enterprise/license/Error;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_a
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_e

    .line 230
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 236
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 238
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 240
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 241
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->activateLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 243
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 233
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    :cond_e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 247
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 251
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 253
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 255
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 257
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 258
    sget-object v0, Landroid/app/enterprise/license/RightsObject;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/license/RightsObject;

    .line 264
    .local v5, "_arg4":Landroid/app/enterprise/license/RightsObject;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 265
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/enterprise/license/Error;

    .line 271
    .restart local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "_arg6":Ljava/lang/String;
    move-object v0, p0

    .line 272
    invoke-virtual/range {v0 .. v7}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->processLicenseActivationResponseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z

    move-result v8

    .line 273
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 274
    if-eqz v8, :cond_11

    const/4 v0, 0x1

    :goto_12
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 275
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 261
    .end local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    .end local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_f
    const/4 v5, 0x0

    .restart local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    goto :goto_10

    .line 268
    :cond_10
    const/4 v6, 0x0

    .restart local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    goto :goto_11

    .line 274
    .restart local v7    # "_arg6":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_11
    const/4 v0, 0x0

    goto :goto_12

    .line 279
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Landroid/app/enterprise/license/RightsObject;
    .end local v6    # "_arg5":Landroid/app/enterprise/license/Error;
    .end local v7    # "_arg6":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_c
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 282
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 288
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 289
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 290
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 291
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 285
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    :cond_12
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 295
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->resetLicense(Ljava/lang/String;)Z

    move-result v8

    .line 299
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 300
    if-eqz v8, :cond_13

    const/4 v0, 0x1

    :goto_14
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 301
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 300
    :cond_13
    const/4 v0, 0x0

    goto :goto_14

    .line 305
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_e
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 307
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 308
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->resetLicenseByAdmin(Ljava/lang/String;)Z

    move-result v8

    .line 309
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 310
    if-eqz v8, :cond_14

    const/4 v0, 0x1

    :goto_15
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 311
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 310
    :cond_14
    const/4 v0, 0x0

    goto :goto_15

    .line 315
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_f
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 317
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 318
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->deleteLicense(Ljava/lang/String;)Z

    move-result v8

    .line 319
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 320
    if-eqz v8, :cond_15

    const/4 v0, 0x1

    :goto_16
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 321
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 320
    :cond_15
    const/4 v0, 0x0

    goto :goto_16

    .line 325
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_10
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 327
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 328
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->deleteLicenseByAdmin(Ljava/lang/String;)Z

    move-result v8

    .line 329
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    if-eqz v8, :cond_16

    const/4 v0, 0x1

    :goto_17
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 331
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 330
    :cond_16
    const/4 v0, 0x0

    goto :goto_17

    .line 335
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_11
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 337
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 339
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 341
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 342
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/license/Error;

    .line 348
    .local v3, "_arg2":Landroid/app/enterprise/license/Error;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 350
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 351
    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->processKnoxLicenseResponse(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z

    move-result v8

    .line 352
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 353
    if-eqz v8, :cond_18

    const/4 v0, 0x1

    :goto_19
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 354
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 345
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v8    # "_result":Z
    :cond_17
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    goto :goto_18

    .line 353
    .restart local v4    # "_arg3":I
    .restart local v5    # "_arg4":I
    .restart local v8    # "_result":Z
    :cond_18
    const/4 v0, 0x0

    goto :goto_19

    .line 358
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v8    # "_result":Z
    :sswitch_12
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 360
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 362
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 364
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 365
    sget-object v0, Landroid/app/enterprise/license/Error;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/license/Error;

    .line 371
    .restart local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 373
    .restart local v4    # "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .restart local v5    # "_arg4":I
    move-object v0, p0

    .line 374
    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->processKnoxLicenseResponseForUMC(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;II)Z

    move-result v8

    .line 375
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 376
    if-eqz v8, :cond_1a

    const/4 v0, 0x1

    :goto_1b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 377
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 368
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v8    # "_result":Z
    :cond_19
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    goto :goto_1a

    .line 376
    .restart local v4    # "_arg3":I
    .restart local v5    # "_arg4":I
    .restart local v8    # "_result":Z
    :cond_1a
    const/4 v0, 0x0

    goto :goto_1b

    .line 381
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/app/enterprise/license/Error;
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":I
    .end local v8    # "_result":Z
    :sswitch_13
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1b

    .line 384
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 390
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 392
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 393
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->activateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 395
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 387
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_1b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 399
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 402
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 408
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 410
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 411
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->activateKnoxLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 412
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 413
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 405
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_1c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    .line 417
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 419
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 420
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 421
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 422
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 423
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 427
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_16
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 430
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getKLMLicenseKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 431
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 432
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 433
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 437
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_17
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 439
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 440
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getKLMLicenseKeyForDeactivation(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 441
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 442
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 443
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 447
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_18
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 450
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 456
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 458
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 459
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->deActivateKnoxLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 461
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 453
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_1d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 465
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 467
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1e

    .line 468
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 474
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 476
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 477
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->deActivateKnoxLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 479
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 471
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    :cond_1e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .line 483
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v0, "android.app.enterprise.license.IEnterpriseLicense"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 486
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->getELMPermissions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v9

    .line 487
    .local v9, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 488
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 489
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
