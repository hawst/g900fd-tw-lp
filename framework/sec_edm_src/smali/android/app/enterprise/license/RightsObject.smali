.class public Landroid/app/enterprise/license/RightsObject;
.super Ljava/lang/Object;
.source "RightsObject.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/license/RightsObject;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private expiryDate:J

.field private instanceId:Ljava/lang/String;

.field private issueDate:J

.field private licenseType:Ljava/lang/String;

.field private permissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private serverUrl:Ljava/lang/String;

.field private state:Ljava/lang/String;

.field private uploadFrequencyTime:J

.field private uploadTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 254
    new-instance v0, Landroid/app/enterprise/license/RightsObject$1;

    invoke-direct {v0}, Landroid/app/enterprise/license/RightsObject$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/license/RightsObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 301
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    invoke-virtual {p0, p1}, Landroid/app/enterprise/license/RightsObject;->readFromParcel(Landroid/os/Parcel;)V

    .line 267
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/license/RightsObject$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/license/RightsObject$1;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/app/enterprise/license/RightsObject;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public getExpiryDate()J
    .locals 2

    .prologue
    .line 133
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->expiryDate:J

    return-wide v0
.end method

.method public getInstanceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->instanceId:Ljava/lang/String;

    return-object v0
.end method

.method public getIssueDate()J
    .locals 2

    .prologue
    .line 125
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->issueDate:J

    return-wide v0
.end method

.method public getLicenseType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->licenseType:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 149
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->permissions:Ljava/util/List;

    return-object v0
.end method

.method public getServerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->serverUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->state:Ljava/lang/String;

    return-object v0
.end method

.method public getUploadFrequencyTime()J
    .locals 2

    .prologue
    .line 157
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadFrequencyTime:J

    return-wide v0
.end method

.method public getUploadTime()J
    .locals 2

    .prologue
    .line 165
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadTime:J

    return-wide v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 284
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/license/RightsObject;->instanceId:Ljava/lang/String;

    .line 285
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/license/RightsObject;->state:Ljava/lang/String;

    .line 286
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->issueDate:J

    .line 287
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->expiryDate:J

    .line 288
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/license/RightsObject;->licenseType:Ljava/lang/String;

    .line 289
    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/license/RightsObject;->permissions:Ljava/util/List;

    .line 290
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadFrequencyTime:J

    .line 291
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadTime:J

    .line 292
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/license/RightsObject;->serverUrl:Ljava/lang/String;

    .line 293
    return-void
.end method

.method public setExpiryDate(J)V
    .locals 1
    .param p1, "expiryDate"    # J

    .prologue
    .line 205
    iput-wide p1, p0, Landroid/app/enterprise/license/RightsObject;->expiryDate:J

    .line 206
    return-void
.end method

.method public setInstanceId(Ljava/lang/String;)V
    .locals 0
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 181
    iput-object p1, p0, Landroid/app/enterprise/license/RightsObject;->instanceId:Ljava/lang/String;

    .line 182
    return-void
.end method

.method public setIssueDate(J)V
    .locals 1
    .param p1, "issueDate"    # J

    .prologue
    .line 197
    iput-wide p1, p0, Landroid/app/enterprise/license/RightsObject;->issueDate:J

    .line 198
    return-void
.end method

.method public setLicenseType(Ljava/lang/String;)V
    .locals 0
    .param p1, "licenseType"    # Ljava/lang/String;

    .prologue
    .line 213
    iput-object p1, p0, Landroid/app/enterprise/license/RightsObject;->licenseType:Ljava/lang/String;

    .line 214
    return-void
.end method

.method public setPermissions(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 221
    .local p1, "permissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Landroid/app/enterprise/license/RightsObject;->permissions:Ljava/util/List;

    .line 222
    return-void
.end method

.method public setServerUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "serverUrl"    # Ljava/lang/String;

    .prologue
    .line 245
    iput-object p1, p0, Landroid/app/enterprise/license/RightsObject;->serverUrl:Ljava/lang/String;

    .line 246
    return-void
.end method

.method public setState(Ljava/lang/String;)V
    .locals 0
    .param p1, "state"    # Ljava/lang/String;

    .prologue
    .line 189
    iput-object p1, p0, Landroid/app/enterprise/license/RightsObject;->state:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public setUploadFrequencyTime(J)V
    .locals 1
    .param p1, "uploadFrequencyTime"    # J

    .prologue
    .line 229
    iput-wide p1, p0, Landroid/app/enterprise/license/RightsObject;->uploadFrequencyTime:J

    .line 230
    return-void
.end method

.method public setUploadTime(J)V
    .locals 1
    .param p1, "uploadTime"    # J

    .prologue
    .line 237
    iput-wide p1, p0, Landroid/app/enterprise/license/RightsObject;->uploadTime:J

    .line 238
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 271
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->instanceId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 272
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->state:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->issueDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 274
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->expiryDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 275
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->licenseType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 276
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->permissions:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 277
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadFrequencyTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 278
    iget-wide v0, p0, Landroid/app/enterprise/license/RightsObject;->uploadTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 279
    iget-object v0, p0, Landroid/app/enterprise/license/RightsObject;->serverUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 280
    return-void
.end method
