.class public Landroid/app/enterprise/license/EnterpriseLicenseManager;
.super Ljava/lang/Object;
.source "EnterpriseLicenseManager.java"


# static fields
.field public static final ACTION_LICENSE_REGISTRATION:Ljava/lang/String; = "edm.intent.action.license.registration.internal"

.field public static final ACTION_LICENSE_REGISTRATION_UMC:Ljava/lang/String; = "edm.intent.action.license.registration.internal_umc"

.field public static final ACTION_LICENSE_STATUS:Ljava/lang/String; = "edm.intent.action.license.status"

.field public static final ERROR_INTERNAL:I = 0x12d

.field public static final ERROR_INTERNAL_SERVER:I = 0x191

.field public static final ERROR_INVALID_LICENSE:I = 0xc9

.field public static final ERROR_INVALID_PACKAGE_NAME:I = 0xcc

.field public static final ERROR_LICENSE_TERMINATED:I = 0xcb

.field public static final ERROR_NETWORK_DISCONNECTED:I = 0x1f5

.field public static final ERROR_NETWORK_GENERAL:I = 0x1f6

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_CURRENT_DATE:I = 0xcd

.field public static final ERROR_NO_MORE_REGISTRATION:I = 0xca

.field public static final ERROR_NULL_PARAMS:I = 0x65

.field public static final ERROR_UNKNOWN:I = 0x66

.field public static final ERROR_USER_DISAGREES_LICENSE_AGREEMENT:I = 0x259

.field public static final EXTRA_LICENSE_DATA_LICENSE_PERMISSIONS:Ljava/lang/String; = "edm.intent.extra.license.data.license_permissions"

.field public static final EXTRA_LICENSE_DATA_PACKAGENAME:Ljava/lang/String; = "edm.intent.extra.license.data.pkgname"

.field public static final EXTRA_LICENSE_DATA_PACKAGEVERSION:Ljava/lang/String; = "edm.intent.extra.license.data.pkgversion"

.field public static final EXTRA_LICENSE_ERROR_CODE:Ljava/lang/String; = "edm.intent.extra.license.errorcode"

.field public static final EXTRA_LICENSE_PERM_GROUP:Ljava/lang/String; = "edm.intent.extra.license.perm_group"

.field public static final EXTRA_LICENSE_RESULT_TYPE:Ljava/lang/String; = "edm.intent.extra.license.result_type"

.field public static final EXTRA_LICENSE_STATUS:Ljava/lang/String; = "edm.intent.extra.license.status"

.field public static final LICENSE_LOG_API:Ljava/lang/String; = "api_call"

.field public static final LICENSE_LOG_DATE:Ljava/lang/String; = "log_date"

.field public static final LICENSE_PERMISSIONS:Ljava/lang/String; = "Permissions"

.field public static final LICENSE_RESULT_TYPE_ACTIVATION:I = 0x320

.field public static final LICENSE_RESULT_TYPE_VALIDATION:I = 0x321

.field private static final TAG:Ljava/lang/String; = "EnterpriseLicenseManager"

.field private static gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

.field private static lService:Landroid/app/enterprise/license/IEnterpriseLicense;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 267
    const-string v0, "enterprise_license_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    .line 268
    iput-object p2, p0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContext:Landroid/content/Context;

    .line 269
    iput-object p1, p0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 270
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 314
    new-instance v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 302
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 303
    :try_start_0
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    if-nez v0, :cond_0

    .line 304
    new-instance v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    .line 306
    :cond_0
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    monitor-exit v1

    return-object v0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 287
    sget-object v2, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 288
    :try_start_0
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    if-nez v1, :cond_0

    .line 289
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 290
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;

    invoke-direct {v1, v0, p0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    .line 292
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->gLicenseMgrInst:Landroid/app/enterprise/license/EnterpriseLicenseManager;

    monitor-exit v2

    return-object v1

    .line 293
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static getService()Landroid/app/enterprise/license/IEnterpriseLicense;
    .locals 1

    .prologue
    .line 273
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    if-nez v0, :cond_0

    .line 274
    const-string v0, "enterprise_license_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/license/IEnterpriseLicense$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    .line 276
    :cond_0
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    return-object v0
.end method

.method public static log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    .locals 3
    .param p0, "contextInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "apiName"    # Ljava/lang/String;

    .prologue
    .line 642
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 643
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p0, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 646
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public activateLicense(Ljava/lang/String;)V
    .locals 4
    .param p1, "licenseKey"    # Ljava/lang/String;

    .prologue
    .line 595
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 596
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    :cond_0
    :goto_0
    return-void

    .line 598
    :catch_0
    move-exception v0

    .line 599
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public activateLicense(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "licenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 609
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 610
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateLicense(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public activateLicenseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "licenseKey"    # Ljava/lang/String;
    .param p2, "pkgName"    # Ljava/lang/String;
    .param p3, "pkgVersion"    # Ljava/lang/String;

    .prologue
    .line 626
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 627
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    iget-object v2, p0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/license/IEnterpriseLicense;->activateLicenseForUMC(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service for UMC"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)Z
    .locals 3
    .param p1, "status"    # Ljava/lang/String;
    .param p2, "instanceId"    # Ljava/lang/String;
    .param p3, "error"    # Landroid/app/enterprise/license/Error;

    .prologue
    .line 485
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 486
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1, p2, p3}, Landroid/app/enterprise/license/IEnterpriseLicense;->deleteApiCallData(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/Error;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 492
    :goto_0
    return v1

    .line 488
    :catch_0
    move-exception v0

    .line 489
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 492
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteLicense(Ljava/lang/String;)Z
    .locals 3
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 699
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 700
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->deleteLicense(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 706
    :goto_0
    return v1

    .line 702
    :catch_0
    move-exception v0

    .line 703
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 706
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteLicenseByAdmin(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 718
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 719
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->deleteLicenseByAdmin(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 725
    :goto_0
    return v1

    .line 721
    :catch_0
    move-exception v0

    .line 722
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 725
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllLicenseInfo()[Landroid/app/enterprise/license/LicenseInfo;
    .locals 3

    .prologue
    .line 556
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 557
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getAllLicenseInfo()[Landroid/app/enterprise/license/LicenseInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 563
    :goto_0
    return-object v1

    .line 559
    :catch_0
    move-exception v0

    .line 560
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 563
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getApiCallData(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 16
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 432
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 433
    const/4 v7, 0x0

    .line 434
    .local v7, "json":Lorg/json/JSONObject;
    sget-object v14, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object/from16 v0, p1

    invoke-interface {v14, v0}, Landroid/app/enterprise/license/IEnterpriseLicense;->getApiCallData(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 435
    .local v2, "bundle":Landroid/os/Bundle;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 437
    .local v6, "jarray":Lorg/json/JSONArray;
    if-eqz v2, :cond_3

    .line 438
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v13

    .line 439
    .local v13, "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 440
    .local v12, "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 441
    .local v9, "jsonOuter":Lorg/json/JSONObject;
    const/4 v8, 0x0

    .line 442
    .local v8, "jsonInner":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .line 444
    .local v1, "b":Landroid/os/Bundle;
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 445
    .local v11, "keyOuter":Ljava/lang/String;
    new-instance v9, Lorg/json/JSONObject;

    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 446
    .restart local v9    # "jsonOuter":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 447
    .restart local v8    # "jsonInner":Lorg/json/JSONObject;
    invoke-virtual {v2, v11}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 448
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    .line 450
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 451
    .local v10, "keyInner":Ljava/lang/String;
    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v8, v10, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 463
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "jarray":Lorg/json/JSONArray;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    .end local v10    # "keyInner":Ljava/lang/String;
    .end local v11    # "keyOuter":Ljava/lang/String;
    .end local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 464
    .local v3, "e":Lorg/json/JSONException;
    const-string v14, "EnterpriseLicenseManager"

    const-string v15, "JSONException"

    invoke-static {v14, v15, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 468
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    :goto_2
    const/4 v6, 0x0

    :cond_1
    :goto_3
    return-object v6

    .line 453
    .restart local v1    # "b":Landroid/os/Bundle;
    .restart local v2    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "jarray":Lorg/json/JSONArray;
    .restart local v7    # "json":Lorg/json/JSONObject;
    .restart local v8    # "jsonInner":Lorg/json/JSONObject;
    .restart local v9    # "jsonOuter":Lorg/json/JSONObject;
    .restart local v11    # "keyOuter":Ljava/lang/String;
    .restart local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    const-string v14, "log_date"

    invoke-virtual {v9, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 454
    const-string v14, "api_call"

    invoke-virtual {v9, v14, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 457
    invoke-virtual {v6, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 465
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "jarray":Lorg/json/JSONArray;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    .end local v11    # "keyOuter":Ljava/lang/String;
    .end local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v3

    .line 466
    .local v3, "e":Landroid/os/RemoteException;
    const-string v14, "EnterpriseLicenseManager"

    const-string v15, "Failed talking to License policy service "

    invoke-static {v14, v15, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 461
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v2    # "bundle":Landroid/os/Bundle;
    .restart local v6    # "jarray":Lorg/json/JSONArray;
    .restart local v7    # "json":Lorg/json/JSONObject;
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v14

    if-nez v14, :cond_1

    const/4 v6, 0x0

    goto :goto_3
.end method

.method public getApiCallDataByAdmin(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 16
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 507
    move-object/from16 v0, p0

    iget-object v14, v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v15, "EnterpriseLicenseManager.getApiCallDataByAdmin"

    invoke-static {v14, v15}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 509
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v14

    if-eqz v14, :cond_0

    .line 510
    const/4 v7, 0x0

    .line 511
    .local v7, "json":Lorg/json/JSONObject;
    sget-object v14, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object/from16 v0, p0

    iget-object v15, v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v0, p1

    invoke-interface {v14, v15, v0}, Landroid/app/enterprise/license/IEnterpriseLicense;->getApiCallDataByAdmin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 512
    .local v2, "bundle":Landroid/os/Bundle;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 514
    .local v6, "jarray":Lorg/json/JSONArray;
    if-eqz v2, :cond_3

    .line 515
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v13

    .line 516
    .local v13, "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v12, 0x0

    .line 517
    .local v12, "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v9, 0x0

    .line 518
    .local v9, "jsonOuter":Lorg/json/JSONObject;
    const/4 v8, 0x0

    .line 519
    .local v8, "jsonInner":Lorg/json/JSONObject;
    const/4 v1, 0x0

    .line 521
    .local v1, "b":Landroid/os/Bundle;
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    .line 522
    .local v11, "keyOuter":Ljava/lang/String;
    new-instance v9, Lorg/json/JSONObject;

    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 523
    .restart local v9    # "jsonOuter":Lorg/json/JSONObject;
    new-instance v8, Lorg/json/JSONObject;

    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    invoke-direct {v8}, Lorg/json/JSONObject;-><init>()V

    .line 524
    .restart local v8    # "jsonInner":Lorg/json/JSONObject;
    invoke-virtual {v2, v11}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 525
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v12

    .line 527
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 528
    .local v10, "keyInner":Ljava/lang/String;
    invoke-virtual {v1, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    invoke-virtual {v8, v10, v14}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 540
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "jarray":Lorg/json/JSONArray;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    .end local v10    # "keyInner":Ljava/lang/String;
    .end local v11    # "keyOuter":Ljava/lang/String;
    .end local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v3

    .line 541
    .local v3, "e":Lorg/json/JSONException;
    const-string v14, "EnterpriseLicenseManager"

    const-string v15, "JSONException"

    invoke-static {v14, v15, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 545
    .end local v3    # "e":Lorg/json/JSONException;
    :cond_0
    :goto_2
    const/4 v6, 0x0

    :cond_1
    :goto_3
    return-object v6

    .line 530
    .restart local v1    # "b":Landroid/os/Bundle;
    .restart local v2    # "bundle":Landroid/os/Bundle;
    .restart local v5    # "i$":Ljava/util/Iterator;
    .restart local v6    # "jarray":Lorg/json/JSONArray;
    .restart local v7    # "json":Lorg/json/JSONObject;
    .restart local v8    # "jsonInner":Lorg/json/JSONObject;
    .restart local v9    # "jsonOuter":Lorg/json/JSONObject;
    .restart local v11    # "keyOuter":Ljava/lang/String;
    .restart local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_2
    :try_start_1
    const-string v14, "log_date"

    invoke-virtual {v9, v14, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 531
    const-string v14, "api_call"

    invoke-virtual {v9, v14, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 534
    invoke-virtual {v6, v9}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 542
    .end local v1    # "b":Landroid/os/Bundle;
    .end local v2    # "bundle":Landroid/os/Bundle;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "jarray":Lorg/json/JSONArray;
    .end local v7    # "json":Lorg/json/JSONObject;
    .end local v8    # "jsonInner":Lorg/json/JSONObject;
    .end local v9    # "jsonOuter":Lorg/json/JSONObject;
    .end local v11    # "keyOuter":Ljava/lang/String;
    .end local v12    # "setInner":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v13    # "setOuter":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_1
    move-exception v3

    .line 543
    .local v3, "e":Landroid/os/RemoteException;
    const-string v14, "EnterpriseLicenseManager"

    const-string v15, "Failed talking to License policy service "

    invoke-static {v14, v15, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 538
    .end local v3    # "e":Landroid/os/RemoteException;
    .restart local v2    # "bundle":Landroid/os/Bundle;
    .restart local v6    # "jarray":Lorg/json/JSONArray;
    .restart local v7    # "json":Lorg/json/JSONObject;
    :cond_3
    :try_start_2
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result v14

    if-nez v14, :cond_1

    const/4 v6, 0x0

    goto :goto_3
.end method

.method public getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 738
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 739
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getELMLicenseKey(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 745
    :goto_0
    return-object v1

    .line 741
    :catch_0
    move-exception v0

    .line 742
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 745
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getELMPermissions(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 758
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 759
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getELMPermissions(Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 765
    :goto_0
    return-object v1

    .line 761
    :catch_0
    move-exception v0

    .line 762
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 765
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLicenseInfo(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;
    .locals 3
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 576
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 577
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getLicenseInfo(Ljava/lang/String;)Landroid/app/enterprise/license/LicenseInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 583
    :goto_0
    return-object v1

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;
    .locals 3
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 386
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 387
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->getRightsObject(Ljava/lang/String;)Landroid/app/enterprise/license/RightsObject;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 393
    :goto_0
    return-object v1

    .line 389
    :catch_0
    move-exception v0

    .line 390
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 393
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public processLicenseActivationResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageVersion"    # Ljava/lang/String;
    .param p3, "status"    # Ljava/lang/String;
    .param p4, "instanceId"    # Ljava/lang/String;
    .param p5, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .param p6, "error"    # Landroid/app/enterprise/license/Error;
    .param p7, "permGroup"    # Ljava/lang/String;

    .prologue
    .line 337
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 338
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-interface/range {v0 .. v7}, Landroid/app/enterprise/license/IEnterpriseLicense;->processLicenseActivationResponse(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 344
    :goto_0
    return v0

    .line 340
    :catch_0
    move-exception v8

    .line 341
    .local v8, "e":Landroid/os/RemoteException;
    const-string v0, "EnterpriseLicenseManager"

    const-string v1, "Failed talking to License policy service "

    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 344
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processLicenseActivationResponseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "packageVersion"    # Ljava/lang/String;
    .param p3, "status"    # Ljava/lang/String;
    .param p4, "instanceId"    # Ljava/lang/String;
    .param p5, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .param p6, "error"    # Landroid/app/enterprise/license/Error;
    .param p7, "permGroup"    # Ljava/lang/String;

    .prologue
    .line 366
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 367
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-interface/range {v0 .. v7}, Landroid/app/enterprise/license/IEnterpriseLicense;->processLicenseActivationResponseForUMC(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 373
    :goto_0
    return v0

    .line 369
    :catch_0
    move-exception v8

    .line 370
    .local v8, "e":Landroid/os/RemoteException;
    const-string v0, "EnterpriseLicenseManager"

    const-string v1, "Failed talking to License policy service for UMC"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 373
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    .locals 7
    .param p1, "status"    # Ljava/lang/String;
    .param p2, "instanceId"    # Ljava/lang/String;
    .param p3, "RO"    # Landroid/app/enterprise/license/RightsObject;
    .param p4, "error"    # Landroid/app/enterprise/license/Error;
    .param p5, "permGroup"    # Ljava/lang/String;

    .prologue
    .line 411
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 412
    sget-object v0, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/license/IEnterpriseLicense;->processLicenseValidationResult(Ljava/lang/String;Ljava/lang/String;Landroid/app/enterprise/license/RightsObject;Landroid/app/enterprise/license/Error;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 418
    :goto_0
    return v0

    .line 414
    :catch_0
    move-exception v6

    .line 415
    .local v6, "e":Landroid/os/RemoteException;
    const-string v0, "EnterpriseLicenseManager"

    const-string v1, "Failed talking to License policy service "

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 418
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetLicense(Ljava/lang/String;)Z
    .locals 3
    .param p1, "instanceId"    # Ljava/lang/String;

    .prologue
    .line 661
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 662
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->resetLicense(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 668
    :goto_0
    return v1

    .line 664
    :catch_0
    move-exception v0

    .line 665
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 668
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetLicenseByAdmin(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 680
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getService()Landroid/app/enterprise/license/IEnterpriseLicense;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 681
    sget-object v1, Landroid/app/enterprise/license/EnterpriseLicenseManager;->lService:Landroid/app/enterprise/license/IEnterpriseLicense;

    invoke-interface {v1, p1}, Landroid/app/enterprise/license/IEnterpriseLicense;->resetLicenseByAdmin(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 687
    :goto_0
    return v1

    .line 683
    :catch_0
    move-exception v0

    .line 684
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "EnterpriseLicenseManager"

    const-string v2, "Failed talking to License policy service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 687
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
