.class public Landroid/app/enterprise/SSOPolicy;
.super Ljava/lang/Object;
.source "SSOPolicy.java"


# static fields
.field public static final CENTRIFY_SSO_PKG:Ljava/lang/String; = "com.centrify.sso.samsung"

.field public static final INTENT_PARAM_SSO_PACKAGE_NAME:Ljava/lang/String; = "pacakgeName"

.field public static final INTENT_PARAM_USER_ID:Ljava/lang/String; = "userid"

.field public static final INTENT_SSO_SERVICE_CONNECTED_IN_USERSPACE:Ljava/lang/String; = "sso.enterprise.userspace.setup.success"

.field public static final INTENT_SSO_SERVICE_DISCONNECTED_IN_USERSPACE:Ljava/lang/String; = "sso.enterprise.userspace.disconnected"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_CUSTOMER_LOGO:Ljava/lang/String; = "SSO_CUSTOMER_LOGO"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_CUSTOMER_NAME:Ljava/lang/String; = "SSO_CUSTOMER_NAME"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_IDP_CONF_DATA:Ljava/lang/String; = "SSO_IDP_CONF_DATA"

.field public static final SAMSUNG_SSO_BUNDLE_KEY_KRB_CONF_DATA:Ljava/lang/String; = "SSO_KRB_CONF_DATA"

.field public static final SAMSUNG_SSO_PKG:Ljava/lang/String; = "com.sec.android.service.singlesignon"

.field public static final SSO_CENTRIFY_SERVICE_PACKAGE_PATH:Ljava/lang/String; = "/system/preloadedsso/ssoservice.apk_"

.field public static final SSO_RESULT_FAILURE:I = 0x1

.field public static final SSO_RESULT_SUCCESS:I = 0x0

.field public static final SSO_SAMSUNG_SERVICE_PACKAGE_PATH:Ljava/lang/String; = "/system/preloadedsso/samsungsso.apk_"

.field private static gSSOService:Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "solutionPackageName"    # Ljava/lang/String;

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    const-string v0, "SSOPolicy"

    iput-object v0, p0, Landroid/app/enterprise/SSOPolicy;->TAG:Ljava/lang/String;

    .line 240
    iput-object p1, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 241
    return-void
.end method

.method static declared-synchronized getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    .locals 2

    .prologue
    .line 229
    const-class v1, Landroid/app/enterprise/SSOPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/app/enterprise/SSOPolicy;->gSSOService:Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    if-nez v0, :cond_0

    .line 230
    const-string v0, "enterprise_user_space_sso_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/SSOPolicy;->gSSOService:Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    .line 233
    :cond_0
    sget-object v0, Landroid/app/enterprise/SSOPolicy;->gSSOService:Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public deleteSSOWhiteListInUserSpace(Ljava/lang/String;Ljava/util/List;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 356
    .local p2, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.deleteSSOWhiteList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 357
    const/4 v0, 0x1

    .line 358
    .local v0, "deleteSSOWhiteListValue":I
    const/4 v3, 0x0

    .line 360
    .local v3, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 361
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 362
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v0

    .line 378
    .end local v0    # "deleteSSOWhiteListValue":I
    .local v1, "deleteSSOWhiteListValue":I
    :goto_0
    return v1

    .line 367
    .end local v1    # "deleteSSOWhiteListValue":I
    .restart local v0    # "deleteSSOWhiteListValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, p2}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->deleteSSOWhiteListInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    .line 368
    if-eqz v3, :cond_1

    .line 369
    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 370
    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_1
    :goto_1
    move v1, v0

    .line 378
    .end local v0    # "deleteSSOWhiteListValue":I
    .restart local v1    # "deleteSSOWhiteListValue":I
    goto :goto_0

    .line 371
    .end local v1    # "deleteSSOWhiteListValue":I
    .restart local v0    # "deleteSSOWhiteListValue":I
    :cond_2
    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 372
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 375
    :catch_0
    move-exception v2

    .line 376
    .local v2, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API deleteSSOWhiteList-Exception"

    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public forceReauthenticateInUserSpace(Ljava/lang/String;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 548
    const/4 v2, 0x1

    .line 549
    .local v2, "forceReauthenticateValue":I
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.forceReauthenticate"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 550
    const/4 v1, 0x0

    .line 552
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 553
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 554
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 570
    .end local v2    # "forceReauthenticateValue":I
    .local v3, "forceReauthenticateValue":I
    :goto_0
    return v3

    .line 559
    .end local v3    # "forceReauthenticateValue":I
    .restart local v2    # "forceReauthenticateValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->forceReauthenticateInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 560
    if-eqz v1, :cond_1

    .line 561
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 562
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 570
    .end local v2    # "forceReauthenticateValue":I
    .restart local v3    # "forceReauthenticateValue":I
    goto :goto_0

    .line 563
    .end local v3    # "forceReauthenticateValue":I
    .restart local v2    # "forceReauthenticateValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 564
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 567
    :catch_0
    move-exception v0

    .line 568
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API forceReauthenticate-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getAppAllowedStateInUserSpace(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 420
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.getAppAllowedState"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 421
    const/4 v2, 0x0

    .line 422
    .local v2, "getAppAllowedStateValue":Z
    const/4 v1, 0x0

    .line 424
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 425
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 426
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 443
    .end local v2    # "getAppAllowedStateValue":Z
    .local v3, "getAppAllowedStateValue":I
    :goto_0
    return v3

    .line 431
    .end local v3    # "getAppAllowedStateValue":I
    .restart local v2    # "getAppAllowedStateValue":Z
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, p2}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->getAppAllowedStateInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 433
    if-eqz v1, :cond_1

    .line 434
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 435
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 443
    .restart local v3    # "getAppAllowedStateValue":I
    goto :goto_0

    .line 436
    .end local v3    # "getAppAllowedStateValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 437
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API getAppAllowedState-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isSSOReadyInUserSpace(Ljava/lang/String;)Z
    .locals 6
    .param p1, "solutionPackageName"    # Ljava/lang/String;

    .prologue
    .line 787
    iget-object v4, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "SSOPolicy.isISAReady"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 788
    const/4 v1, 0x0

    .line 789
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    const/4 v2, 0x0

    .line 791
    .local v2, "result":Z
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v3

    .line 792
    .local v3, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-eqz v3, :cond_0

    .line 794
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->isSSOReadyInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 795
    if-eqz v1, :cond_0

    .line 796
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_1

    .line 797
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 806
    :cond_0
    :goto_0
    return v2

    .line 798
    :cond_1
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 799
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 802
    :catch_0
    move-exception v0

    .line 803
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "SSOPolicy"

    const-string v5, "Failed at SSOPolicy API isISAReady-Exception"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public pushSSODataInUserSpace(Ljava/lang/String;Landroid/os/Bundle;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .param p2, "dataBundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 674
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.pushSSOData"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 675
    const/4 v2, 0x1

    .line 676
    .local v2, "pushDataValue":I
    const/4 v1, 0x0

    .line 678
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 679
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 680
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 696
    .end local v2    # "pushDataValue":I
    .local v3, "pushDataValue":I
    :goto_0
    return v3

    .line 685
    .end local v3    # "pushDataValue":I
    .restart local v2    # "pushDataValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, p2}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->pushSSODataInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/Bundle;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 686
    if-eqz v1, :cond_1

    .line 687
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 688
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 696
    .end local v2    # "pushDataValue":I
    .restart local v3    # "pushDataValue":I
    goto :goto_0

    .line 689
    .end local v3    # "pushDataValue":I
    .restart local v2    # "pushDataValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 690
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 693
    :catch_0
    move-exception v0

    .line 694
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API pushSSOData-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCustomerInfoInUserSpace(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .param p2, "companyName"    # Ljava/lang/String;
    .param p3, "logoFilePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 487
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.setCustomerInfo"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 488
    const/4 v2, 0x1

    .line 489
    .local v2, "setCustomerInfoValue":I
    const/4 v1, 0x0

    .line 491
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 492
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 493
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 510
    .end local v2    # "setCustomerInfoValue":I
    .local v3, "setCustomerInfoValue":I
    :goto_0
    return v3

    .line 498
    .end local v3    # "setCustomerInfoValue":I
    .restart local v2    # "setCustomerInfoValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, p2, p3}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->setCustomerInfoInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 500
    if-eqz v1, :cond_1

    .line 501
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 502
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 510
    .end local v2    # "setCustomerInfoValue":I
    .restart local v3    # "setCustomerInfoValue":I
    goto :goto_0

    .line 503
    .end local v3    # "setCustomerInfoValue":I
    .restart local v2    # "setCustomerInfoValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 504
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 507
    :catch_0
    move-exception v0

    .line 508
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API setCustomerInfo-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setSSOWhiteListInUserSpace(Ljava/lang/String;Ljava/util/List;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 288
    .local p2, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.setSSOWhiteList"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 289
    const/4 v2, 0x1

    .line 290
    .local v2, "setSSOWhiteListValue":I
    const/4 v1, 0x0

    .line 292
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v4

    .line 293
    .local v4, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v4, :cond_0

    .line 294
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 310
    .end local v2    # "setSSOWhiteListValue":I
    .local v3, "setSSOWhiteListValue":I
    :goto_0
    return v3

    .line 299
    .end local v3    # "setSSOWhiteListValue":I
    .restart local v2    # "setSSOWhiteListValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1, p2}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->setSSOWhiteListInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 300
    if-eqz v1, :cond_1

    .line 301
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 302
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :cond_1
    :goto_1
    move v3, v2

    .line 310
    .end local v2    # "setSSOWhiteListValue":I
    .restart local v3    # "setSSOWhiteListValue":I
    goto :goto_0

    .line 303
    .end local v3    # "setSSOWhiteListValue":I
    .restart local v2    # "setSSOWhiteListValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 304
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 307
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API setSSOWhiteList-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setupSSOInUserSpace(Ljava/lang/String;)I
    .locals 6
    .param p1, "solutionPackageName"    # Ljava/lang/String;

    .prologue
    .line 734
    iget-object v4, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "SSOPolicy.setupSSO"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 735
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v3

    .line 736
    .local v3, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    const/4 v2, 0x1

    .line 737
    .local v2, "setupSSOValue":I
    const/4 v1, 0x0

    .line 739
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    if-eqz v3, :cond_0

    .line 741
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v3, v4, p1}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->setupSSOInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 742
    if-eqz v1, :cond_0

    .line 743
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    if-nez v4, :cond_1

    .line 744
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 753
    :cond_0
    :goto_0
    return v2

    .line 745
    :cond_1
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 746
    new-instance v4, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v4}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 749
    :catch_0
    move-exception v0

    .line 750
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "SSOPolicy"

    const-string v5, "Failed at SSOPolicy API setupSSO-Exception"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public unenrollInUserSpace(Ljava/lang/String;)I
    .locals 7
    .param p1, "solutionPackageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 610
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v6, "SSOPolicy.unenroll"

    invoke-static {v5, v6}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 611
    const/4 v3, 0x1

    .line 612
    .local v3, "unenrollValue":I
    const/4 v1, 0x0

    .line 614
    .local v1, "enterpriseSSOResponseData":Landroid/app/enterprise/EnterpriseResponseData;, "Landroid/app/enterprise/EnterpriseResponseData<*>;"
    invoke-static {}, Landroid/app/enterprise/SSOPolicy;->getSSOService()Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;

    move-result-object v2

    .line 615
    .local v2, "ssoService":Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;
    if-nez v2, :cond_0

    .line 616
    const-string v5, "SSOPolicy"

    const-string v6, "SSOService is not yet ready!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v4, v3

    .line 632
    .end local v3    # "unenrollValue":I
    .local v4, "unenrollValue":I
    :goto_0
    return v4

    .line 621
    .end local v4    # "unenrollValue":I
    .restart local v3    # "unenrollValue":I
    :cond_0
    :try_start_0
    iget-object v5, p0, Landroid/app/enterprise/SSOPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v5, p1}, Landroid/app/enterprise/IEnterpriseUserSpaceSSOPolicy;->unenrollInUserSpace(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    .line 622
    if-eqz v1, :cond_1

    .line 623
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    if-nez v5, :cond_2

    .line 624
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_1
    :goto_1
    move v4, v3

    .line 632
    .end local v3    # "unenrollValue":I
    .restart local v4    # "unenrollValue":I
    goto :goto_0

    .line 625
    .end local v4    # "unenrollValue":I
    .restart local v3    # "unenrollValue":I
    :cond_2
    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    .line 626
    new-instance v5, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v5}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Landroid/os/RemoteException;
    const-string v5, "SSOPolicy"

    const-string v6, "Failed at SSOPolicy API unenroll-Exception"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
