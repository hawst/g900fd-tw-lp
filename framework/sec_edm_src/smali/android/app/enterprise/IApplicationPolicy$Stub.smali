.class public abstract Landroid/app/enterprise/IApplicationPolicy$Stub;
.super Landroid/os/Binder;
.source "IApplicationPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IApplicationPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IApplicationPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IApplicationPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IApplicationPolicy"

.field static final TRANSACTION_addAppNotificationBlackList:I = 0x3f

.field static final TRANSACTION_addAppNotificationWhiteList:I = 0x42

.field static final TRANSACTION_addAppPackageNameToBlackList:I = 0x30

.field static final TRANSACTION_addAppPackageNameToWhiteList:I = 0x33

.field static final TRANSACTION_addAppPermissionToBlackList:I = 0x24

.field static final TRANSACTION_addAppSignatureToBlackList:I = 0x27

.field static final TRANSACTION_addAppSignatureToWhiteList:I = 0x5f

.field static final TRANSACTION_addHomeShortcut:I = 0x49

.field static final TRANSACTION_addPackageToInstallWhiteList:I = 0x63

.field static final TRANSACTION_addPackagesToClearCacheBlackList:I = 0x6e

.field static final TRANSACTION_addPackagesToClearCacheWhiteList:I = 0x71

.field static final TRANSACTION_addPackagesToClearDataBlackList:I = 0x67

.field static final TRANSACTION_addPackagesToClearDataWhiteList:I = 0x6a

.field static final TRANSACTION_addPackagesToDisableClipboardBlackList:I = 0x88

.field static final TRANSACTION_addPackagesToDisableClipboardWhiteList:I = 0x8b

.field static final TRANSACTION_addPackagesToDisableUpdateBlackList:I = 0x7a

.field static final TRANSACTION_addPackagesToDisableUpdateWhiteList:I = 0x7d

.field static final TRANSACTION_addPackagesToForceStopBlackList:I = 0x3b

.field static final TRANSACTION_addPackagesToForceStopWhiteList:I = 0x52

.field static final TRANSACTION_addPackagesToPreventStartBlackList:I = 0x83

.field static final TRANSACTION_addPackagesToWidgetBlackList:I = 0x57

.field static final TRANSACTION_addPackagesToWidgetWhiteList:I = 0x55

.field static final TRANSACTION_addUsbDevicesForDefaultAccess:I = 0x95

.field static final TRANSACTION_backupApplicationData:I = 0x4c

.field static final TRANSACTION_changeApplicationIcon:I = 0x22

.field static final TRANSACTION_changeApplicationName:I = 0x75

.field static final TRANSACTION_clearDisableClipboardBlackList:I = 0x8e

.field static final TRANSACTION_clearDisableClipboardWhiteList:I = 0x8f

.field static final TRANSACTION_clearDisableUpdateBlackList:I = 0x81

.field static final TRANSACTION_clearDisableUpdateWhiteList:I = 0x82

.field static final TRANSACTION_clearPreventStartBlackList:I = 0x86

.field static final TRANSACTION_clearUsbDevicesForDefaultAccess:I = 0x96

.field static final TRANSACTION_deleteHomeShortcut:I = 0x4a

.field static final TRANSACTION_deleteManagedAppInfo:I = 0xd

.field static final TRANSACTION_enableOcspCheck:I = 0x5d

.field static final TRANSACTION_enableRevocationCheck:I = 0x5b

.field static final TRANSACTION_getAddHomeShorcutRequested:I = 0x9c

.field static final TRANSACTION_getAllAppLastUsage:I = 0x1f

.field static final TRANSACTION_getAllWidgets:I = 0x4d

.field static final TRANSACTION_getAppInstallToSdCard:I = 0x50

.field static final TRANSACTION_getAppInstallationMode:I = 0x39

.field static final TRANSACTION_getAppNotificationBlackList:I = 0x41

.field static final TRANSACTION_getAppNotificationWhiteList:I = 0x44

.field static final TRANSACTION_getAppPackageNamesAllBlackLists:I = 0x32

.field static final TRANSACTION_getAppPackageNamesAllWhiteLists:I = 0x35

.field static final TRANSACTION_getAppPermissionsAllBlackLists:I = 0x36

.field static final TRANSACTION_getAppPermissionsBlackList:I = 0x26

.field static final TRANSACTION_getAppSignatureBlackList:I = 0x29

.field static final TRANSACTION_getAppSignaturesAllBlackLists:I = 0x37

.field static final TRANSACTION_getAppSignaturesAllWhiteLists:I = 0x62

.field static final TRANSACTION_getAppSignaturesWhiteList:I = 0x61

.field static final TRANSACTION_getApplicationCacheSize:I = 0x18

.field static final TRANSACTION_getApplicationCodeSize:I = 0x16

.field static final TRANSACTION_getApplicationComponentState:I = 0x78

.field static final TRANSACTION_getApplicationCpuUsage:I = 0x1a

.field static final TRANSACTION_getApplicationDataSize:I = 0x17

.field static final TRANSACTION_getApplicationIconFromDb:I = 0x23

.field static final TRANSACTION_getApplicationInstallationEnabled:I = 0xe

.field static final TRANSACTION_getApplicationMemoryUsage:I = 0x19

.field static final TRANSACTION_getApplicationName:I = 0x11

.field static final TRANSACTION_getApplicationNameFromDb:I = 0x76

.field static final TRANSACTION_getApplicationNotificationMode:I = 0x46

.field static final TRANSACTION_getApplicationNotificationModeAsUser:I = 0x47

.field static final TRANSACTION_getApplicationStateEnabled:I = 0xc

.field static final TRANSACTION_getApplicationStateList:I = 0x2d

.field static final TRANSACTION_getApplicationTotalSize:I = 0x15

.field static final TRANSACTION_getApplicationUid:I = 0x12

.field static final TRANSACTION_getApplicationUninstallationEnabled:I = 0xf

.field static final TRANSACTION_getApplicationUninstallationMode:I = 0x4e

.field static final TRANSACTION_getApplicationVersion:I = 0x13

.field static final TRANSACTION_getApplicationVersionCode:I = 0x14

.field static final TRANSACTION_getApplicationsList:I = 0x9

.field static final TRANSACTION_getAvgNoAppUsagePerMonth:I = 0x1e

.field static final TRANSACTION_getDisabledPackages:I = 0x99

.field static final TRANSACTION_getHomeShortcuts:I = 0x91

.field static final TRANSACTION_getInstalledApplicationsIDList:I = 0x10

.field static final TRANSACTION_getInstalledManagedApplicationsList:I = 0x8

.field static final TRANSACTION_getNetworkStats:I = 0x20

.field static final TRANSACTION_getPackagesFromClearCacheBlackList:I = 0x70

.field static final TRANSACTION_getPackagesFromClearCacheWhiteList:I = 0x72

.field static final TRANSACTION_getPackagesFromClearDataBlackList:I = 0x69

.field static final TRANSACTION_getPackagesFromClearDataWhiteList:I = 0x6b

.field static final TRANSACTION_getPackagesFromDisableClipboardBlackList:I = 0x8a

.field static final TRANSACTION_getPackagesFromDisableClipboardWhiteList:I = 0x8d

.field static final TRANSACTION_getPackagesFromDisableUpdateBlackList:I = 0x7c

.field static final TRANSACTION_getPackagesFromDisableUpdateWhiteList:I = 0x7e

.field static final TRANSACTION_getPackagesFromForceStopBlackList:I = 0x51

.field static final TRANSACTION_getPackagesFromForceStopWhiteList:I = 0x53

.field static final TRANSACTION_getPackagesFromInstallWhiteList:I = 0x64

.field static final TRANSACTION_getPackagesFromPreventStartBlackList:I = 0x84

.field static final TRANSACTION_getPackagesFromWidgetBlackList:I = 0x54

.field static final TRANSACTION_getPackagesFromWidgetWhiteList:I = 0x3d

.field static final TRANSACTION_getTopNCPUUsageApp:I = 0x1d

.field static final TRANSACTION_getTopNDataUsageApp:I = 0x1c

.field static final TRANSACTION_getTopNMemoryUsageApp:I = 0x1b

.field static final TRANSACTION_getUsbDevicesForDefaultAccess:I = 0x94

.field static final TRANSACTION_installAppWithCallback:I = 0x92

.field static final TRANSACTION_installApplication:I = 0x5

.field static final TRANSACTION_installExistingApplication:I = 0x97

.field static final TRANSACTION_isApplicationClearCacheDisabled:I = 0x74

.field static final TRANSACTION_isApplicationClearDataDisabled:I = 0x6d

.field static final TRANSACTION_isApplicationForceStopDisabled:I = 0x3c

.field static final TRANSACTION_isApplicationInstalled:I = 0x3

.field static final TRANSACTION_isApplicationRunning:I = 0x4

.field static final TRANSACTION_isApplicationStartDisabledAsUser:I = 0x87

.field static final TRANSACTION_isIntentDisabled:I = 0x2f

.field static final TRANSACTION_isMetaKeyEventRequested:I = 0x9b

.field static final TRANSACTION_isOcspCheckEnabled:I = 0x5e

.field static final TRANSACTION_isPackageClipboardAllowed:I = 0x90

.field static final TRANSACTION_isPackageInInstallWhiteList:I = 0x65

.field static final TRANSACTION_isPackageUpdateAllowed:I = 0x80

.field static final TRANSACTION_isRevocationCheckEnabled:I = 0x5c

.field static final TRANSACTION_isStatusBarNotificationAllowedAsUser:I = 0x48

.field static final TRANSACTION_isWidgetAllowed:I = 0x3e

.field static final TRANSACTION_removeAppNotificationBlackList:I = 0x40

.field static final TRANSACTION_removeAppNotificationWhiteList:I = 0x43

.field static final TRANSACTION_removeAppPackageNameFromBlackList:I = 0x31

.field static final TRANSACTION_removeAppPackageNameFromWhiteList:I = 0x34

.field static final TRANSACTION_removeAppPermissionFromBlackList:I = 0x25

.field static final TRANSACTION_removeAppSignatureFromBlackList:I = 0x28

.field static final TRANSACTION_removeAppSignatureFromWhiteList:I = 0x60

.field static final TRANSACTION_removeManagedApplications:I = 0x1

.field static final TRANSACTION_removePackageFromInstallWhiteList:I = 0x66

.field static final TRANSACTION_removePackagesFromClearCacheBlackList:I = 0x6f

.field static final TRANSACTION_removePackagesFromClearCacheWhiteList:I = 0x73

.field static final TRANSACTION_removePackagesFromClearDataBlackList:I = 0x68

.field static final TRANSACTION_removePackagesFromClearDataWhiteList:I = 0x6c

.field static final TRANSACTION_removePackagesFromDisableClipboardBlackList:I = 0x89

.field static final TRANSACTION_removePackagesFromDisableClipboardWhiteList:I = 0x8c

.field static final TRANSACTION_removePackagesFromDisableUpdateBlackList:I = 0x7b

.field static final TRANSACTION_removePackagesFromDisableUpdateWhiteList:I = 0x7f

.field static final TRANSACTION_removePackagesFromForceStopBlackList:I = 0x5a

.field static final TRANSACTION_removePackagesFromForceStopWhiteList:I = 0x59

.field static final TRANSACTION_removePackagesFromPreventStartBlackList:I = 0x85

.field static final TRANSACTION_removePackagesFromWidgetBlackList:I = 0x58

.field static final TRANSACTION_removePackagesFromWidgetWhiteList:I = 0x56

.field static final TRANSACTION_requestMetaKeyEvent:I = 0x9a

.field static final TRANSACTION_restoreApplicationData:I = 0x4b

.field static final TRANSACTION_setAppInstallToSdCard:I = 0x4f

.field static final TRANSACTION_setAppInstallationMode:I = 0x38

.field static final TRANSACTION_setApplicationComponentState:I = 0x77

.field static final TRANSACTION_setApplicationInstallationDisabled:I = 0xa

.field static final TRANSACTION_setApplicationNotificationMode:I = 0x45

.field static final TRANSACTION_setApplicationState:I = 0x7

.field static final TRANSACTION_setApplicationStateList:I = 0x2e

.field static final TRANSACTION_setApplicationUninstallationDisabled:I = 0xb

.field static final TRANSACTION_setApplicationUninstallationMode:I = 0x3a

.field static final TRANSACTION_setAsManagedApp:I = 0x2a

.field static final TRANSACTION_startApp:I = 0x2c

.field static final TRANSACTION_stopApp:I = 0x2b

.field static final TRANSACTION_uninstallApplication:I = 0x6

.field static final TRANSACTION_updateApplicationTable:I = 0x98

.field static final TRANSACTION_updateDataUsageDb:I = 0x21

.field static final TRANSACTION_updateWidgetStatus:I = 0x93

.field static final TRANSACTION_wipeApplicationData:I = 0x2

.field static final TRANSACTION_writeData:I = 0x79


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 21
    const-string v0, "android.app.enterprise.IApplicationPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 22
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 29
    if-nez p0, :cond_0

    .line 30
    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0

    .line 32
    :cond_0
    const-string v1, "android.app.enterprise.IApplicationPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 33
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IApplicationPolicy;

    if-eqz v1, :cond_1

    .line 34
    check-cast v0, Landroid/app/enterprise/IApplicationPolicy;

    goto :goto_0

    .line 36
    :cond_1
    new-instance v0, Landroid/app/enterprise/IApplicationPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IApplicationPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 40
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 19
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 44
    sparse-switch p1, :sswitch_data_0

    .line 2648
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 48
    :sswitch_0
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 49
    const/4 v2, 0x1

    goto :goto_0

    .line 53
    :sswitch_1
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 55
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 56
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 62
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 63
    .local v9, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeManagedApplications(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    .line 64
    .local v18, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 65
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 66
    const/4 v2, 0x1

    goto :goto_0

    .line 59
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 70
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 79
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 80
    .local v4, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->wipeApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 81
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 82
    if-eqz v12, :cond_2

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 83
    const/4 v2, 0x1

    goto :goto_0

    .line 76
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_1
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 82
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 87
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_3
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    .line 90
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 96
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 97
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationInstalled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 98
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 99
    if-eqz v12, :cond_4

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 100
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 93
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_3
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 99
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_5

    .line 104
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_4
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5

    .line 107
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 113
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 114
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationRunning(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 115
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 116
    if-eqz v12, :cond_6

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 117
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 110
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_5
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 116
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_6
    const/4 v2, 0x0

    goto :goto_7

    .line 121
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_5
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7

    .line 124
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 130
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 132
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v5, 0x1

    .line 133
    .local v5, "_arg2":Z
    :goto_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->installApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v12

    .line 134
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 135
    if-eqz v12, :cond_9

    const/4 v2, 0x1

    :goto_a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 136
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 127
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_7
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 132
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_8
    const/4 v5, 0x0

    goto :goto_9

    .line 135
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_9
    const/4 v2, 0x0

    goto :goto_a

    .line 140
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_6
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a

    .line 143
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 149
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 151
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b

    const/4 v5, 0x1

    .line 152
    .restart local v5    # "_arg2":Z
    :goto_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->uninstallApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v12

    .line 153
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 154
    if-eqz v12, :cond_c

    const/4 v2, 0x1

    :goto_d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 155
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 146
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 151
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_b
    const/4 v5, 0x0

    goto :goto_c

    .line 154
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_c
    const/4 v2, 0x0

    goto :goto_d

    .line 159
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_7
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d

    .line 162
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 168
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 170
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e

    const/4 v5, 0x1

    .line 171
    .restart local v5    # "_arg2":Z
    :goto_f
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v12

    .line 172
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 173
    if-eqz v12, :cond_f

    const/4 v2, 0x1

    :goto_10
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 174
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 165
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e

    .line 170
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_e
    const/4 v5, 0x0

    goto :goto_f

    .line 173
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_f
    const/4 v2, 0x0

    goto :goto_10

    .line 178
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_8
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_10

    .line 181
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 186
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getInstalledManagedApplicationsList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;

    move-result-object v12

    .line 187
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 188
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 189
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 184
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_10
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 193
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 195
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_11

    .line 196
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 202
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 203
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationsList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[Landroid/app/enterprise/ManagedAppInfo;

    move-result-object v12

    .line 204
    .local v12, "_result":[Landroid/app/enterprise/ManagedAppInfo;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 205
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 206
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 199
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":[Landroid/app/enterprise/ManagedAppInfo;
    :cond_11
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 210
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 212
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_12

    .line 213
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 219
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 221
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_13

    const/4 v5, 0x1

    .line 222
    .restart local v5    # "_arg2":Z
    :goto_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationInstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V

    .line 223
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 216
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    :cond_12
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 221
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_13
    const/4 v5, 0x0

    goto :goto_14

    .line 228
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    :sswitch_b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 230
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_14

    .line 231
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 237
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 239
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_15

    const/4 v5, 0x1

    .line 240
    .restart local v5    # "_arg2":Z
    :goto_16
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationUninstallationDisabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)V

    .line 241
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 242
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 234
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    :cond_14
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 239
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_15
    const/4 v5, 0x0

    goto :goto_16

    .line 246
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    :sswitch_c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_16

    .line 249
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 255
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 256
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationStateEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 257
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 258
    if-eqz v12, :cond_17

    const/4 v2, 0x1

    :goto_18
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 259
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 252
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_16
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 258
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_17
    const/4 v2, 0x0

    goto :goto_18

    .line 263
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_18

    .line 266
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 272
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 273
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->deleteManagedAppInfo(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 274
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 275
    if-eqz v12, :cond_19

    const/4 v2, 0x1

    :goto_1a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 276
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 269
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_18
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 275
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_19
    const/4 v2, 0x0

    goto :goto_1a

    .line 280
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 282
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1a

    .line 283
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 289
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 290
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationInstallationEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 291
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 292
    if-eqz v12, :cond_1b

    const/4 v2, 0x1

    :goto_1c
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 293
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 286
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_1a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 292
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_1b
    const/4 v2, 0x0

    goto :goto_1c

    .line 297
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 299
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1c

    .line 300
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 306
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 307
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationUninstallationEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 308
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 309
    if-eqz v12, :cond_1d

    const/4 v2, 0x1

    :goto_1e
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 310
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 303
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_1c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    .line 309
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_1d
    const/4 v2, 0x0

    goto :goto_1e

    .line 314
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_10
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 316
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1e

    .line 317
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 322
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getInstalledApplicationsIDList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;

    move-result-object v12

    .line 323
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 324
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 325
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 320
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_1e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    .line 329
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 331
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1f

    .line 332
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 338
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 339
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 340
    .local v12, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 341
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 342
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 335
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Ljava/lang/String;
    :cond_1f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 346
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 348
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_20

    .line 349
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 355
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 356
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationUid(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I

    move-result v12

    .line 357
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 358
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 359
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 352
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":I
    :cond_20
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 363
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 365
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_21

    .line 366
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 372
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 373
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationVersion(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 374
    .local v12, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 375
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 376
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 369
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Ljava/lang/String;
    :cond_21
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 380
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_22

    .line 383
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 389
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_23
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 390
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationVersionCode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I

    move-result v12

    .line 391
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 392
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 393
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 386
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":I
    :cond_22
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_23

    .line 397
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 399
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_23

    .line 400
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 406
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 407
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationTotalSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 408
    .local v12, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 409
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 410
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 403
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_23
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 414
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 416
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_24

    .line 417
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 423
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 424
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationCodeSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 425
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 426
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 427
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 420
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_24
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    .line 431
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 433
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_25

    .line 434
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 440
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_26
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 441
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationDataSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 442
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 443
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 444
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 437
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_25
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_26

    .line 448
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 450
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_26

    .line 451
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 457
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 458
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationCacheSize(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 459
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 460
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 461
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 454
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_26
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 465
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 467
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_27

    .line 468
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 474
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 475
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationMemoryUsage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 476
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 477
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 478
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 471
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_27
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 482
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 484
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_28

    .line 485
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 491
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 492
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationCpuUsage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v12

    .line 493
    .restart local v12    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 494
    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/os/Parcel;->writeLong(J)V

    .line 495
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 488
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":J
    :cond_28
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 499
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 501
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_29

    .line 502
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 508
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 510
    .local v4, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2a

    const/4 v5, 0x1

    .line 511
    .restart local v5    # "_arg2":Z
    :goto_2b
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getTopNMemoryUsageApp(Landroid/app/enterprise/ContextInfo;IZ)Ljava/util/List;

    move-result-object v14

    .line 512
    .local v14, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 513
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 514
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 505
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Z
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    :cond_29
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2a

    .line 510
    .restart local v4    # "_arg1":I
    :cond_2a
    const/4 v5, 0x0

    goto :goto_2b

    .line 518
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    :sswitch_1c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 520
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2b

    .line 521
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 527
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 528
    .restart local v4    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getTopNDataUsageApp(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v14

    .line 529
    .restart local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 530
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 531
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 524
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    :cond_2b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 535
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 537
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2c

    .line 538
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 544
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 546
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2d

    const/4 v5, 0x1

    .line 547
    .restart local v5    # "_arg2":Z
    :goto_2e
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getTopNCPUUsageApp(Landroid/app/enterprise/ContextInfo;IZ)Ljava/util/List;

    move-result-object v14

    .line 548
    .restart local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 549
    move-object/from16 v0, p3

    invoke-virtual {v0, v14}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 550
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 541
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Z
    .end local v14    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppInfo;>;"
    :cond_2c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 546
    .restart local v4    # "_arg1":I
    :cond_2d
    const/4 v5, 0x0

    goto :goto_2e

    .line 554
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    :sswitch_1e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 556
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2e

    .line 557
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 562
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2f
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAvgNoAppUsagePerMonth(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/AppInfoLastUsage;

    move-result-object v12

    .line 563
    .local v12, "_result":[Landroid/app/enterprise/AppInfoLastUsage;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 564
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 565
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 560
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Landroid/app/enterprise/AppInfoLastUsage;
    :cond_2e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2f

    .line 569
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 571
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2f

    .line 572
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 577
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAllAppLastUsage(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/AppInfoLastUsage;

    move-result-object v12

    .line 578
    .restart local v12    # "_result":[Landroid/app/enterprise/AppInfoLastUsage;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 579
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v2}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 580
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 575
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Landroid/app/enterprise/AppInfoLastUsage;
    :cond_2f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 584
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 586
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_30

    .line 587
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 592
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_31
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getNetworkStats(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v15

    .line 593
    .local v15, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/NetworkStats;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 594
    move-object/from16 v0, p3

    invoke-virtual {v0, v15}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 595
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 590
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v15    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/NetworkStats;>;"
    :cond_30
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_31

    .line 599
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 600
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->updateDataUsageDb()V

    .line 601
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 602
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 606
    :sswitch_22
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 608
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_31

    .line 609
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 615
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 617
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v5

    .line 618
    .local v5, "_arg2":[B
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->changeApplicationIcon(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)Z

    move-result v12

    .line 619
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 620
    if-eqz v12, :cond_32

    const/4 v2, 0x1

    :goto_33
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 621
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 612
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":[B
    .end local v12    # "_result":Z
    :cond_31
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 620
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":[B
    .restart local v12    # "_result":Z
    :cond_32
    const/4 v2, 0x0

    goto :goto_33

    .line 625
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":[B
    .end local v12    # "_result":Z
    :sswitch_23
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 627
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_33

    .line 628
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 634
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 635
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationIconFromDb(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)[B

    move-result-object v12

    .line 636
    .local v12, "_result":[B
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 637
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 638
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 631
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":[B
    :cond_33
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 642
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_24
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 644
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_34

    .line 645
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 651
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 652
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppPermissionToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 653
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 654
    if-eqz v12, :cond_35

    const/4 v2, 0x1

    :goto_36
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 655
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 648
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_34
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 654
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_35
    const/4 v2, 0x0

    goto :goto_36

    .line 659
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_25
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 661
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_36

    .line 662
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 668
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 669
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppPermissionFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 670
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 671
    if-eqz v12, :cond_37

    const/4 v2, 0x1

    :goto_38
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 672
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 665
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_36
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 671
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_37
    const/4 v2, 0x0

    goto :goto_38

    .line 676
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_26
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 678
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_38

    .line 679
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 684
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppPermissionsBlackList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;

    move-result-object v12

    .line 685
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 686
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 687
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 682
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_38
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 691
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 693
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_39

    .line 694
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 700
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 701
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppSignatureToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 702
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 703
    if-eqz v12, :cond_3a

    const/4 v2, 0x1

    :goto_3b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 704
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 697
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_39
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3a

    .line 703
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_3a
    const/4 v2, 0x0

    goto :goto_3b

    .line 708
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_28
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 710
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3b

    .line 711
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 717
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 718
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppSignatureFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 719
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 720
    if-eqz v12, :cond_3c

    const/4 v2, 0x1

    :goto_3d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 721
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 714
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_3b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3c

    .line 720
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_3c
    const/4 v2, 0x0

    goto :goto_3d

    .line 725
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_29
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 727
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3d

    .line 728
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 733
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppSignatureBlackList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;

    move-result-object v12

    .line 734
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 735
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 736
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 731
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_3d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 740
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 742
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3e

    .line 743
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 749
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 750
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setAsManagedApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 751
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 752
    if-eqz v12, :cond_3f

    const/4 v2, 0x1

    :goto_40
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 753
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 746
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_3e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 752
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_3f
    const/4 v2, 0x0

    goto :goto_40

    .line 757
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_2b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 759
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_40

    .line 760
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 766
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 767
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->stopApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 768
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    if-eqz v12, :cond_41

    const/4 v2, 0x1

    :goto_42
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 770
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 763
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_40
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 769
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_41
    const/4 v2, 0x0

    goto :goto_42

    .line 774
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_2c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 776
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_42

    .line 777
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 783
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_43
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 785
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 786
    .local v5, "_arg2":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->startApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    .line 787
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 788
    if-eqz v12, :cond_43

    const/4 v2, 0x1

    :goto_44
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 789
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 780
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_42
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_43

    .line 788
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_43
    const/4 v2, 0x0

    goto :goto_44

    .line 793
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_2d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 795
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_44

    .line 796
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 802
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_45
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_45

    const/4 v4, 0x1

    .line 803
    .local v4, "_arg1":Z
    :goto_46
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationStateList(Landroid/app/enterprise/ContextInfo;Z)[Ljava/lang/String;

    move-result-object v12

    .line 804
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 805
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 806
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 799
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_44
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_45

    .line 802
    :cond_45
    const/4 v4, 0x0

    goto :goto_46

    .line 810
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 812
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_46

    .line 813
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 819
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_47
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v4

    .line 821
    .local v4, "_arg1":[Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_47

    const/4 v5, 0x1

    .line 822
    .local v5, "_arg2":Z
    :goto_48
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationStateList(Landroid/app/enterprise/ContextInfo;[Ljava/lang/String;Z)[Ljava/lang/String;

    move-result-object v12

    .line 823
    .restart local v12    # "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 824
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 825
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 816
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":[Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_46
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_47

    .line 821
    .restart local v4    # "_arg1":[Ljava/lang/String;
    :cond_47
    const/4 v5, 0x0

    goto :goto_48

    .line 829
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":[Ljava/lang/String;
    :sswitch_2f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 831
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_48

    .line 832
    sget-object v2, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 837
    .local v3, "_arg0":Landroid/content/Intent;
    :goto_49
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isIntentDisabled(Landroid/content/Intent;)Z

    move-result v12

    .line 838
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 839
    if-eqz v12, :cond_49

    const/4 v2, 0x1

    :goto_4a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 840
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 835
    .end local v3    # "_arg0":Landroid/content/Intent;
    .end local v12    # "_result":Z
    :cond_48
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/content/Intent;
    goto :goto_49

    .line 839
    .restart local v12    # "_result":Z
    :cond_49
    const/4 v2, 0x0

    goto :goto_4a

    .line 844
    .end local v3    # "_arg0":Landroid/content/Intent;
    .end local v12    # "_result":Z
    :sswitch_30
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 846
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4a

    .line 847
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 853
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 854
    .local v4, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppPackageNameToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 855
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 856
    if-eqz v12, :cond_4b

    const/4 v2, 0x1

    :goto_4c
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 857
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 850
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_4a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4b

    .line 856
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_4b
    const/4 v2, 0x0

    goto :goto_4c

    .line 861
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_31
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 863
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4c

    .line 864
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 870
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 871
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppPackageNameFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 872
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 873
    if-eqz v12, :cond_4d

    const/4 v2, 0x1

    :goto_4e
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 874
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 867
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_4c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4d

    .line 873
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_4d
    const/4 v2, 0x0

    goto :goto_4e

    .line 878
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_32
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 880
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4e

    .line 881
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 886
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4f
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppPackageNamesAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v11

    .line 887
    .local v11, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 888
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 889
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 884
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    :cond_4e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4f

    .line 893
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_33
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 895
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4f

    .line 896
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 902
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_50
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 903
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppPackageNameToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 904
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 905
    if-eqz v12, :cond_50

    const/4 v2, 0x1

    :goto_51
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 906
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 899
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_4f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_50

    .line 905
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_50
    const/4 v2, 0x0

    goto :goto_51

    .line 910
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_34
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 912
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_51

    .line 913
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 919
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_52
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 920
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppPackageNameFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 921
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 922
    if-eqz v12, :cond_52

    const/4 v2, 0x1

    :goto_53
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 923
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 916
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_51
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_52

    .line 922
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_52
    const/4 v2, 0x0

    goto :goto_53

    .line 927
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_35
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 929
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_53

    .line 930
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 935
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_54
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppPackageNamesAllWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v11

    .line 936
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 937
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 938
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 933
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    :cond_53
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_54

    .line 942
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_36
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 944
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_54

    .line 945
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 950
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_55
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppPermissionsAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v11

    .line 951
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 952
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 953
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 948
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    :cond_54
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_55

    .line 957
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_37
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 959
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_55

    .line 960
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 965
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_56
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppSignaturesAllBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v11

    .line 966
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 967
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 968
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 963
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    :cond_55
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_56

    .line 972
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_38
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 974
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_56

    .line 975
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 981
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_57
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 982
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setAppInstallationMode(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v12

    .line 983
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 984
    if-eqz v12, :cond_57

    const/4 v2, 0x1

    :goto_58
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 985
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 978
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :cond_56
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_57

    .line 984
    .restart local v4    # "_arg1":I
    .restart local v12    # "_result":Z
    :cond_57
    const/4 v2, 0x0

    goto :goto_58

    .line 989
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_39
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 991
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_58

    .line 992
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 997
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_59
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppInstallationMode(Landroid/app/enterprise/ContextInfo;)I

    move-result v12

    .line 998
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 999
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1000
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 995
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":I
    :cond_58
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_59

    .line 1004
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1006
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_59

    .line 1007
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1013
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1014
    .restart local v4    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationUninstallationMode(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v12

    .line 1015
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1016
    if-eqz v12, :cond_5a

    const/4 v2, 0x1

    :goto_5b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1017
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1010
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :cond_59
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5a

    .line 1016
    .restart local v4    # "_arg1":I
    .restart local v12    # "_result":Z
    :cond_5a
    const/4 v2, 0x0

    goto :goto_5b

    .line 1021
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_3b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1023
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5b

    .line 1024
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1030
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1031
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToForceStopBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1032
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1033
    if-eqz v12, :cond_5c

    const/4 v2, 0x1

    :goto_5d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1034
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1027
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_5b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5c

    .line 1033
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_5c
    const/4 v2, 0x0

    goto :goto_5d

    .line 1038
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_3c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1040
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1042
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1044
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1046
    .local v5, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1048
    .local v6, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 1050
    .local v7, "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5d

    const/4 v8, 0x1

    .local v8, "_arg5":Z
    :goto_5e
    move-object/from16 v2, p0

    .line 1051
    invoke-virtual/range {v2 .. v8}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationForceStopDisabled(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    move-result v12

    .line 1052
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1053
    if-eqz v12, :cond_5e

    const/4 v2, 0x1

    :goto_5f
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1054
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1050
    .end local v8    # "_arg5":Z
    .end local v12    # "_result":Z
    :cond_5d
    const/4 v8, 0x0

    goto :goto_5e

    .line 1053
    .restart local v8    # "_arg5":Z
    .restart local v12    # "_result":Z
    :cond_5e
    const/4 v2, 0x0

    goto :goto_5f

    .line 1058
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":Ljava/lang/String;
    .end local v7    # "_arg4":Ljava/lang/String;
    .end local v8    # "_arg5":Z
    .end local v12    # "_result":Z
    :sswitch_3d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1060
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5f

    .line 1061
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1066
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_60
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromWidgetWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1067
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1068
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1069
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1064
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_5f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_60

    .line 1073
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1075
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_60

    .line 1076
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1082
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_61
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1083
    .local v4, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isWidgetAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1084
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1085
    if-eqz v12, :cond_61

    const/4 v2, 0x1

    :goto_62
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1086
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1079
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_60
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_61

    .line 1085
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_61
    const/4 v2, 0x0

    goto :goto_62

    .line 1090
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_3f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1092
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_62

    .line 1093
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1099
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_63
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1100
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1101
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1102
    if-eqz v12, :cond_63

    const/4 v2, 0x1

    :goto_64
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1103
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1096
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_62
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_63

    .line 1102
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_63
    const/4 v2, 0x0

    goto :goto_64

    .line 1107
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_40
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1109
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_64

    .line 1110
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1116
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_65
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1117
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1118
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1119
    if-eqz v12, :cond_65

    const/4 v2, 0x1

    :goto_66
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1113
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_64
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_65

    .line 1119
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_65
    const/4 v2, 0x0

    goto :goto_66

    .line 1124
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_41
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1126
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_66

    .line 1127
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1133
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_67
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_67

    const/4 v4, 0x1

    .line 1134
    .local v4, "_arg1":Z
    :goto_68
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppNotificationBlackList(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;

    move-result-object v18

    .line 1135
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1136
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1137
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1130
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_66
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_67

    .line 1133
    :cond_67
    const/4 v4, 0x0

    goto :goto_68

    .line 1141
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_42
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1143
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_68

    .line 1144
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1150
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_69
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1151
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1152
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1153
    if-eqz v12, :cond_69

    const/4 v2, 0x1

    :goto_6a
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1154
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1147
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_68
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_69

    .line 1153
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_69
    const/4 v2, 0x0

    goto :goto_6a

    .line 1158
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_43
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1160
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6a

    .line 1161
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1167
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1168
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1169
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1170
    if-eqz v12, :cond_6b

    const/4 v2, 0x1

    :goto_6c
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1171
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1164
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_6a
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6b

    .line 1170
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_6b
    const/4 v2, 0x0

    goto :goto_6c

    .line 1175
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_44
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1177
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6c

    .line 1178
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1184
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6d

    const/4 v4, 0x1

    .line 1185
    .restart local v4    # "_arg1":Z
    :goto_6e
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppNotificationWhiteList(Landroid/app/enterprise/ContextInfo;Z)Ljava/util/List;

    move-result-object v18

    .line 1186
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1187
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1188
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1181
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_6c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6d

    .line 1184
    :cond_6d
    const/4 v4, 0x0

    goto :goto_6e

    .line 1192
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_45
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1194
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6e

    .line 1195
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1201
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1202
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationNotificationMode(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v12

    .line 1203
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1204
    if-eqz v12, :cond_6f

    const/4 v2, 0x1

    :goto_70
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1205
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1198
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :cond_6e
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6f

    .line 1204
    .restart local v4    # "_arg1":I
    .restart local v12    # "_result":Z
    :cond_6f
    const/4 v2, 0x0

    goto :goto_70

    .line 1209
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_46
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1211
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_70

    .line 1212
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1218
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_71
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_71

    const/4 v4, 0x1

    .line 1219
    .local v4, "_arg1":Z
    :goto_72
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationNotificationMode(Landroid/app/enterprise/ContextInfo;Z)I

    move-result v12

    .line 1220
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1221
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1222
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1215
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":I
    :cond_70
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_71

    .line 1218
    :cond_71
    const/4 v4, 0x0

    goto :goto_72

    .line 1226
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_47
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1228
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 1229
    .local v3, "_arg0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationNotificationModeAsUser(I)I

    move-result v12

    .line 1230
    .restart local v12    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1231
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1232
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1236
    .end local v3    # "_arg0":I
    .end local v12    # "_result":I
    :sswitch_48
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1238
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1240
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1241
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isStatusBarNotificationAllowedAsUser(Ljava/lang/String;I)Z

    move-result v12

    .line 1242
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1243
    if-eqz v12, :cond_72

    const/4 v2, 0x1

    :goto_73
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1244
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1243
    :cond_72
    const/4 v2, 0x0

    goto :goto_73

    .line 1248
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_49
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1250
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_73

    .line 1251
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1257
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_74
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1259
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1260
    .restart local v5    # "_arg2":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    .line 1261
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1262
    if-eqz v12, :cond_74

    const/4 v2, 0x1

    :goto_75
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1263
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1254
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_73
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_74

    .line 1262
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_74
    const/4 v2, 0x0

    goto :goto_75

    .line 1267
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_4a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1269
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_75

    .line 1270
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1276
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_76
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1278
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1279
    .restart local v5    # "_arg2":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->deleteHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    .line 1280
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1281
    if-eqz v12, :cond_76

    const/4 v2, 0x1

    :goto_77
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1282
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1273
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_75
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_76

    .line 1281
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_76
    const/4 v2, 0x0

    goto :goto_77

    .line 1286
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_4b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1288
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_77

    .line 1289
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1295
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_78
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1297
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_78

    .line 1298
    sget-object v2, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelFileDescriptor;

    .line 1303
    .local v5, "_arg2":Landroid/os/ParcelFileDescriptor;
    :goto_79
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->restoreApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I

    move-result v12

    .line 1304
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1305
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1306
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1292
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    .end local v12    # "_result":I
    :cond_77
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_78

    .line 1301
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_78
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    goto :goto_79

    .line 1310
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    :sswitch_4c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1312
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_79

    .line 1313
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1319
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1321
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7a

    .line 1322
    sget-object v2, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/ParcelFileDescriptor;

    .line 1327
    .restart local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    :goto_7b
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->backupApplicationData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)I

    move-result v12

    .line 1328
    .restart local v12    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1329
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1330
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1316
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    .end local v12    # "_result":I
    :cond_79
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7a

    .line 1325
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_7a
    const/4 v5, 0x0

    .restart local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    goto :goto_7b

    .line 1334
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Landroid/os/ParcelFileDescriptor;
    :sswitch_4d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1336
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7b

    .line 1337
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1343
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1344
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAllWidgets(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v12

    .line 1345
    .local v12, "_result":Ljava/util/Map;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1346
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1347
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1340
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Ljava/util/Map;
    :cond_7b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7c

    .line 1351
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1353
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7c

    .line 1354
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1359
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7d
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationUninstallationMode(Landroid/app/enterprise/ContextInfo;)I

    move-result v12

    .line 1360
    .local v12, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1361
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 1362
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1357
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":I
    :cond_7c
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7d

    .line 1366
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1368
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7d

    .line 1369
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1375
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_7e

    const/4 v4, 0x1

    .line 1376
    .local v4, "_arg1":Z
    :goto_7f
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setAppInstallToSdCard(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v12

    .line 1377
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1378
    if-eqz v12, :cond_7f

    const/4 v2, 0x1

    :goto_80
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1379
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1372
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":Z
    :cond_7d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7e

    .line 1375
    :cond_7e
    const/4 v4, 0x0

    goto :goto_7f

    .line 1378
    .restart local v4    # "_arg1":Z
    .restart local v12    # "_result":Z
    :cond_7f
    const/4 v2, 0x0

    goto :goto_80

    .line 1383
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":Z
    :sswitch_50
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1385
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_80

    .line 1386
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1391
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_81
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppInstallToSdCard(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 1392
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1393
    if-eqz v12, :cond_81

    const/4 v2, 0x1

    :goto_82
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1394
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1389
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_80
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_81

    .line 1393
    .restart local v12    # "_result":Z
    :cond_81
    const/4 v2, 0x0

    goto :goto_82

    .line 1398
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_51
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1400
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_82

    .line 1401
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1406
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_83
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromForceStopBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1407
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1408
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1409
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1404
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_82
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_83

    .line 1413
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_52
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1415
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_83

    .line 1416
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1422
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_84
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1423
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToForceStopWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1424
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1425
    if-eqz v12, :cond_84

    const/4 v2, 0x1

    :goto_85
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1426
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1419
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_83
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_84

    .line 1425
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_84
    const/4 v2, 0x0

    goto :goto_85

    .line 1430
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_53
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1432
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_85

    .line 1433
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1438
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_86
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromForceStopWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1439
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1440
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1441
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1436
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_85
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_86

    .line 1445
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_54
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1447
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_86

    .line 1448
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1453
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_87
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromWidgetBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1454
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1455
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1456
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1451
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_86
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_87

    .line 1460
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_55
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1462
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_87

    .line 1463
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1469
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_88
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1470
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToWidgetWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1471
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1472
    if-eqz v12, :cond_88

    const/4 v2, 0x1

    :goto_89
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1473
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1466
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_87
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_88

    .line 1472
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_88
    const/4 v2, 0x0

    goto :goto_89

    .line 1477
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_56
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1479
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_89

    .line 1480
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1486
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1487
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromWidgetWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1488
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1489
    if-eqz v12, :cond_8a

    const/4 v2, 0x1

    :goto_8b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1490
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1483
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_89
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8a

    .line 1489
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_8a
    const/4 v2, 0x0

    goto :goto_8b

    .line 1494
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_57
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1496
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_8b

    .line 1497
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1503
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1504
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToWidgetBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1505
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1506
    if-eqz v12, :cond_8c

    const/4 v2, 0x1

    :goto_8d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1507
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1500
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_8b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8c

    .line 1506
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_8c
    const/4 v2, 0x0

    goto :goto_8d

    .line 1511
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_58
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1513
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_8d

    .line 1514
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1520
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1521
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromWidgetBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1522
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1523
    if-eqz v12, :cond_8e

    const/4 v2, 0x1

    :goto_8f
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1524
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1517
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_8d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8e

    .line 1523
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_8e
    const/4 v2, 0x0

    goto :goto_8f

    .line 1528
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_59
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1530
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_8f

    .line 1531
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1537
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_90
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1538
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromForceStopWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1539
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1540
    if-eqz v12, :cond_90

    const/4 v2, 0x1

    :goto_91
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1541
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1534
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_8f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_90

    .line 1540
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_90
    const/4 v2, 0x0

    goto :goto_91

    .line 1545
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_5a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1547
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_91

    .line 1548
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1554
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_92
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1555
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromForceStopBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1556
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1557
    if-eqz v12, :cond_92

    const/4 v2, 0x1

    :goto_93
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1558
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1551
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_91
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_92

    .line 1557
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_92
    const/4 v2, 0x0

    goto :goto_93

    .line 1562
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_5b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1564
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_93

    .line 1565
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1571
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_94
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1573
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_94

    const/4 v5, 0x1

    .line 1574
    .local v5, "_arg2":Z
    :goto_95
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->enableRevocationCheck(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v12

    .line 1575
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1576
    if-eqz v12, :cond_95

    const/4 v2, 0x1

    :goto_96
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1577
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1568
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_93
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_94

    .line 1573
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_94
    const/4 v5, 0x0

    goto :goto_95

    .line 1576
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_95
    const/4 v2, 0x0

    goto :goto_96

    .line 1581
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_5c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1583
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_96

    .line 1584
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1590
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_97
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1591
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isRevocationCheckEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1592
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1593
    if-eqz v12, :cond_97

    const/4 v2, 0x1

    :goto_98
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1594
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1587
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_96
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_97

    .line 1593
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_97
    const/4 v2, 0x0

    goto :goto_98

    .line 1598
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_5d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1600
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_98

    .line 1601
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1607
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_99
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1609
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_99

    const/4 v5, 0x1

    .line 1610
    .restart local v5    # "_arg2":Z
    :goto_9a
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->enableOcspCheck(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v12

    .line 1611
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1612
    if-eqz v12, :cond_9a

    const/4 v2, 0x1

    :goto_9b
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1613
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1604
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_98
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_99

    .line 1609
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_99
    const/4 v5, 0x0

    goto :goto_9a

    .line 1612
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_9a
    const/4 v2, 0x0

    goto :goto_9b

    .line 1617
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_5e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1619
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9b

    .line 1620
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1626
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1627
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isOcspCheckEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1628
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1629
    if-eqz v12, :cond_9c

    const/4 v2, 0x1

    :goto_9d
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1630
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1623
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_9b
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9c

    .line 1629
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_9c
    const/4 v2, 0x0

    goto :goto_9d

    .line 1634
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_5f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1636
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9d

    .line 1637
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1643
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1644
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addAppSignatureToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1645
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1646
    if-eqz v12, :cond_9e

    const/4 v2, 0x1

    :goto_9f
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1647
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1640
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_9d
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9e

    .line 1646
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_9e
    const/4 v2, 0x0

    goto :goto_9f

    .line 1651
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_60
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1653
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9f

    .line 1654
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1660
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1661
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removeAppSignatureFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1662
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1663
    if-eqz v12, :cond_a0

    const/4 v2, 0x1

    :goto_a1
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1664
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1657
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_9f
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a0

    .line 1663
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_a0
    const/4 v2, 0x0

    goto :goto_a1

    .line 1668
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_61
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1670
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a1

    .line 1671
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1676
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppSignaturesWhiteList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;

    move-result-object v12

    .line 1677
    .local v12, "_result":[Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1678
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 1679
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1674
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":[Ljava/lang/String;
    :cond_a1
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a2

    .line 1683
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_62
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1685
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a2

    .line 1686
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1691
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAppSignaturesAllWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v11

    .line 1692
    .restart local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1693
    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1694
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1689
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v11    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/AppControlInfo;>;"
    :cond_a2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a3

    .line 1698
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_63
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1700
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a3

    .line 1701
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1707
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1708
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackageToInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1709
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1710
    if-eqz v12, :cond_a4

    const/4 v2, 0x1

    :goto_a5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1711
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1704
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_a3
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a4

    .line 1710
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_a4
    const/4 v2, 0x0

    goto :goto_a5

    .line 1715
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_64
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1717
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a5

    .line 1718
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1723
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a6
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1724
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1725
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1726
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1721
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a5
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a6

    .line 1730
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_65
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1732
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a6

    .line 1733
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1739
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1740
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isPackageInInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1741
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1742
    if-eqz v12, :cond_a7

    const/4 v2, 0x1

    :goto_a8
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1743
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1736
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_a6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a7

    .line 1742
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_a7
    const/4 v2, 0x0

    goto :goto_a8

    .line 1747
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_66
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1749
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_a8

    .line 1750
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1756
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1757
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackageFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 1758
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1759
    if-eqz v12, :cond_a9

    const/4 v2, 0x1

    :goto_aa
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1760
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1753
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_a8
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a9

    .line 1759
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_a9
    const/4 v2, 0x0

    goto :goto_aa

    .line 1764
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_67
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1766
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_aa

    .line 1767
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1773
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ab
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1774
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1775
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1776
    if-eqz v12, :cond_ab

    const/4 v2, 0x1

    :goto_ac
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1777
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1770
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_aa
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ab

    .line 1776
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_ab
    const/4 v2, 0x0

    goto :goto_ac

    .line 1781
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_68
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1783
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ac

    .line 1784
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1790
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ad
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1791
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1792
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1793
    if-eqz v12, :cond_ad

    const/4 v2, 0x1

    :goto_ae
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1794
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1787
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_ac
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ad

    .line 1793
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_ad
    const/4 v2, 0x0

    goto :goto_ae

    .line 1798
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_69
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1800
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ae

    .line 1801
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1806
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_af
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1807
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1808
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1809
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1804
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_ae
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_af

    .line 1813
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1815
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_af

    .line 1816
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1822
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b0
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1823
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1824
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1825
    if-eqz v12, :cond_b0

    const/4 v2, 0x1

    :goto_b1
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1826
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1819
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_af
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b0

    .line 1825
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_b0
    const/4 v2, 0x0

    goto :goto_b1

    .line 1830
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_6b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1832
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b1

    .line 1833
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1838
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1839
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1840
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1841
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1836
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_b1
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b2

    .line 1845
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1847
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b2

    .line 1848
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1854
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1855
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1856
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1857
    if-eqz v12, :cond_b3

    const/4 v2, 0x1

    :goto_b4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1858
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1851
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_b2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b3

    .line 1857
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_b3
    const/4 v2, 0x0

    goto :goto_b4

    .line 1862
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_6d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1864
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1866
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1868
    .local v4, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b4

    const/4 v5, 0x1

    .line 1869
    .restart local v5    # "_arg2":Z
    :goto_b5
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationClearDataDisabled(Ljava/lang/String;IZ)Z

    move-result v12

    .line 1870
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1871
    if-eqz v12, :cond_b5

    const/4 v2, 0x1

    :goto_b6
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1872
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1868
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_b4
    const/4 v5, 0x0

    goto :goto_b5

    .line 1871
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_b5
    const/4 v2, 0x0

    goto :goto_b6

    .line 1876
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_6e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1878
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b6

    .line 1879
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1885
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1886
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1887
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1888
    if-eqz v12, :cond_b7

    const/4 v2, 0x1

    :goto_b8
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1889
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1882
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_b6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b7

    .line 1888
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_b7
    const/4 v2, 0x0

    goto :goto_b8

    .line 1893
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_6f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1895
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_b8

    .line 1896
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1902
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1903
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1904
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1905
    if-eqz v12, :cond_b9

    const/4 v2, 0x1

    :goto_ba
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1906
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1899
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_b8
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b9

    .line 1905
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_b9
    const/4 v2, 0x0

    goto :goto_ba

    .line 1910
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_70
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1912
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ba

    .line 1913
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1918
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bb
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1919
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1920
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1921
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1916
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_ba
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bb

    .line 1925
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_71
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1927
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_bb

    .line 1928
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1934
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bc
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1935
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1936
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1937
    if-eqz v12, :cond_bc

    const/4 v2, 0x1

    :goto_bd
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1938
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1931
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_bb
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bc

    .line 1937
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_bc
    const/4 v2, 0x0

    goto :goto_bd

    .line 1942
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_72
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1944
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_bd

    .line 1945
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1950
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_be
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 1951
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1952
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1953
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1948
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_bd
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_be

    .line 1957
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_73
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1959
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_be

    .line 1960
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1966
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_bf
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 1967
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 1968
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1969
    if-eqz v12, :cond_bf

    const/4 v2, 0x1

    :goto_c0
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1970
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1963
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_be
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_bf

    .line 1969
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_bf
    const/4 v2, 0x0

    goto :goto_c0

    .line 1974
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_74
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1976
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1978
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 1980
    .restart local v4    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c0

    const/4 v5, 0x1

    .line 1981
    .restart local v5    # "_arg2":Z
    :goto_c1
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationClearCacheDisabled(Ljava/lang/String;IZ)Z

    move-result v12

    .line 1982
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1983
    if-eqz v12, :cond_c1

    const/4 v2, 0x1

    :goto_c2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1984
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1980
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_c0
    const/4 v5, 0x0

    goto :goto_c1

    .line 1983
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_c1
    const/4 v2, 0x0

    goto :goto_c2

    .line 1988
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_75
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1990
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c2

    .line 1991
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 1997
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 1999
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2000
    .local v5, "_arg2":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->changeApplicationName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    .line 2001
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2002
    if-eqz v12, :cond_c3

    const/4 v2, 0x1

    :goto_c4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2003
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1994
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_c2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c3

    .line 2002
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_c3
    const/4 v2, 0x0

    goto :goto_c4

    .line 2007
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_76
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2009
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2011
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 2012
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationNameFromDb(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    .line 2013
    .local v12, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2014
    move-object/from16 v0, p3

    invoke-virtual {v0, v12}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 2015
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2019
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Ljava/lang/String;
    :sswitch_77
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2021
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c4

    .line 2022
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2028
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c5

    .line 2029
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 2035
    .local v4, "_arg1":Landroid/content/ComponentName;
    :goto_c6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c6

    const/4 v5, 0x1

    .line 2036
    .local v5, "_arg2":Z
    :goto_c7
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->setApplicationComponentState(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)Z

    move-result v12

    .line 2037
    .local v12, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2038
    if-eqz v12, :cond_c7

    const/4 v2, 0x1

    :goto_c8
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2039
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2025
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :cond_c4
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c5

    .line 2032
    :cond_c5
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    goto :goto_c6

    .line 2035
    :cond_c6
    const/4 v5, 0x0

    goto :goto_c7

    .line 2038
    .restart local v5    # "_arg2":Z
    .restart local v12    # "_result":Z
    :cond_c7
    const/4 v2, 0x0

    goto :goto_c8

    .line 2043
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v5    # "_arg2":Z
    .end local v12    # "_result":Z
    :sswitch_78
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2045
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c8

    .line 2046
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2052
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_c9

    .line 2053
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 2058
    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    :goto_ca
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getApplicationComponentState(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z

    move-result v12

    .line 2059
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2060
    if-eqz v12, :cond_ca

    const/4 v2, 0x1

    :goto_cb
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2061
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2049
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v12    # "_result":Z
    :cond_c8
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c9

    .line 2056
    :cond_c9
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    goto :goto_ca

    .line 2060
    .restart local v12    # "_result":Z
    :cond_ca
    const/4 v2, 0x0

    goto :goto_cb

    .line 2065
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v12    # "_result":Z
    :sswitch_79
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2067
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_cb

    .line 2068
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2074
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_cc
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2076
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 2078
    .local v5, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .line 2080
    .local v6, "_arg3":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_cc

    const/4 v7, 0x1

    .line 2082
    .local v7, "_arg4":Z
    :goto_cd
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .local v8, "_arg5":I
    move-object/from16 v2, p0

    .line 2083
    invoke-virtual/range {v2 .. v8}, Landroid/app/enterprise/IApplicationPolicy$Stub;->writeData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;[BZI)Z

    move-result v12

    .line 2084
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2085
    if-eqz v12, :cond_cd

    const/4 v2, 0x1

    :goto_ce
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2086
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2071
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":[B
    .end local v7    # "_arg4":Z
    .end local v8    # "_arg5":I
    .end local v12    # "_result":Z
    :cond_cb
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_cc

    .line 2080
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v5    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_arg3":[B
    :cond_cc
    const/4 v7, 0x0

    goto :goto_cd

    .line 2085
    .restart local v7    # "_arg4":Z
    .restart local v8    # "_arg5":I
    .restart local v12    # "_result":Z
    :cond_cd
    const/4 v2, 0x0

    goto :goto_ce

    .line 2090
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Ljava/lang/String;
    .end local v6    # "_arg3":[B
    .end local v7    # "_arg4":Z
    .end local v8    # "_arg5":I
    .end local v12    # "_result":Z
    :sswitch_7a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2092
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ce

    .line 2093
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2099
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_cf
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2100
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2101
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2102
    if-eqz v12, :cond_cf

    const/4 v2, 0x1

    :goto_d0
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2103
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2096
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_ce
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_cf

    .line 2102
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_cf
    const/4 v2, 0x0

    goto :goto_d0

    .line 2107
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_7b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2109
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d0

    .line 2110
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2116
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2117
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2118
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2119
    if-eqz v12, :cond_d1

    const/4 v2, 0x1

    :goto_d2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2120
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2113
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_d0
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d1

    .line 2119
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_d1
    const/4 v2, 0x0

    goto :goto_d2

    .line 2124
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_7c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2126
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d2

    .line 2127
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2132
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 2133
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2134
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2135
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2130
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_d2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d3

    .line 2139
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2141
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d3

    .line 2142
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2148
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2149
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2150
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2151
    if-eqz v12, :cond_d4

    const/4 v2, 0x1

    :goto_d5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2152
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2145
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_d3
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d4

    .line 2151
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_d4
    const/4 v2, 0x0

    goto :goto_d5

    .line 2156
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_7e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2158
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d5

    .line 2159
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2164
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d6
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 2165
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2166
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2167
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2162
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_d5
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d6

    .line 2171
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2173
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d6

    .line 2174
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2180
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2181
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2182
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2183
    if-eqz v12, :cond_d7

    const/4 v2, 0x1

    :goto_d8
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2184
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2177
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_d6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d7

    .line 2183
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_d7
    const/4 v2, 0x0

    goto :goto_d8

    .line 2188
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_80
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2190
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2192
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_d8

    const/4 v4, 0x1

    .line 2193
    .local v4, "_arg1":Z
    :goto_d9
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isPackageUpdateAllowed(Ljava/lang/String;Z)Z

    move-result v12

    .line 2194
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2195
    if-eqz v12, :cond_d9

    const/4 v2, 0x1

    :goto_da
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2196
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2192
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":Z
    :cond_d8
    const/4 v4, 0x0

    goto :goto_d9

    .line 2195
    .restart local v4    # "_arg1":Z
    .restart local v12    # "_result":Z
    :cond_d9
    const/4 v2, 0x0

    goto :goto_da

    .line 2200
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":Z
    .end local v12    # "_result":Z
    :sswitch_81
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2202
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_da

    .line 2203
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2208
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_db
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearDisableUpdateBlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 2209
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2210
    if-eqz v12, :cond_db

    const/4 v2, 0x1

    :goto_dc
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2211
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2206
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_da
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_db

    .line 2210
    .restart local v12    # "_result":Z
    :cond_db
    const/4 v2, 0x0

    goto :goto_dc

    .line 2215
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_82
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2217
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_dc

    .line 2218
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2223
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_dd
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearDisableUpdateWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 2224
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2225
    if-eqz v12, :cond_dd

    const/4 v2, 0x1

    :goto_de
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2226
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2221
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_dc
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_dd

    .line 2225
    .restart local v12    # "_result":Z
    :cond_dd
    const/4 v2, 0x0

    goto :goto_de

    .line 2230
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_83
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2232
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_de

    .line 2233
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2239
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_df
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2240
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToPreventStartBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Ljava/util/List;

    move-result-object v18

    .line 2241
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2242
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2243
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2236
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_de
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_df

    .line 2247
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_84
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2249
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_df

    .line 2250
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2255
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e0
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromPreventStartBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 2256
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2257
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2258
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2253
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_df
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e0

    .line 2262
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_85
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2264
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e0

    .line 2265
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2271
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2272
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromPreventStartBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2273
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2274
    if-eqz v12, :cond_e1

    const/4 v2, 0x1

    :goto_e2
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2275
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2268
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_e0
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e1

    .line 2274
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_e1
    const/4 v2, 0x0

    goto :goto_e2

    .line 2279
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_86
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2281
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e2

    .line 2282
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2287
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearPreventStartBlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 2288
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2289
    if-eqz v12, :cond_e3

    const/4 v2, 0x1

    :goto_e4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2290
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2285
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_e2
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e3

    .line 2289
    .restart local v12    # "_result":Z
    :cond_e3
    const/4 v2, 0x0

    goto :goto_e4

    .line 2294
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_87
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2296
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2298
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 2299
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isApplicationStartDisabledAsUser(Ljava/lang/String;I)Z

    move-result v12

    .line 2300
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2301
    if-eqz v12, :cond_e4

    const/4 v2, 0x1

    :goto_e5
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2302
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2301
    :cond_e4
    const/4 v2, 0x0

    goto :goto_e5

    .line 2306
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_88
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2308
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e5

    .line 2309
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2315
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2316
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2317
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2318
    if-eqz v12, :cond_e6

    const/4 v2, 0x1

    :goto_e7
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2319
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2312
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_e5
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e6

    .line 2318
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_e6
    const/4 v2, 0x0

    goto :goto_e7

    .line 2323
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_89
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2325
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e7

    .line 2326
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2332
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_e8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2333
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2334
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2335
    if-eqz v12, :cond_e8

    const/4 v2, 0x1

    :goto_e9
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2336
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2329
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_e7
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_e8

    .line 2335
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_e8
    const/4 v2, 0x0

    goto :goto_e9

    .line 2340
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_8a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2342
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_e9

    .line 2343
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2348
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ea
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 2349
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2350
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2351
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2346
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_e9
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ea

    .line 2355
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2357
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ea

    .line 2358
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2364
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_eb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2365
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addPackagesToDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2366
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2367
    if-eqz v12, :cond_eb

    const/4 v2, 0x1

    :goto_ec
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2368
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2361
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_ea
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_eb

    .line 2367
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_eb
    const/4 v2, 0x0

    goto :goto_ec

    .line 2372
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_8c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2374
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ec

    .line 2375
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2381
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ed
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v9

    .line 2382
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v9}, Landroid/app/enterprise/IApplicationPolicy$Stub;->removePackagesFromDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v12

    .line 2383
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2384
    if-eqz v12, :cond_ed

    const/4 v2, 0x1

    :goto_ee
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2385
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2378
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :cond_ec
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ed

    .line 2384
    .restart local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v12    # "_result":Z
    :cond_ed
    const/4 v2, 0x0

    goto :goto_ee

    .line 2389
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v12    # "_result":Z
    :sswitch_8d
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2391
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ee

    .line 2392
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2397
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_ef
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getPackagesFromDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v18

    .line 2398
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2399
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2400
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2395
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_ee
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_ef

    .line 2404
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8e
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2406
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ef

    .line 2407
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2412
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f0
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearDisableClipboardBlackList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 2413
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2414
    if-eqz v12, :cond_f0

    const/4 v2, 0x1

    :goto_f1
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2415
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2410
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_ef
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f0

    .line 2414
    .restart local v12    # "_result":Z
    :cond_f0
    const/4 v2, 0x0

    goto :goto_f1

    .line 2419
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_8f
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2421
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f1

    .line 2422
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2427
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f2
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearDisableClipboardWhiteList(Landroid/app/enterprise/ContextInfo;)Z

    move-result v12

    .line 2428
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2429
    if-eqz v12, :cond_f2

    const/4 v2, 0x1

    :goto_f3
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2430
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2425
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :cond_f1
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f2

    .line 2429
    .restart local v12    # "_result":Z
    :cond_f2
    const/4 v2, 0x0

    goto :goto_f3

    .line 2434
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v12    # "_result":Z
    :sswitch_90
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2436
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 2438
    .local v3, "_arg0":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 2439
    .restart local v4    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isPackageClipboardAllowed(Ljava/lang/String;I)Z

    move-result v12

    .line 2440
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2441
    if-eqz v12, :cond_f3

    const/4 v2, 0x1

    :goto_f4
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2442
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2441
    :cond_f3
    const/4 v2, 0x0

    goto :goto_f4

    .line 2446
    .end local v3    # "_arg0":Ljava/lang/String;
    .end local v4    # "_arg1":I
    .end local v12    # "_result":Z
    :sswitch_91
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2448
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f4

    .line 2449
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2455
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2457
    .local v4, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f5

    const/4 v5, 0x1

    .line 2458
    .local v5, "_arg2":Z
    :goto_f6
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getHomeShortcuts(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Ljava/util/List;

    move-result-object v17

    .line 2459
    .local v17, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2460
    move-object/from16 v0, p3

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2461
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2452
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v17    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/content/ComponentName;>;"
    :cond_f4
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f5

    .line 2457
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_f5
    const/4 v5, 0x0

    goto :goto_f6

    .line 2465
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    :sswitch_92
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2467
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f6

    .line 2468
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2474
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2476
    .restart local v4    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f7

    const/4 v5, 0x1

    .line 2478
    .restart local v5    # "_arg2":Z
    :goto_f8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;

    move-result-object v6

    .line 2479
    .local v6, "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/app/enterprise/IApplicationPolicy$Stub;->installAppWithCallback(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ZLcom/sec/enterprise/knox/IEnterpriseContainerCallback;)Z

    move-result v12

    .line 2480
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2481
    if-eqz v12, :cond_f8

    const/4 v2, 0x1

    :goto_f9
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2482
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2471
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v6    # "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v12    # "_result":Z
    :cond_f6
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f7

    .line 2476
    .restart local v4    # "_arg1":Ljava/lang/String;
    :cond_f7
    const/4 v5, 0x0

    goto :goto_f8

    .line 2481
    .restart local v5    # "_arg2":Z
    .restart local v6    # "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .restart local v12    # "_result":Z
    :cond_f8
    const/4 v2, 0x0

    goto :goto_f9

    .line 2486
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v5    # "_arg2":Z
    .end local v6    # "_arg3":Lcom/sec/enterprise/knox/IEnterpriseContainerCallback;
    .end local v12    # "_result":Z
    :sswitch_93
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2488
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_f9

    .line 2489
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    .line 2495
    .local v3, "_arg0":Landroid/content/ComponentName;
    :goto_fa
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 2496
    .local v4, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->updateWidgetStatus(Landroid/content/ComponentName;I)V

    .line 2497
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2498
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2492
    .end local v3    # "_arg0":Landroid/content/ComponentName;
    .end local v4    # "_arg1":I
    :cond_f9
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/content/ComponentName;
    goto :goto_fa

    .line 2502
    .end local v3    # "_arg0":Landroid/content/ComponentName;
    :sswitch_94
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2504
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_fa

    .line 2505
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2511
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_fb
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2512
    .local v4, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v16

    .line 2513
    .local v16, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2514
    move-object/from16 v0, p3

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 2515
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2508
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v16    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    :cond_fa
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_fb

    .line 2519
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_95
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2521
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_fb

    .line 2522
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2528
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_fc
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2530
    .restart local v4    # "_arg1":Ljava/lang/String;
    sget-object v2, Landroid/app/enterprise/UsbDeviceConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v10

    .line 2531
    .local v10, "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v10}, Landroid/app/enterprise/IApplicationPolicy$Stub;->addUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z

    move-result v12

    .line 2532
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2533
    if-eqz v12, :cond_fc

    const/4 v2, 0x1

    :goto_fd
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2534
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2525
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v10    # "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    .end local v12    # "_result":Z
    :cond_fb
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_fc

    .line 2533
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v10    # "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    .restart local v12    # "_result":Z
    :cond_fc
    const/4 v2, 0x0

    goto :goto_fd

    .line 2538
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v10    # "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/UsbDeviceConfig;>;"
    .end local v12    # "_result":Z
    :sswitch_96
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2540
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_fd

    .line 2541
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2547
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_fe
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2548
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->clearUsbDevicesForDefaultAccess(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 2549
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2550
    if-eqz v12, :cond_fe

    const/4 v2, 0x1

    :goto_ff
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2551
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2544
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_fd
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_fe

    .line 2550
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_fe
    const/4 v2, 0x0

    goto :goto_ff

    .line 2555
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_97
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2557
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_ff

    .line 2558
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2564
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_100
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 2565
    .restart local v4    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->installExistingApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v12

    .line 2566
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2567
    if-eqz v12, :cond_100

    const/4 v2, 0x1

    :goto_101
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2568
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2561
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :cond_ff
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_100

    .line 2567
    .restart local v4    # "_arg1":Ljava/lang/String;
    .restart local v12    # "_result":Z
    :cond_100
    const/4 v2, 0x0

    goto :goto_101

    .line 2572
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Ljava/lang/String;
    .end local v12    # "_result":Z
    :sswitch_98
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2574
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2576
    .local v3, "_arg0":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 2578
    .local v4, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 2579
    .local v5, "_arg2":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->updateApplicationTable(III)Z

    move-result v12

    .line 2580
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2581
    if-eqz v12, :cond_101

    const/4 v2, 0x1

    :goto_102
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2582
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2581
    :cond_101
    const/4 v2, 0x0

    goto :goto_102

    .line 2586
    .end local v3    # "_arg0":I
    .end local v4    # "_arg1":I
    .end local v5    # "_arg2":I
    .end local v12    # "_result":Z
    :sswitch_99
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2588
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 2589
    .restart local v3    # "_arg0":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getDisabledPackages(I)Ljava/util/List;

    move-result-object v18

    .line 2590
    .restart local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2591
    move-object/from16 v0, p3

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 2592
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2596
    .end local v3    # "_arg0":I
    .end local v18    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_9a
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2598
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_102

    .line 2599
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2605
    .local v3, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_103
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_103

    .line 2606
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 2612
    .local v4, "_arg1":Landroid/content/ComponentName;
    :goto_104
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_104

    const/4 v5, 0x1

    .line 2613
    .local v5, "_arg2":Z
    :goto_105
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5}, Landroid/app/enterprise/IApplicationPolicy$Stub;->requestMetaKeyEvent(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V

    .line 2614
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2615
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2602
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v5    # "_arg2":Z
    :cond_102
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_103

    .line 2609
    :cond_103
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    goto :goto_104

    .line 2612
    :cond_104
    const/4 v5, 0x0

    goto :goto_105

    .line 2619
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    :sswitch_9b
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2621
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_105

    .line 2622
    sget-object v2, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/ContextInfo;

    .line 2628
    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_106
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_106

    .line 2629
    sget-object v2, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ComponentName;

    .line 2634
    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    :goto_107
    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4}, Landroid/app/enterprise/IApplicationPolicy$Stub;->isMetaKeyEventRequested(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z

    move-result v12

    .line 2635
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2636
    if-eqz v12, :cond_107

    const/4 v2, 0x1

    :goto_108
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2637
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2625
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v12    # "_result":Z
    :cond_105
    const/4 v3, 0x0

    .restart local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_106

    .line 2632
    :cond_106
    const/4 v4, 0x0

    .restart local v4    # "_arg1":Landroid/content/ComponentName;
    goto :goto_107

    .line 2636
    .restart local v12    # "_result":Z
    :cond_107
    const/4 v2, 0x0

    goto :goto_108

    .line 2641
    .end local v3    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_arg1":Landroid/content/ComponentName;
    .end local v12    # "_result":Z
    :sswitch_9c
    const-string v2, "android.app.enterprise.IApplicationPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 2642
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->getAddHomeShorcutRequested()Z

    move-result v12

    .line 2643
    .restart local v12    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 2644
    if-eqz v12, :cond_108

    const/4 v2, 0x1

    :goto_109
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 2645
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 2644
    :cond_108
    const/4 v2, 0x0

    goto :goto_109

    .line 44
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5c -> :sswitch_5c
        0x5d -> :sswitch_5d
        0x5e -> :sswitch_5e
        0x5f -> :sswitch_5f
        0x60 -> :sswitch_60
        0x61 -> :sswitch_61
        0x62 -> :sswitch_62
        0x63 -> :sswitch_63
        0x64 -> :sswitch_64
        0x65 -> :sswitch_65
        0x66 -> :sswitch_66
        0x67 -> :sswitch_67
        0x68 -> :sswitch_68
        0x69 -> :sswitch_69
        0x6a -> :sswitch_6a
        0x6b -> :sswitch_6b
        0x6c -> :sswitch_6c
        0x6d -> :sswitch_6d
        0x6e -> :sswitch_6e
        0x6f -> :sswitch_6f
        0x70 -> :sswitch_70
        0x71 -> :sswitch_71
        0x72 -> :sswitch_72
        0x73 -> :sswitch_73
        0x74 -> :sswitch_74
        0x75 -> :sswitch_75
        0x76 -> :sswitch_76
        0x77 -> :sswitch_77
        0x78 -> :sswitch_78
        0x79 -> :sswitch_79
        0x7a -> :sswitch_7a
        0x7b -> :sswitch_7b
        0x7c -> :sswitch_7c
        0x7d -> :sswitch_7d
        0x7e -> :sswitch_7e
        0x7f -> :sswitch_7f
        0x80 -> :sswitch_80
        0x81 -> :sswitch_81
        0x82 -> :sswitch_82
        0x83 -> :sswitch_83
        0x84 -> :sswitch_84
        0x85 -> :sswitch_85
        0x86 -> :sswitch_86
        0x87 -> :sswitch_87
        0x88 -> :sswitch_88
        0x89 -> :sswitch_89
        0x8a -> :sswitch_8a
        0x8b -> :sswitch_8b
        0x8c -> :sswitch_8c
        0x8d -> :sswitch_8d
        0x8e -> :sswitch_8e
        0x8f -> :sswitch_8f
        0x90 -> :sswitch_90
        0x91 -> :sswitch_91
        0x92 -> :sswitch_92
        0x93 -> :sswitch_93
        0x94 -> :sswitch_94
        0x95 -> :sswitch_95
        0x96 -> :sswitch_96
        0x97 -> :sswitch_97
        0x98 -> :sswitch_98
        0x99 -> :sswitch_99
        0x9a -> :sswitch_9a
        0x9b -> :sswitch_9b
        0x9c -> :sswitch_9c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
