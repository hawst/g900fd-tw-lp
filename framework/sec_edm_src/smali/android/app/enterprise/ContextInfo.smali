.class public Landroid/app/enterprise/ContextInfo;
.super Ljava/lang/Object;
.source "ContextInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/ContextInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final MIN_PERSONA_ID:I = 0x64


# instance fields
.field public final mCallerUid:I

.field public final mContainerId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 206
    new-instance v0, Landroid/app/enterprise/ContextInfo$1;

    invoke-direct {v0}, Landroid/app/enterprise/ContextInfo$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    .line 133
    iget v1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 134
    .local v0, "userId":I
    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 135
    const/4 v1, 0x0

    iput v1, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 138
    :goto_0
    return-void

    .line 137
    :cond_0
    iput v0, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    goto :goto_0
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "callerUid"    # I

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput p1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    .line 155
    iget v1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v1}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v0

    .line 156
    .local v0, "userId":I
    const/16 v1, 0x64

    if-ge v0, v1, :cond_0

    .line 157
    const/4 v1, 0x0

    iput v1, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_0
    iput v0, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    goto :goto_0
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "callerUid"    # I
    .param p2, "containerId"    # I

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput p1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    .line 176
    iput p2, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 178
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 236
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/ContextInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/ContextInfo$1;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Landroid/app/enterprise/ContextInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Caller uid: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ,Container id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 244
    iget v0, p0, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 246
    iget v0, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 248
    return-void
.end method
