.class public Landroid/app/enterprise/dualsim/DualSimPolicy;
.super Ljava/lang/Object;
.source "DualSimPolicy.java"


# static fields
.field public static final SERVICE_BLOCKED:I = 0xff

.field public static final SIM_SLOT1:I = 0x0

.field public static final SIM_SLOT2:I = 0x1

.field public static final SIM_SLOT_UNENFORCED:I = -0x1

.field private static final TAG:Ljava/lang/String; = "DualSimPolicy"

.field private static mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    iput-object p1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 101
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/dualsim/DualSimPolicy;
    .locals 1
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    .line 149
    new-instance v0, Landroid/app/enterprise/dualsim/DualSimPolicy;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/dualsim/DualSimPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/dualsim/DualSimPolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "cxt"    # Landroid/content/Context;

    .prologue
    .line 137
    sget-object v1, Landroid/app/enterprise/dualsim/DualSimPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    sget-object v0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    if-nez v0, :cond_0

    .line 139
    new-instance v0, Landroid/app/enterprise/dualsim/DualSimPolicy;

    invoke-direct {v0, p0, p1}, Landroid/app/enterprise/dualsim/DualSimPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    .line 141
    :cond_0
    sget-object v0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    monitor-exit v1

    return-object v0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/dualsim/DualSimPolicy;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    sget-object v2, Landroid/app/enterprise/dualsim/DualSimPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 125
    :try_start_0
    sget-object v1, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    if-nez v1, :cond_0

    .line 126
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 127
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/dualsim/DualSimPolicy;

    invoke-direct {v1, v0, p0}, Landroid/app/enterprise/dualsim/DualSimPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    .line 129
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicy:Landroid/app/enterprise/dualsim/DualSimPolicy;

    monitor-exit v2

    return-object v1

    .line 130
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Landroid/app/enterprise/dualsim/IDualSimPolicy;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    if-nez v0, :cond_0

    .line 107
    const-string v0, "dualsim_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/dualsim/IDualSimPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/dualsim/IDualSimPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    .line 110
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    return-object v0
.end method


# virtual methods
.method public clearPreferredSimSlot()Z
    .locals 4

    .prologue
    .line 256
    iget-object v1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DualSimPolicy.clearPreferredSimSlot"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 257
    invoke-direct {p0}, Landroid/app/enterprise/dualsim/DualSimPolicy;->getService()Landroid/app/enterprise/dualsim/IDualSimPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 259
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    iget-object v2, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/dualsim/IDualSimPolicy;->clearPreferredSimSlot(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 264
    :goto_0
    return v1

    .line 260
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DualSimPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed - Could not access Dual SIM policy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPreferredSimSlot()I
    .locals 4

    .prologue
    .line 220
    invoke-direct {p0}, Landroid/app/enterprise/dualsim/DualSimPolicy;->getService()Landroid/app/enterprise/dualsim/IDualSimPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    iget-object v2, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/dualsim/IDualSimPolicy;->getPreferredSimSlot(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 227
    :goto_0
    return v1

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DualSimPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed - Could not access Dual SIM policy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setPreferredSimSlot(I)Z
    .locals 4
    .param p1, "slotNum"    # I

    .prologue
    .line 188
    iget-object v1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DualSimPolicy.setPreferredSimSlot"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 189
    if-eqz p1, :cond_0

    const/4 v1, 0x1

    if-ne p1, v1, :cond_1

    .line 190
    :cond_0
    invoke-direct {p0}, Landroid/app/enterprise/dualsim/DualSimPolicy;->getService()Landroid/app/enterprise/dualsim/IDualSimPolicy;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 192
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mDualSimPolicyService:Landroid/app/enterprise/dualsim/IDualSimPolicy;

    iget-object v2, p0, Landroid/app/enterprise/dualsim/DualSimPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/dualsim/IDualSimPolicy;->setPreferredSimSlot(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 198
    :goto_0
    return v1

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "DualSimPolicy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed - Could not access Dual SIM policy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
