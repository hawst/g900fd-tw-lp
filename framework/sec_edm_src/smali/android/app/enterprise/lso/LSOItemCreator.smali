.class public final Landroid/app/enterprise/lso/LSOItemCreator;
.super Ljava/lang/Object;
.source "LSOItemCreator.java"


# static fields
.field public static final LSO_ITEM_TYPE_CONTAINER:B = 0x4t

.field public static final LSO_ITEM_TYPE_IMAGE:B = 0x3t

.field public static final LSO_ITEM_TYPE_NONE:B = 0x0t

.field public static final LSO_ITEM_TYPE_SPACE:B = 0x1t

.field public static final LSO_ITEM_TYPE_TEXT:B = 0x2t

.field public static final LSO_ITEM_TYPE_WIDGET:B = 0x5t

.field private static final TAG:Ljava/lang/String; = "LSO"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createItem(B)Landroid/app/enterprise/lso/LSOItemData;
    .locals 4
    .param p0, "type"    # B

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 61
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    packed-switch p0, :pswitch_data_0

    .line 88
    const-string v1, "LSO"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown ItemType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :goto_0
    return-object v0

    .line 64
    :pswitch_0
    new-instance v0, Landroid/app/enterprise/lso/LSOItemSpace;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>()V

    .line 65
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 69
    :pswitch_1
    new-instance v0, Landroid/app/enterprise/lso/LSOItemText;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemText;-><init>()V

    .line 70
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 74
    :pswitch_2
    new-instance v0, Landroid/app/enterprise/lso/LSOItemImage;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemImage;-><init>()V

    .line 75
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 79
    :pswitch_3
    new-instance v0, Landroid/app/enterprise/lso/LSOItemContainer;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 80
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 84
    :pswitch_4
    new-instance v0, Landroid/app/enterprise/lso/LSOItemWidget;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemWidget;-><init>()V

    .line 85
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static createItem(BLandroid/os/Parcel;)Landroid/app/enterprise/lso/LSOItemData;
    .locals 4
    .param p0, "type"    # B
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 96
    const/4 v0, 0x0

    .line 98
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    packed-switch p0, :pswitch_data_0

    .line 125
    const-string v1, "LSO"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown ItemType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    :goto_0
    return-object v0

    .line 101
    :pswitch_0
    new-instance v0, Landroid/app/enterprise/lso/LSOItemSpace;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0, p1}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(Landroid/os/Parcel;)V

    .line 102
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 106
    :pswitch_1
    new-instance v0, Landroid/app/enterprise/lso/LSOItemText;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0, p1}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Landroid/os/Parcel;)V

    .line 107
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 111
    :pswitch_2
    new-instance v0, Landroid/app/enterprise/lso/LSOItemImage;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0, p1}, Landroid/app/enterprise/lso/LSOItemImage;-><init>(Landroid/os/Parcel;)V

    .line 112
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 116
    :pswitch_3
    new-instance v0, Landroid/app/enterprise/lso/LSOItemContainer;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0, p1}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>(Landroid/os/Parcel;)V

    .line 117
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 121
    :pswitch_4
    new-instance v0, Landroid/app/enterprise/lso/LSOItemWidget;

    .end local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-direct {v0, p1}, Landroid/app/enterprise/lso/LSOItemWidget;-><init>(Landroid/os/Parcel;)V

    .line 122
    .restart local v0    # "itemData":Landroid/app/enterprise/lso/LSOItemData;
    goto :goto_0

    .line 98
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
