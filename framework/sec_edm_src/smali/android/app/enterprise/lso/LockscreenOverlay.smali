.class public Landroid/app/enterprise/lso/LockscreenOverlay;
.super Ljava/lang/Object;
.source "LockscreenOverlay.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;,
        Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;
    }
.end annotation


# static fields
.field public static final DEFAULT_ALPHA_LEVEL:F = 1.0f

.field private static final EMERGENCY_PHONE_LAYER:I = 0x3

.field public static final ERROR_BAD_STATE:I = -0x6

.field public static final ERROR_FAILED:I = -0x4

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_NOT_ALLOWED:I = -0x1

.field public static final ERROR_NOT_READY:I = -0x5

.field public static final ERROR_NOT_SUPPORTED:I = -0x3

.field public static final ERROR_PERMISSION_DENIED:I = -0x2

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field private static final TAG:Ljava/lang/String; = "LSO"

.field private static gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private final mLSO:Landroid/app/enterprise/lso/LSOInterface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 466
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/lso/LockscreenOverlay;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 2
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 481
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 482
    iput-object p2, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    .line 483
    iput-object p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 484
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Landroid/app/enterprise/lso/LSOInterface;->getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LSOInterface;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    .line 485
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LockscreenOverlay;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 529
    new-instance v0, Landroid/app/enterprise/lso/LockscreenOverlay;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/app/enterprise/lso/LockscreenOverlay;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method private static createLSOItem_EmergencyPhone(Landroid/content/Context;Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;)Landroid/app/enterprise/lso/LSOItemData;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "phoneInfo"    # Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 1597
    new-instance v0, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1600
    .local v0, "itemContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    iget v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    if-lez v2, :cond_0

    .line 1601
    new-instance v2, Landroid/app/enterprise/lso/LSOItemSpace;

    iget v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    int-to-float v3, v3

    invoke-direct {v2, v4, v5, v3}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    invoke-virtual {v0, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1605
    :cond_0
    new-instance v1, Landroid/app/enterprise/lso/LSOItemWidget;

    invoke-direct {v1}, Landroid/app/enterprise/lso/LSOItemWidget;-><init>()V

    .line 1606
    .local v1, "lsoItem":Landroid/app/enterprise/lso/LSOItemWidget;
    const-string v2, "com.sec.widget.lso.EmergencyPhoneWidget"

    invoke-virtual {v1, v2}, Landroid/app/enterprise/lso/LSOItemWidget;->setWidget(Ljava/lang/String;)V

    .line 1607
    const-string v2, "epw:phoneNumber"

    iget-object v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1609
    iget-object v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->text:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->text:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_3

    .line 1610
    const-string v2, "android:text"

    iget-object v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->text:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1611
    const-string v2, "android:maxLines"

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1616
    :goto_0
    iget-object v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 1617
    const-string v2, "android:src"

    iget-object v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1619
    :cond_1
    const-string v2, "android:orientation"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1620
    const-string v2, "epw:showBG"

    iget-boolean v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showBackground:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1621
    const-string v2, "android:topPos"

    iget v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1622
    const-string v2, "android:bottomPos"

    iget v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1623
    iget v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->gravity:I

    invoke-virtual {v1, v2}, Landroid/app/enterprise/lso/LSOItemWidget;->setGravity(I)V

    .line 1625
    invoke-virtual {v1, v4, v5}, Landroid/app/enterprise/lso/LSOItemWidget;->setDimension(II)V

    .line 1626
    iget v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    iget v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/app/enterprise/lso/LSOItemWidget;->setWeight(F)V

    .line 1627
    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1630
    iget v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    const/16 v3, 0x64

    if-ge v2, v3, :cond_2

    .line 1631
    new-instance v2, Landroid/app/enterprise/lso/LSOItemSpace;

    iget v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    rsub-int/lit8 v3, v3, 0x64

    int-to-float v3, v3

    invoke-direct {v2, v4, v5, v3}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    invoke-virtual {v0, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1635
    :cond_2
    const-string v2, "android:alpha"

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/app/enterprise/lso/LSOItemContainer;->setAttribute(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1636
    const/16 v2, 0x33

    invoke-virtual {v0, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1638
    return-object v0

    .line 1613
    :cond_3
    const-string v2, "epw:showText"

    iget-boolean v3, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showDefaultText:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_0
.end method

.method private static createLSOItem_EmergencyPhone(Landroid/content/Context;Ljava/lang/String;)Landroid/app/enterprise/lso/LSOItemData;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "emergencyPhoneNumber"    # Ljava/lang/String;

    .prologue
    const/16 v7, 0x33

    .line 1562
    new-instance v1, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v1}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1565
    .local v1, "itemContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    new-instance v4, Landroid/app/enterprise/lso/LSOItemSpace;

    const/4 v5, -0x1

    const/16 v6, 0x14

    invoke-static {p0, v6}, Landroid/app/enterprise/lso/LSOUtils;->convertDipToPixel(Landroid/content/Context;I)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1568
    new-instance v2, Landroid/app/enterprise/lso/LSOItemWidget;

    invoke-direct {v2}, Landroid/app/enterprise/lso/LSOItemWidget;-><init>()V

    .line 1569
    .local v2, "lsoItem":Landroid/app/enterprise/lso/LSOItemWidget;
    const-string v4, "com.sec.widget.lso.EmergencyPhoneWidget"

    invoke-virtual {v2, v4}, Landroid/app/enterprise/lso/LSOItemWidget;->setWidget(Ljava/lang/String;)V

    .line 1570
    const-string v4, "epw:phoneNumber"

    invoke-virtual {v2, v4, p1}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    const-string v4, "android:orientation"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1572
    const-string v4, "epw:showText"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/app/enterprise/lso/LSOItemWidget;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1576
    const/16 v4, 0x64

    invoke-static {p0, v4}, Landroid/app/enterprise/lso/LSOUtils;->convertDipToPixel(Landroid/content/Context;I)I

    move-result v3

    .line 1577
    .local v3, "w":I
    const/16 v4, 0xc8

    invoke-static {p0, v4}, Landroid/app/enterprise/lso/LSOUtils;->convertDipToPixel(Landroid/content/Context;I)I

    move-result v0

    .line 1578
    .local v0, "h":I
    invoke-virtual {v2, v3, v0}, Landroid/app/enterprise/lso/LSOItemWidget;->setDimension(II)V

    .line 1579
    invoke-virtual {v2, v7}, Landroid/app/enterprise/lso/LSOItemWidget;->setGravity(I)V

    .line 1580
    invoke-virtual {v1, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1586
    const-string v4, "android:alpha"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/app/enterprise/lso/LSOItemContainer;->setAttribute(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1587
    invoke-virtual {v1, v7}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1589
    return-object v1
.end method

.method private static createLSOItem_StyleEnterprise(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/lso/LSOItemData;
    .locals 21
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "enterpriseName"    # Ljava/lang/String;
    .param p2, "enterpriseLogo"    # Ljava/lang/String;
    .param p3, "enterpriseAddress"    # Ljava/lang/String;
    .param p4, "enterprisePhone"    # Ljava/lang/String;

    .prologue
    .line 1445
    const/16 v17, 0xc0

    const/16 v18, 0xc7

    const/16 v19, 0xd1

    invoke-static/range {v17 .. v19}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    .line 1446
    .local v3, "DEFAULT_BG_COLOR":I
    const v17, 0x1040afb

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/app/enterprise/lso/LSOUtils;->getResourceString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v11

    .line 1447
    .local v11, "text1":Ljava/lang/String;
    move-object/from16 v12, p1

    .line 1448
    .local v12, "text2":Ljava/lang/String;
    const v17, 0x1040afc

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/app/enterprise/lso/LSOUtils;->getResourceString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v13

    .line 1450
    .local v13, "text3":Ljava/lang/String;
    const v17, 0x1040afd

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, Landroid/app/enterprise/lso/LSOUtils;->getResourceString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v14

    .line 1451
    .local v14, "text4":Ljava/lang/String;
    move-object/from16 v15, p3

    .line 1452
    .local v15, "text5":Ljava/lang/String;
    move-object/from16 v16, p4

    .line 1454
    .local v16, "text6":Ljava/lang/String;
    new-instance v7, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v7}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1457
    .local v7, "itemContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    new-instance v17, Landroid/app/enterprise/lso/LSOItemSpace;

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/high16 v20, 0x41200000    # 10.0f

    invoke-direct/range {v17 .. v20}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1460
    new-instance v8, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v8}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1462
    .local v8, "itemData":Landroid/app/enterprise/lso/LSOItemContainer;
    new-instance v4, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v4}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1463
    .local v4, "innerContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    invoke-direct {v10, v11}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1464
    .local v10, "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setGravity(I)V

    .line 1465
    invoke-virtual {v4, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1467
    if-eqz p2, :cond_0

    .line 1468
    new-instance v9, Landroid/app/enterprise/lso/LSOItemImage;

    move-object/from16 v0, p2

    invoke-direct {v9, v0}, Landroid/app/enterprise/lso/LSOItemImage;-><init>(Ljava/lang/String;)V

    .line 1469
    .local v9, "itemImage":Landroid/app/enterprise/lso/LSOItemImage;
    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/app/enterprise/lso/LSOItemImage;->setDimension(II)V

    .line 1470
    const-string v17, "android:maxHeight"

    const/16 v18, 0x4b

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, Landroid/app/enterprise/lso/LSOUtils;->convertDipToPixel(Landroid/content/Context;I)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Landroid/app/enterprise/lso/LSOItemImage;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1472
    invoke-virtual {v4, v9}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1475
    .end local v9    # "itemImage":Landroid/app/enterprise/lso/LSOItemImage;
    :cond_0
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    .end local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    invoke-direct {v10, v12}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1476
    .restart local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    sget-object v17, Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;->LARGE:Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextSize(Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;)V

    .line 1477
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextStyle(I)V

    .line 1478
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setGravity(I)V

    .line 1479
    invoke-virtual {v4, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1481
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    .end local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    invoke-direct {v10, v13}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1482
    .restart local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextStyle(I)V

    .line 1483
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setGravity(I)V

    .line 1484
    invoke-virtual {v4, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1486
    invoke-virtual {v4, v3}, Landroid/app/enterprise/lso/LSOItemContainer;->setBgColor(I)V

    .line 1487
    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(II)V

    .line 1488
    invoke-virtual {v8, v4}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1490
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1491
    const/16 v17, -0x1

    const/16 v18, 0x0

    const/high16 v19, 0x42300000    # 44.0f

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v8, v0, v1, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(IIF)V

    .line 1492
    invoke-virtual {v7, v8}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1495
    new-instance v17, Landroid/app/enterprise/lso/LSOItemSpace;

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/high16 v20, 0x41200000    # 10.0f

    invoke-direct/range {v17 .. v20}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1498
    new-instance v8, Landroid/app/enterprise/lso/LSOItemContainer;

    .end local v8    # "itemData":Landroid/app/enterprise/lso/LSOItemContainer;
    invoke-direct {v8}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1500
    .restart local v8    # "itemData":Landroid/app/enterprise/lso/LSOItemContainer;
    new-instance v4, Landroid/app/enterprise/lso/LSOItemContainer;

    .end local v4    # "innerContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    invoke-direct {v4}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1501
    .restart local v4    # "innerContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    sget-object v17, Landroid/app/enterprise/lso/LSOItemContainer$ORIENTATION;->HORIZONTAL:Landroid/app/enterprise/lso/LSOItemContainer$ORIENTATION;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setOrientation(Landroid/app/enterprise/lso/LSOItemContainer$ORIENTATION;)V

    .line 1503
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    .end local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    invoke-direct {v10, v14}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1504
    .restart local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    const/16 v17, 0x0

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/app/enterprise/lso/LSOItemText;->setDimension(II)V

    .line 1505
    const v17, 0x3f19999a    # 0.6f

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setWeight(F)V

    .line 1506
    sget-object v17, Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;->SMALL:Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextSize(Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;)V

    .line 1507
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextStyle(I)V

    .line 1508
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setGravity(I)V

    .line 1509
    const-string v17, "android:maxLines"

    const/16 v18, 0x4

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/app/enterprise/lso/LSOItemText;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1510
    invoke-virtual {v4, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1513
    new-instance v5, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v5}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1514
    .local v5, "innerContainer2":Landroid/app/enterprise/lso/LSOItemContainer;
    const/16 v17, 0x0

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v5, v0, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(II)V

    .line 1515
    const/high16 v17, 0x3f800000    # 1.0f

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setWeight(F)V

    .line 1516
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1519
    new-instance v6, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v6}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 1520
    .local v6, "innerContainer3":Landroid/app/enterprise/lso/LSOItemContainer;
    const/16 v17, -0x2

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v6, v0, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(II)V

    .line 1522
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1524
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    .end local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    invoke-direct {v10, v15}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1525
    .restart local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    sget-object v17, Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;->SMALL:Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextSize(Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;)V

    .line 1526
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextStyle(I)V

    .line 1527
    const-string v17, "android:maxLines"

    const/16 v18, 0x3

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/app/enterprise/lso/LSOItemText;->setAttribute(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1528
    invoke-virtual {v6, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1530
    new-instance v10, Landroid/app/enterprise/lso/LSOItemText;

    .end local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    move-object/from16 v0, v16

    invoke-direct {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;-><init>(Ljava/lang/String;)V

    .line 1531
    .restart local v10    # "lsoText":Landroid/app/enterprise/lso/LSOItemText;
    sget-object v17, Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;->SMALL:Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextSize(Landroid/app/enterprise/lso/LSOItemText$LSOTextSize;)V

    .line 1532
    const/16 v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/enterprise/lso/LSOItemText;->setTextStyle(I)V

    .line 1533
    const-string v17, "android:singleLine"

    const/16 v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Landroid/app/enterprise/lso/LSOItemText;->setAttribute(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1534
    invoke-virtual {v6, v10}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1536
    invoke-virtual {v5, v6}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1539
    invoke-virtual {v4, v5}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1542
    const/16 v17, -0x1

    const/16 v18, -0x2

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v4, v0, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(II)V

    .line 1543
    invoke-virtual {v4, v3}, Landroid/app/enterprise/lso/LSOItemContainer;->setBgColor(I)V

    .line 1544
    const/16 v17, 0x11

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1545
    invoke-virtual {v8, v4}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1547
    const/16 v17, 0x30

    move/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->setGravity(I)V

    .line 1548
    const/16 v17, -0x1

    const/16 v18, 0x0

    const/high16 v19, 0x41f00000    # 30.0f

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v8, v0, v1, v2}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(IIF)V

    .line 1549
    invoke-virtual {v7, v8}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1552
    new-instance v17, Landroid/app/enterprise/lso/LSOItemSpace;

    const/16 v18, -0x1

    const/16 v19, 0x0

    const/high16 v20, 0x40c00000    # 6.0f

    invoke-direct/range {v17 .. v20}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 1554
    return-object v7
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LockscreenOverlay;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 517
    sget-object v1, Landroid/app/enterprise/lso/LockscreenOverlay;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 518
    :try_start_0
    sget-object v0, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    if-nez v0, :cond_0

    .line 519
    new-instance v0, Landroid/app/enterprise/lso/LockscreenOverlay;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/lso/LockscreenOverlay;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    .line 521
    :cond_0
    sget-object v0, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    monitor-exit v1

    return-object v0

    .line 522
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/lso/LockscreenOverlay;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 505
    sget-object v2, Landroid/app/enterprise/lso/LockscreenOverlay;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 506
    :try_start_0
    sget-object v1, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    if-nez v1, :cond_0

    .line 507
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 508
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/lso/LockscreenOverlay;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Landroid/app/enterprise/lso/LockscreenOverlay;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    .line 510
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Landroid/app/enterprise/lso/LockscreenOverlay;->gLSO:Landroid/app/enterprise/lso/LockscreenOverlay;

    monitor-exit v2

    return-object v1

    .line 511
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private static parseLSOItem_EmergencyPhoneInfo(Landroid/content/Context;Landroid/app/enterprise/lso/LSOItemData;)Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "itemData"    # Landroid/app/enterprise/lso/LSOItemData;

    .prologue
    const/4 v6, 0x0

    .line 1646
    invoke-virtual {p1}, Landroid/app/enterprise/lso/LSOItemData;->getType()B

    move-result v7

    const/4 v8, 0x4

    if-eq v7, v8, :cond_1

    .line 1705
    :cond_0
    :goto_0
    return-object v6

    :cond_1
    move-object v3, p1

    .line 1650
    check-cast v3, Landroid/app/enterprise/lso/LSOItemContainer;

    .line 1651
    .local v3, "itemContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    const/4 v4, 0x0

    .line 1653
    .local v4, "lsoWidget":Landroid/app/enterprise/lso/LSOItemWidget;
    invoke-virtual {v3}, Landroid/app/enterprise/lso/LSOItemContainer;->getNumItems()I

    move-result v5

    .line 1654
    .local v5, "numChild":I
    const/4 v1, 0x0

    .line 1655
    .local v1, "i":I
    :goto_1
    if-ge v1, v5, :cond_2

    if-nez v4, :cond_2

    .line 1656
    invoke-virtual {v3, v1}, Landroid/app/enterprise/lso/LSOItemContainer;->getItem(I)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v2

    .line 1657
    .local v2, "item":Landroid/app/enterprise/lso/LSOItemData;
    invoke-virtual {v2}, Landroid/app/enterprise/lso/LSOItemData;->getType()B

    move-result v7

    const/4 v8, 0x5

    if-ne v7, v8, :cond_9

    move-object v4, v2

    .line 1658
    check-cast v4, Landroid/app/enterprise/lso/LSOItemWidget;

    .line 1664
    .end local v2    # "item":Landroid/app/enterprise/lso/LSOItemData;
    :cond_2
    if-eqz v4, :cond_0

    .line 1668
    invoke-virtual {v4}, Landroid/app/enterprise/lso/LSOItemWidget;->getAttrs()Landroid/app/enterprise/lso/LSOAttributeSet;

    move-result-object v0

    .line 1669
    .local v0, "attr":Landroid/app/enterprise/lso/LSOAttributeSet;
    if-eqz v0, :cond_0

    .line 1673
    new-instance v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;

    const-string v7, ""

    invoke-direct {v6, v7}, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;-><init>(Ljava/lang/String;)V

    .line 1675
    .local v6, "phoneInfo":Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;
    invoke-virtual {v4}, Landroid/app/enterprise/lso/LSOItemWidget;->getGravity()I

    move-result v7

    iput v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->gravity:I

    .line 1677
    const-string v7, "epw:phoneNumber"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1678
    const-string v7, "epw:phoneNumber"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    .line 1681
    :cond_3
    const-string v7, "android:text"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1682
    const-string v7, "android:text"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->text:Ljava/lang/String;

    .line 1685
    :cond_4
    const-string v7, "android:src"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1686
    const-string v7, "android:src"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    .line 1689
    :cond_5
    const-string v7, "android:topPos"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1690
    const-string v7, "android:topPos"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    .line 1693
    :cond_6
    const-string v7, "android:bottomPos"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1694
    const-string v7, "android:bottomPos"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iput v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    .line 1697
    :cond_7
    const-string v7, "epw:showBG"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1698
    const-string v7, "epw:showBG"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iput-boolean v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showBackground:Z

    .line 1701
    :cond_8
    const-string v7, "epw:showText"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1702
    const-string v7, "epw:showText"

    invoke-virtual {v0, v7}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iput-boolean v7, v6, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showDefaultText:Z

    goto/16 :goto_0

    .line 1661
    .end local v0    # "attr":Landroid/app/enterprise/lso/LSOAttributeSet;
    .end local v6    # "phoneInfo":Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;
    .restart local v2    # "item":Landroid/app/enterprise/lso/LSOItemData;
    :cond_9
    add-int/lit8 v1, v1, 0x1

    .line 1662
    goto/16 :goto_1
.end method


# virtual methods
.method public canConfigure()Z
    .locals 2

    .prologue
    .line 1350
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOInterface;->canConfigure(I)Z

    move-result v0

    return v0
.end method

.method public configure(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 3
    .param p1, "enterpriseName"    # Ljava/lang/String;
    .param p2, "enterpriseLogo"    # Ljava/lang/String;
    .param p3, "enterpriseAddress"    # Ljava/lang/String;
    .param p4, "enterprisePhone"    # Ljava/lang/String;

    .prologue
    .line 589
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LockscreenOverlay.configure"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 590
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 592
    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Name and Address cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 595
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 596
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    const-string v2, "logo"

    invoke-static {v1, p2, v2}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 598
    if-nez p2, :cond_2

    .line 599
    const-string v1, "LSO"

    const-string v2, "Failed to copy enterprise logo"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/4 v1, -0x4

    .line 610
    :goto_0
    return v1

    .line 604
    :cond_2
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1, p1, p2, p3, p4}, Landroid/app/enterprise/lso/LockscreenOverlay;->createLSOItem_StyleEnterprise(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    .line 606
    .local v0, "lsoData":Landroid/app/enterprise/lso/LSOItemData;
    if-eqz v0, :cond_3

    .line 607
    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LockscreenOverlay;->setData(Landroid/app/enterprise/lso/LSOItemData;)I

    move-result v1

    goto :goto_0

    .line 610
    :cond_3
    const/16 v1, -0x7d0

    goto :goto_0
.end method

.method public configure([Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;)I
    .locals 14
    .param p1, "imageList"    # [Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;

    .prologue
    const/16 v13, 0x64

    const/4 v12, 0x0

    const/4 v11, -0x1

    .line 685
    iget-object v7, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v8, "LockscreenOverlay.configure"

    invoke-static {v7, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 686
    if-eqz p1, :cond_0

    array-length v7, p1

    if-nez v7, :cond_1

    .line 687
    :cond_0
    new-instance v7, Ljava/security/InvalidParameterException;

    const-string v8, "Invalid argument list - List is empty"

    invoke-direct {v7, v8}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 690
    :cond_1
    array-length v7, p1

    add-int/lit8 v5, v7, -0x1

    .line 692
    .local v5, "lastIdx":I
    aget-object v7, p1, v12

    if-eqz v7, :cond_2

    aget-object v7, p1, v12

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    if-ltz v7, :cond_2

    aget-object v7, p1, v5

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    if-gt v7, v13, :cond_2

    aget-object v7, p1, v5

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    aget-object v8, p1, v5

    iget v8, v8, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    if-gt v7, v8, :cond_3

    .line 695
    :cond_2
    new-instance v7, Ljava/security/InvalidParameterException;

    const-string v8, "Invalid argument list - Item[0] top position is less than 0, Item[last_index] is greater than 100, or position of Item[0] > Item[last_index]"

    invoke-direct {v7, v8}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 699
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_6

    .line 700
    aget-object v7, p1, v1

    if-eqz v7, :cond_4

    aget-object v7, p1, v1

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    aget-object v8, p1, v1

    iget v8, v8, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    if-le v7, v8, :cond_4

    aget-object v7, p1, v1

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    add-int/lit8 v8, v1, 0x1

    aget-object v8, p1, v8

    iget v8, v8, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    if-le v7, v8, :cond_5

    .line 702
    :cond_4
    new-instance v7, Ljava/security/InvalidParameterException;

    const-string v8, "Invalid argument list - Item[i] top > bottom or Item[i+1] top < Item[i] bottom"

    invoke-direct {v7, v8}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 699
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 707
    :cond_6
    new-instance v2, Landroid/app/enterprise/lso/LSOItemContainer;

    invoke-direct {v2}, Landroid/app/enterprise/lso/LSOItemContainer;-><init>()V

    .line 708
    .local v2, "itemContainer":Landroid/app/enterprise/lso/LSOItemContainer;
    invoke-virtual {v2, v11, v11}, Landroid/app/enterprise/lso/LSOItemContainer;->setDimension(II)V

    .line 710
    const/4 v6, 0x0

    .line 711
    .local v6, "topPos":I
    const/4 v1, 0x0

    :goto_1
    array-length v7, p1

    if-ge v1, v7, :cond_9

    .line 712
    aget-object v7, p1, v1

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    if-ge v6, v7, :cond_7

    .line 713
    new-instance v4, Landroid/app/enterprise/lso/LSOItemSpace;

    aget-object v7, p1, v1

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    sub-int/2addr v7, v6

    int-to-float v7, v7

    invoke-direct {v4, v11, v12, v7}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    .line 715
    .local v4, "itemSpace":Landroid/app/enterprise/lso/LSOItemSpace;
    invoke-virtual {v2, v4}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 718
    .end local v4    # "itemSpace":Landroid/app/enterprise/lso/LSOItemSpace;
    :cond_7
    iget-object v7, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    aget-object v8, p1, v1

    iget-object v8, v8, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->filePath:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "lso"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 720
    .local v0, "filePath":Ljava/lang/String;
    if-nez v0, :cond_8

    .line 721
    const-string v7, "LSO"

    const-string v8, "Failed to copy images"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 722
    const/4 v7, -0x4

    .line 737
    .end local v0    # "filePath":Ljava/lang/String;
    :goto_2
    return v7

    .line 724
    .restart local v0    # "filePath":Ljava/lang/String;
    :cond_8
    new-instance v3, Landroid/app/enterprise/lso/LSOItemImage;

    invoke-direct {v3, v0}, Landroid/app/enterprise/lso/LSOItemImage;-><init>(Ljava/lang/String;)V

    .line 725
    .local v3, "itemImage":Landroid/app/enterprise/lso/LSOItemImage;
    aget-object v7, p1, v1

    iget v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    aget-object v8, p1, v1

    iget v8, v8, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {v3, v11, v12, v7}, Landroid/app/enterprise/lso/LSOItemImage;->setDimension(IIF)V

    .line 727
    aget-object v7, p1, v1

    iget-object v7, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->scaleType:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v7}, Landroid/app/enterprise/lso/LSOItemImage;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 728
    invoke-virtual {v2, v3}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 730
    aget-object v7, p1, v1

    iget v6, v7, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    .line 711
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 733
    .end local v0    # "filePath":Ljava/lang/String;
    .end local v3    # "itemImage":Landroid/app/enterprise/lso/LSOItemImage;
    :cond_9
    if-ge v6, v13, :cond_a

    .line 734
    new-instance v7, Landroid/app/enterprise/lso/LSOItemSpace;

    rsub-int/lit8 v8, v6, 0x64

    int-to-float v8, v8

    invoke-direct {v7, v11, v12, v8}, Landroid/app/enterprise/lso/LSOItemSpace;-><init>(IIF)V

    invoke-virtual {v2, v7}, Landroid/app/enterprise/lso/LSOItemContainer;->addItem(Landroid/app/enterprise/lso/LSOItemData;)Z

    .line 737
    :cond_a
    invoke-virtual {p0, v2}, Landroid/app/enterprise/lso/LockscreenOverlay;->setData(Landroid/app/enterprise/lso/LSOItemData;)I

    move-result v7

    goto :goto_2
.end method

.method public getAlpha()F
    .locals 2

    .prologue
    .line 1276
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v1}, Landroid/app/enterprise/lso/LSOInterface;->getPreferences()Landroid/app/enterprise/lso/LSOAttributeSet;

    move-result-object v0

    .line 1277
    .local v0, "attrSet":Landroid/app/enterprise/lso/LSOAttributeSet;
    if-eqz v0, :cond_0

    .line 1278
    const-string v1, "android:alpha"

    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOAttributeSet;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1279
    const-string v1, "android:alpha"

    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOAttributeSet;->getAsFloat(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 1283
    :goto_0
    return v1

    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getData()Landroid/app/enterprise/lso/LSOItemData;
    .locals 1

    .prologue
    .line 1429
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0}, Landroid/app/enterprise/lso/LSOInterface;->getData()Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    return-object v0
.end method

.method public getData(I)Landroid/app/enterprise/lso/LSOItemData;
    .locals 1
    .param p1, "layer"    # I

    .prologue
    .line 1436
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0, p1}, Landroid/app/enterprise/lso/LSOInterface;->getData(I)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    return-object v0
.end method

.method public getEmergencyPhone()Ljava/lang/String;
    .locals 2

    .prologue
    .line 975
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/enterprise/lso/LockscreenOverlay;->getData(I)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    .line 976
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    if-eqz v0, :cond_0

    .line 977
    invoke-virtual {v0}, Landroid/app/enterprise/lso/LSOItemData;->getId()Ljava/lang/String;

    move-result-object v1

    .line 980
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEmergencyPhoneInfo()Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;
    .locals 2

    .prologue
    .line 1018
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Landroid/app/enterprise/lso/LockscreenOverlay;->getData(I)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    .line 1019
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    if-eqz v0, :cond_0

    .line 1020
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1, v0}, Landroid/app/enterprise/lso/LockscreenOverlay;->parseLSOItem_EmergencyPhoneInfo(Landroid/content/Context;Landroid/app/enterprise/lso/LSOItemData;)Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;

    move-result-object v1

    .line 1023
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConfigured()Z
    .locals 2

    .prologue
    .line 1315
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOInterface;->isConfigured(I)Z

    move-result v0

    return v0
.end method

.method public removeEmergencyPhone()V
    .locals 2

    .prologue
    .line 1188
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "LockscreenOverlay.removeEmergencyPhone"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1189
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LockscreenOverlay;->resetData(I)V

    .line 1190
    return-void
.end method

.method public resetAll()V
    .locals 2

    .prologue
    .line 1067
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "LockscreenOverlay.resetAll"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1068
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0}, Landroid/app/enterprise/lso/LSOInterface;->reset()V

    .line 1069
    invoke-virtual {p0}, Landroid/app/enterprise/lso/LockscreenOverlay;->resetWallpaper()V

    .line 1070
    return-void
.end method

.method public resetData(I)V
    .locals 1
    .param p1, "layer"    # I

    .prologue
    .line 1117
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0, p1}, Landroid/app/enterprise/lso/LSOInterface;->resetData(I)V

    .line 1118
    return-void
.end method

.method public resetOverlay()V
    .locals 2

    .prologue
    .line 1109
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "LockscreenOverlay.resetOverlay"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1110
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0}, Landroid/app/enterprise/lso/LSOInterface;->resetData()V

    .line 1111
    return-void
.end method

.method public resetWallpaper()V
    .locals 2

    .prologue
    .line 1152
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "LockscreenOverlay.resetWallpaper"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1153
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0}, Landroid/app/enterprise/lso/LSOInterface;->resetWallpaper()V

    .line 1154
    return-void
.end method

.method public setAlpha(F)I
    .locals 3
    .param p1, "alphaLevel"    # F

    .prologue
    .line 1238
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LockscreenOverlay.setAlpha"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1239
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_1

    .line 1240
    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Alpha values must be in between 0 to 1"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1243
    :cond_1
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v1}, Landroid/app/enterprise/lso/LSOInterface;->getPreferences()Landroid/app/enterprise/lso/LSOAttributeSet;

    move-result-object v0

    .line 1244
    .local v0, "attrSet":Landroid/app/enterprise/lso/LSOAttributeSet;
    if-nez v0, :cond_2

    .line 1245
    new-instance v0, Landroid/app/enterprise/lso/LSOAttributeSet;

    .end local v0    # "attrSet":Landroid/app/enterprise/lso/LSOAttributeSet;
    invoke-direct {v0}, Landroid/app/enterprise/lso/LSOAttributeSet;-><init>()V

    .line 1248
    .restart local v0    # "attrSet":Landroid/app/enterprise/lso/LSOAttributeSet;
    :cond_2
    const-string v1, "android:alpha"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/enterprise/lso/LSOAttributeSet;->put(Ljava/lang/String;Ljava/lang/Float;)V

    .line 1249
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v1, v0}, Landroid/app/enterprise/lso/LSOInterface;->setPreferences(Landroid/app/enterprise/lso/LSOAttributeSet;)I

    move-result v1

    return v1
.end method

.method public setData(Landroid/app/enterprise/lso/LSOItemData;)I
    .locals 2
    .param p1, "lsoData"    # Landroid/app/enterprise/lso/LSOItemData;

    .prologue
    .line 1392
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v1, p1}, Landroid/app/enterprise/lso/LSOInterface;->setData(Landroid/app/enterprise/lso/LSOItemData;)I

    move-result v0

    .line 1393
    .local v0, "retVal":I
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->cleanDataLocalDirectory(Landroid/content/Context;)V

    .line 1394
    return v0
.end method

.method public setData(Landroid/app/enterprise/lso/LSOItemData;I)I
    .locals 1
    .param p1, "lsoData"    # Landroid/app/enterprise/lso/LSOItemData;
    .param p2, "layer"    # I

    .prologue
    .line 1401
    iget-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v0, p1, p2}, Landroid/app/enterprise/lso/LSOInterface;->setData(Landroid/app/enterprise/lso/LSOItemData;I)I

    move-result v0

    return v0
.end method

.method public setEmergencyPhone(Ljava/lang/String;)I
    .locals 3
    .param p1, "emergencyPhoneNumber"    # Ljava/lang/String;

    .prologue
    .line 848
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LockscreenOverlay.setEmergencyPhone"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 850
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 851
    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Emergency/Support phone cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 854
    :cond_1
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/app/enterprise/lso/LockscreenOverlay;->createLSOItem_EmergencyPhone(Landroid/content/Context;Ljava/lang/String;)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    .line 855
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    invoke-virtual {v0, p1}, Landroid/app/enterprise/lso/LSOItemData;->setId(Ljava/lang/String;)V

    .line 857
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/lso/LockscreenOverlay;->setData(Landroid/app/enterprise/lso/LSOItemData;I)I

    move-result v1

    return v1
.end method

.method public setEmergencyPhoneInfo(Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;)I
    .locals 4
    .param p1, "phoneInfo"    # Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;

    .prologue
    .line 915
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LockscreenOverlay.setEmergencyPhoneInfo"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 916
    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 918
    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Emergency/Support phone cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 922
    :cond_1
    iget v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    if-ltz v1, :cond_2

    iget v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    const/16 v2, 0x64

    if-gt v1, v2, :cond_2

    iget v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    iget v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    if-gt v1, v2, :cond_3

    .line 924
    :cond_2
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Invalid argument list"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 927
    :cond_3
    iget-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 928
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    iget-object v2, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    const-string v3, "epw"

    invoke-static {v1, v2, v3}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    .line 929
    iget-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 930
    const-string v1, "LSO"

    const-string v2, "Failed to copy icon"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    const/4 v1, -0x4

    .line 938
    :goto_0
    return v1

    .line 935
    :cond_4
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/app/enterprise/lso/LockscreenOverlay;->createLSOItem_EmergencyPhone(Landroid/content/Context;Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    .line 936
    .local v0, "itemData":Landroid/app/enterprise/lso/LSOItemData;
    iget-object v1, p1, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/enterprise/lso/LSOItemData;->setId(Ljava/lang/String;)V

    .line 938
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/lso/LockscreenOverlay;->setData(Landroid/app/enterprise/lso/LSOItemData;I)I

    move-result v1

    goto :goto_0
.end method

.method public setWallpaper(Ljava/lang/String;)I
    .locals 3
    .param p1, "enterpriseWallpaper"    # Ljava/lang/String;

    .prologue
    .line 785
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "LockscreenOverlay.setWallpaper"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 787
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 788
    :cond_0
    new-instance v1, Ljava/security/InvalidParameterException;

    const-string v2, "Wallpaper cannot be null"

    invoke-direct {v1, v2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 791
    :cond_1
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    const-string v2, "wp"

    invoke-static {v1, p1, v2}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 793
    if-nez p1, :cond_2

    .line 794
    const-string v1, "LSO"

    const-string v2, "Failed to copy wallaper"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 795
    const/4 v0, -0x4

    .line 800
    :goto_0
    return v0

    .line 798
    :cond_2
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mLSO:Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {v1, p1}, Landroid/app/enterprise/lso/LSOInterface;->setWallpaper(Ljava/lang/String;)I

    move-result v0

    .line 799
    .local v0, "retVal":I
    iget-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->cleanDataLocalDirectory(Landroid/content/Context;)V

    goto :goto_0
.end method
