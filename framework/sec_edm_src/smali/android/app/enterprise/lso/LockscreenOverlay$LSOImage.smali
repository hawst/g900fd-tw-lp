.class public Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;
.super Ljava/lang/Object;
.source "LockscreenOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/lso/LockscreenOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LSOImage"
.end annotation


# instance fields
.field public bottomPosition:I

.field public filePath:Ljava/lang/String;

.field public scaleType:Landroid/widget/ImageView$ScaleType;

.field public topPosition:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 223
    const/4 v0, 0x0

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    .line 224
    const/16 v0, 0x64

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    .line 225
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->scaleType:Landroid/widget/ImageView$ScaleType;

    .line 226
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;I)V
    .locals 1
    .param p1, "topPos"    # I
    .param p2, "imagePath"    # Ljava/lang/String;
    .param p3, "bottomPos"    # I

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    iput p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    .line 263
    iput p3, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    .line 264
    iput-object p2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->filePath:Ljava/lang/String;

    .line 265
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->scaleType:Landroid/widget/ImageView$ScaleType;

    .line 266
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ILandroid/widget/ImageView$ScaleType;)V
    .locals 0
    .param p1, "topPos"    # I
    .param p2, "imagePath"    # Ljava/lang/String;
    .param p3, "bottomPos"    # I
    .param p4, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 285
    iput p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    .line 286
    iput p3, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    .line 287
    iput-object p2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->filePath:Ljava/lang/String;

    .line 288
    iput-object p4, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->scaleType:Landroid/widget/ImageView$ScaleType;

    .line 289
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 240
    const/4 v0, 0x0

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->topPosition:I

    .line 241
    const/16 v0, 0x64

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->bottomPosition:I

    .line 242
    iput-object p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->filePath:Ljava/lang/String;

    .line 243
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    iput-object v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOImage;->scaleType:Landroid/widget/ImageView$ScaleType;

    .line 244
    return-void
.end method
