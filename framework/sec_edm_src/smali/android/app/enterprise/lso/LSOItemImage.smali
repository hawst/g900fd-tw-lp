.class public Landroid/app/enterprise/lso/LSOItemImage;
.super Landroid/app/enterprise/lso/LSOItemData;
.source "LSOItemImage.java"


# static fields
.field public static final LSO_FIELD_IMAGE_PATH:I = 0x80

.field public static final LSO_FIELD_IMAGE_URL:I = 0x100

.field public static final LSO_FIELD_SCALE_TYPE:I = 0x200


# instance fields
.field private imagePath:Ljava/lang/String;

.field private pollingInterval:J

.field private scaleType:I

.field private url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/app/enterprise/lso/LSOItemData;-><init>(B)V

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 66
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Landroid/app/enterprise/lso/LSOItemData;-><init>(BLandroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 71
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/app/enterprise/lso/LSOItemData;-><init>(B)V

    .line 72
    invoke-virtual {p0, p1}, Landroid/app/enterprise/lso/LSOItemImage;->setImagePath(Ljava/lang/String;)V

    .line 73
    return-void
.end method


# virtual methods
.method public getImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->imagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getPollingInterval()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    return-wide v0
.end method

.method public getScaleType()Landroid/widget/ImageView$ScaleType;
    .locals 3

    .prologue
    .line 110
    invoke-static {}, Landroid/widget/ImageView$ScaleType;->values()[Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    .line 111
    .local v0, "types":[Landroid/widget/ImageView$ScaleType;
    iget v1, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    if-ltz v1, :cond_0

    iget v1, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 112
    iget v1, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    aget-object v1, v0, v1

    .line 115
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    goto :goto_0
.end method

.method public getScaleTypeAsInteger()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    return v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/app/enterprise/lso/LSOItemData;->readFromParcel(Landroid/os/Parcel;)V

    .line 151
    const/16 v0, 0x80

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/lso/LSOItemImage;->readStringFromParcel(Landroid/os/Parcel;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->imagePath:Ljava/lang/String;

    .line 152
    const/16 v0, 0x200

    const/4 v1, -0x1

    invoke-virtual {p0, p1, v0, v1}, Landroid/app/enterprise/lso/LSOItemImage;->readIntFromParcel(Landroid/os/Parcel;II)I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    .line 153
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->isFieldUpdated(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    .line 155
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    .line 157
    :cond_0
    return-void
.end method

.method public setImagePath(Ljava/lang/String;)V
    .locals 1
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 77
    iput-object p1, p0, Landroid/app/enterprise/lso/LSOItemImage;->imagePath:Ljava/lang/String;

    .line 78
    const/16 v0, 0x80

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->updateFieldFlag(I)V

    .line 79
    return-void
.end method

.method public setScaleType(I)V
    .locals 1
    .param p1, "scaleType"    # I

    .prologue
    .line 131
    iput p1, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    .line 132
    const/16 v0, 0x200

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->updateFieldFlag(I)V

    .line 133
    return-void
.end method

.method public setScaleType(Landroid/widget/ImageView$ScaleType;)V
    .locals 1
    .param p1, "scaleType"    # Landroid/widget/ImageView$ScaleType;

    .prologue
    .line 125
    invoke-virtual {p1}, Landroid/widget/ImageView$ScaleType;->ordinal()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    .line 126
    const/16 v0, 0x200

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->updateFieldFlag(I)V

    .line 127
    return-void
.end method

.method public setURL(Ljava/lang/String;J)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "pollingPeriod"    # J

    .prologue
    .line 88
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 89
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    .line 95
    :goto_0
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->updateFieldFlag(I)V

    .line 96
    return-void

    .line 92
    :cond_1
    iput-object p1, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    .line 93
    iput-wide p2, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ImageView "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-super {p0}, Landroid/app/enterprise/lso/LSOItemData;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 167
    .local v0, "str":Ljava/lang/String;
    const/16 v1, 0x80

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ImagePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOItemImage;->imagePath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/enterprise/lso/LSOItemImage;->toString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    const/16 v1, 0x100

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ImageUrl:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " PollingInterval:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Landroid/app/enterprise/lso/LSOItemImage;->toString(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 138
    invoke-super {p0, p1, p2}, Landroid/app/enterprise/lso/LSOItemData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 139
    const/16 v0, 0x80

    iget-object v1, p0, Landroid/app/enterprise/lso/LSOItemImage;->imagePath:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Landroid/app/enterprise/lso/LSOItemImage;->writeToParcel(Landroid/os/Parcel;ILjava/lang/String;)V

    .line 140
    const/16 v0, 0x200

    iget v1, p0, Landroid/app/enterprise/lso/LSOItemImage;->scaleType:I

    invoke-virtual {p0, p1, v0, v1}, Landroid/app/enterprise/lso/LSOItemImage;->writeToParcel(Landroid/os/Parcel;II)V

    .line 141
    const/16 v0, 0x100

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOItemImage;->isFieldUpdated(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget-wide v0, p0, Landroid/app/enterprise/lso/LSOItemImage;->pollingInterval:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 145
    :cond_0
    return-void
.end method
