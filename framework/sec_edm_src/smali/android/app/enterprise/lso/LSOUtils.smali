.class public Landroid/app/enterprise/lso/LSOUtils;
.super Ljava/lang/Object;
.source "LSOUtils.java"


# static fields
.field private static final DEFAULT_COMPRESS_QUALITY:I = 0x64

.field private static MAX_IMAGE_SIZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "LSO"

.field static final TEMP_DIR:Ljava/lang/String; = ".tmp"

.field static final TEMP_LSO_DIR:Ljava/lang/String; = ".lso"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    sput v0, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cleanDataLocalDirectory(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 780
    const-string v0, ".tmp"

    invoke-static {p0, v0}, Landroid/app/enterprise/lso/LSOUtils;->cleanDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)V

    .line 781
    return-void
.end method

.method public static cleanDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dir"    # Ljava/lang/String;

    .prologue
    .line 784
    if-eqz p1, :cond_0

    .line 785
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 786
    .local v0, "tempDir":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->deleteRecursive(Ljava/io/File;)V

    .line 788
    .end local v0    # "tempDir":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static convertDipToPixel(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "sizeInDip"    # I

    .prologue
    .line 766
    const/4 v0, 0x1

    int-to-float v1, p1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static convertImageFormat(Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;Landroid/graphics/Point;)Z
    .locals 3
    .param p0, "sourcePath"    # Ljava/lang/String;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "pt"    # Landroid/graphics/Point;

    .prologue
    .line 542
    const/4 v0, 0x0

    .line 543
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    .line 544
    iget v1, p3, Landroid/graphics/Point;->x:I

    iget v2, p3, Landroid/graphics/Point;->y:I

    invoke-static {p0, v1, v2}, Landroid/app/enterprise/lso/LSOUtils;->getBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 548
    :goto_0
    invoke-static {v0, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->saveBitmapToFile(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 546
    :cond_0
    invoke-static {p0}, Landroid/app/enterprise/lso/LSOUtils;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static convertImageFormatToSize(Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;Landroid/graphics/Point;)Z
    .locals 3
    .param p0, "sourcePath"    # Ljava/lang/String;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "destPath"    # Ljava/lang/String;
    .param p3, "pt"    # Landroid/graphics/Point;

    .prologue
    .line 531
    const/4 v0, 0x0

    .line 532
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    .line 533
    iget v1, p3, Landroid/graphics/Point;->x:I

    iget v2, p3, Landroid/graphics/Point;->y:I

    invoke-static {p0, v1, v2}, Landroid/app/enterprise/lso/LSOUtils;->getBitmapBySize(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 537
    :goto_0
    invoke-static {v0, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->saveBitmapToFile(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;)Z

    move-result v1

    return v1

    .line 535
    :cond_0
    invoke-static {p0}, Landroid/app/enterprise/lso/LSOUtils;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "origFilePath"    # Ljava/lang/String;
    .param p1, "candidatePath"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x1

    .line 588
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move-object p1, v9

    .line 634
    .end local p1    # "candidatePath":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object p1

    .line 591
    .restart local p1    # "candidatePath":Ljava/lang/String;
    :cond_2
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 592
    .local v8, "source":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 595
    .local v1, "candidate":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 601
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v10

    if-nez v10, :cond_4

    .line 602
    :cond_3
    const-string v10, "LSO"

    const-string v11, "created file not exist: "

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v9

    .line 603
    goto :goto_0

    .line 596
    :catch_0
    move-exception v2

    .line 597
    .local v2, "e":Ljava/io/IOException;
    const-string v10, "LSO"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "fail to create new file: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object p1, v9

    .line 598
    goto :goto_0

    .line 606
    .end local v2    # "e":Ljava/io/IOException;
    :cond_4
    invoke-virtual {v1, v11, v12}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 607
    invoke-virtual {v1, v11, v12}, Ljava/io/File;->setReadable(ZZ)Z

    .line 608
    invoke-virtual {v1, v11, v11}, Ljava/io/File;->setWritable(ZZ)Z

    .line 611
    const/4 v5, 0x0

    .line 612
    .local v5, "in":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 614
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v8}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 615
    .end local v5    # "in":Ljava/io/InputStream;
    .local v6, "in":Ljava/io/InputStream;
    :try_start_2
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 616
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .local v4, "fos":Ljava/io/FileOutputStream;
    const/16 v10, 0x400

    :try_start_3
    new-array v0, v10, [B

    .line 618
    .local v0, "buf":[B
    :goto_1
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    move-result v7

    .local v7, "len":I
    if-lez v7, :cond_7

    .line 619
    const/4 v10, 0x0

    invoke-virtual {v4, v0, v10, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 623
    .end local v0    # "buf":[B
    .end local v7    # "len":I
    :catchall_0
    move-exception v10

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    :goto_2
    if-eqz v5, :cond_5

    .line 624
    :try_start_4
    invoke-virtual {v5}, Ljava/io/InputStream;->close()V

    .line 625
    :cond_5
    if-eqz v3, :cond_6

    .line 626
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    :cond_6
    throw v10
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    .line 628
    :catch_1
    move-exception v2

    .line 629
    .local v2, "e":Ljava/lang/Exception;
    :goto_3
    const-string v10, "LSO"

    const-string v11, "fail to save image: "

    invoke-static {v10, v11, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 630
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-object p1, v9

    .line 631
    goto :goto_0

    .line 621
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v0    # "buf":[B
    .restart local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    .restart local v7    # "len":I
    :cond_7
    :try_start_5
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 623
    if-eqz v6, :cond_8

    .line 624
    :try_start_6
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 625
    :cond_8
    if-eqz v4, :cond_1

    .line 626
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 628
    :catch_2
    move-exception v2

    move-object v3, v4

    .end local v4    # "fos":Ljava/io/FileOutputStream;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_3

    .line 623
    .end local v0    # "buf":[B
    .end local v7    # "len":I
    :catchall_1
    move-exception v10

    goto :goto_2

    .end local v5    # "in":Ljava/io/InputStream;
    .restart local v6    # "in":Ljava/io/InputStream;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/InputStream;
    .restart local v5    # "in":Ljava/io/InputStream;
    goto :goto_2
.end method

.method public static copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 791
    const-string v0, ".tmp"

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "imagePath"    # Ljava/lang/String;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 795
    const-string v0, ".tmp"

    invoke-static {p0, v0, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dir"    # Ljava/lang/String;
    .param p2, "imagePath"    # Ljava/lang/String;
    .param p3, "prefix"    # Ljava/lang/String;

    .prologue
    .line 800
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    .line 802
    .local v1, "tempDir":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 803
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 804
    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->mkDir(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 805
    const/4 v0, 0x0

    .line 820
    :cond_0
    :goto_0
    return-object v0

    .line 809
    :cond_1
    if-nez p3, :cond_2

    .line 810
    const-string p3, ""

    .line 813
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 815
    .local v0, "candidatePath":Ljava/lang/String;
    invoke-static {p2, v0}, Landroid/app/enterprise/lso/LSOUtils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 816
    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 817
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/app/enterprise/lso/LSOUtils;->deleteRecursive(Ljava/io/File;)V

    goto :goto_0
.end method

.method public static createRippleImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "ori"    # Landroid/graphics/Bitmap;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 110
    if-nez p0, :cond_0

    .line 111
    const/4 v6, 0x0

    .line 136
    :goto_0
    return-object v6

    .line 113
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    .line 114
    .local v8, "width":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 115
    .local v2, "height":I
    const/4 v3, 0x0

    .line 116
    .local v3, "left":F
    const/4 v7, 0x0

    .line 117
    .local v7, "top":F
    invoke-static {v8, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 119
    .local v5, "size":I
    if-ge v8, v2, :cond_1

    .line 120
    sub-int v9, v2, v8

    int-to-float v9, v9

    div-float v3, v9, v10

    .line 125
    :goto_1
    const/4 v6, 0x0

    .line 127
    .local v6, "target":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v5, v9}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 128
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 129
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/high16 v9, -0x1000000

    invoke-virtual {v0, v9}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 130
    new-instance v4, Landroid/graphics/Paint;

    const/4 v9, 0x2

    invoke-direct {v4, v9}, Landroid/graphics/Paint;-><init>(I)V

    .line 131
    .local v4, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, p0, v3, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 132
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v4    # "paint":Landroid/graphics/Paint;
    :catch_0
    move-exception v1

    .line 133
    .local v1, "e":Ljava/lang/Exception;
    const-string v9, "LSO"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "createRippleImage: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 122
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v6    # "target":Landroid/graphics/Bitmap;
    :cond_1
    sub-int v9, v8, v2

    int-to-float v9, v9

    div-float v7, v9, v10

    goto :goto_1
.end method

.method public static createRippleImage(Ljava/lang/String;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;)Z
    .locals 2
    .param p0, "sourceImage"    # Ljava/lang/String;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "destImage"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-static {p0}, Landroid/app/enterprise/lso/LSOUtils;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->createRippleImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 103
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 104
    const/4 v1, 0x0

    .line 106
    :goto_0
    return v1

    :cond_0
    invoke-static {v0, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->saveBitmapToFile(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method private static decodeFile(Ljava/io/File;II)Landroid/graphics/Bitmap;
    .locals 16
    .param p0, "f"    # Ljava/io/File;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 460
    move/from16 v5, p1

    .line 461
    .local v5, "maxSize":I
    move/from16 v0, p2

    if-le v5, v0, :cond_0

    .line 462
    move/from16 v5, p2

    .line 465
    :cond_0
    const/4 v2, 0x0

    .line 468
    .local v2, "b":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-static/range {p0 .. p0}, Landroid/app/enterprise/lso/LSOUtils;->getBitmapSize(Ljava/io/File;)Landroid/graphics/Point;

    move-result-object v7

    .line 470
    .local v7, "pt":Landroid/graphics/Point;
    const/4 v8, 0x1

    .line 471
    .local v8, "scale":I
    iget v9, v7, Landroid/graphics/Point;->y:I

    move/from16 v0, p2

    if-le v9, v0, :cond_2

    iget v9, v7, Landroid/graphics/Point;->x:I

    move/from16 v0, p1

    if-le v9, v0, :cond_2

    .line 472
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    int-to-double v12, v5

    iget v9, v7, Landroid/graphics/Point;->y:I

    iget v14, v7, Landroid/graphics/Point;->x:I

    invoke-static {v9, v14}, Ljava/lang/Math;->max(II)I

    move-result v9

    int-to-double v14, v9

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    invoke-static {v14, v15}, Ljava/lang/Math;->log(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    long-to-int v9, v12

    int-to-double v12, v9

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v8, v10

    .line 485
    :cond_1
    :goto_0
    new-instance v6, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v6}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 486
    .local v6, "o2":Landroid/graphics/BitmapFactory$Options;
    iput v8, v6, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 487
    new-instance v4, Ljava/io/FileInputStream;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 488
    .local v4, "fis":Ljava/io/FileInputStream;
    const/4 v9, 0x0

    invoke-static {v4, v9, v6}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 489
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 493
    .end local v4    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "o2":Landroid/graphics/BitmapFactory$Options;
    .end local v7    # "pt":Landroid/graphics/Point;
    .end local v8    # "scale":I
    :goto_1
    return-object v2

    .line 476
    .restart local v7    # "pt":Landroid/graphics/Point;
    .restart local v8    # "scale":I
    :cond_2
    iget v9, v7, Landroid/graphics/Point;->y:I

    move/from16 v0, p2

    if-le v9, v0, :cond_3

    .line 477
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    move/from16 v0, p2

    int-to-double v12, v0

    iget v9, v7, Landroid/graphics/Point;->y:I

    int-to-double v14, v9

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    invoke-static {v14, v15}, Ljava/lang/Math;->log(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    long-to-int v9, v12

    int-to-double v12, v9

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v8, v10

    goto :goto_0

    .line 479
    :cond_3
    iget v9, v7, Landroid/graphics/Point;->x:I

    move/from16 v0, p1

    if-le v9, v0, :cond_1

    .line 480
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    move/from16 v0, p1

    int-to-double v12, v0

    iget v9, v7, Landroid/graphics/Point;->x:I

    int-to-double v14, v9

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v12

    const-wide/high16 v14, 0x3fe0000000000000L    # 0.5

    invoke-static {v14, v15}, Ljava/lang/Math;->log(D)D

    move-result-wide v14

    div-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->round(D)J

    move-result-wide v12

    long-to-int v9, v12

    int-to-double v12, v9

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v10

    double-to-int v8, v10

    goto :goto_0

    .line 490
    .end local v7    # "pt":Landroid/graphics/Point;
    .end local v8    # "scale":I
    :catch_0
    move-exception v3

    .line 491
    .local v3, "e":Ljava/io/IOException;
    const-string v9, "LSO"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "decodeFile: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public static deleteFile(Ljava/lang/String;)V
    .locals 4
    .param p0, "fileName"    # Ljava/lang/String;

    .prologue
    .line 700
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 701
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 702
    const-string v2, "LSO"

    const-string v3, "File deleted."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return-void

    .line 704
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    const-string v2, "LSO"

    const-string v3, "Delete operation is failed."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 706
    .end local v1    # "file":Ljava/io/File;
    :catch_0
    move-exception v0

    .line 707
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "LSO"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static deleteRecursive(Ljava/io/File;)V
    .locals 6
    .param p0, "fileOrDirectory"    # Ljava/io/File;

    .prologue
    .line 683
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_1

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 688
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 689
    .local v4, "list":[Ljava/io/File;
    if-eqz v4, :cond_2

    .line 690
    move-object v0, v4

    .local v0, "arr$":[Ljava/io/File;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    .line 691
    .local v1, "child":Ljava/io/File;
    invoke-static {v1}, Landroid/app/enterprise/lso/LSOUtils;->deleteRecursive(Ljava/io/File;)V

    .line 690
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 695
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "child":Ljava/io/File;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "list":[Ljava/io/File;
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public static getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 141
    if-nez p0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 157
    :goto_0
    return-object v0

    .line 145
    :cond_0
    const/4 v0, 0x0

    .line 147
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 148
    .local v2, "imgFile":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 149
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 152
    :cond_1
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 154
    .end local v2    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 155
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBitmap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "maxSize"    # I

    .prologue
    .line 162
    if-nez p0, :cond_0

    .line 163
    const/4 v0, 0x0

    .line 178
    :goto_0
    return-object v0

    .line 166
    :cond_0
    const/4 v0, 0x0

    .line 168
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 169
    .local v2, "imgFile":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 170
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    invoke-static {v2, p1, p1}, Landroid/app/enterprise/lso/LSOUtils;->decodeFile(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_1
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    .end local v2    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBitmap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 368
    move v5, p1

    .line 369
    .local v5, "maxSize":I
    if-le p2, v5, :cond_0

    .line 370
    move v5, p2

    .line 373
    :cond_0
    invoke-static {p0, v5}, Landroid/app/enterprise/lso/LSOUtils;->getBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 374
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    .line 375
    const/4 v0, 0x0

    .line 424
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-object v0

    .line 378
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_2
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 379
    .local v7, "src":Landroid/graphics/Rect;
    const/4 v10, 0x0

    iput v10, v7, Landroid/graphics/Rect;->top:I

    iput v10, v7, Landroid/graphics/Rect;->left:I

    .line 380
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    iput v10, v7, Landroid/graphics/Rect;->right:I

    .line 381
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    iput v10, v7, Landroid/graphics/Rect;->bottom:I

    .line 383
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 384
    .local v2, "dest":Landroid/graphics/RectF;
    const/4 v10, 0x0

    iput v10, v2, Landroid/graphics/RectF;->top:F

    iput v10, v2, Landroid/graphics/RectF;->left:F

    .line 385
    int-to-float v10, p2

    iput v10, v2, Landroid/graphics/RectF;->bottom:F

    .line 386
    int-to-float v10, p1

    iput v10, v2, Landroid/graphics/RectF;->right:F

    .line 388
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v11

    float-to-int v11, v11

    sub-int v9, v10, v11

    .line 389
    .local v9, "widthDiff":I
    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v10

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v11

    float-to-int v11, v11

    sub-int v4, v10, v11

    .line 391
    .local v4, "heightDiff":I
    if-gtz v9, :cond_3

    if-lez v4, :cond_1

    .line 397
    :cond_3
    if-lez v9, :cond_4

    .line 398
    iget v10, v7, Landroid/graphics/Rect;->left:I

    div-int/lit8 v11, v9, 0x2

    add-int/2addr v10, v11

    iput v10, v7, Landroid/graphics/Rect;->left:I

    .line 399
    iget v10, v7, Landroid/graphics/Rect;->right:I

    div-int/lit8 v11, v9, 0x2

    sub-int/2addr v10, v11

    iput v10, v7, Landroid/graphics/Rect;->right:I

    .line 405
    :goto_1
    if-lez v4, :cond_5

    .line 406
    iget v10, v7, Landroid/graphics/Rect;->top:I

    div-int/lit8 v11, v4, 0x2

    add-int/2addr v10, v11

    iput v10, v7, Landroid/graphics/Rect;->top:I

    .line 407
    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    div-int/lit8 v11, v4, 0x2

    sub-int/2addr v10, v11

    iput v10, v7, Landroid/graphics/Rect;->bottom:I

    .line 413
    :goto_2
    const/4 v8, 0x0

    .line 415
    .local v8, "target":Landroid/graphics/Bitmap;
    :try_start_0
    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 416
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 417
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/high16 v10, -0x1000000

    invoke-virtual {v1, v10}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 418
    new-instance v6, Landroid/graphics/Paint;

    const/4 v10, 0x2

    invoke-direct {v6, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 419
    .local v6, "paint":Landroid/graphics/Paint;
    invoke-virtual {v1, v0, v7, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v6    # "paint":Landroid/graphics/Paint;
    :goto_3
    move-object v0, v8

    .line 424
    goto :goto_0

    .line 401
    .end local v8    # "target":Landroid/graphics/Bitmap;
    :cond_4
    iget v10, v2, Landroid/graphics/RectF;->left:F

    div-int/lit8 v11, v9, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iput v10, v2, Landroid/graphics/RectF;->left:F

    .line 402
    iget v10, v2, Landroid/graphics/RectF;->right:F

    div-int/lit8 v11, v9, 0x2

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v2, Landroid/graphics/RectF;->right:F

    goto :goto_1

    .line 409
    :cond_5
    iget v10, v2, Landroid/graphics/RectF;->top:F

    div-int/lit8 v11, v4, 0x2

    int-to-float v11, v11

    sub-float/2addr v10, v11

    iput v10, v2, Landroid/graphics/RectF;->top:F

    .line 410
    iget v10, v2, Landroid/graphics/RectF;->bottom:F

    div-int/lit8 v11, v4, 0x2

    int-to-float v11, v11

    add-float/2addr v10, v11

    iput v10, v2, Landroid/graphics/RectF;->bottom:F

    goto :goto_2

    .line 420
    .restart local v8    # "target":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v3

    .line 421
    .local v3, "e":Ljava/lang/Exception;
    const-string v10, "LSO"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "createRippleImage: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public static getBitmapBySize(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 204
    if-nez p0, :cond_1

    .line 205
    const/4 v0, 0x0

    .line 226
    :cond_0
    :goto_0
    return-object v0

    .line 207
    :cond_1
    const/4 v0, 0x0

    .line 209
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 210
    .local v3, "imgFile":Ljava/io/File;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 211
    const-string v4, "LSO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Image found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 220
    .end local v3    # "imgFile":Ljava/io/File;
    :goto_1
    if-eqz v0, :cond_0

    .line 221
    invoke-static {v0, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->resizeBitmapByScaleAndCropCenter(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 222
    .local v1, "bmpTemp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 223
    move-object v0, v1

    goto :goto_0

    .line 214
    .end local v1    # "bmpTemp":Landroid/graphics/Bitmap;
    .restart local v3    # "imgFile":Ljava/io/File;
    :cond_2
    :try_start_1
    const-string v4, "LSO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Image not found: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 216
    .end local v3    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v2

    .line 217
    .local v2, "e":Ljava/lang/Exception;
    const-string v4, "LSO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getBitmapBySize: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private static getBitmapSize(Ljava/io/File;)Landroid/graphics/Point;
    .locals 7
    .param p0, "f"    # Ljava/io/File;

    .prologue
    .line 440
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 443
    .local v3, "pt":Landroid/graphics/Point;
    :try_start_0
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 444
    .local v2, "o":Landroid/graphics/BitmapFactory$Options;
    const/4 v4, 0x1

    iput-boolean v4, v2, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 446
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 447
    .local v1, "fis":Ljava/io/FileInputStream;
    const/4 v4, 0x0

    invoke-static {v1, v4, v2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 448
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    .line 450
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 451
    iget v4, v2, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v4, v3, Landroid/graphics/Point;->y:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 455
    .end local v1    # "fis":Ljava/io/FileInputStream;
    .end local v2    # "o":Landroid/graphics/BitmapFactory$Options;
    :goto_0
    return-object v3

    .line 452
    :catch_0
    move-exception v0

    .line 453
    .local v0, "e":Ljava/io/IOException;
    const-string v4, "LSO"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "decodeFile: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .param p0, "imagePath"    # Ljava/lang/String;

    .prologue
    .line 737
    if-nez p0, :cond_0

    .line 738
    const/4 v0, 0x0

    .line 753
    :goto_0
    return-object v0

    .line 741
    :cond_0
    const/4 v0, 0x0

    .line 743
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 744
    .local v2, "imgFile":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 745
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 746
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 748
    :cond_1
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 750
    .end local v2    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 751
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDrawable:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getMaxBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "imagePath"    # Ljava/lang/String;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 183
    if-nez p0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 199
    :goto_0
    return-object v0

    .line 187
    :cond_0
    const/4 v0, 0x0

    .line 189
    .local v0, "bmp":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 190
    .local v2, "imgFile":Ljava/io/File;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 191
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    invoke-static {v2, p1, p2}, Landroid/app/enterprise/lso/LSOUtils;->decodeFile(Ljava/io/File;II)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_1
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Image not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 196
    .end local v2    # "imgFile":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 197
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBitmap: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getMaxImageSize(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    sget v2, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    if-lez v2, :cond_0

    .line 84
    sget v2, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    .line 97
    :goto_0
    return v2

    .line 87
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 88
    .local v0, "pt":Landroid/graphics/Point;
    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 89
    .local v1, "winMgr":Landroid/view/WindowManager;
    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 91
    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    if-le v2, v3, :cond_1

    .line 92
    iget v2, v0, Landroid/graphics/Point;->x:I

    sput v2, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    .line 97
    :goto_1
    sget v2, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    goto :goto_0

    .line 94
    :cond_1
    iget v2, v0, Landroid/graphics/Point;->y:I

    sput v2, Landroid/app/enterprise/lso/LSOUtils;->MAX_IMAGE_SIZE:I

    goto :goto_1
.end method

.method public static getResourceDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 771
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 772
    .local v0, "res":Landroid/content/res/Resources;
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 773
    :cond_0
    const/4 v1, 0x0

    .line 776
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_0
.end method

.method public static getResourceString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 757
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 758
    .local v0, "res":Landroid/content/res/Resources;
    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 759
    :cond_0
    const/4 v1, 0x0

    .line 762
    :goto_0
    return-object v1

    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static isTablet()Z
    .locals 2

    .prologue
    .line 77
    const-string v1, "ro.build.characteristics"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 78
    .local v0, "deviceType":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static mkDir(Ljava/lang/String;)Z
    .locals 6
    .param p0, "dirPath"    # Ljava/lang/String;

    .prologue
    .line 712
    const/4 v2, 0x0

    .line 714
    .local v2, "result":Z
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 715
    .local v0, "dir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 716
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_0

    .line 717
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to create directory: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    const/4 v2, 0x0

    .line 732
    .end local v0    # "dir":Ljava/io/File;
    :goto_0
    return v2

    .line 720
    .restart local v0    # "dir":Ljava/io/File;
    :cond_0
    const/4 v2, 0x1

    .line 721
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/io/File;->setReadable(Z)Z

    .line 722
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Ljava/io/File;->setWritable(Z)Z

    .line 723
    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Ljava/io/File;->setExecutable(ZZ)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 729
    .end local v0    # "dir":Ljava/io/File;
    :catch_0
    move-exception v1

    .line 730
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error creating directory "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 726
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "dir":Ljava/io/File;
    :cond_1
    const/4 v3, 0x1

    const/4 v4, 0x0

    :try_start_1
    invoke-virtual {v0, v3, v4}, Ljava/io/File;->setExecutable(ZZ)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 727
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static resizeBitmapByScaleAndCropCenter(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/high16 v11, 0x3f800000    # 1.0f

    .line 273
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne p1, v8, :cond_0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-ne p2, v8, :cond_0

    move-object v6, p0

    .line 320
    :goto_0
    return-object v6

    .line 277
    :cond_0
    const/4 v6, 0x0

    .line 281
    .local v6, "target":Landroid/graphics/Bitmap;
    int-to-float v8, p1

    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 284
    .local v5, "scale":F
    cmpl-float v8, v5, v11

    if-lez v8, :cond_1

    .line 285
    int-to-float v8, p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 286
    .local v3, "minScale":F
    cmpl-float v8, v3, v11

    if-lez v8, :cond_4

    .line 287
    const/high16 v5, 0x3f800000    # 1.0f

    .line 293
    .end local v3    # "minScale":F
    :cond_1
    :goto_1
    const-string v8, "LSO"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "resizeBitmapByScaleAndCropCenter scale:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 296
    .local v7, "widthNew":I
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 298
    .local v2, "heightNew":I
    if-lt v7, p1, :cond_5

    if-lt v2, p2, :cond_5

    .line 299
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 304
    :goto_2
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 305
    .local v0, "canvas":Landroid/graphics/Canvas;
    const/high16 v8, -0x1000000

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 307
    if-lt v7, p1, :cond_2

    if-ge v2, p2, :cond_3

    .line 308
    :cond_2
    sub-int v8, p1, v7

    int-to-float v8, v8

    div-float/2addr v8, v12

    sub-int v9, p2, v2

    int-to-float v9, v9

    div-float/2addr v9, v12

    invoke-virtual {v0, v8, v9}, Landroid/graphics/Canvas;->translate(FF)V

    .line 310
    :cond_3
    invoke-virtual {v0, v5, v5}, Landroid/graphics/Canvas;->scale(FF)V

    .line 312
    new-instance v4, Landroid/graphics/Paint;

    const/4 v8, 0x6

    invoke-direct {v4, v8}, Landroid/graphics/Paint;-><init>(I)V

    .line 313
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, p0, v8, v9, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 316
    .end local v0    # "canvas":Landroid/graphics/Canvas;
    .end local v2    # "heightNew":I
    .end local v4    # "paint":Landroid/graphics/Paint;
    .end local v5    # "scale":F
    .end local v7    # "widthNew":I
    :catch_0
    move-exception v1

    .line 317
    .local v1, "e":Ljava/lang/Exception;
    const-string v8, "LSO"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "resizeBitmapAndCropCenter: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 289
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "minScale":F
    .restart local v5    # "scale":F
    :cond_4
    move v5, v3

    goto :goto_1

    .line 301
    .end local v3    # "minScale":F
    .restart local v2    # "heightNew":I
    .restart local v7    # "widthNew":I
    :cond_5
    :try_start_1
    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v6

    goto :goto_2
.end method

.method public static saveBitmapToFile(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/lang/String;)Z
    .locals 7
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 553
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 555
    .local v0, "candidate":Ljava/io/File;
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v5

    if-nez v5, :cond_1

    .line 562
    :cond_0
    const-string v5, "LSO"

    const-string v6, "created file not exist: "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 584
    :goto_0
    return v3

    .line 556
    :catch_0
    move-exception v1

    .line 557
    .local v1, "e":Ljava/io/IOException;
    const-string v5, "LSO"

    const-string v6, "fail to create new file: "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v3, v4

    .line 558
    goto :goto_0

    .line 566
    .end local v1    # "e":Ljava/io/IOException;
    :cond_1
    invoke-virtual {v0, v6, v4}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 567
    invoke-virtual {v0, v6, v4}, Ljava/io/File;->setReadable(ZZ)Z

    .line 568
    invoke-virtual {v0, v6, v6}, Ljava/io/File;->setWritable(ZZ)Z

    .line 570
    const/4 v3, 0x0

    .line 572
    .local v3, "result":Z
    :try_start_1
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 574
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-static {p0, p1, v2}, Landroid/app/enterprise/lso/LSOUtils;->saveBitmapToOutputStream(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v3

    .line 576
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 578
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 579
    .restart local v1    # "e":Ljava/io/IOException;
    const-string v5, "LSO"

    const-string v6, "fail to save image: "

    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 580
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move v3, v4

    .line 581
    goto :goto_0

    .line 576
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    :catchall_0
    move-exception v5

    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
.end method

.method public static saveBitmapToOutputStream(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$CompressFormat;Ljava/io/OutputStream;)Z
    .locals 3
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;
    .param p1, "format"    # Landroid/graphics/Bitmap$CompressFormat;
    .param p2, "os"    # Ljava/io/OutputStream;

    .prologue
    .line 640
    const/4 v0, 0x0

    .line 643
    .local v0, "result":Z
    if-nez p0, :cond_0

    .line 644
    const/4 v1, 0x0

    .line 651
    :goto_0
    return v1

    .line 645
    :cond_0
    const/16 v1, 0x64

    :try_start_0
    invoke-virtual {p0, p1, v1, p2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v0

    .line 646
    if-nez v0, :cond_1

    .line 647
    const-string v1, "LSO"

    const-string v2, "Bitmap write errror!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    move v1, v0

    .line 651
    goto :goto_0

    .line 649
    :catchall_0
    move-exception v1

    throw v1
.end method

.method public static scaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "bmp"    # Landroid/graphics/Bitmap;
    .param p1, "maxWidth"    # I
    .param p2, "maxHeight"    # I

    .prologue
    .line 498
    move v2, p1

    .line 499
    .local v2, "maxSize":I
    if-le v2, p2, :cond_0

    .line 500
    move v2, p2

    .line 503
    :cond_0
    const/4 v0, 0x0

    .line 505
    .local v0, "b":Landroid/graphics/Bitmap;
    :try_start_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-gt v3, p1, :cond_1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-le v3, p2, :cond_2

    .line 506
    :cond_1
    const/4 v3, 0x0

    invoke-static {p0, p1, p2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 513
    :goto_0
    return-object v0

    .line 508
    :cond_2
    move-object v0, p0

    goto :goto_0

    .line 510
    :catch_0
    move-exception v1

    .line 511
    .local v1, "e":Ljava/lang/Exception;
    const-string v3, "LSO"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "decodeFile: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
