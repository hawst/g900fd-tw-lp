.class public Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;
.super Ljava/lang/Object;
.source "LockscreenOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/lso/LockscreenOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LSOEmergencyPhoneInfo"
.end annotation


# instance fields
.field public bottomPosition:I

.field public gravity:I

.field public icon:Ljava/lang/String;

.field public phoneNumber:Ljava/lang/String;

.field public showBackground:Z

.field public showDefaultText:Z

.field public text:Ljava/lang/String;

.field public topPosition:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    invoke-virtual {p0}, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->init()V

    .line 397
    iput-object p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    .line 398
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "topPos"    # I
    .param p3, "iconFilepath"    # Ljava/lang/String;
    .param p4, "bottomPos"    # I

    .prologue
    .line 437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 438
    invoke-virtual {p0}, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->init()V

    .line 439
    iput-object p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    .line 440
    iput p2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    .line 441
    iput p4, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    .line 442
    iput-object p3, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    .line 443
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "phoneNumber"    # Ljava/lang/String;
    .param p2, "iconFilepath"    # Ljava/lang/String;

    .prologue
    .line 413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414
    invoke-virtual {p0}, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->init()V

    .line 415
    iput-object p1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    .line 416
    iput-object p2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    .line 417
    return-void
.end method


# virtual methods
.method public init()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 453
    iput-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->phoneNumber:Ljava/lang/String;

    .line 454
    iput v2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->topPosition:I

    .line 455
    const/16 v0, 0x64

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->bottomPosition:I

    .line 456
    iput-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->icon:Ljava/lang/String;

    .line 457
    const/16 v0, 0x11

    iput v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->gravity:I

    .line 458
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showBackground:Z

    .line 459
    iput-boolean v2, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->showDefaultText:Z

    .line 460
    iput-object v1, p0, Landroid/app/enterprise/lso/LockscreenOverlay$LSOEmergencyPhoneInfo;->text:Ljava/lang/String;

    .line 461
    return-void
.end method
