.class public Landroid/app/enterprise/lso/LSOInterface;
.super Ljava/lang/Object;
.source "LSOInterface.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# static fields
.field private static gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/lso/LSOInterface;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p2, p0, Landroid/app/enterprise/lso/LSOInterface;->mContext:Landroid/content/Context;

    .line 83
    iput-object p1, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 84
    return-void
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/lso/LSOInterface;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    sget-object v1, Landroid/app/enterprise/lso/LSOInterface;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 119
    :try_start_0
    sget-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    if-nez v0, :cond_0

    .line 120
    new-instance v0, Landroid/app/enterprise/lso/LSOInterface;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/lso/LSOInterface;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    .line 122
    :cond_0
    sget-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    monitor-exit v1

    return-object v0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/lso/LSOInterface;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 107
    sget-object v1, Landroid/app/enterprise/lso/LSOInterface;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    sget-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Landroid/app/enterprise/lso/LSOInterface;

    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/app/enterprise/lso/LSOInterface;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    .line 112
    :cond_0
    sget-object v0, Landroid/app/enterprise/lso/LSOInterface;->gLockscreenOverlay:Landroid/app/enterprise/lso/LSOInterface;

    monitor-exit v1

    return-object v0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private getService()Landroid/app/enterprise/lso/ILockscreenOverlay;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    if-nez v0, :cond_0

    .line 88
    const-string v0, "lockscreen_overlay"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/lso/ILockscreenOverlay$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    .line 91
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    return-object v0
.end method


# virtual methods
.method public canConfigure(I)Z
    .locals 4
    .param p1, "feature"    # I

    .prologue
    const/4 v1, 0x0

    .line 358
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v2

    if-nez v2, :cond_0

    .line 359
    const-string v2, "LSO"

    const-string v3, "LSO Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    :goto_0
    return v1

    .line 364
    :cond_0
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->canConfigure(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 365
    :catch_0
    move-exception v0

    .line 366
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "LSO"

    const-string v3, "Failed talking with Lockscreen customization service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 367
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 368
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v2, "LSO"

    const-string v3, "Caller does not have required permissions"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getData()Landroid/app/enterprise/lso/LSOItemData;
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOInterface;->getData(I)Landroid/app/enterprise/lso/LSOItemData;

    move-result-object v0

    return-object v0
.end method

.method public getData(I)Landroid/app/enterprise/lso/LSOItemData;
    .locals 4
    .param p1, "layer"    # I

    .prologue
    const/4 v1, 0x0

    .line 214
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v2

    if-nez v2, :cond_0

    .line 215
    const-string v2, "LSO"

    const-string v3, "LSO Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :goto_0
    return-object v1

    .line 220
    :cond_0
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->getData(Landroid/app/enterprise/ContextInfo;I)Landroid/app/enterprise/lso/LSOItemData;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "LSO"

    const-string v3, "Failed talking with Lockscreen customization service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPreferences()Landroid/app/enterprise/lso/LSOAttributeSet;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 530
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v2

    if-nez v2, :cond_0

    .line 531
    const-string v2, "LSO"

    const-string v3, "LSO Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 543
    :goto_0
    return-object v1

    .line 536
    :cond_0
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/lso/ILockscreenOverlay;->getPreferences(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/lso/LSOAttributeSet;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    goto :goto_0

    .line 537
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "LSO"

    const-string v3, "Failed talking with Lockscreen customization service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 539
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 540
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "LSO"

    const-string v3, "Unhandled exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isConfigured(I)Z
    .locals 4
    .param p1, "feature"    # I

    .prologue
    const/4 v1, 0x0

    .line 315
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v2

    if-nez v2, :cond_0

    .line 316
    const-string v2, "LSO"

    const-string v3, "LSO Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :goto_0
    return v1

    .line 321
    :cond_0
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->isConfigured(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "LSO"

    const-string v3, "Failed talking with Lockscreen customization service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 129
    invoke-static {p2}, Landroid/app/enterprise/lso/ILockscreenOverlay$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    .line 130
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    .line 136
    return-void
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 285
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOInterface;->resetData(I)V

    .line 286
    return-void
.end method

.method public resetData()V
    .locals 1

    .prologue
    .line 290
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/app/enterprise/lso/LSOInterface;->resetData(I)V

    .line 291
    return-void
.end method

.method public resetData(I)V
    .locals 3
    .param p1, "layer"    # I

    .prologue
    .line 264
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v1

    if-nez v1, :cond_0

    .line 265
    const-string v1, "LSO"

    const-string v2, "LSO Service is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :goto_0
    return-void

    .line 269
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/app/enterprise/lso/LSOInterface;->canConfigure(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    new-instance v1, Ljava/security/AccessControlException;

    const-string v2, "Calling admin is not allowed to reset Lockscreen Overlay."

    invoke-direct {v1, v2}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 275
    :cond_1
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->resetData(Landroid/app/enterprise/ContextInfo;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 276
    :catch_0
    move-exception v0

    .line 277
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LSO"

    const-string v2, "Failed talking with Lockscreen customization service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public resetWallpaper()V
    .locals 3

    .prologue
    .line 449
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v1

    if-nez v1, :cond_0

    .line 450
    const-string v1, "LSO"

    const-string v2, "LSO Service is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    :goto_0
    return-void

    .line 454
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/app/enterprise/lso/LSOInterface;->canConfigure(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 455
    new-instance v1, Ljava/security/AccessControlException;

    const-string v2, "Calling admin is not allowed to reset wallpaper"

    invoke-direct {v1, v2}, Ljava/security/AccessControlException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 459
    :cond_1
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/lso/ILockscreenOverlay;->resetWallpaper(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 460
    :catch_0
    move-exception v0

    .line 461
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LSO"

    const-string v2, "Failed talking with Lockscreen customization service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setData(Landroid/app/enterprise/lso/LSOItemData;)I
    .locals 1
    .param p1, "lsoData"    # Landroid/app/enterprise/lso/LSOItemData;

    .prologue
    .line 183
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Landroid/app/enterprise/lso/LSOInterface;->setData(Landroid/app/enterprise/lso/LSOItemData;I)I

    move-result v0

    return v0
.end method

.method public setData(Landroid/app/enterprise/lso/LSOItemData;I)I
    .locals 3
    .param p1, "lsoData"    # Landroid/app/enterprise/lso/LSOItemData;
    .param p2, "layer"    # I

    .prologue
    .line 168
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v1

    if-nez v1, :cond_0

    .line 169
    const-string v1, "LSO"

    const-string v2, "LSO Service is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const/4 v1, -0x5

    .line 179
    :goto_0
    return v1

    .line 174
    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/lso/ILockscreenOverlay;->setData(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/lso/LSOItemData;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LSO"

    const-string v2, "Failed talking with Lockscreen customization service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 179
    const/4 v1, -0x2

    goto :goto_0
.end method

.method public setPreferences(Landroid/app/enterprise/lso/LSOAttributeSet;)I
    .locals 3
    .param p1, "pref"    # Landroid/app/enterprise/lso/LSOAttributeSet;

    .prologue
    .line 492
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v1

    if-nez v1, :cond_0

    .line 493
    const-string v1, "LSO"

    const-string v2, "LSO Service is not yet ready!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 494
    const/4 v1, -0x5

    .line 508
    :goto_0
    return v1

    .line 498
    :cond_0
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->setPreferences(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/lso/LSOAttributeSet;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    goto :goto_0

    .line 499
    :catch_0
    move-exception v0

    .line 500
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "LSO"

    const-string v2, "Failed talking with Lockscreen customization service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 508
    .end local v0    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v1, -0x2

    goto :goto_0

    .line 501
    :catch_1
    move-exception v0

    .line 502
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "LSO"

    const-string v2, "SecurityException exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 503
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "No Active Admins or MDM Permission to calling Package"

    invoke-direct {v1, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 504
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 505
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "LSO"

    const-string v2, "Unhandled exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setWallpaper(Ljava/lang/String;)I
    .locals 4
    .param p1, "imageFilePath"    # Ljava/lang/String;

    .prologue
    .line 403
    invoke-direct {p0}, Landroid/app/enterprise/lso/LSOInterface;->getService()Landroid/app/enterprise/lso/ILockscreenOverlay;

    move-result-object v2

    if-nez v2, :cond_0

    .line 404
    const-string v2, "LSO"

    const-string v3, "LSO Service is not yet ready!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    const/4 v1, -0x5

    .line 416
    :goto_0
    return v1

    .line 408
    :cond_0
    const/4 v1, -0x2

    .line 411
    .local v1, "ret":I
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/lso/LSOInterface;->mLSOService:Landroid/app/enterprise/lso/ILockscreenOverlay;

    iget-object v3, p0, Landroid/app/enterprise/lso/LSOInterface;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/lso/ILockscreenOverlay;->setWallpaper(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "LSO"

    const-string v3, "Failed talking with Lockscreen customization service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
