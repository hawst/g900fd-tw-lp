.class public Landroid/app/enterprise/VpnPolicy;
.super Ljava/lang/Object;
.source "VpnPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String; = null

.field public static final VPN_NEW_PROFILE:Ljava/lang/String; = "com.android.server.enterprise.VPN_NEW_PROFILE"


# instance fields
.field private lService:Landroid/app/enterprise/IVpnInfoPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "VpnPolicy"

    sput-object v0, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 63
    return-void
.end method

.method private getService()Landroid/app/enterprise/IVpnInfoPolicy;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    if-nez v0, :cond_0

    .line 67
    const-string v0, "vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IVpnInfoPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    .line 70
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    return-object v0
.end method


# virtual methods
.method public allowOnlySecureConnections(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1944
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.allowOnlySecureConnections"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1946
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1947
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->allowOnlySecureConnections(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1952
    :goto_0
    return v1

    .line 1949
    :catch_0
    move-exception v0

    .line 1950
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API allowOnlySecureConnections "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1952
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowUserAddProfiles(Z)Z
    .locals 4
    .param p1, "allow"    # Z

    .prologue
    .line 2366
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2367
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->allowUserAddProfiles(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2372
    :goto_0
    return v1

    .line 2369
    :catch_0
    move-exception v0

    .line 2370
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2372
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowUserChangeProfiles(Z)Z
    .locals 4
    .param p1, "allow"    # Z

    .prologue
    .line 2267
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2268
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->allowUserChangeProfiles(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2273
    :goto_0
    return v1

    .line 2270
    :catch_0
    move-exception v0

    .line 2271
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2273
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowUserSetAlwaysOn(Z)Z
    .locals 4
    .param p1, "allow"    # Z

    .prologue
    .line 2167
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2168
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->allowUserSetAlwaysOn(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2173
    :goto_0
    return v1

    .line 2170
    :catch_0
    move-exception v0

    .line 2171
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2173
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public checkRacoonSecurity([Ljava/lang/String;)Z
    .locals 3
    .param p1, "racoon"    # [Ljava/lang/String;

    .prologue
    .line 2008
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2009
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->checkRacoonSecurity(Landroid/app/enterprise/ContextInfo;[Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2014
    :goto_0
    return v1

    .line 2011
    :catch_0
    move-exception v0

    .line 2012
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API checkRacoonSecurity "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2014
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public createProfile(Landroid/app/enterprise/VpnAdminProfile;)Z
    .locals 3
    .param p1, "profile"    # Landroid/app/enterprise/VpnAdminProfile;

    .prologue
    .line 142
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.createProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 144
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 145
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->createProfile(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/VpnAdminProfile;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 150
    :goto_0
    return v1

    .line 147
    :catch_0
    move-exception v0

    .line 148
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API createProfile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 150
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteProfile(Ljava/lang/String;)V
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 178
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.deleteProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 180
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->deleteProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API deleteProfile"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getAlwaysOnProfile()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2116
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2117
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IVpnInfoPolicy;->getAlwaysOnProfile(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2122
    :goto_0
    return-object v1

    .line 2119
    :catch_0
    move-exception v0

    .line 2120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2122
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDnsDomains(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1667
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getDnsDomains"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1669
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1670
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getDnsDomains(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1676
    :goto_0
    return-object v1

    .line 1673
    :catch_0
    move-exception v0

    .line 1674
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getDnsDomains "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1676
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDnsServers(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1551
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getDnsServers"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1553
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1554
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getDnsServers(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1559
    :goto_0
    return-object v1

    .line 1556
    :catch_0
    move-exception v0

    .line 1557
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getDnsServers "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1559
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForwardRoutes(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1788
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getForwardRoutes"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1790
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1791
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getForwardRoutes(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1796
    :goto_0
    return-object v1

    .line 1793
    :catch_0
    move-exception v0

    .line 1794
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getForwardRoutes "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1796
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIPSecCaCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 622
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getIPSecCaCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 624
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 625
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getCaCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 630
    :goto_0
    return-object v1

    .line 627
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 630
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIPSecPreSharedKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 513
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getIPSecPreSharedKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 515
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getPresharedKey(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 521
    :goto_0
    return-object v1

    .line 518
    :catch_0
    move-exception v0

    .line 519
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getSharedSecret"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIPSecUserCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 740
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getIPSecUserCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 742
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 743
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getUserCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 748
    :goto_0
    return-object v1

    .line 745
    :catch_0
    move-exception v0

    .line 746
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 748
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getId(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 961
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getId"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 962
    const/4 v1, 0x0

    .line 964
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 965
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 970
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 967
    .restart local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 968
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at Vpn policy API getId"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getIpSecIdentifier(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1892
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getIpSecIdentifier"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1894
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1895
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getIpSecIdentifier(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1900
    :goto_0
    return-object v1

    .line 1897
    :catch_0
    move-exception v0

    .line 1898
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getIpSecIdentifier "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1900
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getL2TPSecret(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1299
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getL2TPSecret"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1301
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1302
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getL2TPSecret(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1307
    :goto_0
    return-object v1

    .line 1304
    :catch_0
    move-exception v0

    .line 1305
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getL2TPSecret "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1307
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOcspServerUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 2520
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getOcspServerUrl"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2522
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2523
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getOcspServerUrl(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2528
    :goto_0
    return-object v1

    .line 2525
    :catch_0
    move-exception v0

    .line 2526
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getOcspServerUrl "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2528
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getServerName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 924
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getServerName"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 925
    const/4 v1, 0x0

    .line 927
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 928
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getServerName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 933
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 930
    .restart local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 931
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at Vpn policy API getServerName"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getState(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1020
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getState"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1021
    const/4 v1, 0x0

    .line 1023
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1024
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1029
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1026
    .restart local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1027
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at Vpn policy API getState"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSupportedConnectionTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2582
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getSupportedConnectionTypes"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2584
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2585
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IVpnInfoPolicy;->getSupportedConnectionTypes(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2590
    :goto_0
    return-object v1

    .line 2587
    :catch_0
    move-exception v0

    .line 2588
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getSupportedConnectionTypes "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2590
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getType(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 886
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getType"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 887
    const/4 v1, 0x0

    .line 889
    .local v1, "str":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 890
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 895
    .end local v1    # "str":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 892
    .restart local v1    # "str":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 893
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at Vpn policy API getType"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getUserName(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1073
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getUserName"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1074
    const/4 v1, 0x0

    .line 1076
    .local v1, "userName":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1077
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getUserName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1082
    .end local v1    # "userName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1079
    .restart local v1    # "userName":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1080
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUserNameById(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1368
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getUserNameById"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1370
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1371
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getUserNameById(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1377
    :goto_0
    return-object v1

    .line 1374
    :catch_0
    move-exception v0

    .line 1375
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API isL2TPSecretEnabled "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1377
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUserPassword(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1111
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.getUserPassword"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1112
    const/4 v1, 0x0

    .line 1114
    .local v1, "userPwd":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1115
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getUserPwd(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1120
    .end local v1    # "userPwd":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v1

    .line 1117
    .restart local v1    # "userPwd":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 1118
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUserPwdById(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 1400
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getUserPwdById"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1402
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1403
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getUserPwdById(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1409
    :goto_0
    return-object v1

    .line 1406
    :catch_0
    move-exception v0

    .line 1407
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API isL2TPSecretEnabled "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1409
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVpnList()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 1199
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.getVpnList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1201
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1202
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IVpnInfoPolicy;->getVPNList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1207
    :goto_0
    return-object v1

    .line 1204
    :catch_0
    move-exception v0

    .line 1205
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getVpnList"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1207
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAdminProfile(Ljava/lang/String;)Z
    .locals 6
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1147
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "VpnPolicy.isAdminProfile"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1148
    const/4 v1, 0x0

    .line 1150
    .local v1, "isAdminProfile":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1151
    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v3, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v4, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v5, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->getId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IVpnInfoPolicy;->isAdminProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1156
    .end local v1    # "isAdminProfile":Z
    :cond_0
    :goto_0
    return v1

    .line 1153
    .restart local v1    # "isAdminProfile":Z
    :catch_0
    move-exception v0

    .line 1154
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at Vpn policy API isAdminProfile"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public isL2TPSecretEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1336
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.isL2TPSecretEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1338
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1339
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->isL2TPSecretEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1345
    :goto_0
    return v1

    .line 1342
    :catch_0
    move-exception v0

    .line 1343
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API isL2TPSecretEnabled "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1345
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isOnlySecureConnectionsAllowed()Z
    .locals 3

    .prologue
    .line 1990
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.isOnlySecureConnectionsAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1992
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1993
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IVpnInfoPolicy;->isOnlySecureConnectionsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1998
    :goto_0
    return v1

    .line 1995
    :catch_0
    move-exception v0

    .line 1996
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API isOnlySecureConnectionsAllowed "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1998
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isPPTPEncryptionEnabled(Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 815
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.isPPTPEncryptionEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 817
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 818
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->isPPTPEncryptionEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 823
    :goto_0
    return v1

    .line 820
    :catch_0
    move-exception v0

    .line 821
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API getCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 823
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isUserAddProfilesAllowed()Z
    .locals 1

    .prologue
    .line 2408
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/VpnPolicy;->isUserAddProfilesAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUserAddProfilesAllowed(Z)Z
    .locals 4
    .param p1, "showMsg"    # Z

    .prologue
    .line 2416
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2417
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->isUserAddProfilesAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2422
    :goto_0
    return v1

    .line 2419
    :catch_0
    move-exception v0

    .line 2420
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2422
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUserChangeProfilesAllowed()Z
    .locals 1

    .prologue
    .line 2308
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/VpnPolicy;->isUserChangeProfilesAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUserChangeProfilesAllowed(Z)Z
    .locals 4
    .param p1, "showMsg"    # Z

    .prologue
    .line 2316
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2317
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->isUserChangeProfilesAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2322
    :goto_0
    return v1

    .line 2319
    :catch_0
    move-exception v0

    .line 2320
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2322
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isUserSetAlwaysOnAllowed()Z
    .locals 1

    .prologue
    .line 2209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/VpnPolicy;->isUserSetAlwaysOnAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public isUserSetAlwaysOnAllowed(Z)Z
    .locals 4
    .param p1, "showMsg"    # Z

    .prologue
    .line 2217
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2218
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->isUserSetAlwaysOnAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2223
    :goto_0
    return v1

    .line 2220
    :catch_0
    move-exception v0

    .line 2221
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2223
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setAlwaysOnProfile(Ljava/lang/String;)Z
    .locals 4
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 2072
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2073
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->setAlwaysOnProfile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2078
    :goto_0
    return v1

    .line 2075
    :catch_0
    move-exception v0

    .line 2076
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to communicate with Vpn Policy service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2078
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDnsDomains(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1612
    .local p2, "searchDomains":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setDnsDomains"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1614
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1615
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setDnsDomains(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1620
    :goto_0
    return v1

    .line 1617
    :catch_0
    move-exception v0

    .line 1618
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setDnsDomains "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1620
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDnsServers(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1496
    .local p2, "dnsServers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setDnsServers"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1498
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1499
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setDnsServers(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1504
    :goto_0
    return v1

    .line 1501
    :catch_0
    move-exception v0

    .line 1502
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setDnsServers "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1504
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setForwardRoutes(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1733
    .local p2, "routes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setForwardRoutes"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1735
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1736
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setForwardRoutes(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1741
    :goto_0
    return v1

    .line 1738
    :catch_0
    move-exception v0

    .line 1739
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setForwardRoutes "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1741
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIPSecCaCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "certificate"    # Ljava/lang/String;

    .prologue
    .line 573
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setIPSecCaCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 575
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 576
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setCaCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 581
    :goto_0
    return v1

    .line 578
    :catch_0
    move-exception v0

    .line 579
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 581
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIPSecPreSharedKey(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "psk"    # Ljava/lang/String;

    .prologue
    .line 475
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setIPSecPreSharedKey"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 477
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 478
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setPresharedKey(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 483
    :goto_0
    return v1

    .line 480
    :catch_0
    move-exception v0

    .line 481
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setSharedSecret"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 483
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIPSecUserCertificate(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "certificate"    # Ljava/lang/String;

    .prologue
    .line 703
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setIPSecUserCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 705
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 706
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setUserCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 711
    :goto_0
    return v1

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 711
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setId(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    .line 304
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 306
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 307
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 309
    :catch_0
    move-exception v0

    .line 310
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setId"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setIpSecIdentifier(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "ipSecIdentifier"    # Ljava/lang/String;

    .prologue
    .line 1844
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setIpSecIdentifier"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1846
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1847
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setIpSecIdentifier(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1852
    :goto_0
    return v1

    .line 1849
    :catch_0
    move-exception v0

    .line 1850
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setIpSecIdentifier "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1852
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setL2TPSecret(Ljava/lang/String;ZLjava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z
    .param p3, "secret"    # Ljava/lang/String;

    .prologue
    .line 1261
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setL2TPSecret"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1263
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1264
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IVpnInfoPolicy;->setL2TPSecret(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ZLjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1269
    :goto_0
    return v1

    .line 1266
    :catch_0
    move-exception v0

    .line 1267
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setL2TPSecret"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1269
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOcspServerUrl(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "ocspServerUrl"    # Ljava/lang/String;

    .prologue
    .line 2474
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setOcspServerUrl"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2476
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2477
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setOcspServerUrl(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2482
    :goto_0
    return v1

    .line 2479
    :catch_0
    move-exception v0

    .line 2480
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setOcspServerUrl "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2482
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPPTPEncryptionEnabled(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "enabled"    # Z

    .prologue
    .line 779
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setPPTPEncryptionEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 781
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 782
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setEncryptionEnabledForPPTP(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 787
    :goto_0
    return v1

    .line 784
    :catch_0
    move-exception v0

    .line 785
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setCaCertificate"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 787
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setProfileName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "oldProfileName"    # Ljava/lang/String;
    .param p2, "newProfileName"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setProfileName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 216
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :cond_0
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 220
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setServerName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "serverName"    # Ljava/lang/String;

    .prologue
    .line 250
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setServerName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 252
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setServerName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 255
    :catch_0
    move-exception v0

    .line 256
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setServerName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setUserName(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;

    .prologue
    .line 359
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setUserName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 361
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setUserName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 367
    :goto_0
    return v1

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setServerName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUserPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;
    .param p2, "userPassword"    # Ljava/lang/String;

    .prologue
    .line 415
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setUserPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 417
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 418
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    iget-object v2, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IVpnInfoPolicy;->setUserPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 423
    :goto_0
    return v1

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setServerName"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 423
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVpnProfile(Ljava/lang/String;)Z
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 1431
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "VpnPolicy.setVpnProfile"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1433
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/VpnPolicy;->getService()Landroid/app/enterprise/IVpnInfoPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1434
    iget-object v1, p0, Landroid/app/enterprise/VpnPolicy;->lService:Landroid/app/enterprise/IVpnInfoPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IVpnInfoPolicy;->setVpnProfile(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1439
    :goto_0
    return v1

    .line 1436
    :catch_0
    move-exception v0

    .line 1437
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/VpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at Vpn policy API setVpnProfile "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1439
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
