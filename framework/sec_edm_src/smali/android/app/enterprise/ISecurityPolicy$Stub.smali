.class public abstract Landroid/app/enterprise/ISecurityPolicy$Stub;
.super Landroid/os/Binder;
.source "ISecurityPolicy.java"

# interfaces
.implements Landroid/app/enterprise/ISecurityPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/ISecurityPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/ISecurityPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.ISecurityPolicy"

.field static final TRANSACTION_changeCredentialStoragePassword:I = 0x1a

.field static final TRANSACTION_clearInstalledCertificates:I = 0x18

.field static final TRANSACTION_deleteCertificateFromKeystore:I = 0x2b

.field static final TRANSACTION_deleteCertificateFromUserKeystore:I = 0x2e

.field static final TRANSACTION_enableRebootBanner:I = 0x22

.field static final TRANSACTION_enableRebootBannerWithText:I = 0x2f

.field static final TRANSACTION_formatInternalStorage:I = 0x2

.field static final TRANSACTION_formatSelective:I = 0x1

.field static final TRANSACTION_formatStorageCard:I = 0x3

.field static final TRANSACTION_getCertificatesFromKeystore:I = 0x2a

.field static final TRANSACTION_getCertificatesFromUserKeystore:I = 0x2d

.field static final TRANSACTION_getCredentialStorageStatus:I = 0x1b

.field static final TRANSACTION_getDeviceLastAccessDate:I = 0x28

.field static final TRANSACTION_getFileNamesOnDevice:I = 0xe

.field static final TRANSACTION_getFileNamesWithAttributes:I = 0xf

.field static final TRANSACTION_getFileNamesWithAttributesRecursive:I = 0x10

.field static final TRANSACTION_getInstalledCertificate:I = 0x12

.field static final TRANSACTION_getInstalledCertificateNames:I = 0x17

.field static final TRANSACTION_getInstalledCertificates:I = 0x16

.field static final TRANSACTION_getRebootBannerText:I = 0x30

.field static final TRANSACTION_getRequireDeviceEncryption:I = 0x9

.field static final TRANSACTION_getRequireStorageCardEncryption:I = 0xb

.field static final TRANSACTION_getSystemCertificates:I = 0x15

.field static final TRANSACTION_installCertificate:I = 0x1e

.field static final TRANSACTION_installCertificateToKeystore:I = 0x29

.field static final TRANSACTION_installCertificateToUserKeystore:I = 0x2c

.field static final TRANSACTION_installCertificateWithType:I = 0x13

.field static final TRANSACTION_installCertificatesFromSdCard:I = 0x14

.field static final TRANSACTION_isDodBannerVisible:I = 0x25

.field static final TRANSACTION_isDodBannerVisibleAsUser:I = 0x26

.field static final TRANSACTION_isExternalStorageEncrypted:I = 0x7

.field static final TRANSACTION_isInternalStorageEncrypted:I = 0x6

.field static final TRANSACTION_isRebootBannerEnabled:I = 0x23

.field static final TRANSACTION_lockoutDevice:I = 0x20

.field static final TRANSACTION_powerOffDevice:I = 0xc

.field static final TRANSACTION_readFile:I = 0x11

.field static final TRANSACTION_removeAccountsByType:I = 0xd

.field static final TRANSACTION_removeCertificate:I = 0x1f

.field static final TRANSACTION_removeDeviceLockout:I = 0x21

.field static final TRANSACTION_resetCredentialStorage:I = 0x1d

.field static final TRANSACTION_setCredentialStoragePassword:I = 0x19

.field static final TRANSACTION_setDeviceLastAccessDate:I = 0x27

.field static final TRANSACTION_setDodBannerVisibleStatus:I = 0x24

.field static final TRANSACTION_setExternalStorageEncryption:I = 0x5

.field static final TRANSACTION_setInternalStorageEncryption:I = 0x4

.field static final TRANSACTION_setRequireDeviceEncryption:I = 0x8

.field static final TRANSACTION_setRequireStorageCardEncryption:I = 0xa

.field static final TRANSACTION_unlockCredentialStorage:I = 0x1c


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/ISecurityPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/ISecurityPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.ISecurityPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/ISecurityPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/ISecurityPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/ISecurityPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/ISecurityPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 915
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 46
    :sswitch_0
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    .line 63
    .local v3, "_arg2":[Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->formatSelective(Landroid/app/enterprise/ContextInfo;[Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 64
    .local v8, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 65
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 66
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v8    # "_result":[Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 70
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 79
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v2, 0x1

    .line 81
    .local v2, "_arg1":Z
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v3, 0x1

    .line 82
    .local v3, "_arg2":Z
    :goto_4
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->formatInternalStorage(Landroid/app/enterprise/ContextInfo;ZZ)Z

    move-result v8

    .line 83
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v8, :cond_4

    const/4 v0, 0x1

    :goto_5
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    const/4 v0, 0x1

    goto :goto_0

    .line 76
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Z
    .end local v8    # "_result":Z
    :cond_1
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 79
    :cond_2
    const/4 v2, 0x0

    goto :goto_3

    .line 81
    .restart local v2    # "_arg1":Z
    :cond_3
    const/4 v3, 0x0

    goto :goto_4

    .line 84
    .restart local v3    # "_arg2":Z
    .restart local v8    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 89
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Z
    .end local v8    # "_result":Z
    :sswitch_3
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 92
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 98
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    const/4 v2, 0x1

    .line 99
    .restart local v2    # "_arg1":Z
    :goto_7
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->formatStorageCard(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v8

    .line 100
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 101
    if-eqz v8, :cond_7

    const/4 v0, 0x1

    :goto_8
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 95
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :cond_5
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 98
    :cond_6
    const/4 v2, 0x0

    goto :goto_7

    .line 101
    .restart local v2    # "_arg1":Z
    .restart local v8    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    goto :goto_8

    .line 106
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :sswitch_4
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 109
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 115
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    const/4 v2, 0x1

    .line 116
    .restart local v2    # "_arg1":Z
    :goto_a
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setInternalStorageEncryption(Landroid/app/enterprise/ContextInfo;Z)V

    .line 117
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 118
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 112
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 115
    :cond_9
    const/4 v2, 0x0

    goto :goto_a

    .line 122
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 124
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 125
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 131
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    const/4 v2, 0x1

    .line 132
    .restart local v2    # "_arg1":Z
    :goto_c
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setExternalStorageEncryption(Landroid/app/enterprise/ContextInfo;Z)V

    .line 133
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 134
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 128
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    :cond_a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 131
    :cond_b
    const/4 v2, 0x0

    goto :goto_c

    .line 138
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 141
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 146
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->isInternalStorageEncrypted(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 147
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v8, :cond_d

    const/4 v0, 0x1

    :goto_e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 149
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 144
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 148
    .restart local v8    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    goto :goto_e

    .line 153
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_7
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_e

    .line 156
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 161
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->isExternalStorageEncrypted(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 162
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 163
    if-eqz v8, :cond_f

    const/4 v0, 0x1

    :goto_10
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 164
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 159
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 163
    .restart local v8    # "_result":Z
    :cond_f
    const/4 v0, 0x0

    goto :goto_10

    .line 168
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_8
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 171
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 177
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 178
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 184
    .local v2, "_arg1":Landroid/content/ComponentName;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    const/4 v3, 0x1

    .line 185
    .restart local v3    # "_arg2":Z
    :goto_13
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setRequireDeviceEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V

    .line 186
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 174
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":Z
    :cond_10
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 181
    :cond_11
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_12

    .line 184
    :cond_12
    const/4 v3, 0x0

    goto :goto_13

    .line 191
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_9
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 194
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 200
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    .line 201
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 206
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_15
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getRequireDeviceEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z

    move-result v8

    .line 207
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    if-eqz v8, :cond_15

    const/4 v0, 0x1

    :goto_16
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 197
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_result":Z
    :cond_13
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_14

    .line 204
    :cond_14
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_15

    .line 208
    .restart local v8    # "_result":Z
    :cond_15
    const/4 v0, 0x0

    goto :goto_16

    .line 213
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_result":Z
    :sswitch_a
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    .line 216
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 222
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 223
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 229
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_18

    const/4 v3, 0x1

    .line 230
    .restart local v3    # "_arg2":Z
    :goto_19
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setRequireStorageCardEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;Z)V

    .line 231
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 232
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 219
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v3    # "_arg2":Z
    :cond_16
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_17

    .line 226
    :cond_17
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_18

    .line 229
    :cond_18
    const/4 v3, 0x0

    goto :goto_19

    .line 236
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    :sswitch_b
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 239
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 245
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1a

    .line 246
    sget-object v0, Landroid/content/ComponentName;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ComponentName;

    .line 251
    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    :goto_1b
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getRequireStorageCardEncryption(Landroid/app/enterprise/ContextInfo;Landroid/content/ComponentName;)Z

    move-result v8

    .line 252
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 253
    if-eqz v8, :cond_1b

    const/4 v0, 0x1

    :goto_1c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 254
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 242
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_result":Z
    :cond_19
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1a

    .line 249
    :cond_1a
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/content/ComponentName;
    goto :goto_1b

    .line 253
    .restart local v8    # "_result":Z
    :cond_1b
    const/4 v0, 0x0

    goto :goto_1c

    .line 258
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/content/ComponentName;
    .end local v8    # "_result":Z
    :sswitch_c
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 261
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 266
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->powerOffDevice(Landroid/app/enterprise/ContextInfo;)V

    .line 267
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 268
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 264
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_1c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    .line 272
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 275
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 281
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 282
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->removeAccountsByType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v8

    .line 283
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 284
    if-eqz v8, :cond_1e

    const/4 v0, 0x1

    :goto_1f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 285
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 278
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_1d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 284
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_1e
    const/4 v0, 0x0

    goto :goto_1f

    .line 289
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_e
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 291
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 292
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 298
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 299
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getFileNamesOnDevice(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 300
    .local v10, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 301
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 302
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 295
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 306
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 308
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_20

    .line 309
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 315
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 316
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getFileNamesWithAttributes(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 317
    .restart local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 318
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 319
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 312
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_20
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 323
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 325
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    .line 326
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 332
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 334
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 335
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getFileNamesWithAttributesRecursive(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 336
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 337
    if-eqz v8, :cond_22

    const/4 v0, 0x1

    :goto_23
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 338
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 329
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_21
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 337
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_22
    const/4 v0, 0x0

    goto :goto_23

    .line 342
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_11
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    .line 345
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 351
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 353
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_24

    .line 354
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/ParcelFileDescriptor;

    .line 359
    .local v3, "_arg2":Landroid/os/ParcelFileDescriptor;
    :goto_25
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->readFile(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;)Z

    move-result v8

    .line 360
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 361
    if-eqz v8, :cond_25

    const/4 v0, 0x1

    :goto_26
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 362
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 348
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/os/ParcelFileDescriptor;
    .end local v8    # "_result":Z
    :cond_23
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 357
    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_24
    const/4 v3, 0x0

    .restart local v3    # "_arg2":Landroid/os/ParcelFileDescriptor;
    goto :goto_25

    .line 361
    .restart local v8    # "_result":Z
    :cond_25
    const/4 v0, 0x0

    goto :goto_26

    .line 366
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Landroid/os/ParcelFileDescriptor;
    .end local v8    # "_result":Z
    :sswitch_12
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 368
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_26

    .line 369
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 375
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 376
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getInstalledCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;

    move-result-object v8

    .line 377
    .local v8, "_result":Landroid/app/enterprise/CertificateInfo;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 378
    if-eqz v8, :cond_27

    .line 379
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 380
    const/4 v0, 0x1

    invoke-virtual {v8, p3, v0}, Landroid/app/enterprise/CertificateInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 385
    :goto_28
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 372
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Landroid/app/enterprise/CertificateInfo;
    :cond_26
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 383
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Landroid/app/enterprise/CertificateInfo;
    :cond_27
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_28

    .line 389
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Landroid/app/enterprise/CertificateInfo;
    :sswitch_13
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 391
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_28

    .line 392
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 398
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 400
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 401
    .local v3, "_arg2":[B
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->installCertificateWithType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[B)V

    .line 402
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 403
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 395
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    :cond_28
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 407
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 409
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_29

    .line 410
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 415
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2a
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->installCertificatesFromSdCard(Landroid/app/enterprise/ContextInfo;)V

    .line 416
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 417
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 413
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_29
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2a

    .line 421
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_15
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 423
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2a

    .line 424
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 429
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getSystemCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v9

    .line 430
    .local v9, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 431
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 432
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 427
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_2a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 436
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 438
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2b

    .line 439
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 444
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getInstalledCertificates(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v9

    .line 445
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 446
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 447
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 442
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_2b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 451
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 453
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2c

    .line 454
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 460
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 461
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getInstalledCertificateNames(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 462
    .restart local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 463
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 464
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 457
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v10    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 468
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 470
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2d

    .line 471
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 476
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->clearInstalledCertificates(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 477
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 478
    if-eqz v8, :cond_2e

    const/4 v0, 0x1

    :goto_2f
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 479
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 474
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_2d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 478
    .restart local v8    # "_result":Z
    :cond_2e
    const/4 v0, 0x0

    goto :goto_2f

    .line 483
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_19
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 485
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2f

    .line 486
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 492
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 493
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setCredentialStoragePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v8

    .line 494
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 495
    if-eqz v8, :cond_30

    const/4 v0, 0x1

    :goto_31
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 496
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 489
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_2f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 495
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_30
    const/4 v0, 0x0

    goto :goto_31

    .line 500
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_1a
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 502
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_31

    .line 503
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 509
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 511
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 512
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->changeCredentialStoragePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 513
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 514
    if-eqz v8, :cond_32

    const/4 v0, 0x1

    :goto_33
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 515
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 506
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_31
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 514
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_32
    const/4 v0, 0x0

    goto :goto_33

    .line 519
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_1b
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 521
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_33

    .line 522
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 527
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getCredentialStorageStatus(Landroid/app/enterprise/ContextInfo;)I

    move-result v8

    .line 528
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 529
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 530
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 525
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":I
    :cond_33
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 534
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 536
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_34

    .line 537
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 543
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 544
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->unlockCredentialStorage(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v8

    .line 545
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 546
    if-eqz v8, :cond_35

    const/4 v0, 0x1

    :goto_36
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 547
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 540
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_34
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 546
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_35
    const/4 v0, 0x0

    goto :goto_36

    .line 551
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_1d
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 553
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_36

    .line 554
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 559
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->resetCredentialStorage(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 560
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 561
    if-eqz v8, :cond_37

    const/4 v0, 0x1

    :goto_38
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 562
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 557
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_36
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 561
    .restart local v8    # "_result":Z
    :cond_37
    const/4 v0, 0x0

    goto :goto_38

    .line 566
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_1e
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 568
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_38

    .line 569
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 575
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 577
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 579
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 581
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .local v5, "_arg4":Ljava/lang/String;
    move-object v0, p0

    .line 582
    invoke-virtual/range {v0 .. v5}, Landroid/app/enterprise/ISecurityPolicy$Stub;->installCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 583
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 584
    if-eqz v8, :cond_39

    const/4 v0, 0x1

    :goto_3a
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 585
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 572
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_38
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 584
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":[B
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v5    # "_arg4":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_39
    const/4 v0, 0x0

    goto :goto_3a

    .line 589
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_1f
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 591
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3a

    .line 592
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 598
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3b
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 600
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 601
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->removeCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    .line 602
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 603
    if-eqz v8, :cond_3b

    const/4 v0, 0x1

    :goto_3c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 604
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 595
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_3a
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3b

    .line 603
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_3b
    const/4 v0, 0x0

    goto :goto_3c

    .line 608
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_20
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 610
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3c

    .line 611
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 617
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 619
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 621
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v7

    .line 622
    .local v7, "_arg3":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v1, v2, v3, v7}, Landroid/app/enterprise/ISecurityPolicy$Stub;->lockoutDevice(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 623
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 624
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 614
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v7    # "_arg3":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3d

    .line 628
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 630
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3d

    .line 631
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 636
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3e
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->removeDeviceLockout(Landroid/app/enterprise/ContextInfo;)V

    .line 637
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 638
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 634
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_3d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3e

    .line 642
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 644
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3e

    .line 645
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 651
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3f

    const/4 v2, 0x1

    .line 652
    .local v2, "_arg1":Z
    :goto_40
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->enableRebootBanner(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v8

    .line 653
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 654
    if-eqz v8, :cond_40

    const/4 v0, 0x1

    :goto_41
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 655
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 648
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :cond_3e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 651
    :cond_3f
    const/4 v2, 0x0

    goto :goto_40

    .line 654
    .restart local v2    # "_arg1":Z
    .restart local v8    # "_result":Z
    :cond_40
    const/4 v0, 0x0

    goto :goto_41

    .line 659
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :sswitch_23
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 661
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_41

    .line 662
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 667
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_42
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->isRebootBannerEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 668
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 669
    if-eqz v8, :cond_42

    const/4 v0, 0x1

    :goto_43
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 670
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 665
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_41
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_42

    .line 669
    .restart local v8    # "_result":Z
    :cond_42
    const/4 v0, 0x0

    goto :goto_43

    .line 674
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_24
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 676
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_43

    .line 677
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 683
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_44
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_44

    const/4 v2, 0x1

    .line 684
    .restart local v2    # "_arg1":Z
    :goto_45
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setDodBannerVisibleStatus(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v8

    .line 685
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 686
    if-eqz v8, :cond_45

    const/4 v0, 0x1

    :goto_46
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 687
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 680
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :cond_43
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_44

    .line 683
    :cond_44
    const/4 v2, 0x0

    goto :goto_45

    .line 686
    .restart local v2    # "_arg1":Z
    .restart local v8    # "_result":Z
    :cond_45
    const/4 v0, 0x0

    goto :goto_46

    .line 691
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v8    # "_result":Z
    :sswitch_25
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 693
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_46

    .line 694
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 699
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_47
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->isDodBannerVisible(Landroid/app/enterprise/ContextInfo;)Z

    move-result v8

    .line 700
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 701
    if-eqz v8, :cond_47

    const/4 v0, 0x1

    :goto_48
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 702
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 697
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :cond_46
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_47

    .line 701
    .restart local v8    # "_result":Z
    :cond_47
    const/4 v0, 0x0

    goto :goto_48

    .line 706
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Z
    :sswitch_26
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 708
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 709
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->isDodBannerVisibleAsUser(I)Z

    move-result v8

    .line 710
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 711
    if-eqz v8, :cond_48

    const/4 v0, 0x1

    :goto_49
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 712
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 711
    :cond_48
    const/4 v0, 0x0

    goto :goto_49

    .line 716
    .end local v1    # "_arg0":I
    .end local v8    # "_result":Z
    :sswitch_27
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 718
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_49

    .line 719
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 725
    .local v1, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 726
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->setDeviceLastAccessDate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v8

    .line 727
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 728
    if-eqz v8, :cond_4a

    const/4 v0, 0x1

    :goto_4b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 729
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 722
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_49
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4a

    .line 728
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_4a
    const/4 v0, 0x0

    goto :goto_4b

    .line 733
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_28
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 735
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4b

    .line 736
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 741
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getDeviceLastAccessDate(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v8

    .line 742
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 743
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 744
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 739
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/lang/String;
    :cond_4b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4c

    .line 748
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_29
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 750
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4c

    .line 751
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 757
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 759
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 761
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 763
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 765
    .restart local v5    # "_arg4":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .local v6, "_arg5":I
    move-object v0, p0

    .line 766
    invoke-virtual/range {v0 .. v6}, Landroid/app/enterprise/ISecurityPolicy$Stub;->installCertificateToKeystore(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 767
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 768
    if-eqz v8, :cond_4d

    const/4 v0, 0x1

    :goto_4e
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 769
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 754
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v6    # "_arg5":I
    .end local v8    # "_result":Z
    :cond_4c
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4d

    .line 768
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":[B
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v5    # "_arg4":Ljava/lang/String;
    .restart local v6    # "_arg5":I
    .restart local v8    # "_result":Z
    :cond_4d
    const/4 v0, 0x0

    goto :goto_4e

    .line 773
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v6    # "_arg5":I
    .end local v8    # "_result":Z
    :sswitch_2a
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 775
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4e

    .line 776
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 782
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 784
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 785
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getCertificatesFromKeystore(Landroid/app/enterprise/ContextInfo;II)Ljava/util/List;

    move-result-object v9

    .line 786
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 787
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 788
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 779
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_4e
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4f

    .line 792
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2b
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 794
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4f

    .line 795
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 801
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_50
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_50

    .line 802
    sget-object v0, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/CertificateInfo;

    .line 808
    .local v2, "_arg1":Landroid/app/enterprise/CertificateInfo;
    :goto_51
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 809
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->deleteCertificateFromKeystore(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/CertificateInfo;I)Z

    move-result v8

    .line 810
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 811
    if-eqz v8, :cond_51

    const/4 v0, 0x1

    :goto_52
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 812
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 798
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :cond_4f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_50

    .line 805
    :cond_50
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    goto :goto_51

    .line 811
    .restart local v3    # "_arg2":I
    .restart local v8    # "_result":Z
    :cond_51
    const/4 v0, 0x0

    goto :goto_52

    .line 816
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :sswitch_2c
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 818
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_52

    .line 819
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 825
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_53
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 827
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 829
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 831
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 833
    .restart local v5    # "_arg4":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .restart local v6    # "_arg5":I
    move-object v0, p0

    .line 834
    invoke-virtual/range {v0 .. v6}, Landroid/app/enterprise/ISecurityPolicy$Stub;->installCertificateToUserKeystore(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;[BLjava/lang/String;Ljava/lang/String;I)Z

    move-result v8

    .line 835
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 836
    if-eqz v8, :cond_53

    const/4 v0, 0x1

    :goto_54
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 837
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 822
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v6    # "_arg5":I
    .end local v8    # "_result":Z
    :cond_52
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_53

    .line 836
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":[B
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v5    # "_arg4":Ljava/lang/String;
    .restart local v6    # "_arg5":I
    .restart local v8    # "_result":Z
    :cond_53
    const/4 v0, 0x0

    goto :goto_54

    .line 841
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v5    # "_arg4":Ljava/lang/String;
    .end local v6    # "_arg5":I
    .end local v8    # "_result":Z
    :sswitch_2d
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 843
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_54

    .line 844
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 850
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_55
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 851
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getCertificatesFromUserKeystore(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v9

    .line 852
    .restart local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 853
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 854
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 847
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":I
    .end local v9    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/CertificateInfo;>;"
    :cond_54
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_55

    .line 858
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 860
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_55

    .line 861
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 867
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_56

    .line 868
    sget-object v0, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/CertificateInfo;

    .line 874
    .local v2, "_arg1":Landroid/app/enterprise/CertificateInfo;
    :goto_57
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 875
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->deleteCertificateFromUserKeystore(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/CertificateInfo;I)Z

    move-result v8

    .line 876
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 877
    if-eqz v8, :cond_57

    const/4 v0, 0x1

    :goto_58
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 878
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 864
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :cond_55
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_56

    .line 871
    :cond_56
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    goto :goto_57

    .line 877
    .restart local v3    # "_arg2":I
    .restart local v8    # "_result":Z
    :cond_57
    const/4 v0, 0x0

    goto :goto_58

    .line 882
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/CertificateInfo;
    .end local v3    # "_arg2":I
    .end local v8    # "_result":Z
    :sswitch_2f
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 884
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_58

    .line 885
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 891
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_59
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_59

    const/4 v2, 0x1

    .line 893
    .local v2, "_arg1":Z
    :goto_5a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 894
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Landroid/app/enterprise/ISecurityPolicy$Stub;->enableRebootBannerWithText(Landroid/app/enterprise/ContextInfo;ZLjava/lang/String;)Z

    move-result v8

    .line 895
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 896
    if-eqz v8, :cond_5a

    const/4 v0, 0x1

    :goto_5b
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 897
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 888
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :cond_58
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_59

    .line 891
    :cond_59
    const/4 v2, 0x0

    goto :goto_5a

    .line 896
    .restart local v2    # "_arg1":Z
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_result":Z
    :cond_5a
    const/4 v0, 0x0

    goto :goto_5b

    .line 901
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v8    # "_result":Z
    :sswitch_30
    const-string v0, "android.app.enterprise.ISecurityPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 903
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5b

    .line 904
    sget-object v0, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/ContextInfo;

    .line 909
    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5c
    invoke-virtual {p0, v1}, Landroid/app/enterprise/ISecurityPolicy$Stub;->getRebootBannerText(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v8

    .line 910
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 911
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 912
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 907
    .end local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v8    # "_result":Ljava/lang/String;
    :cond_5b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5c

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
