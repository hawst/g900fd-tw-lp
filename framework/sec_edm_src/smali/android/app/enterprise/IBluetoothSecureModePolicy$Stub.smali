.class public abstract Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;
.super Landroid/os/Binder;
.source "IBluetoothSecureModePolicy.java"

# interfaces
.implements Landroid/app/enterprise/IBluetoothSecureModePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IBluetoothSecureModePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IBluetoothSecureModePolicy"

.field static final TRANSACTION_addBluetoothDevicesToWhiteList:I = 0x8

.field static final TRANSACTION_disableSecureMode:I = 0x2

.field static final TRANSACTION_enableDeviceWhiteList:I = 0x5

.field static final TRANSACTION_enableSecureMode:I = 0x1

.field static final TRANSACTION_getBluetoothDevicesFromWhiteList:I = 0x7

.field static final TRANSACTION_getSecureModeConfiguration:I = 0x3

.field static final TRANSACTION_isDeviceWhiteListEnabled:I = 0x6

.field static final TRANSACTION_isSecureModeEnabled:I = 0x4

.field static final TRANSACTION_removeBluetoothDevicesFromWhiteList:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothSecureModePolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IBluetoothSecureModePolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Landroid/app/enterprise/IBluetoothSecureModePolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 205
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 45
    :sswitch_0
    const-string v6, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1

    .line 53
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_2

    .line 60
    sget-object v8, Landroid/app/enterprise/BluetoothSecureModeConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/BluetoothSecureModeConfig;

    .line 66
    .local v1, "_arg1":Landroid/app/enterprise/BluetoothSecureModeConfig;
    :goto_2
    sget-object v8, Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v3

    .line 67
    .local v3, "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->enableSecureMode(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/BluetoothSecureModeConfig;Ljava/util/List;)Z

    move-result v4

    .line 68
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 69
    if-eqz v4, :cond_0

    move v6, v7

    :cond_0
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Landroid/app/enterprise/BluetoothSecureModeConfig;
    .end local v3    # "_arg2":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    .end local v4    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 63
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg1":Landroid/app/enterprise/BluetoothSecureModeConfig;
    goto :goto_2

    .line 74
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Landroid/app/enterprise/BluetoothSecureModeConfig;
    :sswitch_2
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4

    .line 77
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 82
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->disableSecureMode(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 83
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 84
    if-eqz v4, :cond_3

    move v6, v7

    :cond_3
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 80
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 89
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5

    .line 92
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 97
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->getSecureModeConfiguration(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/BluetoothSecureModeConfig;

    move-result-object v4

    .line 98
    .local v4, "_result":Landroid/app/enterprise/BluetoothSecureModeConfig;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 99
    if-eqz v4, :cond_6

    .line 100
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeInt(I)V

    .line 101
    invoke-virtual {v4, p3, v7}, Landroid/app/enterprise/BluetoothSecureModeConfig;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 95
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Landroid/app/enterprise/BluetoothSecureModeConfig;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 104
    .restart local v4    # "_result":Landroid/app/enterprise/BluetoothSecureModeConfig;
    :cond_6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 110
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Landroid/app/enterprise/BluetoothSecureModeConfig;
    :sswitch_4
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_8

    .line 113
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 118
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->isSecureModeEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 119
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    if-eqz v4, :cond_7

    move v6, v7

    :cond_7
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 116
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 125
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_a

    .line 128
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 134
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_b

    move v1, v7

    .line 135
    .local v1, "_arg1":Z
    :goto_7
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->enableDeviceWhiteList(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 136
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 137
    if-eqz v4, :cond_9

    move v6, v7

    :cond_9
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 131
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    :cond_b
    move v1, v6

    .line 134
    goto :goto_7

    .line 142
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d

    .line 145
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 150
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->isDeviceWhiteListEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 151
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 152
    if-eqz v4, :cond_c

    move v6, v7

    :cond_c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 148
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 157
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v6, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_e

    .line 160
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 165
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->getBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v5

    .line 166
    .local v5, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 167
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 163
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v5    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 172
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 174
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10

    .line 175
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 181
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    sget-object v8, Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 182
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->addBluetoothDevicesToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 183
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v4, :cond_f

    move v6, v7

    :cond_f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 178
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    .end local v4    # "_result":Z
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 189
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v8, "android.app.enterprise.IBluetoothSecureModePolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_12

    .line 192
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 198
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    sget-object v8, Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 199
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->removeBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v4

    .line 200
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 201
    if-eqz v4, :cond_11

    move v6, v7

    :cond_11
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 195
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    .end local v4    # "_result":Z
    :cond_12
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
