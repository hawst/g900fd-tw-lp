.class public Landroid/app/enterprise/FirewallPolicy;
.super Ljava/lang/Object;
.source "FirewallPolicy.java"


# static fields
.field private static final ACCEPT:I = 0x1

.field public static final ALLOW_RULES:I = 0x1

.field private static final DATA_CONNECTION:I = 0x0

.field public static final DENY_RULES:I = 0x2

.field private static final DROP:I = 0x2

.field public static final EXCEPTION_RULES:I = 0x8

.field private static final LOCATION:I = 0x4

.field public static final MARKET_ALL_NETWORKS:I = 0x0

.field public static final MARKET_WIFI_ONLY:I = 0x1

.field private static final PROXY:I = 0x3

.field private static final REDIRECT:I = 0x0

.field private static final REDIRECT_EXCEPTION:I = 0x7

.field private static final REROUTE_DEST:I = 0x5

.field public static final REROUTE_RULES:I = 0x4

.field private static TAG:Ljava/lang/String; = null

.field private static final WIFI_CONNECTION:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IFirewallPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 184
    const-string v0, "FirewallPolicy"

    sput-object v0, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 266
    iput-object p2, p0, Landroid/app/enterprise/FirewallPolicy;->mContext:Landroid/content/Context;

    .line 267
    iput-object p1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 268
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 258
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    invoke-direct {p0, v0, p1}, Landroid/app/enterprise/FirewallPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    .line 259
    return-void
.end method

.method private checkEmptyList(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 1226
    if-nez p1, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return v0

    .line 1229
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1232
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getService()Landroid/app/enterprise/IFirewallPolicy;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    if-nez v0, :cond_0

    .line 272
    const-string v0, "firewall_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    .line 275
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    return-object v0
.end method


# virtual methods
.method public addIptablesAllowRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 342
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.addIptablesAllowRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 343
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 355
    :cond_0
    :goto_0
    return v1

    .line 346
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 349
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x1

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addIptablesDenyRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 428
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.addIptablesDenyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 429
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 441
    :cond_0
    :goto_0
    return v1

    .line 432
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 435
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x2

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1889
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.addIptablesRedirectExceptionsRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1890
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "addIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1891
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1893
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x7

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1898
    :goto_0
    return v1

    .line 1894
    :catch_0
    move-exception v0

    .line 1895
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on addIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1898
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addIptablesRerouteRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 515
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.addIptablesRerouteRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 516
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 528
    :cond_0
    :goto_0
    return v1

    .line 519
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 522
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public addRules(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "objRule"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2500
    if-nez p1, :cond_0

    move v0, v1

    .line 2520
    .end local p1    # "objRule":Ljava/lang/Object;
    :goto_0
    return v0

    .line 2504
    .restart local p1    # "objRule":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Landroid/app/enterprise/FirewallAllowRule;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2505
    check-cast v0, Landroid/app/enterprise/FirewallAllowRule;

    invoke-virtual {v0}, Landroid/app/enterprise/FirewallAllowRule;->getRules()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/FirewallPolicy;->addIptablesAllowRules(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 2506
    goto :goto_0

    .line 2508
    :cond_1
    instance-of v0, p1, Landroid/app/enterprise/FirewallDenyRule;

    if-eqz v0, :cond_2

    move-object v0, p1

    .line 2509
    check-cast v0, Landroid/app/enterprise/FirewallDenyRule;

    invoke-virtual {v0}, Landroid/app/enterprise/FirewallDenyRule;->getRules()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/FirewallPolicy;->addIptablesDenyRules(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 2510
    goto :goto_0

    .line 2512
    :cond_2
    instance-of v0, p1, Landroid/app/enterprise/FirewallRerouteRule;

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2513
    check-cast v0, Landroid/app/enterprise/FirewallRerouteRule;

    invoke-virtual {v0}, Landroid/app/enterprise/FirewallRerouteRule;->getRules()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/FirewallPolicy;->addIptablesRerouteRules(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 2514
    goto :goto_0

    .line 2516
    :cond_3
    instance-of v0, p1, Landroid/app/enterprise/FirewallExceptionRule;

    if-eqz v0, :cond_4

    .line 2517
    check-cast p1, Landroid/app/enterprise/FirewallExceptionRule;

    .end local p1    # "objRule":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/app/enterprise/FirewallExceptionRule;->getRules()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/enterprise/FirewallPolicy;->addIptablesRedirectExceptionsRules(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 2518
    goto :goto_0

    :cond_4
    move v0, v1

    .line 2520
    goto :goto_0
.end method

.method public cleanIptablesAllowRules()Z
    .locals 4

    .prologue
    .line 1031
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.cleanIptablesAllowRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1032
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1035
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1041
    :goto_0
    return v1

    .line 1037
    :catch_0
    move-exception v0

    .line 1038
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1041
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public cleanIptablesDenyRules()Z
    .locals 4

    .prologue
    .line 1059
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.cleanIptablesDenyRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1060
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1063
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x2

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1069
    :goto_0
    return v1

    .line 1065
    :catch_0
    move-exception v0

    .line 1066
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public cleanIptablesProxyRules()Z
    .locals 4

    .prologue
    .line 1140
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.cleanIptablesProxyRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1141
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1144
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1150
    :goto_0
    return v1

    .line 1146
    :catch_0
    move-exception v0

    .line 1147
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1150
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public cleanIptablesRedirectExceptionsRules()Z
    .locals 4

    .prologue
    .line 2047
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.cleanIptablesRedirectExceptionsRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2048
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "cleanIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2049
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2051
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x7

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2056
    :goto_0
    return v1

    .line 2052
    :catch_0
    move-exception v0

    .line 2053
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on cleanIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2056
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public cleanIptablesRerouteRules()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1087
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.cleanIptablesRerouteRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1088
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1091
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1097
    :cond_0
    :goto_0
    return v1

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public clearDnsPerApp()Z
    .locals 3

    .prologue
    .line 2242
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.clearDnsPerApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2243
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2245
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->clearDnsPerApp(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2250
    :goto_0
    return v1

    .line 2246
    :catch_0
    move-exception v0

    .line 2247
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on clearDnsPerApp"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2250
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearGlobalProxyEnable()Z
    .locals 3

    .prologue
    .line 2399
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.clearGlobalProxyEnable"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2400
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2402
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->clearGlobalProxyEnable(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2407
    :goto_0
    return v1

    .line 2403
    :catch_0
    move-exception v0

    .line 2404
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on clearGlobalProxyEnable"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2407
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDNSPerApp(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2185
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getDNSPerApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2186
    if-eqz p1, :cond_0

    .line 2187
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2189
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->getDNSPerApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2195
    :goto_0
    return-object v1

    .line 2190
    :catch_0
    move-exception v0

    .line 2191
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on getDNSPerApp"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2195
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGlobalProxy()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2358
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getGlobalProxy"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2359
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2361
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->getGlobalProxy(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2366
    :goto_0
    return-object v1

    .line 2362
    :catch_0
    move-exception v0

    .line 2363
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on getGlobalProxy"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2366
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIptablesAllowRules()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesAllowRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 678
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getIptablesAllowRules..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 679
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 680
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 682
    :try_start_0
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getIptablesAllowRules... 1"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 684
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v1

    .line 686
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getIptablesAllowRules..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    :cond_0
    :goto_0
    return-object v1

    .line 687
    :catch_0
    move-exception v0

    .line 688
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIptablesDenyRules()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 709
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesDenyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 710
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 712
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 715
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x2

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 721
    :cond_0
    :goto_0
    return-object v1

    .line 717
    :catch_0
    move-exception v0

    .line 718
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIptablesOption()Z
    .locals 3

    .prologue
    .line 828
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getIptablesOption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 829
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 831
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->isEnabledRules(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 836
    :goto_0
    return v1

    .line 832
    :catch_0
    move-exception v0

    .line 833
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 836
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIptablesProxyOption()Z
    .locals 3

    .prologue
    .line 851
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getIptablesProxyOption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 852
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 854
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->isEnabledProxy(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 859
    :goto_0
    return v1

    .line 855
    :catch_0
    move-exception v0

    .line 856
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIptablesProxyRules()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 768
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesProxyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 769
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 770
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 773
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x3

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 779
    :cond_0
    :goto_0
    return-object v1

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIptablesRedirectExceptionsRules()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2012
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesRedirectExceptionsRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2013
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getIptablesRedirectExceptions"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2014
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2016
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2018
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x7

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2023
    :cond_0
    :goto_0
    return-object v1

    .line 2019
    :catch_0
    move-exception v0

    .line 2020
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException on getIptablesRedirectExceptions"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIptablesRerouteRules()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 740
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesRerouteRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 741
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 743
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 746
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 752
    :cond_0
    :goto_0
    return-object v1

    .line 748
    :catch_0
    move-exception v0

    .line 749
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getIptablesRules()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 801
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getIptablesRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 802
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 803
    .local v1, "ret":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 805
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->getAllRulesForUid(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 810
    :cond_0
    :goto_0
    return-object v1

    .line 806
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getNetworkForMarket()I
    .locals 4

    .prologue
    .line 1690
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getNetworkForMarket"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1691
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1693
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->getNetworkForMarket(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1698
    :goto_0
    return v1

    .line 1694
    :catch_0
    move-exception v0

    .line 1695
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getNetworkForMarket!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1698
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getRules(I)Ljava/util/List;
    .locals 7
    .param p1, "ruleType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2537
    move v5, p1

    .line 2538
    .local v5, "type":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2544
    .local v3, "myList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Object;>;"
    if-ltz p1, :cond_0

    const/16 v6, 0x10

    if-lt p1, v6, :cond_1

    .line 2577
    :cond_0
    :goto_0
    return-object v3

    .line 2548
    :cond_1
    const/4 v0, 0x0

    .line 2549
    .local v0, "allowRule":Landroid/app/enterprise/FirewallAllowRule;
    const/4 v1, 0x0

    .line 2550
    .local v1, "denyRule":Landroid/app/enterprise/FirewallDenyRule;
    const/4 v4, 0x0

    .line 2551
    .local v4, "rerouteRule":Landroid/app/enterprise/FirewallRerouteRule;
    const/4 v2, 0x0

    .line 2553
    .local v2, "exceptionRule":Landroid/app/enterprise/FirewallExceptionRule;
    and-int/lit8 v6, v5, 0x1

    if-eqz v6, :cond_2

    .line 2554
    new-instance v0, Landroid/app/enterprise/FirewallAllowRule;

    .end local v0    # "allowRule":Landroid/app/enterprise/FirewallAllowRule;
    invoke-direct {v0}, Landroid/app/enterprise/FirewallAllowRule;-><init>()V

    .line 2555
    .restart local v0    # "allowRule":Landroid/app/enterprise/FirewallAllowRule;
    invoke-virtual {p0}, Landroid/app/enterprise/FirewallPolicy;->getIptablesAllowRules()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/app/enterprise/FirewallAllowRule;->appendList(Ljava/util/List;)V

    .line 2556
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2559
    :cond_2
    and-int/lit8 v6, v5, 0x2

    if-eqz v6, :cond_3

    .line 2560
    new-instance v1, Landroid/app/enterprise/FirewallDenyRule;

    .end local v1    # "denyRule":Landroid/app/enterprise/FirewallDenyRule;
    invoke-direct {v1}, Landroid/app/enterprise/FirewallDenyRule;-><init>()V

    .line 2561
    .restart local v1    # "denyRule":Landroid/app/enterprise/FirewallDenyRule;
    invoke-virtual {p0}, Landroid/app/enterprise/FirewallPolicy;->getIptablesDenyRules()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/app/enterprise/FirewallDenyRule;->appendList(Ljava/util/List;)V

    .line 2562
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2565
    :cond_3
    and-int/lit8 v6, v5, 0x4

    if-eqz v6, :cond_4

    .line 2566
    new-instance v4, Landroid/app/enterprise/FirewallRerouteRule;

    .end local v4    # "rerouteRule":Landroid/app/enterprise/FirewallRerouteRule;
    invoke-direct {v4}, Landroid/app/enterprise/FirewallRerouteRule;-><init>()V

    .line 2567
    .restart local v4    # "rerouteRule":Landroid/app/enterprise/FirewallRerouteRule;
    invoke-virtual {p0}, Landroid/app/enterprise/FirewallPolicy;->getIptablesRerouteRules()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/app/enterprise/FirewallRerouteRule;->appendList(Ljava/util/List;)V

    .line 2568
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2571
    :cond_4
    and-int/lit8 v6, v5, 0x8

    if-eqz v6, :cond_0

    .line 2572
    new-instance v2, Landroid/app/enterprise/FirewallExceptionRule;

    .end local v2    # "exceptionRule":Landroid/app/enterprise/FirewallExceptionRule;
    invoke-direct {v2}, Landroid/app/enterprise/FirewallExceptionRule;-><init>()V

    .line 2573
    .restart local v2    # "exceptionRule":Landroid/app/enterprise/FirewallExceptionRule;
    invoke-virtual {p0}, Landroid/app/enterprise/FirewallPolicy;->getIptablesRedirectExceptionsRules()Ljava/util/List;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/app/enterprise/FirewallExceptionRule;->appendList(Ljava/util/List;)V

    .line 2574
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getURLFilterEnabled()Z
    .locals 5

    .prologue
    .line 1326
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getURLFilterEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1327
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1329
    :try_start_0
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "getURLFilterEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1330
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1335
    :goto_0
    return v1

    .line 1331
    :catch_0
    move-exception v0

    .line 1332
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1335
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getURLFilterList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1425
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getURLFilterList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1426
    const/4 v1, 0x0

    .line 1427
    .local v1, "urlList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1429
    :try_start_0
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getURLFilterList"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1430
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-interface {v2, v3, v4, v5}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterList(Landroid/app/enterprise/ContextInfo;ZZ)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1435
    :cond_0
    :goto_0
    return-object v1

    .line 1431
    :catch_0
    move-exception v0

    .line 1432
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getURLFilterReport()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1598
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.getURLFilterReport"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1599
    const/4 v1, 0x0

    .line 1601
    .local v1, "reportList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1603
    :try_start_0
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getURLFilterReport"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1604
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterReport(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1609
    :cond_0
    :goto_0
    return-object v1

    .line 1605
    :catch_0
    move-exception v0

    .line 1606
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getURLFilterReportEnabled()Z
    .locals 5

    .prologue
    .line 1547
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.getURLFilterReportEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1548
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1550
    :try_start_0
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "getURLFilterReportEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1551
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/app/enterprise/IFirewallPolicy;->getURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1556
    :goto_0
    return v1

    .line 1552
    :catch_0
    move-exception v0

    .line 1553
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1556
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGlobalProxyAllowed()Z
    .locals 3

    .prologue
    .line 2414
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.isGlobalProxyAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2415
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2417
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IFirewallPolicy;->isGlobalProxyAllowed()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2422
    :goto_0
    return v1

    .line 2418
    :catch_0
    move-exception v0

    .line 2419
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on isGlobalProxyAllowed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2422
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isUrlBlocked(Ljava/lang/String;)Z
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 1444
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1447
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->isUrlBlocked(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1452
    :goto_0
    return v1

    .line 1448
    :catch_0
    move-exception v0

    .line 1449
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public listIptablesRules()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1211
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.listIptablesRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1212
    const/4 v1, 0x0

    .line 1214
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1216
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IFirewallPolicy;->listIptablesRules(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1221
    :cond_0
    :goto_0
    return-object v1

    .line 1217
    :catch_0
    move-exception v0

    .line 1218
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeDNSForApp(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 2138
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.removeDNSForApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2139
    if-eqz p1, :cond_0

    .line 2140
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2142
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->removeDNSForApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2148
    :goto_0
    return v1

    .line 2143
    :catch_0
    move-exception v0

    .line 2144
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on removeDNSForApp"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2148
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeIptablesAllowRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 560
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.removeIptablesAllowRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 561
    const-string v2, "Application"

    const-string v3, "removeIptablesAllowRules..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 574
    :cond_0
    :goto_0
    return v1

    .line 565
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 568
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x1

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 570
    :catch_0
    move-exception v0

    .line 571
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeIptablesDenyRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 604
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.removeIptablesDenyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 605
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 616
    :cond_0
    :goto_0
    return v1

    .line 607
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 610
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x2

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 612
    :catch_0
    move-exception v0

    .line 613
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1953
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.removeIptablesRedirectExceptionsRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1954
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "removeIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1957
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x7

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1962
    :goto_0
    return v1

    .line 1958
    :catch_0
    move-exception v0

    .line 1959
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on removeIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1962
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeIptablesRerouteRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 647
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.removeIptablesRerouteRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 648
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 660
    :cond_0
    :goto_0
    return v1

    .line 651
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 654
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 656
    :catch_0
    move-exception v0

    .line 657
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public removeIptablesRules()Z
    .locals 3

    .prologue
    .line 1115
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.removeIptablesRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1116
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1118
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IFirewallPolicy;->cleanAllRules(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1123
    :goto_0
    return v1

    .line 1119
    :catch_0
    move-exception v0

    .line 1120
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1123
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeRules(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "objRule"    # Ljava/lang/Object;

    .prologue
    .line 2606
    const/4 v0, 0x0

    .line 2608
    .local v0, "ret":Z
    if-nez p1, :cond_0

    .line 2609
    const/4 v1, 0x0

    .line 2623
    .end local p1    # "objRule":Ljava/lang/Object;
    :goto_0
    return v1

    .line 2612
    .restart local p1    # "objRule":Ljava/lang/Object;
    :cond_0
    instance-of v1, p1, Landroid/app/enterprise/FirewallAllowRule;

    if-eqz v1, :cond_2

    .line 2613
    check-cast p1, Landroid/app/enterprise/FirewallAllowRule;

    .end local p1    # "objRule":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/app/enterprise/FirewallAllowRule;->getRules()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/enterprise/FirewallPolicy;->removeIptablesAllowRules(Ljava/util/List;)Z

    move-result v0

    :cond_1
    :goto_1
    move v1, v0

    .line 2623
    goto :goto_0

    .line 2614
    .restart local p1    # "objRule":Ljava/lang/Object;
    :cond_2
    instance-of v1, p1, Landroid/app/enterprise/FirewallDenyRule;

    if-eqz v1, :cond_3

    .line 2615
    check-cast p1, Landroid/app/enterprise/FirewallDenyRule;

    .end local p1    # "objRule":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/app/enterprise/FirewallDenyRule;->getRules()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/enterprise/FirewallPolicy;->removeIptablesDenyRules(Ljava/util/List;)Z

    move-result v0

    goto :goto_1

    .line 2616
    .restart local p1    # "objRule":Ljava/lang/Object;
    :cond_3
    instance-of v1, p1, Landroid/app/enterprise/FirewallRerouteRule;

    if-eqz v1, :cond_4

    .line 2617
    check-cast p1, Landroid/app/enterprise/FirewallRerouteRule;

    .end local p1    # "objRule":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/app/enterprise/FirewallRerouteRule;->getRules()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/enterprise/FirewallPolicy;->removeIptablesRerouteRules(Ljava/util/List;)Z

    move-result v0

    goto :goto_1

    .line 2618
    .restart local p1    # "objRule":Ljava/lang/Object;
    :cond_4
    instance-of v1, p1, Landroid/app/enterprise/FirewallExceptionRule;

    if-eqz v1, :cond_1

    .line 2619
    check-cast p1, Landroid/app/enterprise/FirewallExceptionRule;

    .end local p1    # "objRule":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/app/enterprise/FirewallExceptionRule;->getRules()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Landroid/app/enterprise/FirewallPolicy;->removeIptablesRedirectExceptionsRules(Ljava/util/List;)Z

    move-result v0

    goto :goto_1
.end method

.method public setDNSPerApp(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "dns1"    # Ljava/lang/String;
    .param p3, "dns2"    # Ljava/lang/String;

    .prologue
    .line 2094
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setDNSPerApp"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2095
    if-eqz p1, :cond_0

    .line 2096
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2098
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IFirewallPolicy;->setDNSPerApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2104
    :goto_0
    return v1

    .line 2099
    :catch_0
    move-exception v0

    .line 2100
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Exception on setDNSPerApp"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2104
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setGlobalProxy(Ljava/lang/String;ILjava/util/List;)Z
    .locals 4
    .param p1, "hostName"    # Ljava/lang/String;
    .param p2, "proxyPort"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p3, "exclusionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 2292
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.setGlobalProxy"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2294
    if-gez p2, :cond_1

    .line 2295
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "inValid proxyPort"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2306
    :cond_0
    :goto_0
    return v1

    .line 2299
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2301
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2, p3}, Landroid/app/enterprise/IFirewallPolicy;->setGlobalProxy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ILjava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 2302
    :catch_0
    move-exception v0

    .line 2303
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException on setGlobalProxy"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setIptablesAllowRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 887
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.setIptablesAllowRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 888
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 900
    :cond_0
    :goto_0
    return v1

    .line 891
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 894
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x1

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 896
    :catch_0
    move-exception v0

    .line 897
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setIptablesDenyRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 928
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.setIptablesDenyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 929
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 941
    :cond_0
    :goto_0
    return v1

    .line 932
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 935
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x2

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 937
    :catch_0
    move-exception v0

    .line 938
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setIptablesOption(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1168
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setIptablesOption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1169
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1171
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->enableRules(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1176
    :goto_0
    return v1

    .line 1172
    :catch_0
    move-exception v0

    .line 1173
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIptablesProxyOption(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1194
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setIptablesProxyOption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1195
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1197
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->enableProxy(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1202
    :goto_0
    return v1

    .line 1198
    :catch_0
    move-exception v0

    .line 1199
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1202
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIptablesProxyRules(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "ip"    # Ljava/lang/String;
    .param p2, "port"    # Ljava/lang/String;

    .prologue
    .line 1003
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.setIptablesProxyRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1004
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1006
    :try_start_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1007
    .local v1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1008
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IFirewallPolicy;->setProxyRules(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1013
    .end local v1    # "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return v2

    .line 1009
    :catch_0
    move-exception v0

    .line 1010
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1013
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setIptablesRedirectExceptionsRules(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1790
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setIptablesRedirectExceptionsRules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1791
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "setIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1794
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x7

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1799
    :goto_0
    return v1

    .line 1795
    :catch_0
    move-exception v0

    .line 1796
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException on setIptablesRedirectExceptions"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1799
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIptablesRerouteRules(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "rulesList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 970
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "FirewallPolicy.setIptablesRerouteRules"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 971
    invoke-direct {p0, p1}, Landroid/app/enterprise/FirewallPolicy;->checkEmptyList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 983
    :cond_0
    :goto_0
    return v1

    .line 974
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 977
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v3, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v4, 0x0

    invoke-interface {v2, v3, p1, v4}, Landroid/app/enterprise/IFirewallPolicy;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 979
    :catch_0
    move-exception v0

    .line 980
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v3, "RemoteException..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setNetworkForMarket(I)Z
    .locals 3
    .param p1, "networkType"    # I

    .prologue
    .line 1645
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setNetworkForMarket"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1646
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1648
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->setNetworkForMarket(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1653
    :goto_0
    return v1

    .line 1649
    :catch_0
    move-exception v0

    .line 1650
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setNetworkForMarket!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1653
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setURLFilterEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 1277
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setURLFilterEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1278
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1280
    :try_start_0
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setURLFilterEnabled state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1281
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1286
    :goto_0
    return v1

    .line 1282
    :catch_0
    move-exception v0

    .line 1283
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setURLFilterList(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1379
    .local p1, "urls":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setURLFilterList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1380
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 1382
    :try_start_0
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setURLFilterList urls = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1388
    :goto_0
    return v1

    .line 1384
    :catch_0
    move-exception v0

    .line 1385
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1388
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setURLFilterReportEnabled(Z)Z
    .locals 4
    .param p1, "enable"    # Z

    .prologue
    .line 1498
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "FirewallPolicy.setURLFilterReportEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1499
    invoke-direct {p0}, Landroid/app/enterprise/FirewallPolicy;->getService()Landroid/app/enterprise/IFirewallPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1501
    :try_start_0
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setURLFilterReportEnabled state = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    iget-object v1, p0, Landroid/app/enterprise/FirewallPolicy;->mService:Landroid/app/enterprise/IFirewallPolicy;

    iget-object v2, p0, Landroid/app/enterprise/FirewallPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IFirewallPolicy;->setURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1507
    :goto_0
    return v1

    .line 1503
    :catch_0
    move-exception v0

    .line 1504
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/FirewallPolicy;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException..."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1507
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
