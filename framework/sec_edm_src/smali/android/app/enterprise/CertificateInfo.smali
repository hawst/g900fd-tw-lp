.class public Landroid/app/enterprise/CertificateInfo;
.super Ljava/lang/Object;
.source "CertificateInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/CertificateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mAlias:Ljava/lang/String;

.field mCertificate:Ljava/security/cert/Certificate;

.field private mEnabled:Z

.field private mHasPrivateKey:Z

.field mKey:Ljava/security/Key;

.field private mKeystore:I

.field private mSystemPreloaded:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Landroid/app/enterprise/CertificateInfo$1;

    invoke-direct {v0}, Landroid/app/enterprise/CertificateInfo$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/CertificateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 60
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    .line 66
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    .line 68
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 60
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    .line 66
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    .line 68
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    .line 113
    invoke-direct {p0, p1}, Landroid/app/enterprise/CertificateInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 114
    return-void
.end method

.method public constructor <init>(Ljava/security/cert/Certificate;)V
    .locals 2
    .param p1, "cert"    # Ljava/security/cert/Certificate;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 60
    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    .line 64
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    .line 66
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    .line 68
    iput-boolean v1, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    .line 121
    iput-object p1, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 122
    return-void
.end method

.method private compareKeys(Ljava/security/Key;Ljava/security/Key;)Z
    .locals 2
    .param p1, "key"    # Ljava/security/Key;
    .param p2, "other"    # Ljava/security/Key;

    .prologue
    .line 332
    if-ne p1, p2, :cond_0

    .line 333
    const/4 v0, 0x1

    .line 338
    :goto_0
    return v0

    .line 335
    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 336
    invoke-interface {p1}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-interface {p2}, Ljava/security/Key;->getEncoded()[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0

    .line 338
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/security/cert/Certificate;

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/security/Key;

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    .line 171
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    .line 172
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    .line 173
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    .line 175
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    .line 314
    if-ne p0, p1, :cond_1

    .line 325
    :cond_0
    :goto_0
    return v1

    .line 317
    :cond_1
    if-eqz p1, :cond_2

    instance-of v2, p1, Landroid/app/enterprise/CertificateInfo;

    if-eqz v2, :cond_2

    move-object v0, p1

    .line 318
    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    .line 320
    .local v0, "c":Landroid/app/enterprise/CertificateInfo;
    iget-object v2, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    iget-object v3, v0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    invoke-virtual {v2, v3}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    iget-object v3, v0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    invoke-direct {p0, v2, v3}, Landroid/app/enterprise/CertificateInfo;->compareKeys(Ljava/security/Key;Ljava/security/Key;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 325
    .end local v0    # "c":Landroid/app/enterprise/CertificateInfo;
    :cond_2
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    return-object v0
.end method

.method public getCertificate()Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    return-object v0
.end method

.method public getEnabled()Z
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    return v0
.end method

.method public getHasPrivateKey()Z
    .locals 1

    .prologue
    .line 295
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    return v0
.end method

.method public getKeystore()I
    .locals 1

    .prologue
    .line 250
    iget v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    return v0
.end method

.method public getPrivateKey()Ljava/security/Key;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    return-object v0
.end method

.method public getSystemPreloaded()Z
    .locals 1

    .prologue
    .line 265
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    return v0
.end method

.method public setAlias(Ljava/lang/String;)V
    .locals 0
    .param p1, "alias"    # Ljava/lang/String;

    .prologue
    .line 227
    iput-object p1, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    .line 228
    return-void
.end method

.method public setCertificate(Ljava/security/cert/Certificate;)V
    .locals 0
    .param p1, "aCertificate"    # Ljava/security/cert/Certificate;

    .prologue
    .line 186
    iput-object p1, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    .line 187
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 272
    iput-boolean p1, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    .line 273
    return-void
.end method

.method public setHasPrivateKey(Z)V
    .locals 0
    .param p1, "hasPrivateKey"    # Z

    .prologue
    .line 287
    iput-boolean p1, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    .line 288
    return-void
.end method

.method public setKeystore(I)V
    .locals 0
    .param p1, "keystore"    # I

    .prologue
    .line 242
    iput p1, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    .line 243
    return-void
.end method

.method public setPrivateKey(Ljava/security/Key;)V
    .locals 0
    .param p1, "aKey"    # Ljava/security/Key;

    .prologue
    .line 197
    iput-object p1, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    .line 198
    return-void
.end method

.method public setSystemPreloaded(Z)V
    .locals 0
    .param p1, "preloaded"    # Z

    .prologue
    .line 257
    iput-boolean p1, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    .line 258
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 133
    if-eqz p1, :cond_0

    .line 137
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mCertificate:Ljava/security/cert/Certificate;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 139
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mKey:Ljava/security/Key;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 142
    iget-object v0, p0, Landroid/app/enterprise/CertificateInfo;->mAlias:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 143
    iget v0, p0, Landroid/app/enterprise/CertificateInfo;->mKeystore:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 144
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mSystemPreloaded:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 145
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mEnabled:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 146
    iget-boolean v0, p0, Landroid/app/enterprise/CertificateInfo;->mHasPrivateKey:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 149
    :cond_0
    return-void
.end method
