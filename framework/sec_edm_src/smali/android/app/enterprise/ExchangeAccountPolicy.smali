.class public Landroid/app/enterprise/ExchangeAccountPolicy;
.super Ljava/lang/Object;
.source "ExchangeAccountPolicy.java"


# static fields
.field public static final ACTION_CBA_INSTALL_STATUS:Ljava/lang/String; = "android.intent.action.sec.CBA_INSTALL_STATUS"

.field public static final ACTION_EXCHANGE_ACCOUNT_ADD_RESULT:Ljava/lang/String; = "edm.intent.action.EXCHANGE_ACCOUNT_ADD_RESULT"

.field public static final ACTION_EXCHANGE_ACCOUNT_DELETE_RESULT:Ljava/lang/String; = "edm.intent.action.EXCHANGE_ACCOUNT_DELETE_RESULT"

.field public static final ACTION_FORCE_SMIME_CERTIFICATE:Ljava/lang/String; = "com.android.server.enterprise.email.FORCE_SMIME_CERTIFICATE"

.field public static final ACTION_FORCE_SMIME_CERTIFICATE_FOR_ENCRYPTION:Ljava/lang/String; = "com.android.server.enterprise.email.FORCE_SMIME_CERTIFICATE_FOR_ENCRYPTION"

.field public static final ACTION_FORCE_SMIME_CERTIFICATE_FOR_SIGNING:Ljava/lang/String; = "com.android.server.enterprise.email.FORCE_SMIME_CERTIFICATE_FOR_SIGNING"

.field public static final ACTION_RELEASE_SMIME_CERTIFICATE:Ljava/lang/String; = "com.android.server.enterprise.email.RELEASE_SMIME_CERTIFICATE"

.field public static final ACTION_RELEASE_SMIME_CERTIFICATE_FOR_ENCRYPTION:Ljava/lang/String; = "com.android.server.enterprise.email.RELEASE_SMIME_CERTIFICATE_FOR_ENCRYPTION"

.field public static final ACTION_RELEASE_SMIME_CERTIFICATE_FOR_SIGNING:Ljava/lang/String; = "com.android.server.enterprise.email.RELEASE_SMIME_CERTIFICATE_FOR_SIGNING"

.field public static final EXTRA_ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field public static final EXTRA_CERT_PASSWORD:Ljava/lang/String; = "cert_password"

.field public static final EXTRA_CERT_PASSWORD_ID:Ljava/lang/String; = "cert_password_id"

.field public static final EXTRA_CERT_PATH:Ljava/lang/String; = "cert_path"

.field public static final EXTRA_EMAIL_ADDRESS:Ljava/lang/String; = "email_id"

.field public static final EXTRA_RESULT:Ljava/lang/String; = "result"

.field public static final EXTRA_SERVER_ADDRESS:Ljava/lang/String; = "server_host"

.field public static final EXTRA_STATUS:Ljava/lang/String; = "status"

.field public static final SET_SMIME_CERTIFICATE_FAIL_BIND_PASSWORD:I = 0x4

.field public static final SET_SMIME_CERTIFICATE_INVALID_ACCOUNT_ID:I = 0x3

.field public static final SET_SMIME_CERTIFICATE_INVALID_PASSWORD:I = 0x2

.field public static final SET_SMIME_CERTIFICATE_NOT_FOUND:I = 0x1

.field public static final SET_SMIME_CERTIFICATE_OK:I = -0x1

.field public static final SET_SMIME_CERTIFICATE_UNKNOWN_ERROR:I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mAccId:J

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mDomain:Ljava/lang/String;

.field private mEmailAddress:Ljava/lang/String;

.field private mHost:Ljava/lang/String;

.field private mService:Landroid/app/enterprise/IExchangeAccountPolicy;

.field private mUser:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "ExchangeAccountPolicy"

    sput-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 310
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 311
    return-void
.end method

.method private getService()Landroid/app/enterprise/IExchangeAccountPolicy;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    if-nez v0, :cond_0

    .line 315
    const-string v0, "eas_account_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    .line 318
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    return-object v0
.end method


# virtual methods
.method public addNewAccount(Landroid/app/enterprise/ExchangeAccount;)J
    .locals 4
    .param p1, "account"    # Landroid/app/enterprise/ExchangeAccount;

    .prologue
    .line 2622
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.addNewAccount(ExchangeAccount)"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2623
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2625
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IExchangeAccountPolicy;->addNewAccount_new(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/ExchangeAccount;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2630
    :goto_0
    return-wide v2

    .line 2626
    :catch_0
    move-exception v0

    .line 2627
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2630
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public addNewAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)J
    .locals 23
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "emailAddress"    # Ljava/lang/String;
    .param p3, "easUser"    # Ljava/lang/String;
    .param p4, "easDomain"    # Ljava/lang/String;
    .param p5, "syncLookback"    # I
    .param p6, "syncInterval"    # I
    .param p7, "isDefault"    # Z
    .param p8, "senderName"    # Ljava/lang/String;
    .param p9, "protocolVersion"    # Ljava/lang/String;
    .param p10, "signature"    # Ljava/lang/String;
    .param p11, "emailNotificationVibrateAlways"    # Z
    .param p12, "emailNotificationVibrateWhenSilent"    # Z
    .param p13, "serverAddress"    # Ljava/lang/String;
    .param p14, "useSSL"    # Z
    .param p15, "useTLS"    # Z
    .param p16, "acceptAllCertificates"    # Z
    .param p17, "serverPassword"    # Ljava/lang/String;
    .param p18, "serverPathPrefix"    # Ljava/lang/String;

    .prologue
    .line 528
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ExchangeAccountPolicy.addNewAccount"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 529
    invoke-direct/range {p0 .. p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 532
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move/from16 v14, p11

    move/from16 v15, p12

    move-object/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    invoke-interface/range {v2 .. v21}, Landroid/app/enterprise/IExchangeAccountPolicy;->addNewAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 542
    :goto_0
    return-wide v2

    .line 538
    :catch_0
    move-exception v22

    .line 539
    .local v22, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with exchange account policy"

    move-object/from16 v0, v22

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 542
    .end local v22    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public addNewAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;IIIIIIIIZII[BLjava/lang/String;)J
    .locals 35
    .param p1, "displayName"    # Ljava/lang/String;
    .param p2, "emailAddress"    # Ljava/lang/String;
    .param p3, "easUser"    # Ljava/lang/String;
    .param p4, "easDomain"    # Ljava/lang/String;
    .param p5, "syncLookback"    # I
    .param p6, "syncInterval"    # I
    .param p7, "isDefault"    # Z
    .param p8, "senderName"    # Ljava/lang/String;
    .param p9, "protocolVersion"    # Ljava/lang/String;
    .param p10, "signature"    # Ljava/lang/String;
    .param p11, "emailNotificationVibrateAlways"    # Z
    .param p12, "emailNotificationVibrateWhenSilent"    # Z
    .param p13, "serverAddress"    # Ljava/lang/String;
    .param p14, "useSSL"    # Z
    .param p15, "useTLS"    # Z
    .param p16, "acceptAllCertificates"    # Z
    .param p17, "serverPassword"    # Ljava/lang/String;
    .param p18, "serverPathPrefix"    # Ljava/lang/String;
    .param p19, "peakStartTime"    # I
    .param p20, "peakEndTime"    # I
    .param p21, "peakDays"    # I
    .param p22, "offPeakSyncSchedule"    # I
    .param p23, "roamingSyncSchedule"    # I
    .param p24, "reserved"    # I
    .param p25, "retrivalSize"    # I
    .param p26, "periodCalendar"    # I
    .param p27, "isNotify"    # Z
    .param p28, "syncContacts"    # I
    .param p29, "syncCalendar"    # I
    .param p30, "certificate_data"    # [B
    .param p31, "certificate_password"    # Ljava/lang/String;

    .prologue
    .line 702
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ExchangeAccountPolicy.addNewAccount"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 703
    invoke-direct/range {p0 .. p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 705
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move/from16 v10, p7

    move-object/from16 v11, p8

    move-object/from16 v12, p9

    move-object/from16 v13, p10

    move/from16 v14, p11

    move/from16 v15, p12

    move-object/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    move-object/from16 v20, p17

    move-object/from16 v21, p18

    move/from16 v22, p19

    move/from16 v23, p20

    move/from16 v24, p21

    move/from16 v25, p22

    move/from16 v26, p23

    move/from16 v27, p25

    move/from16 v28, p26

    move/from16 v29, p27

    move/from16 v30, p28

    move/from16 v31, p29

    move-object/from16 v32, p30

    move-object/from16 v33, p31

    invoke-interface/range {v2 .. v33}, Landroid/app/enterprise/IExchangeAccountPolicy;->addNewAccount_ex(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;IIIIIIIZII[BLjava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 719
    :goto_0
    return-wide v2

    .line 715
    :catch_0
    move-exception v34

    .line 716
    .local v34, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with exchange account policy"

    move-object/from16 v0, v34

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 719
    .end local v34    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public addNewAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "easUser"    # Ljava/lang/String;
    .param p3, "easDomain"    # Ljava/lang/String;
    .param p4, "serverAddress"    # Ljava/lang/String;
    .param p5, "serverPassword"    # Ljava/lang/String;

    .prologue
    .line 398
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 400
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.addNewAccount"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 401
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-interface/range {v0 .. v6}, Landroid/app/enterprise/IExchangeAccountPolicy;->createAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 408
    :goto_0
    return-wide v0

    .line 404
    :catch_0
    move-exception v7

    .line 405
    .local v7, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 408
    .end local v7    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public allowInComingAttachments(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 2345
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2347
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->allowInComingAttachments(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2352
    :goto_0
    return v1

    .line 2348
    :catch_0
    move-exception v0

    .line 2349
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2352
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteAccount(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1734
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.deleteAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1735
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1737
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->deleteAccount(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1742
    :goto_0
    return v1

    .line 1738
    :catch_0
    move-exception v0

    .line 1739
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1742
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountCertificatePassword(J)Ljava/lang/String;
    .locals 3
    .param p1, "callID"    # J

    .prologue
    .line 2654
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getAccountCertificatePassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2655
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2657
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getAccountCertificatePassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2662
    :goto_0
    return-object v1

    .line 2658
    :catch_0
    move-exception v0

    .line 2659
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getAccountCertificatePassword()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2662
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountDetails(J)Landroid/app/enterprise/Account;
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1676
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getAccountDetails"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1677
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1679
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getAccountDetails(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/Account;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1684
    :goto_0
    return-object v1

    .line 1680
    :catch_0
    move-exception v0

    .line 1681
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1684
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountEmailPassword(J)Ljava/lang/String;
    .locals 3
    .param p1, "callID"    # J

    .prologue
    .line 2638
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getAccountEmailPassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2639
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2641
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getAccountEmailPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2646
    :goto_0
    return-object v1

    .line 2642
    :catch_0
    move-exception v0

    .line 2643
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getAccountEmailPassword()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2646
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "easDomain"    # Ljava/lang/String;
    .param p2, "easUser"    # Ljava/lang/String;
    .param p3, "activeSyncHost"    # Ljava/lang/String;

    .prologue
    .line 1625
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getAccountId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1626
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1628
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->getAccountId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1633
    :goto_0
    return-wide v2

    .line 1629
    :catch_0
    move-exception v0

    .line 1630
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1633
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getAllEASAccounts()[Landroid/app/enterprise/Account;
    .locals 3

    .prologue
    .line 2131
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getAllEASAccounts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2132
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2134
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getAllEASAccounts(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/Account;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2139
    :goto_0
    return-object v1

    .line 2135
    :catch_0
    move-exception v0

    .line 2136
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2139
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2176
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.getDeviceId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2177
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2179
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getDeviceId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2184
    :goto_0
    return-object v1

    .line 2180
    :catch_0
    move-exception v0

    .line 2181
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2184
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForceSMIMECertificate(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2314
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2316
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getForceSMIMECertificate(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2321
    :goto_0
    return v1

    .line 2317
    :catch_0
    move-exception v0

    .line 2318
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2321
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForceSMIMECertificateForEncryption(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2593
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2595
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getForceSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2600
    :goto_0
    return v1

    .line 2596
    :catch_0
    move-exception v0

    .line 2597
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2600
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getForceSMIMECertificateForSigning(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2548
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2550
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getForceSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2555
    :goto_0
    return v1

    .line 2551
    :catch_0
    move-exception v0

    .line 2552
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2555
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIncomingAttachmentsSize(J)I
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2390
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2392
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getIncomingAttachmentsSize(Landroid/app/enterprise/ContextInfo;J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2397
    :goto_0
    return v1

    .line 2393
    :catch_0
    move-exception v0

    .line 2394
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2397
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxCalendarAgeFilter(J)I
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2422
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2424
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getMaxCalendarAgeFilter(Landroid/app/enterprise/ContextInfo;J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2429
    :goto_0
    return v1

    .line 2425
    :catch_0
    move-exception v0

    .line 2426
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2429
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxEmailAgeFilter(J)I
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2454
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2456
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getMaxEmailAgeFilter(Landroid/app/enterprise/ContextInfo;J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2461
    :goto_0
    return v1

    .line 2457
    :catch_0
    move-exception v0

    .line 2458
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2461
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxEmailBodyTruncationSize(J)I
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2486
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2488
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getMaxEmailBodyTruncationSize(Landroid/app/enterprise/ContextInfo;J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2493
    :goto_0
    return v1

    .line 2489
    :catch_0
    move-exception v0

    .line 2490
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2493
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMaxEmailHTMLBodyTruncationSize(J)I
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2518
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2520
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getMaxEmailHTMLBodyTruncationSize(Landroid/app/enterprise/ContextInfo;J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2525
    :goto_0
    return v1

    .line 2521
    :catch_0
    move-exception v0

    .line 2522
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2525
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRequireEncryptedSMIMEMessages(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2281
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2283
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getRequireEncryptedSMIMEMessages(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2288
    :goto_0
    return v1

    .line 2284
    :catch_0
    move-exception v0

    .line 2285
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2288
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRequireSignedSMIMEMessages(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2249
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2251
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->getRequireSignedSMIMEMessages(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2256
    :goto_0
    return v1

    .line 2252
    :catch_0
    move-exception v0

    .line 2253
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2256
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isIncomingAttachmentsAllowed(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2360
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2362
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->isIncomingAttachmentsAllowed(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2367
    :goto_0
    return v1

    .line 2363
    :catch_0
    move-exception v0

    .line 2364
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2367
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removePendingAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "easUser"    # Ljava/lang/String;
    .param p3, "easDomain"    # Ljava/lang/String;
    .param p4, "serverAddress"    # Ljava/lang/String;

    .prologue
    .line 2216
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.removePendingAccount"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2217
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2219
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IExchangeAccountPolicy;->removePendingAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2224
    :cond_0
    :goto_0
    return-void

    .line 2220
    :catch_0
    move-exception v6

    .line 2221
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public sendAccountsChangedBroadcast()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1784
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.sendAccountsChangedBroadcast"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1785
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1787
    :try_start_0
    iget-wide v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1788
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAccountBaseParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mUser:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1789
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAccountBaseParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mDomain:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1790
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAccountBaseParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mEmailAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1791
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAccountBaseParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mHost:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1792
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAccountBaseParameters : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1793
    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mUser:Ljava/lang/String;

    iget-object v3, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mDomain:Ljava/lang/String;

    iget-object v4, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mEmailAddress:Ljava/lang/String;

    iget-object v5, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mHost:Ljava/lang/String;

    iget-wide v6, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    move-object v1, p0

    invoke-virtual/range {v1 .. v7}, Landroid/app/enterprise/ExchangeAccountPolicy;->setAccountBaseParameters(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    .line 1794
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mHost:Ljava/lang/String;

    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mEmailAddress:Ljava/lang/String;

    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mDomain:Ljava/lang/String;

    iput-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mUser:Ljava/lang/String;

    .line 1795
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    .line 1797
    :cond_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy;->sendAccountsChangedBroadcast(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1802
    :cond_1
    :goto_0
    return-void

    .line 1798
    :catch_0
    move-exception v0

    .line 1799
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setAcceptAllCertificates(ZJ)Z
    .locals 4
    .param p1, "acceptAllCertificates"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 978
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAcceptAllCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 979
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 981
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 987
    :goto_0
    return v1

    .line 983
    :catch_0
    move-exception v0

    .line 984
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 987
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAccountBaseParameters(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J
    .locals 9
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "domain"    # Ljava/lang/String;
    .param p3, "emailAddress"    # Ljava/lang/String;
    .param p4, "host"    # Ljava/lang/String;
    .param p5, "accId"    # J

    .prologue
    .line 787
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setAccountBaseParameters"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 788
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 790
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide v6, p5

    invoke-interface/range {v0 .. v7}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAccountBaseParameters(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 796
    :goto_0
    return-wide v0

    .line 792
    :catch_0
    move-exception v8

    .line 793
    .local v8, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 796
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public setAccountCertificatePassword(Ljava/lang/String;)J
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 2685
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAccountCertificatePassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2686
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2688
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAccountCertificatePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2693
    :goto_0
    return-wide v2

    .line 2689
    :catch_0
    move-exception v0

    .line 2690
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setAccountCertificatePassword()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2693
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setAccountEmailPassword(Ljava/lang/String;)J
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 2670
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAccountEmailPassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2671
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2673
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAccountEmailPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2678
    :goto_0
    return-wide v2

    .line 2674
    :catch_0
    move-exception v0

    .line 2675
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setAccountEmailPassword()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2678
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setAccountName(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1574
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAccountName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1575
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1577
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAccountName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1582
    :goto_0
    return v1

    .line 1578
    :catch_0
    move-exception v0

    .line 1579
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1582
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAlwaysVibrateOnEmailNotification(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1036
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAlwaysVibrateOnEmailNotification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1037
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1039
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAlwaysVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1044
    :goto_0
    return v1

    .line 1040
    :catch_0
    move-exception v0

    .line 1041
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1044
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAsDefaultAccount(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1519
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setAsDefaultAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1520
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1522
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->setAsDefaultAccount(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1527
    :goto_0
    return v1

    .line 1523
    :catch_0
    move-exception v0

    .line 1524
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1527
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setClientAuthCert([BLjava/lang/String;J)V
    .locals 7
    .param p1, "certData"    # [B
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "accId"    # J

    .prologue
    .line 1886
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setClientAuthCert"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1887
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1889
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IExchangeAccountPolicy;->setClientAuthCert(Landroid/app/enterprise/ContextInfo;[BLjava/lang/String;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1894
    :cond_0
    :goto_0
    return-void

    .line 1890
    :catch_0
    move-exception v6

    .line 1891
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setDataSyncs(ZZZZJ)Z
    .locals 9
    .param p1, "syncCalendar"    # Z
    .param p2, "syncContacts"    # Z
    .param p3, "syncTasks"    # Z
    .param p4, "syncNotes"    # Z
    .param p5, "accId"    # J

    .prologue
    .line 2084
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setDataSyncs"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2085
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2087
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-wide v6, p5

    invoke-interface/range {v0 .. v7}, Landroid/app/enterprise/IExchangeAccountPolicy;->setDataSyncs(Landroid/app/enterprise/ContextInfo;ZZZZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2093
    :goto_0
    return v0

    .line 2089
    :catch_0
    move-exception v8

    .line 2090
    .local v8, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2093
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDomain(Ljava/lang/String;J)V
    .locals 2
    .param p1, "domain"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1119
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1121
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mDomain:Ljava/lang/String;

    .line 1122
    iput-wide p2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    .line 1127
    :cond_0
    return-void
.end method

.method public setEmailAddress(Ljava/lang/String;J)Z
    .locals 2
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 868
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 870
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mEmailAddress:Ljava/lang/String;

    .line 871
    iput-wide p2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    .line 876
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setForceSMIMECertificate(JLjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "accId"    # J
    .param p3, "certPath"    # Ljava/lang/String;
    .param p4, "certPassword"    # Ljava/lang/String;

    .prologue
    .line 2297
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setForceSMIMECertificate"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2298
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2300
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IExchangeAccountPolicy;->setForceSMIMECertificate(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2305
    :goto_0
    return v0

    .line 2301
    :catch_0
    move-exception v6

    .line 2302
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2305
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setForceSMIMECertificateForEncryption(JLjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "accId"    # J
    .param p3, "certPath"    # Ljava/lang/String;
    .param p4, "certPassword"    # Ljava/lang/String;

    .prologue
    .line 2577
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setForceSMIMECertificateForEncryption"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2578
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2580
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IExchangeAccountPolicy;->setForceSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2585
    :goto_0
    return v0

    .line 2581
    :catch_0
    move-exception v6

    .line 2582
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2585
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setForceSMIMECertificateForSigning(JLjava/lang/String;Ljava/lang/String;)I
    .locals 7
    .param p1, "accId"    # J
    .param p3, "certPath"    # Ljava/lang/String;
    .param p4, "certPassword"    # Ljava/lang/String;

    .prologue
    .line 2532
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "ExchangeAccountPolicy.setForceSMIMECertificateForSigning"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2533
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2535
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IExchangeAccountPolicy;->setForceSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2540
    :goto_0
    return v0

    .line 2536
    :catch_0
    move-exception v6

    .line 2537
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with exchange account policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2540
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setHost(Ljava/lang/String;J)V
    .locals 2
    .param p1, "host"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 815
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 817
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mHost:Ljava/lang/String;

    .line 818
    iput-wide p2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    .line 823
    :cond_0
    return-void
.end method

.method public setIncomingAttachmentsSize(IJ)Z
    .locals 4
    .param p1, "size"    # I
    .param p2, "accId"    # J

    .prologue
    .line 2374
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setIncomingAttachmentsSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2375
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2377
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setIncomingAttachmentsSize(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2382
    :goto_0
    return v1

    .line 2378
    :catch_0
    move-exception v0

    .line 2379
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2382
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaxCalendarAgeFilter(IJ)Z
    .locals 4
    .param p1, "maxage"    # I
    .param p2, "accId"    # J

    .prologue
    .line 2405
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setMaxCalendarAgeFilter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2406
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2408
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setMaxCalendarAgeFilter(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2413
    :goto_0
    return v1

    .line 2409
    :catch_0
    move-exception v0

    .line 2410
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2413
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaxEmailAgeFilter(IJ)Z
    .locals 4
    .param p1, "maxage"    # I
    .param p2, "accId"    # J

    .prologue
    .line 2437
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setMaxEmailAgeFilter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2438
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2440
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setMaxEmailAgeFilter(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2445
    :goto_0
    return v1

    .line 2441
    :catch_0
    move-exception v0

    .line 2442
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2445
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaxEmailBodyTruncationSize(IJ)Z
    .locals 4
    .param p1, "size"    # I
    .param p2, "accId"    # J

    .prologue
    .line 2469
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setMaxEmailBodyTruncationSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2470
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2472
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setMaxEmailBodyTruncationSize(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2477
    :goto_0
    return v1

    .line 2473
    :catch_0
    move-exception v0

    .line 2474
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2477
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMaxEmailHTMLBodyTruncationSize(IJ)Z
    .locals 4
    .param p1, "size"    # I
    .param p2, "accId"    # J

    .prologue
    .line 2501
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setMaxEmailHTMLBodyTruncationSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2502
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2504
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setMaxEmailHTMLBodyTruncationSize(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2509
    :goto_0
    return v1

    .line 2505
    :catch_0
    move-exception v0

    .line 2506
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2509
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPassword(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1171
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1172
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1174
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1179
    :goto_0
    return v1

    .line 1175
    :catch_0
    move-exception v0

    .line 1176
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1179
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPastDaysToSync(IJ)Z
    .locals 4
    .param p1, "pastDaysToSync"    # I
    .param p2, "accId"    # J

    .prologue
    .line 1338
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setPastDaysToSync"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1339
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1341
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setPastDaysToSync(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1346
    :goto_0
    return v1

    .line 1342
    :catch_0
    move-exception v0

    .line 1343
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1346
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setProtocolVersion(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "protocolVersion"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1228
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setProtocolVersion"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1229
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1231
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setProtocolVersion(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1236
    :goto_0
    return v1

    .line 1232
    :catch_0
    move-exception v0

    .line 1233
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1236
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setReleaseSMIMECertificate(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2329
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setReleaseSMIMECertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2330
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2332
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->setReleaseSMIMECertificate(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2337
    :goto_0
    return v1

    .line 2333
    :catch_0
    move-exception v0

    .line 2334
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setReleaseSMIMECertificateForEncryption(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2607
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setReleaseSMIMECertificateForEncryption"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2608
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2610
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->setReleaseSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2615
    :goto_0
    return v1

    .line 2611
    :catch_0
    move-exception v0

    .line 2612
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2615
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setReleaseSMIMECertificateForSigning(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 2562
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setReleaseSMIMECertificateForSigning"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2563
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2565
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IExchangeAccountPolicy;->setReleaseSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2570
    :goto_0
    return v1

    .line 2566
    :catch_0
    move-exception v0

    .line 2567
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2570
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setRequireEncryptedSMIMEMessages(JZ)Z
    .locals 3
    .param p1, "accId"    # J
    .param p3, "enable"    # Z

    .prologue
    .line 2264
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setRequireEncryptedSMIMEMessages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2265
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2267
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setRequireEncryptedSMIMEMessages(Landroid/app/enterprise/ContextInfo;JZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2272
    :goto_0
    return v1

    .line 2268
    :catch_0
    move-exception v0

    .line 2269
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2272
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setRequireSignedSMIMEMessages(JZ)Z
    .locals 3
    .param p1, "accId"    # J
    .param p3, "enable"    # Z

    .prologue
    .line 2232
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setRequireSignedSMIMEMessages"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2233
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2235
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setRequireSignedSMIMEMessages(Landroid/app/enterprise/ContextInfo;JZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2240
    :goto_0
    return v1

    .line 2236
    :catch_0
    move-exception v0

    .line 2237
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2240
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSSL(ZJ)Z
    .locals 4
    .param p1, "enableSSL"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 922
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSSL"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 923
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 925
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 930
    :goto_0
    return v1

    .line 926
    :catch_0
    move-exception v0

    .line 927
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 930
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSenderName(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "senderName"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1457
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSenderName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1458
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1460
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSenderName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1465
    :goto_0
    return v1

    .line 1461
    :catch_0
    move-exception v0

    .line 1462
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1465
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setServerPathPrefix(Ljava/lang/String;J)Z
    .locals 1
    .param p1, "pathPrefix"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1477
    const/4 v0, 0x0

    return v0
.end method

.method public setSignature(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "signature"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1282
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSignature"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1283
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1285
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSignature(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1290
    :goto_0
    return v1

    .line 1286
    :catch_0
    move-exception v0

    .line 1287
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1290
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSilentVibrateOnEmailNotification(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1093
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSilentVibrateOnEmailNotification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1094
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1096
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSilentVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1101
    :goto_0
    return v1

    .line 1097
    :catch_0
    move-exception v0

    .line 1098
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1101
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSyncInterval(IJ)Z
    .locals 4
    .param p1, "syncInterval"    # I
    .param p2, "accId"    # J

    .prologue
    .line 1398
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSyncInterval"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1399
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1401
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSyncInterval(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1406
    :goto_0
    return v1

    .line 1402
    :catch_0
    move-exception v0

    .line 1403
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1406
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSyncPeakTimings(IIIJ)Z
    .locals 8
    .param p1, "peakDays"    # I
    .param p2, "peakStartMinute"    # I
    .param p3, "peakEndMinute"    # I
    .param p4, "accId"    # J

    .prologue
    .line 1946
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSyncPeakTimings"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1947
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1949
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSyncPeakTimings(Landroid/app/enterprise/ContextInfo;IIIJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1955
    :goto_0
    return v1

    .line 1951
    :catch_0
    move-exception v0

    .line 1952
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1955
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSyncSchedules(IIIJ)Z
    .locals 8
    .param p1, "peakSyncSchedule"    # I
    .param p2, "offPeakSyncSchedule"    # I
    .param p3, "roamingSyncSchedule"    # I
    .param p4, "accId"    # J

    .prologue
    .line 2019
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "ExchangeAccountPolicy.setSyncSchedules"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2020
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2022
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mService:Landroid/app/enterprise/IExchangeAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move v3, p1

    move v4, p2

    move v5, p3

    move-wide v6, p4

    invoke-interface/range {v1 .. v7}, Landroid/app/enterprise/IExchangeAccountPolicy;->setSyncSchedules(Landroid/app/enterprise/ContextInfo;IIIJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2028
    :goto_0
    return v1

    .line 2024
    :catch_0
    move-exception v0

    .line 2025
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/ExchangeAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with exchange account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2028
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUser(Ljava/lang/String;J)V
    .locals 2
    .param p1, "user"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 841
    invoke-direct {p0}, Landroid/app/enterprise/ExchangeAccountPolicy;->getService()Landroid/app/enterprise/IExchangeAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 843
    iput-object p1, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mUser:Ljava/lang/String;

    .line 844
    iput-wide p2, p0, Landroid/app/enterprise/ExchangeAccountPolicy;->mAccId:J

    .line 849
    :cond_0
    return-void
.end method
