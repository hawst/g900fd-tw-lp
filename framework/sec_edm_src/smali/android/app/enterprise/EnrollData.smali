.class public Landroid/app/enterprise/EnrollData;
.super Ljava/lang/Object;
.source "EnrollData.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/EnrollData;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "EnrollData"


# instance fields
.field private comment:Ljava/lang/String;

.field private constrainedState:I

.field private downloadUrl:Ljava/lang/String;

.field private pkgName:Ljava/lang/String;

.field private policyBitMask:I

.field private signature:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Landroid/app/enterprise/EnrollData$1;

    invoke-direct {v0}, Landroid/app/enterprise/EnrollData$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/EnrollData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v1, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    .line 10
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    .line 12
    iput v1, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    .line 13
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput v1, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    .line 10
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    .line 11
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    .line 12
    iput v1, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    .line 13
    iput-object v0, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    .line 51
    invoke-virtual {p0, p1}, Landroid/app/enterprise/EnrollData;->readFromParcel(Landroid/os/Parcel;)V

    .line 52
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public getComment()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    return-object v0
.end method

.method public getConstrainedState()I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    return v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    return-object v0
.end method

.method public getPolicyBitMask()I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    return v0
.end method

.method public getSignature()[B
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    .line 79
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    .line 80
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    .line 81
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    .line 82
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 83
    .local v0, "size":I
    if-eqz v0, :cond_0

    .line 84
    new-array v1, v0, [B

    iput-object v1, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    .line 85
    iget-object v1, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 87
    :cond_0
    return-void
.end method

.method public setData(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I[B)V
    .locals 0
    .param p1, "pkg"    # Ljava/lang/String;
    .param p2, "comment"    # Ljava/lang/String;
    .param p3, "policyBitMask"    # I
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "constrainedState"    # I
    .param p6, "signature"    # [B

    .prologue
    .line 21
    iput-object p1, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    .line 22
    iput-object p2, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    .line 23
    iput p3, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    .line 24
    iput-object p4, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    .line 25
    iput p5, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    .line 26
    iput-object p6, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    .line 27
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 62
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->pkgName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->comment:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    iget v0, p0, Landroid/app/enterprise/EnrollData;->policyBitMask:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 65
    iget v0, p0, Landroid/app/enterprise/EnrollData;->constrainedState:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 73
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 71
    iget-object v0, p0, Landroid/app/enterprise/EnrollData;->signature:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_0
.end method
