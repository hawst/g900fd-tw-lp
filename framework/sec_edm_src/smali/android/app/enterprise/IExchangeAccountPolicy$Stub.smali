.class public abstract Landroid/app/enterprise/IExchangeAccountPolicy$Stub;
.super Landroid/os/Binder;
.source "IExchangeAccountPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IExchangeAccountPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IExchangeAccountPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IExchangeAccountPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IExchangeAccountPolicy"

.field static final TRANSACTION_addNewAccount:I = 0x2

.field static final TRANSACTION_addNewAccount_ex:I = 0x3

.field static final TRANSACTION_addNewAccount_new:I = 0x39

.field static final TRANSACTION_allowEmailSettingsChange:I = 0x29

.field static final TRANSACTION_allowInComingAttachments:I = 0x23

.field static final TRANSACTION_createAccount:I = 0x1

.field static final TRANSACTION_deleteAccount:I = 0x14

.field static final TRANSACTION_getAccountCertificatePassword:I = 0x3b

.field static final TRANSACTION_getAccountDetails:I = 0x13

.field static final TRANSACTION_getAccountEmailPassword:I = 0x3a

.field static final TRANSACTION_getAccountId:I = 0x12

.field static final TRANSACTION_getAllEASAccounts:I = 0x19

.field static final TRANSACTION_getDeviceId:I = 0x1a

.field static final TRANSACTION_getForceSMIMECertificate:I = 0x21

.field static final TRANSACTION_getForceSMIMECertificateForEncryption:I = 0x37

.field static final TRANSACTION_getForceSMIMECertificateForSigning:I = 0x34

.field static final TRANSACTION_getIncomingAttachmentsSize:I = 0x26

.field static final TRANSACTION_getMaxCalendarAgeFilter:I = 0x2c

.field static final TRANSACTION_getMaxEmailAgeFilter:I = 0x2e

.field static final TRANSACTION_getMaxEmailBodyTruncationSize:I = 0x30

.field static final TRANSACTION_getMaxEmailHTMLBodyTruncationSize:I = 0x32

.field static final TRANSACTION_getRequireEncryptedSMIMEMessages:I = 0x1f

.field static final TRANSACTION_getRequireSignedSMIMEMessages:I = 0x1d

.field static final TRANSACTION_isEmailNotificationsEnabled:I = 0x28

.field static final TRANSACTION_isEmailSettingsChangeAllowed:I = 0x2a

.field static final TRANSACTION_isIncomingAttachmentsAllowed:I = 0x24

.field static final TRANSACTION_removePendingAccount:I = 0x1b

.field static final TRANSACTION_sendAccountsChangedBroadcast:I = 0x15

.field static final TRANSACTION_setAcceptAllCertificates:I = 0x6

.field static final TRANSACTION_setAccountBaseParameters:I = 0x4

.field static final TRANSACTION_setAccountCertificatePassword:I = 0x3d

.field static final TRANSACTION_setAccountEmailPassword:I = 0x3c

.field static final TRANSACTION_setAccountName:I = 0x11

.field static final TRANSACTION_setAlwaysVibrateOnEmailNotification:I = 0x7

.field static final TRANSACTION_setAsDefaultAccount:I = 0x10

.field static final TRANSACTION_setClientAuthCert:I = 0xc

.field static final TRANSACTION_setDataSyncs:I = 0x18

.field static final TRANSACTION_setEmailNotificationsState:I = 0x27

.field static final TRANSACTION_setForceSMIMECertificate:I = 0x20

.field static final TRANSACTION_setForceSMIMECertificateForEncryption:I = 0x36

.field static final TRANSACTION_setForceSMIMECertificateForSigning:I = 0x33

.field static final TRANSACTION_setIncomingAttachmentsSize:I = 0x25

.field static final TRANSACTION_setMaxCalendarAgeFilter:I = 0x2b

.field static final TRANSACTION_setMaxEmailAgeFilter:I = 0x2d

.field static final TRANSACTION_setMaxEmailBodyTruncationSize:I = 0x2f

.field static final TRANSACTION_setMaxEmailHTMLBodyTruncationSize:I = 0x31

.field static final TRANSACTION_setPassword:I = 0x9

.field static final TRANSACTION_setPastDaysToSync:I = 0xd

.field static final TRANSACTION_setProtocolVersion:I = 0xa

.field static final TRANSACTION_setReleaseSMIMECertificate:I = 0x22

.field static final TRANSACTION_setReleaseSMIMECertificateForEncryption:I = 0x38

.field static final TRANSACTION_setReleaseSMIMECertificateForSigning:I = 0x35

.field static final TRANSACTION_setRequireEncryptedSMIMEMessages:I = 0x1e

.field static final TRANSACTION_setRequireSignedSMIMEMessages:I = 0x1c

.field static final TRANSACTION_setSSL:I = 0x5

.field static final TRANSACTION_setSenderName:I = 0xf

.field static final TRANSACTION_setSignature:I = 0xb

.field static final TRANSACTION_setSilentVibrateOnEmailNotification:I = 0x8

.field static final TRANSACTION_setSyncInterval:I = 0xe

.field static final TRANSACTION_setSyncPeakTimings:I = 0x16

.field static final TRANSACTION_setSyncSchedules:I = 0x17


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.IExchangeAccountPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IExchangeAccountPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.IExchangeAccountPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IExchangeAccountPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/IExchangeAccountPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/IExchangeAccountPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 64
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 1285
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 46
    :sswitch_0
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v4, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 54
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 62
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 64
    .local v7, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 66
    .local v8, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 68
    .local v9, "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .local v10, "_arg5":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 69
    invoke-virtual/range {v4 .. v10}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->createAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v62

    .line 70
    .local v62, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 71
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 72
    const/4 v4, 0x1

    goto :goto_0

    .line 57
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v10    # "_arg5":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 76
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 78
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    .line 79
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 85
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 87
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 89
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 91
    .restart local v8    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 93
    .restart local v9    # "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 95
    .local v10, "_arg5":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 97
    .local v11, "_arg6":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    const/4 v12, 0x1

    .line 99
    .local v12, "_arg7":Z
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    .line 101
    .local v13, "_arg8":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    .line 103
    .local v14, "_arg9":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    .line 105
    .local v15, "_arg10":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3

    const/16 v16, 0x1

    .line 107
    .local v16, "_arg11":Z
    :goto_4
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    const/16 v17, 0x1

    .line 109
    .local v17, "_arg12":Z
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v18

    .line 111
    .local v18, "_arg13":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5

    const/16 v19, 0x1

    .line 113
    .local v19, "_arg14":Z
    :goto_6
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6

    const/16 v20, 0x1

    .line 115
    .local v20, "_arg15":Z
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7

    const/16 v21, 0x1

    .line 117
    .local v21, "_arg16":Z
    :goto_8
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v22

    .line 119
    .local v22, "_arg17":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v23

    .local v23, "_arg18":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 120
    invoke-virtual/range {v4 .. v23}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->addNewAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;)J

    move-result-wide v62

    .line 121
    .restart local v62    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 123
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 82
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v10    # "_arg5":I
    .end local v11    # "_arg6":I
    .end local v12    # "_arg7":Z
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":Ljava/lang/String;
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Z
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Ljava/lang/String;
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    .end local v21    # "_arg16":Z
    .end local v22    # "_arg17":Ljava/lang/String;
    .end local v23    # "_arg18":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_1
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 97
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v7    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_arg3":Ljava/lang/String;
    .restart local v9    # "_arg4":Ljava/lang/String;
    .restart local v10    # "_arg5":I
    .restart local v11    # "_arg6":I
    :cond_2
    const/4 v12, 0x0

    goto :goto_3

    .line 105
    .restart local v12    # "_arg7":Z
    .restart local v13    # "_arg8":Ljava/lang/String;
    .restart local v14    # "_arg9":Ljava/lang/String;
    .restart local v15    # "_arg10":Ljava/lang/String;
    :cond_3
    const/16 v16, 0x0

    goto :goto_4

    .line 107
    .restart local v16    # "_arg11":Z
    :cond_4
    const/16 v17, 0x0

    goto :goto_5

    .line 111
    .restart local v17    # "_arg12":Z
    .restart local v18    # "_arg13":Ljava/lang/String;
    :cond_5
    const/16 v19, 0x0

    goto :goto_6

    .line 113
    .restart local v19    # "_arg14":Z
    :cond_6
    const/16 v20, 0x0

    goto :goto_7

    .line 115
    .restart local v20    # "_arg15":Z
    :cond_7
    const/16 v21, 0x0

    goto :goto_8

    .line 127
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v10    # "_arg5":I
    .end local v11    # "_arg6":I
    .end local v12    # "_arg7":Z
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":Ljava/lang/String;
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Z
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Ljava/lang/String;
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    :sswitch_3
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8

    .line 130
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 136
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 138
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 140
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 142
    .restart local v8    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 144
    .restart local v9    # "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 146
    .restart local v10    # "_arg5":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v11

    .line 148
    .restart local v11    # "_arg6":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9

    const/4 v12, 0x1

    .line 150
    .restart local v12    # "_arg7":Z
    :goto_a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v13

    .line 152
    .restart local v13    # "_arg8":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v14

    .line 154
    .restart local v14    # "_arg9":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v15

    .line 156
    .restart local v15    # "_arg10":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a

    const/16 v16, 0x1

    .line 158
    .restart local v16    # "_arg11":Z
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_b

    const/16 v17, 0x1

    .line 160
    .restart local v17    # "_arg12":Z
    :goto_c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v18

    .line 162
    .restart local v18    # "_arg13":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_c

    const/16 v19, 0x1

    .line 164
    .restart local v19    # "_arg14":Z
    :goto_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_d

    const/16 v20, 0x1

    .line 166
    .restart local v20    # "_arg15":Z
    :goto_e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_e

    const/16 v21, 0x1

    .line 168
    .restart local v21    # "_arg16":Z
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v22

    .line 170
    .restart local v22    # "_arg17":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v23

    .line 172
    .restart local v23    # "_arg18":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v24

    .line 174
    .local v24, "_arg19":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v25

    .line 176
    .local v25, "_arg20":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v26

    .line 178
    .local v26, "_arg21":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v27

    .line 180
    .local v27, "_arg22":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v28

    .line 182
    .local v28, "_arg23":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v29

    .line 184
    .local v29, "_arg24":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v30

    .line 186
    .local v30, "_arg25":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_f

    const/16 v31, 0x1

    .line 188
    .local v31, "_arg26":Z
    :goto_10
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v32

    .line 190
    .local v32, "_arg27":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v33

    .line 192
    .local v33, "_arg28":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v34

    .line 194
    .local v34, "_arg29":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v35

    .local v35, "_arg30":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 195
    invoke-virtual/range {v4 .. v35}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->addNewAccount_ex(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;ZZZLjava/lang/String;Ljava/lang/String;IIIIIIIZII[BLjava/lang/String;)J

    move-result-wide v62

    .line 196
    .restart local v62    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 197
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 198
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 133
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v10    # "_arg5":I
    .end local v11    # "_arg6":I
    .end local v12    # "_arg7":Z
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":Ljava/lang/String;
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Z
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Ljava/lang/String;
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    .end local v21    # "_arg16":Z
    .end local v22    # "_arg17":Ljava/lang/String;
    .end local v23    # "_arg18":Ljava/lang/String;
    .end local v24    # "_arg19":I
    .end local v25    # "_arg20":I
    .end local v26    # "_arg21":I
    .end local v27    # "_arg22":I
    .end local v28    # "_arg23":I
    .end local v29    # "_arg24":I
    .end local v30    # "_arg25":I
    .end local v31    # "_arg26":Z
    .end local v32    # "_arg27":I
    .end local v33    # "_arg28":I
    .end local v34    # "_arg29":[B
    .end local v35    # "_arg30":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_8
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto/16 :goto_9

    .line 148
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v7    # "_arg2":Ljava/lang/String;
    .restart local v8    # "_arg3":Ljava/lang/String;
    .restart local v9    # "_arg4":Ljava/lang/String;
    .restart local v10    # "_arg5":I
    .restart local v11    # "_arg6":I
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_a

    .line 156
    .restart local v12    # "_arg7":Z
    .restart local v13    # "_arg8":Ljava/lang/String;
    .restart local v14    # "_arg9":Ljava/lang/String;
    .restart local v15    # "_arg10":Ljava/lang/String;
    :cond_a
    const/16 v16, 0x0

    goto :goto_b

    .line 158
    .restart local v16    # "_arg11":Z
    :cond_b
    const/16 v17, 0x0

    goto :goto_c

    .line 162
    .restart local v17    # "_arg12":Z
    .restart local v18    # "_arg13":Ljava/lang/String;
    :cond_c
    const/16 v19, 0x0

    goto :goto_d

    .line 164
    .restart local v19    # "_arg14":Z
    :cond_d
    const/16 v20, 0x0

    goto :goto_e

    .line 166
    .restart local v20    # "_arg15":Z
    :cond_e
    const/16 v21, 0x0

    goto :goto_f

    .line 186
    .restart local v21    # "_arg16":Z
    .restart local v22    # "_arg17":Ljava/lang/String;
    .restart local v23    # "_arg18":Ljava/lang/String;
    .restart local v24    # "_arg19":I
    .restart local v25    # "_arg20":I
    .restart local v26    # "_arg21":I
    .restart local v27    # "_arg22":I
    .restart local v28    # "_arg23":I
    .restart local v29    # "_arg24":I
    .restart local v30    # "_arg25":I
    :cond_f
    const/16 v31, 0x0

    goto :goto_10

    .line 202
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v10    # "_arg5":I
    .end local v11    # "_arg6":I
    .end local v12    # "_arg7":Z
    .end local v13    # "_arg8":Ljava/lang/String;
    .end local v14    # "_arg9":Ljava/lang/String;
    .end local v15    # "_arg10":Ljava/lang/String;
    .end local v16    # "_arg11":Z
    .end local v17    # "_arg12":Z
    .end local v18    # "_arg13":Ljava/lang/String;
    .end local v19    # "_arg14":Z
    .end local v20    # "_arg15":Z
    .end local v21    # "_arg16":Z
    .end local v22    # "_arg17":Ljava/lang/String;
    .end local v23    # "_arg18":Ljava/lang/String;
    .end local v24    # "_arg19":I
    .end local v25    # "_arg20":I
    .end local v26    # "_arg21":I
    .end local v27    # "_arg22":I
    .end local v28    # "_arg23":I
    .end local v29    # "_arg24":I
    .end local v30    # "_arg25":I
    :sswitch_4
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 204
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_10

    .line 205
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 211
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 213
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 215
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 217
    .restart local v8    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .line 219
    .restart local v9    # "_arg4":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v42

    .local v42, "_arg5":J
    move-object/from16 v36, p0

    move-object/from16 v37, v5

    move-object/from16 v38, v6

    move-object/from16 v39, v7

    move-object/from16 v40, v8

    move-object/from16 v41, v9

    .line 220
    invoke-virtual/range {v36 .. v43}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAccountBaseParameters(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v62

    .line 221
    .restart local v62    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 222
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 223
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 208
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    .end local v42    # "_arg5":J
    .end local v62    # "_result":J
    :cond_10
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 227
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_11

    .line 230
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 236
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_12

    const/4 v6, 0x1

    .line 238
    .local v6, "_arg1":Z
    :goto_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 239
    .local v60, "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v62

    .line 240
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 241
    if-eqz v62, :cond_13

    const/4 v4, 0x1

    :goto_14
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 242
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 233
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_11
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 236
    :cond_12
    const/4 v6, 0x0

    goto :goto_13

    .line 241
    .restart local v6    # "_arg1":Z
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_13
    const/4 v4, 0x0

    goto :goto_14

    .line 246
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_6
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_14

    .line 249
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 255
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_15

    const/4 v6, 0x1

    .line 257
    .restart local v6    # "_arg1":Z
    :goto_16
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 258
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v62

    .line 259
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v62, :cond_16

    const/4 v4, 0x1

    :goto_17
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 261
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 252
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_14
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    .line 255
    :cond_15
    const/4 v6, 0x0

    goto :goto_16

    .line 260
    .restart local v6    # "_arg1":Z
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_16
    const/4 v4, 0x0

    goto :goto_17

    .line 265
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_7
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_17

    .line 268
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 274
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_18

    const/4 v6, 0x1

    .line 276
    .restart local v6    # "_arg1":Z
    :goto_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 277
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAlwaysVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v62

    .line 278
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 279
    if-eqz v62, :cond_19

    const/4 v4, 0x1

    :goto_1a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 280
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 271
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_17
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 274
    :cond_18
    const/4 v6, 0x0

    goto :goto_19

    .line 279
    .restart local v6    # "_arg1":Z
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_19
    const/4 v4, 0x0

    goto :goto_1a

    .line 284
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_8
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 286
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1a

    .line 287
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 293
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1b

    const/4 v6, 0x1

    .line 295
    .restart local v6    # "_arg1":Z
    :goto_1c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 296
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSilentVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v62

    .line 297
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 298
    if-eqz v62, :cond_1c

    const/4 v4, 0x1

    :goto_1d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 299
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 290
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_1a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1b

    .line 293
    :cond_1b
    const/4 v6, 0x0

    goto :goto_1c

    .line 298
    .restart local v6    # "_arg1":Z
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_1c
    const/4 v4, 0x0

    goto :goto_1d

    .line 303
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_9
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1d

    .line 306
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 312
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 314
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 315
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v62

    .line 316
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    if-eqz v62, :cond_1e

    const/4 v4, 0x1

    :goto_1f
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 318
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 309
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_1d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 317
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_1e
    const/4 v4, 0x0

    goto :goto_1f

    .line 322
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_a
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 324
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1f

    .line 325
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 331
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 333
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 334
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setProtocolVersion(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v62

    .line 335
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336
    if-eqz v62, :cond_20

    const/4 v4, 0x1

    :goto_21
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 337
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 328
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_1f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 336
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_20
    const/4 v4, 0x0

    goto :goto_21

    .line 341
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_b
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 343
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_21

    .line 344
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 350
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 352
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 353
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSignature(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v62

    .line 354
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 355
    if-eqz v62, :cond_22

    const/4 v4, 0x1

    :goto_23
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 356
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 347
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_21
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 355
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_22
    const/4 v4, 0x0

    goto :goto_23

    .line 360
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_c
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_23

    .line 363
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 369
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .line 371
    .local v6, "_arg1":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 373
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v40

    .local v40, "_arg3":J
    move-object/from16 v36, p0

    move-object/from16 v37, v5

    move-object/from16 v38, v6

    move-object/from16 v39, v7

    .line 374
    invoke-virtual/range {v36 .. v41}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setClientAuthCert(Landroid/app/enterprise/ContextInfo;[BLjava/lang/String;J)V

    .line 375
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 376
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 366
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":[B
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v40    # "_arg3":J
    :cond_23
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 380
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_24

    .line 383
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 389
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 391
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 392
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setPastDaysToSync(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 393
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 394
    if-eqz v62, :cond_25

    const/4 v4, 0x1

    :goto_26
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 395
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 386
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_24
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    .line 394
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_25
    const/4 v4, 0x0

    goto :goto_26

    .line 399
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_e
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 401
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_26

    .line 402
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 408
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 410
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 411
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSyncInterval(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 412
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 413
    if-eqz v62, :cond_27

    const/4 v4, 0x1

    :goto_28
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 414
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 405
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_26
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 413
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_27
    const/4 v4, 0x0

    goto :goto_28

    .line 418
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_f
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 420
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_28

    .line 421
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 427
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 429
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 430
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSenderName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v62

    .line 431
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 432
    if-eqz v62, :cond_29

    const/4 v4, 0x1

    :goto_2a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 433
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 424
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_28
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 432
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_29
    const/4 v4, 0x0

    goto :goto_2a

    .line 437
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_10
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 439
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2a

    .line 440
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 446
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 447
    .local v46, "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAsDefaultAccount(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 448
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 449
    if-eqz v62, :cond_2b

    const/4 v4, 0x1

    :goto_2c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 450
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 443
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_2a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 449
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_2b
    const/4 v4, 0x0

    goto :goto_2c

    .line 454
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_11
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 456
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2c

    .line 457
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 463
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 465
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 466
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAccountName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z

    move-result v62

    .line 467
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 468
    if-eqz v62, :cond_2d

    const/4 v4, 0x1

    :goto_2e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 469
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 460
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_2c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 468
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_2d
    const/4 v4, 0x0

    goto :goto_2e

    .line 473
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_12
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 475
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2e

    .line 476
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 482
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 484
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 486
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 487
    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getAccountId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v62

    .line 488
    .local v62, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 489
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 490
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 479
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_2e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2f

    .line 494
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 496
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2f

    .line 497
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 503
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 504
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getAccountDetails(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/Account;

    move-result-object v62

    .line 505
    .local v62, "_result":Landroid/app/enterprise/Account;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 506
    if-eqz v62, :cond_30

    .line 507
    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 508
    const/4 v4, 0x1

    move-object/from16 v0, v62

    move-object/from16 v1, p3

    invoke-virtual {v0, v1, v4}, Landroid/app/enterprise/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 513
    :goto_31
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 500
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Landroid/app/enterprise/Account;
    :cond_2f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 511
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Landroid/app/enterprise/Account;
    :cond_30
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_31

    .line 517
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Landroid/app/enterprise/Account;
    :sswitch_14
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 519
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_31

    .line 520
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 526
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 527
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->deleteAccount(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 528
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 529
    if-eqz v62, :cond_32

    const/4 v4, 0x1

    :goto_33
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 530
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 523
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_31
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 529
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_32
    const/4 v4, 0x0

    goto :goto_33

    .line 534
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_15
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 536
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_33

    .line 537
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 542
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->sendAccountsChangedBroadcast(Landroid/app/enterprise/ContextInfo;)V

    .line 543
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 544
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 540
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :cond_33
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 548
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 550
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_34

    .line 551
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 557
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_35
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 559
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 561
    .local v7, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 563
    .local v8, "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v50

    .local v50, "_arg4":J
    move-object/from16 v45, p0

    move-object/from16 v46, v5

    move/from16 v47, v6

    move/from16 v48, v7

    move/from16 v49, v8

    .line 564
    invoke-virtual/range {v45 .. v51}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSyncPeakTimings(Landroid/app/enterprise/ContextInfo;IIIJ)Z

    move-result v62

    .line 565
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 566
    if-eqz v62, :cond_35

    const/4 v4, 0x1

    :goto_36
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 567
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 554
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v7    # "_arg2":I
    .end local v8    # "_arg3":I
    .end local v50    # "_arg4":J
    .end local v62    # "_result":Z
    :cond_34
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_35

    .line 566
    .restart local v6    # "_arg1":I
    .restart local v7    # "_arg2":I
    .restart local v8    # "_arg3":I
    .restart local v50    # "_arg4":J
    .restart local v62    # "_result":Z
    :cond_35
    const/4 v4, 0x0

    goto :goto_36

    .line 571
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v7    # "_arg2":I
    .end local v8    # "_arg3":I
    .end local v50    # "_arg4":J
    .end local v62    # "_result":Z
    :sswitch_17
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 573
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_36

    .line 574
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 580
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_37
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 582
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 584
    .restart local v7    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 586
    .restart local v8    # "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v50

    .restart local v50    # "_arg4":J
    move-object/from16 v45, p0

    move-object/from16 v46, v5

    move/from16 v47, v6

    move/from16 v48, v7

    move/from16 v49, v8

    .line 587
    invoke-virtual/range {v45 .. v51}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setSyncSchedules(Landroid/app/enterprise/ContextInfo;IIIJ)Z

    move-result v62

    .line 588
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 589
    if-eqz v62, :cond_37

    const/4 v4, 0x1

    :goto_38
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 590
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 577
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v7    # "_arg2":I
    .end local v8    # "_arg3":I
    .end local v50    # "_arg4":J
    .end local v62    # "_result":Z
    :cond_36
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_37

    .line 589
    .restart local v6    # "_arg1":I
    .restart local v7    # "_arg2":I
    .restart local v8    # "_arg3":I
    .restart local v50    # "_arg4":J
    .restart local v62    # "_result":Z
    :cond_37
    const/4 v4, 0x0

    goto :goto_38

    .line 594
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v7    # "_arg2":I
    .end local v8    # "_arg3":I
    .end local v50    # "_arg4":J
    .end local v62    # "_result":Z
    :sswitch_18
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 596
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_38

    .line 597
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 603
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_39
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_39

    const/4 v6, 0x1

    .line 605
    .local v6, "_arg1":Z
    :goto_3a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3a

    const/4 v7, 0x1

    .line 607
    .local v7, "_arg2":Z
    :goto_3b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3b

    const/4 v8, 0x1

    .line 609
    .local v8, "_arg3":Z
    :goto_3c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3c

    const/4 v9, 0x1

    .line 611
    .local v9, "_arg4":Z
    :goto_3d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v42

    .restart local v42    # "_arg5":J
    move-object/from16 v52, p0

    move-object/from16 v53, v5

    move/from16 v54, v6

    move/from16 v55, v7

    move/from16 v56, v8

    move/from16 v57, v9

    move-wide/from16 v58, v42

    .line 612
    invoke-virtual/range {v52 .. v59}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setDataSyncs(Landroid/app/enterprise/ContextInfo;ZZZZJ)Z

    move-result v62

    .line 613
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 614
    if-eqz v62, :cond_3d

    const/4 v4, 0x1

    :goto_3e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 615
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 600
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v7    # "_arg2":Z
    .end local v8    # "_arg3":Z
    .end local v9    # "_arg4":Z
    .end local v42    # "_arg5":J
    .end local v62    # "_result":Z
    :cond_38
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_39

    .line 603
    :cond_39
    const/4 v6, 0x0

    goto :goto_3a

    .line 605
    .restart local v6    # "_arg1":Z
    :cond_3a
    const/4 v7, 0x0

    goto :goto_3b

    .line 607
    .restart local v7    # "_arg2":Z
    :cond_3b
    const/4 v8, 0x0

    goto :goto_3c

    .line 609
    .restart local v8    # "_arg3":Z
    :cond_3c
    const/4 v9, 0x0

    goto :goto_3d

    .line 614
    .restart local v9    # "_arg4":Z
    .restart local v42    # "_arg5":J
    .restart local v62    # "_result":Z
    :cond_3d
    const/4 v4, 0x0

    goto :goto_3e

    .line 619
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v7    # "_arg2":Z
    .end local v8    # "_arg3":Z
    .end local v9    # "_arg4":Z
    .end local v42    # "_arg5":J
    .end local v62    # "_result":Z
    :sswitch_19
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 621
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3e

    .line 622
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 627
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getAllEASAccounts(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/Account;

    move-result-object v62

    .line 628
    .local v62, "_result":[Landroid/app/enterprise/Account;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 629
    const/4 v4, 0x1

    move-object/from16 v0, p3

    move-object/from16 v1, v62

    invoke-virtual {v0, v1, v4}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    .line 630
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 625
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v62    # "_result":[Landroid/app/enterprise/Account;
    :cond_3e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 634
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 636
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3f

    .line 637
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 642
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_40
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getDeviceId(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v62

    .line 643
    .local v62, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 644
    move-object/from16 v0, p3

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 645
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 640
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v62    # "_result":Ljava/lang/String;
    :cond_3f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_40

    .line 649
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 651
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_40

    .line 652
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 658
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 660
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 662
    .local v7, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 664
    .local v8, "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v9

    .local v9, "_arg4":Ljava/lang/String;
    move-object/from16 v4, p0

    .line 665
    invoke-virtual/range {v4 .. v9}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->removePendingAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 667
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 655
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v9    # "_arg4":Ljava/lang/String;
    :cond_40
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 671
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 673
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_41

    .line 674
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 680
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_42
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 682
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_42

    const/4 v7, 0x1

    .line 683
    .local v7, "_arg2":Z
    :goto_43
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2, v7}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setRequireSignedSMIMEMessages(Landroid/app/enterprise/ContextInfo;JZ)Z

    move-result v62

    .line 684
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 685
    if-eqz v62, :cond_43

    const/4 v4, 0x1

    :goto_44
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 686
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 677
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_41
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_42

    .line 682
    .restart local v46    # "_arg1":J
    :cond_42
    const/4 v7, 0x0

    goto :goto_43

    .line 685
    .restart local v7    # "_arg2":Z
    .restart local v62    # "_result":Z
    :cond_43
    const/4 v4, 0x0

    goto :goto_44

    .line 690
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_1d
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 692
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_44

    .line 693
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 699
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_45
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 700
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getRequireSignedSMIMEMessages(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 701
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 702
    if-eqz v62, :cond_45

    const/4 v4, 0x1

    :goto_46
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 703
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 696
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_44
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_45

    .line 702
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_45
    const/4 v4, 0x0

    goto :goto_46

    .line 707
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_1e
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 709
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_46

    .line 710
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 716
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_47
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 718
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_47

    const/4 v7, 0x1

    .line 719
    .restart local v7    # "_arg2":Z
    :goto_48
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2, v7}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setRequireEncryptedSMIMEMessages(Landroid/app/enterprise/ContextInfo;JZ)Z

    move-result v62

    .line 720
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 721
    if-eqz v62, :cond_48

    const/4 v4, 0x1

    :goto_49
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 722
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 713
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_46
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_47

    .line 718
    .restart local v46    # "_arg1":J
    :cond_47
    const/4 v7, 0x0

    goto :goto_48

    .line 721
    .restart local v7    # "_arg2":Z
    .restart local v62    # "_result":Z
    :cond_48
    const/4 v4, 0x0

    goto :goto_49

    .line 726
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_1f
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 728
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_49

    .line 729
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 735
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 736
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getRequireEncryptedSMIMEMessages(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 737
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 738
    if-eqz v62, :cond_4a

    const/4 v4, 0x1

    :goto_4b
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 739
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 732
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_49
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4a

    .line 738
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_4a
    const/4 v4, 0x0

    goto :goto_4b

    .line 743
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_20
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 745
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4b

    .line 746
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 752
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 754
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 756
    .local v7, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v44, p0

    move-object/from16 v45, v5

    move-object/from16 v48, v7

    move-object/from16 v49, v8

    .line 757
    invoke-virtual/range {v44 .. v49}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setForceSMIMECertificate(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I

    move-result v62

    .line 758
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 759
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 760
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 749
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_4b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4c

    .line 764
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 766
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4c

    .line 767
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 773
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 774
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getForceSMIMECertificate(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 775
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 776
    if-eqz v62, :cond_4d

    const/4 v4, 0x1

    :goto_4e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 777
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 770
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_4c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4d

    .line 776
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_4d
    const/4 v4, 0x0

    goto :goto_4e

    .line 781
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_22
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 783
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4e

    .line 784
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 790
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 791
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setReleaseSMIMECertificate(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 792
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 793
    if-eqz v62, :cond_4f

    const/4 v4, 0x1

    :goto_50
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 794
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 787
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_4e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4f

    .line 793
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_4f
    const/4 v4, 0x0

    goto :goto_50

    .line 798
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_23
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 800
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_50

    .line 801
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 807
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_51
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_51

    const/4 v6, 0x1

    .line 809
    .local v6, "_arg1":Z
    :goto_52
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 810
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->allowInComingAttachments(Landroid/app/enterprise/ContextInfo;ZJ)Z

    move-result v62

    .line 811
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 812
    if-eqz v62, :cond_52

    const/4 v4, 0x1

    :goto_53
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 813
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 804
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_50
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_51

    .line 807
    :cond_51
    const/4 v6, 0x0

    goto :goto_52

    .line 812
    .restart local v6    # "_arg1":Z
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_52
    const/4 v4, 0x0

    goto :goto_53

    .line 817
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_24
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 819
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_53

    .line 820
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 826
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_54
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 827
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->isIncomingAttachmentsAllowed(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 828
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 829
    if-eqz v62, :cond_54

    const/4 v4, 0x1

    :goto_55
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 830
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 823
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_53
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_54

    .line 829
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_54
    const/4 v4, 0x0

    goto :goto_55

    .line 834
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_25
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 836
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_55

    .line 837
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 843
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_56
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 845
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 846
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setIncomingAttachmentsSize(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 847
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 848
    if-eqz v62, :cond_56

    const/4 v4, 0x1

    :goto_57
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 849
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 840
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_55
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_56

    .line 848
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_56
    const/4 v4, 0x0

    goto :goto_57

    .line 853
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_26
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 855
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_57

    .line 856
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 862
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_58
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 863
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getIncomingAttachmentsSize(Landroid/app/enterprise/ContextInfo;J)I

    move-result v62

    .line 864
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 865
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 866
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 859
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_57
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_58

    .line 870
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 872
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_58

    .line 873
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 879
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_59
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 881
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_59

    const/4 v7, 0x1

    .line 882
    .local v7, "_arg2":Z
    :goto_5a
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2, v7}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setEmailNotificationsState(Landroid/app/enterprise/ContextInfo;JZ)Z

    move-result v62

    .line 883
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 884
    if-eqz v62, :cond_5a

    const/4 v4, 0x1

    :goto_5b
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 885
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 876
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_58
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_59

    .line 881
    .restart local v46    # "_arg1":J
    :cond_59
    const/4 v7, 0x0

    goto :goto_5a

    .line 884
    .restart local v7    # "_arg2":Z
    .restart local v62    # "_result":Z
    :cond_5a
    const/4 v4, 0x0

    goto :goto_5b

    .line 889
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_28
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 891
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5b

    .line 892
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 898
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 899
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->isEmailNotificationsEnabled(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 900
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 901
    if-eqz v62, :cond_5c

    const/4 v4, 0x1

    :goto_5d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 902
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 895
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_5b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5c

    .line 901
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_5c
    const/4 v4, 0x0

    goto :goto_5d

    .line 906
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_29
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 908
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5d

    .line 909
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 915
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 917
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5e

    const/4 v7, 0x1

    .line 918
    .restart local v7    # "_arg2":Z
    :goto_5f
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2, v7}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->allowEmailSettingsChange(Landroid/app/enterprise/ContextInfo;JZ)Z

    move-result v62

    .line 919
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 920
    if-eqz v62, :cond_5f

    const/4 v4, 0x1

    :goto_60
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 921
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 912
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_5d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5e

    .line 917
    .restart local v46    # "_arg1":J
    :cond_5e
    const/4 v7, 0x0

    goto :goto_5f

    .line 920
    .restart local v7    # "_arg2":Z
    .restart local v62    # "_result":Z
    :cond_5f
    const/4 v4, 0x0

    goto :goto_60

    .line 925
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Z
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_2a
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 927
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_60

    .line 928
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 934
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_61
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 935
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->isEmailSettingsChangeAllowed(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 936
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 937
    if-eqz v62, :cond_61

    const/4 v4, 0x1

    :goto_62
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 938
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 931
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_60
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_61

    .line 937
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_61
    const/4 v4, 0x0

    goto :goto_62

    .line 942
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_2b
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 944
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_62

    .line 945
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 951
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_63
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 953
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 954
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setMaxCalendarAgeFilter(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 955
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 956
    if-eqz v62, :cond_63

    const/4 v4, 0x1

    :goto_64
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 957
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 948
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_62
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_63

    .line 956
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_63
    const/4 v4, 0x0

    goto :goto_64

    .line 961
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_2c
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 963
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_64

    .line 964
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 970
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_65
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 971
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getMaxCalendarAgeFilter(Landroid/app/enterprise/ContextInfo;J)I

    move-result v62

    .line 972
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 973
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 974
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 967
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_64
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_65

    .line 978
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2d
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 980
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_65

    .line 981
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 987
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_66
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 989
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 990
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setMaxEmailAgeFilter(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 991
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 992
    if-eqz v62, :cond_66

    const/4 v4, 0x1

    :goto_67
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 993
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 984
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_65
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_66

    .line 992
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_66
    const/4 v4, 0x0

    goto :goto_67

    .line 997
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_2e
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 999
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_67

    .line 1000
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1006
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_68
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1007
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getMaxEmailAgeFilter(Landroid/app/enterprise/ContextInfo;J)I

    move-result v62

    .line 1008
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1009
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1010
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1003
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_67
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_68

    .line 1014
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2f
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1016
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_68

    .line 1017
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1023
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_69
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 1025
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 1026
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setMaxEmailBodyTruncationSize(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 1027
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1028
    if-eqz v62, :cond_69

    const/4 v4, 0x1

    :goto_6a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1029
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1020
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_68
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_69

    .line 1028
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_69
    const/4 v4, 0x0

    goto :goto_6a

    .line 1033
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_30
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1035
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6a

    .line 1036
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1042
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1043
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getMaxEmailBodyTruncationSize(Landroid/app/enterprise/ContextInfo;J)I

    move-result v62

    .line 1044
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1045
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1046
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1039
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_6a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6b

    .line 1050
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_31
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1052
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6b

    .line 1053
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1059
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 1061
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v60

    .line 1062
    .restart local v60    # "_arg2":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v60

    invoke-virtual {v0, v5, v6, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setMaxEmailHTMLBodyTruncationSize(Landroid/app/enterprise/ContextInfo;IJ)Z

    move-result v62

    .line 1063
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1064
    if-eqz v62, :cond_6c

    const/4 v4, 0x1

    :goto_6d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1065
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1056
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :cond_6b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6c

    .line 1064
    .restart local v6    # "_arg1":I
    .restart local v60    # "_arg2":J
    .restart local v62    # "_result":Z
    :cond_6c
    const/4 v4, 0x0

    goto :goto_6d

    .line 1069
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v60    # "_arg2":J
    .end local v62    # "_result":Z
    :sswitch_32
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1071
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6d

    .line 1072
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1078
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1079
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getMaxEmailHTMLBodyTruncationSize(Landroid/app/enterprise/ContextInfo;J)I

    move-result v62

    .line 1080
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1081
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1082
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1075
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_6d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6e

    .line 1086
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_33
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1088
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6e

    .line 1089
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1095
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1097
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 1099
    .local v7, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v44, p0

    move-object/from16 v45, v5

    move-object/from16 v48, v7

    move-object/from16 v49, v8

    .line 1100
    invoke-virtual/range {v44 .. v49}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setForceSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I

    move-result v62

    .line 1101
    .restart local v62    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1102
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1103
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1092
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_6e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6f

    .line 1107
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_34
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1109
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6f

    .line 1110
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1116
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_70
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1117
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getForceSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 1118
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1119
    if-eqz v62, :cond_70

    const/4 v4, 0x1

    :goto_71
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1120
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1113
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_6f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_70

    .line 1119
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_70
    const/4 v4, 0x0

    goto :goto_71

    .line 1124
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_35
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1126
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_71

    .line 1127
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1133
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_72
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1134
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setReleaseSMIMECertificateForSigning(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 1135
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1136
    if-eqz v62, :cond_72

    const/4 v4, 0x1

    :goto_73
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1137
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1130
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_71
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_72

    .line 1136
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_72
    const/4 v4, 0x0

    goto :goto_73

    .line 1141
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_36
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1143
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_73

    .line 1144
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1150
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_74
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1152
    .restart local v46    # "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 1154
    .restart local v7    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "_arg3":Ljava/lang/String;
    move-object/from16 v44, p0

    move-object/from16 v45, v5

    move-object/from16 v48, v7

    move-object/from16 v49, v8

    .line 1155
    invoke-virtual/range {v44 .. v49}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setForceSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;JLjava/lang/String;Ljava/lang/String;)I

    move-result v62

    .line 1156
    .local v62, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1157
    move-object/from16 v0, p3

    move/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1158
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1147
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_arg2":Ljava/lang/String;
    .end local v8    # "_arg3":Ljava/lang/String;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":I
    :cond_73
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_74

    .line 1162
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_37
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_74

    .line 1165
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1171
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_75
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1172
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getForceSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 1173
    .local v62, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1174
    if-eqz v62, :cond_75

    const/4 v4, 0x1

    :goto_76
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1175
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1168
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_74
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_75

    .line 1174
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_75
    const/4 v4, 0x0

    goto :goto_76

    .line 1179
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_38
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1181
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_76

    .line 1182
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1188
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_77
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1189
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setReleaseSMIMECertificateForEncryption(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v62

    .line 1190
    .restart local v62    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1191
    if-eqz v62, :cond_77

    const/4 v4, 0x1

    :goto_78
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1192
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1185
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :cond_76
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_77

    .line 1191
    .restart local v46    # "_arg1":J
    .restart local v62    # "_result":Z
    :cond_77
    const/4 v4, 0x0

    goto :goto_78

    .line 1196
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Z
    :sswitch_39
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1198
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_78

    .line 1199
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1205
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_79
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_79

    .line 1206
    sget-object v4, Landroid/app/enterprise/ExchangeAccount;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/enterprise/ExchangeAccount;

    .line 1211
    .local v6, "_arg1":Landroid/app/enterprise/ExchangeAccount;
    :goto_7a
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->addNewAccount_new(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/ExchangeAccount;)J

    move-result-wide v62

    .line 1212
    .local v62, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1213
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 1214
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1202
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Landroid/app/enterprise/ExchangeAccount;
    .end local v62    # "_result":J
    :cond_78
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_79

    .line 1209
    :cond_79
    const/4 v6, 0x0

    .restart local v6    # "_arg1":Landroid/app/enterprise/ExchangeAccount;
    goto :goto_7a

    .line 1218
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Landroid/app/enterprise/ExchangeAccount;
    :sswitch_3a
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1220
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7a

    .line 1221
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1227
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1228
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getAccountEmailPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;

    move-result-object v62

    .line 1229
    .local v62, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1230
    move-object/from16 v0, p3

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1231
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1224
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Ljava/lang/String;
    :cond_7a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7b

    .line 1235
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3b
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1237
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7b

    .line 1238
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1244
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7c
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v46

    .line 1245
    .restart local v46    # "_arg1":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v46

    invoke-virtual {v0, v5, v1, v2}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->getAccountCertificatePassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;

    move-result-object v62

    .line 1246
    .restart local v62    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1247
    move-object/from16 v0, p3

    move-object/from16 v1, v62

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1248
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1241
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v46    # "_arg1":J
    .end local v62    # "_result":Ljava/lang/String;
    :cond_7b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7c

    .line 1252
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3c
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1254
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7c

    .line 1255
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1261
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1262
    .local v6, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAccountEmailPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v62

    .line 1263
    .local v62, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1264
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 1265
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1258
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_7c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7d

    .line 1269
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3d
    const-string v4, "android.app.enterprise.IExchangeAccountPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1271
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7d

    .line 1272
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1278
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1279
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IExchangeAccountPolicy$Stub;->setAccountCertificatePassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J

    move-result-wide v62

    .line 1280
    .restart local v62    # "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1281
    move-object/from16 v0, p3

    move-wide/from16 v1, v62

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 1282
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1275
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v62    # "_result":J
    :cond_7d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7e

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
