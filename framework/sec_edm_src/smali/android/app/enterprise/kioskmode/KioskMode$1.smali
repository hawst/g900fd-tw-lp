.class Landroid/app/enterprise/kioskmode/KioskMode$1;
.super Ljava/lang/Object;
.source "KioskMode.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/app/enterprise/kioskmode/KioskMode;->enableKioskMode(Landroid/app/enterprise/kioskmode/KioskSetting;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/app/enterprise/kioskmode/KioskMode;

.field final synthetic val$kiosk:Landroid/app/enterprise/kioskmode/KioskSetting;

.field final synthetic val$km:Landroid/app/enterprise/kioskmode/IKioskMode;


# direct methods
.method constructor <init>(Landroid/app/enterprise/kioskmode/KioskMode;Landroid/app/enterprise/kioskmode/KioskSetting;Landroid/app/enterprise/kioskmode/IKioskMode;)V
    .locals 0

    .prologue
    .line 1235
    iput-object p1, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    iput-object p2, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$kiosk:Landroid/app/enterprise/kioskmode/KioskSetting;

    iput-object p3, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1238
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$kiosk:Landroid/app/enterprise/kioskmode/KioskSetting;

    .line 1239
    .local v4, "kiosksetting":Landroid/app/enterprise/kioskmode/KioskSetting;
    if-nez v4, :cond_0

    .line 1240
    new-instance v4, Landroid/app/enterprise/kioskmode/KioskSetting;

    .end local v4    # "kiosksetting":Landroid/app/enterprise/kioskmode/KioskSetting;
    invoke-direct {v4}, Landroid/app/enterprise/kioskmode/KioskSetting;-><init>()V

    .line 1241
    .restart local v4    # "kiosksetting":Landroid/app/enterprise/kioskmode/KioskSetting;
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HomeKey:Z

    .line 1242
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SettingsChanges:Z

    .line 1243
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBarExpansion:Z

    .line 1244
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->AirCommand:Z

    .line 1245
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->AirView:Z

    .line 1246
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->MultiWindow:Z

    .line 1247
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SmartClip:Z

    .line 1248
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->TaskManager:Z

    .line 1249
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->ClearAllNotifications:Z

    .line 1250
    iput-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->NavigationBar:Z

    .line 1251
    iput-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBar:Z

    .line 1252
    iput-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SystemBar:Z

    .line 1253
    iput-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->WipeRecentTasks:Z

    .line 1256
    :cond_0
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # invokes: Landroid/app/enterprise/kioskmode/KioskMode;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;
    invoke-static {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->access$000(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v6

    .line 1257
    .local v6, "rp":Landroid/app/enterprise/IRestrictionPolicy;
    if-nez v6, :cond_4

    .line 1258
    const-string v7, "KioskMode"

    const-string v8, "Failed talking with restriction service"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1307
    :cond_1
    :goto_0
    :try_start_0
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->AirCommand:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowAirCommandMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1309
    .local v5, "ret":Z
    if-nez v5, :cond_2

    .line 1310
    const-string v7, "KioskMode"

    const-string v8, "set air command mode failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_4

    .line 1318
    .end local v5    # "ret":Z
    :cond_2
    :goto_1
    :try_start_1
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->AirView:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowAirViewMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1320
    .restart local v5    # "ret":Z
    if-nez v5, :cond_3

    .line 1321
    const-string v7, "KioskMode"

    const-string v8, "set air view mode failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5

    .line 1328
    .end local v5    # "ret":Z
    :cond_3
    :goto_2
    iget-object v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    if-eqz v7, :cond_9

    .line 1329
    iget-object v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    new-array v2, v7, [I

    .line 1330
    .local v2, "keyArray":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    iget-object v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_8

    .line 1331
    iget-object v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v2, v1

    .line 1330
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1262
    .end local v1    # "i":I
    .end local v2    # "keyArray":[I
    :cond_4
    :try_start_2
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v7

    iget-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SettingsChanges:Z

    invoke-interface {v6, v7, v8}, Landroid/app/enterprise/IRestrictionPolicy;->allowSettingsChanges(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1264
    .restart local v5    # "ret":Z
    if-nez v5, :cond_5

    .line 1265
    const-string v7, "KioskMode"

    const-string v8, "allow settings changes failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    .line 1273
    .end local v5    # "ret":Z
    :cond_5
    :goto_4
    :try_start_3
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v7

    iget-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBarExpansion:Z

    invoke-interface {v6, v7, v8}, Landroid/app/enterprise/IRestrictionPolicy;->allowStatusBarExpansion(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1275
    .restart local v5    # "ret":Z
    if-nez v5, :cond_6

    .line 1276
    const-string v7, "KioskMode"

    const-string v8, "allow status bar expansion failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1284
    .end local v5    # "ret":Z
    :cond_6
    :goto_5
    :try_start_4
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v7

    iget-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->HomeKey:Z

    invoke-interface {v6, v7, v8}, Landroid/app/enterprise/IRestrictionPolicy;->setHomeKeyState(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1286
    .restart local v5    # "ret":Z
    if-nez v5, :cond_7

    .line 1287
    const-string v7, "KioskMode"

    const-string v8, "set home key state failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_3

    .line 1295
    .end local v5    # "ret":Z
    :cond_7
    :goto_6
    :try_start_5
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v7

    iget-boolean v8, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SmartClip:Z

    invoke-interface {v6, v7, v8}, Landroid/app/enterprise/IRestrictionPolicy;->allowSmartClipMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1297
    .restart local v5    # "ret":Z
    if-nez v5, :cond_1

    .line 1298
    const-string v7, "KioskMode"

    const-string v8, "allow smart clip mode failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    goto/16 :goto_0

    .line 1301
    .end local v5    # "ret":Z
    :catch_0
    move-exception v0

    .line 1302
    .local v0, "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow smart clip mode"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1268
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 1269
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow settings changes"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 1279
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_2
    move-exception v0

    .line 1280
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow status bar expansion"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 1290
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_3
    move-exception v0

    .line 1291
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to set home key state"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    .line 1313
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v0

    .line 1314
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow air command mode"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 1324
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_5
    move-exception v0

    .line 1325
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow air view mode"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 1335
    .end local v0    # "e":Landroid/os/RemoteException;
    .restart local v1    # "i":I
    .restart local v2    # "keyArray":[I
    :cond_8
    :try_start_6
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    const/4 v9, 0x0

    invoke-interface {v7, v8, v2, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowHardwareKeys(Landroid/app/enterprise/ContextInfo;[IZ)[I

    move-result-object v3

    .line 1337
    .local v3, "keys":[I
    if-nez v3, :cond_9

    .line 1338
    const-string v7, "KioskMode"

    const-string v8, "allowHardwareKeys failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    .line 1347
    .end local v1    # "i":I
    .end local v2    # "keyArray":[I
    .end local v3    # "keys":[I
    :cond_9
    :goto_7
    :try_start_7
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->MultiWindow:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowMultiWindowMode(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1349
    .restart local v5    # "ret":Z
    if-nez v5, :cond_a

    .line 1350
    const-string v7, "KioskMode"

    const-string v8, "set multiwindow mode failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_7

    .line 1358
    .end local v5    # "ret":Z
    :cond_a
    :goto_8
    :try_start_8
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->TaskManager:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowTaskManager(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1360
    .restart local v5    # "ret":Z
    if-nez v5, :cond_b

    .line 1361
    const-string v7, "KioskMode"

    const-string v8, "set task manager failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_8

    .line 1368
    .end local v5    # "ret":Z
    :cond_b
    :goto_9
    iget-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->ClearAllNotifications:Z

    if-eqz v7, :cond_c

    .line 1370
    :try_start_9
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    invoke-interface {v7, v8}, Landroid/app/enterprise/kioskmode/IKioskMode;->clearAllNotifications(Landroid/app/enterprise/ContextInfo;)Z

    move-result v5

    .line 1372
    .restart local v5    # "ret":Z
    if-nez v5, :cond_c

    .line 1373
    const-string v7, "KioskMode"

    const-string v8, "clear all notifications failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_9} :catch_9

    .line 1382
    .end local v5    # "ret":Z
    :cond_c
    :goto_a
    :try_start_a
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->NavigationBar:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideNavigationBar(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1384
    .restart local v5    # "ret":Z
    if-nez v5, :cond_d

    .line 1385
    const-string v7, "KioskMode"

    const-string v8, "hide navigationbar failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_a

    .line 1393
    .end local v5    # "ret":Z
    :cond_d
    :goto_b
    :try_start_b
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBar:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideStatusBar(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1395
    .restart local v5    # "ret":Z
    if-nez v5, :cond_e

    .line 1396
    const-string v7, "KioskMode"

    const-string v8, "hide status bar failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_b .. :try_end_b} :catch_b

    .line 1404
    .end local v5    # "ret":Z
    :cond_e
    :goto_c
    :try_start_c
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    iget-boolean v9, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->SystemBar:Z

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideSystemBar(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v5

    .line 1406
    .restart local v5    # "ret":Z
    if-nez v5, :cond_f

    .line 1407
    const-string v7, "KioskMode"

    const-string v8, "hide system bar failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_c

    .line 1414
    .end local v5    # "ret":Z
    :cond_f
    :goto_d
    iget-boolean v7, v4, Landroid/app/enterprise/kioskmode/KioskSetting;->WipeRecentTasks:Z

    if-eqz v7, :cond_10

    .line 1416
    :try_start_d
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    invoke-interface {v7, v8}, Landroid/app/enterprise/kioskmode/IKioskMode;->wipeRecentTasks(Landroid/app/enterprise/ContextInfo;)Z

    move-result v5

    .line 1418
    .restart local v5    # "ret":Z
    if-nez v5, :cond_10

    .line 1419
    const-string v7, "KioskMode"

    const-string v8, "wipe recent task failed"

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_d

    .line 1427
    .end local v5    # "ret":Z
    :cond_10
    :goto_e
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    invoke-virtual {v7}, Landroid/app/enterprise/kioskmode/KioskMode;->isKioskModeEnabled()Z

    move-result v7

    if-nez v7, :cond_11

    .line 1429
    :try_start_e
    iget-object v7, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->val$km:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode$1;->this$0:Landroid/app/enterprise/kioskmode/KioskMode;

    # getter for: Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;
    invoke-static {v8}, Landroid/app/enterprise/kioskmode/KioskMode;->access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;

    move-result-object v8

    const-string v9, "com.sec.android.kiosk"

    invoke-interface {v7, v8, v9}, Landroid/app/enterprise/kioskmode/IKioskMode;->enableKioskMode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_e} :catch_e

    .line 1435
    :cond_11
    :goto_f
    return-void

    .line 1341
    .restart local v1    # "i":I
    .restart local v2    # "keyArray":[I
    :catch_6
    move-exception v0

    .line 1342
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow hardware keys"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_7

    .line 1353
    .end local v0    # "e":Landroid/os/RemoteException;
    .end local v1    # "i":I
    .end local v2    # "keyArray":[I
    :catch_7
    move-exception v0

    .line 1354
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow multiwindow mode"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_8

    .line 1364
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_8
    move-exception v0

    .line 1365
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to allow task manager"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_9

    .line 1376
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_9
    move-exception v0

    .line 1377
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to clear all notifications"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_a

    .line 1388
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_a
    move-exception v0

    .line 1389
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to hide navigationbar"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_b

    .line 1399
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_b
    move-exception v0

    .line 1400
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to hide status bar"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_c

    .line 1410
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_c
    move-exception v0

    .line 1411
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to hide system bar"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_d

    .line 1422
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_d
    move-exception v0

    .line 1423
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed to wipe recent task"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_e

    .line 1430
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_e
    move-exception v0

    .line 1431
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v7, "KioskMode"

    const-string v8, "Failed talking with kiosk mode service"

    invoke-static {v7, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_f
.end method
