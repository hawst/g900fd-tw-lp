.class public Landroid/app/enterprise/kioskmode/KioskSetting;
.super Ljava/lang/Object;
.source "KioskSetting.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/kioskmode/KioskSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public AirCommand:Z

.field public AirView:Z

.field public ClearAllNotifications:Z

.field public HardwareKey:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public HomeKey:Z

.field public MultiWindow:Z

.field public NavigationBar:Z

.field public SettingsChanges:Z

.field public SmartClip:Z

.field public StatusBar:Z

.field public StatusBarExpansion:Z

.field public SystemBar:Z

.field public TaskManager:Z

.field public WipeRecentTasks:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 157
    new-instance v0, Landroid/app/enterprise/kioskmode/KioskSetting$1;

    invoke-direct {v0}, Landroid/app/enterprise/kioskmode/KioskSetting$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/kioskmode/KioskSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-virtual {p0, p1}, Landroid/app/enterprise/kioskmode/KioskSetting;->readFromParcel(Landroid/os/Parcel;)V

    .line 76
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 8
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    :goto_0
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SettingsChanges:Z

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    move v3, v4

    :goto_1
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBarExpansion:Z

    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    move v3, v4

    :goto_2
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HomeKey:Z

    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    move v3, v4

    :goto_3
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->AirCommand:Z

    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_4
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->AirView:Z

    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_5

    move v2, v4

    .line 126
    .local v2, "isHardwareArray":Z
    :goto_5
    if-eqz v2, :cond_6

    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    .line 129
    .local v0, "hardwareKeyArray":[I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    .line 131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_6
    array-length v3, v0

    if-ge v1, v3, :cond_6

    .line 132
    iget-object v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    new-instance v6, Ljava/lang/Integer;

    aget v7, v0, v1

    invoke-direct {v6, v7}, Ljava/lang/Integer;-><init>(I)V

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .end local v0    # "hardwareKeyArray":[I
    .end local v1    # "i":I
    .end local v2    # "isHardwareArray":Z
    :cond_0
    move v3, v5

    .line 119
    goto :goto_0

    :cond_1
    move v3, v5

    .line 120
    goto :goto_1

    :cond_2
    move v3, v5

    .line 121
    goto :goto_2

    :cond_3
    move v3, v5

    .line 122
    goto :goto_3

    :cond_4
    move v3, v5

    .line 123
    goto :goto_4

    :cond_5
    move v2, v5

    .line 125
    goto :goto_5

    .line 136
    .restart local v2    # "isHardwareArray":Z
    :cond_6
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_7

    move v3, v4

    :goto_7
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->MultiWindow:Z

    .line 137
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_8

    move v3, v4

    :goto_8
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SmartClip:Z

    .line 138
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_9

    move v3, v4

    :goto_9
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->TaskManager:Z

    .line 139
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_a

    move v3, v4

    :goto_a
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->ClearAllNotifications:Z

    .line 140
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_b

    move v3, v4

    :goto_b
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->NavigationBar:Z

    .line 141
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_c

    move v3, v4

    :goto_c
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBar:Z

    .line 142
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_d

    move v3, v4

    :goto_d
    iput-boolean v3, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SystemBar:Z

    .line 143
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_e

    :goto_e
    iput-boolean v4, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->WipeRecentTasks:Z

    .line 144
    return-void

    :cond_7
    move v3, v5

    .line 136
    goto :goto_7

    :cond_8
    move v3, v5

    .line 137
    goto :goto_8

    :cond_9
    move v3, v5

    .line 138
    goto :goto_9

    :cond_a
    move v3, v5

    .line 139
    goto :goto_a

    :cond_b
    move v3, v5

    .line 140
    goto :goto_b

    :cond_c
    move v3, v5

    .line 141
    goto :goto_c

    :cond_d
    move v3, v5

    .line 142
    goto :goto_d

    :cond_e
    move v4, v5

    .line 143
    goto :goto_e
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 83
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SettingsChanges:Z

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBarExpansion:Z

    if-eqz v2, :cond_1

    move v2, v3

    :goto_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HomeKey:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->AirCommand:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_3
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->AirView:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_4
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    if-eqz v2, :cond_6

    .line 91
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v0, v2, [I

    .line 94
    .local v0, "hardwareKeyArray":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_5
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_5

    .line 95
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->HardwareKey:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v0, v1

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .end local v0    # "hardwareKeyArray":[I
    .end local v1    # "i":I
    :cond_0
    move v2, v4

    .line 83
    goto :goto_0

    :cond_1
    move v2, v4

    .line 84
    goto :goto_1

    :cond_2
    move v2, v4

    .line 85
    goto :goto_2

    :cond_3
    move v2, v4

    .line 86
    goto :goto_3

    :cond_4
    move v2, v4

    .line 87
    goto :goto_4

    .line 98
    .restart local v0    # "hardwareKeyArray":[I
    .restart local v1    # "i":I
    :cond_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 104
    .end local v0    # "hardwareKeyArray":[I
    .end local v1    # "i":I
    :goto_6
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->MultiWindow:Z

    if-eqz v2, :cond_7

    move v2, v3

    :goto_7
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SmartClip:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_8
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->TaskManager:Z

    if-eqz v2, :cond_9

    move v2, v3

    :goto_9
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->ClearAllNotifications:Z

    if-eqz v2, :cond_a

    move v2, v3

    :goto_a
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->NavigationBar:Z

    if-eqz v2, :cond_b

    move v2, v3

    :goto_b
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 109
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->StatusBar:Z

    if-eqz v2, :cond_c

    move v2, v3

    :goto_c
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->SystemBar:Z

    if-eqz v2, :cond_d

    move v2, v3

    :goto_d
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    iget-boolean v2, p0, Landroid/app/enterprise/kioskmode/KioskSetting;->WipeRecentTasks:Z

    if-eqz v2, :cond_e

    :goto_e
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 112
    return-void

    .line 101
    :cond_6
    invoke-virtual {p1, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    :cond_7
    move v2, v4

    .line 104
    goto :goto_7

    :cond_8
    move v2, v4

    .line 105
    goto :goto_8

    :cond_9
    move v2, v4

    .line 106
    goto :goto_9

    :cond_a
    move v2, v4

    .line 107
    goto :goto_a

    :cond_b
    move v2, v4

    .line 108
    goto :goto_b

    :cond_c
    move v2, v4

    .line 109
    goto :goto_c

    :cond_d
    move v2, v4

    .line 110
    goto :goto_d

    :cond_e
    move v3, v4

    .line 111
    goto :goto_e
.end method
