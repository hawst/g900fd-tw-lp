.class public Landroid/app/enterprise/kioskmode/KioskMode;
.super Ljava/lang/Object;
.source "KioskMode.java"


# static fields
.field public static final ACTION_DISABLE_KIOSK_MODE_RESULT:Ljava/lang/String; = "edm.intent.action.disable.kiosk.mode.result"

.field public static final ACTION_ENABLE_KIOSK_MODE_RESULT:Ljava/lang/String; = "edm.intent.action.enable.kiosk.mode.result"

.field public static final ACTION_UNEXPECTED_KIOSK_BEHAVIOR:Ljava/lang/String; = "edm.intent.action.unexpected.kiosk.behavior"

.field public static CONTROL_PANEL_PKGNAME:Ljava/lang/String; = null

.field private static final DEFAULT_KIOSK_PKG:Ljava/lang/String; = "com.sec.android.kiosk"

.field public static final ERROR_BUSY:I = -0x4

.field public static final ERROR_KIOSK_ALREADY_ENABLED:I = -0x1

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_PACKAGE_NOT_FOUND:I = -0x3

.field public static final ERROR_PERMISSION_DENIED:I = -0x2

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field public static final EXTRA_KIOSK_RESULT:Ljava/lang/String; = "edm.intent.extra.kiosk.mode.result"

.field public static MINI_TASK_MANAGER_PKGNAME:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "KioskMode"

.field public static TASK_MANAGER_PKGNAME:Ljava/lang/String;

.field private static gKioskMode:Landroid/app/enterprise/kioskmode/KioskMode;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

.field private mRestrictionPolicy:Landroid/app/enterprise/IRestrictionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/kioskmode/KioskMode;->mSync:Ljava/lang/Object;

    .line 85
    const-string v0, "com.sec.android.app.controlpanel"

    sput-object v0, Landroid/app/enterprise/kioskmode/KioskMode;->CONTROL_PANEL_PKGNAME:Ljava/lang/String;

    .line 88
    const-string v0, "com.sec.android.app.taskmanager"

    sput-object v0, Landroid/app/enterprise/kioskmode/KioskMode;->TASK_MANAGER_PKGNAME:Ljava/lang/String;

    .line 91
    const-string v0, "com.sec.minimode.taskcloser"

    sput-object v0, Landroid/app/enterprise/kioskmode/KioskMode;->MINI_TASK_MANAGER_PKGNAME:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    .line 177
    iput-object p1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 178
    return-void
.end method

.method static synthetic access$000(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/IRestrictionPolicy;
    .locals 1
    .param p0, "x0"    # Landroid/app/enterprise/kioskmode/KioskMode;

    .prologue
    .line 70
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Landroid/app/enterprise/kioskmode/KioskMode;)Landroid/app/enterprise/ContextInfo;
    .locals 1
    .param p0, "x0"    # Landroid/app/enterprise/kioskmode/KioskMode;

    .prologue
    .line 70
    iget-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    return-object v0
.end method

.method private getCallingPackage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 399
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 400
    .local v1, "uid":I
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "packages":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    return-object v2
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/kioskmode/KioskMode;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 222
    sget-object v1, Landroid/app/enterprise/kioskmode/KioskMode;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 223
    :try_start_0
    new-instance v0, Landroid/app/enterprise/kioskmode/KioskMode;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/kioskmode/KioskMode;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v1

    return-object v0

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/kioskmode/KioskMode;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 213
    sget-object v2, Landroid/app/enterprise/kioskmode/KioskMode;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 214
    :try_start_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 215
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/kioskmode/KioskMode;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Landroid/app/enterprise/kioskmode/KioskMode;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v2

    return-object v1

    .line 216
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getRestrictionService()Landroid/app/enterprise/IRestrictionPolicy;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mRestrictionPolicy:Landroid/app/enterprise/IRestrictionPolicy;

    if-nez v0, :cond_0

    .line 190
    const-string v0, "restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IRestrictionPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mRestrictionPolicy:Landroid/app/enterprise/IRestrictionPolicy;

    .line 193
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mRestrictionPolicy:Landroid/app/enterprise/IRestrictionPolicy;

    return-object v0
.end method

.method private getService()Landroid/app/enterprise/kioskmode/IKioskMode;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    if-nez v0, :cond_0

    .line 182
    const-string v0, "kioskmode"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/kioskmode/IKioskMode$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    .line 185
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    return-object v0
.end method

.method private isEnableKioskModeAllowed()Z
    .locals 5

    .prologue
    .line 408
    const/4 v1, 0x0

    .line 410
    .local v1, "rtn":Z
    iget-object v3, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v3, v3, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v3}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v2

    .line 411
    .local v2, "user_id":I
    if-nez v2, :cond_0

    iget-object v3, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v3, v3, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    if-nez v3, :cond_0

    .line 412
    iget-object v3, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    const-string v4, "persona"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 413
    .local v0, "persona":Landroid/os/PersonaManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/os/PersonaManager;->isKioskContainerExistOnDevice()Z

    move-result v3

    if-nez v3, :cond_0

    .line 414
    const/4 v1, 0x1

    .line 417
    .end local v0    # "persona":Landroid/os/PersonaManager;
    :cond_0
    return v1
.end method


# virtual methods
.method public allowAirCommandMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1744
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.allowAirCommandMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1745
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1747
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowAirCommandMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1752
    :goto_0
    return v1

    .line 1748
    :catch_0
    move-exception v0

    .line 1749
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with Kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1752
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowAirViewMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1782
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.allowAirViewMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1783
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1785
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowAirViewMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1790
    :goto_0
    return v1

    .line 1786
    :catch_0
    move-exception v0

    .line 1787
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with Kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1790
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowHardwareKeys(Ljava/util/List;Z)Ljava/util/List;
    .locals 9
    .param p2, "allow"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "hwKeyId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v7, 0x0

    .line 790
    iget-object v6, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v8, "KioskMode.allowHardwareKeys"

    invoke-static {v6, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 791
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 793
    if-eqz p1, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x0

    invoke-interface {p1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 794
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    new-array v4, v6, [I

    .line 795
    .local v4, "keysArray":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v6, v4

    if-ge v2, v6, :cond_0

    .line 796
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput v6, v4, v2

    .line 795
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 797
    :cond_0
    iget-object v6, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v8, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v6, v8, v4, p2}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowHardwareKeys(Landroid/app/enterprise/ContextInfo;[IZ)[I

    move-result-object v5

    .line 798
    .local v5, "ret":[I
    if-eqz v5, :cond_1

    .line 799
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 800
    .local v1, "hwKeyIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "index":I
    :goto_1
    array-length v6, v5

    if-ge v3, v6, :cond_2

    .line 801
    aget v6, v5, v3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 800
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 806
    .end local v1    # "hwKeyIdsList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "i":I
    .end local v3    # "index":I
    .end local v4    # "keysArray":[I
    .end local v5    # "ret":[I
    :catch_0
    move-exception v0

    .line 807
    .local v0, "e":Landroid/os/RemoteException;
    const-string v6, "KioskMode"

    const-string v8, "Failed talking with kiosk mode service"

    invoke-static {v6, v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    move-object v1, v7

    .line 810
    :cond_2
    return-object v1
.end method

.method public allowMultiWindowMode(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1096
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.allowMultiWindowMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1097
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1099
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowMultiWindowMode(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1104
    :goto_0
    return v1

    .line 1100
    :catch_0
    move-exception v0

    .line 1101
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1104
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowSmartClipMode(Z)Z
    .locals 1
    .param p1, "allow"    # Z

    .prologue
    .line 1713
    const/4 v0, 0x0

    return v0
.end method

.method public allowTaskManager(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 645
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.allowTaskManager"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 646
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 648
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->allowTaskManager(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 653
    :goto_0
    return v1

    .line 649
    :catch_0
    move-exception v0

    .line 650
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 653
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearAllNotifications()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1168
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "KioskMode.clearAllNotifications"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1169
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1171
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v3, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/kioskmode/IKioskMode;->clearAllNotifications(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1178
    :cond_0
    :goto_0
    return v1

    .line 1173
    :catch_0
    move-exception v0

    .line 1174
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "KioskMode"

    const-string v3, "Failed to clear notification bar"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public disableKioskMode()V
    .locals 6

    .prologue
    .line 372
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KioskMode.disableKioskMode"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 373
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v2

    .line 374
    .local v2, "km":Landroid/app/enterprise/kioskmode/IKioskMode;
    if-eqz v2, :cond_0

    .line 376
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v4}, Landroid/app/enterprise/kioskmode/IKioskMode;->disableKioskMode(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 390
    :goto_0
    return-void

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KioskMode"

    const-string v5, "Failed talking with kiosk mode service"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 384
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v4, v4, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 385
    .local v3, "user_id":I
    new-instance v1, Landroid/content/Intent;

    const-string v4, "edm.intent.action.disable.kiosk.mode.result"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 386
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "edm.intent.extra.kiosk.mode.result"

    const/16 v5, -0x7d0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 387
    const-string v4, "admin_uid"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 388
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 389
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    const-string v5, "android.permission.sec.MDM_KIOSK_MODE"

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public disableKioskMode(Landroid/app/enterprise/kioskmode/KioskSetting;)V
    .locals 3
    .param p1, "kiosk"    # Landroid/app/enterprise/kioskmode/KioskSetting;

    .prologue
    .line 1486
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.disableKioskMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1487
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v0

    .line 1488
    .local v0, "km":Landroid/app/enterprise/kioskmode/IKioskMode;
    if-eqz v0, :cond_0

    .line 1489
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Landroid/app/enterprise/kioskmode/KioskMode$2;

    invoke-direct {v2, p0, v0, p1}, Landroid/app/enterprise/kioskmode/KioskMode$2;-><init>(Landroid/app/enterprise/kioskmode/KioskMode;Landroid/app/enterprise/kioskmode/IKioskMode;Landroid/app/enterprise/kioskmode/KioskSetting;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 1692
    :cond_0
    return-void
.end method

.method public enableKioskMode()V
    .locals 6

    .prologue
    .line 263
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KioskMode.enableKioskMode"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 264
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v2

    .line 265
    .local v2, "km":Landroid/app/enterprise/kioskmode/IKioskMode;
    if-eqz v2, :cond_0

    .line 267
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "com.sec.android.kiosk"

    invoke-interface {v2, v4, v5}, Landroid/app/enterprise/kioskmode/IKioskMode;->enableKioskMode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KioskMode"

    const-string v5, "Failed talking with kiosk mode service"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v4, v4, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 276
    .local v3, "user_id":I
    new-instance v1, Landroid/content/Intent;

    const-string v4, "edm.intent.action.enable.kiosk.mode.result"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 277
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "edm.intent.extra.kiosk.mode.result"

    const/16 v5, -0x7d0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 278
    const-string v4, "admin_uid"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 279
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    const-string v5, "android.permission.sec.MDM_KIOSK_MODE"

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public enableKioskMode(Landroid/app/enterprise/kioskmode/KioskSetting;)V
    .locals 3
    .param p1, "kiosk"    # Landroid/app/enterprise/kioskmode/KioskSetting;

    .prologue
    .line 1226
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.enableKioskMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1228
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->isEnableKioskModeAllowed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1229
    const-string v1, "KioskMode"

    const-string v2, "call enableKioskMode is not allowed"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1439
    :cond_0
    :goto_0
    return-void

    .line 1233
    :cond_1
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v0

    .line 1234
    .local v0, "km":Landroid/app/enterprise/kioskmode/IKioskMode;
    if-eqz v0, :cond_0

    .line 1235
    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Landroid/app/enterprise/kioskmode/KioskMode$1;

    invoke-direct {v2, p0, p1, v0}, Landroid/app/enterprise/kioskmode/KioskMode$1;-><init>(Landroid/app/enterprise/kioskmode/KioskMode;Landroid/app/enterprise/kioskmode/KioskSetting;Landroid/app/enterprise/kioskmode/IKioskMode;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public enableKioskMode(Ljava/lang/String;)V
    .locals 6
    .param p1, "kioskPackage"    # Ljava/lang/String;

    .prologue
    .line 320
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "KioskMode.enableKioskMode"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 321
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v2

    .line 322
    .local v2, "km":Landroid/app/enterprise/kioskmode/IKioskMode;
    if-eqz v2, :cond_0

    .line 324
    :try_start_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v4, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->enableKioskMode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 338
    :goto_0
    return-void

    .line 326
    :catch_0
    move-exception v0

    .line 327
    .local v0, "e":Landroid/os/RemoteException;
    const-string v4, "KioskMode"

    const-string v5, "Failed talking with kiosk mode service"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 332
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v4, v4, Landroid/app/enterprise/ContextInfo;->mCallerUid:I

    invoke-static {v4}, Landroid/os/UserHandle;->getUserId(I)I

    move-result v3

    .line 333
    .local v3, "user_id":I
    new-instance v1, Landroid/content/Intent;

    const-string v4, "edm.intent.action.enable.kiosk.mode.result"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 334
    .local v1, "intent":Landroid/content/Intent;
    const-string v4, "edm.intent.extra.kiosk.mode.result"

    const/16 v5, -0x7d0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 335
    const-string v4, "admin_uid"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 336
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getCallingPackage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 337
    iget-object v4, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContext:Landroid/content/Context;

    const-string v5, "android.permission.sec.MDM_KIOSK_MODE"

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAllBlockedHardwareKeys()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 902
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 904
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->getAllBlockedHardwareKeys(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 909
    :goto_0
    return-object v1

    .line 905
    :catch_0
    move-exception v0

    .line 906
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 909
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getHardwareKeyList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 729
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 731
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->getHardwareKeyList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 736
    :goto_0
    return-object v1

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 736
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method

.method public getKioskHomePackage()Ljava/lang/String;
    .locals 3

    .prologue
    .line 460
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 462
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->getKioskHomePackage(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 467
    :goto_0
    return-object v1

    .line 463
    :catch_0
    move-exception v0

    .line 464
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 467
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hideNavigationBar(Z)Z
    .locals 3
    .param p1, "hide"    # Z

    .prologue
    .line 1012
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.hideNavigationBar"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1013
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1015
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideNavigationBar(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1020
    :goto_0
    return v1

    .line 1016
    :catch_0
    move-exception v0

    .line 1017
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1020
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hideStatusBar(Z)Z
    .locals 3
    .param p1, "hide"    # Z

    .prologue
    .line 944
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.hideStatusBar"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 945
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 947
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideStatusBar(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 952
    :goto_0
    return v1

    .line 948
    :catch_0
    move-exception v0

    .line 949
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 952
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hideSystemBar(Z)Z
    .locals 3
    .param p1, "hide"    # Z

    .prologue
    .line 524
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.hideSystemBar"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 525
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 527
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->hideSystemBar(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 532
    :goto_0
    return v1

    .line 528
    :catch_0
    move-exception v0

    .line 529
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 532
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isAirCommandModeAllowed()Z
    .locals 3

    .prologue
    .line 1725
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.isAirCommandModeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1726
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1728
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isAirCommandModeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1733
    :goto_0
    return v1

    .line 1729
    :catch_0
    move-exception v0

    .line 1730
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with Kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1733
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAirViewModeAllowed()Z
    .locals 3

    .prologue
    .line 1763
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.isAirViewModeAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1764
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1766
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isAirViewModeAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1771
    :goto_0
    return v1

    .line 1767
    :catch_0
    move-exception v0

    .line 1768
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with Kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1771
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isHardwareKeyAllowed(I)Z
    .locals 4
    .param p1, "hwKeyId"    # I

    .prologue
    .line 848
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 850
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/kioskmode/IKioskMode;->isHardwareKeyAllowed(Landroid/app/enterprise/ContextInfo;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 855
    :goto_0
    return v1

    .line 851
    :catch_0
    move-exception v0

    .line 852
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 855
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isHardwareKeyAllowed(IZ)Z
    .locals 3
    .param p1, "hwKeyId"    # I
    .param p2, "showMsg"    # Z

    .prologue
    .line 862
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 864
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isHardwareKeyAllowed(Landroid/app/enterprise/ContextInfo;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 869
    :goto_0
    return v1

    .line 865
    :catch_0
    move-exception v0

    .line 866
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 869
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isKioskModeEnabled()Z
    .locals 3

    .prologue
    .line 434
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 436
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isKioskModeEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 441
    :goto_0
    return v1

    .line 437
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 441
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isMultiWindowModeAllowed()Z
    .locals 4

    .prologue
    .line 1137
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1139
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/kioskmode/IKioskMode;->isMultiWindowModeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1144
    :goto_0
    return v1

    .line 1140
    :catch_0
    move-exception v0

    .line 1141
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1144
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isMultiWindowModeAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1151
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1153
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->isMultiWindowModeAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1158
    :goto_0
    return v1

    .line 1154
    :catch_0
    move-exception v0

    .line 1155
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1158
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isNavigationBarHidden()Z
    .locals 3

    .prologue
    .line 1040
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1042
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isNavigationBarHidden(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1047
    :goto_0
    return v1

    .line 1043
    :catch_0
    move-exception v0

    .line 1044
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1047
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSmartClipModeAllowed()Z
    .locals 1

    .prologue
    .line 1702
    const/4 v0, 0x1

    return v0
.end method

.method public isStatusBarHidden()Z
    .locals 3

    .prologue
    .line 972
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 974
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isStatusBarHidden(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 979
    :goto_0
    return v1

    .line 975
    :catch_0
    move-exception v0

    .line 976
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 979
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSystemBarHidden()Z
    .locals 3

    .prologue
    .line 551
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 553
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->isSystemBarHidden(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 558
    :goto_0
    return v1

    .line 554
    :catch_0
    move-exception v0

    .line 555
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 558
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isTaskManagerAllowed()Z
    .locals 4

    .prologue
    .line 687
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 689
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/kioskmode/IKioskMode;->isTaskManagerAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 694
    :goto_0
    return v1

    .line 690
    :catch_0
    move-exception v0

    .line 691
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 694
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isTaskManagerAllowed(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 701
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 703
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/kioskmode/IKioskMode;->isTaskManagerAllowed(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 708
    :goto_0
    return v1

    .line 704
    :catch_0
    move-exception v0

    .line 705
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 708
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public wipeRecentTasks()Z
    .locals 3

    .prologue
    .line 587
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "KioskMode.wipeRecentTasks"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 588
    invoke-direct {p0}, Landroid/app/enterprise/kioskmode/KioskMode;->getService()Landroid/app/enterprise/kioskmode/IKioskMode;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 590
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mKioskService:Landroid/app/enterprise/kioskmode/IKioskMode;

    iget-object v2, p0, Landroid/app/enterprise/kioskmode/KioskMode;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/kioskmode/IKioskMode;->wipeRecentTasks(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 595
    :goto_0
    return v1

    .line 591
    :catch_0
    move-exception v0

    .line 592
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KioskMode"

    const-string v2, "Failed talking with kiosk mode service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 595
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
