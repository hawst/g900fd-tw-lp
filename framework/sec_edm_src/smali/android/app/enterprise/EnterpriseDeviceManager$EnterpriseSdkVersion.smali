.class public final enum Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;
.super Ljava/lang/Enum;
.source "EnterpriseDeviceManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/EnterpriseDeviceManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnterpriseSdkVersion"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_2_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_2_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_4:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_4_0_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_4_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_5:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_5_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_5_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_5_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

.field public static final enum ENTERPRISE_SDK_VERSION_NONE:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2103
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_2"

    invoke-direct {v0, v1, v3}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2105
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_2_1"

    invoke-direct {v0, v1, v4}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2107
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_2_2"

    invoke-direct {v0, v1, v5}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2109
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_3"

    invoke-direct {v0, v1, v6}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2111
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_4"

    invoke-direct {v0, v1, v7}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2113
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_4_0_1"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4_0_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2115
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_4_1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2117
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_5"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2119
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_5_1"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2121
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_5_2"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2123
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_5_3"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2125
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    const-string v1, "ENTERPRISE_SDK_VERSION_NONE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_NONE:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    .line 2102
    const/16 v0, 0xc

    new-array v0, v0, [Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v1, v0, v3

    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v1, v0, v4

    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_2_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v1, v0, v5

    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v1, v0, v6

    sget-object v1, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4_0_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_4_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_1:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_2:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_5_3:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ENTERPRISE_SDK_VERSION_NONE:Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    aput-object v2, v0, v1

    sput-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->$VALUES:[Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 2102
    const-class v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    return-object v0
.end method

.method public static values()[Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;
    .locals 1

    .prologue
    .line 2102
    sget-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->$VALUES:[Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    invoke-virtual {v0}, [Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;

    return-object v0
.end method


# virtual methods
.method public getInternalVersion()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2128
    sget-object v0, Landroid/app/enterprise/EnterpriseDeviceManager$1;->$SwitchMap$android$app$enterprise$EnterpriseDeviceManager$EnterpriseSdkVersion:[I

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceManager$EnterpriseSdkVersion;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2152
    const-string v0, "N/A"

    :goto_0
    return-object v0

    .line 2130
    :pswitch_0
    const-string v0, "2.0.0"

    goto :goto_0

    .line 2132
    :pswitch_1
    const-string v0, "2.1.0"

    goto :goto_0

    .line 2134
    :pswitch_2
    const-string v0, "2.2.0"

    goto :goto_0

    .line 2136
    :pswitch_3
    const-string v0, "3.0.0"

    goto :goto_0

    .line 2138
    :pswitch_4
    const-string v0, "4.0.0"

    goto :goto_0

    .line 2140
    :pswitch_5
    const-string v0, "4.0.1"

    goto :goto_0

    .line 2142
    :pswitch_6
    const-string v0, "4.1.0"

    goto :goto_0

    .line 2144
    :pswitch_7
    const-string v0, "5.0.0"

    goto :goto_0

    .line 2146
    :pswitch_8
    const-string v0, "5.1.0"

    goto :goto_0

    .line 2148
    :pswitch_9
    const-string v0, "5.2.0"

    goto :goto_0

    .line 2150
    :pswitch_a
    const-string v0, "5.3.0"

    goto :goto_0

    .line 2128
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
