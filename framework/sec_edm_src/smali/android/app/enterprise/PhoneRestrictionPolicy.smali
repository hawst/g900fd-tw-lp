.class public Landroid/app/enterprise/PhoneRestrictionPolicy;
.super Ljava/lang/Object;
.source "PhoneRestrictionPolicy.java"


# static fields
.field public static final EXTRA_SIM_PIN:Ljava/lang/String; = "verify_sim_pin"

.field public static final ICCID_AVAILABLE:Ljava/lang/String; = "com.android.server.enterprise.ICCID_AVAILABLE"

.field public static final LIMIT_NUMBER_OF_CALLS_BY_DAY:I = 0x0

.field public static final LIMIT_NUMBER_OF_CALLS_BY_MONTH:I = 0x2

.field public static final LIMIT_NUMBER_OF_CALLS_BY_WEEK:I = 0x1

.field public static final LIMIT_NUMBER_OF_DATA_CALLS_BY_DAY:I = 0x0

.field public static final LIMIT_NUMBER_OF_DATA_CALLS_BY_MONTH:I = 0x2

.field public static final LIMIT_NUMBER_OF_DATA_CALLS_BY_WEEK:I = 0x1

.field public static final LIMIT_NUMBER_OF_SMS_BY_DAY:I = 0x0

.field public static final LIMIT_NUMBER_OF_SMS_BY_MONTH:I = 0x2

.field public static final LIMIT_NUMBER_OF_SMS_BY_WEEK:I = 0x1

.field public static final PERMISSION_NOTIFY_ICCID_AVAILABLE:Ljava/lang/String; = "android.permission.sec.MDM_NOTIFY_ICCID_AVAILABLE"

.field public static final SIM_PIN_ALREADY_LOCKED:I = 0x4

.field public static final SIM_PIN_ALREADY_LOCKED_BY_ADMIN:I = 0xb

.field public static final SIM_PIN_ALREADY_UNLOCKED:I = 0x5

.field public static final SIM_PIN_BLOCKED_BY_PUK:I = 0x6

.field public static final SIM_PIN_DATABASE_ERROR:I = 0xa

.field public static final SIM_PIN_ERROR_UNKNOWN:I = 0x64

.field public static final SIM_PIN_FAILED:I = 0x1

.field public static final SIM_PIN_ID_NOT_READY:I = 0x9

.field public static final SIM_PIN_INCORRECT_CODE:I = 0x3

.field public static final SIM_PIN_INVALID_CODE:I = 0x2

.field public static final SIM_PIN_MAX_RETRIES_EXCEEDED:I = 0x8

.field public static final SIM_PIN_OWNED_BY_OTHER_ADMIN:I = 0xc

.field public static final SIM_PIN_SUCCESS:I

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "PhoneRestrictionPolicy"

    sput-object v0, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 180
    return-void
.end method

.method private getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    if-nez v0, :cond_0

    .line 184
    const-string v0, "phone_restriction_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    .line 187
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    return-object v0
.end method


# virtual methods
.method public addIncomingCallExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2381
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addIncomingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2382
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2384
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2389
    :goto_0
    return v1

    .line 2385
    :catch_0
    move-exception v0

    .line 2386
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed addIncomingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2389
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addIncomingCallRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 409
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addIncomingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 410
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 412
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 417
    :goto_0
    return v1

    .line 413
    :catch_0
    move-exception v0

    .line 414
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 417
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addIncomingSmsExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2502
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addIncomingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2503
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2505
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2510
    :goto_0
    return v1

    .line 2506
    :catch_0
    move-exception v0

    .line 2507
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed addIncomingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2510
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addIncomingSmsRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 673
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addIncomingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 674
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 676
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 681
    :goto_0
    return v1

    .line 677
    :catch_0
    move-exception v0

    .line 678
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 681
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addNumberOfIncomingCalls()Z
    .locals 3

    .prologue
    .line 1000
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1002
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addNumberOfIncomingCalls()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1007
    :goto_0
    return v1

    .line 1003
    :catch_0
    move-exception v0

    .line 1004
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1007
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addNumberOfIncomingSms()Z
    .locals 3

    .prologue
    .line 1247
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1249
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addNumberOfIncomingSms()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1254
    :goto_0
    return v1

    .line 1250
    :catch_0
    move-exception v0

    .line 1251
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1254
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addNumberOfOutgoingCalls()Z
    .locals 3

    .prologue
    .line 1019
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1021
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addNumberOfOutgoingCalls()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1026
    :goto_0
    return v1

    .line 1022
    :catch_0
    move-exception v0

    .line 1023
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1026
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addNumberOfOutgoingSms()Z
    .locals 3

    .prologue
    .line 1266
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1268
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addNumberOfOutgoingSms()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1273
    :goto_0
    return v1

    .line 1269
    :catch_0
    move-exception v0

    .line 1270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1273
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addOutgoingCallExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2366
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addOutgoingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2367
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2369
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2374
    :goto_0
    return v1

    .line 2370
    :catch_0
    move-exception v0

    .line 2371
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed addOutgoingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2374
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addOutgoingCallRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 350
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addOutgoingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 351
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 353
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 358
    :goto_0
    return v1

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 358
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addOutgoingSmsExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2487
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addOutgoingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2488
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2490
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2495
    :goto_0
    return v1

    .line 2491
    :catch_0
    move-exception v0

    .line 2492
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed addOutgoingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2495
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addOutgoingSmsRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 644
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.addOutgoingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 645
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 647
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->addOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 652
    :goto_0
    return v1

    .line 648
    :catch_0
    move-exception v0

    .line 649
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 652
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowCallerIDDisplay(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2083
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowCallerIDDisplay"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2084
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2086
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowCallerIDDisplay(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2091
    :goto_0
    return v1

    .line 2087
    :catch_0
    move-exception v0

    .line 2088
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to block caller id display "

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2091
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowCopyContactToSim(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2206
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowCopyContactToSim"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2207
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2209
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowCopyContactToSim(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2214
    :goto_0
    return v1

    .line 2210
    :catch_0
    move-exception v0

    .line 2211
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2214
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowIncomingMms(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1731
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowIncomingMms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1732
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1734
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowIncomingMms(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1739
    :goto_0
    return v1

    .line 1735
    :catch_0
    move-exception v0

    .line 1736
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to set incoming MMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1739
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public allowIncomingSms(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1547
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowIncomingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1548
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1550
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowIncomingSms(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1555
    :goto_0
    return v1

    .line 1551
    :catch_0
    move-exception v0

    .line 1552
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to set incoming SMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public allowOutgoingMms(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1778
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowOutgoingMms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1779
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1781
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowOutgoingMms(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1786
    :goto_0
    return v1

    .line 1782
    :catch_0
    move-exception v0

    .line 1783
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to set outgoing MMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1786
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public allowOutgoingSms(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 1594
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowOutgoingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1595
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1597
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowOutgoingSms(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1602
    :goto_0
    return v1

    .line 1598
    :catch_0
    move-exception v0

    .line 1599
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to set outgoing SMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1602
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public allowWapPush(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2018
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.allowWapPush"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2019
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2021
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->allowWapPush(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2026
    :goto_0
    return v1

    .line 2022
    :catch_0
    move-exception v0

    .line 2023
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to allow wap push"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public blockMmsWithStorage(Z)Z
    .locals 3
    .param p1, "block"    # Z

    .prologue
    .line 1927
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.blockMmsWithStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1928
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1930
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->blockMmsWithStorage(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1935
    :goto_0
    return v1

    .line 1931
    :catch_0
    move-exception v0

    .line 1932
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to block MMS with storage"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1935
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public blockSmsWithStorage(Z)Z
    .locals 3
    .param p1, "block"    # Z

    .prologue
    .line 1890
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.blockSmsWithStorage"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1891
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1893
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->blockSmsWithStorage(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1898
    :goto_0
    return v1

    .line 1894
    :catch_0
    move-exception v0

    .line 1895
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to block SMS with storage"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1898
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canIncomingCall(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dialNumber"    # Ljava/lang/String;

    .prologue
    .line 462
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 464
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->canIncomingCall(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 469
    :goto_0
    return v1

    .line 465
    :catch_0
    move-exception v0

    .line 466
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 469
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canIncomingSms(Ljava/lang/String;)Z
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 772
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 774
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->canIncomingSms(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 779
    :goto_0
    return v1

    .line 775
    :catch_0
    move-exception v0

    .line 776
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 779
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canOutgoingCall(Ljava/lang/String;)Z
    .locals 3
    .param p1, "dialNumber"    # Ljava/lang/String;

    .prologue
    .line 436
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 438
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->canOutgoingCall(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 443
    :goto_0
    return v1

    .line 439
    :catch_0
    move-exception v0

    .line 440
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 443
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public canOutgoingSms(Ljava/lang/String;)Z
    .locals 3
    .param p1, "phoneNumber"    # Ljava/lang/String;

    .prologue
    .line 751
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 753
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->canOutgoingSms(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 758
    :goto_0
    return v1

    .line 754
    :catch_0
    move-exception v0

    .line 755
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 758
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public changeSimPinCode(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4
    .param p1, "currentPinCode"    # Ljava/lang/String;
    .param p2, "newPinCode"    # Ljava/lang/String;

    .prologue
    .line 2263
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.changeSimPinCode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2264
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2266
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->changeSimPinCode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2271
    :goto_0
    return v1

    .line 2267
    :catch_0
    move-exception v0

    .line 2268
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed changeSimPinCode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2271
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/16 v1, 0x64

    goto :goto_0
.end method

.method public checkDataCallLimit()Z
    .locals 3

    .prologue
    .line 1467
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1469
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->checkDataCallLimit()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1474
    :goto_0
    return v1

    .line 1470
    :catch_0
    move-exception v0

    .line 1471
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to checkDataCallLimit"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public checkEnableUseOfPacketData(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 1451
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1453
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->checkEnableUseOfPacketData(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1458
    :goto_0
    return v1

    .line 1454
    :catch_0
    move-exception v0

    .line 1455
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to checkEnableUseOfPacketData"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public clearStoredBlockedMms()Z
    .locals 3

    .prologue
    .line 1999
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.clearStoredBlockedMms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2000
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2002
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->clearStoredBlockedMms(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2007
    :goto_0
    return v1

    .line 2003
    :catch_0
    move-exception v0

    .line 2004
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to clear stored blocked mms"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2007
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearStoredBlockedSms()Z
    .locals 3

    .prologue
    .line 1981
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.clearStoredBlockedSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1982
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1984
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->clearStoredBlockedSms(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1989
    :goto_0
    return v1

    .line 1985
    :catch_0
    move-exception v0

    .line 1986
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to clear stored blocked sms"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1989
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public decreaseNumberOfOutgoingSms()Z
    .locals 3

    .prologue
    .line 1285
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1287
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->decreaseNumberOfOutgoingSms()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1292
    :goto_0
    return v1

    .line 1288
    :catch_0
    move-exception v0

    .line 1289
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1292
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableSimPinLock(Ljava/lang/String;)I
    .locals 4
    .param p1, "pinCode"    # Ljava/lang/String;

    .prologue
    .line 2162
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.disableSimPinLock"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2163
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2165
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->lockUnlockCorporateSimCard(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2170
    :goto_0
    return v1

    .line 2166
    :catch_0
    move-exception v0

    .line 2167
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed disableSimPinLock"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2170
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/16 v1, 0x64

    goto :goto_0
.end method

.method public enableLimitNumberOfCalls(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 801
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.enableLimitNumberOfCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 802
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 804
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->enableLimitNumberOfCalls(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 809
    :goto_0
    return v1

    .line 805
    :catch_0
    move-exception v0

    .line 806
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 809
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableLimitNumberOfSms(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1048
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.enableLimitNumberOfSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1049
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1051
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->enableLimitNumberOfSms(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1056
    :goto_0
    return v1

    .line 1052
    :catch_0
    move-exception v0

    .line 1053
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1056
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableSimPinLock(Ljava/lang/String;)I
    .locals 4
    .param p1, "pinCode"    # Ljava/lang/String;

    .prologue
    .line 2143
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.enableSimPinLock"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2144
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2146
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x1

    invoke-interface {v1, v2, p1, v3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->lockUnlockCorporateSimCard(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2151
    :goto_0
    return v1

    .line 2147
    :catch_0
    move-exception v0

    .line 2148
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed enableSimPinLock"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2151
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/16 v1, 0x64

    goto :goto_0
.end method

.method public getDataCallLimitEnabled()Z
    .locals 3

    .prologue
    .line 1340
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1342
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getDataCallLimitEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1347
    :goto_0
    return v1

    .line 1343
    :catch_0
    move-exception v0

    .line 1344
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to getDataCallLimitEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1347
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getEmergencyCallOnly(Z)Z
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 515
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getEmergencyCallOnly"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 516
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 518
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getEmergencyCallOnly(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 523
    :goto_0
    return v1

    .line 519
    :catch_0
    move-exception v0

    .line 520
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 523
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIncomingCallExceptionPatterns()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2321
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getIncomingCallExceptionPatterns"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2322
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2324
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getIncomingCallExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2329
    :goto_0
    return-object v1

    .line 2325
    :catch_0
    move-exception v0

    .line 2326
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed getIncomingCallExceptionPatterns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2329
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIncomingCallRestriction(Z)Ljava/lang/String;
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 237
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getIncomingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 238
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 240
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 245
    :goto_0
    return-object v1

    .line 241
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 245
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIncomingSmsExceptionPatterns()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2442
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getIncomingSmsExceptionPatterns"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2443
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2445
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getIncomingSmsExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2450
    :goto_0
    return-object v1

    .line 2446
    :catch_0
    move-exception v0

    .line 2447
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed getIncomingSmsExceptionPatterns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2450
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIncomingSmsRestriction(Z)Ljava/lang/String;
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 567
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getIncomingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 568
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 570
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 575
    :goto_0
    return-object v1

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLimitOfDataCalls(I)J
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 1404
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getLimitOfDataCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1405
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1407
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getLimitOfDataCalls(Landroid/app/enterprise/ContextInfo;I)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1412
    :goto_0
    return-wide v2

    .line 1408
    :catch_0
    move-exception v0

    .line 1409
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to getLimitOfDataCalls"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1412
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getLimitOfIncomingCalls(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 890
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getLimitOfIncomingCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 891
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 893
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getLimitOfIncomingCalls(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 898
    :goto_0
    return v1

    .line 894
    :catch_0
    move-exception v0

    .line 895
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 898
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getLimitOfIncomingSms(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 1164
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getLimitOfIncomingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1165
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1167
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getLimitOfIncomingSms(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1172
    :goto_0
    return v1

    .line 1168
    :catch_0
    move-exception v0

    .line 1169
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1172
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getLimitOfOutgoingCalls(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 953
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getLimitOfOutgoingCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 954
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 956
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getLimitOfOutgoingCalls(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 961
    :goto_0
    return v1

    .line 957
    :catch_0
    move-exception v0

    .line 958
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 961
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getLimitOfOutgoingSms(I)I
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 1227
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getLimitOfOutgoingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1228
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1230
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getLimitOfOutgoingSms(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1235
    :goto_0
    return v1

    .line 1231
    :catch_0
    move-exception v0

    .line 1232
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1235
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getOutgoingCallExceptionPatterns()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2306
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getOutgoingCallExceptionPatterns"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2307
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2309
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getOutgoingCallExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2314
    :goto_0
    return-object v1

    .line 2310
    :catch_0
    move-exception v0

    .line 2311
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed getOutgoingCallExceptionPatterns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2314
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOutgoingCallRestriction(Z)Ljava/lang/String;
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 208
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getOutgoingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 209
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 211
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 216
    :goto_0
    return-object v1

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 216
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOutgoingSmsExceptionPatterns()Ljava/lang/String;
    .locals 4

    .prologue
    .line 2427
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getOutgoingSmsExceptionPatterns"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2428
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2430
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getOutgoingSmsExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2435
    :goto_0
    return-object v1

    .line 2431
    :catch_0
    move-exception v0

    .line 2432
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed getOutgoingSmsExceptionPatterns"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2435
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOutgoingSmsRestriction(Z)Ljava/lang/String;
    .locals 3
    .param p1, "allAdmins"    # Z

    .prologue
    .line 541
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.getOutgoingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 542
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 544
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 549
    :goto_0
    return-object v1

    .line 545
    :catch_0
    move-exception v0

    .line 546
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 549
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPinCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "iccid"    # Ljava/lang/String;

    .prologue
    .line 2289
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2291
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->getPinCode(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2296
    :goto_0
    return-object v1

    .line 2292
    :catch_0
    move-exception v0

    .line 2293
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed getPinCode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2296
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public isBlockMmsWithStorageEnabled()Z
    .locals 3

    .prologue
    .line 1946
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1948
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isBlockMmsWithStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1953
    :goto_0
    return v1

    .line 1949
    :catch_0
    move-exception v0

    .line 1950
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get status of block MMS with storage"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1953
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBlockSmsWithStorageEnabled()Z
    .locals 3

    .prologue
    .line 1909
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1911
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isBlockSmsWithStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1916
    :goto_0
    return v1

    .line 1912
    :catch_0
    move-exception v0

    .line 1913
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get status of block SMS with storage"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1916
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCallerIDDisplayAllowed()Z
    .locals 3

    .prologue
    .line 2125
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2127
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isCallerIDDisplayAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2132
    :goto_0
    return v1

    .line 2128
    :catch_0
    move-exception v0

    .line 2129
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getting caller id display status"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2132
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isCopyContactToSimAllowed()Z
    .locals 3

    .prologue
    .line 2247
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2249
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isCopyContactToSimAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2254
    :goto_0
    return v1

    .line 2250
    :catch_0
    move-exception v0

    .line 2251
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with security policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2254
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isIncomingMmsAllowed()Z
    .locals 3

    .prologue
    .line 1824
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1826
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isIncomingMmsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1831
    :goto_0
    return v1

    .line 1827
    :catch_0
    move-exception v0

    .line 1828
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get incoming MMS status"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1831
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isIncomingSmsAllowed()Z
    .locals 3

    .prologue
    .line 1639
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.isIncomingSmsAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1640
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1642
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isIncomingSmsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1647
    :goto_0
    return v1

    .line 1643
    :catch_0
    move-exception v0

    .line 1644
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get incoming SMS status"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1647
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isLimitNumberOfCallsEnabled()Z
    .locals 3

    .prologue
    .line 827
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.isLimitNumberOfCallsEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 828
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 830
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isLimitNumberOfCallsEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 835
    :goto_0
    return v1

    .line 831
    :catch_0
    move-exception v0

    .line 832
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 835
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isLimitNumberOfSmsEnabled()Z
    .locals 3

    .prologue
    .line 1074
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.isLimitNumberOfSmsEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1075
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1077
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isLimitNumberOfSmsEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1082
    :goto_0
    return v1

    .line 1078
    :catch_0
    move-exception v0

    .line 1079
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1082
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isOutgoingMmsAllowed()Z
    .locals 3

    .prologue
    .line 1869
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1871
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isOutgoingMmsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1876
    :goto_0
    return v1

    .line 1872
    :catch_0
    move-exception v0

    .line 1873
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get outgoing MMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1876
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isOutgoingSmsAllowed()Z
    .locals 3

    .prologue
    .line 1684
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.isOutgoingSmsAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1685
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1687
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isOutgoingSmsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1692
    :goto_0
    return v1

    .line 1688
    :catch_0
    move-exception v0

    .line 1689
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get outgoing SMS"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1692
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isSimLockedByAdmin(Ljava/lang/String;)Z
    .locals 4
    .param p1, "iccId"    # Ljava/lang/String;

    .prologue
    .line 2276
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2278
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isSimLockedByAdmin(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2283
    :goto_0
    return v1

    .line 2279
    :catch_0
    move-exception v0

    .line 2280
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed isSimLockedByAdmin"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2283
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWapPushAllowed()Z
    .locals 3

    .prologue
    .line 2037
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2039
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->isWapPushAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2044
    :goto_0
    return v1

    .line 2040
    :catch_0
    move-exception v0

    .line 2041
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to get status of allow wap push"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2044
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public removeIncomingCallExceptionPattern()Z
    .locals 4

    .prologue
    .line 2351
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeIncomingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2352
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2354
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2359
    :goto_0
    return v1

    .line 2355
    :catch_0
    move-exception v0

    .line 2356
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed removeIncomingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2359
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeIncomingCallRestriction()Z
    .locals 3

    .prologue
    .line 291
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeIncomingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 292
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 294
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 299
    :goto_0
    return v1

    .line 295
    :catch_0
    move-exception v0

    .line 296
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 299
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeIncomingSmsExceptionPattern()Z
    .locals 4

    .prologue
    .line 2472
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeIncomingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2473
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2475
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2480
    :goto_0
    return v1

    .line 2476
    :catch_0
    move-exception v0

    .line 2477
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed removeIncomingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2480
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeIncomingSmsRestriction()Z
    .locals 3

    .prologue
    .line 615
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeIncomingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 616
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 618
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 623
    :goto_0
    return v1

    .line 619
    :catch_0
    move-exception v0

    .line 620
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 623
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeOutgoingCallExceptionPattern()Z
    .locals 4

    .prologue
    .line 2336
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeOutgoingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2337
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2339
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2344
    :goto_0
    return v1

    .line 2340
    :catch_0
    move-exception v0

    .line 2341
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed removeOutgoingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2344
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeOutgoingCallRestriction()Z
    .locals 3

    .prologue
    .line 264
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeOutgoingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 265
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 272
    :goto_0
    return v1

    .line 268
    :catch_0
    move-exception v0

    .line 269
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 272
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeOutgoingSmsExceptionPattern()Z
    .locals 4

    .prologue
    .line 2457
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeOutgoingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2458
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2460
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2465
    :goto_0
    return v1

    .line 2461
    :catch_0
    move-exception v0

    .line 2462
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed removeOutgoingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2465
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeOutgoingSmsRestriction()Z
    .locals 3

    .prologue
    .line 591
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.removeOutgoingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 592
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 594
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->removeOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 599
    :goto_0
    return v1

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 599
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetCallsCount()Z
    .locals 3

    .prologue
    .line 980
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.resetCallsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 981
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 983
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->resetCallsCount(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 988
    :goto_0
    return v1

    .line 984
    :catch_0
    move-exception v0

    .line 985
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 988
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetDataCallLimitCounter()Z
    .locals 3

    .prologue
    .line 1428
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.resetDataCallLimitCounter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1429
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1431
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->resetDataCallLimitCounter(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1436
    :goto_0
    return v1

    .line 1432
    :catch_0
    move-exception v0

    .line 1433
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to resetDataCallLimitCounter"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1436
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetSmsCount()Z
    .locals 3

    .prologue
    .line 1101
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.resetSmsCount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1102
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1104
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->resetSmsCount(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1109
    :goto_0
    return v1

    .line 1105
    :catch_0
    move-exception v0

    .line 1106
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1109
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDataCallLimitEnabled(Z)Z
    .locals 3
    .param p1, "status"    # Z

    .prologue
    .line 1315
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setDataCallLimitEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1316
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1318
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setDataCallLimitEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1323
    :goto_0
    return v1

    .line 1319
    :catch_0
    move-exception v0

    .line 1320
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to setDataCallLimitEnabled"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1323
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEmergencyCallOnly(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 489
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setEmergencyCallOnly"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 490
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 492
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setEmergencyCallOnly(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 497
    :goto_0
    return v1

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 497
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIncomingCallExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2411
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setIncomingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2412
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2414
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2419
    :goto_0
    return v1

    .line 2415
    :catch_0
    move-exception v0

    .line 2416
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed setIncomingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2419
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIncomingCallRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 379
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setIncomingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 380
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 382
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 387
    :goto_0
    return v1

    .line 383
    :catch_0
    move-exception v0

    .line 384
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 387
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIncomingSmsExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2532
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setIncomingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2533
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2535
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2540
    :goto_0
    return v1

    .line 2536
    :catch_0
    move-exception v0

    .line 2537
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed setIncomingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2540
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIncomingSmsRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 729
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setIncomingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 730
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 732
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 737
    :goto_0
    return v1

    .line 733
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 737
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLimitOfDataCalls(JJJ)Z
    .locals 9
    .param p1, "limitByDay"    # J
    .param p3, "limitByWeek"    # J
    .param p5, "limitByMonth"    # J

    .prologue
    .line 1373
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PhoneRestrictionPolicy.setLimitOfDataCalls"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1374
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1376
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-interface/range {v0 .. v7}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setLimitOfDataCalls(Landroid/app/enterprise/ContextInfo;JJJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1382
    :goto_0
    return v0

    .line 1378
    :catch_0
    move-exception v8

    .line 1379
    .local v8, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed to setLimitOfDataCalls"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1382
    .end local v8    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLimitOfIncomingCalls(III)Z
    .locals 3
    .param p1, "limitByDay"    # I
    .param p2, "limitByWeek"    # I
    .param p3, "limitByMonth"    # I

    .prologue
    .line 861
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setLimitOfIncomingCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 862
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 864
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setLimitOfIncomingCalls(Landroid/app/enterprise/ContextInfo;III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 870
    :goto_0
    return v1

    .line 866
    :catch_0
    move-exception v0

    .line 867
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 870
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLimitOfIncomingSms(III)Z
    .locals 3
    .param p1, "limitByDay"    # I
    .param p2, "limitByWeek"    # I
    .param p3, "limitByMonth"    # I

    .prologue
    .line 1135
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setLimitOfIncomingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1136
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1138
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setLimitOfIncomingSms(Landroid/app/enterprise/ContextInfo;III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1144
    :goto_0
    return v1

    .line 1140
    :catch_0
    move-exception v0

    .line 1141
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1144
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLimitOfOutgoingCalls(III)Z
    .locals 3
    .param p1, "limitByDay"    # I
    .param p2, "limitByWeek"    # I
    .param p3, "limitByMonth"    # I

    .prologue
    .line 924
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setLimitOfOutgoingCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 925
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 927
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setLimitOfOutgoingCalls(Landroid/app/enterprise/ContextInfo;III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 933
    :goto_0
    return v1

    .line 929
    :catch_0
    move-exception v0

    .line 930
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 933
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLimitOfOutgoingSms(III)Z
    .locals 3
    .param p1, "limitByDay"    # I
    .param p2, "limitByWeek"    # I
    .param p3, "limitByMonth"    # I

    .prologue
    .line 1198
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setLimitOfOutgoingSms"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1199
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1201
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setLimitOfOutgoingSms(Landroid/app/enterprise/ContextInfo;III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1207
    :goto_0
    return v1

    .line 1203
    :catch_0
    move-exception v0

    .line 1204
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1207
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutgoingCallExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2396
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setOutgoingCallExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2397
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2399
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2404
    :goto_0
    return v1

    .line 2400
    :catch_0
    move-exception v0

    .line 2401
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed setOutgoingCallExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2404
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutgoingCallRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 320
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setOutgoingCallRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 321
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 323
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 328
    :goto_0
    return v1

    .line 324
    :catch_0
    move-exception v0

    .line 325
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 328
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutgoingSmsExceptionPattern(Ljava/lang/String;)Z
    .locals 4
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 2517
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setOutgoingSmsExceptionPattern"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2518
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2520
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2525
    :goto_0
    return v1

    .line 2521
    :catch_0
    move-exception v0

    .line 2522
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed setOutgoingSmsExceptionPattern"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2525
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutgoingSmsRestriction(Ljava/lang/String;)Z
    .locals 3
    .param p1, "pattern"    # Ljava/lang/String;

    .prologue
    .line 701
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "PhoneRestrictionPolicy.setOutgoingSmsRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 702
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 704
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    iget-object v2, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->setOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 709
    :goto_0
    return v1

    .line 705
    :catch_0
    move-exception v0

    .line 706
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 709
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public storeBlockedSmsMms(Z[BLjava/lang/String;ILjava/lang/String;)V
    .locals 7
    .param p1, "isSms"    # Z
    .param p2, "pdu"    # [B
    .param p3, "srcAddress"    # Ljava/lang/String;
    .param p4, "sendType"    # I
    .param p5, "timeStamp"    # Ljava/lang/String;

    .prologue
    .line 1963
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "PhoneRestrictionPolicy.storeBlockedSmsMms"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1964
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1966
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->storeBlockedSmsMms(Z[BLjava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1972
    :cond_0
    :goto_0
    return-void

    .line 1968
    :catch_0
    move-exception v6

    .line 1969
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed to store blocked sms/mms"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public updateDataLimitState()V
    .locals 3

    .prologue
    .line 1499
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1501
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->updateDataLimitState()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1506
    :cond_0
    :goto_0
    return-void

    .line 1502
    :catch_0
    move-exception v0

    .line 1503
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public updateDateAndDataCallCounters(J)V
    .locals 3
    .param p1, "totalBytes"    # J

    .prologue
    .line 1482
    invoke-direct {p0}, Landroid/app/enterprise/PhoneRestrictionPolicy;->getService()Landroid/app/enterprise/IPhoneRestrictionPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1484
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/PhoneRestrictionPolicy;->mService:Landroid/app/enterprise/IPhoneRestrictionPolicy;

    invoke-interface {v1, p1, p2}, Landroid/app/enterprise/IPhoneRestrictionPolicy;->updateDateAndDataCallCounters(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1489
    :cond_0
    :goto_0
    return-void

    .line 1485
    :catch_0
    move-exception v0

    .line 1486
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/PhoneRestrictionPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with phone restriction policy"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
