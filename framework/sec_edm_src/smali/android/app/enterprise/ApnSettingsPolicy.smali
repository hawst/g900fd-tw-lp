.class public Landroid/app/enterprise/ApnSettingsPolicy;
.super Ljava/lang/Object;
.source "ApnSettingsPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private lService:Landroid/app/enterprise/IApnSettingsPolicy;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-string v0, "ApnSettingsPolicy"

    sput-object v0, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 58
    return-void
.end method

.method private getService()Landroid/app/enterprise/IApnSettingsPolicy;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    if-nez v0, :cond_0

    .line 63
    const-string v0, "apn_settings_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApnSettingsPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    .line 66
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    return-object v0
.end method


# virtual methods
.method public createApnSettings(Landroid/app/enterprise/ApnSettings;)J
    .locals 6
    .param p1, "apn"    # Landroid/app/enterprise/ApnSettings;

    .prologue
    .line 88
    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ApnSettingsPolicy.createApnSettings"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 89
    const-wide/16 v0, -0x1

    .line 91
    .local v0, "apnId":J
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 92
    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v4, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5, p1}, Landroid/app/enterprise/IApnSettingsPolicy;->addUpdateApn(Landroid/app/enterprise/ContextInfo;ZLandroid/app/enterprise/ApnSettings;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 98
    :cond_0
    :goto_0
    sget-object v3, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createApnSettings: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    return-wide v0

    .line 94
    :catch_0
    move-exception v2

    .line 95
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v3, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at update APN Settings policy "

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public deleteApn(J)Z
    .locals 5
    .param p1, "apnId"    # J

    .prologue
    .line 223
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApnSettingsPolicy.deleteApn"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 224
    const/4 v1, 0x0

    .line 226
    .local v1, "status":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IApnSettingsPolicy;->deleteApn(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 232
    :cond_0
    :goto_0
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteApn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 233
    return v1

    .line 229
    :catch_0
    move-exception v0

    .line 230
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at APN Settings policy API deleteApn()"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getApnList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/ApnSettings;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApnSettingsPolicy.getApnList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 253
    const/4 v1, 0x0

    .line 255
    .local v1, "listApn":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/ApnSettings;>;"
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 256
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IApnSettingsPolicy;->getApnList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 261
    :cond_0
    :goto_0
    return-object v1

    .line 258
    :catch_0
    move-exception v0

    .line 259
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at APN Settings policy API getApnList()"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getApnSettings(J)Landroid/app/enterprise/ApnSettings;
    .locals 5
    .param p1, "apnId"    # J

    .prologue
    .line 278
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApnSettingsPolicy.getApnSettings"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 279
    const/4 v0, 0x0

    .line 281
    .local v0, "apn":Landroid/app/enterprise/ApnSettings;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 282
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IApnSettingsPolicy;->getApnSettings(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/ApnSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 287
    :cond_0
    :goto_0
    return-object v0

    .line 284
    :catch_0
    move-exception v1

    .line 285
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at APN Settings policy API getApnSettings()"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getPreferredApnSettings()Landroid/app/enterprise/ApnSettings;
    .locals 4

    .prologue
    .line 305
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApnSettingsPolicy.getPreferredApnSettings"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 306
    const/4 v0, 0x0

    .line 308
    .local v0, "apn":Landroid/app/enterprise/ApnSettings;
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 309
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IApnSettingsPolicy;->getPreferredApn(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/ApnSettings;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 314
    :cond_0
    :goto_0
    return-object v0

    .line 311
    :catch_0
    move-exception v1

    .line 312
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at APN Settings policy API getPreferredApnSettings()"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public saveApnSettings(Landroid/app/enterprise/ApnSettings;)Z
    .locals 1
    .param p1, "apn"    # Landroid/app/enterprise/ApnSettings;

    .prologue
    .line 169
    invoke-virtual {p0, p1}, Landroid/app/enterprise/ApnSettingsPolicy;->updateApnSettings(Landroid/app/enterprise/ApnSettings;)Z

    move-result v0

    return v0
.end method

.method public setPreferredApn(J)Z
    .locals 5
    .param p1, "apnId"    # J

    .prologue
    .line 191
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "ApnSettingsPolicy.setPreferredApn"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 192
    const/4 v1, 0x0

    .line 194
    .local v1, "status":Z
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 195
    iget-object v2, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v3, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IApnSettingsPolicy;->setPreferredApn(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 201
    :cond_0
    :goto_0
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPreferredApn: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    return v1

    .line 197
    :catch_0
    move-exception v0

    .line 198
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at APN Settings policy API setPreferredApn()"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public updateApnSettings(Landroid/app/enterprise/ApnSettings;)Z
    .locals 9
    .param p1, "apn"    # Landroid/app/enterprise/ApnSettings;

    .prologue
    const-wide/16 v4, -0x1

    .line 123
    iget-object v6, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ApnSettingsPolicy.updateApnSettings"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 124
    const/4 v3, 0x0

    .line 125
    .local v3, "status":Z
    if-eqz p1, :cond_2

    iget-wide v0, p1, Landroid/app/enterprise/ApnSettings;->id:J

    .line 127
    .local v0, "apnId":J
    :goto_0
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/ApnSettingsPolicy;->getService()Landroid/app/enterprise/IApnSettingsPolicy;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 128
    iget-object v6, p0, Landroid/app/enterprise/ApnSettingsPolicy;->lService:Landroid/app/enterprise/IApnSettingsPolicy;

    iget-object v7, p0, Landroid/app/enterprise/ApnSettingsPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8, p1}, Landroid/app/enterprise/IApnSettingsPolicy;->addUpdateApn(Landroid/app/enterprise/ContextInfo;ZLandroid/app/enterprise/ApnSettings;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 134
    :cond_0
    :goto_1
    cmp-long v4, v0, v4

    if-eqz v4, :cond_1

    .line 135
    const/4 v3, 0x1

    .line 137
    :cond_1
    sget-object v4, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateApnSettings: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    return v3

    .end local v0    # "apnId":J
    :cond_2
    move-wide v0, v4

    .line 125
    goto :goto_0

    .line 130
    .restart local v0    # "apnId":J
    :catch_0
    move-exception v2

    .line 131
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v6, Landroid/app/enterprise/ApnSettingsPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at update APN Settings policy "

    invoke-static {v6, v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
