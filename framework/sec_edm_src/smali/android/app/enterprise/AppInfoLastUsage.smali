.class public Landroid/app/enterprise/AppInfoLastUsage;
.super Landroid/app/enterprise/AppInfo;
.source "AppInfoLastUsage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/AppInfoLastUsage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mLastAppUsage:J

.field public mLastLaunchTime:J

.field public mLaunchCountPerMonth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 107
    new-instance v0, Landroid/app/enterprise/AppInfoLastUsage$1;

    invoke-direct {v0}, Landroid/app/enterprise/AppInfoLastUsage$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/AppInfoLastUsage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 138
    invoke-direct {p0}, Landroid/app/enterprise/AppInfo;-><init>()V

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLaunchCountPerMonth:I

    .line 96
    iput-wide v2, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastAppUsage:J

    .line 102
    iput-wide v2, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastLaunchTime:J

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const-wide/16 v2, -0x1

    .line 153
    invoke-direct {p0}, Landroid/app/enterprise/AppInfo;-><init>()V

    .line 90
    const/4 v0, -0x1

    iput v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLaunchCountPerMonth:I

    .line 96
    iput-wide v2, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastAppUsage:J

    .line 102
    iput-wide v2, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastLaunchTime:J

    .line 155
    invoke-virtual {p0, p1}, Landroid/app/enterprise/AppInfoLastUsage;->readFromParcel(Landroid/os/Parcel;)V

    .line 157
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 240
    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 221
    invoke-super {p0, p1}, Landroid/app/enterprise/AppInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLaunchCountPerMonth:I

    .line 225
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastAppUsage:J

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastLaunchTime:J

    .line 229
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 183
    invoke-super {p0, p1, p2}, Landroid/app/enterprise/AppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 185
    iget v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLaunchCountPerMonth:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 187
    iget-wide v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastAppUsage:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 189
    iget-wide v0, p0, Landroid/app/enterprise/AppInfoLastUsage;->mLastLaunchTime:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 193
    return-void
.end method
