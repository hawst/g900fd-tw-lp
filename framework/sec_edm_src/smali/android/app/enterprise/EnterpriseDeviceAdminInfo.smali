.class public final Landroid/app/enterprise/EnterpriseDeviceAdminInfo;
.super Ljava/lang/Object;
.source "EnterpriseDeviceAdminInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/EnterpriseDeviceAdminInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final TAG:Ljava/lang/String; = "EnterpriseDeviceAdminInfo"

.field public static final USES_POLICY_ENTERPRISE_CONTAINER_TAG:Ljava/lang/String; = "android.permission.sec.ENTERPRISE_CONTAINER"

.field public static final USES_POLICY_ENTERPRISE_MOUNT_UNMOUNT_ENCRYPT_TAG:Ljava/lang/String; = "android.permission.sec.ENTERPRISE_MOUNT_UNMOUNT_ENCRYPT"

.field public static final USES_POLICY_KNOX_CCM:I = 0x4c

.field public static final USES_POLICY_KNOX_CCM_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_CCM"

.field public static final USES_POLICY_KNOX_CERTENROL:I = 0x51

.field public static final USES_POLICY_KNOX_CERTENROL_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_CERTENROLL"

.field public static final USES_POLICY_KNOX_CONTAINER_VPN:I = 0x45

.field public static final USES_POLICY_KNOX_CONTAINER_VPN_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.KNOX_CONTAINER_VPN"

.field public static final USES_POLICY_KNOX_CUSTOM_RUBENS_FEATURES:I = 0x54

.field public static final USES_POLICY_KNOX_CUSTOM_RUBENS_FEATURES_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.CUSTOM_RUBENS_FEATURES"

.field public static final USES_POLICY_KNOX_CUSTOM_SEALEDMODE:I = 0x50

.field public static final USES_POLICY_KNOX_CUSTOM_SEALEDMODE_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.CUSTOM_SEALEDMODE"

.field public static final USES_POLICY_KNOX_CUSTOM_SETTING:I = 0x4e

.field public static final USES_POLICY_KNOX_CUSTOM_SETTING_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.CUSTOM_SETTING"

.field public static final USES_POLICY_KNOX_CUSTOM_SYSTEM:I = 0x4f

.field public static final USES_POLICY_KNOX_CUSTOM_SYSTEM_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.CUSTOM_SYSTEM"

.field public static final USES_POLICY_KNOX_DEACTIVATE_LICENSE:I = 0x48

.field public static final USES_POLICY_KNOX_DEACTIVATE_LICENSE_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_DEACTIVATE_LICENSE"

.field public static final USES_POLICY_KNOX_GENERIC_VPN:I = 0x44

.field public static final USES_POLICY_KNOX_GENERIC_VPN_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.KNOX_GENERIC_VPN"

.field public static final USES_POLICY_KNOX_KEYSTORE:I = 0x4d

.field public static final USES_POLICY_KNOX_KEYSTORE_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_KEYSTORE"

.field public static final USES_POLICY_KNOX_RESTRICTION_PERM:I = 0x4b

.field public static final USES_POLICY_KNOX_RESTRICTION_PERM_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_RESTRICTION"

.field public static final USES_POLICY_KNOX_SEAMS_PERM:I = 0x49

.field public static final USES_POLICY_KNOX_SEAMS_PERM_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_SEAMS"

.field public static final USES_POLICY_KNOX_SEAMS_SEPOLICY_PERM:I = 0x4a

.field public static final USES_POLICY_KNOX_SEAMS_SEPOLICY_PERM_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_SEAMS_SEPOLICY"

.field public static final USES_POLICY_KNOX_TRUSTED_PINPAD:I = 0x53

.field public static final USES_POLICY_KNOX_TRUSTED_PINPAD_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_TRUSTED_PINPAD"

.field public static final USES_POLICY_MDM_ANALYTICS:I = 0x40

.field public static final USES_POLICY_MDM_ANALYTICS_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ANALYTICS"

.field public static final USES_POLICY_MDM_APN_SETTINGS:I = 0x28

.field public static final USES_POLICY_MDM_APN_SETTINGS_TAG:Ljava/lang/String; = "android.permission.sec.MDM_APN"

.field public static final USES_POLICY_MDM_APPLICATION:I = 0x1b

.field public static final USES_POLICY_MDM_APPLICATION_BACKUP:I = 0x35

.field public static final USES_POLICY_MDM_APPLICATION_BACKUP_TAG:Ljava/lang/String; = "android.permission.sec.MDM_APP_BACKUP"

.field public static final USES_POLICY_MDM_APPLICATION_PERMISSION:I = 0x31

.field public static final USES_POLICY_MDM_APPLICATION_PERMISSION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_APP_PERMISSION_MGMT"

.field public static final USES_POLICY_MDM_APPLICATION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_APP_MGMT"

.field public static final USES_POLICY_MDM_AUDIT_LOG_PERMISSION:I = 0x33

.field public static final USES_POLICY_MDM_AUDIT_LOG_PERMISSION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_AUDIT_LOG"

.field public static final USES_POLICY_MDM_BLUETOOTH:I = 0x1c

.field public static final USES_POLICY_MDM_BLUETOOTH_SECURE_MODE:I = 0x41

.field public static final USES_POLICY_MDM_BLUETOOTH_SECURE_MODE_TAG:Ljava/lang/String; = "android.permission.sec.MDM_BLUETOOTH_SECUREMODE"

.field public static final USES_POLICY_MDM_BLUETOOTH_TAG:Ljava/lang/String; = "android.permission.sec.MDM_BLUETOOTH"

.field public static final USES_POLICY_MDM_BROWSER_PROXY:I = 0x43

.field public static final USES_POLICY_MDM_BROWSER_PROXY_TAG:Ljava/lang/String; = "com.sec.enterprise.mdm.permission.BROWSER_PROXY"

.field public static final USES_POLICY_MDM_BROWSER_SETTINGS:I = 0x2a

.field public static final USES_POLICY_MDM_BROWSER_SETTINGS_TAG:Ljava/lang/String; = "android.permission.sec.MDM_BROWSER_SETTINGS"

.field public static final USES_POLICY_MDM_CALLING:I = 0x25

.field public static final USES_POLICY_MDM_CALLING_TAG:Ljava/lang/String; = "android.permission.sec.MDM_CALLING"

.field public static final USES_POLICY_MDM_CERTIFICATE_PERMISSION:I = 0x32

.field public static final USES_POLICY_MDM_CERTIFICATE_PERMISSION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_CERTIFICATE"

.field public static final USES_POLICY_MDM_DATE_TIME:I = 0x2b

.field public static final USES_POLICY_MDM_DATE_TIME_TAG:Ljava/lang/String; = "android.permission.sec.MDM_DATE_TIME"

.field public static final USES_POLICY_MDM_DEVICE_INVENTORY:I = 0x1d

.field public static final USES_POLICY_MDM_DEVICE_INVENTORY_TAG:Ljava/lang/String; = "android.permission.sec.MDM_INVENTORY"

.field public static final USES_POLICY_MDM_DUAL_SIM:I = 0x3b

.field public static final USES_POLICY_MDM_DUAL_SIM_TAG:Ljava/lang/String; = "android.permission.sec.MDM_DUAL_SIM"

.field public static final USES_POLICY_MDM_EMAIL_ACCOUNT:I = 0x26

.field public static final USES_POLICY_MDM_EMAIL_ACCOUNT_TAG:Ljava/lang/String; = "android.permission.sec.MDM_EMAIL"

.field public static final USES_POLICY_MDM_ENTERPRISE_CONTAINER:I = 0x3c

.field public static final USES_POLICY_MDM_ENTERPRISE_CONTAINER_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ENTERPRISE_CONTAINER"

.field public static final USES_POLICY_MDM_ENTERPRISE_DEVICE_ADMIN:I = 0x2e

.field public static final USES_POLICY_MDM_ENTERPRISE_DEVICE_ADMIN_TAG:Ljava/lang/String; = "android.permission.sec.ENTERPRISE_DEVICE_ADMIN"

.field public static final USES_POLICY_MDM_ENTERPRISE_ISL:I = 0x3a

.field public static final USES_POLICY_MDM_ENTERPRISE_ISL_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ENTERPRISE_ISL"

.field public static final USES_POLICY_MDM_ENTERPRISE_SSO:I = 0x38

.field public static final USES_POLICY_MDM_ENTERPRISE_SSO_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ENTERPRISE_SSO"

.field public static final USES_POLICY_MDM_ENTERPRISE_VPN:I = 0x2c

.field public static final USES_POLICY_MDM_ENTERPRISE_VPN_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ENTERPRISE_VPN"

.field public static final USES_POLICY_MDM_EXCHANGE_ACCOUNT:I = 0x1e

.field public static final USES_POLICY_MDM_EXCHANGE_ACCOUNT_TAG:Ljava/lang/String; = "android.permission.sec.MDM_EXCHANGE"

.field public static final USES_POLICY_MDM_FIREWALL:I = 0x2d

.field public static final USES_POLICY_MDM_FIREWALL_TAG:Ljava/lang/String; = "android.permission.sec.MDM_FIREWALL"

.field public static final USES_POLICY_MDM_GEOFENCING:I = 0x36

.field public static final USES_POLICY_MDM_GEOFENCING_TAG:Ljava/lang/String; = "android.permission.sec.MDM_GEOFENCING"

.field public static final USES_POLICY_MDM_HARDWARE_CONTROL:I = 0x22

.field public static final USES_POLICY_MDM_HARDWARE_CONTROL_TAG:Ljava/lang/String; = "android.permission.sec.MDM_HW_CONTROL"

.field public static final USES_POLICY_MDM_KIOSK_MODE:I = 0x30

.field public static final USES_POLICY_MDM_KIOSK_MODE_TAG:Ljava/lang/String; = "android.permission.sec.MDM_KIOSK_MODE"

.field public static final USES_POLICY_MDM_KNOX_ACTIVATE_DEVICE_PERMISSIONS:I = 0x46

.field public static final USES_POLICY_MDM_KNOX_ACTIVATE_DEVICE_PERMISSIONS_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_ACTIVATE_DEVICE_PERMISSIONS"

.field public static final USES_POLICY_MDM_KNOX_ATTESTATION:I = 0x42

.field public static final USES_POLICY_MDM_KNOX_ATTESTATION_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

.field public static final USES_POLICY_MDM_LDAP_SETTINGS:I = 0x34

.field public static final USES_POLICY_MDM_LDAP_SETTINGS_TAG:Ljava/lang/String; = "android.permission.sec.MDM_LDAP"

.field public static final USES_POLICY_MDM_LICENSE_LOG:I = 0x3e

.field public static final USES_POLICY_MDM_LICENSE_LOG_TAG:Ljava/lang/String; = "android.permission.sec.MDM_LICENSE_LOG"

.field public static final USES_POLICY_MDM_LOCATION:I = 0x24

.field public static final USES_POLICY_MDM_LOCATION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_LOCATION"

.field public static final USES_POLICY_MDM_LOCKSCREEN:I = 0x37

.field public static final USES_POLICY_MDM_LOCKSCREEN_TAG:Ljava/lang/String; = "android.permission.sec.MDM_LOCKSCREEN"

.field public static final USES_POLICY_MDM_MULTI_USER_MGMT:I = 0x3f

.field public static final USES_POLICY_MDM_MULTI_USER_MGMT_TAG:Ljava/lang/String; = "android.permission.sec.MDM_MULTI_USER_MGMT"

.field public static final USES_POLICY_MDM_PHONE_RESTRICTION:I = 0x29

.field public static final USES_POLICY_MDM_PHONE_RESTRICTION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_PHONE_RESTRICTION"

.field public static final USES_POLICY_MDM_RCP_SYNC_MGMT:I = 0x47

.field public static final USES_POLICY_MDM_RCP_SYNC_MGMT_TAG:Ljava/lang/String; = "com.sec.enterprise.knox.permission.KNOX_RCP_SYNC_MGMT"

.field public static final USES_POLICY_MDM_REMOTE_CONTROL:I = 0x2f

.field public static final USES_POLICY_MDM_REMOTE_CONTROL_TAG:Ljava/lang/String; = "android.permission.sec.MDM_REMOTE_CONTROL"

.field public static final USES_POLICY_MDM_RESTRICTION:I = 0x23

.field public static final USES_POLICY_MDM_RESTRICTION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_RESTRICTION"

.field public static final USES_POLICY_MDM_ROAMING:I = 0x1f

.field public static final USES_POLICY_MDM_ROAMING_TAG:Ljava/lang/String; = "android.permission.sec.MDM_ROAMING"

.field public static final USES_POLICY_MDM_SEANDROID_PERMISSION:I = 0x39

.field public static final USES_POLICY_MDM_SEANDROID_PERMISSION_TAG:Ljava/lang/String; = "android.permission.sec.MDM_SEANDROID"

.field public static final USES_POLICY_MDM_SECURITY:I = 0x21

.field public static final USES_POLICY_MDM_SECURITY_TAG:Ljava/lang/String; = "android.permission.sec.MDM_SECURITY"

.field public static final USES_POLICY_MDM_SMARTCARD:I = 0x3d

.field public static final USES_POLICY_MDM_SMARTCARD_TAG:Ljava/lang/String; = "android.permission.sec.MDM_SMARTCARD"

.field public static final USES_POLICY_MDM_SSO:I = 0x52

.field public static final USES_POLICY_MDM_SSO_TAG:Ljava/lang/String; = "com.sec.enterprise.mdm.permission.MDM_SSO"

.field public static final USES_POLICY_MDM_VPN:I = 0x27

.field public static final USES_POLICY_MDM_VPN_TAG:Ljava/lang/String; = "android.permission.sec.MDM_VPN"

.field public static final USES_POLICY_MDM_WIFI:I = 0x20

.field public static final USES_POLICY_MDM_WIFI_TAG:Ljava/lang/String; = "android.permission.sec.MDM_WIFI"

.field public static sKnownPolicies:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field static sPoliciesDisplayOrder:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field static sRevKnownPolicies:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation
.end field

.field static final timaversion:Z


# instance fields
.field mAuthorized:Z

.field mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

.field mLicenseExpiryTime:J

.field final mReceiver:Landroid/content/pm/ResolveInfo;

.field mRequestedPermissions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mUsesPolicies:Ljava/util/BitSet;

.field mVisible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 158
    const-string v2, "3.0"

    const-string v3, "ro.config.timaversion"

    const-string v4, "0"

    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    sput-boolean v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->timaversion:Z

    .line 1201
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    sput-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    .line 1205
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    .line 1209
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    sput-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    .line 1492
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x1b

    const-string v5, "android.permission.sec.MDM_APP_MGMT"

    const v6, 0x1040a81

    const v7, 0x1040a82

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1500
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x1c

    const-string v5, "android.permission.sec.MDM_BLUETOOTH"

    const v6, 0x1040a87

    const v7, 0x1040a88

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1508
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x1d

    const-string v5, "android.permission.sec.MDM_INVENTORY"

    const v6, 0x1040a89

    const v7, 0x1040a8a

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1518
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x1e

    const-string v5, "android.permission.sec.MDM_EXCHANGE"

    const v6, 0x1040a8b

    const v7, 0x1040a8c

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1528
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x1f

    const-string v5, "android.permission.sec.MDM_ROAMING"

    const v6, 0x1040a8d

    const v7, 0x1040a8e

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1538
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x20

    const-string v5, "android.permission.sec.MDM_WIFI"

    const v6, 0x1040a8f

    const v7, 0x1040a90

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1548
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x21

    const-string v5, "android.permission.sec.MDM_SECURITY"

    const v6, 0x1040a91

    const v7, 0x1040a92

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1558
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x22

    const-string v5, "android.permission.sec.MDM_HW_CONTROL"

    const v6, 0x1040a93

    const v7, 0x1040a94

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1568
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x23

    const-string v5, "android.permission.sec.MDM_RESTRICTION"

    const v6, 0x1040a95

    const v7, 0x1040a96

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1578
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x24

    const-string v5, "android.permission.sec.MDM_LOCATION"

    const v6, 0x1040a97

    const v7, 0x1040a98

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1588
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x25

    const-string v5, "android.permission.sec.MDM_CALLING"

    const v6, 0x1040a99

    const v7, 0x1040a9a

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1598
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x26

    const-string v5, "android.permission.sec.MDM_EMAIL"

    const v6, 0x1040a9b

    const v7, 0x1040a9c

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1608
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x27

    const-string v5, "android.permission.sec.MDM_VPN"

    const v6, 0x1040a9d

    const v7, 0x1040a9e

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1618
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x28

    const-string v5, "android.permission.sec.MDM_APN"

    const v6, 0x1040a9f

    const v7, 0x1040aa0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1628
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x29

    const-string v5, "android.permission.sec.MDM_PHONE_RESTRICTION"

    const v6, 0x1040aa1

    const v7, 0x1040aa2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1638
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2a

    const-string v5, "android.permission.sec.MDM_BROWSER_SETTINGS"

    const v6, 0x1040aa3

    const v7, 0x1040aa4

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1648
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x43

    const-string v5, "com.sec.enterprise.mdm.permission.BROWSER_PROXY"

    const v6, 0x1040aa5

    const v7, 0x1040aa6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1658
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2b

    const-string v5, "android.permission.sec.MDM_DATE_TIME"

    const v6, 0x1040aa7

    const v7, 0x1040aa8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1670
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2c

    const-string v5, "android.permission.sec.MDM_ENTERPRISE_VPN"

    const v6, 0x1040aa9

    const v7, 0x1040aaa

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1684
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x44

    const-string v5, "com.sec.enterprise.knox.KNOX_GENERIC_VPN"

    const v6, 0x1040aab

    const v7, 0x1040aac

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1698
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x45

    const-string v5, "com.sec.enterprise.knox.KNOX_CONTAINER_VPN"

    const v6, 0x1040aad

    const v7, 0x1040aae

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1710
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2d

    const-string v5, "android.permission.sec.MDM_FIREWALL"

    const v6, 0x1040aaf

    const v7, 0x1040ab0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1720
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2e

    const-string v5, "android.permission.sec.ENTERPRISE_DEVICE_ADMIN"

    const v6, 0x1040ab1

    const v7, 0x1040ab2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1730
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x2f

    const-string v5, "android.permission.sec.MDM_REMOTE_CONTROL"

    const v6, 0x1040ab3

    const v7, 0x1040ab4

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1740
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x30

    const-string v5, "android.permission.sec.MDM_KIOSK_MODE"

    const v6, 0x1040ab5

    const v7, 0x1040ab6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1750
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x31

    const-string v5, "android.permission.sec.MDM_APP_PERMISSION_MGMT"

    const v6, 0x1040a85

    const v7, 0x1040a86

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1760
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x32

    const-string v5, "android.permission.sec.MDM_CERTIFICATE"

    const v6, 0x1040ac5

    const v7, 0x1040ac6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1770
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x33

    const-string v5, "android.permission.sec.MDM_AUDIT_LOG"

    const v6, 0x1040ac1

    const v7, 0x1040ac2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1780
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3c

    const-string v5, "android.permission.sec.MDM_ENTERPRISE_CONTAINER"

    const v6, 0x1040ab9

    const v7, 0x1040aba

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1791
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x39

    const-string v5, "android.permission.sec.MDM_SEANDROID"

    const v6, 0x1040ac3

    const v7, 0x1040ac4

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1808
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x34

    const-string v5, "android.permission.sec.MDM_LDAP"

    const v6, 0x1040ac7

    const v7, 0x1040ac8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1818
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x35

    const-string v5, "android.permission.sec.MDM_APP_BACKUP"

    const v6, 0x1040a83

    const v7, 0x1040a84

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1826
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x37

    const-string v5, "android.permission.sec.MDM_LOCKSCREEN"

    const v6, 0x1040af9

    const v7, 0x1040afa

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1836
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3b

    const-string v5, "android.permission.sec.MDM_DUAL_SIM"

    const v6, 0x1040ac9

    const v7, 0x1040aca

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1850
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x52

    const-string v5, "com.sec.enterprise.mdm.permission.MDM_SSO"

    const v6, 0x1040adf

    const v7, 0x1040ae0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1870
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x38

    const-string v5, "android.permission.sec.MDM_ENTERPRISE_SSO"

    const v6, 0x1040ab7

    const v7, 0x1040ab8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1884
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3a

    const-string v5, "android.permission.sec.MDM_ENTERPRISE_ISL"

    const v6, 0x1040abb

    const v7, 0x1040abc

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1898
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x36

    const-string v5, "android.permission.sec.MDM_GEOFENCING"

    const v6, 0x1040acd

    const v7, 0x1040ace

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1910
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3d

    const-string v5, "android.permission.sec.MDM_SMARTCARD"

    const v6, 0x1040af5

    const v7, 0x1040af6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1922
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3e

    const-string v5, "android.permission.sec.MDM_LICENSE_LOG"

    const v6, 0x1040af7

    const v7, 0x1040af8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1936
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x3f

    const-string v5, "android.permission.sec.MDM_MULTI_USER_MGMT"

    const v6, 0x1040acf

    const v7, 0x1040ad0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1947
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x40

    const-string v5, "android.permission.sec.MDM_ANALYTICS"

    const v6, 0x1040ad1

    const v7, 0x1040ad2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1958
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x41

    const-string v5, "android.permission.sec.MDM_BLUETOOTH_SECUREMODE"

    const v6, 0x1040ad3

    const v7, 0x1040ad4

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1969
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x42

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_ATTESTATION"

    const v6, 0x1040ad5

    const v7, 0x1040ad6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1982
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x47

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_RCP_SYNC_MGMT"

    const v6, 0x1040adb

    const v7, 0x1040adc

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1995
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x46

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_ACTIVATE_DEVICE_PERMISSIONS"

    const v6, 0x1040af1

    const v7, 0x1040af2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2007
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x48

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_DEACTIVATE_LICENSE"

    const v6, 0x1040ae1

    const v7, 0x1040ae2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2021
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x49

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_SEAMS"

    const v6, 0x1040aed

    const v7, 0x1040aee

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2032
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4a

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_SEAMS_SEPOLICY"

    const v6, 0x1040aef

    const v7, 0x1040af0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2045
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4b

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_RESTRICTION"

    const v6, 0x1040ae3

    const v7, 0x1040ae4

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2058
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4e

    const-string v5, "com.sec.enterprise.knox.permission.CUSTOM_SETTING"

    const v6, 0x1040a6d

    const v7, 0x1040a6e

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2070
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4f

    const-string v5, "com.sec.enterprise.knox.permission.CUSTOM_SYSTEM"

    const v6, 0x1040a6f

    const v7, 0x1040a70

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2082
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x50

    const-string v5, "com.sec.enterprise.knox.permission.CUSTOM_SEALEDMODE"

    const v6, 0x1040a71

    const v7, 0x1040a72

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2093
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x54

    const-string v5, "com.sec.enterprise.knox.permission.CUSTOM_RUBENS_FEATURES"

    const v6, 0x104071c

    const v7, 0x104071d

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2106
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4c

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_CCM"

    const v6, 0x1040ae5

    const v7, 0x1040ae6

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2117
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x4d

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_KEYSTORE"

    const v6, 0x1040ae7

    const v7, 0x1040ae8

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2129
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x51

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_CERTENROLL"

    const v6, 0x1040aeb

    const v7, 0x1040aec

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2138
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    new-instance v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    const/16 v4, 0x53

    const-string v5, "com.sec.enterprise.knox.permission.KNOX_TRUSTED_PINPAD"

    const v6, 0x1040ae9

    const v7, 0x1040aea

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2160
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    .line 2162
    .local v1, "pi":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    iget v3, v1, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;->ident:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2164
    sget-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    iget-object v3, v1, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;->tag:Ljava/lang/String;

    iget v4, v1, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;->ident:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2760
    .end local v1    # "pi":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    :cond_0
    new-instance v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$1;

    invoke-direct {v2}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$1;-><init>()V

    sput-object v2, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/admin/ProxyDeviceAdminInfo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "proxyAdmin"    # Landroid/app/admin/ProxyDeviceAdminInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2214
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    .line 2291
    new-instance v3, Landroid/app/admin/DeviceAdminInfo;

    invoke-direct {v3, p1, p2}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/app/admin/ProxyDeviceAdminInfo;)V

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    .line 2293
    invoke-virtual {p2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getReceiver()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    .line 2295
    new-instance v3, Ljava/util/BitSet;

    invoke-direct {v3}, Ljava/util/BitSet;-><init>()V

    iput-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    .line 2297
    invoke-virtual {p2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    invoke-virtual {p2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getRequestedPermissions()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 2303
    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->parseRequestedPermissions()Ljava/util/List;

    .line 2331
    :cond_0
    return-void

    .line 2309
    :cond_1
    invoke-virtual {p2}, Landroid/app/admin/ProxyDeviceAdminInfo;->getRequestedPermissions()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2311
    .local v1, "policyName":Ljava/lang/String;
    sget-object v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 2313
    .local v2, "val":Ljava/lang/Integer;
    if-eqz v2, :cond_2

    .line 2315
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317
    iget-object v3, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 2321
    :cond_2
    const-string v3, "EnterpriseDeviceAdminInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown tag under uses-policies of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Landroid/content/pm/ResolveInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    .line 2260
    new-instance v0, Landroid/app/admin/DeviceAdminInfo;

    invoke-direct {v0, p1, p2}, Landroid/app/admin/DeviceAdminInfo;-><init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    .line 2262
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    .line 2264
    iput-object p2, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    .line 2267
    const-string v0, "com.android.email"

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2269
    invoke-virtual {p0}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->parseRequestedPermissions()Ljava/util/List;

    .line 2273
    :cond_0
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 2276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    .line 2278
    sget-object v0, Landroid/content/pm/ResolveInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    .line 2280
    invoke-direct {p0, p1}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    .line 2282
    return-void
.end method

.method private readBitSet(Landroid/os/Parcel;)Ljava/util/BitSet;
    .locals 4
    .param p1, "src"    # Landroid/os/Parcel;

    .prologue
    .line 2743
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 2745
    .local v0, "cardinality":I
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    .line 2746
    .local v2, "set":Ljava/util/BitSet;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2747
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2746
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2750
    :cond_0
    return-object v2
.end method

.method private writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "set"    # Ljava/util/BitSet;

    .prologue
    .line 2733
    const/4 v0, -0x1

    .line 2735
    .local v0, "nextSetBit":I
    invoke-virtual {p2}, Ljava/util/BitSet;->cardinality()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 2737
    :goto_0
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p2, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2738
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 2740
    :cond_0
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 2780
    const/4 v0, 0x0

    return v0
.end method

.method public dump(Landroid/util/Printer;Ljava/lang/String;)V
    .locals 3
    .param p1, "pw"    # Landroid/util/Printer;
    .param p2, "prefix"    # Ljava/lang/String;

    .prologue
    .line 2692
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Receiver:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/util/Printer;->println(Ljava/lang/String;)V

    .line 2694
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/ResolveInfo;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 2696
    return-void
.end method

.method public getActivityInfo()Landroid/content/pm/ActivityInfo;
    .locals 1

    .prologue
    .line 2467
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getActivityInfo()Landroid/content/pm/ActivityInfo;

    move-result-object v0

    return-object v0
.end method

.method public getComponent()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 2480
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    return-object v0
.end method

.method public getLicenseExpiry()J
    .locals 2

    .prologue
    .line 2396
    iget-wide v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mLicenseExpiryTime:J

    return-wide v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2422
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReceiver()Landroid/content/pm/ResolveInfo;
    .locals 1

    .prologue
    .line 2452
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    return-object v0
.end method

.method public getReceiverName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2437
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->getReceiverName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestedPermissions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2493
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    return-object v0
.end method

.method public getTagForPolicy(I)Ljava/lang/String;
    .locals 1
    .param p1, "policyIdent"    # I

    .prologue
    .line 2616
    const/16 v0, 0x1b

    if-ge p1, v0, :cond_0

    .line 2618
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->getTagForPolicy(I)Ljava/lang/String;

    move-result-object v0

    .line 2621
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sRevKnownPolicies:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;->tag:Ljava/lang/String;

    goto :goto_0
.end method

.method public getUsedPolicies()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2630
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2632
    .local v5, "res":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;>;"
    iget-object v6, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v6}, Landroid/app/admin/DeviceAdminInfo;->getUsedPolicies()Ljava/util/ArrayList;

    move-result-object v4

    .line 2638
    .local v4, "policyInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/admin/DeviceAdminInfo$PolicyInfo;>;"
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v2, v6, :cond_0

    .line 2640
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    iget v7, v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->ident:I

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    iget-object v8, v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->tag:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    iget v9, v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->label:I

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;

    iget v6, v6, Landroid/app/admin/DeviceAdminInfo$PolicyInfo;->description:I

    invoke-direct {v0, v7, v8, v9, v6}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;-><init>(ILjava/lang/String;II)V

    .line 2646
    .local v0, "dpmPolicy":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2638
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2652
    .end local v0    # "dpmPolicy":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    sget-object v6, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 2654
    sget-object v6, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sPoliciesDisplayOrder:Ljava/util/ArrayList;

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;

    .line 2656
    .local v3, "pi":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    iget v6, v3, Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;->ident:I

    invoke-virtual {p0, v6}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->usesPolicy(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2658
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2652
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2664
    .end local v3    # "pi":Landroid/app/enterprise/EnterpriseDeviceAdminInfo$PolicyInfo;
    :cond_2
    return-object v5
.end method

.method public isAuthorized()Z
    .locals 1

    .prologue
    .line 2360
    iget-boolean v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mAuthorized:Z

    return v0
.end method

.method public isProxy()Z
    .locals 1

    .prologue
    .line 2342
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->isProxy()Z

    move-result v0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 2574
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0}, Landroid/app/admin/DeviceAdminInfo;->isVisible()Z

    move-result v0

    return v0
.end method

.method public loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "pm"    # Landroid/content/pm/PackageManager;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/res/Resources$NotFoundException;
        }
    .end annotation

    .prologue
    .line 2538
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->loadDescription(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 2559
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "pm"    # Landroid/content/pm/PackageManager;

    .prologue
    .line 2515
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public parseRequestedPermissions()Ljava/util/List;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2796
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v14, v0, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    .line 2798
    .local v14, "path":Ljava/lang/String;
    const/4 v13, 0x0

    .line 2800
    .local v13, "parser":Landroid/content/res/XmlResourceParser;
    const/4 v2, 0x0

    .line 2804
    .local v2, "assmgr":Landroid/content/res/AssetManager;
    :try_start_0
    new-instance v3, Landroid/content/res/AssetManager;

    invoke-direct {v3}, Landroid/content/res/AssetManager;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 2806
    .end local v2    # "assmgr":Landroid/content/res/AssetManager;
    .local v3, "assmgr":Landroid/content/res/AssetManager;
    :try_start_1
    invoke-virtual {v3, v14}, Landroid/content/res/AssetManager;->addAssetPath(Ljava/lang/String;)I

    move-result v5

    .line 2808
    .local v5, "cookie":I
    if-eqz v5, :cond_1

    .line 2810
    const-string v21, "AndroidManifest.xml"

    move-object/from16 v0, v21

    invoke-virtual {v3, v5, v0}, Landroid/content/res/AssetManager;->openXmlResourceParser(ILjava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v13

    :goto_0
    move-object v2, v3

    .line 2824
    .end local v3    # "assmgr":Landroid/content/res/AssetManager;
    .end local v5    # "cookie":I
    .restart local v2    # "assmgr":Landroid/content/res/AssetManager;
    :goto_1
    if-nez v13, :cond_2

    .line 2826
    if-eqz v2, :cond_0

    .line 2828
    invoke-virtual {v2}, Landroid/content/res/AssetManager;->close()V

    .line 2830
    :cond_0
    const/16 v21, 0x0

    .line 2965
    :goto_2
    return-object v21

    .line 2814
    .end local v2    # "assmgr":Landroid/content/res/AssetManager;
    .restart local v3    # "assmgr":Landroid/content/res/AssetManager;
    .restart local v5    # "cookie":I
    :cond_1
    :try_start_2
    const-string v21, "EnterpriseDeviceAdminInfo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Failed adding asset path:"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 2818
    .end local v5    # "cookie":I
    :catch_0
    move-exception v6

    move-object v2, v3

    .line 2820
    .end local v3    # "assmgr":Landroid/content/res/AssetManager;
    .restart local v2    # "assmgr":Landroid/content/res/AssetManager;
    .local v6, "e":Ljava/lang/Exception;
    :goto_3
    const-string v21, "EnterpriseDeviceAdminInfo"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "Unable to read AndroidManifest.xml of "

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 2835
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v10, Landroid/util/DisplayMetrics;

    invoke-direct {v10}, Landroid/util/DisplayMetrics;-><init>()V

    .line 2837
    .local v10, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v10}, Landroid/util/DisplayMetrics;->setToDefaults()V

    .line 2839
    const/16 v17, 0x0

    .line 2845
    .local v17, "sa":Landroid/content/res/TypedArray;
    :try_start_3
    new-instance v16, Landroid/content/res/Resources;

    const/16 v21, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v2, v10, v1}, Landroid/content/res/Resources;-><init>(Landroid/content/res/AssetManager;Landroid/util/DisplayMetrics;Landroid/content/res/Configuration;)V

    .line 2847
    .local v16, "res":Landroid/content/res/Resources;
    move-object v4, v13

    .line 2854
    .local v4, "attrs":Landroid/util/AttributeSet;
    :cond_3
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v19

    .local v19, "type":I
    const/16 v21, 0x2

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_4

    const/16 v21, 0x1

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 2861
    :cond_4
    sget-object v21, Lcom/android/internal/R$styleable;->AndroidManifest:[I

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v17

    .line 2865
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v12

    .line 2869
    .local v12, "outerDepth":I
    :cond_5
    :goto_4
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v19

    const/16 v21, 0x1

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_c

    const/16 v21, 0x3

    move/from16 v0, v19

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->getDepth()I

    move-result v21

    move/from16 v0, v21

    if-le v0, v12, :cond_c

    .line 2871
    :cond_6
    const/16 v21, 0x3

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    const/16 v21, 0x4

    move/from16 v0, v19

    move/from16 v1, v21

    if-eq v0, v1, :cond_5

    .line 2878
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v18

    .line 2880
    .local v18, "tagName":Ljava/lang/String;
    if-eqz v18, :cond_5

    const-string v21, "uses-permission"

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 2882
    sget-object v21, Lcom/android/internal/R$styleable;->AndroidManifestUsesPermission:[I

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v4, v1}, Landroid/content/res/Resources;->obtainAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v17

    .line 2893
    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getNonResourceString(I)Ljava/lang/String;

    move-result-object v11

    .line 2897
    .local v11, "name":Ljava/lang/String;
    sget-object v21, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 2899
    .local v20, "val":Ljava/lang/Integer;
    if-eqz v20, :cond_7

    .line 2901
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/BitSet;->set(I)V

    .line 2904
    if-eqz v11, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v11}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_7

    .line 2906
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-virtual {v11}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2922
    :cond_7
    invoke-static {v13}, Lcom/android/internal/util/XmlUtils;->skipCurrentTag(Lorg/xmlpull/v1/XmlPullParser;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_4

    .line 2929
    .end local v4    # "attrs":Landroid/util/AttributeSet;
    .end local v11    # "name":Ljava/lang/String;
    .end local v12    # "outerDepth":I
    .end local v16    # "res":Landroid/content/res/Resources;
    .end local v18    # "tagName":Ljava/lang/String;
    .end local v19    # "type":I
    .end local v20    # "val":Ljava/lang/Integer;
    :catch_1
    move-exception v6

    .line 2931
    .restart local v6    # "e":Ljava/lang/Exception;
    :try_start_4
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2934
    if-eqz v17, :cond_8

    .line 2935
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/TypedArray;->recycle()V

    .line 2938
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_8
    :goto_5
    invoke-interface {v13}, Landroid/content/res/XmlResourceParser;->close()V

    .line 2940
    if-eqz v2, :cond_9

    .line 2942
    invoke-virtual {v2}, Landroid/content/res/AssetManager;->close()V

    .line 2945
    :cond_9
    const/16 v21, 0x0

    invoke-static/range {v21 .. v21}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getInstance(Landroid/content/Context;)Landroid/app/enterprise/license/EnterpriseLicenseManager;

    move-result-object v9

    .line 2947
    .local v9, "licenseMgr":Landroid/app/enterprise/license/EnterpriseLicenseManager;
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/app/admin/DeviceAdminInfo;->getPackageName()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v9, v0}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->getELMPermissions(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 2948
    .local v7, "elmPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v7, :cond_b

    .line 2949
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_a
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v21

    if-eqz v21, :cond_b

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    .line 2950
    .local v15, "permission":Ljava/lang/String;
    sget-object v21, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->sKnownPolicies:Ljava/util/HashMap;

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/lang/Integer;

    .line 2951
    .restart local v20    # "val":Ljava/lang/Integer;
    if-eqz v20, :cond_a

    .line 2952
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/util/BitSet;->set(I)V

    .line 2954
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-interface {v0, v15}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_a

    .line 2955
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    move-object/from16 v21, v0

    invoke-virtual {v15}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v22

    invoke-interface/range {v21 .. v22}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    .line 2960
    .end local v7    # "elmPermissions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v15    # "permission":Ljava/lang/String;
    .end local v20    # "val":Ljava/lang/Integer;
    :catch_2
    move-exception v6

    .line 2961
    .restart local v6    # "e":Ljava/lang/Exception;
    const-string v21, "EnterpriseDeviceAdminInfo"

    const-string v22, "Failed to get ELM permissions"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2965
    .end local v6    # "e":Ljava/lang/Exception;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mRequestedPermissions:Ljava/util/List;

    move-object/from16 v21, v0

    goto/16 :goto_2

    .line 2934
    .end local v9    # "licenseMgr":Landroid/app/enterprise/license/EnterpriseLicenseManager;
    .restart local v4    # "attrs":Landroid/util/AttributeSet;
    .restart local v12    # "outerDepth":I
    .restart local v16    # "res":Landroid/content/res/Resources;
    .restart local v19    # "type":I
    :cond_c
    if-eqz v17, :cond_8

    .line 2935
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_5

    .line 2934
    .end local v4    # "attrs":Landroid/util/AttributeSet;
    .end local v12    # "outerDepth":I
    .end local v16    # "res":Landroid/content/res/Resources;
    .end local v19    # "type":I
    :catchall_0
    move-exception v21

    if-eqz v17, :cond_d

    .line 2935
    invoke-virtual/range {v17 .. v17}, Landroid/content/res/TypedArray;->recycle()V

    :cond_d
    throw v21

    .line 2818
    .end local v10    # "metrics":Landroid/util/DisplayMetrics;
    .end local v17    # "sa":Landroid/content/res/TypedArray;
    :catch_3
    move-exception v6

    goto/16 :goto_3
.end method

.method public readPoliciesFromXml(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 1
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2685
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->readPoliciesFromXml(Lorg/xmlpull/v1/XmlPullParser;)V

    .line 2687
    return-void
.end method

.method public setAuthorized(Z)V
    .locals 0
    .param p1, "authorized"    # Z

    .prologue
    .line 2381
    iput-boolean p1, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mAuthorized:Z

    .line 2383
    return-void
.end method

.method public setLicenseExpiry(J)V
    .locals 1
    .param p1, "licenseExpiry"    # J

    .prologue
    .line 2409
    iput-wide p1, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mLicenseExpiryTime:J

    .line 2411
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2703
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeviceAdminInfo{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public usesMDMPolicy()Z
    .locals 1

    .prologue
    .line 2368
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public usesPolicy(I)Z
    .locals 1
    .param p1, "policyIdent"    # I

    .prologue
    .line 2593
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->usesPolicy(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2595
    const/4 v0, 0x1

    .line 2598
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    invoke-virtual {v0, p1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    goto :goto_0
.end method

.method public writePoliciesToXml(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 1
    .param p1, "out"    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2675
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mDeviceAdminInfo:Landroid/app/admin/DeviceAdminInfo;

    invoke-virtual {v0, p1}, Landroid/app/admin/DeviceAdminInfo;->writePoliciesToXml(Lorg/xmlpull/v1/XmlSerializer;)V

    .line 2676
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 2726
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mReceiver:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v0, p1, p2}, Landroid/content/pm/ResolveInfo;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2728
    iget-object v0, p0, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->mUsesPolicies:Ljava/util/BitSet;

    invoke-direct {p0, p1, v0}, Landroid/app/enterprise/EnterpriseDeviceAdminInfo;->writeBitSet(Landroid/os/Parcel;Ljava/util/BitSet;)V

    .line 2730
    return-void
.end method
