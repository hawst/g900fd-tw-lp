.class public Landroid/app/enterprise/MiscPolicy;
.super Ljava/lang/Object;
.source "MiscPolicy.java"


# static fields
.field public static final ACTION_SIM_CARD_CHANGED:Ljava/lang/String; = "android.intent.action.sec.SIM_CARD_CHANGED"

.field public static final EXTRA_SIM_CHANGE_INFO:Ljava/lang/String; = "simChangeInfo"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mClipboard:Landroid/content/ClipboardManager;

.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

.field private mService:Landroid/app/enterprise/IMiscPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-string v0, "MiscPolicy"

    sput-object v0, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mClipboard:Landroid/content/ClipboardManager;

    .line 97
    iput-object p2, p0, Landroid/app/enterprise/MiscPolicy;->mContext:Landroid/content/Context;

    .line 98
    iput-object p1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 99
    return-void
.end method

.method private getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Landroid/app/enterprise/EnterpriseDeviceManager;-><init>(Landroid/content/Context;Landroid/app/enterprise/ContextInfo;Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 113
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mEDM:Landroid/app/enterprise/EnterpriseDeviceManager;

    return-object v0
.end method

.method private getService()Landroid/app/enterprise/IMiscPolicy;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    if-nez v0, :cond_0

    .line 103
    const-string v0, "misc_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IMiscPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IMiscPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    .line 106
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    return-object v0
.end method


# virtual methods
.method public addClipboardTextData(Ljava/lang/String;)Z
    .locals 3
    .param p1, "clip"    # Ljava/lang/String;

    .prologue
    .line 875
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.addClipboardTextData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 876
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 878
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IMiscPolicy;->addClipboardTextData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 883
    :goto_0
    return v1

    .line 879
    :catch_0
    move-exception v0

    .line 880
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to Add ClipBoard!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 883
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addWebBookmarkBitmap(Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "iconBm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 575
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.addWebBookmarkBitmap"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 576
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 578
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IMiscPolicy;->addWebBookmarkBitmap(Landroid/app/enterprise/ContextInfo;Landroid/net/Uri;Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 583
    :goto_0
    return v1

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed addWebBookmarkBitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addWebBookmarkByteBuffer(Landroid/net/Uri;Ljava/lang/String;[B)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "iconBuffer"    # [B

    .prologue
    .line 547
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.addWebBookmarkByteBuffer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 548
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 550
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IMiscPolicy;->addWebBookmarkByteBuffer(Landroid/app/enterprise/ContextInfo;Landroid/net/Uri;Ljava/lang/String;[B)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 556
    :goto_0
    return v1

    .line 552
    :catch_0
    move-exception v0

    .line 553
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed addWebBookmarkByteBuffer"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 556
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public changeLockScreenString(Ljava/lang/String;)Z
    .locals 3
    .param p1, "statement"    # Ljava/lang/String;

    .prologue
    .line 624
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.changeLockScreenString"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 625
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 627
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IMiscPolicy;->changeLockScreenString(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 632
    :goto_0
    return v1

    .line 628
    :catch_0
    move-exception v0

    .line 629
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed changeLockScreenString"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 632
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearClipboardData()Z
    .locals 3

    .prologue
    .line 812
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.clearClipboardData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 813
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 815
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->clearClipboardData(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 820
    :goto_0
    return v1

    .line 816
    :catch_0
    move-exception v0

    .line 817
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to Clear ClipBoard!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 820
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteWebBookmark(Landroid/net/Uri;Ljava/lang/String;)Z
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 600
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.deleteWebBookmark"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 601
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 603
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IMiscPolicy;->deleteWebBookmark(Landroid/app/enterprise/ContextInfo;Landroid/net/Uri;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 608
    :goto_0
    return v1

    .line 604
    :catch_0
    move-exception v0

    .line 605
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed deleteWebBookmark"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 608
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getClipboardTextData()Ljava/lang/String;
    .locals 3

    .prologue
    .line 842
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.getClipboardTextData"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 843
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 845
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getClipboardTextData(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 850
    :goto_0
    return-object v1

    .line 846
    :catch_0
    move-exception v0

    .line 847
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to Get ClipBoard!!!!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 850
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentLockScreenString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 643
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.getCurrentLockScreenString"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 644
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 646
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getCurrentLockScreenString(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 651
    :goto_0
    return-object v1

    .line 647
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getCurrentLockScreenString!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 651
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLastSimChangeInfo()Landroid/app/enterprise/SimChangeInfo;
    .locals 4

    .prologue
    .line 518
    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MiscPolicy.getLastSimChangeInfo"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 519
    new-instance v1, Landroid/app/enterprise/SimChangeInfo;

    invoke-direct {v1}, Landroid/app/enterprise/SimChangeInfo;-><init>()V

    .line 520
    .local v1, "ret":Landroid/app/enterprise/SimChangeInfo;
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 522
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v3, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IMiscPolicy;->getLastSimChangeInfo(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/SimChangeInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 527
    .end local v1    # "ret":Landroid/app/enterprise/SimChangeInfo;
    :cond_0
    :goto_0
    return-object v1

    .line 523
    .restart local v1    # "ret":Landroid/app/enterprise/SimChangeInfo;
    :catch_0
    move-exception v0

    .line 524
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with misc policy"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSystemActiveFont()Ljava/lang/String;
    .locals 3

    .prologue
    .line 705
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 707
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getSystemActiveFont(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 712
    :goto_0
    return-object v1

    .line 708
    :catch_0
    move-exception v0

    .line 709
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to getSystemActiveFont!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 712
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemActiveFontSize()F
    .locals 3

    .prologue
    .line 965
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 967
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getSystemActiveFontSize(Landroid/app/enterprise/ContextInfo;)F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 972
    :goto_0
    return v1

    .line 968
    :catch_0
    move-exception v0

    .line 969
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 972
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemFontSizes()[F
    .locals 3

    .prologue
    .line 999
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1001
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getSystemFontSizes(Landroid/app/enterprise/ContextInfo;)[F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1006
    :goto_0
    return-object v1

    .line 1002
    :catch_0
    move-exception v0

    .line 1003
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1006
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSystemFonts()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 724
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 726
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IMiscPolicy;->getSystemFonts(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 731
    :goto_0
    return-object v1

    .line 727
    :catch_0
    move-exception v0

    .line 728
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to getSystemFonts!!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 731
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public installCertificateWithType(Ljava/lang/String;[B)V
    .locals 2
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "value"    # [B

    .prologue
    .line 248
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.installCertificateWithType"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 249
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/app/enterprise/SecurityPolicy;->installCertificateWithType(Ljava/lang/String;[B)V

    .line 250
    return-void
.end method

.method public isBluetoothEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 420
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/BluetoothPolicy;->isBluetoothEnabled()Z

    move-result v0

    return v0
.end method

.method public isCameraEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 291
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/RestrictionPolicy;->isCameraEnabled(Z)Z

    move-result v0

    return v0
.end method

.method public isExternalStorageEncrypted()Z
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.isExternalStorageEncrypted"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 505
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/SecurityPolicy;->isExternalStorageEncrypted()Z

    move-result v0

    return v0
.end method

.method public isInternalStorageEncrypted()Z
    .locals 2

    .prologue
    .line 484
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.isInternalStorageEncrypted"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 485
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/SecurityPolicy;->isInternalStorageEncrypted()Z

    move-result v0

    return v0
.end method

.method public isMicrophoneEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 334
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/RestrictionPolicy;->isMicrophoneEnabled(Z)Z

    move-result v0

    return v0
.end method

.method public isWiFiEnabled(Z)Z
    .locals 1
    .param p1, "showMsg"    # Z

    .prologue
    .line 377
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getWifiPolicy()Landroid/app/enterprise/WifiPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/WifiPolicy;->isWifiAllowed(Z)Z

    move-result v0

    return v0
.end method

.method public setBluetoothState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 401
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setBluetoothState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 402
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getBluetoothPolicy()Landroid/app/enterprise/BluetoothPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/BluetoothPolicy;->setBluetoothState(Z)Z

    move-result v0

    return v0
.end method

.method public setCameraState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 272
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setCameraState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/RestrictionPolicy;->setCameraState(Z)Z

    move-result v0

    return v0
.end method

.method public setExternalStorageEncryption(Z)V
    .locals 2
    .param p1, "isEncrypt"    # Z

    .prologue
    .line 464
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setExternalStorageEncryption"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 465
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/SecurityPolicy;->setExternalStorageEncryption(Z)V

    .line 466
    return-void
.end method

.method public setInternalStorageEncryption(Z)V
    .locals 2
    .param p1, "isEncrypt"    # Z

    .prologue
    .line 442
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setInternalStorageEncryption"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 443
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getSecurityPolicy()Landroid/app/enterprise/SecurityPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/SecurityPolicy;->setInternalStorageEncryption(Z)V

    .line 444
    return-void
.end method

.method public setMicrophoneState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 315
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setMicrophoneState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 316
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/RestrictionPolicy;->setMicrophoneState(Z)Z

    move-result v0

    return v0
.end method

.method public setRingerBytes([BLjava/lang/String;)V
    .locals 3
    .param p1, "buffer"    # [B
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 179
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.setRingerBytes"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 180
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 182
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IMiscPolicy;->setRingerBytes(Landroid/app/enterprise/ContextInfo;[BLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    .line 184
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setRingerFilePath(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "aRingtonefilePath"    # Ljava/lang/String;
    .param p2, "aMmimeType"    # Ljava/lang/String;

    .prologue
    .line 199
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 201
    if-nez p1, :cond_1

    .line 219
    :cond_0
    :goto_0
    return-void

    .line 204
    :cond_1
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContext:Landroid/content/Context;

    invoke-static {v1, p1}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 206
    if-nez p1, :cond_2

    .line 207
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to copy file"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 212
    :cond_2
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IMiscPolicy;->setRingerFilePath(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :goto_1
    invoke-static {p1}, Landroid/app/enterprise/lso/LSOUtils;->deleteFile(Ljava/lang/String;)V

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setSystemActiveFont(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "fontName"    # Ljava/lang/String;
    .param p2, "apkPath"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 671
    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "MiscPolicy.setSystemActiveFont"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 672
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 674
    if-eqz p2, :cond_1

    .line 675
    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContext:Landroid/content/Context;

    invoke-static {v2, p2}, Landroid/app/enterprise/lso/LSOUtils;->copyFileToDataLocalDirectory(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 677
    if-nez p2, :cond_1

    .line 678
    sget-object v2, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to copy file"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 692
    :cond_0
    :goto_0
    return v1

    .line 684
    :cond_1
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v3, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1, p2}, Landroid/app/enterprise/IMiscPolicy;->setSystemActiveFont(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 685
    :catch_0
    move-exception v0

    .line 686
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed setSystemFont!!!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 689
    if-eqz p2, :cond_0

    .line 690
    invoke-static {p2}, Landroid/app/enterprise/lso/LSOUtils;->deleteFile(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setSystemActiveFontSize(F)Z
    .locals 3
    .param p1, "fontSize"    # F

    .prologue
    .line 942
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "MiscPolicy.setSystemActiveFontSize"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 943
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getService()Landroid/app/enterprise/IMiscPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 945
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/MiscPolicy;->mService:Landroid/app/enterprise/IMiscPolicy;

    iget-object v2, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IMiscPolicy;->setSystemActiveFontSize(Landroid/app/enterprise/ContextInfo;F)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 950
    :goto_0
    return v1

    .line 946
    :catch_0
    move-exception v0

    .line 947
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/MiscPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 950
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setWiFiState(Z)Z
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 358
    iget-object v0, p0, Landroid/app/enterprise/MiscPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "MiscPolicy.setWiFiState"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 359
    invoke-direct {p0}, Landroid/app/enterprise/MiscPolicy;->getEDM()Landroid/app/enterprise/EnterpriseDeviceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getWifiPolicy()Landroid/app/enterprise/WifiPolicy;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/enterprise/WifiPolicy;->setWifi(Z)Z

    move-result v0

    return v0
.end method
