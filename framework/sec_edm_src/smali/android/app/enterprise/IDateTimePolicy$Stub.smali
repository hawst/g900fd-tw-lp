.class public abstract Landroid/app/enterprise/IDateTimePolicy$Stub;
.super Landroid/os/Binder;
.source "IDateTimePolicy.java"

# interfaces
.implements Landroid/app/enterprise/IDateTimePolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IDateTimePolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IDateTimePolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IDateTimePolicy"

.field static final TRANSACTION_getAutomaticTime:I = 0xa

.field static final TRANSACTION_getDateFormat:I = 0x8

.field static final TRANSACTION_getDateTime:I = 0x2

.field static final TRANSACTION_getDaylightSavingTime:I = 0xb

.field static final TRANSACTION_getTimeFormat:I = 0x6

.field static final TRANSACTION_getTimeZone:I = 0x4

.field static final TRANSACTION_isDateTimeChangeEnabled:I = 0xd

.field static final TRANSACTION_setAutomaticTime:I = 0x9

.field static final TRANSACTION_setDateFormat:I = 0x7

.field static final TRANSACTION_setDateTime:I = 0x1

.field static final TRANSACTION_setDateTimeChangeEnabled:I = 0xc

.field static final TRANSACTION_setTimeFormat:I = 0x5

.field static final TRANSACTION_setTimeZone:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IDateTimePolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IDateTimePolicy;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Landroid/app/enterprise/IDateTimePolicy;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Landroid/app/enterprise/IDateTimePolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IDateTimePolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 253
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 42
    :sswitch_0
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_1

    .line 50
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 56
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 57
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setDateTime(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v4

    .line 58
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    if-eqz v4, :cond_0

    move v1, v6

    :cond_0
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 53
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 64
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 67
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 72
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getDateTime(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v4

    .line 73
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 74
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto :goto_0

    .line 70
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":J
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 79
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_4

    .line 82
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 88
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 89
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setTimeZone(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v4

    .line 90
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    if-eqz v4, :cond_3

    move v1, v6

    :cond_3
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 85
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 96
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_5

    .line 99
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 104
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getTimeZone(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v4

    .line 105
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 102
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Ljava/lang/String;
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 111
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_7

    .line 114
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 120
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 121
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setTimeFormat(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v4

    .line 122
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    if-eqz v4, :cond_6

    move v1, v6

    :cond_6
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 117
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 128
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    .line 131
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 136
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getTimeFormat(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v4

    .line 137
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 134
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Ljava/lang/String;
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 143
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_a

    .line 146
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 152
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 153
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setDateFormat(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v4

    .line 154
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v4, :cond_9

    move v1, v6

    :cond_9
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 149
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v4    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 160
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v1, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_b

    .line 163
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v1, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 168
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getDateFormat(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v4

    .line 169
    .local v4, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 170
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 166
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Ljava/lang/String;
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 175
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_d

    .line 178
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 184
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_e

    move v2, v6

    .line 185
    .local v2, "_arg1":Z
    :goto_a
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setAutomaticTime(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 186
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    if-eqz v4, :cond_c

    move v1, v6

    :cond_c
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 181
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    :cond_e
    move v2, v1

    .line 184
    goto :goto_a

    .line 192
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_10

    .line 195
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 200
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getAutomaticTime(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 201
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 202
    if-eqz v4, :cond_f

    move v1, v6

    :cond_f
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 198
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 207
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_12

    .line 210
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 215
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->getDaylightSavingTime(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 216
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 217
    if-eqz v4, :cond_11

    move v1, v6

    :cond_11
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 213
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_12
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 222
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 224
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_14

    .line 225
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 231
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_15

    move v2, v6

    .line 232
    .restart local v2    # "_arg1":Z
    :goto_e
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IDateTimePolicy$Stub;->setDateTimeChangeEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v4

    .line 233
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 234
    if-eqz v4, :cond_13

    move v1, v6

    :cond_13
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 228
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Z
    .end local v4    # "_result":Z
    :cond_14
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    :cond_15
    move v2, v1

    .line 231
    goto :goto_e

    .line 239
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_d
    const-string v7, "android.app.enterprise.IDateTimePolicy"

    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v7

    if-eqz v7, :cond_17

    .line 242
    sget-object v7, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 247
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->isDateTimeChangeEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 248
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 249
    if-eqz v4, :cond_16

    move v1, v6

    :cond_16
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 245
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_17
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
