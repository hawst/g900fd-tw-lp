.class public Landroid/app/enterprise/DatafadePolicy;
.super Ljava/lang/Object;
.source "DatafadePolicy.java"


# static fields
.field public static final EXTERNAL_MEMORY:Ljava/lang/String; = "external"

.field public static final FACTORY_RESET:Ljava/lang/String; = "factory_reset"

.field public static final INTERNAL_EXTERNAL_MEMORY:Ljava/lang/String; = "all"

.field public static final INTERNAL_MEMORY:Ljava/lang/String; = "internal"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mService:Landroid/app/enterprise/IDataFadePolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 144
    const-string v0, "DataFadePolicy"

    sput-object v0, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    .line 184
    return-void
.end method


# virtual methods
.method public addDatafadeWipeTask(IIII[Ljava/lang/String;)Z
    .locals 7
    .param p1, "wipeTypeWeight"    # I
    .param p2, "timeDurationToPromptAuth"    # I
    .param p3, "timeDurationToWaitForAuthInput"    # I
    .param p4, "validAuthAttempts"    # I
    .param p5, "wipeTypeMetaData"    # [Ljava/lang/String;

    .prologue
    .line 376
    iget-object v0, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v0, :cond_0

    .line 380
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Landroid/app/enterprise/IDataFadePolicy;->addDatafadeWipeTask(IIII[Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 392
    :goto_0
    return v0

    .line 384
    :catch_0
    move-exception v6

    .line 386
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with misc policy"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 392
    .end local v6    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public deleteDatafadeWipeTask(I)Z
    .locals 3
    .param p1, "wipeTypeWeight"    # I

    .prologue
    .line 430
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 434
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IDataFadePolicy;->deleteDatafadeWipeTask(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 444
    :goto_0
    return v1

    .line 436
    :catch_0
    move-exception v0

    .line 438
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 444
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDatafadeWipeInfoList(I)[Landroid/app/enterprise/DatafadeInfo;
    .locals 3
    .param p1, "wipeTypeWeight"    # I

    .prologue
    .line 482
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 486
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    invoke-interface {v1, p1}, Landroid/app/enterprise/IDataFadePolicy;->getDatafadeWipeInfoList(I)[Landroid/app/enterprise/DatafadeInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 496
    :goto_0
    return-object v1

    .line 488
    :catch_0
    move-exception v0

    .line 490
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 496
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public registerDatafadeWipeCallback(Landroid/app/enterprise/DatafadeWipeCallback;)V
    .locals 3
    .param p1, "cb"    # Landroid/app/enterprise/DatafadeWipeCallback;

    .prologue
    .line 530
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 534
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    iget-object v2, p1, Landroid/app/enterprise/DatafadeWipeCallback;->callback:Landroid/app/enterprise/IDatafadeWipeCallback;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDataFadePolicy;->registerDatafadeWipeCallback(Landroid/app/enterprise/IDatafadeWipeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 536
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public startDatafadeWipe()Z
    .locals 3

    .prologue
    .line 224
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 228
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IDataFadePolicy;->startDatafadeWipe()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 238
    :goto_0
    return v1

    .line 230
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 238
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopDatafadeWipe()Z
    .locals 3

    .prologue
    .line 266
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 270
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    invoke-interface {v1}, Landroid/app/enterprise/IDataFadePolicy;->stopDatafadeWipe()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 280
    :goto_0
    return v1

    .line 272
    :catch_0
    move-exception v0

    .line 274
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 280
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unregisterDatafadeWipeCallback(Landroid/app/enterprise/DatafadeWipeCallback;)V
    .locals 3
    .param p1, "cb"    # Landroid/app/enterprise/DatafadeWipeCallback;

    .prologue
    .line 568
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    if-eqz v1, :cond_0

    .line 572
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/DatafadePolicy;->mService:Landroid/app/enterprise/IDataFadePolicy;

    iget-object v2, p1, Landroid/app/enterprise/DatafadeWipeCallback;->callback:Landroid/app/enterprise/IDatafadeWipeCallback;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDataFadePolicy;->unregisterDatafadeWipeCallback(Landroid/app/enterprise/IDatafadeWipeCallback;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 574
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DatafadePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
