.class public Landroid/app/enterprise/DateTimePolicy;
.super Ljava/lang/Object;
.source "DateTimePolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IDateTimePolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-string v0, "DateTimePolicy"

    sput-object v0, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 69
    return-void
.end method

.method private getService()Landroid/app/enterprise/IDateTimePolicy;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    if-nez v0, :cond_0

    .line 74
    const-string v0, "date_time_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IDateTimePolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    .line 77
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    return-object v0
.end method


# virtual methods
.method public getAutomaticTime()Z
    .locals 3

    .prologue
    .line 336
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 337
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->getAutomaticTime(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 342
    :goto_0
    return v1

    .line 339
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API getAutomaticTime"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 342
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDateFormat()Ljava/lang/String;
    .locals 3

    .prologue
    .line 289
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->getDateFormat(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 295
    :goto_0
    return-object v1

    .line 292
    :catch_0
    move-exception v0

    .line 293
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API getDateFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 295
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDateTime()Ljava/util/Date;
    .locals 6

    .prologue
    .line 141
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 143
    iget-object v4, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v5, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v4, v5}, Landroid/app/enterprise/IDateTimePolicy;->getDateTime(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v2

    .line 144
    .local v2, "millis":J
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 145
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 146
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 151
    .end local v0    # "c":Ljava/util/Calendar;
    .end local v2    # "millis":J
    :goto_0
    return-object v4

    .line 148
    :catch_0
    move-exception v1

    .line 149
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at DateTime policy API getDateTime"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 151
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getDaylightSavingTime()Z
    .locals 3

    .prologue
    .line 356
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 357
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->getDaylightSavingTime(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 362
    :goto_0
    return v1

    .line 359
    :catch_0
    move-exception v0

    .line 360
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API getDaylightSavingTime"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTimeFormat()Ljava/lang/String;
    .locals 3

    .prologue
    .line 240
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 241
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->getTimeFormat(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 246
    :goto_0
    return-object v1

    .line 243
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API getTimeFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 246
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTimeZone()Ljava/lang/String;
    .locals 3

    .prologue
    .line 195
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->getTimeZone(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 201
    :goto_0
    return-object v1

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API getTimeZone"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 201
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDateTimeChangeEnabled()Z
    .locals 3

    .prologue
    .line 427
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IDateTimePolicy;->isDateTimeChangeEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 433
    :goto_0
    return v1

    .line 430
    :catch_0
    move-exception v0

    .line 431
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setTimeFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 433
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setAutomaticTime(Z)Z
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 315
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DateTimePolicy.setAutomaticTime"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 317
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 318
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDateTimePolicy;->setAutomaticTime(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 323
    :goto_0
    return v1

    .line 320
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setAutomaticTime"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 323
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDateFormat(Ljava/lang/String;)Z
    .locals 3
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 264
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DateTimePolicy.setDateFormat"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 266
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDateTimePolicy;->setDateFormat(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 272
    :goto_0
    return v1

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setDateFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 272
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDateTime(IIIIII)Z
    .locals 6
    .param p1, "day"    # I
    .param p2, "month"    # I
    .param p3, "year"    # I
    .param p4, "hour"    # I
    .param p5, "minute"    # I
    .param p6, "second"    # I

    .prologue
    .line 108
    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "DateTimePolicy.setDateTime"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 110
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 112
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 114
    .local v0, "c":Ljava/util/Calendar;
    const/4 v2, 0x5

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->set(II)V

    .line 115
    const/4 v2, 0x2

    invoke-virtual {v0, v2, p2}, Ljava/util/Calendar;->set(II)V

    .line 116
    const/4 v2, 0x1

    invoke-virtual {v0, v2, p3}, Ljava/util/Calendar;->set(II)V

    .line 117
    const/16 v2, 0xb

    invoke-virtual {v0, v2, p4}, Ljava/util/Calendar;->set(II)V

    .line 118
    const/16 v2, 0xc

    invoke-virtual {v0, v2, p5}, Ljava/util/Calendar;->set(II)V

    .line 119
    const/16 v2, 0xd

    invoke-virtual {v0, v2, p6}, Ljava/util/Calendar;->set(II)V

    .line 120
    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v3, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Landroid/app/enterprise/IDateTimePolicy;->setDateTime(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 125
    .end local v0    # "c":Ljava/util/Calendar;
    :goto_0
    return v2

    .line 122
    :catch_0
    move-exception v1

    .line 123
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed at DateTime policy API setDateTime"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 125
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDateTimeChangeEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 399
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DateTimePolicy.setDateTimeChangeEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 401
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 402
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDateTimePolicy;->setDateTimeChangeEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 407
    :goto_0
    return v1

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setTimeFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 407
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTimeFormat(Ljava/lang/String;)Z
    .locals 3
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 219
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DateTimePolicy.setTimeFormat"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 221
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDateTimePolicy;->setTimeFormat(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 227
    :goto_0
    return v1

    .line 224
    :catch_0
    move-exception v0

    .line 225
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setTimeFormat"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 227
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setTimeZone(Ljava/lang/String;)Z
    .locals 3
    .param p1, "timeZoneID"    # Ljava/lang/String;

    .prologue
    .line 171
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "DateTimePolicy.setTimeZone"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 173
    :try_start_0
    invoke-direct {p0}, Landroid/app/enterprise/DateTimePolicy;->getService()Landroid/app/enterprise/IDateTimePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Landroid/app/enterprise/DateTimePolicy;->mService:Landroid/app/enterprise/IDateTimePolicy;

    iget-object v2, p0, Landroid/app/enterprise/DateTimePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IDateTimePolicy;->setTimeZone(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 179
    :goto_0
    return v1

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/DateTimePolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at DateTime policy API setTimeZone"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 179
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
