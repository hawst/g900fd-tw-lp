.class public Landroid/app/enterprise/EmailPolicy;
.super Ljava/lang/Object;
.source "EmailPolicy.java"


# static fields
.field private static TAG:Ljava/lang/String;


# instance fields
.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IEmailPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "EmailPolicy"

    sput-object v0, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 56
    return-void
.end method

.method private getService()Landroid/app/enterprise/IEmailPolicy;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    if-nez v0, :cond_0

    .line 60
    const-string v0, "email_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEmailPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEmailPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    .line 63
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    return-object v0
.end method


# virtual methods
.method public allowAccountAddition(Z)Z
    .locals 3
    .param p1, "allowed"    # Z

    .prologue
    .line 87
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.allowAccountAddition"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 88
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 90
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailPolicy;->allowAccountAddition(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 96
    :goto_0
    return v1

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 96
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowEmailSettingsChange(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 279
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailPolicy.allowEmailSettingsChange"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 280
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 282
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailPolicy;->allowEmailSettingsChange(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 287
    :goto_0
    return v1

    .line 283
    :catch_0
    move-exception v0

    .line 284
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 287
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowPopImapEmail(Z)Z
    .locals 3
    .param p1, "allowed"    # Z

    .prologue
    .line 145
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.allowPopImapEmail"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 146
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 148
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailPolicy;->allowPopImapEmail(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 154
    :goto_0
    return v1

    .line 149
    :catch_0
    move-exception v0

    .line 150
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 154
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllowEmailForwarding(Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 206
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailPolicy;->getAllowEmailForwarding(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 211
    :goto_0
    return v1

    .line 207
    :catch_0
    move-exception v0

    .line 208
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 211
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getAllowHtmlEmail(Ljava/lang/String;)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;

    .prologue
    .line 236
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 238
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailPolicy;->getAllowHTMLEmail(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 243
    :goto_0
    return v1

    .line 239
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 243
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isAccountAdditionAllowed()Z
    .locals 3

    .prologue
    .line 114
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 116
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEmailPolicy;->isAccountAdditionAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 122
    :goto_0
    return v1

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 122
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEmailNotificationsEnabled(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 264
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 266
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailPolicy;->isEmailNotificationsEnabled(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 271
    :goto_0
    return v1

    .line 267
    :catch_0
    move-exception v0

    .line 268
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 271
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isEmailSettingsChangeAllowed(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 296
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 298
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailPolicy;->isEmailSettingsChangeAllowed(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 303
    :goto_0
    return v1

    .line 299
    :catch_0
    move-exception v0

    .line 300
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 303
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPopImapEmailAllowed()Z
    .locals 3

    .prologue
    .line 170
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.isPopImapEmailAllowed"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 171
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 173
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEmailPolicy;->isPopImapEmailAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 179
    :goto_0
    return v1

    .line 174
    :catch_0
    move-exception v0

    .line 175
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 179
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public setAllowEmailForwarding(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "allow"    # Z

    .prologue
    .line 187
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailPolicy.setAllowEmailForwarding"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 188
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 190
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailPolicy;->setAllowEmailForwarding(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 195
    :goto_0
    return v1

    .line 191
    :catch_0
    move-exception v0

    .line 192
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 195
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAllowHtmlEmail(Ljava/lang/String;Z)Z
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "allow"    # Z

    .prologue
    .line 219
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailPolicy.setAllowHTMLEmail"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 220
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 222
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailPolicy;->setAllowHTMLEmail(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 227
    :goto_0
    return v1

    .line 223
    :catch_0
    move-exception v0

    .line 224
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 227
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEmailNotificationsState(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 247
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailPolicy.setEmailNotificationsState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 248
    invoke-direct {p0}, Landroid/app/enterprise/EmailPolicy;->getService()Landroid/app/enterprise/IEmailPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 250
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailPolicy;->mService:Landroid/app/enterprise/IEmailPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailPolicy;->setEmailNotificationsState(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 255
    :goto_0
    return v1

    .line 251
    :catch_0
    move-exception v0

    .line 252
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 255
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
