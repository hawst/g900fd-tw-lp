.class public Landroid/app/enterprise/BluetoothPolicy;
.super Ljava/lang/Object;
.source "BluetoothPolicy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/BluetoothPolicy$BluetoothProfile;,
        Landroid/app/enterprise/BluetoothPolicy$BluetoothUUID;
    }
.end annotation


# static fields
.field public static final NO_PROFILE:I = -0x1

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IBluetoothPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    const-string v0, "BluetoothPolicy"

    sput-object v0, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 448
    iput-object p1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 449
    return-void
.end method

.method private convertBluetoothProfile(I)Ljava/lang/String;
    .locals 1
    .param p1, "profile"    # I

    .prologue
    .line 1070
    packed-switch p1, :pswitch_data_0

    .line 1086
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 1072
    :pswitch_1
    const-string v0, "Profile: A2DP\n"

    goto :goto_0

    .line 1074
    :pswitch_2
    const-string v0, "Profile: Headset and Handsfree\n"

    goto :goto_0

    .line 1076
    :pswitch_3
    const-string v0, "Profile: HEALTH\n"

    goto :goto_0

    .line 1078
    :pswitch_4
    const-string v0, "Profile: INPUT DEVICE\n"

    goto :goto_0

    .line 1080
    :pswitch_5
    const-string v0, "Profile: MAP\n"

    goto :goto_0

    .line 1082
    :pswitch_6
    const-string v0, "Profile: PAN\n"

    goto :goto_0

    .line 1084
    :pswitch_7
    const-string v0, "Profile: PBAP\n"

    goto :goto_0

    .line 1070
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private getService()Landroid/app/enterprise/IBluetoothPolicy;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    if-nez v0, :cond_0

    .line 453
    const-string v0, "bluetooth_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IBluetoothPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    .line 456
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    return-object v0
.end method


# virtual methods
.method public activateBluetoothDeviceRestriction(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 2233
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.activateBluetoothDeviceRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2234
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2236
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->activateBluetoothDeviceRestriction(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2241
    :goto_0
    return v1

    .line 2237
    :catch_0
    move-exception v0

    .line 2238
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2241
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public activateBluetoothUUIDRestriction(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 1651
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.activateBluetoothUUIDRestriction"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1652
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1654
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->activateBluetoothUUIDRestriction(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1659
    :goto_0
    return v1

    .line 1655
    :catch_0
    move-exception v0

    .line 1656
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1659
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBluetoothDevicesToBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1742
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.addBluetoothDevicesToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1743
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1745
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->addBluetoothDevicesToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1750
    :goto_0
    return v1

    .line 1746
    :catch_0
    move-exception v0

    .line 1747
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1750
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBluetoothDevicesToWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1970
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.addBluetoothDevicesToWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1971
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1973
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->addBluetoothDevicesToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1978
    :goto_0
    return v1

    .line 1974
    :catch_0
    move-exception v0

    .line 1975
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1978
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBluetoothDevicesToWhiteList(Ljava/util/List;Z)Z
    .locals 4
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 2003
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "BluetoothPolicy.addBluetoothDevicesToWhiteList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2004
    const/4 v0, 0x1

    .line 2006
    .local v0, "retBlackList":Z
    if-eqz p2, :cond_0

    .line 2007
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2008
    .local v1, "wildCard":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "*"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2009
    invoke-virtual {p0, v1}, Landroid/app/enterprise/BluetoothPolicy;->addBluetoothDevicesToBlackList(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2010
    const/4 v0, 0x0

    .line 2011
    sget-object v2, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to update WildCard"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2015
    .end local v1    # "wildCard":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/enterprise/BluetoothPolicy;->addBluetoothDevicesToWhiteList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public addBluetoothUUIDsToBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1140
    .local p1, "UUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.addBluetoothUUIDsToBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1141
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1143
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->addBluetoothUUIDsToBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1148
    :goto_0
    return v1

    .line 1144
    :catch_0
    move-exception v0

    .line 1145
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1148
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBluetoothUUIDsToWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1379
    .local p1, "UUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.addBluetoothUUIDsToWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1380
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1382
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->addBluetoothUUIDsToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1387
    :goto_0
    return v1

    .line 1383
    :catch_0
    move-exception v0

    .line 1384
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1387
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public addBluetoothUUIDsToWhiteList(Ljava/util/List;Z)Z
    .locals 4
    .param p2, "defaultBlackList"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)Z"
        }
    .end annotation

    .prologue
    .line 1414
    .local p1, "UUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "BluetoothPolicy.addBluetoothUUIDsToWhiteList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1415
    const/4 v0, 0x1

    .line 1417
    .local v0, "retBlackList":Z
    if-eqz p2, :cond_0

    .line 1418
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1419
    .local v1, "wildCard":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v2, "*"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1420
    invoke-virtual {p0, v1}, Landroid/app/enterprise/BluetoothPolicy;->addBluetoothUUIDsToBlackList(Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1421
    const/4 v0, 0x0

    .line 1422
    sget-object v2, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed to update wildCard"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1426
    .end local v1    # "wildCard":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-virtual {p0, p1}, Landroid/app/enterprise/BluetoothPolicy;->addBluetoothUUIDsToWhiteList(Ljava/util/List;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public allowBluetooth(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 576
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 578
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->allowBluetooth(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 583
    :goto_0
    return v1

    .line 579
    :catch_0
    move-exception v0

    .line 580
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 583
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowCallerIDDisplay(Z)Z
    .locals 3
    .param p1, "allow"    # Z

    .prologue
    .line 2374
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.allowCallerIDDisplay"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2375
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2377
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->allowCallerIDDisplay(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2382
    :goto_0
    return v1

    .line 2378
    :catch_0
    move-exception v0

    .line 2379
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed to block caller id display "

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2382
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public allowOutgoingCalls(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 648
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.allowOutgoingCalls"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 649
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 651
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->allowOutgoingCalls(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 656
    :goto_0
    return v1

    .line 652
    :catch_0
    move-exception v0

    .line 653
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 656
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bluetoothLog(Ljava/lang/String;ILandroid/bluetooth/BluetoothDevice;)V
    .locals 8
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "profile"    # I
    .param p3, "device"    # Landroid/bluetooth/BluetoothDevice;

    .prologue
    const/16 v7, 0xa

    .line 1027
    const-string v2, ""

    .line 1028
    .local v2, "localName":Ljava/lang/String;
    const-string v1, ""

    .line 1029
    .local v1, "localAddress":Ljava/lang/String;
    const-string v5, ""

    .line 1030
    .local v5, "remoteName":Ljava/lang/String;
    const-string v4, ""

    .line 1031
    .local v4, "remoteAddress":Ljava/lang/String;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 1032
    .local v0, "adapter":Landroid/bluetooth/BluetoothAdapter;
    if-eqz v0, :cond_0

    .line 1033
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    move-result-object v2

    .line 1034
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 1036
    :cond_0
    if-eqz p3, :cond_1

    .line 1037
    invoke-virtual {p3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v5

    .line 1038
    invoke-virtual {p3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    .line 1041
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, ""

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1042
    .local v3, "logMsg":Ljava/lang/StringBuilder;
    const/4 v6, -0x1

    if-eq p2, v6, :cond_2

    .line 1043
    invoke-direct {p0, p2}, Landroid/app/enterprise/BluetoothPolicy;->convertBluetoothProfile(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1045
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 1046
    const-string v6, "Remote Address: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1047
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1048
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1050
    :cond_3
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_4

    .line 1051
    const-string v6, "Remote Name: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1052
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1053
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1055
    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_5

    .line 1056
    const-string v6, "Local Address: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1057
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1058
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1060
    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_6

    .line 1061
    const-string v6, "Local Name: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1062
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1063
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1066
    :cond_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, p1, v6}, Landroid/app/enterprise/BluetoothPolicy;->bluetoothLog(Ljava/lang/String;Ljava/lang/String;)V

    .line 1067
    return-void
.end method

.method public bluetoothLog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "msg"    # Ljava/lang/String;

    .prologue
    .line 1014
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1016
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IBluetoothPolicy;->bluetoothLog(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1021
    :cond_0
    :goto_0
    return-void

    .line 1017
    :catch_0
    move-exception v0

    .line 1018
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public clearBluetoothDevicesFromBlackList()Z
    .locals 3

    .prologue
    .line 1851
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.clearBluetoothDevicesFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1852
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1854
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->clearBluetoothDevicesFromBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1859
    :goto_0
    return v1

    .line 1855
    :catch_0
    move-exception v0

    .line 1856
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1859
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearBluetoothDevicesFromList()Z
    .locals 4

    .prologue
    .line 2035
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "BluetoothPolicy.clearBluetoothDevicesFromList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2036
    invoke-virtual {p0}, Landroid/app/enterprise/BluetoothPolicy;->clearBluetoothDevicesFromBlackList()Z

    move-result v0

    .line 2037
    .local v0, "retCBlackList":Z
    invoke-virtual {p0}, Landroid/app/enterprise/BluetoothPolicy;->clearBluetoothDevicesFromWhiteList()Z

    move-result v1

    .line 2038
    .local v1, "retCWhiteList":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearBluetoothDevicesFromWhiteList()Z
    .locals 3

    .prologue
    .line 2139
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.clearBluetoothDevicesFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2140
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2142
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->clearBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2147
    :goto_0
    return v1

    .line 2143
    :catch_0
    move-exception v0

    .line 2144
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2147
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearBluetoothUUIDsFromBlackList()Z
    .locals 3

    .prologue
    .line 1255
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.clearBluetoothUUIDsFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1256
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1258
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->clearBluetoothUUIDsFromBlackList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1263
    :goto_0
    return v1

    .line 1259
    :catch_0
    move-exception v0

    .line 1260
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1263
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public clearBluetoothUUIDsFromList()Z
    .locals 4

    .prologue
    .line 1447
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "BluetoothPolicy.clearBluetoothUUIDsFromList"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1448
    invoke-virtual {p0}, Landroid/app/enterprise/BluetoothPolicy;->clearBluetoothUUIDsFromBlackList()Z

    move-result v0

    .line 1449
    .local v0, "retCBlackList":Z
    invoke-virtual {p0}, Landroid/app/enterprise/BluetoothPolicy;->clearBluetoothUUIDsFromWhiteList()Z

    move-result v1

    .line 1450
    .local v1, "retCWhiteList":Z
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public clearBluetoothUUIDsFromWhiteList()Z
    .locals 3

    .prologue
    .line 1555
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.clearBluetoothUUIDsFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1556
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1558
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->clearBluetoothUUIDsFromWhiteList(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1563
    :goto_0
    return v1

    .line 1559
    :catch_0
    move-exception v0

    .line 1560
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1563
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAllowBluetoothDataTransfer()Z
    .locals 4

    .prologue
    .line 493
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 495
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/app/enterprise/IBluetoothPolicy;->getAllowBluetoothDataTransfer(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 500
    :goto_0
    return v1

    .line 496
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 500
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getBluetoothDevicesFromBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1884
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.getBluetoothDevicesFromBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1885
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1887
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getAllBluetoothDevicesBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1892
    :goto_0
    return-object v1

    .line 1888
    :catch_0
    move-exception v0

    .line 1889
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1892
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getBluetoothDevicesFromWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2172
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.getBluetoothDevicesFromWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2173
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2175
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getAllBluetoothDevicesWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2180
    :goto_0
    return-object v1

    .line 2176
    :catch_0
    move-exception v0

    .line 2177
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2180
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getBluetoothLog()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 997
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.getBluetoothLog"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 998
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1000
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getBluetoothLog(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1005
    :goto_0
    return-object v1

    .line 1001
    :catch_0
    move-exception v0

    .line 1002
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1005
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBluetoothUUIDsFromBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1287
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.getBluetoothUUIDsFromBlackLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1288
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1290
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getAllBluetoothUUIDsBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1295
    :goto_0
    return-object v1

    .line 1291
    :catch_0
    move-exception v0

    .line 1292
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1295
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getBluetoothUUIDsFromWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothControlInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1588
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.getBluetoothUUIDsFromWhiteLists"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1589
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1591
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getAllBluetoothUUIDsWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1596
    :goto_0
    return-object v1

    .line 1592
    :catch_0
    move-exception v0

    .line 1593
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1596
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getEffectiveBluetoothDevicesBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1912
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1914
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getEffectiveBluetoothDevicesBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1919
    :goto_0
    return-object v1

    .line 1915
    :catch_0
    move-exception v0

    .line 1916
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1919
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getEffectiveBluetoothDevicesWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2200
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2202
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getEffectiveBluetoothDevicesWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2207
    :goto_0
    return-object v1

    .line 2203
    :catch_0
    move-exception v0

    .line 2204
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2207
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getEffectiveBluetoothUUIDsBlackLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1317
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1319
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getEffectiveBluetoothUUIDsBlackLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1324
    :goto_0
    return-object v1

    .line 1320
    :catch_0
    move-exception v0

    .line 1321
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1324
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getEffectiveBluetoothUUIDsWhiteLists()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1618
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1620
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->getEffectiveBluetoothUUIDsWhiteLists(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1625
    :goto_0
    return-object v1

    .line 1621
    :catch_0
    move-exception v0

    .line 1622
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1625
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public isBluetoothDeviceAllowed(Ljava/lang/String;)Z
    .locals 3
    .param p1, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 2328
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2330
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothDeviceAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2335
    :goto_0
    return v1

    .line 2331
    :catch_0
    move-exception v0

    .line 2332
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2335
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBluetoothDeviceRestrictionActive()Z
    .locals 3

    .prologue
    .line 2265
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.isBluetoothDeviceRestrictionActive"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2266
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2268
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothDeviceRestrictionActive(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2273
    :goto_0
    return v1

    .line 2269
    :catch_0
    move-exception v0

    .line 2270
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2273
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBluetoothEnabled()Z
    .locals 4

    .prologue
    .line 532
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v2

    if-nez v2, :cond_0

    .line 533
    const/4 v1, 0x1

    .line 543
    :goto_0
    return v1

    .line 535
    :cond_0
    const/4 v1, 0x1

    .line 538
    .local v1, "enabled":Z
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v3, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 540
    :catch_0
    move-exception v0

    .line 541
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    .line 543
    goto :goto_0
.end method

.method public isBluetoothEnabled(Z)Z
    .locals 3
    .param p1, "showMsg"    # Z

    .prologue
    .line 552
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v2

    if-nez v2, :cond_0

    .line 553
    const/4 v1, 0x1

    .line 563
    :goto_0
    return v1

    .line 555
    :cond_0
    const/4 v1, 0x1

    .line 558
    .local v1, "enabled":Z
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    invoke-interface {v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothEnabledWithMsg(Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x1

    .line 563
    goto :goto_0
.end method

.method public isBluetoothLogEnabled()Z
    .locals 3

    .prologue
    .line 982
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 984
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothLogEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 989
    :goto_0
    return v1

    .line 985
    :catch_0
    move-exception v0

    .line 986
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 989
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isBluetoothUUIDAllowed(Ljava/lang/String;)Z
    .locals 3
    .param p1, "uuid"    # Ljava/lang/String;

    .prologue
    .line 2297
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2299
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothUUIDAllowed(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2304
    :goto_0
    return v1

    .line 2300
    :catch_0
    move-exception v0

    .line 2301
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2304
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isBluetoothUUIDRestrictionActive()Z
    .locals 3

    .prologue
    .line 1684
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.isBluetoothUUIDRestrictionActive"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1685
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1687
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isBluetoothUUIDRestrictionActive(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1692
    :goto_0
    return v1

    .line 1688
    :catch_0
    move-exception v0

    .line 1689
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1692
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isCallerIDDisplayAllowed()Z
    .locals 3

    .prologue
    .line 2416
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2418
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isCallerIDDisplayAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2423
    :goto_0
    return v1

    .line 2419
    :catch_0
    move-exception v0

    .line 2420
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed getting caller id display status"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2423
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isDesktopConnectivityEnabled()Z
    .locals 3

    .prologue
    .line 949
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 951
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isDesktopConnectivityEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 956
    :goto_0
    return v1

    .line 952
    :catch_0
    move-exception v0

    .line 953
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 956
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isDiscoverableEnabled()Z
    .locals 3

    .prologue
    .line 903
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 905
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isDiscoverableEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 910
    :goto_0
    return v1

    .line 906
    :catch_0
    move-exception v0

    .line 907
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 910
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isLimitedDiscoverableEnabled()Z
    .locals 3

    .prologue
    .line 716
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 718
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isLimitedDiscoverableEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 723
    :goto_0
    return v1

    .line 719
    :catch_0
    move-exception v0

    .line 720
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 723
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isOutgoingCallsAllowed()Z
    .locals 3

    .prologue
    .line 669
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 671
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isOutgoingCallsAllowed(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 676
    :goto_0
    return v1

    .line 672
    :catch_0
    move-exception v0

    .line 673
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 676
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isPairingEnabled()Z
    .locals 3

    .prologue
    .line 624
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 626
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothPolicy;->isPairingEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 631
    :goto_0
    return v1

    .line 627
    :catch_0
    move-exception v0

    .line 628
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 631
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isProfileEnabled(I)Z
    .locals 3
    .param p1, "profile"    # I

    .prologue
    .line 858
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 860
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->isProfileEnabled(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 865
    :goto_0
    return v1

    .line 861
    :catch_0
    move-exception v0

    .line 862
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 865
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isRequiredPasswordForDiscovery()Z
    .locals 1

    .prologue
    .line 768
    const/4 v0, 0x0

    return v0
.end method

.method public isRequiredPasswordForEnable()Z
    .locals 1

    .prologue
    .line 814
    const/4 v0, 0x0

    return v0
.end method

.method public removeBluetoothDevicesFromBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1799
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.removeBluetoothDevicesFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1800
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1802
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->removeBluetoothDevicesFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1807
    :goto_0
    return v1

    .line 1803
    :catch_0
    move-exception v0

    .line 1804
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1807
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeBluetoothDevicesFromWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2087
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.removeBluetoothDevicesFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2088
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2090
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->removeBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2095
    :goto_0
    return v1

    .line 2091
    :catch_0
    move-exception v0

    .line 2092
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2095
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeBluetoothUUIDsFromBlackList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1202
    .local p1, "UUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.removeBluetoothUUIDsFromBlackList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1203
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1205
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->removeBluetoothUUIDsFromBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1210
    :goto_0
    return v1

    .line 1206
    :catch_0
    move-exception v0

    .line 1207
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1210
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeBluetoothUUIDsFromWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1502
    .local p1, "UUIDs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.removeBluetoothUUIDsFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1503
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1505
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->removeBluetoothUUIDsFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1510
    :goto_0
    return v1

    .line 1506
    :catch_0
    move-exception v0

    .line 1507
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1510
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAllowBluetoothDataTransfer(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 473
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setAllowBluetoothDataTransfer"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 474
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 476
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setAllowBluetoothDataTransfer(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 481
    :goto_0
    return v1

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 481
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBluetoothLogEnabled(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 965
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setBluetoothLogEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 966
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 968
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setBluetoothLogEnabled(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 973
    :goto_0
    return v1

    .line 969
    :catch_0
    move-exception v0

    .line 970
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 973
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setBluetoothState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 514
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 516
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setBluetooth(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 521
    :goto_0
    return v1

    .line 517
    :catch_0
    move-exception v0

    .line 518
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with misc info policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 521
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDesktopConnectivityState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 928
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setDesktopConnectivityState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 929
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 931
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setDesktopConnectivityState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 936
    :goto_0
    return v1

    .line 932
    :catch_0
    move-exception v0

    .line 933
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 936
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setDiscoverableState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 883
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setDiscoverableState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 884
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 886
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setDiscoverableState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 891
    :goto_0
    return v1

    .line 887
    :catch_0
    move-exception v0

    .line 888
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 891
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLimitedDiscoverableState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 695
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setLimitedDiscoverableState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 696
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 698
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setLimitedDiscoverableState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 703
    :goto_0
    return v1

    .line 699
    :catch_0
    move-exception v0

    .line 700
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 703
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setPairingState(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 603
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setPairingState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 604
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 606
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothPolicy;->setPairingState(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 611
    :goto_0
    return v1

    .line 607
    :catch_0
    move-exception v0

    .line 608
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 611
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setProfileState(ZI)Z
    .locals 3
    .param p1, "enable"    # Z
    .param p2, "profile"    # I

    .prologue
    .line 835
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothPolicy.setProfileState"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 836
    invoke-direct {p0}, Landroid/app/enterprise/BluetoothPolicy;->getService()Landroid/app/enterprise/IBluetoothPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 838
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothPolicy;->mService:Landroid/app/enterprise/IBluetoothPolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IBluetoothPolicy;->setProfileState(Landroid/app/enterprise/ContextInfo;ZI)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 843
    :goto_0
    return v1

    .line 839
    :catch_0
    move-exception v0

    .line 840
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/BluetoothPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with bluetooth policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 843
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setRequiredPasswordForDiscovery(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 748
    const/4 v0, 0x0

    return v0
.end method

.method public setRequiredPasswordForEnable(Z)Z
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 794
    const/4 v0, 0x0

    return v0
.end method
