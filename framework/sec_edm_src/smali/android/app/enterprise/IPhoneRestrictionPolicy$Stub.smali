.class public abstract Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;
.super Landroid/os/Binder;
.source "IPhoneRestrictionPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IPhoneRestrictionPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IPhoneRestrictionPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IPhoneRestrictionPolicy"

.field static final TRANSACTION_addIncomingCallExceptionPattern:I = 0x51

.field static final TRANSACTION_addIncomingCallRestriction:I = 0x6

.field static final TRANSACTION_addIncomingSmsExceptionPattern:I = 0x59

.field static final TRANSACTION_addIncomingSmsRestriction:I = 0x1b

.field static final TRANSACTION_addNumberOfIncomingCalls:I = 0x13

.field static final TRANSACTION_addNumberOfIncomingSms:I = 0x27

.field static final TRANSACTION_addNumberOfOutgoingCalls:I = 0x14

.field static final TRANSACTION_addNumberOfOutgoingSms:I = 0x28

.field static final TRANSACTION_addOutgoingCallExceptionPattern:I = 0x50

.field static final TRANSACTION_addOutgoingCallRestriction:I = 0x5

.field static final TRANSACTION_addOutgoingSmsExceptionPattern:I = 0x58

.field static final TRANSACTION_addOutgoingSmsRestriction:I = 0x1a

.field static final TRANSACTION_allowCallerIDDisplay:I = 0x44

.field static final TRANSACTION_allowCopyContactToSim:I = 0x4a

.field static final TRANSACTION_allowIncomingMms:I = 0x37

.field static final TRANSACTION_allowIncomingSms:I = 0x33

.field static final TRANSACTION_allowOutgoingMms:I = 0x38

.field static final TRANSACTION_allowOutgoingSms:I = 0x34

.field static final TRANSACTION_allowWapPush:I = 0x42

.field static final TRANSACTION_blockMmsWithStorage:I = 0x3d

.field static final TRANSACTION_blockSmsWithStorage:I = 0x3b

.field static final TRANSACTION_canIncomingCall:I = 0xa

.field static final TRANSACTION_canIncomingSms:I = 0x1f

.field static final TRANSACTION_canOutgoingCall:I = 0x9

.field static final TRANSACTION_canOutgoingSms:I = 0x1e

.field static final TRANSACTION_changeSimPinCode:I = 0x47

.field static final TRANSACTION_checkDataCallLimit:I = 0x30

.field static final TRANSACTION_checkEnableUseOfPacketData:I = 0x2f

.field static final TRANSACTION_clearStoredBlockedMms:I = 0x41

.field static final TRANSACTION_clearStoredBlockedSms:I = 0x40

.field static final TRANSACTION_decreaseNumberOfOutgoingSms:I = 0x29

.field static final TRANSACTION_enableLimitNumberOfCalls:I = 0xd

.field static final TRANSACTION_enableLimitNumberOfSms:I = 0x20

.field static final TRANSACTION_getDataCallLimitEnabled:I = 0x2b

.field static final TRANSACTION_getEmergencyCallOnly:I = 0xc

.field static final TRANSACTION_getIncomingCallExceptionPatterns:I = 0x4d

.field static final TRANSACTION_getIncomingCallRestriction:I = 0x2

.field static final TRANSACTION_getIncomingSmsExceptionPatterns:I = 0x55

.field static final TRANSACTION_getIncomingSmsRestriction:I = 0x17

.field static final TRANSACTION_getLimitOfDataCalls:I = 0x2d

.field static final TRANSACTION_getLimitOfIncomingCalls:I = 0x10

.field static final TRANSACTION_getLimitOfIncomingSms:I = 0x24

.field static final TRANSACTION_getLimitOfOutgoingCalls:I = 0x12

.field static final TRANSACTION_getLimitOfOutgoingSms:I = 0x26

.field static final TRANSACTION_getOutgoingCallExceptionPatterns:I = 0x4c

.field static final TRANSACTION_getOutgoingCallRestriction:I = 0x1

.field static final TRANSACTION_getOutgoingSmsExceptionPatterns:I = 0x54

.field static final TRANSACTION_getOutgoingSmsRestriction:I = 0x16

.field static final TRANSACTION_getPinCode:I = 0x49

.field static final TRANSACTION_isBlockMmsWithStorageEnabled:I = 0x3e

.field static final TRANSACTION_isBlockSmsWithStorageEnabled:I = 0x3c

.field static final TRANSACTION_isCallerIDDisplayAllowed:I = 0x45

.field static final TRANSACTION_isCopyContactToSimAllowed:I = 0x4b

.field static final TRANSACTION_isIncomingMmsAllowed:I = 0x39

.field static final TRANSACTION_isIncomingSmsAllowed:I = 0x35

.field static final TRANSACTION_isLimitNumberOfCallsEnabled:I = 0xe

.field static final TRANSACTION_isLimitNumberOfSmsEnabled:I = 0x21

.field static final TRANSACTION_isOutgoingMmsAllowed:I = 0x3a

.field static final TRANSACTION_isOutgoingSmsAllowed:I = 0x36

.field static final TRANSACTION_isSimLockedByAdmin:I = 0x48

.field static final TRANSACTION_isWapPushAllowed:I = 0x43

.field static final TRANSACTION_lockUnlockCorporateSimCard:I = 0x46

.field static final TRANSACTION_removeIncomingCallExceptionPattern:I = 0x4f

.field static final TRANSACTION_removeIncomingCallRestriction:I = 0x4

.field static final TRANSACTION_removeIncomingSmsExceptionPattern:I = 0x57

.field static final TRANSACTION_removeIncomingSmsRestriction:I = 0x19

.field static final TRANSACTION_removeOutgoingCallExceptionPattern:I = 0x4e

.field static final TRANSACTION_removeOutgoingCallRestriction:I = 0x3

.field static final TRANSACTION_removeOutgoingSmsExceptionPattern:I = 0x56

.field static final TRANSACTION_removeOutgoingSmsRestriction:I = 0x18

.field static final TRANSACTION_resetCallsCount:I = 0x15

.field static final TRANSACTION_resetDataCallLimitCounter:I = 0x2e

.field static final TRANSACTION_resetSmsCount:I = 0x22

.field static final TRANSACTION_setDataCallLimitEnabled:I = 0x2a

.field static final TRANSACTION_setEmergencyCallOnly:I = 0xb

.field static final TRANSACTION_setIncomingCallExceptionPattern:I = 0x53

.field static final TRANSACTION_setIncomingCallRestriction:I = 0x8

.field static final TRANSACTION_setIncomingSmsExceptionPattern:I = 0x5b

.field static final TRANSACTION_setIncomingSmsRestriction:I = 0x1d

.field static final TRANSACTION_setLimitOfDataCalls:I = 0x2c

.field static final TRANSACTION_setLimitOfIncomingCalls:I = 0xf

.field static final TRANSACTION_setLimitOfIncomingSms:I = 0x23

.field static final TRANSACTION_setLimitOfOutgoingCalls:I = 0x11

.field static final TRANSACTION_setLimitOfOutgoingSms:I = 0x25

.field static final TRANSACTION_setOutgoingCallExceptionPattern:I = 0x52

.field static final TRANSACTION_setOutgoingCallRestriction:I = 0x7

.field static final TRANSACTION_setOutgoingSmsExceptionPattern:I = 0x5a

.field static final TRANSACTION_setOutgoingSmsRestriction:I = 0x1c

.field static final TRANSACTION_storeBlockedSmsMms:I = 0x3f

.field static final TRANSACTION_updateDataLimitState:I = 0x32

.field static final TRANSACTION_updateDateAndDataCallCounters:I = 0x31


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "android.app.enterprise.IPhoneRestrictionPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IPhoneRestrictionPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "android.app.enterprise.IPhoneRestrictionPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IPhoneRestrictionPolicy;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Landroid/app/enterprise/IPhoneRestrictionPolicy;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 22
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 1438
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 42
    :sswitch_0
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 43
    const/4 v4, 0x1

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 50
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 56
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    const/4 v6, 0x1

    .line 57
    .local v6, "_arg1":Z
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;

    move-result-object v20

    .line 58
    .local v20, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 59
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 60
    const/4 v4, 0x1

    goto :goto_0

    .line 53
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Ljava/lang/String;
    :cond_0
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 56
    :cond_1
    const/4 v6, 0x0

    goto :goto_2

    .line 64
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    .line 67
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 73
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3

    const/4 v6, 0x1

    .line 74
    .restart local v6    # "_arg1":Z
    :goto_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;

    move-result-object v20

    .line 75
    .restart local v20    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 76
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 77
    const/4 v4, 0x1

    goto :goto_0

    .line 70
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Ljava/lang/String;
    :cond_2
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 73
    :cond_3
    const/4 v6, 0x0

    goto :goto_4

    .line 81
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 83
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    .line 84
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 89
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 90
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 91
    if-eqz v20, :cond_5

    const/4 v4, 0x1

    :goto_6
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 92
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 87
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_4
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 91
    .restart local v20    # "_result":Z
    :cond_5
    const/4 v4, 0x0

    goto :goto_6

    .line 96
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_4
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6

    .line 99
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 104
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 105
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 106
    if-eqz v20, :cond_7

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 102
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_6
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 106
    .restart local v20    # "_result":Z
    :cond_7
    const/4 v4, 0x0

    goto :goto_8

    .line 111
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_5
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8

    .line 114
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 120
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 121
    .local v6, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 122
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 123
    if-eqz v20, :cond_9

    const/4 v4, 0x1

    :goto_a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 117
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_8
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 123
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_9
    const/4 v4, 0x0

    goto :goto_a

    .line 128
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_6
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a

    .line 131
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 137
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 138
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 139
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 140
    if-eqz v20, :cond_b

    const/4 v4, 0x1

    :goto_c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 141
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 134
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 140
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_b
    const/4 v4, 0x0

    goto :goto_c

    .line 145
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_7
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 147
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_c

    .line 148
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 154
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 155
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setOutgoingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 156
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 157
    if-eqz v20, :cond_d

    const/4 v4, 0x1

    :goto_e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 158
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 151
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 157
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_d
    const/4 v4, 0x0

    goto :goto_e

    .line 162
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_8
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_e

    .line 165
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 171
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 172
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setIncomingCallRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 173
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 174
    if-eqz v20, :cond_f

    const/4 v4, 0x1

    :goto_10
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 175
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 168
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 174
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_f
    const/4 v4, 0x0

    goto :goto_10

    .line 179
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_9
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 181
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 182
    .local v5, "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->canOutgoingCall(Ljava/lang/String;)Z

    move-result v20

    .line 183
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 184
    if-eqz v20, :cond_10

    const/4 v4, 0x1

    :goto_11
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 185
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 184
    :cond_10
    const/4 v4, 0x0

    goto :goto_11

    .line 189
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 192
    .restart local v5    # "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->canIncomingCall(Ljava/lang/String;)Z

    move-result v20

    .line 193
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 194
    if-eqz v20, :cond_11

    const/4 v4, 0x1

    :goto_12
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 195
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 194
    :cond_11
    const/4 v4, 0x0

    goto :goto_12

    .line 199
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 201
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_12

    .line 202
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 208
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_13

    const/4 v6, 0x1

    .line 209
    .local v6, "_arg1":Z
    :goto_14
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setEmergencyCallOnly(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 210
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 211
    if-eqz v20, :cond_14

    const/4 v4, 0x1

    :goto_15
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 212
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 205
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_12
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 208
    :cond_13
    const/4 v6, 0x0

    goto :goto_14

    .line 211
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_14
    const/4 v4, 0x0

    goto :goto_15

    .line 216
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_c
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 218
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_15

    .line 219
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 225
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_16

    const/4 v6, 0x1

    .line 226
    .restart local v6    # "_arg1":Z
    :goto_17
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getEmergencyCallOnly(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 227
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 228
    if-eqz v20, :cond_17

    const/4 v4, 0x1

    :goto_18
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 229
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 222
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_15
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 225
    :cond_16
    const/4 v6, 0x0

    goto :goto_17

    .line 228
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_17
    const/4 v4, 0x0

    goto :goto_18

    .line 233
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_d
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 235
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_18

    .line 236
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 242
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_19

    const/4 v6, 0x1

    .line 243
    .restart local v6    # "_arg1":Z
    :goto_1a
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->enableLimitNumberOfCalls(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 244
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 245
    if-eqz v20, :cond_1a

    const/4 v4, 0x1

    :goto_1b
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 246
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 239
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_18
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    .line 242
    :cond_19
    const/4 v6, 0x0

    goto :goto_1a

    .line 245
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_1a
    const/4 v4, 0x0

    goto :goto_1b

    .line 250
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_e
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 252
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1b

    .line 253
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 258
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isLimitNumberOfCallsEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 259
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v20, :cond_1c

    const/4 v4, 0x1

    :goto_1d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 261
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 256
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_1b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 260
    .restart local v20    # "_result":Z
    :cond_1c
    const/4 v4, 0x0

    goto :goto_1d

    .line 265
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_f
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1d

    .line 268
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 274
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1e
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 276
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 278
    .local v8, "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 279
    .local v10, "_arg3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8, v10}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setLimitOfIncomingCalls(Landroid/app/enterprise/ContextInfo;III)Z

    move-result v20

    .line 280
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 281
    if-eqz v20, :cond_1e

    const/4 v4, 0x1

    :goto_1f
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 282
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 271
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :cond_1d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1e

    .line 281
    .restart local v6    # "_arg1":I
    .restart local v8    # "_arg2":I
    .restart local v10    # "_arg3":I
    .restart local v20    # "_result":Z
    :cond_1e
    const/4 v4, 0x0

    goto :goto_1f

    .line 286
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :sswitch_10
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 288
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1f

    .line 289
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 295
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_20
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 296
    .restart local v6    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getLimitOfIncomingCalls(Landroid/app/enterprise/ContextInfo;I)I

    move-result v20

    .line 297
    .local v20, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 298
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 299
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 292
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v20    # "_result":I
    :cond_1f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_20

    .line 303
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_20

    .line 306
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 312
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_21
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 314
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 316
    .restart local v8    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 317
    .restart local v10    # "_arg3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8, v10}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setLimitOfOutgoingCalls(Landroid/app/enterprise/ContextInfo;III)Z

    move-result v20

    .line 318
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 319
    if-eqz v20, :cond_21

    const/4 v4, 0x1

    :goto_22
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 320
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 309
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :cond_20
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_21

    .line 319
    .restart local v6    # "_arg1":I
    .restart local v8    # "_arg2":I
    .restart local v10    # "_arg3":I
    .restart local v20    # "_result":Z
    :cond_21
    const/4 v4, 0x0

    goto :goto_22

    .line 324
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :sswitch_12
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 326
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_22

    .line 327
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 333
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_23
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 334
    .restart local v6    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getLimitOfOutgoingCalls(Landroid/app/enterprise/ContextInfo;I)I

    move-result v20

    .line 335
    .local v20, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 336
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 337
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 330
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v20    # "_result":I
    :cond_22
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_23

    .line 341
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_13
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 342
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addNumberOfIncomingCalls()Z

    move-result v20

    .line 343
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 344
    if-eqz v20, :cond_23

    const/4 v4, 0x1

    :goto_24
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 345
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 344
    :cond_23
    const/4 v4, 0x0

    goto :goto_24

    .line 349
    .end local v20    # "_result":Z
    :sswitch_14
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 350
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addNumberOfOutgoingCalls()Z

    move-result v20

    .line 351
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 352
    if-eqz v20, :cond_24

    const/4 v4, 0x1

    :goto_25
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 353
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 352
    :cond_24
    const/4 v4, 0x0

    goto :goto_25

    .line 357
    .end local v20    # "_result":Z
    :sswitch_15
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 359
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_25

    .line 360
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 365
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_26
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->resetCallsCount(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 366
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 367
    if-eqz v20, :cond_26

    const/4 v4, 0x1

    :goto_27
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 368
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 363
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_25
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_26

    .line 367
    .restart local v20    # "_result":Z
    :cond_26
    const/4 v4, 0x0

    goto :goto_27

    .line 372
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_16
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 374
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_27

    .line 375
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 381
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_28

    const/4 v6, 0x1

    .line 382
    .local v6, "_arg1":Z
    :goto_29
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;

    move-result-object v20

    .line 383
    .local v20, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 384
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 385
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 378
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Ljava/lang/String;
    :cond_27
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 381
    :cond_28
    const/4 v6, 0x0

    goto :goto_29

    .line 389
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 391
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_29

    .line 392
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 398
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2a

    const/4 v6, 0x1

    .line 399
    .restart local v6    # "_arg1":Z
    :goto_2b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Z)Ljava/lang/String;

    move-result-object v20

    .line 400
    .restart local v20    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 401
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 402
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 395
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Ljava/lang/String;
    :cond_29
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2a

    .line 398
    :cond_2a
    const/4 v6, 0x0

    goto :goto_2b

    .line 406
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_18
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 408
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2b

    .line 409
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 414
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 415
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 416
    if-eqz v20, :cond_2c

    const/4 v4, 0x1

    :goto_2d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 417
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 412
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_2b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 416
    .restart local v20    # "_result":Z
    :cond_2c
    const/4 v4, 0x0

    goto :goto_2d

    .line 421
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_19
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 423
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2d

    .line 424
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 429
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2e
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 430
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 431
    if-eqz v20, :cond_2e

    const/4 v4, 0x1

    :goto_2f
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 432
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 427
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_2d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2e

    .line 431
    .restart local v20    # "_result":Z
    :cond_2e
    const/4 v4, 0x0

    goto :goto_2f

    .line 436
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_1a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 438
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2f

    .line 439
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 445
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_30
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 446
    .local v6, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 447
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 448
    if-eqz v20, :cond_30

    const/4 v4, 0x1

    :goto_31
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 449
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 442
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_2f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_30

    .line 448
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_30
    const/4 v4, 0x0

    goto :goto_31

    .line 453
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_1b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_31

    .line 456
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 462
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_32
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 463
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 464
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 465
    if-eqz v20, :cond_32

    const/4 v4, 0x1

    :goto_33
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 466
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 459
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_31
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_32

    .line 465
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_32
    const/4 v4, 0x0

    goto :goto_33

    .line 470
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_1c
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_33

    .line 473
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 479
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_34
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 480
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setOutgoingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 481
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 482
    if-eqz v20, :cond_34

    const/4 v4, 0x1

    :goto_35
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 483
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 476
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_33
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_34

    .line 482
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_34
    const/4 v4, 0x0

    goto :goto_35

    .line 487
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_1d
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 489
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_35

    .line 490
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 496
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_36
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 497
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setIncomingSmsRestriction(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 498
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 499
    if-eqz v20, :cond_36

    const/4 v4, 0x1

    :goto_37
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 500
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 493
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_35
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_36

    .line 499
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_36
    const/4 v4, 0x0

    goto :goto_37

    .line 504
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_1e
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 506
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 507
    .local v5, "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->canOutgoingSms(Ljava/lang/String;)Z

    move-result v20

    .line 508
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 509
    if-eqz v20, :cond_37

    const/4 v4, 0x1

    :goto_38
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 510
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 509
    :cond_37
    const/4 v4, 0x0

    goto :goto_38

    .line 514
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_1f
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 516
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 517
    .restart local v5    # "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->canIncomingSms(Ljava/lang/String;)Z

    move-result v20

    .line 518
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 519
    if-eqz v20, :cond_38

    const/4 v4, 0x1

    :goto_39
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 520
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 519
    :cond_38
    const/4 v4, 0x0

    goto :goto_39

    .line 524
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_20
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 526
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_39

    .line 527
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 533
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3a

    const/4 v6, 0x1

    .line 534
    .local v6, "_arg1":Z
    :goto_3b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->enableLimitNumberOfSms(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 535
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 536
    if-eqz v20, :cond_3b

    const/4 v4, 0x1

    :goto_3c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 537
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 530
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_39
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3a

    .line 533
    :cond_3a
    const/4 v6, 0x0

    goto :goto_3b

    .line 536
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_3b
    const/4 v4, 0x0

    goto :goto_3c

    .line 541
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_21
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 543
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3c

    .line 544
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 549
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isLimitNumberOfSmsEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 550
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 551
    if-eqz v20, :cond_3d

    const/4 v4, 0x1

    :goto_3e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 552
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 547
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_3c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3d

    .line 551
    .restart local v20    # "_result":Z
    :cond_3d
    const/4 v4, 0x0

    goto :goto_3e

    .line 556
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_22
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 558
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_3e

    .line 559
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 564
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->resetSmsCount(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 565
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 566
    if-eqz v20, :cond_3f

    const/4 v4, 0x1

    :goto_40
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 567
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 562
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_3e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3f

    .line 566
    .restart local v20    # "_result":Z
    :cond_3f
    const/4 v4, 0x0

    goto :goto_40

    .line 571
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_23
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 573
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_40

    .line 574
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 580
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_41
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 582
    .local v6, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 584
    .restart local v8    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 585
    .restart local v10    # "_arg3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8, v10}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setLimitOfIncomingSms(Landroid/app/enterprise/ContextInfo;III)Z

    move-result v20

    .line 586
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 587
    if-eqz v20, :cond_41

    const/4 v4, 0x1

    :goto_42
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 588
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 577
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :cond_40
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_41

    .line 587
    .restart local v6    # "_arg1":I
    .restart local v8    # "_arg2":I
    .restart local v10    # "_arg3":I
    .restart local v20    # "_result":Z
    :cond_41
    const/4 v4, 0x0

    goto :goto_42

    .line 592
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :sswitch_24
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 594
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_42

    .line 595
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 601
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_43
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 602
    .restart local v6    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getLimitOfIncomingSms(Landroid/app/enterprise/ContextInfo;I)I

    move-result v20

    .line 603
    .local v20, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 604
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 605
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 598
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v20    # "_result":I
    :cond_42
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_43

    .line 609
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_25
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 611
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_43

    .line 612
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 618
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_44
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 620
    .restart local v6    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    .line 622
    .restart local v8    # "_arg2":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 623
    .restart local v10    # "_arg3":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8, v10}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setLimitOfOutgoingSms(Landroid/app/enterprise/ContextInfo;III)Z

    move-result v20

    .line 624
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 625
    if-eqz v20, :cond_44

    const/4 v4, 0x1

    :goto_45
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 626
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 615
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :cond_43
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_44

    .line 625
    .restart local v6    # "_arg1":I
    .restart local v8    # "_arg2":I
    .restart local v10    # "_arg3":I
    .restart local v20    # "_result":Z
    :cond_44
    const/4 v4, 0x0

    goto :goto_45

    .line 630
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v8    # "_arg2":I
    .end local v10    # "_arg3":I
    .end local v20    # "_result":Z
    :sswitch_26
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 632
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_45

    .line 633
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 639
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_46
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 640
    .restart local v6    # "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getLimitOfOutgoingSms(Landroid/app/enterprise/ContextInfo;I)I

    move-result v20

    .line 641
    .local v20, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 642
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 643
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 636
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v20    # "_result":I
    :cond_45
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_46

    .line 647
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_27
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 648
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addNumberOfIncomingSms()Z

    move-result v20

    .line 649
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 650
    if-eqz v20, :cond_46

    const/4 v4, 0x1

    :goto_47
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 651
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 650
    :cond_46
    const/4 v4, 0x0

    goto :goto_47

    .line 655
    .end local v20    # "_result":Z
    :sswitch_28
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 656
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addNumberOfOutgoingSms()Z

    move-result v20

    .line 657
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 658
    if-eqz v20, :cond_47

    const/4 v4, 0x1

    :goto_48
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 659
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 658
    :cond_47
    const/4 v4, 0x0

    goto :goto_48

    .line 663
    .end local v20    # "_result":Z
    :sswitch_29
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 664
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->decreaseNumberOfOutgoingSms()Z

    move-result v20

    .line 665
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 666
    if-eqz v20, :cond_48

    const/4 v4, 0x1

    :goto_49
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 667
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 666
    :cond_48
    const/4 v4, 0x0

    goto :goto_49

    .line 671
    .end local v20    # "_result":Z
    :sswitch_2a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 673
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_49

    .line 674
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 680
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4a

    const/4 v6, 0x1

    .line 681
    .local v6, "_arg1":Z
    :goto_4b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setDataCallLimitEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 682
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 683
    if-eqz v20, :cond_4b

    const/4 v4, 0x1

    :goto_4c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 684
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 677
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_49
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4a

    .line 680
    :cond_4a
    const/4 v6, 0x0

    goto :goto_4b

    .line 683
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_4b
    const/4 v4, 0x0

    goto :goto_4c

    .line 688
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_2b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 690
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4c

    .line 691
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 696
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getDataCallLimitEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 697
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 698
    if-eqz v20, :cond_4d

    const/4 v4, 0x1

    :goto_4e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 699
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 694
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_4c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4d

    .line 698
    .restart local v20    # "_result":Z
    :cond_4d
    const/4 v4, 0x0

    goto :goto_4e

    .line 703
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_2c
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 705
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4e

    .line 706
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 712
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 714
    .local v6, "_arg1":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v8

    .line 716
    .local v8, "_arg2":J
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    .local v10, "_arg3":J
    move-object/from16 v4, p0

    .line 717
    invoke-virtual/range {v4 .. v11}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setLimitOfDataCalls(Landroid/app/enterprise/ContextInfo;JJJ)Z

    move-result v20

    .line 718
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 719
    if-eqz v20, :cond_4f

    const/4 v4, 0x1

    :goto_50
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 720
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 709
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":J
    .end local v8    # "_arg2":J
    .end local v10    # "_arg3":J
    .end local v20    # "_result":Z
    :cond_4e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4f

    .line 719
    .restart local v6    # "_arg1":J
    .restart local v8    # "_arg2":J
    .restart local v10    # "_arg3":J
    .restart local v20    # "_result":Z
    :cond_4f
    const/4 v4, 0x0

    goto :goto_50

    .line 724
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":J
    .end local v8    # "_arg2":J
    .end local v10    # "_arg3":J
    .end local v20    # "_result":Z
    :sswitch_2d
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 726
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_50

    .line 727
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 733
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_51
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    .line 734
    .local v6, "_arg1":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getLimitOfDataCalls(Landroid/app/enterprise/ContextInfo;I)J

    move-result-wide v20

    .line 735
    .local v20, "_result":J
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 736
    move-object/from16 v0, p3

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/os/Parcel;->writeLong(J)V

    .line 737
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 730
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":I
    .end local v20    # "_result":J
    :cond_50
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_51

    .line 741
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2e
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 743
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_51

    .line 744
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 749
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_52
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->resetDataCallLimitCounter(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 750
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 751
    if-eqz v20, :cond_52

    const/4 v4, 0x1

    :goto_53
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 752
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 747
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_51
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_52

    .line 751
    .restart local v20    # "_result":Z
    :cond_52
    const/4 v4, 0x0

    goto :goto_53

    .line 756
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_2f
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 758
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_53

    const/4 v5, 0x1

    .line 759
    .local v5, "_arg0":Z
    :goto_54
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->checkEnableUseOfPacketData(Z)Z

    move-result v20

    .line 760
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 761
    if-eqz v20, :cond_54

    const/4 v4, 0x1

    :goto_55
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 762
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 758
    .end local v5    # "_arg0":Z
    .end local v20    # "_result":Z
    :cond_53
    const/4 v5, 0x0

    goto :goto_54

    .line 761
    .restart local v5    # "_arg0":Z
    .restart local v20    # "_result":Z
    :cond_54
    const/4 v4, 0x0

    goto :goto_55

    .line 766
    .end local v5    # "_arg0":Z
    .end local v20    # "_result":Z
    :sswitch_30
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 767
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->checkDataCallLimit()Z

    move-result v20

    .line 768
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    if-eqz v20, :cond_55

    const/4 v4, 0x1

    :goto_56
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 770
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 769
    :cond_55
    const/4 v4, 0x0

    goto :goto_56

    .line 774
    .end local v20    # "_result":Z
    :sswitch_31
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 776
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v18

    .line 777
    .local v18, "_arg0":J
    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->updateDateAndDataCallCounters(J)V

    .line 778
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 779
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 783
    .end local v18    # "_arg0":J
    :sswitch_32
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 784
    invoke-virtual/range {p0 .. p0}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->updateDataLimitState()V

    .line 785
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 786
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 790
    :sswitch_33
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 792
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_56

    .line 793
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 799
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_57
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_57

    const/4 v6, 0x1

    .line 800
    .local v6, "_arg1":Z
    :goto_58
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowIncomingSms(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 801
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 802
    if-eqz v20, :cond_58

    const/4 v4, 0x1

    :goto_59
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 803
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 796
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_56
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_57

    .line 799
    :cond_57
    const/4 v6, 0x0

    goto :goto_58

    .line 802
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_58
    const/4 v4, 0x0

    goto :goto_59

    .line 807
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_34
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 809
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_59

    .line 810
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 816
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5a

    const/4 v6, 0x1

    .line 817
    .restart local v6    # "_arg1":Z
    :goto_5b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowOutgoingSms(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 818
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 819
    if-eqz v20, :cond_5b

    const/4 v4, 0x1

    :goto_5c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 820
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 813
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_59
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5a

    .line 816
    :cond_5a
    const/4 v6, 0x0

    goto :goto_5b

    .line 819
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_5b
    const/4 v4, 0x0

    goto :goto_5c

    .line 824
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_35
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 826
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5c

    .line 827
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 832
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isIncomingSmsAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 833
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 834
    if-eqz v20, :cond_5d

    const/4 v4, 0x1

    :goto_5e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 835
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 830
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_5c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5d

    .line 834
    .restart local v20    # "_result":Z
    :cond_5d
    const/4 v4, 0x0

    goto :goto_5e

    .line 839
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_36
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 841
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_5e

    .line 842
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 847
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isOutgoingSmsAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 848
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 849
    if-eqz v20, :cond_5f

    const/4 v4, 0x1

    :goto_60
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 850
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 845
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_5e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5f

    .line 849
    .restart local v20    # "_result":Z
    :cond_5f
    const/4 v4, 0x0

    goto :goto_60

    .line 854
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_37
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 856
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_60

    .line 857
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 863
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_61
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_61

    const/4 v6, 0x1

    .line 864
    .restart local v6    # "_arg1":Z
    :goto_62
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowIncomingMms(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 865
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 866
    if-eqz v20, :cond_62

    const/4 v4, 0x1

    :goto_63
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 867
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 860
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_60
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_61

    .line 863
    :cond_61
    const/4 v6, 0x0

    goto :goto_62

    .line 866
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_62
    const/4 v4, 0x0

    goto :goto_63

    .line 871
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_38
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 873
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_63

    .line 874
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 880
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_64
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_64

    const/4 v6, 0x1

    .line 881
    .restart local v6    # "_arg1":Z
    :goto_65
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowOutgoingMms(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 882
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 883
    if-eqz v20, :cond_65

    const/4 v4, 0x1

    :goto_66
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 884
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 877
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_63
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_64

    .line 880
    :cond_64
    const/4 v6, 0x0

    goto :goto_65

    .line 883
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_65
    const/4 v4, 0x0

    goto :goto_66

    .line 888
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_39
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 890
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_66

    .line 891
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 896
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_67
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isIncomingMmsAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 897
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 898
    if-eqz v20, :cond_67

    const/4 v4, 0x1

    :goto_68
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 899
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 894
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_66
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_67

    .line 898
    .restart local v20    # "_result":Z
    :cond_67
    const/4 v4, 0x0

    goto :goto_68

    .line 903
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_3a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 905
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_68

    .line 906
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 911
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_69
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isOutgoingMmsAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 912
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 913
    if-eqz v20, :cond_69

    const/4 v4, 0x1

    :goto_6a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 914
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 909
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_68
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_69

    .line 913
    .restart local v20    # "_result":Z
    :cond_69
    const/4 v4, 0x0

    goto :goto_6a

    .line 918
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_3b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 920
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6a

    .line 921
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 927
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6b

    const/4 v6, 0x1

    .line 928
    .restart local v6    # "_arg1":Z
    :goto_6c
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->blockSmsWithStorage(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 929
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 930
    if-eqz v20, :cond_6c

    const/4 v4, 0x1

    :goto_6d
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 931
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 924
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_6a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6b

    .line 927
    :cond_6b
    const/4 v6, 0x0

    goto :goto_6c

    .line 930
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_6c
    const/4 v4, 0x0

    goto :goto_6d

    .line 935
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_3c
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 937
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6d

    .line 938
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 943
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6e
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isBlockSmsWithStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 944
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 945
    if-eqz v20, :cond_6e

    const/4 v4, 0x1

    :goto_6f
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 946
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 941
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_6d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6e

    .line 945
    .restart local v20    # "_result":Z
    :cond_6e
    const/4 v4, 0x0

    goto :goto_6f

    .line 950
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_3d
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 952
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_6f

    .line 953
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 959
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_70
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_70

    const/4 v6, 0x1

    .line 960
    .restart local v6    # "_arg1":Z
    :goto_71
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->blockMmsWithStorage(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 961
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 962
    if-eqz v20, :cond_71

    const/4 v4, 0x1

    :goto_72
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 963
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 956
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_6f
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_70

    .line 959
    :cond_70
    const/4 v6, 0x0

    goto :goto_71

    .line 962
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_71
    const/4 v4, 0x0

    goto :goto_72

    .line 967
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_3e
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 969
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_72

    .line 970
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 975
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_73
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isBlockMmsWithStorageEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 976
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 977
    if-eqz v20, :cond_73

    const/4 v4, 0x1

    :goto_74
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 978
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 973
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_72
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_73

    .line 977
    .restart local v20    # "_result":Z
    :cond_73
    const/4 v4, 0x0

    goto :goto_74

    .line 982
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_3f
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 984
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_74

    const/4 v5, 0x1

    .line 986
    .local v5, "_arg0":Z
    :goto_75
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v6

    .line 988
    .local v6, "_arg1":[B
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 990
    .local v8, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    .line 992
    .local v10, "_arg3":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v17

    .local v17, "_arg4":Ljava/lang/String;
    move-object/from16 v12, p0

    move v13, v5

    move-object v14, v6

    move-object v15, v8

    move/from16 v16, v10

    .line 993
    invoke-virtual/range {v12 .. v17}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->storeBlockedSmsMms(Z[BLjava/lang/String;ILjava/lang/String;)V

    .line 994
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 995
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 984
    .end local v5    # "_arg0":Z
    .end local v6    # "_arg1":[B
    .end local v8    # "_arg2":Ljava/lang/String;
    .end local v10    # "_arg3":I
    .end local v17    # "_arg4":Ljava/lang/String;
    :cond_74
    const/4 v5, 0x0

    goto :goto_75

    .line 999
    :sswitch_40
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1001
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_75

    .line 1002
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1007
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_76
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->clearStoredBlockedSms(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1008
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1009
    if-eqz v20, :cond_76

    const/4 v4, 0x1

    :goto_77
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1010
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1005
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_75
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_76

    .line 1009
    .restart local v20    # "_result":Z
    :cond_76
    const/4 v4, 0x0

    goto :goto_77

    .line 1014
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_41
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1016
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_77

    .line 1017
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1022
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_78
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->clearStoredBlockedMms(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1023
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1024
    if-eqz v20, :cond_78

    const/4 v4, 0x1

    :goto_79
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1025
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1020
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_77
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_78

    .line 1024
    .restart local v20    # "_result":Z
    :cond_78
    const/4 v4, 0x0

    goto :goto_79

    .line 1029
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_42
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1031
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_79

    .line 1032
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1038
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7a
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7a

    const/4 v6, 0x1

    .line 1039
    .local v6, "_arg1":Z
    :goto_7b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowWapPush(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 1040
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1041
    if-eqz v20, :cond_7b

    const/4 v4, 0x1

    :goto_7c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1042
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1035
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_79
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7a

    .line 1038
    :cond_7a
    const/4 v6, 0x0

    goto :goto_7b

    .line 1041
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_7b
    const/4 v4, 0x0

    goto :goto_7c

    .line 1046
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_43
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1048
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7c

    .line 1049
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1054
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isWapPushAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1055
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1056
    if-eqz v20, :cond_7d

    const/4 v4, 0x1

    :goto_7e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1057
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1052
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_7c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7d

    .line 1056
    .restart local v20    # "_result":Z
    :cond_7d
    const/4 v4, 0x0

    goto :goto_7e

    .line 1061
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_44
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1063
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7e

    .line 1064
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1070
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7f

    const/4 v6, 0x1

    .line 1071
    .restart local v6    # "_arg1":Z
    :goto_80
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowCallerIDDisplay(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 1072
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1073
    if-eqz v20, :cond_80

    const/4 v4, 0x1

    :goto_81
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1074
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1067
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_7e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7f

    .line 1070
    :cond_7f
    const/4 v6, 0x0

    goto :goto_80

    .line 1073
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_80
    const/4 v4, 0x0

    goto :goto_81

    .line 1078
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_45
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1080
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_81

    .line 1081
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1086
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_82
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isCallerIDDisplayAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1087
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1088
    if-eqz v20, :cond_82

    const/4 v4, 0x1

    :goto_83
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1089
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1084
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_81
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_82

    .line 1088
    .restart local v20    # "_result":Z
    :cond_82
    const/4 v4, 0x0

    goto :goto_83

    .line 1093
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_46
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1095
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_83

    .line 1096
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1102
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_84
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1104
    .local v6, "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_84

    const/4 v8, 0x1

    .line 1105
    .local v8, "_arg2":Z
    :goto_85
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->lockUnlockCorporateSimCard(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)I

    move-result v20

    .line 1106
    .local v20, "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1107
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1108
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1099
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v8    # "_arg2":Z
    .end local v20    # "_result":I
    :cond_83
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_84

    .line 1104
    .restart local v6    # "_arg1":Ljava/lang/String;
    :cond_84
    const/4 v8, 0x0

    goto :goto_85

    .line 1112
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    :sswitch_47
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1114
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_85

    .line 1115
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1121
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_86
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1123
    .restart local v6    # "_arg1":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 1124
    .local v8, "_arg2":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v8}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->changeSimPinCode(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    .line 1125
    .restart local v20    # "_result":I
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1126
    move-object/from16 v0, p3

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1127
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1118
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v8    # "_arg2":Ljava/lang/String;
    .end local v20    # "_result":I
    :cond_85
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_86

    .line 1131
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_48
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1133
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1134
    .local v5, "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isSimLockedByAdmin(Ljava/lang/String;)Z

    move-result v20

    .line 1135
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1136
    if-eqz v20, :cond_86

    const/4 v4, 0x1

    :goto_87
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1137
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1136
    :cond_86
    const/4 v4, 0x0

    goto :goto_87

    .line 1141
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_49
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1143
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 1144
    .restart local v5    # "_arg0":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getPinCode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 1145
    .local v20, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1146
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1147
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1151
    .end local v5    # "_arg0":Ljava/lang/String;
    .end local v20    # "_result":Ljava/lang/String;
    :sswitch_4a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1153
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_87

    .line 1154
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1160
    .local v5, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_88
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_88

    const/4 v6, 0x1

    .line 1161
    .local v6, "_arg1":Z
    :goto_89
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->allowCopyContactToSim(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v20

    .line 1162
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1163
    if-eqz v20, :cond_89

    const/4 v4, 0x1

    :goto_8a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1164
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1157
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :cond_87
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_88

    .line 1160
    :cond_88
    const/4 v6, 0x0

    goto :goto_89

    .line 1163
    .restart local v6    # "_arg1":Z
    .restart local v20    # "_result":Z
    :cond_89
    const/4 v4, 0x0

    goto :goto_8a

    .line 1168
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Z
    .end local v20    # "_result":Z
    :sswitch_4b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1170
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8a

    .line 1171
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1176
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->isCopyContactToSimAllowed(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1177
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1178
    if-eqz v20, :cond_8b

    const/4 v4, 0x1

    :goto_8c
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1179
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1174
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_8a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8b

    .line 1178
    .restart local v20    # "_result":Z
    :cond_8b
    const/4 v4, 0x0

    goto :goto_8c

    .line 1183
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_4c
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1185
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8c

    .line 1186
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1191
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getOutgoingCallExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v20

    .line 1192
    .local v20, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1193
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1194
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1189
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Ljava/lang/String;
    :cond_8c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8d

    .line 1198
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4d
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1200
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8d

    .line 1201
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1206
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8e
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getIncomingCallExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v20

    .line 1207
    .restart local v20    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1208
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1209
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1204
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Ljava/lang/String;
    :cond_8d
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8e

    .line 1213
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4e
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1215
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_8e

    .line 1216
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1221
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1222
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1223
    if-eqz v20, :cond_8f

    const/4 v4, 0x1

    :goto_90
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1224
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1219
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_8e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8f

    .line 1223
    .restart local v20    # "_result":Z
    :cond_8f
    const/4 v4, 0x0

    goto :goto_90

    .line 1228
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_4f
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1230
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_90

    .line 1231
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1236
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_91
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1237
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1238
    if-eqz v20, :cond_91

    const/4 v4, 0x1

    :goto_92
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1239
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1234
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_90
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_91

    .line 1238
    .restart local v20    # "_result":Z
    :cond_91
    const/4 v4, 0x0

    goto :goto_92

    .line 1243
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_50
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1245
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_92

    .line 1246
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1252
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_93
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1253
    .local v6, "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1254
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1255
    if-eqz v20, :cond_93

    const/4 v4, 0x1

    :goto_94
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1256
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1249
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_92
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_93

    .line 1255
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_93
    const/4 v4, 0x0

    goto :goto_94

    .line 1260
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_51
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1262
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_94

    .line 1263
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1269
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_95
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1270
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1271
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1272
    if-eqz v20, :cond_95

    const/4 v4, 0x1

    :goto_96
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1273
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1266
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_94
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_95

    .line 1272
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_95
    const/4 v4, 0x0

    goto :goto_96

    .line 1277
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_52
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1279
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_96

    .line 1280
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1286
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_97
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1287
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setOutgoingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1288
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1289
    if-eqz v20, :cond_97

    const/4 v4, 0x1

    :goto_98
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1290
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1283
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_96
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_97

    .line 1289
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_97
    const/4 v4, 0x0

    goto :goto_98

    .line 1294
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_53
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1296
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_98

    .line 1297
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1303
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_99
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1304
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setIncomingCallExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1305
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1306
    if-eqz v20, :cond_99

    const/4 v4, 0x1

    :goto_9a
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1307
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1300
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_98
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_99

    .line 1306
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_99
    const/4 v4, 0x0

    goto :goto_9a

    .line 1311
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_54
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1313
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9a

    .line 1314
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1319
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9b
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getOutgoingSmsExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v20

    .line 1320
    .local v20, "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1321
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1322
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1317
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Ljava/lang/String;
    :cond_9a
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9b

    .line 1326
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_55
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1328
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9b

    .line 1329
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1334
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9c
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->getIncomingSmsExceptionPatterns(Landroid/app/enterprise/ContextInfo;)Ljava/lang/String;

    move-result-object v20

    .line 1335
    .restart local v20    # "_result":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1336
    move-object/from16 v0, p3

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1337
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1332
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Ljava/lang/String;
    :cond_9b
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9c

    .line 1341
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_56
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1343
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9c

    .line 1344
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1349
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9d
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1350
    .local v20, "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1351
    if-eqz v20, :cond_9d

    const/4 v4, 0x1

    :goto_9e
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1352
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1347
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_9c
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9d

    .line 1351
    .restart local v20    # "_result":Z
    :cond_9d
    const/4 v4, 0x0

    goto :goto_9e

    .line 1356
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_57
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1358
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_9e

    .line 1359
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1364
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9f
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->removeIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;)Z

    move-result v20

    .line 1365
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1366
    if-eqz v20, :cond_9f

    const/4 v4, 0x1

    :goto_a0
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1367
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1362
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :cond_9e
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9f

    .line 1366
    .restart local v20    # "_result":Z
    :cond_9f
    const/4 v4, 0x0

    goto :goto_a0

    .line 1371
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v20    # "_result":Z
    :sswitch_58
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1373
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a0

    .line 1374
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1380
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1381
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1382
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1383
    if-eqz v20, :cond_a1

    const/4 v4, 0x1

    :goto_a2
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1384
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1377
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_a0
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a1

    .line 1383
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_a1
    const/4 v4, 0x0

    goto :goto_a2

    .line 1388
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_59
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1390
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a2

    .line 1391
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1397
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1398
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->addIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1399
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1400
    if-eqz v20, :cond_a3

    const/4 v4, 0x1

    :goto_a4
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1401
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1394
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_a2
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a3

    .line 1400
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_a3
    const/4 v4, 0x0

    goto :goto_a4

    .line 1405
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_5a
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1407
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a4

    .line 1408
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1414
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1415
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setOutgoingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1416
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1417
    if-eqz v20, :cond_a5

    const/4 v4, 0x1

    :goto_a6
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1418
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1411
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_a4
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a5

    .line 1417
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_a5
    const/4 v4, 0x0

    goto :goto_a6

    .line 1422
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :sswitch_5b
    const-string v4, "android.app.enterprise.IPhoneRestrictionPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 1424
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a6

    .line 1425
    sget-object v4, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/enterprise/ContextInfo;

    .line 1431
    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 1432
    .restart local v6    # "_arg1":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6}, Landroid/app/enterprise/IPhoneRestrictionPolicy$Stub;->setIncomingSmsExceptionPattern(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v20

    .line 1433
    .restart local v20    # "_result":Z
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 1434
    if-eqz v20, :cond_a7

    const/4 v4, 0x1

    :goto_a8
    move-object/from16 v0, p3

    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 1435
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 1428
    .end local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_arg1":Ljava/lang/String;
    .end local v20    # "_result":Z
    :cond_a6
    const/4 v5, 0x0

    .restart local v5    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a7

    .line 1434
    .restart local v6    # "_arg1":Ljava/lang/String;
    .restart local v20    # "_result":Z
    :cond_a7
    const/4 v4, 0x0

    goto :goto_a8

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
