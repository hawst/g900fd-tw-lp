.class public abstract Landroid/app/enterprise/IFirewallPolicy$Stub;
.super Landroid/os/Binder;
.source "IFirewallPolicy.java"

# interfaces
.implements Landroid/app/enterprise/IFirewallPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/IFirewallPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/IFirewallPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.IFirewallPolicy"

.field static final TRANSACTION_addRules:I = 0x1

.field static final TRANSACTION_cleanAllRules:I = 0x8

.field static final TRANSACTION_cleanProxyRules:I = 0x7

.field static final TRANSACTION_cleanRules:I = 0x6

.field static final TRANSACTION_clearDnsPerApp:I = 0x1f

.field static final TRANSACTION_clearGlobalProxyEnable:I = 0x22

.field static final TRANSACTION_enableProxy:I = 0xb

.field static final TRANSACTION_enableProxyWithUid:I = 0xc

.field static final TRANSACTION_enableRules:I = 0x9

.field static final TRANSACTION_enableRulesWithUid:I = 0xa

.field static final TRANSACTION_getAllRulesForUid:I = 0xf

.field static final TRANSACTION_getDNSPerApp:I = 0x1e

.field static final TRANSACTION_getGlobalProxy:I = 0x21

.field static final TRANSACTION_getNetworkForMarket:I = 0x1b

.field static final TRANSACTION_getRules:I = 0x3

.field static final TRANSACTION_getURLFilterEnabled:I = 0x12

.field static final TRANSACTION_getURLFilterList:I = 0x14

.field static final TRANSACTION_getURLFilterReport:I = 0x18

.field static final TRANSACTION_getURLFilterReportEnabled:I = 0x17

.field static final TRANSACTION_isEnabledProxy:I = 0xd

.field static final TRANSACTION_isEnabledRules:I = 0xe

.field static final TRANSACTION_isGlobalProxyAllowed:I = 0x23

.field static final TRANSACTION_isUrlBlocked:I = 0x15

.field static final TRANSACTION_listIptablesRules:I = 0x19

.field static final TRANSACTION_removeAdmin:I = 0x10

.field static final TRANSACTION_removeDNSForApp:I = 0x1d

.field static final TRANSACTION_removeRules:I = 0x2

.field static final TRANSACTION_setDNSPerApp:I = 0x1c

.field static final TRANSACTION_setGlobalProxy:I = 0x20

.field static final TRANSACTION_setNetworkForMarket:I = 0x1a

.field static final TRANSACTION_setProxyRules:I = 0x5

.field static final TRANSACTION_setRules:I = 0x4

.field static final TRANSACTION_setURLFilterEnabled:I = 0x11

.field static final TRANSACTION_setURLFilterList:I = 0x13

.field static final TRANSACTION_setURLFilterReportEnabled:I = 0x16


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IFirewallPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "android.app.enterprise.IFirewallPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/IFirewallPolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Landroid/app/enterprise/IFirewallPolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Landroid/app/enterprise/IFirewallPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/IFirewallPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 627
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1

    .line 53
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 59
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 61
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 62
    .local v3, "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->addRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z

    move-result v6

    .line 63
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 64
    if-eqz v6, :cond_0

    move v8, v9

    :cond_0
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 56
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Z
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 69
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_2
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3

    .line 72
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 78
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 80
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 81
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->removeRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z

    move-result v6

    .line 82
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 83
    if-eqz v6, :cond_2

    move v8, v9

    :cond_2
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 75
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2

    .line 88
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_4

    .line 91
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 97
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 98
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getRules(Landroid/app/enterprise/ContextInfo;I)Ljava/util/List;

    move-result-object v7

    .line 99
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto :goto_0

    .line 94
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 105
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_6

    .line 108
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 114
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 116
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 117
    .restart local v3    # "_arg2":I
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setRules(Landroid/app/enterprise/ContextInfo;Ljava/util/List;I)Z

    move-result v6

    .line 118
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 119
    if-eqz v6, :cond_5

    move v8, v9

    :cond_5
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 111
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Z
    :cond_6
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 124
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 126
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_8

    .line 127
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 133
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 136
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setProxyRules(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 137
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v6, :cond_7

    move v8, v9

    :cond_7
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 130
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_8
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 143
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_a

    .line 146
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 152
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 153
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->cleanRules(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v6

    .line 154
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 155
    if-eqz v6, :cond_9

    move v8, v9

    :cond_9
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 149
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 160
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_c

    .line 163
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 169
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 170
    .restart local v1    # "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->cleanProxyRules(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v6

    .line 171
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 172
    if-eqz v6, :cond_b

    move v8, v9

    :cond_b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 166
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 177
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_e

    .line 180
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 185
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->cleanAllRules(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 186
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 187
    if-eqz v6, :cond_d

    move v8, v9

    :cond_d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 183
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 192
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_10

    .line 195
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 201
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_11

    move v1, v9

    .line 202
    .local v1, "_arg1":Z
    :goto_a
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->enableRules(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 203
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 204
    if-eqz v6, :cond_f

    move v8, v9

    :cond_f
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 198
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    :cond_11
    move v1, v8

    .line 201
    goto :goto_a

    .line 209
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_13

    move v0, v9

    .line 213
    .local v0, "_arg0":Z
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 214
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->enableRulesWithUid(ZI)Z

    move-result v6

    .line 215
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 216
    if-eqz v6, :cond_12

    move v8, v9

    :cond_12
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_13
    move v0, v8

    .line 211
    goto :goto_b

    .line 221
    :sswitch_b
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_15

    .line 224
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 230
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_16

    move v1, v9

    .line 231
    .local v1, "_arg1":Z
    :goto_d
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->enableProxy(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 232
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 233
    if-eqz v6, :cond_14

    move v8, v9

    :cond_14
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 227
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_15
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    :cond_16
    move v1, v8

    .line 230
    goto :goto_d

    .line 238
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_c
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_18

    move v0, v9

    .line 242
    .local v0, "_arg0":Z
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 243
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->enableProxyWithUid(ZI)Z

    move-result v6

    .line 244
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 245
    if-eqz v6, :cond_17

    move v8, v9

    :cond_17
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v1    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_18
    move v0, v8

    .line 240
    goto :goto_e

    .line 250
    :sswitch_d
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1a

    .line 253
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 258
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->isEnabledProxy(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 259
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v6, :cond_19

    move v8, v9

    :cond_19
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 256
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 265
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_e
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1c

    .line 268
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 273
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_10
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->isEnabledRules(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 274
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 275
    if-eqz v6, :cond_1b

    move v8, v9

    :cond_1b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 271
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_10

    .line 280
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_f
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 282
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_1d

    .line 283
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 288
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getAllRulesForUid(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 289
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 290
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 286
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 295
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_10
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 297
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1f

    .line 298
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 303
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_12
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->removeAdmin(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 304
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 305
    if-eqz v6, :cond_1e

    move v8, v9

    :cond_1e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 301
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_1f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_12

    .line 310
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_11
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_21

    .line 313
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 319
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_22

    move v1, v9

    .line 320
    .local v1, "_arg1":Z
    :goto_14
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setURLFilterEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 321
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 322
    if-eqz v6, :cond_20

    move v8, v9

    :cond_20
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 316
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_21
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    :cond_22
    move v1, v8

    .line 319
    goto :goto_14

    .line 327
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_12
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_24

    .line 330
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 336
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_25

    move v1, v9

    .line 338
    .restart local v1    # "_arg1":Z
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_26

    move v3, v9

    .line 339
    .local v3, "_arg2":Z
    :goto_17
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getURLFilterEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z

    move-result v6

    .line 340
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 341
    if-eqz v6, :cond_23

    move v8, v9

    :cond_23
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 333
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_24
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_15

    :cond_25
    move v1, v8

    .line 336
    goto :goto_16

    .restart local v1    # "_arg1":Z
    :cond_26
    move v3, v8

    .line 338
    goto :goto_17

    .line 346
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    :sswitch_13
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_28

    .line 349
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 355
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 356
    .restart local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setURLFilterList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z

    move-result v6

    .line 357
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 358
    if-eqz v6, :cond_27

    move v8, v9

    :cond_27
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 352
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Z
    :cond_28
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_18

    .line 363
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_14
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 365
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_29

    .line 366
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 372
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2a

    move v1, v9

    .line 374
    .restart local v1    # "_arg1":Z
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2b

    move v3, v9

    .line 375
    .restart local v3    # "_arg2":Z
    :goto_1b
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getURLFilterList(Landroid/app/enterprise/ContextInfo;ZZ)Ljava/util/List;

    move-result-object v7

    .line 376
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 377
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 369
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":Z
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_29
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_19

    :cond_2a
    move v1, v8

    .line 372
    goto :goto_1a

    .restart local v1    # "_arg1":Z
    :cond_2b
    move v3, v8

    .line 374
    goto :goto_1b

    .line 382
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    :sswitch_15
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 384
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2d

    .line 385
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 391
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 392
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->isUrlBlocked(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 393
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 394
    if-eqz v6, :cond_2c

    move v8, v9

    :cond_2c
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 388
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_2d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1c

    .line 399
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_16
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 401
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_2f

    .line 402
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 408
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_30

    move v1, v9

    .line 409
    .local v1, "_arg1":Z
    :goto_1e
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;Z)Z

    move-result v6

    .line 410
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 411
    if-eqz v6, :cond_2e

    move v8, v9

    :cond_2e
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 405
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v6    # "_result":Z
    :cond_2f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1d

    :cond_30
    move v1, v8

    .line 408
    goto :goto_1e

    .line 416
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_17
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 418
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_32

    .line 419
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 425
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_33

    move v1, v9

    .line 427
    .restart local v1    # "_arg1":Z
    :goto_20
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_34

    move v3, v9

    .line 428
    .restart local v3    # "_arg2":Z
    :goto_21
    invoke-virtual {p0, v0, v1, v3}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getURLFilterReportEnabled(Landroid/app/enterprise/ContextInfo;ZZ)Z

    move-result v6

    .line 429
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 430
    if-eqz v6, :cond_31

    move v8, v9

    :cond_31
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 422
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Z
    :cond_32
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1f

    :cond_33
    move v1, v8

    .line 425
    goto :goto_20

    .restart local v1    # "_arg1":Z
    :cond_34
    move v3, v8

    .line 427
    goto :goto_21

    .line 435
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    :sswitch_18
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 437
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_35

    .line 438
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 443
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_22
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getURLFilterReport(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 444
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 445
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 441
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_35
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_22

    .line 450
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_19
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 452
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_36

    .line 453
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 458
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_23
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->listIptablesRules(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 459
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 460
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 456
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_36
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_23

    .line 465
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1a
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 467
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_38

    .line 468
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 474
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_24
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 475
    .local v1, "_arg1":I
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setNetworkForMarket(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v6

    .line 476
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 477
    if-eqz v6, :cond_37

    move v8, v9

    :cond_37
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 471
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":I
    .end local v6    # "_result":Z
    :cond_38
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_24

    .line 482
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1b
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 484
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_39

    .line 485
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 491
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_25
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3a

    move v1, v9

    .line 492
    .local v1, "_arg1":Z
    :goto_26
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getNetworkForMarket(Landroid/app/enterprise/ContextInfo;Z)I

    move-result v6

    .line 493
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 494
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 488
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Z
    .end local v6    # "_result":I
    :cond_39
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_25

    :cond_3a
    move v1, v8

    .line 491
    goto :goto_26

    .line 499
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1c
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 501
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3c

    .line 502
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 508
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 510
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 512
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 513
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3, v4}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setDNSPerApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 514
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 515
    if-eqz v6, :cond_3b

    move v8, v9

    :cond_3b
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 505
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_3c
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_27

    .line 520
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1d
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 522
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_3e

    .line 523
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 529
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_28
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 530
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->removeDNSForApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    .line 531
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 532
    if-eqz v6, :cond_3d

    move v8, v9

    :cond_3d
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 526
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Z
    :cond_3e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_28

    .line 537
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1e
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 539
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3f

    .line 540
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 546
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_29
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 547
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getDNSPerApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 548
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 549
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 543
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3f
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_29

    .line 554
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_1f
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 556
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_41

    .line 557
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 562
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->clearDnsPerApp(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 563
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 564
    if-eqz v6, :cond_40

    move v8, v9

    :cond_40
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 560
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_41
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2a

    .line 569
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_20
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 571
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_43

    .line 572
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 578
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 580
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 582
    .local v3, "_arg2":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 583
    .local v5, "_arg3":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1, v3, v5}, Landroid/app/enterprise/IFirewallPolicy$Stub;->setGlobalProxy(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ILjava/util/List;)Z

    move-result v6

    .line 584
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 585
    if-eqz v6, :cond_42

    move v8, v9

    :cond_42
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 575
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v5    # "_arg3":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Z
    :cond_43
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2b

    .line 590
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_21
    const-string v8, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 592
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_44

    .line 593
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 598
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->getGlobalProxy(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v7

    .line 599
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 600
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 596
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_44
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2c

    .line 605
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_22
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 607
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_46

    .line 608
    sget-object v10, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 613
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_2d
    invoke-virtual {p0, v0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->clearGlobalProxyEnable(Landroid/app/enterprise/ContextInfo;)Z

    move-result v6

    .line 614
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 615
    if-eqz v6, :cond_45

    move v8, v9

    :cond_45
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 611
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v6    # "_result":Z
    :cond_46
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_2d

    .line 620
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_23
    const-string v10, "android.app.enterprise.IFirewallPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 621
    invoke-virtual {p0}, Landroid/app/enterprise/IFirewallPolicy$Stub;->isGlobalProxyAllowed()Z

    move-result v6

    .line 622
    .restart local v6    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 623
    if-eqz v6, :cond_47

    move v8, v9

    :cond_47
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
