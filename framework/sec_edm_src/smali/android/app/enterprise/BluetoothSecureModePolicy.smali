.class public Landroid/app/enterprise/BluetoothSecureModePolicy;
.super Ljava/lang/Object;
.source "BluetoothSecureModePolicy.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BTSecureModePolicy"

.field private static btSecureMode:Landroid/app/enterprise/BluetoothSecureModePolicy;

.field private static mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object p2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContext:Landroid/content/Context;

    .line 92
    iput-object p1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 94
    return-void
.end method

.method private static getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    if-nez v0, :cond_0

    .line 100
    const-string v0, "bluetooth_secure_mode_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IBluetoothSecureModePolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    .line 106
    :cond_0
    sget-object v0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    return-object v0
.end method


# virtual methods
.method public addBluetoothDevicesToWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 296
    .local p1, "whiteListConfig":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.addBluetoothDevicesToWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 298
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 300
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->addBluetoothDevicesToWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 310
    :goto_0
    return v1

    .line 304
    :catch_0
    move-exception v0

    .line 306
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 310
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public disableSecureMode()Z
    .locals 3

    .prologue
    .line 144
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.disableSecureMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 146
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 148
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->disableSecureMode(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 158
    :goto_0
    return v1

    .line 152
    :catch_0
    move-exception v0

    .line 154
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 158
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableDeviceWhiteList(Z)Z
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 219
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.enableDeviceWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 221
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 223
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->enableDeviceWhiteList(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 233
    :goto_0
    return v1

    .line 227
    :catch_0
    move-exception v0

    .line 229
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 233
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enableSecureMode(Landroid/app/enterprise/BluetoothSecureModeConfig;Ljava/util/List;)Z
    .locals 3
    .param p1, "configObj"    # Landroid/app/enterprise/BluetoothSecureModeConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/enterprise/BluetoothSecureModeConfig;",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 119
    .local p2, "whiteList":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.enableSecureMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 121
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 123
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->enableSecureMode(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/BluetoothSecureModeConfig;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 133
    :goto_0
    return v1

    .line 127
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 133
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getBluetoothDevicesFromWhiteList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 273
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->getBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 283
    :goto_0
    return-object v1

    .line 277
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 283
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSecureModeConfiguration()Landroid/app/enterprise/BluetoothSecureModeConfig;
    .locals 3

    .prologue
    .line 171
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 173
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->getSecureModeConfiguration(Landroid/app/enterprise/ContextInfo;)Landroid/app/enterprise/BluetoothSecureModeConfig;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 183
    :goto_0
    return-object v1

    .line 177
    :catch_0
    move-exception v0

    .line 179
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 183
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isDeviceWhiteListEnabled()Z
    .locals 3

    .prologue
    .line 244
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.isDeviceWhiteListEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 246
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 248
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->isDeviceWhiteListEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 258
    :goto_0
    return v1

    .line 252
    :catch_0
    move-exception v0

    .line 254
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 258
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSecureModeEnabled()Z
    .locals 3

    .prologue
    .line 196
    :try_start_0
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 198
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->isSecureModeEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 208
    :goto_0
    return v1

    .line 202
    :catch_0
    move-exception v0

    .line 204
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 208
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removeBluetoothDevicesFromWhiteList(Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 323
    .local p1, "whiteListConfig":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/BluetoothSecureModeWhitelistConfig;>;"
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "BluetoothSecureModePolicy.removeBluetoothDevicesFromWhiteList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 325
    invoke-static {}, Landroid/app/enterprise/BluetoothSecureModePolicy;->getService()Landroid/app/enterprise/IBluetoothSecureModePolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 327
    sget-object v1, Landroid/app/enterprise/BluetoothSecureModePolicy;->mBTSecureModeService:Landroid/app/enterprise/IBluetoothSecureModePolicy;

    iget-object v2, p0, Landroid/app/enterprise/BluetoothSecureModePolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IBluetoothSecureModePolicy;->removeBluetoothDevicesFromWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 337
    :goto_0
    return v1

    .line 331
    :catch_0
    move-exception v0

    .line 333
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "BTSecureModePolicy"

    const-string v2, "Failed talking to BT Secure Mode service "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 337
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
