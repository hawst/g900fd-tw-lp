.class public Landroid/app/enterprise/LogManager;
.super Ljava/lang/Object;
.source "LogManager.java"


# static fields
.field public static final ERROR_INVALID_LOG_LEVEL:I = -0x4

.field public static final ERROR_INVALID_LOG_TYPE:I = -0x3

.field public static final ERROR_IO_WRITE:I = -0x6

.field public static final ERROR_LOG_TYPE_NOT_ACTIVE:I = -0x5

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_UNAUTHORIZED:I = -0x2

.field public static final ERROR_UNKNOWN:I = -0x1

.field public static final LOG_TYPE_CONSOLE:I = 0x1

.field public static final LOG_TYPE_FILE:I = 0x2

.field public static final LVL_DEBUG:I = 0x3

.field public static final LVL_ERROR:I = 0x6

.field public static final LVL_INFO:I = 0x4

.field public static final LVL_SENSITIVE:I = 0x1

.field public static final LVL_VERBOSE:I = 0x2

.field public static final LVL_WARNING:I = 0x5

.field private static TAG:Ljava/lang/String;

.field private static mService:Landroid/app/enterprise/ILogManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 140
    const-string v0, "LogManager"

    sput-object v0, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    return-void
.end method

.method public static copyLogs(Landroid/os/ParcelFileDescriptor;)I
    .locals 3
    .param p0, "file"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 467
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.copyLogs"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 468
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 470
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0}, Landroid/app/enterprise/ILogManager;->copyLogs(Landroid/os/ParcelFileDescriptor;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 475
    :goto_0
    return v1

    .line 471
    :catch_0
    move-exception v0

    .line 472
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 475
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static disableLogging(I)I
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 238
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.disableLogging"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 239
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 241
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0}, Landroid/app/enterprise/ILogManager;->disableLogging(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 246
    :goto_0
    return v1

    .line 242
    :catch_0
    move-exception v0

    .line 243
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 246
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static enableLogging(I)I
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 191
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.enableLogging"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 192
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 194
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0}, Landroid/app/enterprise/ILogManager;->enableLogging(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 199
    :goto_0
    return v1

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 199
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public static getLogLevel(I)I
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 354
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.getLogLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 355
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 357
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0}, Landroid/app/enterprise/ILogManager;->getLogLevel(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 362
    :goto_0
    return v1

    .line 358
    :catch_0
    move-exception v0

    .line 359
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 362
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method private static getService()Landroid/app/enterprise/ILogManager;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    if-nez v0, :cond_0

    .line 149
    const-string v0, "log_manager_service"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/ILogManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/ILogManager;

    move-result-object v0

    sput-object v0, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    .line 152
    :cond_0
    sget-object v0, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    return-object v0
.end method

.method public static isLoggingEnabled(I)Z
    .locals 3
    .param p0, "type"    # I

    .prologue
    .line 406
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.isLoggingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 407
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 409
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0}, Landroid/app/enterprise/ILogManager;->isLoggingEnabled(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 414
    :goto_0
    return v1

    .line 410
    :catch_0
    move-exception v0

    .line 411
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 414
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static setLogLevel(II)I
    .locals 3
    .param p0, "type"    # I
    .param p1, "level"    # I

    .prologue
    .line 299
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "LogManager.setLogLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 300
    invoke-static {}, Landroid/app/enterprise/LogManager;->getService()Landroid/app/enterprise/ILogManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 302
    :try_start_0
    sget-object v1, Landroid/app/enterprise/LogManager;->mService:Landroid/app/enterprise/ILogManager;

    invoke-interface {v1, p0, p1}, Landroid/app/enterprise/ILogManager;->setLogLevel(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 307
    :goto_0
    return v1

    .line 303
    :catch_0
    move-exception v0

    .line 304
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/LogManager;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with Log Manager Service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 307
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
