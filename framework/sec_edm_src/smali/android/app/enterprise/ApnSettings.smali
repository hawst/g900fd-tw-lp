.class public Landroid/app/enterprise/ApnSettings;
.super Ljava/lang/Object;
.source "ApnSettings.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/app/enterprise/ApnSettings;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public apn:Ljava/lang/String;

.field public authType:I

.field public id:J

.field public mcc:Ljava/lang/String;

.field public mmsPort:Ljava/lang/String;

.field public mmsProxy:Ljava/lang/String;

.field public mmsc:Ljava/lang/String;

.field public mnc:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public password:Ljava/lang/String;

.field public port:I

.field public proxy:Ljava/lang/String;

.field public server:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public user:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    new-instance v0, Landroid/app/enterprise/ApnSettings$1;

    invoke-direct {v0}, Landroid/app/enterprise/ApnSettings$1;-><init>()V

    sput-object v0, Landroid/app/enterprise/ApnSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/app/enterprise/ApnSettings;->id:J

    .line 46
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    .line 86
    iput v2, p0, Landroid/app/enterprise/ApnSettings;->port:I

    .line 91
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    .line 111
    iput v2, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    .line 180
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v2, -0x1

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/app/enterprise/ApnSettings;->id:J

    .line 46
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    .line 61
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    .line 66
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    .line 71
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    .line 76
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    .line 81
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    .line 86
    iput v2, p0, Landroid/app/enterprise/ApnSettings;->port:I

    .line 91
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    .line 96
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    .line 101
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    .line 106
    const-string v0, ""

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    .line 111
    iput v2, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    .line 132
    invoke-virtual {p0, p1}, Landroid/app/enterprise/ApnSettings;->readFromParcel(Landroid/os/Parcel;)V

    .line 133
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/app/enterprise/ApnSettings$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Landroid/app/enterprise/ApnSettings$1;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/app/enterprise/ApnSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public getApn()Ljava/lang/String;
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    return-object v0
.end method

.method public getAuthType()I
    .locals 1

    .prologue
    .line 550
    iget v0, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 204
    iget-wide v0, p0, Landroid/app/enterprise/ApnSettings;->id:J

    return-wide v0
.end method

.method public getMcc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    return-object v0
.end method

.method public getMmsPort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    return-object v0
.end method

.method public getMmsProxy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    return-object v0
.end method

.method public getMmsc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    return-object v0
.end method

.method public getMnc()Ljava/lang/String;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    return-object v0
.end method

.method public getPort()I
    .locals 1

    .prologue
    .line 426
    iget v0, p0, Landroid/app/enterprise/ApnSettings;->port:I

    return v0
.end method

.method public getProxy()Ljava/lang/String;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    return-object v0
.end method

.method public getServer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUser()Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Landroid/app/enterprise/ApnSettings;->id:J

    .line 157
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    .line 159
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    .line 160
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    .line 161
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    .line 162
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    .line 163
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    .line 164
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    .line 165
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ApnSettings;->port:I

    .line 166
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    .line 167
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    .line 168
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    .line 169
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    .line 170
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    .line 171
    return-void
.end method

.method public setApn(Ljava/lang/String;)V
    .locals 0
    .param p1, "apn"    # Ljava/lang/String;

    .prologue
    .line 241
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    .line 242
    return-void
.end method

.method public setAuthType(I)V
    .locals 0
    .param p1, "authType"    # I

    .prologue
    .line 539
    iput p1, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    .line 540
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 192
    iput-wide p1, p0, Landroid/app/enterprise/ApnSettings;->id:J

    .line 193
    return-void
.end method

.method public setMcc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mcc"    # Ljava/lang/String;

    .prologue
    .line 266
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    .line 267
    return-void
.end method

.method public setMmsPort(Ljava/lang/String;)V
    .locals 0
    .param p1, "mmsPort"    # Ljava/lang/String;

    .prologue
    .line 464
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    .line 465
    return-void
.end method

.method public setMmsProxy(Ljava/lang/String;)V
    .locals 0
    .param p1, "mmsProxy"    # Ljava/lang/String;

    .prologue
    .line 439
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    .line 440
    return-void
.end method

.method public setMmsc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mmsc"    # Ljava/lang/String;

    .prologue
    .line 488
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    .line 490
    return-void
.end method

.method public setMnc(Ljava/lang/String;)V
    .locals 0
    .param p1, "mnc"    # Ljava/lang/String;

    .prologue
    .line 291
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    .line 292
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    .line 218
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 365
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    .line 366
    return-void
.end method

.method public setPort(I)V
    .locals 0
    .param p1, "port"    # I

    .prologue
    .line 414
    iput p1, p0, Landroid/app/enterprise/ApnSettings;->port:I

    .line 415
    return-void
.end method

.method public setProxy(Ljava/lang/String;)V
    .locals 0
    .param p1, "proxy"    # Ljava/lang/String;

    .prologue
    .line 389
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    .line 390
    return-void
.end method

.method public setServer(Ljava/lang/String;)V
    .locals 0
    .param p1, "server"    # Ljava/lang/String;

    .prologue
    .line 340
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    .line 341
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 514
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    .line 515
    return-void
.end method

.method public setUser(Ljava/lang/String;)V
    .locals 0
    .param p1, "user"    # Ljava/lang/String;

    .prologue
    .line 315
    iput-object p1, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    .line 316
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flag"    # I

    .prologue
    .line 137
    iget-wide v0, p0, Landroid/app/enterprise/ApnSettings;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 138
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 139
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->apn:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mcc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 141
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mnc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 142
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->user:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->server:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->password:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 145
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->proxy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 146
    iget v0, p0, Landroid/app/enterprise/ApnSettings;->port:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 147
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsProxy:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsPort:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 149
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->mmsc:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Landroid/app/enterprise/ApnSettings;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 151
    iget v0, p0, Landroid/app/enterprise/ApnSettings;->authType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    return-void
.end method
