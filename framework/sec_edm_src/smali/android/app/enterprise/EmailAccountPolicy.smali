.class public Landroid/app/enterprise/EmailAccountPolicy;
.super Ljava/lang/Object;
.source "EmailAccountPolicy.java"


# static fields
.field public static final ACCOUNT_TYPE_IMAP:Ljava/lang/String; = "imap"

.field public static final ACCOUNT_TYPE_POP3:Ljava/lang/String; = "pop3"

.field public static final ACTION_EMAIL_ACCOUNT_ADD_RESULT:Ljava/lang/String; = "edm.intent.action.EMAIL_ACCOUNT_ADD_RESULT"

.field public static final ACTION_EMAIL_ACCOUNT_DELETE_RESULT:Ljava/lang/String; = "edm.intent.action.EMAIL_ACCOUNT_DELETE_RESULT"

.field public static final EXTRA_ACCOUNT_ID:Ljava/lang/String; = "account_id"

.field public static final EXTRA_EMAIL_ADDRESS:Ljava/lang/String; = "email_id"

.field public static final EXTRA_INCOMING_PROTOCOL:Ljava/lang/String; = "protocol"

.field public static final EXTRA_INCOMING_SERVER_ADDRESS:Ljava/lang/String; = "receive_host"

.field public static final EXTRA_RESULT:Ljava/lang/String; = "result"

.field private static TAG:Ljava/lang/String;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mService:Landroid/app/enterprise/IEmailAccountPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-string v0, "EmailAccountPolicy"

    sput-object v0, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    const-string v0, "email_account_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    .line 179
    iput-object p1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 180
    return-void
.end method

.method private getService()Landroid/app/enterprise/IEmailAccountPolicy;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    if-nez v0, :cond_0

    .line 184
    const-string v0, "email_account_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IEmailAccountPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    .line 187
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    return-object v0
.end method


# virtual methods
.method public addNewAccount(Landroid/app/enterprise/EmailAccount;)J
    .locals 4
    .param p1, "account"    # Landroid/app/enterprise/EmailAccount;

    .prologue
    .line 2114
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.addNewAccount(EmailAccount)"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2115
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2117
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailAccountPolicy;->addNewAccount_new(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/EmailAccount;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2122
    :goto_0
    return-wide v2

    .line 2118
    :catch_0
    move-exception v0

    .line 2119
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2122
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public addNewAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)J
    .locals 14
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "inComingProtocol"    # Ljava/lang/String;
    .param p3, "inComingServerAddress"    # Ljava/lang/String;
    .param p4, "inComingServerPort"    # I
    .param p5, "inComingServerLogin"    # Ljava/lang/String;
    .param p6, "inComingServerPassword"    # Ljava/lang/String;
    .param p7, "outGoingProtocol"    # Ljava/lang/String;
    .param p8, "outGoingServerAddress"    # Ljava/lang/String;
    .param p9, "outGoingServerPort"    # I
    .param p10, "outGoingServerLogin"    # Ljava/lang/String;
    .param p11, "outGoingServerPassword"    # Ljava/lang/String;

    .prologue
    .line 263
    iget-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EmailAccountPolicy.addNewAccount"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 264
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 266
    :try_start_0
    iget-object v0, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    invoke-interface/range {v0 .. v12}, Landroid/app/enterprise/IEmailAccountPolicy;->addNewAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 274
    :goto_0
    return-wide v0

    .line 270
    :catch_0
    move-exception v13

    .line 271
    .local v13, "e":Landroid/os/RemoteException;
    sget-object v0, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed talking with email account policy"

    invoke-static {v0, v1, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 274
    .end local v13    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public addNewAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Z)J
    .locals 24
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "inComingProtocol"    # Ljava/lang/String;
    .param p3, "inComingServerAddress"    # Ljava/lang/String;
    .param p4, "inComingServerPort"    # I
    .param p5, "inComingServerLogin"    # Ljava/lang/String;
    .param p6, "inComingServerPassword"    # Ljava/lang/String;
    .param p7, "outGoingProtocol"    # Ljava/lang/String;
    .param p8, "outGoingServerAddress"    # Ljava/lang/String;
    .param p9, "outGoingServerPort"    # I
    .param p10, "outGoingServerLogin"    # Ljava/lang/String;
    .param p11, "outGoingServerPassword"    # Ljava/lang/String;
    .param p12, "outGoingServerUseSSL"    # Z
    .param p13, "outGoingServerUseTLS"    # Z
    .param p14, "outGoingServerAcceptAllCertificates"    # Z
    .param p15, "inComingServerUseSSL"    # Z
    .param p16, "inComingServerUseTLS"    # Z
    .param p17, "inComingServerAcceptAllCertificates"    # Z
    .param p18, "signature"    # Ljava/lang/String;
    .param p19, "isNotify"    # Z

    .prologue
    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "EmailAccountPolicy.addNewAccount"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 365
    invoke-direct/range {p0 .. p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 367
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    move-object/from16 v0, p0

    iget-object v3, v0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    move-object/from16 v13, p10

    move-object/from16 v14, p11

    move/from16 v15, p12

    move/from16 v16, p13

    move/from16 v17, p14

    move/from16 v18, p15

    move/from16 v19, p16

    move/from16 v20, p17

    move-object/from16 v21, p18

    move/from16 v22, p19

    invoke-interface/range {v2 .. v22}, Landroid/app/enterprise/IEmailAccountPolicy;->addNewAccount_ex(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZZZZZLjava/lang/String;Z)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 379
    :goto_0
    return-wide v2

    .line 375
    :catch_0
    move-exception v23

    .line 376
    .local v23, "e":Landroid/os/RemoteException;
    sget-object v2, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v3, "Failed talking with email account policy"

    move-object/from16 v0, v23

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 379
    .end local v23    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public deleteAccount(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1958
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.deleteAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1959
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1961
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailAccountPolicy;->deleteAccount(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1966
    :goto_0
    return v1

    .line 1962
    :catch_0
    move-exception v0

    .line 1963
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1966
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountDetails(J)Landroid/app/enterprise/Account;
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1897
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.getAccountDetails"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1898
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1900
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailAccountPolicy;->getAccountDetails(Landroid/app/enterprise/ContextInfo;J)Landroid/app/enterprise/Account;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1905
    :goto_0
    return-object v1

    .line 1901
    :catch_0
    move-exception v0

    .line 1902
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1905
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getAccountId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 4
    .param p1, "login"    # Ljava/lang/String;
    .param p2, "serverAddress"    # Ljava/lang/String;
    .param p3, "protocol"    # Ljava/lang/String;

    .prologue
    .line 1846
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.getAccountId"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1847
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1849
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->getAccountId(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1854
    :goto_0
    return-wide v2

    .line 1850
    :catch_0
    move-exception v0

    .line 1851
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1854
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public getAllEmailAccounts()[Landroid/app/enterprise/Account;
    .locals 3

    .prologue
    .line 2058
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.getAllEmailAccounts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2059
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2061
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy;->getAllEmailAccounts(Landroid/app/enterprise/ContextInfo;)[Landroid/app/enterprise/Account;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2066
    :goto_0
    return-object v1

    .line 2062
    :catch_0
    move-exception v0

    .line 2063
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2066
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSecurityInComingServerPassword(J)Ljava/lang/String;
    .locals 3
    .param p1, "callID"    # J

    .prologue
    .line 2146
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.getSecurityInComingServerPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2147
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2149
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailAccountPolicy;->getSecurityInComingServerPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2154
    :goto_0
    return-object v1

    .line 2150
    :catch_0
    move-exception v0

    .line 2151
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2154
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSecurityOutGoingServerPassword(J)Ljava/lang/String;
    .locals 3
    .param p1, "callID"    # J

    .prologue
    .line 2130
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.getSecurityOutGoingServerPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2131
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2133
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailAccountPolicy;->getSecurityOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;J)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2138
    :goto_0
    return-object v1

    .line 2134
    :catch_0
    move-exception v0

    .line 2135
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2138
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePendingAccount(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "incomingProtocol"    # Ljava/lang/String;
    .param p3, "incomingServerAddress"    # Ljava/lang/String;

    .prologue
    .line 2099
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.removePendingAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2100
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2102
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->removePendingAccount(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2107
    :cond_0
    :goto_0
    return-void

    .line 2103
    :catch_0
    move-exception v0

    .line 2104
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public sendAccountsChangedBroadcast()V
    .locals 3

    .prologue
    .line 2010
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.sendAccountsChangedBroadcast"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2011
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2013
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/IEmailAccountPolicy;->sendAccountsChangedBroadcast(Landroid/app/enterprise/ContextInfo;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2018
    :cond_0
    :goto_0
    return-void

    .line 2014
    :catch_0
    move-exception v0

    .line 2015
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public setAccountName(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 427
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setAccountName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 428
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 430
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setAccountName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 435
    :goto_0
    return v1

    .line 431
    :catch_0
    move-exception v0

    .line 432
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 435
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAlwaysVibrateOnEmailNotification(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 730
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setAlwaysVibrateOnEmailNotification"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 731
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 733
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setAlwaysVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 738
    :goto_0
    return v1

    .line 734
    :catch_0
    move-exception v0

    .line 735
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 738
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setAsDefaultAccount(J)Z
    .locals 3
    .param p1, "accId"    # J

    .prologue
    .line 1793
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setAsDefaultAccount"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1794
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1796
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/IEmailAccountPolicy;->setAsDefaultAccount(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1801
    :goto_0
    return v1

    .line 1797
    :catch_0
    move-exception v0

    .line 1798
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1801
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setEmailAddress(Ljava/lang/String;J)J
    .locals 4
    .param p1, "emailAddress"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 489
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setEmailAddress"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 490
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 492
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setEmailAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 497
    :goto_0
    return-wide v2

    .line 493
    :catch_0
    move-exception v0

    .line 494
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 497
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setInComingProtocol(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 844
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingProtocol"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 845
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 847
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingProtocol(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 852
    :goto_0
    return v1

    .line 848
    :catch_0
    move-exception v0

    .line 849
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 852
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInComingServerAcceptAllCertificates(ZJ)Z
    .locals 4
    .param p1, "acceptAllCertificates"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1079
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerAcceptAllCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1080
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1082
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1088
    :goto_0
    return v1

    .line 1084
    :catch_0
    move-exception v0

    .line 1085
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1088
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInComingServerAddress(Ljava/lang/String;J)J
    .locals 4
    .param p1, "serverAddress"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 907
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerAddress"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 908
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 910
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 915
    :goto_0
    return-wide v2

    .line 911
    :catch_0
    move-exception v0

    .line 912
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 915
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setInComingServerLogin(Ljava/lang/String;J)J
    .locals 4
    .param p1, "loginId"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1144
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerLogin"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1145
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1147
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerLogin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1152
    :goto_0
    return-wide v2

    .line 1148
    :catch_0
    move-exception v0

    .line 1149
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1152
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setInComingServerPassword(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1200
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1201
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1203
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1208
    :goto_0
    return v1

    .line 1204
    :catch_0
    move-exception v0

    .line 1205
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1208
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInComingServerPathPrefix(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "pathPrefix"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1261
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerPathPrefix"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1262
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1264
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerPathPrefix(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1269
    :goto_0
    return v1

    .line 1265
    :catch_0
    move-exception v0

    .line 1266
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1269
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInComingServerPort(IJ)Z
    .locals 4
    .param p1, "port"    # I
    .param p2, "accId"    # J

    .prologue
    .line 963
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerPort"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 964
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 966
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerPort(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 971
    :goto_0
    return v1

    .line 967
    :catch_0
    move-exception v0

    .line 968
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 971
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setInComingServerSSL(ZJ)Z
    .locals 4
    .param p1, "enableSSL"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1021
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setInComingServerSSL"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1022
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1024
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setInComingServerSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1029
    :goto_0
    return v1

    .line 1025
    :catch_0
    move-exception v0

    .line 1026
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1029
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingProtocol(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "protocol"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1320
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingProtocol"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1321
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1323
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingProtocol(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1328
    :goto_0
    return v1

    .line 1324
    :catch_0
    move-exception v0

    .line 1325
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1328
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingServerAcceptAllCertificates(ZJ)Z
    .locals 4
    .param p1, "acceptAllCertificates"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1557
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerAcceptAllCertificates"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1558
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1560
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerAcceptAllCertificates(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1566
    :goto_0
    return v1

    .line 1562
    :catch_0
    move-exception v0

    .line 1563
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1566
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingServerAddress(Ljava/lang/String;J)J
    .locals 4
    .param p1, "serverAddress"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1383
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerAddress"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1384
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1386
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerAddress(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1391
    :goto_0
    return-wide v2

    .line 1387
    :catch_0
    move-exception v0

    .line 1388
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1391
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setOutGoingServerLogin(Ljava/lang/String;J)J
    .locals 4
    .param p1, "loginId"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1624
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerLogin"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1625
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1627
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerLogin(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 1632
    :goto_0
    return-wide v2

    .line 1628
    :catch_0
    move-exception v0

    .line 1629
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1632
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setOutGoingServerPassword(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1681
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerPassword"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1682
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1684
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1689
    :goto_0
    return v1

    .line 1685
    :catch_0
    move-exception v0

    .line 1686
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1689
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingServerPathPrefix(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "pathPrefix"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 1742
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerPathPrefix"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1743
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1745
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerPathPrefix(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1750
    :goto_0
    return v1

    .line 1746
    :catch_0
    move-exception v0

    .line 1747
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1750
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingServerPort(IJ)Z
    .locals 4
    .param p1, "port"    # I
    .param p2, "accId"    # J

    .prologue
    .line 1440
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerPort"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1441
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1443
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerPort(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1448
    :goto_0
    return v1

    .line 1444
    :catch_0
    move-exception v0

    .line 1445
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1448
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setOutGoingServerSSL(ZJ)Z
    .locals 4
    .param p1, "enableSSL"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 1498
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setOutGoingServerSSL"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1499
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1501
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setOutGoingServerSSL(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1506
    :goto_0
    return v1

    .line 1502
    :catch_0
    move-exception v0

    .line 1503
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1506
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSecurityInComingServerPassword(Ljava/lang/String;)J
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 2178
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setSecurityInComingServerPassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2179
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2181
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailAccountPolicy;->setSecurityInComingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2186
    :goto_0
    return-wide v2

    .line 2182
    :catch_0
    move-exception v0

    .line 2183
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setAccountCertificatePassword "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2186
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setSecurityOutGoingServerPassword(Ljava/lang/String;)J
    .locals 4
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 2162
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setSecurityOutGoingServerPassword()"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 2163
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2165
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/IEmailAccountPolicy;->setSecurityOutGoingServerPassword(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 2170
    :goto_0
    return-wide v2

    .line 2166
    :catch_0
    move-exception v0

    .line 2167
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed setSecurityOutGoingServerPassword "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 2170
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public setSenderName(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "senderName"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 619
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setSenderName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 620
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 622
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setSenderName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 627
    :goto_0
    return v1

    .line 623
    :catch_0
    move-exception v0

    .line 624
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 627
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSignature(Ljava/lang/String;J)Z
    .locals 4
    .param p1, "signature"    # Ljava/lang/String;
    .param p2, "accId"    # J

    .prologue
    .line 674
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setSignature"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 675
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 677
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setSignature(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 682
    :goto_0
    return v1

    .line 678
    :catch_0
    move-exception v0

    .line 679
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 682
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSilentVibrateOnEmailNotification(ZJ)Z
    .locals 4
    .param p1, "enable"    # Z
    .param p2, "accId"    # J

    .prologue
    .line 789
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 791
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setSilentVibrateOnEmailNotification(Landroid/app/enterprise/ContextInfo;ZJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 796
    :goto_0
    return v1

    .line 792
    :catch_0
    move-exception v0

    .line 793
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 796
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSyncInterval(IJ)Z
    .locals 4
    .param p1, "syncInterval"    # I
    .param p2, "accId"    # J

    .prologue
    .line 563
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EmailAccountPolicy.setSyncInterval"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 564
    invoke-direct {p0}, Landroid/app/enterprise/EmailAccountPolicy;->getService()Landroid/app/enterprise/IEmailAccountPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 566
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/EmailAccountPolicy;->mService:Landroid/app/enterprise/IEmailAccountPolicy;

    iget-object v2, p0, Landroid/app/enterprise/EmailAccountPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2, p3}, Landroid/app/enterprise/IEmailAccountPolicy;->setSyncInterval(Landroid/app/enterprise/ContextInfo;IJ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 571
    :goto_0
    return v1

    .line 567
    :catch_0
    move-exception v0

    .line 568
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Landroid/app/enterprise/EmailAccountPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed talking with email account policy"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 571
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
