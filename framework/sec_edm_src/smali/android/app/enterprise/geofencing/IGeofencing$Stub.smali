.class public abstract Landroid/app/enterprise/geofencing/IGeofencing$Stub;
.super Landroid/os/Binder;
.source "IGeofencing.java"

# interfaces
.implements Landroid/app/enterprise/geofencing/IGeofencing;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/app/enterprise/geofencing/IGeofencing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/app/enterprise/geofencing/IGeofencing$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "android.app.enterprise.geofencing.IGeofencing"

.field static final TRANSACTION_createGeofence:I = 0x1

.field static final TRANSACTION_destroyGeofence:I = 0x2

.field static final TRANSACTION_getGeofences:I = 0x6

.field static final TRANSACTION_getMinDistanceParameter:I = 0xb

.field static final TRANSACTION_getMinTimeParameter:I = 0x9

.field static final TRANSACTION_isDeviceInsideGeofence:I = 0x7

.field static final TRANSACTION_isGeofencingEnabled:I = 0x5

.field static final TRANSACTION_setMinDistanceParameter:I = 0xa

.field static final TRANSACTION_setMinTimeParameter:I = 0x8

.field static final TRANSACTION_startGeofencing:I = 0x3

.field static final TRANSACTION_stopGeofencing:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p0, p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/geofencing/IGeofencing;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "android.app.enterprise.geofencing.IGeofencing"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Landroid/app/enterprise/geofencing/IGeofencing;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Landroid/app/enterprise/geofencing/IGeofencing;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Landroid/app/enterprise/geofencing/IGeofencing$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 9
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 228
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v7

    :goto_0
    return v7

    .line 46
    :sswitch_0
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_0

    .line 54
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v0, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_1

    .line 61
    sget-object v6, Landroid/app/enterprise/geofencing/Geofence;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/geofencing/Geofence;

    .line 66
    .local v2, "_arg1":Landroid/app/enterprise/geofencing/Geofence;
    :goto_2
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->createGeofence(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/geofencing/Geofence;)I

    move-result v4

    .line 67
    .local v4, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 68
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 57
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/geofencing/Geofence;
    .end local v4    # "_result":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 64
    :cond_1
    const/4 v2, 0x0

    .restart local v2    # "_arg1":Landroid/app/enterprise/geofencing/Geofence;
    goto :goto_2

    .line 73
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":Landroid/app/enterprise/geofencing/Geofence;
    :sswitch_2
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_3

    .line 76
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 82
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 83
    .local v2, "_arg1":I
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->destroyGeofence(Landroid/app/enterprise/ContextInfo;I)Z

    move-result v4

    .line 84
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    if-eqz v4, :cond_2

    move v6, v7

    :cond_2
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 79
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":I
    .end local v4    # "_result":Z
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 90
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_3
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_5

    .line 93
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 98
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_4
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->startGeofencing(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 99
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    if-eqz v4, :cond_4

    move v6, v7

    :cond_4
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 96
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_4

    .line 105
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_4
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_7

    .line 108
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 113
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->stopGeofencing(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 114
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 115
    if-eqz v4, :cond_6

    move v6, v7

    :cond_6
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 111
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 120
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_5
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_9

    .line 123
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 128
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_6
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->isGeofencingEnabled(Landroid/app/enterprise/ContextInfo;)Z

    move-result v4

    .line 129
    .restart local v4    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 130
    if-eqz v4, :cond_8

    move v6, v7

    :cond_8
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 126
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Z
    :cond_9
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_6

    .line 135
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_6
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_a

    .line 138
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 143
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->getGeofences(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v1

    .line 144
    .local v1, "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/geofencing/Geofence;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 145
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 141
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v1    # "_result":Ljava/util/List;, "Ljava/util/List<Landroid/app/enterprise/geofencing/Geofence;>;"
    :cond_a
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 150
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_7
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 152
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_b

    .line 153
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 158
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_8
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->isDeviceInsideGeofence(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;

    move-result-object v4

    .line 159
    .local v4, "_result":Ljava/util/List;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 156
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":Ljava/util/List;
    :cond_b
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_8

    .line 165
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_8
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_d

    .line 168
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 174
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    .line 175
    .local v2, "_arg1":J
    invoke-virtual {p0, v0, v2, v3}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->setMinTimeParameter(Landroid/app/enterprise/ContextInfo;J)Z

    move-result v4

    .line 176
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    if-eqz v4, :cond_c

    move v6, v7

    :cond_c
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 171
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":J
    .end local v4    # "_result":Z
    :cond_d
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 182
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_9
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_e

    .line 185
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 190
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_a
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->getMinTimeParameter(Landroid/app/enterprise/ContextInfo;)J

    move-result-wide v4

    .line 191
    .local v4, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 192
    invoke-virtual {p3, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 188
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":J
    :cond_e
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_a

    .line 197
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_a
    const-string v8, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v8}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v8

    if-eqz v8, :cond_10

    .line 200
    sget-object v8, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v8, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 206
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 207
    .local v2, "_arg1":F
    invoke-virtual {p0, v0, v2}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->setMinDistanceParameter(Landroid/app/enterprise/ContextInfo;F)Z

    move-result v4

    .line 208
    .local v4, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 209
    if-eqz v4, :cond_f

    move v6, v7

    :cond_f
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 203
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v2    # "_arg1":F
    .end local v4    # "_result":Z
    :cond_10
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 214
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :sswitch_b
    const-string v6, "android.app.enterprise.geofencing.IGeofencing"

    invoke-virtual {p2, v6}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v6

    if-eqz v6, :cond_11

    .line 217
    sget-object v6, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v6, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/ContextInfo;

    .line 222
    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_c
    invoke-virtual {p0, v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->getMinDistanceParameter(Landroid/app/enterprise/ContextInfo;)F

    move-result v4

    .line 223
    .local v4, "_result":F
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 224
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeFloat(F)V

    goto/16 :goto_0

    .line 220
    .end local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v4    # "_result":F
    :cond_11
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_c

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
