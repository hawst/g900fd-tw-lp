.class public Landroid/app/enterprise/geofencing/Geofencing;
.super Ljava/lang/Object;
.source "Geofencing.java"


# static fields
.field public static final ACTION_DEVICE_INSIDE_GEOFENCE:Ljava/lang/String; = "edm.intent.action.device.inside"

.field public static final ACTION_DEVICE_LOCATION_UNAVAILABLE:Ljava/lang/String; = "edm.intent.action.device.location.unavailable"

.field public static final ACTION_DEVICE_OUTSIDE_GEOFENCE:Ljava/lang/String; = "edm.intent.action.device.outside"

.field public static final ERROR_GEOFENCING_FAILED:I = -0x1

.field public static final ERROR_NONE:I = 0x0

.field public static final ERROR_UNKNOWN:I = -0x7d0

.field public static final INTENT_EXTRA_ID:Ljava/lang/String; = "edm.intent.extra.geofence.id"

.field public static final INTENT_EXTRA_USER_ID:Ljava/lang/String; = "edm.intent.extra.geofence.user.id"

.field private static final TAG:Ljava/lang/String; = "Geofencing"

.field public static final TYPE_CIRCLE:I = 0x1

.field public static final TYPE_LINEAR:I = 0x3

.field public static final TYPE_POLYGON:I = 0x2

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/app/enterprise/geofencing/Geofencing;->mSync:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iput-object p2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContext:Landroid/content/Context;

    .line 139
    iput-object p1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 140
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 192
    new-instance v0, Landroid/app/enterprise/geofencing/Geofencing;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Landroid/app/enterprise/geofencing/Geofencing;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 179
    sget-object v1, Landroid/app/enterprise/geofencing/Geofencing;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 180
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 181
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    monitor-exit v1

    .line 183
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/app/enterprise/geofencing/Geofencing;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/app/enterprise/geofencing/Geofencing;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v1

    goto :goto_0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/app/enterprise/geofencing/Geofencing;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 166
    sget-object v2, Landroid/app/enterprise/geofencing/Geofencing;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 167
    if-nez p0, :cond_0

    .line 168
    const/4 v1, 0x0

    :try_start_0
    monitor-exit v2

    .line 171
    :goto_0
    return-object v1

    .line 170
    :cond_0
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 171
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Landroid/app/enterprise/geofencing/Geofencing;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Landroid/app/enterprise/geofencing/Geofencing;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    monitor-exit v2

    goto :goto_0

    .line 172
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Landroid/app/enterprise/geofencing/IGeofencing;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    if-nez v0, :cond_0

    .line 144
    const-string v0, "geofencing"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/geofencing/IGeofencing$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v0

    iput-object v0, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    .line 146
    :cond_0
    iget-object v0, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    return-object v0
.end method


# virtual methods
.method public createGeofence(Landroid/app/enterprise/geofencing/Geofence;)I
    .locals 4
    .param p1, "geofence"    # Landroid/app/enterprise/geofencing/Geofence;

    .prologue
    .line 272
    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "Geofencing.createGeofence"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 273
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 275
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v3, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3, p1}, Landroid/app/enterprise/geofencing/IGeofencing;->createGeofence(Landroid/app/enterprise/ContextInfo;Landroid/app/enterprise/geofencing/Geofence;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 282
    :goto_0
    return v1

    .line 278
    :catch_0
    move-exception v0

    .line 279
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "Geofencing"

    const-string v3, "Failed talking with geofencing service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 282
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public destroyGeofence(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 316
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.destroyGeofence"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 317
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 319
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/geofencing/IGeofencing;->destroyGeofence(Landroid/app/enterprise/ContextInfo;I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 325
    :goto_0
    return v1

    .line 321
    :catch_0
    move-exception v0

    .line 322
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 325
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getGeofences()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/app/enterprise/geofencing/Geofence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 357
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.getGeofences"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 358
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 360
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/geofencing/IGeofencing;->getGeofences(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 366
    :goto_0
    return-object v1

    .line 362
    :catch_0
    move-exception v0

    .line 363
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 366
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMinDistanceParameter()F
    .locals 4

    .prologue
    .line 624
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 626
    :try_start_0
    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v3, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Landroid/app/enterprise/geofencing/IGeofencing;->getMinDistanceParameter(Landroid/app/enterprise/ContextInfo;)F
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 633
    :goto_0
    return v1

    .line 629
    :catch_0
    move-exception v0

    .line 630
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "Geofencing"

    const-string v3, "Failed talking with geofencing service"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 633
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public getMinTimeParameter()J
    .locals 5

    .prologue
    .line 566
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 568
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v4, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v4}, Landroid/app/enterprise/geofencing/IGeofencing;->getMinTimeParameter(Landroid/app/enterprise/ContextInfo;)J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 575
    :goto_0
    return-wide v2

    .line 571
    :catch_0
    move-exception v0

    .line 572
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v4, "Failed talking with geofencing service"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 575
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-wide/16 v2, -0x1

    goto :goto_0
.end method

.method public isDeviceInsideGeofence()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 510
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.isDeviceInsideGeofence"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 511
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 513
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/geofencing/IGeofencing;->isDeviceInsideGeofence(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 519
    :goto_0
    return-object v1

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 519
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isGeofencingEnabled()Z
    .locals 3

    .prologue
    .line 481
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.isGeofencingEnabled"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 482
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 484
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/geofencing/IGeofencing;->isGeofencingEnabled(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 490
    :goto_0
    return v1

    .line 486
    :catch_0
    move-exception v0

    .line 487
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 490
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMinDistanceParameter(F)Z
    .locals 3
    .param p1, "distance"    # F

    .prologue
    .line 594
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.setMinDistanceParameter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 595
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 597
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Landroid/app/enterprise/geofencing/IGeofencing;->setMinDistanceParameter(Landroid/app/enterprise/ContextInfo;F)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 603
    :goto_0
    return v1

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 603
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setMinTimeParameter(J)Z
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 536
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.setMinTimeParameter"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 537
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 539
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Landroid/app/enterprise/geofencing/IGeofencing;->setMinTimeParameter(Landroid/app/enterprise/ContextInfo;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 545
    :goto_0
    return v1

    .line 541
    :catch_0
    move-exception v0

    .line 542
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 545
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public startGeofencing()Z
    .locals 3

    .prologue
    .line 407
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.startGeofencing"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 408
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 410
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/geofencing/IGeofencing;->startGeofencing(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 416
    :goto_0
    return v1

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 416
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopGeofencing()Z
    .locals 3

    .prologue
    .line 452
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "Geofencing.stopGeofencing"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 453
    invoke-direct {p0}, Landroid/app/enterprise/geofencing/Geofencing;->getService()Landroid/app/enterprise/geofencing/IGeofencing;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 455
    :try_start_0
    iget-object v1, p0, Landroid/app/enterprise/geofencing/Geofencing;->mGeofenceService:Landroid/app/enterprise/geofencing/IGeofencing;

    iget-object v2, p0, Landroid/app/enterprise/geofencing/Geofencing;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Landroid/app/enterprise/geofencing/IGeofencing;->stopGeofencing(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 461
    :goto_0
    return v1

    .line 457
    :catch_0
    move-exception v0

    .line 458
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "Geofencing"

    const-string v2, "Failed talking with geofencing service"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 461
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
