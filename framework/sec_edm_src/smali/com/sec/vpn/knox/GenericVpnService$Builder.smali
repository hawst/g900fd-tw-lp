.class public Lcom/sec/vpn/knox/GenericVpnService$Builder;
.super Ljava/lang/Object;
.source "GenericVpnService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/vpn/knox/GenericVpnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Builder"
.end annotation


# instance fields
.field private final mAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final mConfig:Lcom/android/internal/net/VpnConfig;

.field private final mRoutes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/vpn/knox/GenericVpnService;


# direct methods
.method public constructor <init>(Lcom/sec/vpn/knox/GenericVpnService;)V
    .locals 2

    .prologue
    .line 378
    iput-object p1, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->this$0:Lcom/sec/vpn/knox/GenericVpnService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 374
    new-instance v0, Lcom/android/internal/net/VpnConfig;

    invoke-direct {v0}, Lcom/android/internal/net/VpnConfig;-><init>()V

    iput-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    .line 375
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mAddresses:Ljava/util/List;

    .line 376
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mRoutes:Ljava/util/List;

    .line 379
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    .line 380
    return-void
.end method


# virtual methods
.method public addAddress(Ljava/lang/String;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "prefixLength"    # I

    .prologue
    .line 444
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/vpn/knox/GenericVpnService$Builder;->addAddress(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addAddress(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 2
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "prefixLength"    # I

    .prologue
    .line 425
    # invokes: Lcom/sec/vpn/knox/GenericVpnService;->check(Ljava/net/InetAddress;I)V
    invoke-static {p1, p2}, Lcom/sec/vpn/knox/GenericVpnService;->access$100(Ljava/net/InetAddress;I)V

    .line 427
    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 428
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mAddresses:Ljava/util/List;

    new-instance v1, Landroid/net/LinkAddress;

    invoke-direct {v1, p1, p2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 432
    return-object p0
.end method

.method public addAllowedApplication(Ljava/lang/String;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 577
    return-object p0
.end method

.method public addDisallowedApplication(Ljava/lang/String;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 602
    return-object p0
.end method

.method public addDnsServer(Ljava/lang/String;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 510
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vpn/knox/GenericVpnService$Builder;->addDnsServer(Ljava/net/InetAddress;)Lcom/sec/vpn/knox/GenericVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addDnsServer(Ljava/net/InetAddress;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 2
    .param p1, "address"    # Ljava/net/InetAddress;

    .prologue
    .line 491
    invoke-virtual {p1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 492
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 494
    :cond_1
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    if-nez v0, :cond_2

    .line 495
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    .line 497
    :cond_2
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 498
    return-object p0
.end method

.method public addRoute(Ljava/lang/String;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "prefixLength"    # I

    .prologue
    .line 480
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/vpn/knox/GenericVpnService$Builder;->addRoute(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addRoute(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 6
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "prefixLength"    # I

    .prologue
    .line 454
    # invokes: Lcom/sec/vpn/knox/GenericVpnService;->check(Ljava/net/InetAddress;I)V
    invoke-static {p1, p2}, Lcom/sec/vpn/knox/GenericVpnService;->access$100(Ljava/net/InetAddress;I)V

    .line 456
    div-int/lit8 v1, p2, 0x8

    .line 457
    .local v1, "offset":I
    invoke-virtual {p1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 458
    .local v0, "bytes":[B
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 459
    aget-byte v2, v0, v1

    rem-int/lit8 v3, p2, 0x8

    shl-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 460
    aget-byte v2, v0, v1

    if-eqz v2, :cond_0

    .line 461
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Bad address"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 459
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 466
    :cond_1
    iget-object v2, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mRoutes:Ljava/util/List;

    new-instance v3, Landroid/net/RouteInfo;

    new-instance v4, Landroid/net/LinkAddress;

    invoke-direct {v4, p1, p2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 467
    iget-object v2, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    invoke-virtual {v2, p1}, Lcom/android/internal/net/VpnConfig;->updateAllowedFamilies(Ljava/net/InetAddress;)V

    .line 468
    return-object p0
.end method

.method public addSearchDomain(Ljava/lang/String;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 2
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 517
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    if-nez v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    .line 520
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 521
    return-object p0
.end method

.method public allowBypass()Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 2

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/android/internal/net/VpnConfig;->allowBypass:Z

    .line 617
    return-object p0
.end method

.method public allowFamily(I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 3
    .param p1, "family"    # I

    .prologue
    const/4 v1, 0x1

    .line 543
    sget v0, Landroid/system/OsConstants;->AF_INET:I

    if-ne p1, v0, :cond_0

    .line 544
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-boolean v1, v0, Lcom/android/internal/net/VpnConfig;->allowIPv4:Z

    .line 551
    :goto_0
    return-object p0

    .line 545
    :cond_0
    sget v0, Landroid/system/OsConstants;->AF_INET6:I

    if-ne p1, v0, :cond_1

    .line 546
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-boolean v1, v0, Lcom/android/internal/net/VpnConfig;->allowIPv6:Z

    goto :goto_0

    .line 548
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is neither "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/system/OsConstants;->AF_INET:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " nor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Landroid/system/OsConstants;->AF_INET6:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public establish()Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 669
    const-string v1, "GenericVpnService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "establish is getting called : mVpnProfileName value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    # getter for: Lcom/sec/vpn/knox/GenericVpnService;->mVpnProfileName:Ljava/lang/String;
    invoke-static {}, Lcom/sec/vpn/knox/GenericVpnService;->access$200()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "config session value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-object v1, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mAddresses:Ljava/util/List;

    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/util/List;

    .line 671
    iget-object v1, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mRoutes:Ljava/util/List;

    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->routes:Ljava/util/List;

    .line 673
    :try_start_0
    # invokes: Lcom/sec/vpn/knox/GenericVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/GenericVpnService;->access$300()Landroid/net/IConnectivityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 674
    # invokes: Lcom/sec/vpn/knox/GenericVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/GenericVpnService;->access$300()Landroid/net/IConnectivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, v3, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/net/IConnectivityManager;->establishEnterpriseVpn(Lcom/android/internal/net/VpnConfig;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 676
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 677
    :catch_0
    move-exception v0

    .line 678
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "VPN establish failed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setBlocking(Z)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "blocking"    # Z

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-boolean p1, v0, Lcom/android/internal/net/VpnConfig;->blocking:Z

    .line 631
    return-object p0
.end method

.method public setConfigureIntent(Landroid/app/PendingIntent;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "intent"    # Landroid/app/PendingIntent;

    .prologue
    .line 397
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-object p1, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    .line 398
    return-object p0
.end method

.method public setMtu(I)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 2
    .param p1, "mtu"    # I

    .prologue
    .line 408
    if-gtz p1, :cond_0

    .line 409
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad mtu"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput p1, v0, Lcom/android/internal/net/VpnConfig;->mtu:I

    .line 412
    return-object p0
.end method

.method public setSession(Ljava/lang/String;)Lcom/sec/vpn/knox/GenericVpnService$Builder;
    .locals 1
    .param p1, "session"    # Ljava/lang/String;

    .prologue
    .line 387
    iget-object v0, p0, Lcom/sec/vpn/knox/GenericVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-object p1, v0, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    .line 388
    return-object p0
.end method
