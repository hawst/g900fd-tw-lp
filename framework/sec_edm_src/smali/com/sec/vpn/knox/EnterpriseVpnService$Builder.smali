.class public Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
.super Ljava/lang/Object;
.source "EnterpriseVpnService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/vpn/knox/EnterpriseVpnService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Builder"
.end annotation


# instance fields
.field private final mAddresses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/LinkAddress;",
            ">;"
        }
    .end annotation
.end field

.field private final mConfig:Lcom/android/internal/net/VpnConfig;

.field private final mRoutes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/RouteInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mTunHandle:Landroid/os/ParcelFileDescriptor;

.field private mVpnProfileName:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/vpn/knox/EnterpriseVpnService;


# direct methods
.method public constructor <init>(Lcom/sec/vpn/knox/EnterpriseVpnService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p2, "mPackage"    # Ljava/lang/String;
    .param p3, "vpnProfileName"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->this$0:Lcom/sec/vpn/knox/EnterpriseVpnService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    new-instance v1, Lcom/android/internal/net/VpnConfig;

    invoke-direct {v1}, Lcom/android/internal/net/VpnConfig;-><init>()V

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    .line 151
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mAddresses:Ljava/util/List;

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mRoutes:Ljava/util/List;

    .line 153
    const-string v1, ""

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mVpnProfileName:Ljava/lang/String;

    .line 154
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mTunHandle:Landroid/os/ParcelFileDescriptor;

    .line 157
    iget-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->user:Ljava/lang/String;

    .line 158
    iput-object p3, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mVpnProfileName:Ljava/lang/String;

    .line 159
    iget-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-object p3, v1, Lcom/android/internal/net/VpnConfig;->session:Ljava/lang/String;

    .line 161
    :try_start_0
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 162
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    invoke-interface {v1, p2, p3}, Landroid/net/IConnectivityManager;->createEnterpriseVpn(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :cond_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    .line 165
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "VPN Object creation failed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private check(Ljava/net/InetAddress;I)V
    .locals 2
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "prefixLength"    # I

    .prologue
    .line 249
    invoke-virtual {p1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_0
    instance-of v0, p1, Ljava/net/Inet4Address;

    if-eqz v0, :cond_2

    .line 253
    if-ltz p2, :cond_1

    const/16 v0, 0x20

    if-le p2, v0, :cond_5

    .line 254
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad prefixLength"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 256
    :cond_2
    instance-of v0, p1, Ljava/net/Inet6Address;

    if-eqz v0, :cond_4

    .line 257
    if-ltz p2, :cond_3

    const/16 v0, 0x80

    if-le p2, v0, :cond_5

    .line 258
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad prefixLength"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 261
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported family"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 263
    :cond_5
    return-void
.end method


# virtual methods
.method public addAddress(Ljava/lang/String;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "prefixLength"    # I

    .prologue
    .line 292
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->addAddress(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addAddress(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 2
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "prefixLength"    # I

    .prologue
    .line 273
    invoke-direct {p0, p1, p2}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->check(Ljava/net/InetAddress;I)V

    .line 275
    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mAddresses:Ljava/util/List;

    new-instance v1, Landroid/net/LinkAddress;

    invoke-direct {v1, p1, p2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    return-object p0
.end method

.method public addDnsServer(Ljava/lang/String;)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 357
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->addDnsServer(Ljava/net/InetAddress;)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addDnsServer(Ljava/net/InetAddress;)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 2
    .param p1, "address"    # Ljava/net/InetAddress;

    .prologue
    .line 338
    invoke-virtual {p1}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/InetAddress;->isAnyLocalAddress()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 339
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad address"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    if-nez v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    .line 344
    :cond_2
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->dnsServers:Ljava/util/List;

    invoke-virtual {p1}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 345
    return-object p0
.end method

.method public addRoute(Ljava/lang/String;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 1
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "prefixLength"    # I

    .prologue
    .line 327
    invoke-static {p1}, Ljava/net/InetAddress;->parseNumericAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->addRoute(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;

    move-result-object v0

    return-object v0
.end method

.method public addRoute(Ljava/net/InetAddress;I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 6
    .param p1, "address"    # Ljava/net/InetAddress;
    .param p2, "prefixLength"    # I

    .prologue
    .line 302
    invoke-direct {p0, p1, p2}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->check(Ljava/net/InetAddress;I)V

    .line 304
    div-int/lit8 v1, p2, 0x8

    .line 305
    .local v1, "offset":I
    invoke-virtual {p1}, Ljava/net/InetAddress;->getAddress()[B

    move-result-object v0

    .line 306
    .local v0, "bytes":[B
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 307
    aget-byte v2, v0, v1

    rem-int/lit8 v3, p2, 0x8

    shl-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 308
    aget-byte v2, v0, v1

    if-eqz v2, :cond_0

    .line 309
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Bad address"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 307
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 314
    :cond_1
    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mRoutes:Ljava/util/List;

    new-instance v3, Landroid/net/RouteInfo;

    new-instance v4, Landroid/net/LinkAddress;

    invoke-direct {v4, p1, p2}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/net/RouteInfo;-><init>(Landroid/net/LinkAddress;Ljava/net/InetAddress;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 315
    return-object p0
.end method

.method public addSearchDomain(Ljava/lang/String;)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 2
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    if-nez v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v0, v0, Lcom/android/internal/net/VpnConfig;->searchDomains:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 368
    return-object p0
.end method

.method public disconnect()V
    .locals 4

    .prologue
    .line 452
    const/4 v1, 0x0

    .line 454
    .local v1, "isConnectedProfile":Z
    :try_start_0
    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mTunHandle:Landroid/os/ParcelFileDescriptor;

    if-eqz v2, :cond_0

    .line 455
    const/4 v1, 0x1

    .line 456
    const-string v2, "EnterpriseVpnService"

    const-string v3, "Tunnel is getting closed"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mTunHandle:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 459
    :cond_0
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 460
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mVpnProfileName:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Landroid/net/IConnectivityManager;->disconnect(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 465
    :cond_1
    return-void

    .line 462
    :catch_0
    move-exception v0

    .line 463
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Exception in disconnect interface"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public establish()Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    .line 413
    iget-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mAddresses:Ljava/util/List;

    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->addresses:Ljava/util/List;

    .line 414
    iget-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mRoutes:Ljava/util/List;

    iput-object v2, v1, Lcom/android/internal/net/VpnConfig;->routes:Ljava/util/List;

    .line 416
    :try_start_0
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 417
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iget-object v3, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mVpnProfileName:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Landroid/net/IConnectivityManager;->establishEnterpriseVpn(Lcom/android/internal/net/VpnConfig;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mTunHandle:Landroid/os/ParcelFileDescriptor;

    .line 419
    :cond_0
    iget-object v1, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mTunHandle:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 420
    :catch_0
    move-exception v0

    .line 421
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "VPN establish failed"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getInterfaceName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 434
    :try_start_0
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 435
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mVpnProfileName:Ljava/lang/String;

    invoke-interface {v1, v2}, Landroid/net/IConnectivityManager;->getInterfaceName(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 437
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Exception in getInterfaceName"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public protect(I)Z
    .locals 4
    .param p1, "socket"    # I

    .prologue
    .line 185
    :try_start_0
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 186
    # invokes: Lcom/sec/vpn/knox/EnterpriseVpnService;->getService()Landroid/net/IConnectivityManager;
    invoke-static {}, Lcom/sec/vpn/knox/EnterpriseVpnService;->access$100()Landroid/net/IConnectivityManager;

    move-result-object v1

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-interface {v1, v2}, Landroid/net/IConnectivityManager;->getChainingEnabledForProfile(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 187
    invoke-static {p1}, Landroid/net/NetworkUtils;->protectFromVpn(I)Z

    move-result v1

    .line 196
    :goto_0
    return v1

    .line 189
    :cond_0
    const-string v1, "EnterpriseVpnService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "protect is not going to be called for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    const/4 v1, 0x1

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {p1}, Landroid/net/NetworkUtils;->protectFromVpn(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public protect(Ljava/net/DatagramSocket;Ljava/lang/String;)Z
    .locals 1
    .param p1, "socket"    # Ljava/net/DatagramSocket;
    .param p2, "mPackage"    # Ljava/lang/String;

    .prologue
    .line 218
    invoke-virtual {p1}, Ljava/net/DatagramSocket;->getFileDescriptor$()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->getInt$()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->protect(I)Z

    move-result v0

    return v0
.end method

.method public protect(Ljava/net/Socket;)Z
    .locals 1
    .param p1, "socket"    # Ljava/net/Socket;

    .prologue
    .line 207
    invoke-virtual {p1}, Ljava/net/Socket;->getFileDescriptor$()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->getInt$()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->protect(I)Z

    move-result v0

    return v0
.end method

.method public setConfigureIntent(Landroid/app/PendingIntent;)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 1
    .param p1, "intent"    # Landroid/app/PendingIntent;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput-object p1, v0, Lcom/android/internal/net/VpnConfig;->configureIntent:Landroid/app/PendingIntent;

    .line 228
    return-object p0
.end method

.method public setMtu(I)Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;
    .locals 2
    .param p1, "mtu"    # I

    .prologue
    .line 238
    if-gtz p1, :cond_0

    .line 239
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Bad mtu"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/sec/vpn/knox/EnterpriseVpnService$Builder;->mConfig:Lcom/android/internal/net/VpnConfig;

    iput p1, v0, Lcom/android/internal/net/VpnConfig;->mtu:I

    .line 242
    return-object p0
.end method
