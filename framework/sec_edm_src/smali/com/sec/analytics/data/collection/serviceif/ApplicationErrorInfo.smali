.class public Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;
.super Ljava/lang/Object;
.source "ApplicationErrorInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final ACTION_APP_ERROR:Ljava/lang/String; = "com.sec.analytics.intent.action.APP_ERROR"

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final EXTRA_APP_ERROR_INFO:Ljava/lang/String; = "com.sec.analytics.intent.extra.APP_ERROR_INFO"


# instance fields
.field private anr:I

.field private javaCrash:I

.field private nativeCrash:I

.field private packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo$1;

    invoke-direct {v0}, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo$1;-><init>()V

    sput-object v0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    invoke-virtual {p0, p1}, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->readFromParcel(Landroid/os/Parcel;)V

    .line 67
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo$1;

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->packageName:Ljava/lang/String;

    .line 71
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method public getAnr()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->anr:I

    return v0
.end method

.method public getJavaCrash()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->javaCrash:I

    return v0
.end method

.method public getNativeCrash()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->nativeCrash:I

    return v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->packageName:Ljava/lang/String;

    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 107
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->packageName:Ljava/lang/String;

    .line 108
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->javaCrash:I

    .line 109
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->nativeCrash:I

    .line 110
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->anr:I

    .line 111
    return-void
.end method

.method public setAnr(I)V
    .locals 0
    .param p1, "anr"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->anr:I

    .line 118
    return-void
.end method

.method public setJavaCrash(I)V
    .locals 0
    .param p1, "javaCrash"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->javaCrash:I

    .line 125
    return-void
.end method

.method public setNativeCrash(I)V
    .locals 0
    .param p1, "nativeCrash"    # I

    .prologue
    .line 131
    iput p1, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->nativeCrash:I

    .line 132
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->javaCrash:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 138
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->nativeCrash:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 139
    iget v0, p0, Lcom/sec/analytics/data/collection/serviceif/ApplicationErrorInfo;->anr:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    return-void
.end method
