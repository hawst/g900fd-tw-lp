.class public Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;
.super Ljava/lang/Object;
.source "DataCollectionManager.java"


# static fields
.field public static final SEC_ANALYTICS_SERVICE:Ljava/lang/String; = "sec_analytics"

.field private static binder:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;

.field private static mInstance:Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;

.field private static mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager$1;

    invoke-direct {v0}, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager$1;-><init>()V

    sput-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->binder:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    return-void
.end method

.method public static declared-synchronized getLogCollector(Landroid/content/Context;)Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const-class v1, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mInstance:Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;

    if-nez v0, :cond_0

    .line 64
    new-instance v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;

    invoke-direct {v0}, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;-><init>()V

    sput-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mInstance:Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;

    .line 65
    const-string v0, "sec_analytics"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    move-result-object v0

    sput-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    .line 68
    :cond_0
    sget-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mInstance:Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static getService()Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    if-nez v0, :cond_0

    .line 76
    const-string v0, "SEC_ANALYTICS_SERVICE"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    move-result-object v0

    sput-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    .line 79
    :cond_0
    sget-object v0, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    return-object v0
.end method


# virtual methods
.method public disableLogCollection(ILandroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1, "logGroups"    # I
    .param p2, "file"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 93
    invoke-static {}, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->getService()Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 95
    :try_start_0
    sget-object v1, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    sget-object v2, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->binder:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;->disableLogCollection(Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;ILandroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public enableLogCollection(ILandroid/os/ParcelFileDescriptor;)V
    .locals 3
    .param p1, "logGroups"    # I
    .param p2, "file"    # Landroid/os/ParcelFileDescriptor;

    .prologue
    .line 83
    invoke-static {}, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->getService()Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 85
    :try_start_0
    sget-object v1, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->mService:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;

    sget-object v2, Lcom/sec/analytics/data/collection/serviceif/DataCollectionManager;->binder:Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManager;->enableLogCollection(Lcom/sec/analytics/data/collection/serviceif/IDataCollectionManagerCb;ILandroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
