.class public interface abstract Lcom/sec/analytics/data/collection/serviceif/LogCollector;
.super Ljava/lang/Object;
.source "LogCollector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/analytics/data/collection/serviceif/LogCollector$LogCollectorCallback;
    }
.end annotation


# static fields
.field public static final LOG_GROUP_APPLICATION:I = 0x1

.field public static final LOG_GROUP_BROWSER:I = 0x2

.field public static final LOG_GROUP_INPUT:I = 0x4


# virtual methods
.method public abstract getActiveLogGroups()I
.end method

.method public abstract getSupportedLogGroups()I
.end method

.method public abstract registerLogCollectorCallback(Lcom/sec/analytics/data/collection/serviceif/LogCollector$LogCollectorCallback;)V
.end method

.method public abstract restartLogCollection(I)V
.end method

.method public abstract startLogCollection(I)V
.end method

.method public abstract stopLogCollection()V
.end method

.method public abstract unregisterLogCollectorCallback(Lcom/sec/analytics/data/collection/serviceif/LogCollector$LogCollectorCallback;)V
.end method
