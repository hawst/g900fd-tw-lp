.class public Lcom/sec/knox/container/ContainerCreationParams;
.super Ljava/lang/Object;
.source "ContainerCreationParams.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/knox/container/ContainerCreationParams;",
            ">;"
        }
    .end annotation
.end field

.field public static final LOCK_TYPE_FINGER_PRINT:I = 0x1

.field public static final LOCK_TYPE_PASSWORD:I = 0x0

.field public static final LOCK_TYPE_PATTERN:I = 0x3

.field public static final LOCK_TYPE_PIN:I = 0x2

.field public static final STATE_SETUP_POST_CREATE:I = 0x1

.field public static final STATE_SETUP_PROGRESS:I = 0x0

.field public static final STATE_WIZARD_EXIT_CLEAN:I = 0x2

.field private static final TAG:Ljava/lang/String; = "ContainerCreationParams"


# instance fields
.field private isMigrationFlow:Z

.field private mAdminParam:Ljava/lang/String;

.field private mAdminRemovable:Z

.field private mAdminUid:I

.field private mBackupPin:Ljava/lang/String;

.field private mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

.field private mCreatorUid:I

.field private mFeatureType:Ljava/lang/String;

.field private mFingerprintPlus:Z

.field private mFlags:I

.field private mLockType:I

.field private mName:Ljava/lang/String;

.field private mPackagePoliciesMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPassword:Ljava/lang/String;

.field private mRequestId:I

.field private mRequestState:I

.field private mResetPwdKey:Ljava/lang/String;

.field private mRestoreSelected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 203
    new-instance v0, Lcom/sec/knox/container/ContainerCreationParams$1;

    invoke-direct {v0}, Lcom/sec/knox/container/ContainerCreationParams$1;-><init>()V

    sput-object v0, Lcom/sec/knox/container/ContainerCreationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    .line 26
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    .line 27
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    .line 28
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    .line 30
    iput v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestState:I

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    .line 33
    iput v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    .line 34
    iput v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    .line 35
    iput v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    .line 37
    iput v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    .line 40
    iput-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    .line 42
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 45
    iput-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    .line 49
    iput-object v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    .line 200
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    .line 26
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    .line 27
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    .line 28
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    .line 30
    iput v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestState:I

    .line 32
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    .line 33
    iput v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    .line 34
    iput v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    .line 35
    iput v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    .line 36
    iput-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    .line 37
    iput v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    .line 39
    iput-boolean v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    .line 40
    iput-boolean v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    .line 42
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 45
    iput-boolean v2, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    .line 49
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    .line 217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    .line 218
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    .line 221
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    .line 222
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    .line 225
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    .line 226
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    .line 229
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    .line 230
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 231
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    .line 233
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    .line 234
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 235
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    .line 237
    :cond_4
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    .line 238
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    .line 239
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    .line 240
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    .line 241
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    .line 242
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    .line 243
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    .line 244
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 247
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    .line 249
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_3
    iput-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    .line 251
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    .line 252
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 253
    iput-object v3, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    .line 256
    :cond_5
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0}, Lcom/sec/knox/container/ContainerCreationParams;->deserializePackagePolicies(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 257
    return-void

    :cond_6
    move v0, v2

    .line 242
    goto :goto_0

    :cond_7
    move v0, v2

    .line 243
    goto :goto_1

    :cond_8
    move v0, v2

    .line 247
    goto :goto_2

    :cond_9
    move v1, v2

    .line 249
    goto :goto_3
.end method

.method private deserializePackagePolicies(Landroid/os/Parcel;Ljava/util/HashMap;)V
    .locals 10
    .param p1, "in"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 370
    .local p2, "packagePolicies":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Object;>;>;"
    const/4 v6, 0x0

    .line 371
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v7, 0x0

    .line 372
    .local v7, "propertyCount":I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 374
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_3

    .line 375
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 376
    .local v0, "app":Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 377
    .restart local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v7

    .line 378
    const/4 v5, 0x1

    .line 379
    .local v5, "k":I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v7, :cond_2

    .line 380
    const/4 v9, 0x1

    if-ne v5, v9, :cond_1

    .line 381
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 382
    .local v8, "value":Ljava/lang/String;
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    .end local v8    # "value":Ljava/lang/String;
    :cond_0
    :goto_2
    add-int/lit8 v5, v5, 0x1

    .line 379
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 383
    :cond_1
    const/4 v9, 0x2

    if-ne v5, v9, :cond_0

    .line 384
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v9

    new-array v1, v9, [B

    .line 385
    .local v1, "bData":[B
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    .line 386
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 390
    .end local v1    # "bData":[B
    :cond_2
    invoke-virtual {p2, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 392
    .end local v0    # "app":Ljava/lang/String;
    .end local v4    # "j":I
    .end local v5    # "k":I
    :cond_3
    return-void
.end method

.method private serializePackagePolicies(Landroid/os/Parcel;Ljava/util/HashMap;)V
    .locals 11
    .param p1, "dest"    # Landroid/os/Parcel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .local p2, "packagePolicies":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Object;>;>;"
    const/4 v10, 0x0

    .line 339
    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 340
    .local v5, "keySet":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_4

    .line 341
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v9

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 342
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 343
    .local v7, "pkg":Ljava/lang/String;
    invoke-virtual {p1, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 344
    invoke-virtual {p2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/ArrayList;

    .line 345
    .local v6, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz v6, :cond_3

    .line 346
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v9

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    const/4 v4, 0x1

    .line 348
    .local v4, "k":I
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    .line 349
    .local v8, "value":Ljava/lang/Object;
    const/4 v9, 0x1

    if-ne v4, v9, :cond_2

    instance-of v9, v8, Ljava/lang/String;

    if-eqz v9, :cond_2

    move-object v1, v8

    .line 350
    check-cast v1, Ljava/lang/String;

    .line 351
    .local v1, "data":Ljava/lang/String;
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 357
    .end local v1    # "data":Ljava/lang/String;
    .end local v8    # "value":Ljava/lang/Object;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    .line 358
    goto :goto_1

    .line 352
    .restart local v8    # "value":Ljava/lang/Object;
    :cond_2
    const/4 v9, 0x2

    if-ne v4, v9, :cond_1

    instance-of v9, v8, [B

    if-eqz v9, :cond_1

    .line 353
    check-cast v8, [B

    .end local v8    # "value":Ljava/lang/Object;
    move-object v0, v8

    check-cast v0, [B

    .line 354
    .local v0, "bData":[B
    array-length v9, v0

    invoke-virtual {p1, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 355
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    goto :goto_2

    .line 360
    .end local v0    # "bData":[B
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "k":I
    :cond_3
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 364
    .end local v6    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v7    # "pkg":Ljava/lang/String;
    :cond_4
    invoke-virtual {p1, v10}, Landroid/os/Parcel;->writeInt(I)V

    .line 366
    :cond_5
    return-void
.end method


# virtual methods
.method public clone()Lcom/sec/knox/container/ContainerCreationParams;
    .locals 2

    .prologue
    .line 326
    new-instance v0, Lcom/sec/knox/container/ContainerCreationParams;

    invoke-direct {v0}, Lcom/sec/knox/container/ContainerCreationParams;-><init>()V

    .line 327
    .local v0, "params":Lcom/sec/knox/container/ContainerCreationParams;
    iget v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setRequestId(I)V

    .line 328
    iget-object v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setName(Ljava/lang/String;)V

    .line 329
    iget v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setLockType(I)V

    .line 330
    iget-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setFingerprintPlus(Z)V

    .line 331
    iget-object v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setBackupPin(Ljava/lang/String;)V

    .line 332
    iget-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setRestoreSelection(Z)V

    .line 333
    iget-boolean v1, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    invoke-virtual {v0, v1}, Lcom/sec/knox/container/ContainerCreationParams;->setIsMigrationFlow(Z)V

    .line 334
    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 15
    invoke-virtual {p0}, Lcom/sec/knox/container/ContainerCreationParams;->clone()Lcom/sec/knox/container/ContainerCreationParams;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 3

    .prologue
    .line 262
    const-string v0, "ContainerCreationParams"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getRequestId :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/knox/container/ContainerCreationParams;->getRequestId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    const-string v0, "ContainerCreationParams"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getName :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/knox/container/ContainerCreationParams;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const-string v0, "ContainerCreationParams"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAdminUid :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/knox/container/ContainerCreationParams;->getAdminUid()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    const/4 v0, 0x0

    return v0
.end method

.method public getAdminParam()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    return-object v0
.end method

.method public getAdminRemovable()Z
    .locals 1

    .prologue
    .line 180
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    return v0
.end method

.method public getAdminUid()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    return v0
.end method

.method public getBackupPin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    return-object v0
.end method

.method public getConfigurationType()Lcom/sec/enterprise/knox/container/KnoxConfigurationType;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    return-object v0
.end method

.method public getCreatorUid()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    return v0
.end method

.method public getFeatureType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    return-object v0
.end method

.method public getFingerprintPlus()Z
    .locals 1

    .prologue
    .line 164
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    return v0
.end method

.method public getFlags()I
    .locals 1

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    return v0
.end method

.method public getIsMigrationFlow()Z
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    return v0
.end method

.method public getLockType()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getPackagePoliciesMap()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getRequestId()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    return v0
.end method

.method public getRequestState()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestState:I

    return v0
.end method

.method public getResetPasswordKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    return-object v0
.end method

.method public getRestoreSelection()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    return v0
.end method

.method public setAdminParam(Ljava/lang/String;)V
    .locals 0
    .param p1, "adminParam"    # Ljava/lang/String;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    .line 111
    return-void
.end method

.method public setAdminRemovable(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 176
    iput-boolean p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    .line 177
    return-void
.end method

.method public setAdminUid(I)V
    .locals 0
    .param p1, "adminUid"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    .line 79
    return-void
.end method

.method public setBackupPin(Ljava/lang/String;)V
    .locals 0
    .param p1, "backupPin"    # Ljava/lang/String;

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    .line 153
    return-void
.end method

.method public setConfigurationType(Lcom/sec/enterprise/knox/container/KnoxConfigurationType;)V
    .locals 0
    .param p1, "type"    # Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    .line 95
    return-void
.end method

.method public setCreatorUid(I)V
    .locals 0
    .param p1, "creatorUid"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    .line 87
    return-void
.end method

.method public setFeatureType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public setFingerprintPlus(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    .line 161
    return-void
.end method

.method public setFlags(I)V
    .locals 0
    .param p1, "flags"    # I

    .prologue
    .line 118
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    .line 119
    return-void
.end method

.method public setIsMigrationFlow(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 135
    iput-boolean p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    .line 136
    return-void
.end method

.method public setLockType(I)V
    .locals 0
    .param p1, "lockType"    # I

    .prologue
    .line 126
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    .line 127
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    .line 71
    return-void
.end method

.method public setPackagePoliciesMap(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 102
    .local p1, "policyMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/Object;>;>;"
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    .line 103
    return-void
.end method

.method public setPassword(Ljava/lang/String;)V
    .locals 0
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    .line 145
    return-void
.end method

.method public setRequestId(I)V
    .locals 0
    .param p1, "requestId"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    .line 63
    return-void
.end method

.method public setRequestState(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestState:I

    .line 55
    return-void
.end method

.method public setResetPasswordKey(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    .line 185
    return-void
.end method

.method public setRestoreSelection(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 168
    iput-boolean p1, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    .line 169
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 277
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPassword:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 287
    :goto_1
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 288
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mBackupPin:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 292
    :goto_2
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 293
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminParam:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 297
    :goto_3
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 298
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mResetPwdKey:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 302
    :goto_4
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRequestId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 303
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFlags:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 304
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mLockType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 305
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 306
    iget v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mCreatorUid:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 307
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFingerprintPlus:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 308
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mRestoreSelected:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 309
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mConfigurationType:Lcom/sec/enterprise/knox/container/KnoxConfigurationType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 312
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->isMigrationFlow:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 314
    iget-boolean v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mAdminRemovable:Z

    if-eqz v0, :cond_8

    :goto_8
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 316
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 317
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mFeatureType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 321
    :goto_9
    iget-object v0, p0, Lcom/sec/knox/container/ContainerCreationParams;->mPackagePoliciesMap:Ljava/util/HashMap;

    invoke-direct {p0, p1, v0}, Lcom/sec/knox/container/ContainerCreationParams;->serializePackagePolicies(Landroid/os/Parcel;Ljava/util/HashMap;)V

    .line 323
    return-void

    .line 280
    :cond_0
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 285
    :cond_1
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 290
    :cond_2
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_2

    .line 295
    :cond_3
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_3

    .line 300
    :cond_4
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_4

    :cond_5
    move v0, v2

    .line 307
    goto :goto_5

    :cond_6
    move v0, v2

    .line 308
    goto :goto_6

    :cond_7
    move v0, v2

    .line 312
    goto :goto_7

    :cond_8
    move v1, v2

    .line 314
    goto :goto_8

    .line 319
    :cond_9
    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_9
.end method
