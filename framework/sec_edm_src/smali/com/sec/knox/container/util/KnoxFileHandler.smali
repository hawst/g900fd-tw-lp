.class public Lcom/sec/knox/container/util/KnoxFileHandler;
.super Landroid/content/pm/IPersonaFileHandler$Stub;
.source "KnoxFileHandler.java"


# static fields
.field private static ALIAS_NAME:Ljava/lang/String; = null

.field private static final DEBUG:Z

.field private static final ECRYPTFS_KEY_LENGTH:I = 0x20

.field private static KEYSTORE_FILE_PATH:Ljava/lang/String; = null

.field private static final MAX_LENGTH:I = 0x10

.field static final TAG:Ljava/lang/String; = "KnoxFileHandler"

.field private static TIMA_KEYSTORE_NAME:Ljava/lang/String;

.field private static timaVersion20:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "TIMAKeyStore"

    sput-object v0, Lcom/sec/knox/container/util/KnoxFileHandler;->TIMA_KEYSTORE_NAME:Ljava/lang/String;

    .line 48
    const-string v0, "/data/system/container/key"

    sput-object v0, Lcom/sec/knox/container/util/KnoxFileHandler;->KEYSTORE_FILE_PATH:Ljava/lang/String;

    .line 49
    const-string v0, "ecryptfsKey"

    sput-object v0, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    .line 56
    const-string v0, "eng"

    const-string v1, "ro.build.type"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Landroid/content/pm/IPersonaFileHandler$Stub;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mContext:Landroid/content/Context;

    .line 55
    iput-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    .line 58
    invoke-static {p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->getInstance(Landroid/content/Context;)Lcom/sec/knox/container/util/EnterprisePartitionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    .line 59
    iput-object p1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mContext:Landroid/content/Context;

    .line 61
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 63
    :try_start_0
    const-string v1, "2.0"

    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v2

    invoke-interface {v2}, Landroid/service/tima/ITimaService;->getTimaVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    .line 64
    const-string v1, "KnoxFileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "KnoxFileHandler: TimaVersion is 2.0 ? --- > "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-boolean v3, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "KnoxFileHandler"

    const-string v2, "KnoxFileHandler : Unable to get TIMA version"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private checkTimaError(IZ)V
    .locals 4
    .param p1, "personaId"    # I
    .param p2, "mountError"    # Z

    .prologue
    .line 503
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTIMAStatus()I

    move-result v0

    .line 505
    .local v0, "timaCode":I
    const-string v1, "KnoxFileHandler"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "timaCode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 506
    packed-switch v0, :pswitch_data_0

    .line 517
    if-eqz p2, :cond_0

    .line 518
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->setFsErrorState(I)V

    .line 523
    :cond_0
    :goto_0
    return-void

    .line 511
    :pswitch_0
    const-string v1, "KnoxFileHandler"

    const-string v2, "Setting to KNOX_STATE_TIMA_COMPROMISED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 512
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->setTimaCompromisedState(I)V

    goto :goto_0

    .line 506
    :pswitch_data_0
    .packed-switch 0x1000c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private deleteEcryptFSKey(IZ)V
    .locals 12
    .param p1, "personaId"    # I
    .param p2, "isSecureStorageEnabled"    # Z

    .prologue
    .line 379
    const-string v8, "KnoxFileHandler"

    const-string v9, "deleteEcryptFSKey enter"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    const/4 v3, 0x0

    .line 381
    .local v3, "in":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 384
    .local v6, "isTimaEnabled":Z
    move v6, p2

    .line 385
    :try_start_0
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "deleteEcryptFSKey-> isTimaEnabled :"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    if-eqz v6, :cond_3

    .line 388
    sget-object v8, Lcom/sec/knox/container/util/KnoxFileHandler;->TIMA_KEYSTORE_NAME:Ljava/lang/String;

    invoke-static {v8}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v7

    .line 389
    .local v7, "ks":Ljava/security/KeyStore;
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V

    .line 390
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 391
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 392
    sget-boolean v8, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 393
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "key successfully removed : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " from TIMA keyStore"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 432
    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    .line 433
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 439
    .end local v7    # "ks":Ljava/security/KeyStore;
    :cond_1
    :goto_1
    return-void

    .line 396
    .restart local v7    # "ks":Ljava/security/KeyStore;
    :cond_2
    :try_start_2
    sget-boolean v8, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v8, :cond_0

    .line 397
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "key not successfully removed : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " from TIMA keyStore"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 422
    .end local v7    # "ks":Ljava/security/KeyStore;
    :catch_0
    move-exception v1

    .line 423
    .local v1, "e":Ljava/security/KeyStoreException;
    :try_start_3
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 432
    if-eqz v3, :cond_1

    .line 433
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 435
    :catch_1
    move-exception v5

    .line 436
    .local v5, "ioe":Ljava/io/IOException;
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close input stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 401
    .end local v1    # "e":Ljava/security/KeyStoreException;
    .end local v5    # "ioe":Ljava/io/IOException;
    :cond_3
    :try_start_5
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v7

    .line 402
    .restart local v7    # "ks":Ljava/security/KeyStore;
    new-instance v2, Ljava/io/File;

    sget-object v8, Lcom/sec/knox/container/util/KnoxFileHandler;->KEYSTORE_FILE_PATH:Ljava/lang/String;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 403
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 404
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v3    # "in":Ljava/io/FileInputStream;
    .local v4, "in":Ljava/io/FileInputStream;
    move-object v3, v4

    .line 406
    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {v7, v3, v8}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 407
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 408
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V

    .line 409
    sget-boolean v8, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v8, :cond_5

    .line 410
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "key successfully removed : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " from Android keyStore"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :cond_5
    invoke-virtual {v7}, Ljava/security/KeyStore;->aliases()Ljava/util/Enumeration;

    move-result-object v0

    .line 414
    .local v0, "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v8

    if-nez v8, :cond_0

    .line 415
    if-eqz v3, :cond_0

    .line 416
    const-string v8, "KnoxFileHandler"

    const-string v9, "deleteing key store file."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_5
    .catch Ljava/security/KeyStoreException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 424
    .end local v0    # "aliases":Ljava/util/Enumeration;, "Ljava/util/Enumeration<Ljava/lang/String;>;"
    .end local v2    # "file":Ljava/io/File;
    .end local v7    # "ks":Ljava/security/KeyStore;
    :catch_2
    move-exception v1

    .line 425
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_6
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 432
    if-eqz v3, :cond_1

    .line 433
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto/16 :goto_1

    .line 435
    :catch_3
    move-exception v5

    .line 436
    .restart local v5    # "ioe":Ljava/io/IOException;
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close input stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 435
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v5    # "ioe":Ljava/io/IOException;
    .restart local v7    # "ks":Ljava/security/KeyStore;
    :catch_4
    move-exception v5

    .line 436
    .restart local v5    # "ioe":Ljava/io/IOException;
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close input stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 426
    .end local v5    # "ioe":Ljava/io/IOException;
    .end local v7    # "ks":Ljava/security/KeyStore;
    :catch_5
    move-exception v1

    .line 427
    .local v1, "e":Ljava/security/cert/CertificateException;
    :try_start_8
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 432
    if-eqz v3, :cond_1

    .line 433
    :try_start_9
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_1

    .line 435
    :catch_6
    move-exception v5

    .line 436
    .restart local v5    # "ioe":Ljava/io/IOException;
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close input stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 428
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    .end local v5    # "ioe":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 429
    .local v1, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 432
    if-eqz v3, :cond_1

    .line 433
    :try_start_b
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_1

    .line 435
    :catch_8
    move-exception v5

    .line 436
    .restart local v5    # "ioe":Ljava/io/IOException;
    const-string v8, "KnoxFileHandler"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Failed to close input stream: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 431
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 432
    if-eqz v3, :cond_6

    .line 433
    :try_start_c
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 437
    :cond_6
    :goto_2
    throw v8

    .line 435
    :catch_9
    move-exception v5

    .line 436
    .restart local v5    # "ioe":Ljava/io/IOException;
    const-string v9, "KnoxFileHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Failed to close input stream: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private getEntryPassword(I)Ljava/security/KeyStore$ProtectionParameter;
    .locals 4
    .param p1, "personaId"    # I

    .prologue
    .line 443
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 444
    .local v0, "password":Ljava/lang/String;
    const/4 v1, 0x0

    .line 446
    .local v1, "pwd":[C
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 447
    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    .line 448
    const-string v2, "KnoxFileHandler"

    const-string v3, "Returning key password"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 449
    new-instance v2, Ljava/security/KeyStore$PasswordProtection;

    invoke-direct {v2, v1}, Ljava/security/KeyStore$PasswordProtection;-><init>([C)V

    .line 451
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getTIMAStatus()I
    .locals 9

    .prologue
    .line 526
    const/4 v1, -0x1

    .line 527
    .local v1, "timaStatus":I
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v6

    if-nez v6, :cond_0

    move v2, v1

    .line 545
    .end local v1    # "timaStatus":I
    .local v2, "timaStatus":I
    :goto_0
    return v2

    .line 530
    .end local v2    # "timaStatus":I
    .restart local v1    # "timaStatus":I
    :cond_0
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 532
    .local v4, "token":J
    :try_start_0
    const-string v6, "3.0"

    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v7

    invoke-interface {v7}, Landroid/service/tima/ITimaService;->getTimaVersion()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    .line 533
    .local v3, "timaVersion30":Z
    if-eqz v3, :cond_2

    .line 534
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v6

    invoke-interface {v6}, Landroid/service/tima/ITimaService;->KeyStore3_init()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 542
    :cond_1
    :goto_1
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 544
    .end local v3    # "timaVersion30":Z
    :goto_2
    const-string v6, "KnoxFileHandler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "TIMA getTIMAStatus "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    .line 545
    .end local v1    # "timaStatus":I
    .restart local v2    # "timaStatus":I
    goto :goto_0

    .line 535
    .end local v2    # "timaStatus":I
    .restart local v1    # "timaStatus":I
    .restart local v3    # "timaVersion30":Z
    :cond_2
    :try_start_1
    sget-boolean v6, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-eqz v6, :cond_1

    .line 536
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v6

    invoke-interface {v6}, Landroid/service/tima/ITimaService;->keystoreInit()I

    move-result v1

    .line 537
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v6

    invoke-interface {v6}, Landroid/service/tima/ITimaService;->keystoreShutdown()I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 539
    .end local v3    # "timaVersion30":Z
    :catch_0
    move-exception v0

    .line 540
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_2
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 542
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_2

    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v6

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v6
.end method

.method private getTimaService()Landroid/service/tima/ITimaService;
    .locals 2

    .prologue
    .line 484
    const-string v1, "tima"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Landroid/service/tima/ITimaService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/service/tima/ITimaService;

    move-result-object v0

    .line 486
    .local v0, "timaService":Landroid/service/tima/ITimaService;
    return-object v0
.end method

.method private getTimaState()I
    .locals 5

    .prologue
    .line 490
    const/4 v1, -0x1

    .line 491
    .local v1, "timaStatus":I
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 493
    :try_start_0
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v2

    invoke-interface {v2}, Landroid/service/tima/ITimaService;->KeyStore3_init()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 498
    :cond_0
    :goto_0
    const-string v2, "KnoxFileHandler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "timaStatus :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 499
    return v1

    .line 494
    :catch_0
    move-exception v0

    .line 495
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private isFileSystemEncrypted(I)Z
    .locals 1
    .param p1, "personaId"    # I

    .prologue
    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method private retrieveEcryptFSKey(IZI)Ljava/lang/String;
    .locals 10
    .param p1, "personaId"    # I
    .param p2, "isSecureStorageEnabled"    # Z
    .param p3, "index"    # I

    .prologue
    .line 324
    const-string v7, "KnoxFileHandler"

    const-string v8, "retrieveEcryptFSKey enter"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    const/4 v2, 0x0

    .line 326
    .local v2, "eCryptfsKey":Ljava/lang/String;
    const/4 v4, 0x0

    .line 329
    .local v4, "isTimaEnabled":Z
    move v4, p2

    .line 330
    :try_start_0
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "retrieveEcryptFSKey-> isTimaEnabled :"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    if-eqz v4, :cond_2

    .line 332
    const/4 v6, -0x1

    .line 333
    .local v6, "timaStatus":I
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 334
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTIMAStatus()I

    move-result v6

    .line 336
    :cond_0
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Tima device status "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    if-nez v6, :cond_1

    sget-boolean v7, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-eqz v7, :cond_1

    .line 338
    invoke-direct {p0, p1, p3}, Lcom/sec/knox/container/util/KnoxFileHandler;->retrieveEcryptFSKeyForTima20(II)Ljava/lang/String;

    move-result-object v7

    .line 375
    .end local v6    # "timaStatus":I
    :goto_0
    return-object v7

    .line 339
    .restart local v6    # "timaStatus":I
    :cond_1
    if-nez v6, :cond_5

    .line 340
    sget-object v7, Lcom/sec/knox/container/util/KnoxFileHandler;->TIMA_KEYSTORE_NAME:Ljava/lang/String;

    invoke-static {v7}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v5

    .line 341
    .local v5, "ks":Ljava/security/KeyStore;
    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V

    .line 342
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 343
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->getEntryPassword(I)Ljava/security/KeyStore$ProtectionParameter;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;

    move-result-object v7

    check-cast v7, Ljava/security/KeyStore$SecretKeyEntry;

    move-object v0, v7

    check-cast v0, Ljava/security/KeyStore$SecretKeyEntry;

    move-object v3, v0

    .line 344
    .local v3, "entry":Ljava/security/KeyStore$SecretKeyEntry;
    if-eqz v3, :cond_3

    .line 345
    invoke-virtual {v3}, Ljava/security/KeyStore$SecretKeyEntry;->getSecretKey()Ljavax/crypto/SecretKey;

    move-result-object v7

    invoke-interface {v7}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v7, v8}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 347
    sget-boolean v7, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v7, :cond_2

    .line 348
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Found key : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " from TIMA keystore"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .end local v3    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .end local v5    # "ks":Ljava/security/KeyStore;
    .end local v6    # "timaStatus":I
    :cond_2
    :goto_1
    move-object v7, v2

    .line 375
    goto :goto_0

    .line 351
    .restart local v3    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .restart local v5    # "ks":Ljava/security/KeyStore;
    .restart local v6    # "timaStatus":I
    :cond_3
    sget-boolean v7, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v7, :cond_2

    .line 352
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "key "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " entry is null in TIMA keystore"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 364
    .end local v3    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .end local v5    # "ks":Ljava/security/KeyStore;
    .end local v6    # "timaStatus":I
    :catch_0
    move-exception v1

    .line 365
    .local v1, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_1

    .line 356
    .end local v1    # "e":Ljava/security/KeyStoreException;
    .restart local v5    # "ks":Ljava/security/KeyStore;
    .restart local v6    # "timaStatus":I
    :cond_4
    :try_start_1
    sget-boolean v7, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v7, :cond_2

    .line 357
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "key not found : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " in TIMA keystore"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_1 .. :try_end_1} :catch_4

    goto :goto_1

    .line 366
    .end local v5    # "ks":Ljava/security/KeyStore;
    .end local v6    # "timaStatus":I
    :catch_1
    move-exception v1

    .line 367
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1

    .line 361
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v6    # "timaStatus":I
    :cond_5
    :try_start_2
    const-string v7, "KnoxFileHandler"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "KNOX_TIMA_KEYSTORE error with device status -"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 368
    .end local v6    # "timaStatus":I
    :catch_2
    move-exception v1

    .line 369
    .local v1, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_1

    .line 370
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_3
    move-exception v1

    .line 371
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 372
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 373
    .local v1, "e":Ljava/security/UnrecoverableEntryException;
    invoke-virtual {v1}, Ljava/security/UnrecoverableEntryException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method private retrieveEcryptFSKeyForTima20(II)Ljava/lang/String;
    .locals 12
    .param p1, "personaId"    # I
    .param p2, "timaEcrytfsIndex"    # I

    .prologue
    const/16 v7, 0x20

    .line 283
    const/4 v4, 0x0

    .line 284
    .local v4, "ret":Ljava/lang/String;
    const/4 v3, 0x0

    .line 285
    .local v3, "key":[B
    new-array v0, v7, [B

    .line 286
    .local v0, "ecryptfsKey":[B
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v8

    .line 287
    .local v8, "token":J
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v6

    .line 288
    .local v6, "timaService":Landroid/service/tima/ITimaService;
    if-eqz v6, :cond_1

    .line 290
    :try_start_0
    invoke-interface {v6}, Landroid/service/tima/ITimaService;->keystoreInit()I

    move-result v1

    .line 291
    .local v1, "error":I
    const-string v7, "KnoxFileHandler"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "retrieveEcryptFSKeyForTima20 errorCode "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v7, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    if-nez v1, :cond_0

    const/4 v7, -0x1

    if-eq p2, v7, :cond_0

    .line 294
    invoke-interface {v6, p2}, Landroid/service/tima/ITimaService;->keystoreRetrieveKey(I)[B

    move-result-object v3

    .line 295
    if-eqz v3, :cond_0

    const/4 v7, 0x0

    aget-byte v7, v3, v7

    if-nez v7, :cond_0

    .line 296
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/16 v11, 0x20

    invoke-static {v3, v7, v0, v10, v11}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 297
    const/4 v7, 0x0

    invoke-static {v0, v7}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v4

    .line 301
    :cond_0
    invoke-interface {v6}, Landroid/service/tima/ITimaService;->keystoreShutdown()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .end local v1    # "error":I
    :goto_0
    move-object v5, v4

    .line 309
    .end local v4    # "ret":Ljava/lang/String;
    .local v5, "ret":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 302
    .end local v5    # "ret":Ljava/lang/String;
    .restart local v4    # "ret":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 303
    .local v2, "ex":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 305
    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v2    # "ex":Landroid/os/RemoteException;
    :catchall_0
    move-exception v7

    invoke-static {v8, v9}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v7

    :cond_1
    move-object v5, v4

    .line 309
    .end local v4    # "ret":Ljava/lang/String;
    .restart local v5    # "ret":Ljava/lang/String;
    goto :goto_1
.end method

.method private retrieveEcryptFSPwd(IZI)Ljava/lang/String;
    .locals 15
    .param p1, "personaId"    # I
    .param p2, "isSecureStorageEnabled"    # Z
    .param p3, "index"    # I

    .prologue
    .line 549
    const-string v12, "KnoxFileHandler"

    const-string v13, "retrieveEcryptFSKey enter"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    const/4 v2, 0x0

    .line 551
    .local v2, "ecryptfsPwd":Ljava/lang/String;
    const/4 v6, 0x0

    .line 554
    .local v6, "isTimaEnabled":Z
    move/from16 v6, p2

    .line 555
    :try_start_0
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "retrieveEcryptFSKey-> isTimaEnabled :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    if-eqz v6, :cond_4

    .line 557
    const/4 v11, -0x1

    .line 558
    .local v11, "timaStatus":I
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v12

    if-eqz v12, :cond_0

    .line 559
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTIMAStatus()I

    move-result v11

    .line 561
    :cond_0
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Tima device status "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 562
    if-nez v11, :cond_3

    sget-boolean v12, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-eqz v12, :cond_3

    .line 563
    const/16 v12, 0x10

    new-array v9, v12, [B

    .line 564
    .local v9, "pwdBytes":[B
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v10

    .line 565
    .local v10, "timaService":Landroid/service/tima/ITimaService;
    if-eqz v10, :cond_2

    .line 566
    invoke-interface {v10}, Landroid/service/tima/ITimaService;->keystoreInit()I

    move-result v5

    .line 567
    .local v5, "error":I
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "retrieveEcryptFSPwdForTima20 errorCode "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 568
    if-nez v5, :cond_1

    const/4 v12, -0x1

    move/from16 v0, p3

    if-eq v0, v12, :cond_1

    .line 569
    move/from16 v0, p3

    invoke-interface {v10, v0}, Landroid/service/tima/ITimaService;->keystoreRetrieveKey(I)[B

    move-result-object v7

    .line 570
    .local v7, "key":[B
    if-eqz v7, :cond_1

    const/4 v12, 0x0

    aget-byte v12, v7, v12

    if-nez v12, :cond_1

    .line 571
    const/4 v12, 0x1

    const/4 v13, 0x0

    const/16 v14, 0x10

    invoke-static {v7, v12, v9, v13, v14}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 572
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v9}, Ljava/lang/String;-><init>([B)V

    .end local v2    # "ecryptfsPwd":Ljava/lang/String;
    .local v3, "ecryptfsPwd":Ljava/lang/String;
    move-object v2, v3

    .line 575
    .end local v3    # "ecryptfsPwd":Ljava/lang/String;
    .end local v7    # "key":[B
    .restart local v2    # "ecryptfsPwd":Ljava/lang/String;
    :cond_1
    invoke-interface {v10}, Landroid/service/tima/ITimaService;->keystoreShutdown()I

    .end local v5    # "error":I
    :cond_2
    move-object v12, v2

    .line 612
    .end local v9    # "pwdBytes":[B
    .end local v10    # "timaService":Landroid/service/tima/ITimaService;
    .end local v11    # "timaStatus":I
    :goto_0
    return-object v12

    .line 578
    .restart local v11    # "timaStatus":I
    :cond_3
    if-nez v11, :cond_7

    .line 579
    sget-object v12, Lcom/sec/knox/container/util/KnoxFileHandler;->TIMA_KEYSTORE_NAME:Ljava/lang/String;

    invoke-static {v12}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v8

    .line 580
    .local v8, "ks":Ljava/security/KeyStore;
    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V

    .line 581
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Ljava/security/KeyStore;->isKeyEntry(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 582
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v13, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct/range {p0 .. p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->getEntryPassword(I)Ljava/security/KeyStore$ProtectionParameter;

    move-result-object v13

    invoke-virtual {v8, v12, v13}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;

    move-result-object v12

    check-cast v12, Ljava/security/KeyStore$SecretKeyEntry;

    move-object v0, v12

    check-cast v0, Ljava/security/KeyStore$SecretKeyEntry;

    move-object v4, v0

    .line 583
    .local v4, "entry":Ljava/security/KeyStore$SecretKeyEntry;
    if-eqz v4, :cond_5

    .line 584
    invoke-virtual {v4}, Ljava/security/KeyStore$SecretKeyEntry;->getSecretKey()Ljavax/crypto/SecretKey;

    move-result-object v12

    invoke-interface {v12}, Ljavax/crypto/SecretKey;->getEncoded()[B

    move-result-object v12

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 585
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Found key : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " from TIMA keystore"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_5

    .line 609
    .end local v4    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .end local v8    # "ks":Ljava/security/KeyStore;
    .end local v11    # "timaStatus":I
    :cond_4
    :goto_1
    if-eqz v2, :cond_8

    .line 610
    const/4 v12, 0x0

    const/16 v13, 0x10

    invoke-virtual {v2, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_0

    .line 587
    .restart local v4    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .restart local v8    # "ks":Ljava/security/KeyStore;
    .restart local v11    # "timaStatus":I
    :cond_5
    :try_start_1
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "key "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " entry is null in TIMA keystore"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_5

    goto :goto_1

    .line 596
    .end local v4    # "entry":Ljava/security/KeyStore$SecretKeyEntry;
    .end local v8    # "ks":Ljava/security/KeyStore;
    .end local v11    # "timaStatus":I
    :catch_0
    move-exception v1

    .line 597
    .local v1, "e":Ljava/security/KeyStoreException;
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V

    goto :goto_1

    .line 590
    .end local v1    # "e":Ljava/security/KeyStoreException;
    .restart local v8    # "ks":Ljava/security/KeyStore;
    .restart local v11    # "timaStatus":I
    :cond_6
    :try_start_2
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "key not found : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " in TIMA keystore"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_5

    goto :goto_1

    .line 598
    .end local v8    # "ks":Ljava/security/KeyStore;
    .end local v11    # "timaStatus":I
    :catch_1
    move-exception v1

    .line 599
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_1

    .line 593
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .restart local v11    # "timaStatus":I
    :cond_7
    :try_start_3
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "KNOX_TIMA_KEYSTORE error with device status -"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/security/KeyStoreException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_5

    goto :goto_1

    .line 600
    .end local v11    # "timaStatus":I
    :catch_2
    move-exception v1

    .line 601
    .local v1, "e":Ljava/security/cert/CertificateException;
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V

    goto :goto_1

    .line 602
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_3
    move-exception v1

    .line 603
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    .line 604
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 605
    .local v1, "e":Ljava/security/UnrecoverableEntryException;
    invoke-virtual {v1}, Ljava/security/UnrecoverableEntryException;->printStackTrace()V

    goto/16 :goto_1

    .line 606
    .end local v1    # "e":Ljava/security/UnrecoverableEntryException;
    :catch_5
    move-exception v1

    .line 607
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_1

    .line 612
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_8
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method private setFsErrorState(I)V
    .locals 3
    .param p1, "personaId"    # I

    .prologue
    .line 468
    const-string v1, "KnoxFileHandler"

    const-string v2, "setFsErrorState is called.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 469
    iget-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 470
    .local v0, "personaManager":Landroid/os/PersonaManager;
    if-eqz v0, :cond_0

    .line 471
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/os/PersonaManager;->setFsMountState(IZ)V

    .line 473
    :cond_0
    return-void
.end method

.method private setFsSuccessState(I)V
    .locals 3
    .param p1, "personaId"    # I

    .prologue
    .line 476
    const-string v1, "KnoxFileHandler"

    const-string v2, "setFsSuccessState is called.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    iget-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 478
    .local v0, "personaManager":Landroid/os/PersonaManager;
    if-eqz v0, :cond_0

    .line 479
    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/os/PersonaManager;->setFsMountState(IZ)V

    .line 481
    :cond_0
    return-void
.end method

.method private setTimaCompromisedState(I)V
    .locals 3
    .param p1, "personaId"    # I

    .prologue
    .line 456
    const-string v1, "KnoxFileHandler"

    const-string v2, "setTimaCompromisedState is called.."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    iget-object v1, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mContext:Landroid/content/Context;

    const-string v2, "persona"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 458
    .local v0, "personaManager":Landroid/os/PersonaManager;
    if-eqz v0, :cond_0

    .line 461
    invoke-virtual {v0, p1}, Landroid/os/PersonaManager;->getStateManager(I)Landroid/os/PersonaManager$StateManager;

    move-result-object v1

    sget-object v2, Landroid/content/pm/PersonaNewEvent;->TIMA_COMPROMISED:Landroid/content/pm/PersonaNewEvent;

    invoke-virtual {v1, v2}, Landroid/os/PersonaManager$StateManager;->fireEvent(Landroid/content/pm/PersonaNewEvent;)Landroid/content/pm/PersonaState;

    .line 465
    :cond_0
    return-void
.end method

.method private storeEcryptFSKey(I[BZ)Z
    .locals 15
    .param p1, "personaId"    # I
    .param p2, "eCryptfskey"    # [B
    .param p3, "isSecureStorageEnabled"    # Z

    .prologue
    .line 224
    const-string v11, "KnoxFileHandler"

    const-string v12, "storeEcryptFSKey enter"

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    const/4 v2, 0x0

    .line 226
    .local v2, "in":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 228
    .local v7, "out":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .line 229
    .local v4, "isTimaEnabled":Z
    const/4 v8, 0x0

    .line 231
    .local v8, "resullt":Z
    move/from16 v4, p3

    .line 232
    :try_start_0
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "storeEcryptFSKey->  isTimaEnabled :"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    if-eqz v4, :cond_1

    .line 235
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTIMAStatus()I

    move-result v10

    .line 236
    .local v10, "timaStatus":I
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Tima device status "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    if-nez v10, :cond_6

    .line 238
    sget-object v11, Lcom/sec/knox/container/util/KnoxFileHandler;->TIMA_KEYSTORE_NAME:Ljava/lang/String;

    invoke-static {v11}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v6

    .line 239
    .local v6, "ks":Ljava/security/KeyStore;
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V

    .line 241
    if-eqz p2, :cond_4

    .line 242
    new-instance v5, Ljavax/crypto/spec/SecretKeySpec;

    const-string v11, ""

    move-object/from16 v0, p2

    invoke-direct {v5, v0, v11}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 243
    .local v5, "key":Ljavax/crypto/SecretKey;
    new-instance v9, Ljava/security/KeyStore$SecretKeyEntry;

    invoke-direct {v9, v5}, Ljava/security/KeyStore$SecretKeyEntry;-><init>(Ljavax/crypto/SecretKey;)V

    .line 245
    .local v9, "secretKeyEntry":Ljava/security/KeyStore$SecretKeyEntry;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct/range {p0 .. p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->getEntryPassword(I)Ljava/security/KeyStore$ProtectionParameter;

    move-result-object v12

    invoke-virtual {v6, v11, v9, v12}, Ljava/security/KeyStore;->setEntry(Ljava/lang/String;Ljava/security/KeyStore$Entry;Ljava/security/KeyStore$ProtectionParameter;)V

    .line 246
    sget-boolean v11, Lcom/sec/knox/container/util/KnoxFileHandler;->DEBUG:Z

    if-eqz v11, :cond_0

    .line 247
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Succesfully saved key "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Lcom/sec/knox/container/util/KnoxFileHandler;->ALIAS_NAME:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move/from16 v0, p1

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " inside TIMA keystore"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :cond_0
    const/4 v8, 0x1

    .line 268
    .end local v5    # "key":Ljavax/crypto/SecretKey;
    .end local v6    # "ks":Ljava/security/KeyStore;
    .end local v9    # "secretKeyEntry":Ljava/security/KeyStore$SecretKeyEntry;
    .end local v10    # "timaStatus":I
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 269
    :try_start_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_2
    if-eqz v7, :cond_3

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 279
    :cond_3
    :goto_1
    return v8

    .line 251
    .restart local v6    # "ks":Ljava/security/KeyStore;
    .restart local v10    # "timaStatus":I
    :cond_4
    :try_start_2
    const-string v11, "KnoxFileHandler"

    const-string v12, "Illegal argument for TIMA keystore"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_7
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 258
    .end local v6    # "ks":Ljava/security/KeyStore;
    .end local v10    # "timaStatus":I
    :catch_0
    move-exception v1

    .line 259
    .local v1, "e":Ljava/security/KeyStoreException;
    :try_start_3
    invoke-virtual {v1}, Ljava/security/KeyStoreException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 268
    if-eqz v2, :cond_5

    .line 269
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_5
    if-eqz v7, :cond_3

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 274
    :catch_1
    move-exception v3

    .line 275
    .local v3, "ioe":Ljava/io/IOException;
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close input/output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 254
    .end local v1    # "e":Ljava/security/KeyStoreException;
    .end local v3    # "ioe":Ljava/io/IOException;
    .restart local v10    # "timaStatus":I
    :cond_6
    :try_start_5
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "KNOX_TIMA_KEYSTORE error with device status -"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/security/KeyStoreException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 260
    .end local v10    # "timaStatus":I
    :catch_2
    move-exception v1

    .line 261
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    :try_start_6
    invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 268
    if-eqz v2, :cond_7

    .line 269
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_7
    if-eqz v7, :cond_3

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 274
    :catch_3
    move-exception v3

    .line 275
    .restart local v3    # "ioe":Ljava/io/IOException;
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close input/output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 274
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_4
    move-exception v3

    .line 275
    .restart local v3    # "ioe":Ljava/io/IOException;
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close input/output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 262
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_5
    move-exception v1

    .line 263
    .local v1, "e":Ljava/security/cert/CertificateException;
    :try_start_8
    invoke-virtual {v1}, Ljava/security/cert/CertificateException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 268
    if-eqz v2, :cond_8

    .line 269
    :try_start_9
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_8
    if-eqz v7, :cond_3

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_1

    .line 274
    :catch_6
    move-exception v3

    .line 275
    .restart local v3    # "ioe":Ljava/io/IOException;
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close input/output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 264
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    .end local v3    # "ioe":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 265
    .local v1, "e":Ljava/io/IOException;
    :try_start_a
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 268
    if-eqz v2, :cond_9

    .line 269
    :try_start_b
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_9
    if-eqz v7, :cond_3

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    goto/16 :goto_1

    .line 274
    :catch_8
    move-exception v3

    .line 275
    .restart local v3    # "ioe":Ljava/io/IOException;
    const-string v11, "KnoxFileHandler"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Failed to close input/output stream: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 267
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v11

    .line 268
    if-eqz v2, :cond_a

    .line 269
    :try_start_c
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 271
    :cond_a
    if-eqz v7, :cond_b

    .line 272
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9

    .line 276
    :cond_b
    :goto_2
    throw v11

    .line 274
    :catch_9
    move-exception v3

    .line 275
    .restart local v3    # "ioe":Ljava/io/IOException;
    const-string v12, "KnoxFileHandler"

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Failed to close input/output stream: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private storeEcryptfsKeyForTima20(I[BI)Z
    .locals 8
    .param p1, "personaId"    # I
    .param p2, "eCryptfsKey"    # [B
    .param p3, "timaEcrytfsIndex"    # I

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v2

    .line 201
    .local v2, "timaService":Landroid/service/tima/ITimaService;
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v4

    .line 202
    .local v4, "token":J
    if-eqz v2, :cond_1

    .line 204
    :try_start_0
    invoke-interface {v2}, Landroid/service/tima/ITimaService;->keystoreInit()I

    move-result v0

    .line 205
    .local v0, "error":I
    const-string v3, "KnoxFileHandler"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "storeEcryptfsKeyForTima20 errorCode "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    if-nez v0, :cond_0

    const/4 v3, -0x1

    if-eq p3, v3, :cond_0

    .line 209
    invoke-interface {v2, p3, p2}, Landroid/service/tima/ITimaService;->keystoreInstallKey(I[B)I

    .line 211
    :cond_0
    invoke-interface {v2}, Landroid/service/tima/ITimaService;->keystoreShutdown()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 217
    .end local v0    # "error":I
    :goto_0
    const/4 v3, 0x1

    .line 220
    :goto_1
    return v3

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, "ex":Landroid/os/RemoteException;
    :try_start_1
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    goto :goto_0

    .end local v1    # "ex":Landroid/os/RemoteException;
    :catchall_0
    move-exception v3

    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v3

    .line 219
    :cond_1
    const-string v3, "KnoxFileHandler"

    const-string v6, "storeEcryptfsKeyForTima20 failed returning false"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public changeEncryptionKey(ILjava/lang/String;Ljava/lang/String;ZI)Z
    .locals 1
    .param p1, "personaId"    # I
    .param p2, "oldKey"    # Ljava/lang/String;
    .param p3, "newKey"    # Ljava/lang/String;
    .param p4, "isSecureStorageEnabled"    # Z
    .param p5, "timaEcrytfsIndex"    # I

    .prologue
    .line 187
    const/4 v0, 0x1

    return v0
.end method

.method public createPartition(ILjava/lang/String;ZI)Z
    .locals 9
    .param p1, "personaId"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "isSecureStorageEnabled"    # Z
    .param p4, "timaEcrytfsIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x10

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 74
    const/4 v2, 0x0

    .line 75
    .local v2, "password":Ljava/lang/String;
    if-nez p2, :cond_0

    .line 77
    new-array v1, v8, [B

    .line 78
    .local v1, "passowrdBytes":[B
    new-instance v3, Ljava/security/SecureRandom;

    invoke-direct {v3}, Ljava/security/SecureRandom;-><init>()V

    .line 79
    .local v3, "random":Ljava/security/SecureRandom;
    invoke-virtual {v3, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 80
    invoke-static {v1, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v6, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 86
    .end local v1    # "passowrdBytes":[B
    .end local v3    # "random":Ljava/security/SecureRandom;
    :goto_0
    invoke-static {}, Lcom/sec/knox/container/util/KeyManagementUtil;->getInstance()Lcom/sec/knox/container/util/KeyManagementUtil;

    move-result-object v7

    invoke-virtual {v7, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateEcryptfsKey(Ljava/lang/String;)[B

    move-result-object v0

    .line 87
    .local v0, "eCryptfsKey":[B
    if-eqz v0, :cond_3

    iget-object v7, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v7, p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->createPartition(I)Z

    move-result v7

    if-ne v7, v5, :cond_3

    .line 88
    const-string v7, "KnoxFileHandler"

    const-string v8, "Knox persona partition successfully created.."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    const/4 v4, 0x0

    .line 90
    .local v4, "status":Z
    sget-boolean v7, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-eqz v7, :cond_1

    .line 91
    invoke-direct {p0, p1, v0, p4}, Lcom/sec/knox/container/util/KnoxFileHandler;->storeEcryptfsKeyForTima20(I[BI)Z

    move-result v4

    .line 95
    :goto_1
    if-eqz v4, :cond_2

    .line 104
    .end local v4    # "status":Z
    :goto_2
    return v5

    .line 82
    .end local v0    # "eCryptfsKey":[B
    :cond_0
    move-object v2, p2

    .line 83
    const-string v7, "KnoxFileHandler"

    const-string v8, "createPartition : password with password"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    .restart local v0    # "eCryptfsKey":[B
    .restart local v4    # "status":Z
    :cond_1
    invoke-direct {p0, p1, v0, p3}, Lcom/sec/knox/container/util/KnoxFileHandler;->storeEcryptFSKey(I[BZ)Z

    move-result v4

    goto :goto_1

    .line 98
    :cond_2
    const-string v5, "KnoxFileHandler"

    const-string v7, "createPartition : secretkey not saved successfully. Removing partition"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-virtual {p0, p1, p3}, Lcom/sec/knox/container/util/KnoxFileHandler;->removePartition(IZ)Z

    move v5, v6

    .line 100
    goto :goto_2

    .line 103
    .end local v4    # "status":Z
    :cond_3
    const-string v5, "KnoxFileHandler"

    const-string v7, "createPartition : mEpm.createPartition failed..."

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v6

    .line 104
    goto :goto_2
.end method

.method public isEncrypted(I)Z
    .locals 1
    .param p1, "personaId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->isFileSystemEncrypted(I)Z

    move-result v0

    return v0
.end method

.method public isMounted(I)Z
    .locals 2
    .param p1, "personaId"    # I

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->isFileSystemEncrypted(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    const-string v0, "KnoxFileHandler"

    const-string v1, "Unmounting failed.., file system not encrypted!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const/4 v0, 0x0

    .line 162
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v0

    goto :goto_0
.end method

.method public migrateEcryptFSKey(IZI)Z
    .locals 6
    .param p1, "personaId"    # I
    .param p2, "isSecureStorageEnabled"    # Z
    .param p3, "timaEcrytfsIndex"    # I

    .prologue
    .line 167
    const/4 v2, 0x0

    .line 168
    .local v2, "status":Z
    if-eqz p2, :cond_0

    .line 169
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/knox/container/util/KnoxFileHandler;->retrieveEcryptFSPwd(IZI)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "ecryptfsPwd":Ljava/lang/String;
    invoke-static {}, Lcom/sec/knox/container/util/KeyManagementUtil;->getInstance()Lcom/sec/knox/container/util/KeyManagementUtil;

    move-result-object v3

    invoke-virtual {v3, p1, v0}, Lcom/sec/knox/container/util/KeyManagementUtil;->getEcryptfsKeySkmm1(ILjava/lang/String;)[B

    move-result-object v1

    .line 171
    .local v1, "fekek":[B
    if-eqz v1, :cond_2

    .line 172
    sget-boolean v3, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-eqz v3, :cond_1

    .line 173
    invoke-direct {p0, p1, v1, p3}, Lcom/sec/knox/container/util/KnoxFileHandler;->storeEcryptfsKeyForTima20(I[BI)Z

    move-result v2

    .line 181
    .end local v0    # "ecryptfsPwd":Ljava/lang/String;
    .end local v1    # "fekek":[B
    :cond_0
    :goto_0
    const-string v3, "KnoxFileHandler"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "eCryptfs key migration status: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    return v2

    .line 175
    .restart local v0    # "ecryptfsPwd":Ljava/lang/String;
    .restart local v1    # "fekek":[B
    :cond_1
    invoke-direct {p0, p1, v1, p2}, Lcom/sec/knox/container/util/KnoxFileHandler;->storeEcryptFSKey(I[BZ)Z

    move-result v2

    goto :goto_0

    .line 178
    :cond_2
    const-string v3, "KnoxFileHandler"

    const-string v4, "Failed to get eCryptfs password OR fekek!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public mount(ILjava/lang/String;ZI)Z
    .locals 6
    .param p1, "personaId"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "isSecureStorageEnabled"    # Z
    .param p4, "timaEcrytfsIndex"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 122
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/knox/container/util/KnoxFileHandler;->mountFS(ILjava/lang/String;ZIZ)Z

    move-result v0

    return v0
.end method

.method public mountFS(ILjava/lang/String;ZIZ)Z
    .locals 3
    .param p1, "personaId"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "isSecureStorageEnabled"    # Z
    .param p4, "timaEcrytfsIndex"    # I
    .param p5, "sdpEnabled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p3, p4}, Lcom/sec/knox/container/util/KnoxFileHandler;->retrieveEcryptFSKey(IZI)Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "ecryptfsKey":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 132
    iget-object v2, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v2, p1, v0, p5}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mount(ILjava/lang/String;Z)Z

    move-result v1

    .line 133
    .local v1, "status":Z
    if-nez v1, :cond_0

    .line 134
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->setFsErrorState(I)V

    .line 143
    .end local v1    # "status":Z
    :goto_0
    return v1

    .line 136
    .restart local v1    # "status":Z
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->setFsSuccessState(I)V

    goto :goto_0

    .line 140
    .end local v1    # "status":Z
    :cond_1
    if-eqz p3, :cond_2

    .line 141
    const/4 v2, 0x1

    invoke-direct {p0, p1, v2}, Lcom/sec/knox/container/util/KnoxFileHandler;->checkTimaError(IZ)V

    .line 143
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public mountOldContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 6
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .param p4, "excludeMediaTypes"    # I
    .param p5, "containerId"    # I

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mountOldContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public removePartition(IZ)Z
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "isSecureStorageEnabled"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 109
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->isFileSystemEncrypted(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    const-string v0, "KnoxFileHandler"

    const-string v1, "remove partition failed.., file system not encrypted!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    .line 115
    :goto_0
    return v0

    .line 113
    :cond_0
    sget-boolean v0, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z

    if-nez v0, :cond_1

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/sec/knox/container/util/KnoxFileHandler;->deleteEcryptFSKey(IZ)V

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->removePartition(I)Z

    move-result v0

    goto :goto_0
.end method

.method public unmount(I)Z
    .locals 2
    .param p1, "personaId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/sec/knox/container/util/KnoxFileHandler;->isFileSystemEncrypted(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 150
    const-string v0, "KnoxFileHandler"

    const-string v1, "Unmounting failed.., file system not encrypted!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const/4 v0, 0x0

    .line 153
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->unmount(I)Z

    move-result v0

    goto :goto_0
.end method

.method public unmountOldContainer(Ljava/lang/String;)Z
    .locals 1
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 633
    iget-object v0, p0, Lcom/sec/knox/container/util/KnoxFileHandler;->mEpm:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->unmountOldContainer(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public updateTimaVersion()V
    .locals 3

    .prologue
    .line 617
    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 619
    :try_start_0
    const-string v1, "2.0"

    invoke-direct {p0}, Lcom/sec/knox/container/util/KnoxFileHandler;->getTimaService()Landroid/service/tima/ITimaService;

    move-result-object v2

    invoke-interface {v2}, Landroid/service/tima/ITimaService;->getTimaVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lcom/sec/knox/container/util/KnoxFileHandler;->timaVersion20:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 624
    :cond_0
    :goto_0
    return-void

    .line 620
    :catch_0
    move-exception v0

    .line 621
    .local v0, "re":Landroid/os/RemoteException;
    const-string v1, "KnoxFileHandler"

    const-string v2, "KnoxFileHandler : Unable to get TIMA version"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
