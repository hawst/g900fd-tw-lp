.class public Lcom/sec/knox/container/util/KeyManagementUtil;
.super Ljava/lang/Object;
.source "KeyManagementUtil.java"


# static fields
.field private static final CMK_MDM_FILE:Ljava/lang/String; = "ECMK_MDM"

.field private static final CMK_PWD_FILE:Ljava/lang/String; = "ECMK_PWD"

.field private static final MAX_LENGTH:I = 0x10

.field private static final MAX_SALT_LENGTH:I = 0x20

.field static final TAG:Ljava/lang/String; = "KeyManagementUtil"

.field public static final TYPE_PASSWORD:I = 0x1

.field public static final TYPE_RST_TOKEN:I = 0x2

.field private static mKeyManagementUtil:Lcom/sec/knox/container/util/KeyManagementUtil;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/knox/container/util/KeyManagementUtil;->mKeyManagementUtil:Lcom/sec/knox/container/util/KeyManagementUtil;

    .line 371
    const-string v0, "knox_km"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 372
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static aesDecrypt([B[B[B)[B
    .locals 8
    .param p0, "encrypted"    # [B
    .param p1, "key"    # [B
    .param p2, "iv"    # [B

    .prologue
    const/16 v7, 0x10

    const/4 v4, 0x0

    .line 241
    if-eqz p1, :cond_0

    array-length v5, p1

    if-eq v5, v7, :cond_2

    array-length v5, p1

    const/16 v6, 0x18

    if-eq v5, v6, :cond_2

    array-length v5, p1

    const/16 v6, 0x20

    if-eq v5, v6, :cond_2

    .line 242
    :cond_0
    if-eqz p1, :cond_1

    .line 243
    const-string v5, "KeyManagementUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "inside aesDecrypt - bad length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_1
    :goto_0
    return-object v4

    .line 247
    :cond_2
    if-eqz p2, :cond_3

    array-length v5, p2

    if-ne v5, v7, :cond_1

    .line 252
    :cond_3
    const/4 v2, 0x0

    .line 253
    .local v2, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v0, 0x0

    .line 254
    .local v0, "cipher":Ljavax/crypto/Cipher;
    if-eqz p2, :cond_4

    .line 255
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "AES/CBC/PKCS7Padding"

    invoke-direct {v3, p1, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 256
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .local v3, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_1
    const-string v5, "AES/CBC/PKCS7Padding"

    const-string v6, "AndroidOpenSSL"

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 257
    const/4 v5, 0x2

    new-instance v6, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v6, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, v5, v3, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 263
    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :goto_1
    :try_start_2
    invoke-virtual {v0, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    goto :goto_0

    .line 259
    :cond_4
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "AES/ECB/PKCS7Padding"

    invoke-direct {v3, p1, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 260
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_3
    const-string v5, "AES/ECB/PKCS7Padding"

    const-string v6, "AndroidOpenSSL"

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 261
    const/4 v5, 0x2

    invoke-virtual {v0, v5, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v3

    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_1

    .line 265
    :catch_0
    move-exception v1

    .line 266
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 267
    const-string v5, "KeyManagementUtil"

    const-string v6, "Error inside aesDecrypt "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_2
.end method

.method public static aesEncrypt([B[B[B)[B
    .locals 8
    .param p0, "original"    # [B
    .param p1, "key"    # [B
    .param p2, "iv"    # [B

    .prologue
    const/16 v7, 0x10

    const/4 v4, 0x0

    .line 208
    if-eqz p1, :cond_0

    array-length v5, p1

    if-eq v5, v7, :cond_2

    array-length v5, p1

    const/16 v6, 0x18

    if-eq v5, v6, :cond_2

    array-length v5, p1

    const/16 v6, 0x20

    if-eq v5, v6, :cond_2

    .line 210
    :cond_0
    if-eqz p1, :cond_1

    .line 211
    const-string v5, "KeyManagementUtil"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, " inside aesEncrypt - bad length: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    array-length v7, p1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    :cond_1
    :goto_0
    return-object v4

    .line 215
    :cond_2
    if-eqz p2, :cond_3

    array-length v5, p2

    if-ne v5, v7, :cond_1

    .line 220
    :cond_3
    const/4 v2, 0x0

    .line 221
    .local v2, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    const/4 v0, 0x0

    .line 222
    .local v0, "cipher":Ljavax/crypto/Cipher;
    if-eqz p2, :cond_4

    .line 223
    :try_start_0
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "AES/CBC/PKCS7Padding"

    invoke-direct {v3, p1, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .local v3, "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_1
    const-string v5, "AES/CBC/PKCS7Padding"

    const-string v6, "AndroidOpenSSL"

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 225
    const/4 v5, 0x1

    new-instance v6, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v6, p2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v0, v5, v3, v6}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v3

    .line 231
    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :goto_1
    :try_start_2
    invoke-virtual {v0, p0}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v4

    goto :goto_0

    .line 227
    :cond_4
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "AES/ECB/PKCS7Padding"

    invoke-direct {v3, p1, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 228
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :try_start_3
    const-string v5, "AES/ECB/PKCS7Padding"

    const-string v6, "AndroidOpenSSL"

    invoke-static {v5, v6}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 229
    const/4 v5, 0x1

    invoke-virtual {v0, v5, v3}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    move-object v2, v3

    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_1

    .line 232
    :catch_0
    move-exception v1

    .line 233
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 234
    const-string v5, "KeyManagementUtil"

    const-string v6, "error inside aesEncrypt "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 232
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    :catch_1
    move-exception v1

    move-object v2, v3

    .end local v3    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    .restart local v2    # "keySpec":Ljavax/crypto/spec/SecretKeySpec;
    goto :goto_2
.end method

.method public static generateSalt()[B
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    .line 55
    .local v0, "random":Ljava/security/SecureRandom;
    const/16 v2, 0x20

    new-array v1, v2, [B

    .line 56
    .local v1, "salt":[B
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 57
    return-object v1
.end method

.method public static getInstance()Lcom/sec/knox/container/util/KeyManagementUtil;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/knox/container/util/KeyManagementUtil;->mKeyManagementUtil:Lcom/sec/knox/container/util/KeyManagementUtil;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/sec/knox/container/util/KeyManagementUtil;

    invoke-direct {v0}, Lcom/sec/knox/container/util/KeyManagementUtil;-><init>()V

    sput-object v0, Lcom/sec/knox/container/util/KeyManagementUtil;->mKeyManagementUtil:Lcom/sec/knox/container/util/KeyManagementUtil;

    .line 49
    :cond_0
    sget-object v0, Lcom/sec/knox/container/util/KeyManagementUtil;->mKeyManagementUtil:Lcom/sec/knox/container/util/KeyManagementUtil;

    return-object v0
.end method

.method private getKeyFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 157
    if-nez p1, :cond_0

    .line 158
    const/4 v6, 0x0

    .line 180
    :goto_0
    return-object v6

    .line 160
    :cond_0
    const/4 v6, 0x0

    .line 161
    .local v6, "key":Ljava/lang/String;
    const/4 v1, 0x0

    .line 162
    .local v1, "file":Ljava/io/File;
    const/4 v3, 0x0

    .line 164
    .local v3, "fis":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    .end local v1    # "file":Ljava/io/File;
    .local v2, "file":Ljava/io/File;
    :try_start_1
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 166
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .local v4, "fis":Ljava/io/FileInputStream;
    :try_start_2
    invoke-virtual {v4}, Ljava/io/FileInputStream;->available()I

    move-result v8

    .line 167
    .local v8, "length":I
    new-array v0, v8, [B

    .line 168
    .local v0, "b":[B
    invoke-virtual {v4, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    .line 169
    .local v9, "value":I
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v0}, Ljava/lang/String;-><init>([B)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 174
    .end local v6    # "key":Ljava/lang/String;
    .local v7, "key":Ljava/lang/String;
    :try_start_3
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    move-object v6, v7

    .line 176
    .end local v7    # "key":Ljava/lang/String;
    .restart local v6    # "key":Ljava/lang/String;
    goto :goto_0

    .line 175
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .end local v6    # "key":Ljava/lang/String;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v7    # "key":Ljava/lang/String;
    :catch_0
    move-exception v10

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    move-object v6, v7

    .line 177
    .end local v7    # "key":Ljava/lang/String;
    .restart local v6    # "key":Ljava/lang/String;
    goto :goto_0

    .line 170
    .end local v0    # "b":[B
    .end local v8    # "length":I
    .end local v9    # "value":I
    :catch_1
    move-exception v5

    .line 171
    .local v5, "ioe":Ljava/io/IOException;
    :goto_1
    :try_start_4
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 174
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    .line 175
    :catch_2
    move-exception v10

    goto :goto_0

    .line 173
    .end local v5    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 174
    :goto_2
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 176
    :goto_3
    throw v10

    .line 175
    :catch_3
    move-exception v11

    goto :goto_3

    .line 173
    .end local v1    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :catchall_1
    move-exception v10

    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_2

    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v10

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_2

    .line 170
    .end local v1    # "file":Ljava/io/File;
    .restart local v2    # "file":Ljava/io/File;
    :catch_4
    move-exception v5

    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_1

    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "fis":Ljava/io/FileInputStream;
    .restart local v2    # "file":Ljava/io/File;
    .restart local v4    # "fis":Ljava/io/FileInputStream;
    :catch_5
    move-exception v5

    move-object v3, v4

    .end local v4    # "fis":Ljava/io/FileInputStream;
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    move-object v1, v2

    .end local v2    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    goto :goto_1
.end method

.method static native nativeChangeKEK(ILjava/lang/String;Ljava/lang/String;)Z
.end method

.method static native nativeCreateKEKMdm(ILjava/lang/String;)[B
.end method

.method static native nativeCreateKEKPwd(ILjava/lang/String;)[B
.end method

.method static native nativeGetEcryptfsKeySkmm1(ILjava/lang/String;)[B
.end method

.method static native nativeIsSkmm2Supported()Z
.end method

.method static native nativeVerifyKEKMdm(ILjava/lang/String;)[B
.end method

.method static native nativeVerifyKEKPwd(ILjava/lang/String;)[B
.end method

.method private storeInFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 135
    const/4 v5, 0x0

    .line 136
    .local v5, "result":Z
    const/4 v0, 0x0

    .line 137
    .local v0, "file":Ljava/io/File;
    const/4 v2, 0x0

    .line 139
    .local v2, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 140
    .end local v0    # "file":Ljava/io/File;
    .local v1, "file":Ljava/io/File;
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 141
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .local v3, "fos":Ljava/io/FileOutputStream;
    :try_start_2
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/io/FileOutputStream;->write([B)V

    .line 142
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 143
    const/4 v5, 0x1

    .line 148
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 152
    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    :goto_0
    return v5

    .line 149
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_0
    move-exception v6

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 151
    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    goto :goto_0

    .line 144
    :catch_1
    move-exception v4

    .line 145
    .local v4, "ioe":Ljava/io/IOException;
    :goto_1
    const/4 v5, 0x0

    .line 148
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 149
    :catch_2
    move-exception v6

    goto :goto_0

    .line 147
    .end local v4    # "ioe":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 148
    :goto_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 150
    :goto_3
    throw v6

    .line 149
    :catch_3
    move-exception v7

    goto :goto_3

    .line 147
    .end local v0    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    :catchall_1
    move-exception v6

    move-object v0, v1

    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    goto :goto_2

    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v6

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    goto :goto_2

    .line 144
    .end local v0    # "file":Ljava/io/File;
    .restart local v1    # "file":Ljava/io/File;
    :catch_4
    move-exception v4

    move-object v0, v1

    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    goto :goto_1

    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v4

    move-object v2, v3

    .end local v3    # "fos":Ljava/io/FileOutputStream;
    .restart local v2    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "file":Ljava/io/File;
    .restart local v0    # "file":Ljava/io/File;
    goto :goto_1
.end method


# virtual methods
.method public changePassword(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "newPassword"    # Ljava/lang/String;
    .param p3, "oldPassword"    # Ljava/lang/String;

    .prologue
    .line 318
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside changeKEK "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 320
    :cond_0
    const/4 v0, 0x0

    .line 322
    :goto_0
    return v0

    :cond_1
    invoke-static {p1, p2, p3}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeChangeKEK(ILjava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public createKEKMdm(ILjava/lang/String;)[B
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "resetPwdKey"    # Ljava/lang/String;

    .prologue
    .line 354
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside createKEK "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    if-nez p2, :cond_0

    .line 356
    const/4 v0, 0x0

    .line 358
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeCreateKEKMdm(ILjava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public createKEKPwd(ILjava/lang/String;)[B
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 345
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside createKEK "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    if-nez p2, :cond_0

    .line 347
    const/4 v0, 0x0

    .line 349
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeCreateKEKPwd(ILjava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public generateAndStoreCMK(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "userId"    # I
    .param p2, "pw"    # Ljava/lang/String;
    .param p3, "pwRstToken"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 478
    invoke-virtual {p0, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateCMK(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 479
    .local v0, "cmk":Ljava/lang/String;
    const-string v3, "KeyManagementUtil"

    const-string v4, "generateAndStoreCMK"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 481
    if-nez v0, :cond_0

    const-string v2, "KeyManagementUtil"

    const-string v3, "can\'t generate CMK"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :goto_0
    return v1

    .line 483
    :cond_0
    invoke-virtual {p0, p1, v0, p2, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->storeCMK(ILjava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 484
    const-string v2, "KeyManagementUtil"

    const-string v3, "storeCMK(PW) failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 488
    :cond_1
    const/4 v3, 0x2

    invoke-virtual {p0, p1, v0, p3, v3}, Lcom/sec/knox/container/util/KeyManagementUtil;->storeCMK(ILjava/lang/String;Ljava/lang/String;I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 489
    const-string v2, "KeyManagementUtil"

    const-string v3, "storeCMK(RST_TOKEN) failed"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v1, v2

    .line 493
    goto :goto_0
.end method

.method public generateCMK(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 83
    if-nez p1, :cond_0

    .line 84
    const-string v6, "KeyManagementUtil"

    const-string v7, "inside Error generateCMK password is NULL!!!!  "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    const/4 v3, 0x0

    .line 102
    :goto_0
    return-object v3

    .line 87
    :cond_0
    const/4 v3, 0x0

    .line 89
    .local v3, "masterKey":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateSalt()[B

    move-result-object v5

    .line 90
    .local v5, "salt":[B
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v6, "HmacSHA1"

    invoke-direct {v2, v5, v6}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 91
    .local v2, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string v6, "HmacSHA1"

    const-string v7, "AndroidOpenSSL"

    invoke-static {v6, v7}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    .line 92
    .local v1, "hmac":Ljavax/crypto/Mac;
    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 95
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v6

    invoke-virtual {v1, v6}, Ljavax/crypto/Mac;->doFinal([B)[B

    move-result-object v4

    .line 96
    .local v4, "rawHmac":[B
    const/4 v6, 0x0

    invoke-static {v4, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x10

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    .line 98
    .end local v1    # "hmac":Ljavax/crypto/Mac;
    .end local v2    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v4    # "rawHmac":[B
    .end local v5    # "salt":[B
    :catch_0
    move-exception v0

    .line 99
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 100
    const-string v6, "KeyManagementUtil"

    const-string v7, "Error inside generateCMK "

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public generateECMKWithMdm(ILjava/lang/String;[B)Z
    .locals 7
    .param p1, "userId"    # I
    .param p2, "containerMasterKey"    # Ljava/lang/String;
    .param p3, "kek"    # [B

    .prologue
    const/4 v4, 0x0

    .line 185
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v3, v4

    .line 203
    :goto_0
    return v3

    .line 188
    :cond_1
    const/4 v3, 0x0

    .line 190
    .local v3, "result":Z
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, p3, v6}, Lcom/sec/knox/container/util/KeyManagementUtil;->aesEncrypt([B[B[B)[B

    move-result-object v1

    .line 191
    .local v1, "encBytes":[B
    if-nez v1, :cond_2

    .line 192
    const-string v5, "KeyManagementUtil"

    const-string v6, "encBytes is null in generateECMKWithMdm "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 193
    goto :goto_0

    .line 195
    :cond_2
    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, "masterKeyEncWithMdm":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/system/users/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ECMK_MDM"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->storeInFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 197
    const/4 v3, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "encBytes":[B
    .end local v2    # "masterKeyEncWithMdm":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 200
    const/4 v3, 0x0

    .line 201
    const-string v4, "KeyManagementUtil"

    const-string v5, "Error inside generateECMKWithMdm "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public generateECMKWithPwd(ILjava/lang/String;[B)Z
    .locals 7
    .param p1, "userId"    # I
    .param p2, "containerMasterKey"    # Ljava/lang/String;
    .param p3, "kek"    # [B

    .prologue
    const/4 v4, 0x0

    .line 107
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    move v3, v4

    .line 131
    :goto_0
    return v3

    .line 110
    :cond_1
    const/4 v3, 0x0

    .line 112
    .local v3, "result":Z
    :try_start_0
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, p3, v6}, Lcom/sec/knox/container/util/KeyManagementUtil;->aesEncrypt([B[B[B)[B

    move-result-object v1

    .line 117
    .local v1, "encBytes":[B
    if-nez v1, :cond_2

    .line 118
    const-string v5, "KeyManagementUtil"

    const-string v6, "encBytes is null  "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v4

    .line 119
    goto :goto_0

    .line 121
    :cond_2
    const/4 v4, 0x0

    invoke-static {v1, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    .line 124
    .local v2, "masterKeyEncWithPwd":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/data/system/users/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ECMK_PWD"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->storeInFile(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    const/4 v3, 0x1

    goto :goto_0

    .line 126
    .end local v1    # "encBytes":[B
    .end local v2    # "masterKeyEncWithPwd":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 127
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 128
    const/4 v3, 0x0

    .line 129
    const-string v4, "KeyManagementUtil"

    const-string v5, "inside Error generateECMKWithPwd  "

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public generateEcryptfsKey(Ljava/lang/String;)[B
    .locals 7
    .param p1, "password"    # Ljava/lang/String;

    .prologue
    .line 62
    if-nez p1, :cond_0

    .line 63
    const/4 v1, 0x0

    .line 78
    :goto_0
    return-object v1

    .line 65
    :cond_0
    const/4 v1, 0x0

    .line 67
    .local v1, "ecryptfsKey":[B
    :try_start_0
    invoke-static {}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateSalt()[B

    move-result-object v4

    .line 68
    .local v4, "salt":[B
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    const-string v5, "HmacSHA256"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 69
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    const-string v5, "HmacSHA256"

    const-string v6, "AndroidOpenSSL"

    invoke-static {v5, v6}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v2

    .line 70
    .local v2, "hmac":Ljavax/crypto/Mac;
    invoke-virtual {v2, v3}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V

    .line 73
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    invoke-virtual {v2, v5}, Ljavax/crypto/Mac;->doFinal([B)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 74
    .end local v2    # "hmac":Ljavax/crypto/Mac;
    .end local v3    # "key":Ljavax/crypto/spec/SecretKeySpec;
    .end local v4    # "salt":[B
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 76
    const-string v5, "KeyManagementUtil"

    const-string v6, "Error inside generateCMK "

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public generatePasswordResetToken()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x10

    .line 376
    new-instance v1, Ljava/security/SecureRandom;

    invoke-direct {v1}, Ljava/security/SecureRandom;-><init>()V

    .line 377
    .local v1, "random":Ljava/security/SecureRandom;
    new-array v2, v4, [B

    .line 378
    .local v2, "salt":[B
    invoke-virtual {v1, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 379
    invoke-static {v2, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 380
    .local v0, "passwordResetToken":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v4, :cond_0

    .line 381
    invoke-virtual {v0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 383
    :cond_0
    return-object v0
.end method

.method public getEcryptfsKeySkmm1(ILjava/lang/String;)[B
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "eCryptfsPwd"    # Ljava/lang/String;

    .prologue
    .line 363
    if-nez p2, :cond_0

    .line 364
    const/4 v0, 0x0

    .line 367
    :goto_0
    return-object v0

    .line 366
    :cond_0
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside getEcryptfsKeySkmm1 "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 367
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeGetEcryptfsKeySkmm1(ILjava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public getPlainCMKWithMdm(I[B)Ljava/lang/String;
    .locals 5
    .param p1, "userId"    # I
    .param p2, "kek"    # [B

    .prologue
    const/4 v2, 0x0

    .line 293
    if-nez p2, :cond_0

    .line 307
    :goto_0
    return-object v2

    .line 296
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/system/users/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ECMK_MDM"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/knox/container/util/KeyManagementUtil;->getKeyFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 297
    .local v0, "encCMK":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 298
    const-string v3, "KeyManagementUtil"

    const-string v4, "getPlainCMKWithMdm encCMK is null!!."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_1
    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-static {v3, p2, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->aesDecrypt([B[B[B)[B

    move-result-object v1

    .line 303
    .local v1, "plainCMK":[B
    if-nez v1, :cond_2

    .line 304
    const-string v3, "KeyManagementUtil"

    const-string v4, "getPlainCMKWithMdm CMK is null!!."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 307
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public getPlainCMKWithPwd(I[B)Ljava/lang/String;
    .locals 5
    .param p1, "userId"    # I
    .param p2, "kek"    # [B

    .prologue
    const/4 v2, 0x0

    .line 274
    if-nez p2, :cond_0

    .line 288
    :goto_0
    return-object v2

    .line 277
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/data/system/users/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ECMK_PWD"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/knox/container/util/KeyManagementUtil;->getKeyFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 278
    .local v0, "encCMK":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 279
    const-string v3, "KeyManagementUtil"

    const-string v4, "getPlainCMKWithPwd encCMK is null!!."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :cond_1
    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v3

    invoke-static {v3, p2, v2}, Lcom/sec/knox/container/util/KeyManagementUtil;->aesDecrypt([B[B[B)[B

    move-result-object v1

    .line 284
    .local v1, "plainCMK":[B
    if-nez v1, :cond_2

    .line 285
    const-string v3, "KeyManagementUtil"

    const-string v4, "getPlainCMKWithPwd CMK is null!!."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 288
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method

.method public isCMKExists(II)Z
    .locals 1
    .param p1, "userId"    # I
    .param p2, "type"    # I

    .prologue
    .line 412
    packed-switch p2, :pswitch_data_0

    .line 418
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 414
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/knox/container/util/KeyManagementUtil;->isECMKPWDFileExists(I)Z

    move-result v0

    goto :goto_0

    .line 416
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/knox/container/util/KeyManagementUtil;->isECMKMDMFileExists(I)Z

    move-result v0

    goto :goto_0

    .line 412
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isECMKMDMFileExists(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 400
    const/4 v1, 0x0

    .line 402
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/system/users/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ECMK_MDM"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 403
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 404
    const/4 v1, 0x1

    .line 407
    :cond_0
    return v1
.end method

.method public isECMKPWDFileExists(I)Z
    .locals 4
    .param p1, "userId"    # I

    .prologue
    .line 388
    const/4 v1, 0x0

    .line 390
    .local v1, "result":Z
    new-instance v0, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/system/users/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ECMK_PWD"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 391
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 392
    const/4 v1, 0x1

    .line 395
    :cond_0
    return v1
.end method

.method public isSkmm2Supported()Z
    .locals 2

    .prologue
    .line 312
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside isSkmm2Supported "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    invoke-static {}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeIsSkmm2Supported()Z

    move-result v0

    return v0
.end method

.method public retrieveCMK(ILjava/lang/String;I)Ljava/lang/String;
    .locals 5
    .param p1, "userId"    # I
    .param p2, "pw"    # Ljava/lang/String;
    .param p3, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 424
    const/4 v0, 0x0

    .line 426
    .local v0, "kek":[B
    const-string v2, "KeyManagementUtil"

    const-string v3, "retrieveCMK"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    packed-switch p3, :pswitch_data_0

    .line 447
    const-string v2, "KeyManagementUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unknown pw-type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 451
    :goto_0
    return-object v1

    .line 429
    :pswitch_0
    invoke-virtual {p0, p1}, Lcom/sec/knox/container/util/KeyManagementUtil;->isECMKPWDFileExists(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 430
    const-string v2, "KeyManagementUtil"

    const-string v3, "can\'t find ECMD_PWD"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 434
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->verifyKEKPwd(ILjava/lang/String;)[B

    move-result-object v0

    .line 435
    if-nez v0, :cond_1

    const-string v2, "KeyManagementUtil"

    const-string v3, "invalid password"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 436
    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/sec/knox/container/util/KeyManagementUtil;->getPlainCMKWithPwd(I[B)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 438
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/sec/knox/container/util/KeyManagementUtil;->isECMKMDMFileExists(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 439
    const-string v2, "KeyManagementUtil"

    const-string v3, "can\'t find ECMD_MDM"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 443
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->verifyKEKMdm(ILjava/lang/String;)[B

    move-result-object v0

    .line 444
    if-nez v0, :cond_3

    const-string v2, "KeyManagementUtil"

    const-string v3, "invalid reset-token"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 445
    :cond_3
    invoke-virtual {p0, p1, v0}, Lcom/sec/knox/container/util/KeyManagementUtil;->getPlainCMKWithMdm(I[B)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 427
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public storeCMK(ILjava/lang/String;Ljava/lang/String;I)Z
    .locals 5
    .param p1, "userId"    # I
    .param p2, "cmk"    # Ljava/lang/String;
    .param p3, "pw"    # Ljava/lang/String;
    .param p4, "type"    # I

    .prologue
    const/4 v1, 0x0

    .line 456
    const/4 v0, 0x0

    .line 458
    .local v0, "kek":[B
    const-string v2, "KeyManagementUtil"

    const-string v3, "storeCMK"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    packed-switch p4, :pswitch_data_0

    .line 469
    const-string v2, "KeyManagementUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unknown pw-type : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    :goto_0
    return v1

    .line 461
    :pswitch_0
    invoke-virtual {p0, p1, p3}, Lcom/sec/knox/container/util/KeyManagementUtil;->createKEKPwd(ILjava/lang/String;)[B

    move-result-object v0

    .line 462
    if-nez v0, :cond_0

    const-string v2, "KeyManagementUtil"

    const-string v3, "can\'t create kek-pw"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :cond_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateECMKWithPwd(ILjava/lang/String;[B)Z

    move-result v1

    goto :goto_0

    .line 465
    :pswitch_1
    invoke-virtual {p0, p1, p3}, Lcom/sec/knox/container/util/KeyManagementUtil;->createKEKMdm(ILjava/lang/String;)[B

    move-result-object v0

    .line 466
    if-nez v0, :cond_1

    const-string v2, "KeyManagementUtil"

    const-string v3, "can\'t create kek-rst_token"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 467
    :cond_1
    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/knox/container/util/KeyManagementUtil;->generateECMKWithMdm(ILjava/lang/String;[B)Z

    move-result v1

    goto :goto_0

    .line 459
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public verifyKEKMdm(ILjava/lang/String;)[B
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "mdmSecreteKey"    # Ljava/lang/String;

    .prologue
    .line 336
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside verifyKEKMdm "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 337
    if-nez p2, :cond_0

    .line 338
    const/4 v0, 0x0

    .line 340
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeVerifyKEKMdm(ILjava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public verifyKEKPwd(ILjava/lang/String;)[B
    .locals 2
    .param p1, "personaId"    # I
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 327
    const-string v0, "KeyManagementUtil"

    const-string v1, " inside verifyKEKPwd "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    if-nez p2, :cond_0

    .line 329
    const/4 v0, 0x0

    .line 331
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/KeyManagementUtil;->nativeVerifyKEKPwd(ILjava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method
