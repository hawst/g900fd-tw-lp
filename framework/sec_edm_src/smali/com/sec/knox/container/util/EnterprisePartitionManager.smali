.class public Lcom/sec/knox/container/util/EnterprisePartitionManager;
.super Ljava/lang/Object;
.source "EnterprisePartitionManager.java"

# interfaces
.implements Lcom/sec/knox/container/util/IDaemonConnectorCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/container/util/EnterprisePartitionManager$EpmResponseCode;,
        Lcom/sec/knox/container/util/EnterprisePartitionManager$Request;
    }
.end annotation


# static fields
.field public static final CopyFlagExitOneError:I = 0x8

.field public static final CopyFlagForce:I = 0x1

.field public static final CopyFlagRecursive:I = 0x2

.field public static final CopyFlagRemoveFrom:I = 0x4

.field public static final CopyFlagRenameBeforeRemove:I = 0x10

.field public static final CopyFlagRenameWithNumber:I = 0x20

.field public static final ENODEV:I = 0x13

.field public static final ENOENT:I = 0x2

.field private static final EPM_SOCKET_NAME:Ljava/lang/String; = "epm"

.field static final EPM_TAG:Ljava/lang/String; = "EnterprisePartitionManager"

.field public static final PartitionInserted:I = 0x276

.field public static final PartitionRemoved:I = 0x277

.field private static SEANDROID_SECURITY_VERIFICATION:Z

.field private static sContext:Landroid/content/Context;

.field private static sInstance:Lcom/sec/knox/container/util/EnterprisePartitionManager;


# instance fields
.field private mConnector:Lcom/sec/knox/container/util/DaemonConnector;

.field private mReady:Z

.field private mSessionIdDstPath:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    sput-object v1, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sContext:Landroid/content/Context;

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->SEANDROID_SECURITY_VERIFICATION:Z

    .line 77
    sput-object v1, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sInstance:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mReady:Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    .line 88
    invoke-direct {p0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->createConnector()V

    .line 89
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    .line 90
    return-void
.end method

.method private static checkCallerPermissionFor(Ljava/lang/String;)I
    .locals 5
    .param p0, "methodName"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v1, "EnterprisePartitionManager"

    .line 52
    .local v1, "serviceName":Ljava/lang/String;
    sget-object v2, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/enterprise/knox/seams/SEAMS;->getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/seams/SEAMS;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v2, v3, v4, v1, p0}, Lcom/sec/enterprise/knox/seams/SEAMS;->forceAuthorized(IILjava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 53
    sget-boolean v2, Lcom/sec/knox/container/util/EnterprisePartitionManager;->SEANDROID_SECURITY_VERIFICATION:Z

    if-eqz v2, :cond_0

    .line 54
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security Exception Occurred while pid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] with uid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] trying to access methodName ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] in ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 57
    throw v0

    .line 59
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_0
    const-string v2, "EnterprisePartitionManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Security Exception Occurred while pid["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] with uid["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] trying to access methodName ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] in ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] service"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Security Exception Occurred while pid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] with uid["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] trying to access methodName ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] in ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] service"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    .line 63
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 67
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v2, 0x0

    return v2
.end method

.method private command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;
    .locals 4
    .param p1, "cmd"    # Lcom/sec/knox/container/util/DaemonConnector$Command;

    .prologue
    .line 115
    const/4 v1, 0x0

    .line 117
    .local v1, "event":Lcom/sec/knox/container/util/DaemonEvent;
    iget-object v2, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    if-eqz v2, :cond_0

    .line 119
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    invoke-virtual {v2, p1}, Lcom/sec/knox/container/util/DaemonConnector;->execute(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 120
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/knox/container/util/DaemonEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->logD(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/knox/container/util/DaemonConnectorException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move-object v2, v1

    .line 126
    :goto_0
    return-object v2

    .line 121
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Lcom/sec/knox/container/util/DaemonConnectorException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send command"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/knox/container/util/DaemonConnectorException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->logE(Ljava/lang/String;)V

    .line 123
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private createConnector()V
    .locals 7

    .prologue
    .line 71
    const-string v0, "createConnector() for socket epm"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->logD(Ljava/lang/String;)V

    .line 72
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector;

    const-string v2, "epm"

    const/16 v3, 0x1f4

    const-string v4, "EnterprisePartitionManager"

    const/16 v5, 0xa0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/knox/container/util/DaemonConnector;-><init>(Lcom/sec/knox/container/util/IDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    .line 73
    new-instance v6, Ljava/lang/Thread;

    iget-object v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    const-string v1, "EnterprisePartitionManager"

    invoke-direct {v6, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 74
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 75
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/container/util/EnterprisePartitionManager;
    .locals 1
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 80
    sput-object p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sContext:Landroid/content/Context;

    .line 81
    sget-object v0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sInstance:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/sec/knox/container/util/EnterprisePartitionManager;

    invoke-direct {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;-><init>()V

    sput-object v0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sInstance:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    .line 83
    :cond_0
    const-string v0, "getInstance"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 84
    sget-object v0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->sInstance:Lcom/sec/knox/container/util/EnterprisePartitionManager;

    return-object v0
.end method

.method private isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 206
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0x119

    if-ne v0, v1, :cond_1

    .line 207
    :cond_0
    const/4 v0, 0x1

    .line 209
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnGoing(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 199
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_0

    .line 200
    const/4 v0, 0x1

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 192
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 193
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logD(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 130
    const-string v0, "EnterprisePartitionManager"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    return-void
.end method

.method public static logE(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 134
    const-string v0, "EnterprisePartitionManager"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    return-void
.end method

.method private scanDir(Ljava/lang/String;J)Lcom/sec/knox/container/util/DaemonEvent;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "sessionId"    # J

    .prologue
    .line 534
    const/4 v0, 0x0

    .line 535
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 537
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "scan_dir"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 539
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 541
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 543
    return-object v1
.end method


# virtual methods
.method public cancelCopyChunks(J)V
    .locals 7
    .param p1, "sessionId"    # J

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 616
    const-string v3, "cancelCopyChunks"

    invoke-static {v3}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 617
    const/4 v0, 0x0

    .line 618
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v2, 0x0

    .line 619
    .local v2, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    iget-object v3, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 620
    .local v1, "dstRealPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "copy"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-direct {v0, v3, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 623
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "cancel"

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 624
    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 625
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 626
    invoke-virtual {v0, v1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 627
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 628
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 629
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 630
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 631
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 633
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v2

    .line 635
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 636
    iget-object v3, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public changePassword(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "containerId"    # I
    .param p2, "oldPassword"    # Ljava/lang/String;
    .param p3, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 357
    const-string v2, "changePassword"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 358
    const/4 v0, 0x0

    .line 359
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 361
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "change_password"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 362
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 363
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 364
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 366
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 368
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public changePermissionMigration(Ljava/lang/String;III)I
    .locals 5
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "uid"    # I
    .param p3, "gid"    # I
    .param p4, "mode"    # I

    .prologue
    const/4 v2, 0x0

    .line 827
    const/4 v0, 0x0

    .line 828
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 830
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "change_permission_migration"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-direct {v0, v3, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 831
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 832
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 833
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 834
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 836
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 838
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 847
    :cond_0
    :goto_0
    return v2

    .line 840
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 841
    const/4 v2, -0x1

    .line 842
    .local v2, "rc":I
    if-eqz v1, :cond_2

    .line 843
    invoke-virtual {v1}, Lcom/sec/knox/container/util/DaemonEvent;->getSubErrorCode()I

    move-result v2

    .line 844
    :cond_2
    if-ltz v2, :cond_0

    .line 847
    .end local v2    # "rc":I
    :cond_3
    const/4 v2, -0x1

    goto :goto_0
.end method

.method public changePrivateModePassword(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "oldPassword"    # Ljava/lang/String;
    .param p3, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 343
    const/4 v0, 0x0

    .line 344
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 346
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "privatemode_change_password"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 348
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 349
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 351
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 353
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public copy(Ljava/lang/String;ILjava/lang/String;I)I
    .locals 6
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "fromContainerId"    # I
    .param p3, "toPath"    # Ljava/lang/String;
    .param p4, "toContainerId"    # I

    .prologue
    .line 701
    const-string v0, "copy"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 702
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->copy(Ljava/lang/String;ILjava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public copy(Ljava/lang/String;ILjava/lang/String;II)I
    .locals 8
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "fromContainerId"    # I
    .param p3, "toPath"    # Ljava/lang/String;
    .param p4, "toContainerId"    # I
    .param p5, "copyFlag"    # I

    .prologue
    const/4 v6, -0x2

    const/16 v3, -0x13

    const/4 v5, 0x0

    .line 711
    const-string v7, "copy"

    invoke-static {v7}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 712
    const/4 v0, 0x0

    .line 713
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v2, 0x0

    .line 718
    .local v2, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 719
    .local v4, "srcRealPath":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move v3, v6

    .line 756
    :cond_1
    :goto_0
    return v3

    .line 722
    :cond_2
    invoke-static {p3, p4}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 723
    .local v1, "dstRealPath":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    move v3, v6

    .line 724
    goto :goto_0

    .line 726
    :cond_4
    if-lez p2, :cond_5

    invoke-virtual {p0, p2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 729
    :cond_5
    if-lez p4, :cond_6

    invoke-virtual {p0, p4}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 733
    :cond_6
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "copy"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-direct {v0, v6, v7}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 734
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "whole"

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 735
    invoke-virtual {v0, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 736
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 737
    invoke-virtual {v0, v1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 738
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 739
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 740
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 741
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 742
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 744
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v2

    .line 746
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_7

    move v3, v5

    .line 747
    goto :goto_0

    .line 749
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 750
    const/4 v3, -0x1

    .line 751
    .local v3, "rc":I
    if-eqz v2, :cond_8

    .line 752
    invoke-virtual {v2}, Lcom/sec/knox/container/util/DaemonEvent;->getSubErrorCode()I

    move-result v3

    .line 753
    :cond_8
    if-ltz v3, :cond_1

    .line 756
    .end local v3    # "rc":I
    :cond_9
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public copyChunks(Ljava/lang/String;ILjava/lang/String;IJIJZ)I
    .locals 8
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "fromContainerId"    # I
    .param p3, "toPath"    # Ljava/lang/String;
    .param p4, "toContainerId"    # I
    .param p5, "offset"    # J
    .param p7, "length"    # I
    .param p8, "sessionId"    # J
    .param p10, "deleteSource"    # Z

    .prologue
    .line 643
    const-string v6, "copyChunks"

    invoke-static {v6}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 644
    const/4 v0, 0x0

    .line 645
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v2, 0x0

    .line 646
    .local v2, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    const/16 v3, 0x20

    .line 648
    .local v3, "flag":I
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .line 649
    .local v5, "srcRealPath":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 650
    :cond_0
    const/4 v4, -0x2

    .line 697
    :cond_1
    :goto_0
    return v4

    .line 652
    :cond_2
    invoke-static {p3, p4}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 653
    .local v1, "dstRealPath":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 654
    :cond_3
    const/4 v4, -0x2

    goto :goto_0

    .line 656
    :cond_4
    if-lez p2, :cond_5

    invoke-virtual {p0, p2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-nez v6, :cond_5

    .line 657
    const/16 v4, -0x13

    goto :goto_0

    .line 659
    :cond_5
    if-lez p4, :cond_6

    invoke-virtual {p0, p4}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 660
    const/16 v4, -0x13

    goto :goto_0

    .line 663
    :cond_6
    iget-object v6, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 664
    iget-object v6, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 666
    :cond_7
    if-eqz p10, :cond_8

    .line 667
    or-int/lit8 v3, v3, 0x4

    .line 669
    :cond_8
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "copy"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-direct {v0, v6, v7}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 670
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "chunk"

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 671
    invoke-virtual {v0, v5}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 672
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 673
    invoke-virtual {v0, v1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 674
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 675
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 676
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 677
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 678
    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 680
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v2

    .line 682
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isOnGoing(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 683
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 685
    :cond_9
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 686
    iget-object v6, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    invoke-static/range {p8 .. p9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 687
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 689
    :cond_a
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 690
    const/4 v4, -0x1

    .line 691
    .local v4, "rc":I
    if-eqz v2, :cond_b

    .line 692
    invoke-virtual {v2}, Lcom/sec/knox/container/util/DaemonEvent;->getSubErrorCode()I

    move-result v4

    .line 693
    :cond_b
    if-ltz v4, :cond_1

    .line 697
    .end local v4    # "rc":I
    :cond_c
    const/4 v4, -0x1

    goto/16 :goto_0
.end method

.method public createExtSdPartition(ILjava/lang/String;Ljava/lang/String;J)Z
    .locals 4
    .param p1, "containerId"    # I
    .param p2, "imgPath"    # Ljava/lang/String;
    .param p3, "signature"    # Ljava/lang/String;
    .param p4, "blocks"    # J

    .prologue
    .line 429
    const-string v2, "createExtSdPartition"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 430
    const/4 v0, 0x0

    .line 431
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 433
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "create_extsd"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 434
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 435
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 436
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 437
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 439
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 441
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public createPartition(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 388
    const-string v2, "createPartition"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 389
    const/4 v0, 0x0

    .line 390
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 392
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "create_partition"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 393
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 395
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 397
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public deleteFile(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I

    .prologue
    .line 514
    const-string v3, "deleteFile"

    invoke-static {v3}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 515
    const/4 v0, 0x0

    .line 516
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 518
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 519
    .local v2, "realPath":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 520
    :cond_0
    const-string v3, "EnterprisePartitionManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path translation failed for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 521
    move-object v2, p1

    .line 524
    :cond_1
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "delete_file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {v0, v3, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 525
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 526
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 528
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 530
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v3

    return v3
.end method

.method public getFileInfo(Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 10
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I

    .prologue
    .line 572
    const-string v7, "getFileInfo"

    invoke-static {v7}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 573
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 574
    .local v0, "b":Landroid/os/Bundle;
    const/4 v6, 0x0

    .line 576
    .local v6, "res":I
    const/4 v1, 0x0

    .line 577
    .local v1, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v2, 0x0

    .line 578
    .local v2, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 579
    .local v3, "realPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 580
    :cond_0
    const/4 v6, -0x2

    .line 581
    const-string v7, "result"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 612
    :cond_1
    :goto_0
    return-object v0

    .line 585
    :cond_2
    new-instance v1, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v1    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v7, "get_file_info"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-direct {v1, v7, v8}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 586
    .restart local v1    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v1, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 587
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 589
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v2

    .line 591
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 592
    const/4 v6, 0x0

    .line 593
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 594
    if-eqz v2, :cond_4

    .line 595
    invoke-virtual {v2}, Lcom/sec/knox/container/util/DaemonEvent;->getSubErrorCode()I

    move-result v6

    .line 596
    :cond_4
    if-nez v6, :cond_5

    .line 597
    const/4 v6, -0x1

    .line 600
    :cond_5
    const-wide/16 v4, 0x0

    .line 601
    .local v4, "lastModified":J
    if-eqz v2, :cond_6

    .line 602
    invoke-virtual {v2}, Lcom/sec/knox/container/util/DaemonEvent;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    int-to-long v4, v7

    .line 603
    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    .line 607
    :cond_6
    const-string v7, "result"

    invoke-virtual {v0, v7, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 608
    if-nez v6, :cond_1

    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-eqz v7, :cond_1

    .line 609
    const-string v7, "last_modified_date"

    invoke-virtual {v0, v7, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method public getFiles(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 547
    const-string v6, "getFiles"

    invoke-static {v6}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 548
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 549
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 550
    .local v4, "sessionId":J
    const/4 v0, 0x0

    .line 552
    .local v0, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    .line 553
    .local v3, "realPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_0
    move-object v2, v1

    .line 568
    .end local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v2, "fileList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v2

    .line 561
    .end local v2    # "fileList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    if-eqz v0, :cond_2

    .line 562
    invoke-virtual {v0}, Lcom/sec/knox/container/util/DaemonEvent;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_2
    invoke-direct {p0, v3, v4, v5}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->scanDir(Ljava/lang/String;J)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v0

    .line 559
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isOnGoing(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 565
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 566
    const/4 v1, 0x0

    :cond_3
    move-object v2, v1

    .line 568
    .restart local v2    # "fileList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0
.end method

.method public isFileExist(Ljava/lang/String;I)Z
    .locals 6
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "containerId"    # I

    .prologue
    .line 494
    const-string v3, "isFileExist"

    invoke-static {v3}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 495
    const/4 v0, 0x0

    .line 496
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 498
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 499
    .local v2, "realPath":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 500
    :cond_0
    const-string v3, "EnterprisePartitionManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "path translation failed for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 501
    move-object v2, p1

    .line 504
    :cond_1
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "is_file_exist"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {v0, v3, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 505
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 506
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 508
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 510
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v3

    return v3
.end method

.method public isMounted(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 809
    const-string v2, "isMounted"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 810
    const/4 v0, 0x0

    .line 811
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 813
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "is_mounted"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 814
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 816
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 818
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 93
    const-string v0, "isReady"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 94
    iget-boolean v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mReady:Z

    return v0
.end method

.method public mount(ILjava/lang/String;Z)Z
    .locals 5
    .param p1, "containerId"    # I
    .param p2, "password"    # Ljava/lang/String;
    .param p3, "sdpEnabled"    # Z

    .prologue
    const/4 v2, 0x0

    .line 213
    const-string v3, "mount"

    invoke-static {v3}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x0

    .line 215
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 217
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v3, "mount"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-direct {v0, v3, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 218
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 219
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 220
    if-eqz p3, :cond_0

    const/4 v2, 0x1

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 222
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 224
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public mountExtSd(ILjava/lang/String;Ljava/lang/String;ZZZII)Z
    .locals 4
    .param p1, "containerId"    # I
    .param p2, "imgPath"    # Ljava/lang/String;
    .param p3, "signature"    # Ljava/lang/String;
    .param p4, "ro"    # Z
    .param p5, "remount"    # Z
    .param p6, "executable"    # Z
    .param p7, "uid"    # I
    .param p8, "gid"    # I

    .prologue
    .line 461
    const-string v2, "mountExtSd"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 462
    const/4 v0, 0x0

    .line 463
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 465
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "mount_extsd"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 466
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 467
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 468
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 469
    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 470
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 471
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 472
    invoke-static {p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 473
    invoke-static {p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 475
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 477
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public mountOldContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Z
    .locals 4
    .param p1, "password"    # Ljava/lang/String;
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .param p4, "excludeMediaTypes"    # I
    .param p5, "containerId"    # I

    .prologue
    .line 308
    const/4 v0, 0x0

    .line 309
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 311
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "mount_legacy"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 312
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 313
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 314
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 315
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 316
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 318
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 320
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public mountPersonalPage(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "srcPath"    # Ljava/lang/String;
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 246
    const/4 v0, 0x0

    .line 247
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 249
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "mount_privatemode"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 250
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 251
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 252
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 253
    const-string v2, "0000"

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 255
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 257
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public mountPrivateMode(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .param p4, "pwd"    # Ljava/lang/String;

    .prologue
    .line 277
    const/4 v0, 0x0

    .line 278
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 280
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "mount_privatemode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 282
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 283
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 284
    invoke-virtual {v0, p4}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 286
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 288
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public move(Ljava/lang/String;ILjava/lang/String;I)I
    .locals 6
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "fromContainerId"    # I
    .param p3, "toPath"    # Ljava/lang/String;
    .param p4, "toContainerId"    # I

    .prologue
    .line 706
    const-string v0, "move"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 707
    const/16 v5, 0x24

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->move(Ljava/lang/String;ILjava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public move(Ljava/lang/String;ILjava/lang/String;II)I
    .locals 8
    .param p1, "fromPath"    # Ljava/lang/String;
    .param p2, "fromContainerId"    # I
    .param p3, "toPath"    # Ljava/lang/String;
    .param p4, "toContainerId"    # I
    .param p5, "copyFlag"    # I

    .prologue
    const/4 v6, -0x2

    const/16 v3, -0x13

    const/4 v5, 0x0

    .line 760
    const-string v7, "move"

    invoke-static {v7}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 761
    or-int/lit8 p5, p5, 0x4

    .line 762
    const/4 v0, 0x0

    .line 763
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v2, 0x0

    .line 768
    .local v2, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    invoke-static {p1, p2}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 769
    .local v4, "srcRealPath":Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_0
    move v3, v6

    .line 805
    :cond_1
    :goto_0
    return v3

    .line 772
    :cond_2
    invoke-static {p3, p4}, Lcom/sec/knox/container/util/PathTranslator;->getRealPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 773
    .local v1, "dstRealPath":Ljava/lang/String;
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    move v3, v6

    .line 774
    goto :goto_0

    .line 776
    :cond_4
    if-lez p2, :cond_5

    invoke-virtual {p0, p2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 779
    :cond_5
    if-lez p4, :cond_6

    invoke-virtual {p0, p4}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isMounted(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 783
    :cond_6
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "copy"

    new-array v7, v5, [Ljava/lang/Object;

    invoke-direct {v0, v6, v7}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 784
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v6, "whole"

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 785
    invoke-virtual {v0, v4}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 786
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 787
    invoke-virtual {v0, v1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 788
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 789
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 790
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 791
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 792
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 794
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v2

    .line 796
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v6

    if-eqz v6, :cond_7

    move v3, v5

    .line 797
    goto :goto_0

    .line 798
    :cond_7
    invoke-direct {p0, v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 799
    const/4 v3, -0x1

    .line 800
    .local v3, "rc":I
    if-eqz v2, :cond_8

    .line 801
    invoke-virtual {v2}, Lcom/sec/knox/container/util/DaemonEvent;->getSubErrorCode()I

    move-result v3

    .line 802
    :cond_8
    if-ltz v3, :cond_1

    .line 805
    .end local v3    # "rc":I
    :cond_9
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public onDaemonConnected()V
    .locals 1

    .prologue
    .line 100
    const-string v0, "onDaemonConnected"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 101
    const-string v0, "onDaemonConnected() for socket epm"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->logD(Ljava/lang/String;)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/container/util/EnterprisePartitionManager;->mReady:Z

    .line 103
    return-void
.end method

.method public onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "raw"    # Ljava/lang/String;
    .param p3, "cooked"    # [Ljava/lang/String;

    .prologue
    .line 107
    const-string v0, "onEvent"

    invoke-static {v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public removeExtSdPartition(ILjava/lang/String;)Z
    .locals 4
    .param p1, "containerId"    # I
    .param p2, "imgPath"    # Ljava/lang/String;

    .prologue
    .line 445
    const-string v2, "removeExtSdPartition"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 446
    const/4 v0, 0x0

    .line 447
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 449
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "remove_extsd"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 451
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 453
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 455
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public removePartition(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 402
    const-string v2, "removePartition"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x0

    .line 404
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 406
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "remove_partition"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 407
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 409
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 411
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public resetPartition(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 415
    const-string v2, "resetPartition"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 416
    const/4 v0, 0x0

    .line 417
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 419
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "reset_partition"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 422
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 424
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public unmount(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 232
    const-string v2, "unmount"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 233
    const/4 v0, 0x0

    .line 234
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 236
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 237
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 239
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 241
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public unmountExtSd(I)Z
    .locals 4
    .param p1, "containerId"    # I

    .prologue
    .line 481
    const-string v2, "unmountExtSd"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 482
    const/4 v0, 0x0

    .line 483
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 485
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount_extsd"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 486
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 488
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 490
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public unmountOldContainer(Ljava/lang/String;)Z
    .locals 4
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 329
    const/4 v0, 0x0

    .line 330
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 332
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount_legacy"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 335
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 337
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public unmountPersonalPage(Ljava/lang/String;)Z
    .locals 5
    .param p1, "dstPath"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 262
    const/4 v0, 0x0

    .line 263
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 265
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount_privatemode"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 267
    invoke-virtual {v0, p1}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 269
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 271
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public unmountPrivateMode(ILjava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 294
    const/4 v0, 0x0

    .line 295
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 297
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount_privatemode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 299
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 301
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 303
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public verifyPassword(ILjava/lang/String;)Z
    .locals 4
    .param p1, "containerId"    # I
    .param p2, "password"    # Ljava/lang/String;

    .prologue
    .line 373
    const-string v2, "verifyPassword"

    invoke-static {v2}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->checkCallerPermissionFor(Ljava/lang/String;)I

    .line 374
    const/4 v0, 0x0

    .line 375
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 377
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "verify_password"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 379
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 381
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 383
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/EnterprisePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method
