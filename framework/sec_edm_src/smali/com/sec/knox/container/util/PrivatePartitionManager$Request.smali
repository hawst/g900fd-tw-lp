.class Lcom/sec/knox/container/util/PrivatePartitionManager$Request;
.super Ljava/lang/Object;
.source "PrivatePartitionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/knox/container/util/PrivatePartitionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Request"
.end annotation


# static fields
.field static final PPM_CMD_MOUNT_PRIVATEMODE:Ljava/lang/String; = "mount_privatemode"

.field static final PPM_CMD_PRIVATEMODE_CHANGE_PASSWORD:Ljava/lang/String; = "privatemode_change_password"

.field static final PPM_CMD_UNMOUNT_PRIVATEMODE:Ljava/lang/String; = "unmount_privatemode"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
