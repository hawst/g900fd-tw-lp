.class public Lcom/sec/knox/container/util/PrivatePartitionManager;
.super Ljava/lang/Object;
.source "PrivatePartitionManager.java"

# interfaces
.implements Lcom/sec/knox/container/util/IDaemonConnectorCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/knox/container/util/PrivatePartitionManager$EpmResponseCode;,
        Lcom/sec/knox/container/util/PrivatePartitionManager$Request;
    }
.end annotation


# static fields
.field public static final CopyFlagExitOneError:I = 0x8

.field public static final CopyFlagForce:I = 0x1

.field public static final CopyFlagRecursive:I = 0x2

.field public static final CopyFlagRemoveFrom:I = 0x4

.field public static final CopyFlagRenameBeforeRemove:I = 0x10

.field public static final CopyFlagRenameWithNumber:I = 0x20

.field public static final ENODEV:I = 0x13

.field public static final ENOENT:I = 0x2

.field private static final PPM_SOCKET_NAME:Ljava/lang/String; = "ppm"

.field static final PPM_TAG:Ljava/lang/String; = "PrivatePartitionManager"

.field public static final PartitionInserted:I = 0x276

.field public static final PartitionRemoved:I = 0x277

.field private static SEANDROID_SECURITY_VERIFICATION:Z

.field private static sContext:Landroid/content/Context;

.field private static sInstance:Lcom/sec/knox/container/util/PrivatePartitionManager;


# instance fields
.field private mConnector:Lcom/sec/knox/container/util/DaemonConnector;

.field private mReady:Z

.field private mSessionIdDstPath:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 47
    sput-object v1, Lcom/sec/knox/container/util/PrivatePartitionManager;->sContext:Landroid/content/Context;

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/knox/container/util/PrivatePartitionManager;->SEANDROID_SECURITY_VERIFICATION:Z

    .line 78
    sput-object v1, Lcom/sec/knox/container/util/PrivatePartitionManager;->sInstance:Lcom/sec/knox/container/util/PrivatePartitionManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mReady:Z

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    .line 89
    invoke-direct {p0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->createConnector()V

    .line 90
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mSessionIdDstPath:Ljava/util/Hashtable;

    .line 91
    return-void
.end method

.method private static checkCallerPermissionFor(Ljava/lang/String;)I
    .locals 2
    .param p0, "methodName"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v0, "PrivatePartitionManager"

    .line 67
    .local v0, "serviceName":Ljava/lang/String;
    const/4 v1, 0x0

    return v1
.end method

.method private command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;
    .locals 4
    .param p1, "cmd"    # Lcom/sec/knox/container/util/DaemonConnector$Command;

    .prologue
    .line 116
    const/4 v1, 0x0

    .line 118
    .local v1, "event":Lcom/sec/knox/container/util/DaemonEvent;
    iget-object v2, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    if-eqz v2, :cond_0

    .line 120
    :try_start_0
    iget-object v2, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    invoke-virtual {v2, p1}, Lcom/sec/knox/container/util/DaemonConnector;->execute(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 121
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lcom/sec/knox/container/util/DaemonEvent;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/container/util/PrivatePartitionManager;->logD(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/knox/container/util/DaemonConnectorException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    move-object v2, v1

    .line 127
    :goto_0
    return-object v2

    .line 122
    :catch_0
    move-exception v0

    .line 123
    .local v0, "e":Lcom/sec/knox/container/util/DaemonConnectorException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to send command"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/sec/knox/container/util/DaemonConnectorException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/knox/container/util/PrivatePartitionManager;->logE(Ljava/lang/String;)V

    .line 124
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private createConnector()V
    .locals 7

    .prologue
    .line 71
    const-string v0, "createConnector() for socket ppm"

    invoke-static {v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->logD(Ljava/lang/String;)V

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Current Context: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/sec/knox/container/util/PrivatePartitionManager;->sContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->logD(Ljava/lang/String;)V

    .line 73
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector;

    const-string v2, "ppm"

    const/16 v3, 0x1f4

    const-string v4, "PrivatePartitionManager"

    const/16 v5, 0xa0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/knox/container/util/DaemonConnector;-><init>(Lcom/sec/knox/container/util/IDaemonConnectorCallbacks;Ljava/lang/String;ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    .line 74
    new-instance v6, Ljava/lang/Thread;

    iget-object v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mConnector:Lcom/sec/knox/container/util/DaemonConnector;

    const-string v1, "PrivatePartitionManager"

    invoke-direct {v6, v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 75
    .local v6, "thread":Ljava/lang/Thread;
    invoke-virtual {v6}, Ljava/lang/Thread;->start()V

    .line 76
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/knox/container/util/PrivatePartitionManager;
    .locals 1
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 81
    sput-object p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->sContext:Landroid/content/Context;

    .line 82
    sget-object v0, Lcom/sec/knox/container/util/PrivatePartitionManager;->sInstance:Lcom/sec/knox/container/util/PrivatePartitionManager;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/sec/knox/container/util/PrivatePartitionManager;

    invoke-direct {v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;-><init>()V

    sput-object v0, Lcom/sec/knox/container/util/PrivatePartitionManager;->sInstance:Lcom/sec/knox/container/util/PrivatePartitionManager;

    .line 85
    :cond_0
    sget-object v0, Lcom/sec/knox/container/util/PrivatePartitionManager;->sInstance:Lcom/sec/knox/container/util/PrivatePartitionManager;

    return-object v0
.end method

.method private isFailed(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 181
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0x119

    if-ne v0, v1, :cond_1

    .line 182
    :cond_0
    const/4 v0, 0x1

    .line 184
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isOnGoing(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 174
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0xc9

    if-ne v0, v1, :cond_0

    .line 175
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z
    .locals 2
    .param p1, "evt"    # Lcom/sec/knox/container/util/DaemonEvent;

    .prologue
    .line 167
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/knox/container/util/DaemonEvent;->getCode()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static logD(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 131
    const-string v0, "PrivatePartitionManager"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    return-void
.end method

.method public static logE(Ljava/lang/String;)V
    .locals 1
    .param p0, "msg"    # Ljava/lang/String;

    .prologue
    .line 135
    const-string v0, "PrivatePartitionManager"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    return-void
.end method


# virtual methods
.method public changePrivateModePassword(ILjava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "oldPassword"    # Ljava/lang/String;
    .param p3, "newPassword"    # Ljava/lang/String;

    .prologue
    .line 223
    const/4 v0, 0x0

    .line 224
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 226
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "privatemode_change_password"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 228
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 229
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 231
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 233
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/PrivatePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mReady:Z

    return v0
.end method

.method public mountPrivateMode(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "srcPath"    # Ljava/lang/String;
    .param p3, "dstPath"    # Ljava/lang/String;
    .param p4, "pwd"    # Ljava/lang/String;

    .prologue
    .line 191
    const/4 v0, 0x0

    .line 192
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 194
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "mount_privatemode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 195
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 196
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 197
    invoke-virtual {v0, p3}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 198
    invoke-virtual {v0, p4}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 200
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 202
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/PrivatePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method

.method public onDaemonConnected()V
    .locals 1

    .prologue
    .line 102
    const-string v0, "onDaemonConnected() for socket ppm"

    invoke-static {v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->logD(Ljava/lang/String;)V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/knox/container/util/PrivatePartitionManager;->mReady:Z

    .line 104
    return-void
.end method

.method public onEvent(ILjava/lang/String;[Ljava/lang/String;)Z
    .locals 1
    .param p1, "code"    # I
    .param p2, "raw"    # Ljava/lang/String;
    .param p3, "cooked"    # [Ljava/lang/String;

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method public unmountPrivateMode(ILjava/lang/String;)Z
    .locals 4
    .param p1, "userId"    # I
    .param p2, "dstPath"    # Ljava/lang/String;

    .prologue
    .line 209
    const/4 v0, 0x0

    .line 210
    .local v0, "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const/4 v1, 0x0

    .line 212
    .local v1, "evt":Lcom/sec/knox/container/util/DaemonEvent;
    new-instance v0, Lcom/sec/knox/container/util/DaemonConnector$Command;

    .end local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    const-string v2, "unmount_privatemode"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-direct {v0, v2, v3}, Lcom/sec/knox/container/util/DaemonConnector$Command;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    .restart local v0    # "cmd":Lcom/sec/knox/container/util/DaemonConnector$Command;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 214
    invoke-virtual {v0, p2}, Lcom/sec/knox/container/util/DaemonConnector$Command;->appendArg(Ljava/lang/Object;)Lcom/sec/knox/container/util/DaemonConnector$Command;

    .line 216
    invoke-direct {p0, v0}, Lcom/sec/knox/container/util/PrivatePartitionManager;->command(Lcom/sec/knox/container/util/DaemonConnector$Command;)Lcom/sec/knox/container/util/DaemonEvent;

    move-result-object v1

    .line 218
    invoke-direct {p0, v1}, Lcom/sec/knox/container/util/PrivatePartitionManager;->isSuccess(Lcom/sec/knox/container/util/DaemonEvent;)Z

    move-result v2

    return v2
.end method
