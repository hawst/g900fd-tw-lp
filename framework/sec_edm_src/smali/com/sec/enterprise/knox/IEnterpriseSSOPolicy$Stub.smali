.class public abstract Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseSSOPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

.field static final TRANSACTION_deleteSSOWhiteList:I = 0x4

.field static final TRANSACTION_forceReauthenticate:I = 0x7

.field static final TRANSACTION_getAppAllowedState:I = 0x5

.field static final TRANSACTION_getSSOCustomerId:I = 0x2

.field static final TRANSACTION_isSSOReady:I = 0xb

.field static final TRANSACTION_pushSSOData:I = 0xa

.field static final TRANSACTION_setCustomerInfo:I = 0x6

.field static final TRANSACTION_setSSOCustomerId:I = 0x1

.field static final TRANSACTION_setSSOWhiteList:I = 0x3

.field static final TRANSACTION_setupSSO:I = 0x9

.field static final TRANSACTION_unenroll:I = 0x8


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 14
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 348
    invoke-super/range {p0 .. p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 46
    :sswitch_0
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 47
    const/4 v1, 0x1

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 60
    .local v2, "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 62
    .local v3, "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 64
    .local v4, "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 65
    .local v5, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->setSSOCustomerId(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 66
    .local v13, "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 67
    if-eqz v13, :cond_1

    .line 68
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 69
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 74
    :goto_2
    const/4 v1, 0x1

    goto :goto_0

    .line 57
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_0
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_1

    .line 72
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_arg3":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_2

    .line 78
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_2
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 87
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_3
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 89
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 90
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->getSSOCustomerId(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 91
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    if-eqz v13, :cond_3

    .line 93
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 99
    :goto_4
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 84
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_3

    .line 97
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_3
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_4

    .line 103
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_3
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 106
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 112
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_5
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 114
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 116
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 118
    .restart local v5    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .local v6, "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v1, p0

    .line 119
    invoke-virtual/range {v1 .. v6}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->setSSOWhiteList(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 120
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    if-eqz v13, :cond_5

    .line 122
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 123
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 128
    :goto_6
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 109
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_4
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_5

    .line 126
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_arg3":Ljava/lang/String;
    .restart local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_5
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 132
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_4
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 134
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_6

    .line 135
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 141
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_7
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 143
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 145
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 147
    .restart local v5    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v6

    .restart local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    move-object v1, p0

    .line 148
    invoke-virtual/range {v1 .. v6}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->deleteSSOWhiteList(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 149
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 150
    if-eqz v13, :cond_7

    .line 151
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 152
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 157
    :goto_8
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 138
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_6
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_7

    .line 155
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_arg3":Ljava/lang/String;
    .restart local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_7
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_8

    .line 161
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_arg4":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_5
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 163
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    .line 164
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 170
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_9
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 172
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 174
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 175
    .restart local v5    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->getAppAllowedState(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 176
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 177
    if-eqz v13, :cond_9

    .line 178
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 179
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 184
    :goto_a
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 167
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_8
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_9

    .line 182
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_arg3":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_9
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_a

    .line 188
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_6
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_a

    .line 191
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 197
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_b
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 199
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 201
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 203
    .restart local v5    # "_arg3":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v12

    .local v12, "_arg4":Ljava/lang/String;
    move-object v7, p0

    move-object v8, v2

    move v9, v3

    move-object v10, v4

    move-object v11, v5

    .line 204
    invoke-virtual/range {v7 .. v12}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->setCustomerInfo(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 205
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 206
    if-eqz v13, :cond_b

    .line 207
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 208
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 213
    :goto_c
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 194
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v12    # "_arg4":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_a
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_b

    .line 211
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v5    # "_arg3":Ljava/lang/String;
    .restart local v12    # "_arg4":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_b
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 217
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v12    # "_arg4":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_7
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 219
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_c

    .line 220
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 226
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_d
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 228
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 229
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->forceReauthenticate(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 230
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 231
    if-eqz v13, :cond_d

    .line 232
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 233
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 238
    :goto_e
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 223
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_c
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_d

    .line 236
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_d
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_e

    .line 242
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_8
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 244
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_e

    .line 245
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 251
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_f
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 253
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 254
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->unenroll(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 255
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 256
    if-eqz v13, :cond_f

    .line 257
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 258
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 263
    :goto_10
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 248
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_e
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_f

    .line 261
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_f
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_10

    .line 267
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_9
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 269
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_10

    .line 270
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 276
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_11
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 278
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 279
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->setupSSO(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 280
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 281
    if-eqz v13, :cond_11

    .line 282
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 283
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 288
    :goto_12
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 273
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_10
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_11

    .line 286
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_11
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_12

    .line 292
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_a
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 294
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_12

    .line 295
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 301
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_13
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 303
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 305
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_13

    .line 306
    sget-object v1, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/Bundle;

    .line 311
    .local v5, "_arg3":Landroid/os/Bundle;
    :goto_14
    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->pushSSOData(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;Landroid/os/Bundle;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 312
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 313
    if-eqz v13, :cond_14

    .line 314
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 315
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 320
    :goto_15
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 298
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Landroid/os/Bundle;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_12
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_13

    .line 309
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    :cond_13
    const/4 v5, 0x0

    .restart local v5    # "_arg3":Landroid/os/Bundle;
    goto :goto_14

    .line 318
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_14
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_15

    .line 324
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v5    # "_arg3":Landroid/os/Bundle;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_b
    const-string v1, "com.sec.enterprise.knox.IEnterpriseSSOPolicy"

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 326
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_15

    .line 327
    sget-object v1, Landroid/app/enterprise/ContextInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    move-object/from16 v0, p2

    invoke-interface {v1, v0}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/enterprise/ContextInfo;

    .line 333
    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    :goto_16
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 335
    .restart local v3    # "_arg1":I
    invoke-virtual/range {p2 .. p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 336
    .restart local v4    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v2, v3, v4}, Lcom/sec/enterprise/knox/IEnterpriseSSOPolicy$Stub;->isSSOReady(Landroid/app/enterprise/ContextInfo;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v13

    .line 337
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual/range {p3 .. p3}, Landroid/os/Parcel;->writeNoException()V

    .line 338
    if-eqz v13, :cond_16

    .line 339
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 340
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v13, v0, v1}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    .line 345
    :goto_17
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 330
    .end local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    .end local v3    # "_arg1":I
    .end local v4    # "_arg2":Ljava/lang/String;
    .end local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_15
    const/4 v2, 0x0

    .restart local v2    # "_arg0":Landroid/app/enterprise/ContextInfo;
    goto :goto_16

    .line 343
    .restart local v3    # "_arg1":I
    .restart local v4    # "_arg2":Ljava/lang/String;
    .restart local v13    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_16
    const/4 v1, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_17

    .line 42
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
