.class public Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;
.super Ljava/lang/Object;
.source "SEAndroidPolicy.java"


# static fields
.field public static final ACTION_SEANDROID_POLICY_CHANGED:Ljava/lang/String; = "mdm.intent.action.seandroid.policy.state.changed"

.field public static final ACTION_SEANDROID_POLICY_ENFORCED:Ljava/lang/String; = "mdm.intent.action.seandroid.policy.enforced"

.field public static final ALL:I = 0x2

.field public static final DENIAL_ONLY:I = 0x1

.field public static final EXTRA_NEW_STATE:Ljava/lang/String; = "mdm.intent.extra.seandroid.state"

.field public static final MDM_AGENT:I = 0x1

.field public static final NONE:I = 0x0

.field public static final POLICY_SET_FAILED:I = -0x1

.field public static final POLICY_SET_OK:I = 0x0

.field public static final POLICY_SET_REFUSED:I = -0x2

.field public static final SPOTA_AGENT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "SEAndroidPolicy"

.field private static mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

.field private static final mSync:Ljava/lang/Object;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;

.field private mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSync:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V
    .locals 1
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    .line 57
    iput-object p1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 58
    return-void
.end method

.method public static createInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;
    .locals 2
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 255
    new-instance v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    return-object v0
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;
    .locals 3
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 242
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v1, "SEAndroidPolicy.getInstance"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 243
    sget-object v1, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    .line 247
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    monitor-exit v1

    return-object v0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 228
    new-instance v1, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v2, "SEAndroidPolicy.getInstance"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 229
    sget-object v2, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSync:Ljava/lang/Object;

    monitor-enter v2

    .line 230
    :try_start_0
    sget-object v1, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    if-nez v1, :cond_0

    .line 231
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    .line 232
    .local v0, "cxtInfo":Landroid/app/enterprise/ContextInfo;
    new-instance v1, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;)V

    sput-object v1, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    .line 234
    .end local v0    # "cxtInfo":Landroid/app/enterprise/ContextInfo;
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidPolicy:Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;

    monitor-exit v2

    return-object v1

    .line 235
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method private getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    return-object v0
.end method


# virtual methods
.method public amsGetEnforce()Z
    .locals 4

    .prologue
    .line 636
    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v3, "SEAndroidPolicy.amsGetEnforce"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 637
    const/4 v1, 0x0

    .line 638
    .local v1, "isEnforced":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 640
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v3, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v2, v3}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->amsGetEnforce(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 645
    :cond_0
    :goto_0
    return v1

    .line 641
    :catch_0
    move-exception v0

    .line 642
    .local v0, "e":Landroid/os/RemoteException;
    const-string v2, "SEAndroidPolicy"

    const-string v3, "Failed to amc_GetEnforce"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public amsSetEnforce(Z)I
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 591
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.amsSetEnforce"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 592
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 594
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->amsSetEnforce(Landroid/app/enterprise/ContextInfo;Z)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 599
    :goto_0
    return v1

    .line 595
    :catch_0
    move-exception v0

    .line 596
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to amc_setEnforce"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public amsSetLogLevel(I)I
    .locals 3
    .param p1, "FLAG"    # I

    .prologue
    .line 685
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.amsSetLogLevel"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 686
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 688
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->amsSetLogLevel(Landroid/app/enterprise/ContextInfo;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 693
    :goto_0
    return v1

    .line 689
    :catch_0
    move-exception v0

    .line 690
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to amc_setLogLevel"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 693
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getDataType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "seInfo"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1054
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getDataType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1055
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1057
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getDataType(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1062
    :goto_0
    return-object v1

    .line 1058
    :catch_0
    move-exception v0

    .line 1059
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getDatatype"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDataTypeFromPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1006
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getDataType"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1007
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1009
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getDataTypeFromPackageName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1014
    :goto_0
    return-object v1

    .line 1010
    :catch_0
    move-exception v0

    .line 1011
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getDatatype"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDomain(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "seInfo"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .prologue
    .line 961
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getDomain"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 962
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 964
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getDomain(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 969
    :goto_0
    return-object v1

    .line 965
    :catch_0
    move-exception v0

    .line 966
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getDomain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 969
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDomainFromPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 914
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getDomainFromPackageName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 915
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 917
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getDomainFromPackageName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 922
    :goto_0
    return-object v1

    .line 918
    :catch_0
    move-exception v0

    .line 919
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getDomain"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 922
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEAndroidAgent()I
    .locals 3

    .prologue
    .line 544
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getSEAndroidAgent"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 545
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 547
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getSEAndroidAgent(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 552
    :goto_0
    return v1

    .line 548
    :catch_0
    move-exception v0

    .line 549
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getSEAndroidAgent"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEInfoFromCertificate(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "certificate"    # Ljava/lang/String;

    .prologue
    .line 867
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getSEInfoFromCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 868
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 870
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getSEInfoFromCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 875
    :goto_0
    return-object v1

    .line 871
    :catch_0
    move-exception v0

    .line 872
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getSeinfoFromCertificate"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSEInfoFromPackageName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 822
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getSEInfoFromPackageName"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 823
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 825
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getSEInfoFromPackageName(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 830
    :goto_0
    return-object v1

    .line 826
    :catch_0
    move-exception v0

    .line 827
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getSeinfo"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getSELinuxMode()Z
    .locals 3

    .prologue
    .line 776
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.getSELinuxMode"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 777
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 779
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->getSELinuxMode(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 784
    :goto_0
    return v1

    .line 780
    :catch_0
    move-exception v0

    .line 781
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to getSELinuxMode"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public revokeSELinuxPolicy()I
    .locals 3

    .prologue
    .line 492
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.revokeSELinuxPolicy"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 493
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 495
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->revokeSELinuxPolicy(Landroid/app/enterprise/ContextInfo;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 500
    :goto_0
    return v1

    .line 496
    :catch_0
    move-exception v0

    .line 497
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to revokeSELinuxPolicy"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setFileContexts([B)I
    .locals 3
    .param p1, "fileContexts"    # [B

    .prologue
    .line 345
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setFileContexts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 346
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setFileContexts(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 353
    :goto_0
    return v1

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setFileContexts"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setMacPermission([B)I
    .locals 3
    .param p1, "macPerm"    # [B

    .prologue
    .line 1100
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setMacPermission"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1101
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1103
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setMacPermission(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1108
    :goto_0
    return v1

    .line 1104
    :catch_0
    move-exception v0

    .line 1105
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setMacPerm"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1108
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setPropertyContexts([B)I
    .locals 3
    .param p1, "propertyContexts"    # [B

    .prologue
    .line 393
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setPropertyContexts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 394
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 396
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setPropertyContexts(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 401
    :goto_0
    return v1

    .line 397
    :catch_0
    move-exception v0

    .line 398
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setPropertyContexts"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSEAppContexts([B)I
    .locals 3
    .param p1, "seAppContexts"    # [B

    .prologue
    .line 439
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setSEAppContexts"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 440
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 442
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setSEAppContexts(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 447
    :goto_0
    return v1

    .line 443
    :catch_0
    move-exception v0

    .line 444
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setSEAppContexts"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 447
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public setSELinuxEnforcing()Z
    .locals 3

    .prologue
    .line 729
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setSELinuxEnforcing"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 730
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 732
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setSELinuxEnforcing(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 737
    :goto_0
    return v1

    .line 733
    :catch_0
    move-exception v0

    .line 734
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setEnforce"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setSELinuxPolicy([B)I
    .locals 3
    .param p1, "sePolicy"    # [B

    .prologue
    .line 297
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "SEAndroidPolicy.setSELinuxPolicy"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->getService()Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 300
    :try_start_0
    iget-object v1, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mSEAndroidService:Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;

    iget-object v2, p0, Lcom/sec/enterprise/knox/seandroid/SEAndroidPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/seandroid/ISEAndroidPolicy;->setSELinuxPolicy(Landroid/app/enterprise/ContextInfo;[B)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 305
    :goto_0
    return v1

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "SEAndroidPolicy"

    const-string v2, "Failed to setSELinuxPolicy"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method
