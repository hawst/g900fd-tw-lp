.class public abstract Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterprisePremiumVpnPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

.field static final TRANSACTION_addVpnProfileToApp:I = 0x11

.field static final TRANSACTION_bindMocanaVpnInterface:I = 0x1a

.field static final TRANSACTION_containerPackageListFromVpnDatabase:I = 0x18

.field static final TRANSACTION_enableDefaultRoute:I = 0xf

.field static final TRANSACTION_getAllEnterpriseVpnConnections:I = 0x4

.field static final TRANSACTION_getAllPackagesForProfile:I = 0x13

.field static final TRANSACTION_getCACertificate:I = 0x8

.field static final TRANSACTION_getDomainsByProfileName:I = 0x1c

.field static final TRANSACTION_getEnterpriseVpnConnection:I = 0x1

.field static final TRANSACTION_getErrorString:I = 0xc

.field static final TRANSACTION_getForwardRoutes:I = 0xe

.field static final TRANSACTION_getProfilesByDomain:I = 0x1d

.field static final TRANSACTION_getState:I = 0xb

.field static final TRANSACTION_getUserCertificate:I = 0x7

.field static final TRANSACTION_getVpnModeOfOperation:I = 0x15

.field static final TRANSACTION_isDefaultRouteEnabled:I = 0x10

.field static final TRANSACTION_notifyContainerAppLaunch:I = 0x16

.field static final TRANSACTION_removeEnterpriseVpnConnection:I = 0x3

.field static final TRANSACTION_removeVpnForApplication:I = 0x12

.field static final TRANSACTION_setCACertificate:I = 0x6

.field static final TRANSACTION_setDnsToVpn:I = 0x17

.field static final TRANSACTION_setEnterprisePremiumVpnConnection:I = 0x2

.field static final TRANSACTION_setForwardRoutes:I = 0xd

.field static final TRANSACTION_setUserCertificate:I = 0x5

.field static final TRANSACTION_setVpnFrameworkSystemProperty:I = 0x19

.field static final TRANSACTION_setVpnModeOfOperation:I = 0x14

.field static final TRANSACTION_startConnection:I = 0x9

.field static final TRANSACTION_startVpnAfterMigration:I = 0x1b

.field static final TRANSACTION_stopConnection:I = 0xa


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 520
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v8

    :goto_0
    return v8

    .line 45
    :sswitch_0
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 54
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 56
    .local v6, "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 57
    if-eqz v6, :cond_0

    .line 58
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 68
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_2
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_1

    .line 71
    sget-object v10, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v10, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;

    .line 77
    .local v0, "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 78
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setEnterprisePremiumVpnConnection(Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 79
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 80
    if-eqz v6, :cond_2

    .line 81
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 82
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 74
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    goto :goto_1

    .line 85
    .restart local v1    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 91
    .end local v0    # "_arg0":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_3
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 95
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 97
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v3

    .line 98
    .local v3, "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->removeEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 99
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 100
    if-eqz v6, :cond_3

    .line 101
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 111
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_4
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 113
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 114
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getAllEnterpriseVpnConnections(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 115
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 116
    if-eqz v6, :cond_4

    .line 117
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 118
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 121
    :cond_4
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 127
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_5
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 131
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 133
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 135
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 136
    .local v5, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3, v5}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setUserCertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 137
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 138
    if-eqz v6, :cond_5

    .line 139
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 140
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 143
    :cond_5
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 149
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_6
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 153
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 155
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 157
    .restart local v3    # "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 158
    .restart local v5    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3, v5}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setCACertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 159
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 160
    if-eqz v6, :cond_6

    .line 161
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 162
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 165
    :cond_6
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 171
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v5    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_7
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 175
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 176
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getUserCertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 177
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 178
    if-eqz v6, :cond_7

    .line 179
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 180
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 183
    :cond_7
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 189
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_8
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 193
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 194
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getCACertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 195
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 196
    if-eqz v6, :cond_8

    .line 197
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 198
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 201
    :cond_8
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 207
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_9
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 209
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 211
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 213
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v3

    .line 214
    .local v3, "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->startConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 215
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 216
    if-eqz v6, :cond_9

    .line 217
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 218
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 221
    :cond_9
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 227
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_a
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 229
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 231
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 233
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v10

    invoke-static {v10}, Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;

    move-result-object v3

    .line 234
    .restart local v3    # "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->stopConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 235
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 236
    if-eqz v6, :cond_a

    .line 237
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 238
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 241
    :cond_a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 247
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_b
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 251
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 252
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getState(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 253
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 254
    if-eqz v6, :cond_b

    .line 255
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 256
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 259
    :cond_b
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 265
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_c
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 269
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 270
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getErrorString(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 271
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 272
    if-eqz v6, :cond_c

    .line 273
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 274
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 277
    :cond_c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 283
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_d
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 285
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 287
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 289
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 290
    .local v4, "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v1, v4}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setForwardRoutes(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 291
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 292
    if-eqz v6, :cond_d

    .line 293
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 294
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 297
    :cond_d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 303
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v4    # "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_e
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 307
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 308
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getForwardRoutes(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 309
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 310
    if-eqz v6, :cond_e

    .line 311
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 312
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 315
    :cond_e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 321
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_f
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 323
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 325
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 327
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_f

    move v3, v8

    .line 328
    .local v3, "_arg2":Z
    :goto_2
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->enableDefaultRoute(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 329
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 330
    if-eqz v6, :cond_10

    .line 331
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 332
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_f
    move v3, v9

    .line 327
    goto :goto_2

    .line 335
    .restart local v3    # "_arg2":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_10
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 341
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_10
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 343
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 345
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 346
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->isDefaultRouteEnabled(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 347
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 348
    if-eqz v6, :cond_11

    .line 349
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 350
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 353
    :cond_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 359
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_11
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 361
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 363
    .local v0, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 365
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 366
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->addVpnProfileToApp(ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 367
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 368
    if-eqz v6, :cond_12

    .line 369
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 370
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 373
    :cond_12
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 379
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_12
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 381
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 383
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 384
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->removeVpnForApplication(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 385
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 386
    if-eqz v6, :cond_13

    .line 387
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 388
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 391
    :cond_13
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 397
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_13
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 399
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 401
    .restart local v0    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 402
    .restart local v1    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getAllPackagesForProfile(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 403
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 404
    if-eqz v6, :cond_14

    .line 405
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 406
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 409
    :cond_14
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 415
    .end local v0    # "_arg0":I
    .end local v1    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_14
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 417
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v10

    if-eqz v10, :cond_15

    move v0, v8

    .line 418
    .local v0, "_arg0":Z
    :goto_3
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setVpnModeOfOperation(Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 419
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 420
    if-eqz v6, :cond_16

    .line 421
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 422
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v0    # "_arg0":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_15
    move v0, v9

    .line 417
    goto :goto_3

    .line 425
    .restart local v0    # "_arg0":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_16
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 431
    .end local v0    # "_arg0":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_15
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 432
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getVpnModeOfOperation()Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 433
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 434
    if-eqz v6, :cond_17

    .line 435
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 436
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 439
    :cond_17
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 445
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_16
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 447
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 448
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->notifyContainerAppLaunch(I)V

    .line 449
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 454
    .end local v0    # "_arg0":I
    :sswitch_17
    const-string v10, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v10}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 456
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 458
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v2

    .line 460
    .local v2, "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 461
    .restart local v4    # "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p0, v0, v2, v4}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setDnsToVpn(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Z

    move-result v6

    .line 462
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 463
    if-eqz v6, :cond_18

    move v9, v8

    :cond_18
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 468
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v4    # "_arg2":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v6    # "_result":Z
    :sswitch_18
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 470
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 471
    .local v0, "_arg0":I
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->containerPackageListFromVpnDatabase(I)[Ljava/lang/String;

    move-result-object v6

    .line 472
    .local v6, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 473
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 478
    .end local v0    # "_arg0":I
    .end local v6    # "_result":[Ljava/lang/String;
    :sswitch_19
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 480
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 481
    .local v0, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V

    .line 482
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 487
    .end local v0    # "_arg0":Ljava/lang/String;
    :sswitch_1a
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 488
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->bindMocanaVpnInterface()V

    .line 489
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 494
    :sswitch_1b
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 495
    invoke-virtual {p0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->startVpnAfterMigration()V

    .line 496
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 501
    :sswitch_1c
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 503
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 504
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getDomainsByProfileName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 505
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 506
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 511
    .end local v0    # "_arg0":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_1d
    const-string v9, "com.sec.enterprise.knox.vpn.IEnterprisePremiumVpnPolicy"

    invoke-virtual {p2, v9}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 513
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 514
    .restart local v0    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->getProfilesByDomain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 515
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 516
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
