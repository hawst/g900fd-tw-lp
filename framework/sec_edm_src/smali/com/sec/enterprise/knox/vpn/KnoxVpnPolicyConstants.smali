.class public Lcom/sec/enterprise/knox/vpn/KnoxVpnPolicyConstants;
.super Ljava/lang/Object;
.source "KnoxVpnPolicyConstants.java"


# static fields
.field public static final ACTION_MDM_VPN_ADD_SOLUTION:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_ADD_SOLUTION"

.field public static final ACTION_MDM_VPN_ADMIN_ADDED:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_ADMIN_ADDED"

.field public static final ACTION_MDM_VPN_ADMIN_REMOVED:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_ADMIN_REMOVED"

.field public static final ACTION_MDM_VPN_CONTAINER_CREATED:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_CONTAINER_CREATED"

.field public static final ACTION_MDM_VPN_CONTAINER_REMOVED:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_CONTAINER_REMOVED"

.field public static final ACTION_MDM_VPN_INTERFACE_STATUS:Ljava/lang/String; = "com.sec.enterprise.mdm.services.vpn.INTERFACE_STATUS"

.field public static final ACTION_MDM_VPN_PRE_ADMIN_REMOVAL:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_PRE_ADMIN_REMOVAL"

.field public static final ACTION_MDM_VPN_REMOVE_SOLUTION:Ljava/lang/String; = "com.sec.enterprise.mdm.MDM_VPN_REMOVE_SOLUTION"

.field public static final ACTION_MDM_VPN_RETRY:Ljava/lang/String; = "com.sec.enterprise.mdm.services.vpn.RETRY"

.field public static final EXTRA_ACTION:Ljava/lang/String; = "extra_action"

.field public static final EXTRA_CONTAINER_ID:Ljava/lang/String; = "extra_container_id"

.field public static final EXTRA_NET_ID:Ljava/lang/String; = "extra_net_id"

.field public static final EXTRA_PROFILE_NAME:Ljava/lang/String; = "extra_profile_name"

.field public static final EXTRA_START_ACTION:Ljava/lang/String; = "extra_start_action"

.field public static final EXTRA_STATE:Ljava/lang/String; = "extra_state"

.field public static final EXTRA_TUN_ID:Ljava/lang/String; = "extra_tun_id"

.field public static final EXTRA_UID:Ljava/lang/String; = "extra_uid"

.field public static final EXTRA_VPN_TYPE:Ljava/lang/String; = "extra_vpn_type"

.field public static final INTENT_CONTAINER_ADMIN_CHANGED_ACTION:Ljava/lang/String; = "enterprise.container.admin.changed"

.field public static final NEW_FW:Ljava/lang/String; = "2.0"

.field public static final OLD_FW:Ljava/lang/String; = "1.0"

.field protected static TAG:Ljava/lang/String; = null

.field public static final VPN_CERT_TYPE_AUTOMATIC:Ljava/lang/String; = "Automatic"

.field public static final VPN_CERT_TYPE_DISABLED:Ljava/lang/String; = "Disabled"

.field public static final VPN_CERT_TYPE_MANUAL:Ljava/lang/String; = "Manual"

.field public static final VPN_FW_PROP:Ljava/lang/String; = "net.vpn.framework"

.field public static final VPN_TYPE_ANYCONNECT:Ljava/lang/String; = "anyconnect"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    const-string v0, "KnoxVpnPolicyConstants"

    sput-object v0, Lcom/sec/enterprise/knox/vpn/KnoxVpnPolicyConstants;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
