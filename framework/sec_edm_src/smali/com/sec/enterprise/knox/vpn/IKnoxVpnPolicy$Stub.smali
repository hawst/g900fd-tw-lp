.class public abstract Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;
.super Landroid/os/Binder;
.source "IKnoxVpnPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

.field static final TRANSACTION_activateVpnProfile:I = 0x5

.field static final TRANSACTION_addAllContainerPackagesToVpn:I = 0x18

.field static final TRANSACTION_addAllPackagesToVpn:I = 0x13

.field static final TRANSACTION_addContainerPackagesToVpn:I = 0x15

.field static final TRANSACTION_addPackagesToVpn:I = 0x10

.field static final TRANSACTION_bindKnoxVpnInterface:I = 0x1c

.field static final TRANSACTION_checkIfVendorCreatedKnoxProfile:I = 0x25

.field static final TRANSACTION_createVpnProfile:I = 0x1

.field static final TRANSACTION_getActiveProfilesForVendor:I = 0x22

.field static final TRANSACTION_getAllContainerPackagesInVpnProfile:I = 0x17

.field static final TRANSACTION_getAllPackagesInVpnProfile:I = 0x12

.field static final TRANSACTION_getAllVpnProfiles:I = 0x4

.field static final TRANSACTION_getCACertificate:I = 0x9

.field static final TRANSACTION_getChainingEnabledForProfile:I = 0x1f

.field static final TRANSACTION_getDomainsByProfileName:I = 0x23

.field static final TRANSACTION_getErrorString:I = 0xd

.field static final TRANSACTION_getKnoxVpnProfileType:I = 0x21

.field static final TRANSACTION_getProfilesByDomain:I = 0x24

.field static final TRANSACTION_getState:I = 0xc

.field static final TRANSACTION_getUidPidSearchEnabledForProfile:I = 0x1e

.field static final TRANSACTION_getUserCertificate:I = 0x7

.field static final TRANSACTION_getVendorNameForProfile:I = 0x26

.field static final TRANSACTION_getVpnConnectionType:I = 0x20

.field static final TRANSACTION_getVpnModeOfOperation:I = 0xf

.field static final TRANSACTION_getVpnProfile:I = 0x2

.field static final TRANSACTION_removeAllContainerPackagesFromVpn:I = 0x19

.field static final TRANSACTION_removeAllPackagesFromVpn:I = 0x14

.field static final TRANSACTION_removeContainerPackagesFromVpn:I = 0x16

.field static final TRANSACTION_removePackagesFromVpn:I = 0x11

.field static final TRANSACTION_removeVpnProfile:I = 0x3

.field static final TRANSACTION_setAutoRetryOnConnectionError:I = 0x1b

.field static final TRANSACTION_setCACertificate:I = 0x8

.field static final TRANSACTION_setServerCertValidationUserAcceptanceCriteria:I = 0x1a

.field static final TRANSACTION_setUserCertificate:I = 0x6

.field static final TRANSACTION_setVpnFrameworkSystemProperty:I = 0x1d

.field static final TRANSACTION_setVpnModeOfOperation:I = 0xe

.field static final TRANSACTION_startConnection:I = 0xa

.field static final TRANSACTION_stopConnection:I = 0xb


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 820
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 59
    .local v1, "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->createVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 61
    .local v6, "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 62
    if-eqz v6, :cond_1

    .line 63
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 56
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_1

    .line 67
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 73
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 76
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 82
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 83
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 84
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 85
    if-eqz v6, :cond_3

    .line 86
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 79
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_2

    .line 90
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_3
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 96
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 99
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 105
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 106
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->removeVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 107
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 108
    if-eqz v6, :cond_5

    .line 109
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 110
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 102
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_4
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_3

    .line 113
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_5
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 119
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 122
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 127
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_4
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getAllVpnProfiles(Lcom/sec/enterprise/knox/KnoxVpnContext;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 128
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 129
    if-eqz v6, :cond_7

    .line 130
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 125
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_6
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_4

    .line 134
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_7
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 140
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 143
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 149
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 151
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    move v3, v9

    .line 152
    .local v3, "_arg2":Z
    :goto_6
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->activateVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 153
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 154
    if-eqz v6, :cond_a

    .line 155
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 156
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 146
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_8
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_5

    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_9
    move v3, v10

    .line 151
    goto :goto_6

    .line 159
    .restart local v3    # "_arg2":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_a
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 165
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 168
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 174
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 176
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 178
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 179
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setUserCertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 180
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 181
    if-eqz v6, :cond_c

    .line 182
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 183
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 171
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_7

    .line 186
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":[B
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_c
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 192
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 194
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 195
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 201
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 202
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getUserCertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 203
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 204
    if-eqz v6, :cond_e

    .line 205
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 198
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_8

    .line 209
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_e
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 215
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_8
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 218
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 224
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 226
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 227
    .restart local v3    # "_arg2":[B
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setCACertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;[B)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 228
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 229
    if-eqz v6, :cond_10

    .line 230
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 231
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 221
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_9

    .line 234
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":[B
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_10
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 240
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_9
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 242
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 243
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 249
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 250
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getCACertificate(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 251
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 252
    if-eqz v6, :cond_12

    .line 253
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 254
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 246
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_11
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_a

    .line 257
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_12
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 263
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_a
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 265
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 266
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 272
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 273
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->startConnection(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 274
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 275
    if-eqz v6, :cond_14

    .line 276
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 277
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 269
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_13
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_b

    .line 280
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_14
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 286
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_b
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 288
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    .line 289
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 295
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 296
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->stopConnection(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 297
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 298
    if-eqz v6, :cond_16

    .line 299
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 300
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 292
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_15
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_c

    .line 303
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_16
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 309
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_c
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 311
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 312
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 318
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 319
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getState(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 320
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 321
    if-eqz v6, :cond_18

    .line 322
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 323
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 315
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_17
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_d

    .line 326
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_18
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 332
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_d
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 335
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 341
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 342
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getErrorString(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 343
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 344
    if-eqz v6, :cond_1a

    .line 345
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 338
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_19
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_e

    .line 349
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1a
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 355
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_e
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1b

    .line 358
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 364
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 366
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 367
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setVpnModeOfOperation(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 368
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 369
    if-eqz v6, :cond_1c

    .line 370
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 371
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 361
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_f

    .line 374
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v3    # "_arg2":I
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1c
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 380
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_f
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 382
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 383
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 389
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 390
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getVpnModeOfOperation(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 391
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 392
    if-eqz v6, :cond_1e

    .line 393
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 394
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 386
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_10

    .line 397
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1e
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 403
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_10
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 405
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 406
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 412
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 414
    .local v2, "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 415
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->addPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 416
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 417
    if-eqz v6, :cond_20

    .line 418
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 419
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 409
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_1f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_11

    .line 422
    .restart local v2    # "_arg1":[Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_20
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 428
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_11
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 430
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    .line 431
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 437
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v2

    .line 439
    .restart local v2    # "_arg1":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 440
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->removePackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 441
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 442
    if-eqz v6, :cond_22

    .line 443
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 444
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 434
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_21
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_12

    .line 447
    .restart local v2    # "_arg1":[Ljava/lang/String;
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_22
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 453
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":[Ljava/lang/String;
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_12
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_23

    .line 456
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 462
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 463
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getAllPackagesInVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 464
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 465
    if-eqz v6, :cond_24

    .line 466
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 467
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 459
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_23
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_13

    .line 470
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_24
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 476
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_13
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 478
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_25

    .line 479
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 485
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 486
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->addAllPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 487
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 488
    if-eqz v6, :cond_26

    .line 489
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 490
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 482
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_25
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_14

    .line 493
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_26
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 499
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_14
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 501
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_27

    .line 502
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 508
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 509
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->removeAllPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 510
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 511
    if-eqz v6, :cond_28

    .line 512
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 513
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 505
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_27
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_15

    .line 516
    .restart local v2    # "_arg1":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_28
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 522
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_15
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 524
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_29

    .line 525
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 531
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 533
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    .line 535
    .local v3, "_arg2":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 536
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->addContainerPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 537
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 538
    if-eqz v6, :cond_2a

    .line 539
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 540
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 528
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_29
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_16

    .line 543
    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":[Ljava/lang/String;
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2a
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 549
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_16
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 551
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2b

    .line 552
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 558
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_17
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 560
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    .line 562
    .restart local v3    # "_arg2":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 563
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->removeContainerPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 564
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 565
    if-eqz v6, :cond_2c

    .line 566
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 567
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 555
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2b
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_17

    .line 570
    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":[Ljava/lang/String;
    .restart local v4    # "_arg3":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2c
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 576
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_17
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 578
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2d

    .line 579
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 585
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_18
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 587
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 588
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getAllContainerPackagesInVpnProfile(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 589
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 590
    if-eqz v6, :cond_2e

    .line 591
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 592
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 582
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2d
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_18

    .line 595
    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2e
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 601
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_18
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 603
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2f

    .line 604
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 610
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 612
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 613
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->addAllContainerPackagesToVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 614
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 615
    if-eqz v6, :cond_30

    .line 616
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 617
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 607
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_2f
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_19

    .line 620
    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_30
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 626
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_19
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 628
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_31

    .line 629
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 635
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_1a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 637
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 638
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->removeAllContainerPackagesFromVpn(Lcom/sec/enterprise/knox/KnoxVpnContext;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 639
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 640
    if-eqz v6, :cond_32

    .line 641
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 642
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 632
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_31
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_1a

    .line 645
    .restart local v2    # "_arg1":I
    .restart local v3    # "_arg2":Ljava/lang/String;
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_32
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 651
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_1a
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 653
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_33

    .line 654
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 660
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_1b
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 662
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_34

    move v3, v9

    .line 664
    .local v3, "_arg2":Z
    :goto_1c
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    .line 665
    .local v8, "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v8}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v4

    .line 667
    .local v4, "_arg3":Ljava/util/List;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 668
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setServerCertValidationUserAcceptanceCriteria(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;ZLjava/util/List;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 669
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 670
    if-eqz v6, :cond_35

    .line 671
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 672
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 657
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v4    # "_arg3":Ljava/util/List;
    .end local v5    # "_arg4":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .end local v8    # "cl":Ljava/lang/ClassLoader;
    :cond_33
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_1b

    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_34
    move v3, v10

    .line 662
    goto :goto_1c

    .line 675
    .restart local v3    # "_arg2":Z
    .restart local v4    # "_arg3":Ljava/util/List;
    .restart local v5    # "_arg4":I
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .restart local v8    # "cl":Ljava/lang/ClassLoader;
    :cond_35
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 681
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v4    # "_arg3":Ljava/util/List;
    .end local v5    # "_arg4":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .end local v8    # "cl":Ljava/lang/ClassLoader;
    :sswitch_1b
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 683
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_36

    .line 684
    sget-object v0, Lcom/sec/enterprise/knox/KnoxVpnContext;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/enterprise/knox/KnoxVpnContext;

    .line 690
    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    :goto_1d
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 692
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_37

    move v3, v9

    .line 693
    .restart local v3    # "_arg2":Z
    :goto_1e
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setAutoRetryOnConnectionError(Lcom/sec/enterprise/knox/KnoxVpnContext;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 694
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 695
    if-eqz v6, :cond_38

    .line 696
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    .line 697
    invoke-virtual {v6, p3, v9}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 687
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_36
    const/4 v1, 0x0

    .restart local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    goto :goto_1d

    .restart local v2    # "_arg1":Ljava/lang/String;
    :cond_37
    move v3, v10

    .line 692
    goto :goto_1e

    .line 700
    .restart local v3    # "_arg2":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_38
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 706
    .end local v1    # "_arg0":Lcom/sec/enterprise/knox/KnoxVpnContext;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_1c
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 708
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 709
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v6

    .line 710
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 711
    if-eqz v6, :cond_39

    move v10, v9

    :cond_39
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 716
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_1d
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 718
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 719
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V

    .line 720
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 725
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_1e
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 727
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 729
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 730
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getUidPidSearchEnabledForProfile(ILjava/lang/String;)I

    move-result v6

    .line 731
    .local v6, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 732
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 737
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":I
    :sswitch_1f
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 739
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 740
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getChainingEnabledForProfile(I)I

    move-result v6

    .line 741
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 742
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 747
    .end local v1    # "_arg0":I
    .end local v6    # "_result":I
    :sswitch_20
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 749
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 750
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getVpnConnectionType(Ljava/lang/String;)I

    move-result v6

    .line 751
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 752
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 757
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":I
    :sswitch_21
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 759
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 760
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getKnoxVpnProfileType(Ljava/lang/String;)I

    move-result v6

    .line 761
    .restart local v6    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 762
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 767
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":I
    :sswitch_22
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 769
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 770
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getActiveProfilesForVendor(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 771
    .local v7, "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 772
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 777
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_23
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 779
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 780
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getDomainsByProfileName(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 781
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 782
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 787
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_24
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 789
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 790
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getProfilesByDomain(Ljava/lang/String;)Ljava/util/List;

    move-result-object v7

    .line 791
    .restart local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 792
    invoke-virtual {p3, v7}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    goto/16 :goto_0

    .line 797
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v7    # "_result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :sswitch_25
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 801
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 803
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 804
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->checkIfVendorCreatedKnoxProfile(Ljava/lang/String;II)Z

    move-result v6

    .line 805
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 806
    if-eqz v6, :cond_3a

    move v10, v9

    :cond_3a
    invoke-virtual {p3, v10}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 811
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Z
    :sswitch_26
    const-string v0, "com.sec.enterprise.knox.vpn.IKnoxVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 813
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 814
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IKnoxVpnPolicy$Stub;->getVendorNameForProfile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 815
    .local v6, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 816
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
