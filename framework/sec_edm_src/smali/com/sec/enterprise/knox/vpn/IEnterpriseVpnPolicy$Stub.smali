.class public abstract Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;
.super Landroid/os/Binder;
.source "IEnterpriseVpnPolicy.java"

# interfaces
.implements Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

.field static final TRANSACTION_activateVpnProfile:I = 0x5

.field static final TRANSACTION_addAllContainerPackagesToVpn:I = 0x13

.field static final TRANSACTION_addPackagesToVpn:I = 0x10

.field static final TRANSACTION_bindKnoxVpnInterface:I = 0x19

.field static final TRANSACTION_containerPackageListFromVpnDatabase:I = 0x18

.field static final TRANSACTION_createVpnProfile:I = 0x1

.field static final TRANSACTION_getAllPackagesInVpnProfile:I = 0x12

.field static final TRANSACTION_getAllVpnProfiles:I = 0x4

.field static final TRANSACTION_getCACertificate:I = 0x9

.field static final TRANSACTION_getErrorString:I = 0xd

.field static final TRANSACTION_getState:I = 0xc

.field static final TRANSACTION_getUserCertificate:I = 0x7

.field static final TRANSACTION_getVpnModeOfOperation:I = 0xf

.field static final TRANSACTION_getVpnProfile:I = 0x2

.field static final TRANSACTION_notifyContainerAppLaunch:I = 0x17

.field static final TRANSACTION_removeAllContainerPackagesFromVpn:I = 0x14

.field static final TRANSACTION_removePackagesFromVpn:I = 0x11

.field static final TRANSACTION_removeVpnProfile:I = 0x3

.field static final TRANSACTION_setAutoRetryOnConnectionError:I = 0x16

.field static final TRANSACTION_setCACertificate:I = 0x8

.field static final TRANSACTION_setServerCertValidationUserAcceptanceCriteria:I = 0x15

.field static final TRANSACTION_setUserCertificate:I = 0x6

.field static final TRANSACTION_setVpnFrameworkSystemProperty:I = 0x1a

.field static final TRANSACTION_setVpnModeOfOperation:I = 0xe

.field static final TRANSACTION_startConnection:I = 0xa

.field static final TRANSACTION_stopConnection:I = 0xb


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p0, p0, v0}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v1, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 514
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v8

    :goto_0
    return v8

    .line 45
    :sswitch_0
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->createVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 56
    .local v6, "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 57
    if-eqz v6, :cond_0

    .line 58
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 68
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_2
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 72
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 73
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 74
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 75
    if-eqz v6, :cond_1

    .line 76
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 77
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 80
    :cond_1
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 86
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_3
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 88
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 90
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 91
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->removeVpnProfile(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 92
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 93
    if-eqz v6, :cond_2

    .line 94
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 95
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 104
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_4
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 107
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getAllVpnProfiles(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 108
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 109
    if-eqz v6, :cond_3

    .line 110
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 111
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 114
    :cond_3
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 120
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_5
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 124
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 126
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v3, v8

    .line 127
    .local v3, "_arg2":Z
    :goto_1
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->activateVpnProfile(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 128
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 129
    if-eqz v6, :cond_5

    .line 130
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 131
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_4
    move v3, v9

    .line 126
    goto :goto_1

    .line 134
    .restart local v3    # "_arg2":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_5
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 140
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_6
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 144
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 146
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 148
    .local v3, "_arg2":[B
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 149
    .local v4, "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setUserCertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 150
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 151
    if-eqz v6, :cond_6

    .line 152
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 153
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 156
    :cond_6
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 162
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_7
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 166
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 167
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getUserCertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 168
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 169
    if-eqz v6, :cond_7

    .line 170
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 171
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 174
    :cond_7
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 180
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_8
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 182
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 184
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 186
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    .line 187
    .restart local v3    # "_arg2":[B
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setCACertificate(Ljava/lang/String;Ljava/lang/String;[B)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 188
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 189
    if-eqz v6, :cond_8

    .line 190
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 191
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 194
    :cond_8
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 200
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[B
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_9
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 204
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 205
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getCACertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 206
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 207
    if-eqz v6, :cond_9

    .line 208
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 209
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 212
    :cond_9
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 218
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_a
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 220
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 223
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->startConnection(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 224
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 225
    if-eqz v6, :cond_a

    .line 226
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 227
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 230
    :cond_a
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 236
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_b
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 238
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 240
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 241
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->stopConnection(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 242
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 243
    if-eqz v6, :cond_b

    .line 244
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 245
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 248
    :cond_b
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 254
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_c
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 256
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 258
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 259
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getState(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 260
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 261
    if-eqz v6, :cond_c

    .line 262
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 263
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 266
    :cond_c
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 272
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_d
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 276
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 277
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getErrorString(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 278
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 279
    if-eqz v6, :cond_d

    .line 280
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 281
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 284
    :cond_d
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 290
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_e
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 292
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 294
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 296
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 297
    .local v3, "_arg2":I
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setVpnModeOfOperation(Ljava/lang/String;Ljava/lang/String;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 298
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 299
    if-eqz v6, :cond_e

    .line 300
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 301
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 304
    :cond_e
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 310
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_f
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 312
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 314
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 315
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getVpnModeOfOperation(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 316
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 317
    if-eqz v6, :cond_f

    .line 318
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 319
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 322
    :cond_f
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 328
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_10
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 332
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 334
    .local v2, "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "_arg2":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 337
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->addPackagesToVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 338
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 339
    if-eqz v6, :cond_10

    .line 340
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 341
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 344
    :cond_10
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 350
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_11
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 352
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 354
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 356
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v3

    .line 358
    .restart local v3    # "_arg2":[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 359
    .restart local v4    # "_arg3":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->removePackagesFromVpn(Ljava/lang/String;I[Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 360
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 361
    if-eqz v6, :cond_11

    .line 362
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 363
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 366
    :cond_11
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 372
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":[Ljava/lang/String;
    .end local v4    # "_arg3":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_12
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 374
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 376
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 378
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 379
    .local v3, "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->getAllPackagesInVpnProfile(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 380
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 381
    if-eqz v6, :cond_12

    .line 382
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 383
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 386
    :cond_12
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 392
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_13
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 394
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 396
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 398
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 399
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->addAllContainerPackagesToVpn(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 400
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 401
    if-eqz v6, :cond_13

    .line 402
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 403
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 406
    :cond_13
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 412
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_14
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 414
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 416
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 418
    .restart local v2    # "_arg1":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 419
    .restart local v3    # "_arg2":Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->removeAllContainerPackagesFromVpn(Ljava/lang/String;ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 420
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 421
    if-eqz v6, :cond_14

    .line 422
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 423
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .line 426
    :cond_14
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 432
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    .end local v3    # "_arg2":Ljava/lang/String;
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_15
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 434
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 436
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 438
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    move v3, v8

    .line 440
    .local v3, "_arg2":Z
    :goto_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    .line 441
    .local v7, "cl":Ljava/lang/ClassLoader;
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v4

    .line 443
    .local v4, "_arg3":Ljava/util/List;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .local v5, "_arg4":I
    move-object v0, p0

    .line 444
    invoke-virtual/range {v0 .. v5}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setServerCertValidationUserAcceptanceCriteria(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;I)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 445
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 446
    if-eqz v6, :cond_16

    .line 447
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 448
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v3    # "_arg2":Z
    .end local v4    # "_arg3":Ljava/util/List;
    .end local v5    # "_arg4":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .end local v7    # "cl":Ljava/lang/ClassLoader;
    :cond_15
    move v3, v9

    .line 438
    goto :goto_2

    .line 451
    .restart local v3    # "_arg2":Z
    .restart local v4    # "_arg3":Ljava/util/List;
    .restart local v5    # "_arg4":I
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .restart local v7    # "cl":Ljava/lang/ClassLoader;
    :cond_16
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 457
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v4    # "_arg3":Ljava/util/List;
    .end local v5    # "_arg4":I
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    .end local v7    # "cl":Ljava/lang/ClassLoader;
    :sswitch_16
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 459
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 461
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 463
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    move v3, v8

    .line 464
    .restart local v3    # "_arg2":Z
    :goto_3
    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setAutoRetryOnConnectionError(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v6

    .line 465
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 466
    if-eqz v6, :cond_18

    .line 467
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    .line 468
    invoke-virtual {v6, p3, v8}, Landroid/app/enterprise/EnterpriseResponseData;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_17
    move v3, v9

    .line 463
    goto :goto_3

    .line 471
    .restart local v3    # "_arg2":Z
    .restart local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :cond_18
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 477
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":Z
    .end local v6    # "_result":Landroid/app/enterprise/EnterpriseResponseData;
    :sswitch_17
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 479
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 480
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->notifyContainerAppLaunch(I)V

    .line 481
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 486
    .end local v1    # "_arg0":I
    :sswitch_18
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 488
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 489
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->containerPackageListFromVpnDatabase(I)[Ljava/lang/String;

    move-result-object v6

    .line 490
    .local v6, "_result":[Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 491
    invoke-virtual {p3, v6}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    goto/16 :goto_0

    .line 496
    .end local v1    # "_arg0":I
    .end local v6    # "_result":[Ljava/lang/String;
    :sswitch_19
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 499
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->bindKnoxVpnInterface(Ljava/lang/String;)Z

    move-result v6

    .line 500
    .local v6, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 501
    if-eqz v6, :cond_19

    move v9, v8

    :cond_19
    invoke-virtual {p3, v9}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 506
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v6    # "_result":Z
    :sswitch_1a
    const-string v0, "com.sec.enterprise.knox.vpn.IEnterpriseVpnPolicy"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 508
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 509
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/sec/enterprise/knox/vpn/IEnterpriseVpnPolicy$Stub;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V

    .line 510
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
