.class public Lcom/sec/enterprise/knox/ContainerApplicationPolicy;
.super Ljava/lang/Object;
.source "ContainerApplicationPolicy.java"


# static fields
.field public static final INSTALL_ONLY:I = 0x1f7

.field private static TAG:Ljava/lang/String;

.field private static gAppService:Landroid/app/enterprise/IApplicationPolicy;


# instance fields
.field private final mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "ContainerApplicationPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(I)V
    .locals 2
    .param p1, "containerId"    # I

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-direct {v0, v1, p1}, Landroid/app/enterprise/ContextInfo;-><init>(II)V

    iput-object v0, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 96
    return-void
.end method

.method private declared-synchronized getAppService()Landroid/app/enterprise/IApplicationPolicy;
    .locals 1

    .prologue
    .line 86
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->gAppService:Landroid/app/enterprise/IApplicationPolicy;

    if-nez v0, :cond_0

    .line 87
    const-string v0, "application_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/app/enterprise/IApplicationPolicy$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->gAppService:Landroid/app/enterprise/IApplicationPolicy;

    .line 91
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->gAppService:Landroid/app/enterprise/IApplicationPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public addHomeShortcut(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 995
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerApplicationPolicy.addHomeShortcut"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 996
    const/4 v4, 0x0

    .line 997
    .local v4, "retVal":Z
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 998
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 1000
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1001
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1002
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 1012
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 1007
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x0

    invoke-interface {v0, v6, p1, v7}, Landroid/app/enterprise/IApplicationPolicy;->addHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    move v5, v4

    .line 1012
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 1008
    .end local v5    # "retVal":I
    :catch_0
    move-exception v3

    .line 1009
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at Application PolicyService API addHomeShortcut "

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addPackageToInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 738
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.addPackageToInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 739
    const/4 v2, 0x0

    .line 740
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 741
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 742
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 752
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 747
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackageToInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 752
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 748
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 749
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API addPackageToInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addPackagesToClearCacheBlackList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1573
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.addPackagesToClearCacheBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1574
    const/4 v2, 0x0

    .line 1575
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1576
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1577
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1586
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1582
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1586
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1583
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1584
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addPackagesToClearCacheWhiteList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1776
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.addPackagesToClearCacheWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1777
    const/4 v2, 0x0

    .line 1778
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1779
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1780
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1789
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1785
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1789
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1786
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1787
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addPackagesToClearDataBlackList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1139
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.addPackagesToClearDataBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1140
    const/4 v2, 0x0

    .line 1141
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1142
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1143
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1152
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1148
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1152
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1149
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1150
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public addPackagesToClearDataWhiteList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1342
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.addPackagesToClearDataWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1343
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1344
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    const/4 v2, 0x0

    .line 1345
    .local v2, "retVal":Z
    if-nez v0, :cond_0

    .line 1346
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1355
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1351
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->addPackagesToClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1355
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1352
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1353
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public deleteHomeShortcut(Ljava/lang/String;)Z
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 1056
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerApplicationPolicy.deleteHomeShortcut"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1057
    const/4 v4, 0x0

    .line 1058
    .local v4, "retVal":Z
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 1059
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 1061
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1062
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1063
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 1073
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 1068
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x0

    invoke-interface {v0, v6, p1, v7}, Landroid/app/enterprise/IApplicationPolicy;->deleteHomeShortcut(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    move v5, v4

    .line 1073
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 1069
    .end local v5    # "retVal":I
    :catch_0
    move-exception v3

    .line 1070
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at Application PolicyService API addHomeShortcut "

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getApplicationStateEnabled(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 573
    const/4 v2, 0x0

    .line 574
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 575
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 576
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 586
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 581
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->getApplicationStateEnabled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 586
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 582
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 583
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API setEnableApplication "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackages()[Ljava/lang/String;
    .locals 9

    .prologue
    .line 135
    const/4 v5, 0x0

    .line 137
    .local v5, "retVal":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v7, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 138
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 140
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v7, "2.0"

    const-string v8, "version"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 142
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 143
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 144
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 166
    .end local v0    # "appService":Landroid/app/enterprise/IApplicationPolicy;
    .end local v5    # "retVal":[Ljava/lang/String;
    .local v6, "retVal":[Ljava/lang/String;
    :goto_0
    return-object v6

    .line 149
    .end local v6    # "retVal":[Ljava/lang/String;
    .restart local v0    # "appService":Landroid/app/enterprise/IApplicationPolicy;
    .restart local v5    # "retVal":[Ljava/lang/String;
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v7}, Landroid/app/enterprise/IApplicationPolicy;->getInstalledApplicationsIDList(Landroid/app/enterprise/ContextInfo;)[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .end local v0    # "appService":Landroid/app/enterprise/IApplicationPolicy;
    :goto_1
    move-object v6, v5

    .line 166
    .end local v5    # "retVal":[Ljava/lang/String;
    .restart local v6    # "retVal":[Ljava/lang/String;
    goto :goto_0

    .line 150
    .end local v6    # "retVal":[Ljava/lang/String;
    .restart local v0    # "appService":Landroid/app/enterprise/IApplicationPolicy;
    .restart local v5    # "retVal":[Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 151
    .local v4, "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed at ContainerApplicationPolicy API getPackages "

    invoke-static {v7, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 154
    .end local v0    # "appService":Landroid/app/enterprise/IApplicationPolicy;
    .end local v4    # "e":Landroid/os/RemoteException;
    :cond_1
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseContainerManager;->getContainerService()Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;

    move-result-object v3

    .line 155
    .local v3, "containerService":Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;
    if-nez v3, :cond_2

    .line 156
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerPolicy Service is not yet ready!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v5

    .line 157
    .end local v5    # "retVal":[Ljava/lang/String;
    .restart local v6    # "retVal":[Ljava/lang/String;
    goto :goto_0

    .line 161
    .end local v6    # "retVal":[Ljava/lang/String;
    .restart local v5    # "retVal":[Ljava/lang/String;
    :cond_2
    :try_start_1
    invoke-interface {v3, v2}, Lcom/sec/enterprise/knox/IEnterpriseContainerPolicy;->getContainerPackages(I)[Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v5

    goto :goto_1

    .line 162
    :catch_1
    move-exception v4

    .line 163
    .restart local v4    # "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed at ContainerApplicationPolicy API getPackages "

    invoke-static {v7, v8, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackagesFromClearCacheBlackList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1641
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.getPackagesFromClearCacheBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1643
    const/4 v2, 0x0

    .line 1644
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1645
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1646
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1655
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1651
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1655
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1652
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1653
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackagesFromClearCacheWhiteList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1839
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.getPackagesFromClearCacheWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1841
    const/4 v2, 0x0

    .line 1842
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1843
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1844
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1853
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1849
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1853
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1850
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1851
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackagesFromClearDataBlackList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1207
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.getPackagesFromClearDataBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1209
    const/4 v2, 0x0

    .line 1210
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1211
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1212
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1221
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1217
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1221
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1218
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1219
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackagesFromClearDataWhiteList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1405
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.getPackagesFromClearDataWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1407
    const/4 v2, 0x0

    .line 1408
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1409
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1410
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1419
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1415
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 1419
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 1416
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 1417
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getPackagesFromInstallWhiteList()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 800
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.getPackagesFromInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 801
    const/4 v2, 0x0

    .line 802
    .local v2, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 803
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 804
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 815
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local v3, "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 809
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4}, Landroid/app/enterprise/IApplicationPolicy;->getPackagesFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_1
    move-object v3, v2

    .line 815
    .end local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    goto :goto_0

    .line 810
    .end local v3    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .restart local v2    # "retVal":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v1

    .line 811
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API getPackagesFromInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public installPackage(Ljava/lang/String;ILcom/sec/enterprise/knox/EnterpriseContainerCallback;)Z
    .locals 8
    .param p1, "packagePath"    # Ljava/lang/String;
    .param p2, "installType"    # I
    .param p3, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;

    .prologue
    .line 215
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerApplicationPolicy.installPackage"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 216
    const/4 v4, 0x0

    .line 217
    .local v4, "retVal":Z
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 218
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 220
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 221
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 222
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 231
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 227
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x0

    invoke-interface {v0, v6, p1, v7, p3}, Landroid/app/enterprise/IApplicationPolicy;->installAppWithCallback(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;ZLcom/sec/enterprise/knox/IEnterpriseContainerCallback;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    move v5, v4

    .line 231
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 228
    .end local v5    # "retVal":I
    :catch_0
    move-exception v3

    .line 229
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at ContainerApplicationPolicy API getPackages "

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isApplicationClearCacheDisabled(Ljava/lang/String;IZ)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "showMsg"    # Z

    .prologue
    .line 1928
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.isApplicationClearCacheDisabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1929
    const/4 v2, 0x0

    .line 1930
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1931
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1932
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1942
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1937
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationClearCacheDisabled(Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1942
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1938
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1939
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isApplicationClearDataDisabled(Ljava/lang/String;IZ)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "userId"    # I
    .param p3, "showMsg"    # Z

    .prologue
    .line 1494
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.isApplicationClearDataDisabled"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1495
    const/4 v2, 0x0

    .line 1496
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1497
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1498
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1507
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1503
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    invoke-interface {v0, p1, p2, p3}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationClearDataDisabled(Ljava/lang/String;IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1507
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1504
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1505
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public isPackageInInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 865
    const/4 v2, 0x0

    .line 866
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 867
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 868
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 878
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 873
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->isPackageInInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 878
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 874
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 875
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API isPackageInInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removePackageFromInstallWhiteList(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 933
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.removePackageFromInstallWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 935
    const/4 v2, 0x0

    .line 936
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 937
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 938
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 950
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 943
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackageFromInstallWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 950
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 944
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 945
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API removePackageFromInstallWhiteList "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removePackagesFromClearCacheBlackList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1701
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.removePackagesFromClearCacheBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1703
    const/4 v2, 0x0

    .line 1704
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1705
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1706
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1715
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1711
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearCacheBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1715
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1712
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1713
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removePackagesFromClearCacheWhiteList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1901
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.removePackagesFromClearCacheWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1903
    const/4 v2, 0x0

    .line 1904
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1905
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1906
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1915
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1911
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearCacheWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1915
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1912
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1913
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removePackagesFromClearDataBlackList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1267
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.removePackagesFromClearDataBlackList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1269
    const/4 v2, 0x0

    .line 1270
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1271
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1272
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1281
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1277
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearDataBlackList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1281
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1278
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1279
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public removePackagesFromClearDataWhiteList(Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1467
    .local p1, "packageList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.removePackagesFromClearDataWhiteList"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1469
    const/4 v2, 0x0

    .line 1470
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 1471
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 1472
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 1481
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 1477
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v4, p1}, Landroid/app/enterprise/IApplicationPolicy;->removePackagesFromClearDataWhiteList(Landroid/app/enterprise/ContextInfo;Ljava/util/List;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 1481
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 1478
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 1479
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed talking with application policy"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setDisableApplication(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 482
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.setDisableApplication"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 483
    const/4 v2, 0x0

    .line 484
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 485
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 486
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 496
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 491
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x0

    invoke-interface {v0, v4, p1, v5}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 496
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 492
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 493
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API setDisableApplication "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setEnableApplication(Ljava/lang/String;)Z
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 542
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v5, "ContainerApplicationPolicy.setEnableApplication"

    invoke-static {v4, v5}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 543
    const/4 v2, 0x0

    .line 544
    .local v2, "retVal":Z
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 545
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 546
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v3, v2

    .line 556
    .end local v2    # "retVal":Z
    .local v3, "retVal":I
    :goto_0
    return v3

    .line 551
    .end local v3    # "retVal":I
    .restart local v2    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v4, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v5, 0x1

    invoke-interface {v0, v4, p1, v5}, Landroid/app/enterprise/IApplicationPolicy;->setApplicationState(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_1
    move v3, v2

    .line 556
    .restart local v3    # "retVal":I
    goto :goto_0

    .line 552
    .end local v3    # "retVal":I
    :catch_0
    move-exception v1

    .line 553
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v4, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v5, "Failed at ContainerApplicationPolicy API setEnableApplication "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public startApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "className"    # Ljava/lang/String;

    .prologue
    .line 355
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerApplicationPolicy.startApp"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 356
    const/4 v4, 0x0

    .line 357
    .local v4, "retVal":Z
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 358
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 360
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 361
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 362
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 372
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 367
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v6, p1, p2}, Landroid/app/enterprise/IApplicationPolicy;->startApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    move v5, v4

    .line 372
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 368
    .end local v5    # "retVal":I
    :catch_0
    move-exception v3

    .line 369
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at ContainerApplicationPolicy API startApp "

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public stopApp(Ljava/lang/String;)Z
    .locals 8
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 412
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v7, "ContainerApplicationPolicy.stopApp"

    invoke-static {v6, v7}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 413
    const/4 v4, 0x0

    .line 414
    .local v4, "retVal":Z
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v6, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 415
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 417
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 418
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 419
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 429
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 424
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v6, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v0, v6, p1}, Landroid/app/enterprise/IApplicationPolicy;->stopApp(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    :goto_1
    move v5, v4

    .line 429
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 425
    .end local v5    # "retVal":I
    :catch_0
    move-exception v3

    .line 426
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v6, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v7, "Failed at ContainerApplicationPolicy API stopApp "

    invoke-static {v6, v7, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public uninstallPackage(Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseContainerCallback;)Z
    .locals 9
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/sec/enterprise/knox/EnterpriseContainerCallback;

    .prologue
    .line 278
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v8, "ContainerApplicationPolicy.uninstallPackage"

    invoke-static {v7, v8}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 279
    const/4 v4, 0x0

    .line 280
    .local v4, "retVal":Z
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget v2, v7, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    .line 281
    .local v2, "containerId":I
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v1

    .line 283
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-direct {p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v0

    .line 284
    .local v0, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v0, :cond_0

    .line 285
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v5, v4

    .line 313
    .end local v4    # "retVal":Z
    .local v5, "retVal":I
    :goto_0
    return v5

    .line 290
    .end local v5    # "retVal":I
    .restart local v4    # "retVal":Z
    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v8, 0x0

    invoke-interface {v0, v7, p1, v8}, Landroid/app/enterprise/IApplicationPolicy;->uninstallApplication(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Z)Z

    move-result v4

    .line 291
    if-eqz p2, :cond_1

    .line 292
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 293
    .local v6, "uninstallState":Landroid/os/Bundle;
    const-string v7, "packageName"

    invoke-virtual {v6, v7, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 296
    const/4 v7, 0x1

    if-ne v4, v7, :cond_2

    .line 297
    const/16 v7, 0x3f0

    invoke-virtual {p2, v7, v6}, Lcom/sec/enterprise/knox/EnterpriseContainerCallback;->updateStatus(ILandroid/os/Bundle;)V

    .end local v6    # "uninstallState":Landroid/os/Bundle;
    :cond_1
    :goto_1
    move v5, v4

    .line 313
    .restart local v5    # "retVal":I
    goto :goto_0

    .line 301
    .end local v5    # "retVal":I
    .restart local v6    # "uninstallState":Landroid/os/Bundle;
    :cond_2
    const-string v7, "pmerrorcode"

    const/4 v8, -0x1

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 304
    const/16 v7, 0x3f1

    invoke-virtual {p2, v7, v6}, Lcom/sec/enterprise/knox/EnterpriseContainerCallback;->updateStatus(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 309
    .end local v6    # "uninstallState":Landroid/os/Bundle;
    :catch_0
    move-exception v3

    .line 310
    .local v3, "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v8, "Failed at ContainerApplicationPolicy API getPackages "

    invoke-static {v7, v8, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public writeData(Ljava/lang/String;Ljava/lang/String;[B)Z
    .locals 16
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "data"    # [B

    .prologue
    .line 631
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v4, "ContainerApplicationPolicy.writeData"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 632
    const/high16 v9, 0x40000

    .line 633
    .local v9, "MAX_NUM_OF_BYTES":I
    const/4 v14, 0x0

    .line 634
    .local v14, "retVal":Z
    invoke-static {}, Landroid/os/PersonaManager;->getKnoxInfo()Landroid/os/Bundle;

    move-result-object v10

    .line 636
    .local v10, "bundle":Landroid/os/Bundle;
    invoke-direct/range {p0 .. p0}, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->getAppService()Landroid/app/enterprise/IApplicationPolicy;

    move-result-object v2

    .line 637
    .local v2, "appService":Landroid/app/enterprise/IApplicationPolicy;
    if-nez v2, :cond_0

    .line 638
    sget-object v3, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "ContainerApplication PolicyService is not yet ready!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v15, v14

    .line 682
    .end local v14    # "retVal":Z
    .local v15, "retVal":I
    :goto_0
    return v15

    .line 641
    .end local v15    # "retVal":I
    .restart local v14    # "retVal":Z
    :cond_0
    const/4 v6, 0x0

    .line 642
    .local v6, "buffer":[B
    const/4 v8, 0x0

    .line 644
    .local v8, "totalLength":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    move-object/from16 v0, p1

    invoke-interface {v2, v3, v0}, Landroid/app/enterprise/IApplicationPolicy;->isApplicationInstalled(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 645
    sget-object v3, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "package is not in container!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v15, v14

    .line 646
    .restart local v15    # "retVal":I
    goto :goto_0

    .line 648
    .end local v15    # "retVal":I
    :cond_1
    const/4 v13, 0x0

    .line 649
    .local v13, "num_of_packets":I
    if-eqz p3, :cond_2

    .line 650
    move-object/from16 v0, p3

    array-length v8, v0

    .line 651
    move-object/from16 v0, p3

    array-length v3, v0

    const/high16 v4, 0x40000

    div-int v13, v3, v4

    .line 652
    if-lez v13, :cond_3

    .line 653
    const/4 v3, 0x0

    const/high16 v4, 0x40000

    move-object/from16 v0, p3

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v6

    .line 659
    :cond_2
    :goto_1
    monitor-enter p0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 660
    :try_start_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-interface/range {v2 .. v8}, Landroid/app/enterprise/IApplicationPolicy;->writeData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;[BZI)Z

    move-result v14

    .line 662
    const/4 v12, 0x1

    .local v12, "i":I
    :goto_2
    if-gt v12, v13, :cond_5

    if-eqz v14, :cond_5

    .line 663
    const-wide/16 v4, 0x64

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, Ljava/lang/Object;->wait(J)V

    .line 664
    if-ne v12, v13, :cond_4

    .line 665
    const/high16 v3, 0x40000

    mul-int/2addr v3, v12

    move-object/from16 v0, p3

    invoke-static {v0, v3, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v6

    .line 670
    :goto_3
    const/4 v14, 0x0

    .line 671
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const/4 v7, 0x1

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-interface/range {v2 .. v8}, Landroid/app/enterprise/IApplicationPolicy;->writeData(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;[BZI)Z

    move-result v14

    .line 662
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 655
    .end local v12    # "i":I
    :cond_3
    move-object/from16 v6, p3

    goto :goto_1

    .line 667
    .restart local v12    # "i":I
    :cond_4
    const/high16 v3, 0x40000

    mul-int/2addr v3, v12

    add-int/lit8 v4, v12, 0x1

    const/high16 v5, 0x40000

    mul-int/2addr v4, v5

    move-object/from16 v0, p3

    invoke-static {v0, v3, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v6

    goto :goto_3

    .line 675
    :cond_5
    monitor-exit p0

    .end local v12    # "i":I
    .end local v13    # "num_of_packets":I
    :goto_4
    move v15, v14

    .line 682
    .restart local v15    # "retVal":I
    goto :goto_0

    .line 675
    .end local v15    # "retVal":I
    .restart local v13    # "num_of_packets":I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v3
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    .line 676
    .end local v13    # "num_of_packets":I
    :catch_0
    move-exception v11

    .line 677
    .local v11, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "Failed at ContainerApplicationPolicy API writeData "

    invoke-static {v3, v4, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 678
    .end local v11    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v11

    .line 679
    .local v11, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/sec/enterprise/knox/ContainerApplicationPolicy;->TAG:Ljava/lang/String;

    const-string v4, "InterruptedException at ContainerApplicationPolicy API writeData "

    invoke-static {v3, v4, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4
.end method
