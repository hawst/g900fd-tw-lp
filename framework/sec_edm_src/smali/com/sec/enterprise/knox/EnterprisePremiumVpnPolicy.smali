.class public Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;
.super Ljava/lang/Object;
.source "EnterprisePremiumVpnPolicy.java"


# static fields
.field private static final INVALID_CONTAINER_ID:I = 0x0

.field private static TAG:Ljava/lang/String; = null

.field public static final VPN_TYPE_KEYVPN:Ljava/lang/String; = "key_vpn"


# instance fields
.field private mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/app/enterprise/EnterpriseResponseData",
            "<*>;"
        }
    .end annotation
.end field

.field private mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "EnterprisePremiumVpnPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    return-void
.end method

.method private getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    if-nez v0, :cond_0

    .line 90
    const-string v0, "enterprise_premium_vpn_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    return-object v0
.end method


# virtual methods
.method public addVpnProfileToApp(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 899
    const/4 v1, 0x0

    .line 901
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 902
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    const/4 v3, 0x0

    invoke-interface {v2, v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->addVpnProfileToApp(ILjava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 906
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 907
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 908
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 922
    :cond_0
    :goto_0
    return v1

    .line 909
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 910
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 917
    :catch_0
    move-exception v0

    .line 918
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "add premium pakcage in profile >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 912
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "add premium pakcage in profile > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 915
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "add premium pakcage in profile > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public enableDefaultRoute(Ljava/lang/String;Ljava/lang/String;Z)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .param p3, "enable"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 807
    const/4 v1, 0x0

    .line 809
    .local v1, "enableDefaultRouteValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 810
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->enableDefaultRoute(Ljava/lang/String;Ljava/lang/String;Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 813
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 814
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 815
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 829
    :cond_0
    :goto_0
    return v1

    .line 816
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 817
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 824
    :catch_0
    move-exception v0

    .line 825
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enable premium default route >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 819
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "enable premium default route > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 822
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "enable premium default route > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getAllEnterpriseVpnConnections(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 1208
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "EnterprisePremiumVpnPolicy.getAllEnterpriseVpnConnections"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 1209
    const/4 v1, 0x0

    .line 1211
    .local v1, "connectionList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1212
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getAllEnterpriseVpnConnections(Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1214
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 1215
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 1216
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v1, v0

    .line 1232
    :cond_0
    :goto_0
    return-object v1

    .line 1218
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 1219
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1228
    :catch_0
    move-exception v2

    .line 1229
    .local v2, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting all premium vpn connection  >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1222
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting all premium vpn connection > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1225
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting all premium vpn connection  > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getAllPackagesForProfile(Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1, "profileName"    # Ljava/lang/String;

    .prologue
    .line 1011
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1012
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    const/4 v2, 0x0

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getAllPackagesForProfile(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1014
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v1, :cond_0

    .line 1015
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v1

    if-nez v1, :cond_1

    .line 1016
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v1}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    .line 1026
    :goto_0
    return-object v1

    .line 1018
    :cond_0
    sget-object v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "get all premium added package > response data == null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    :cond_1
    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1021
    :cond_2
    sget-object v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "get all premium added package > service == null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1023
    :catch_0
    move-exception v0

    .line 1024
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v2, "get all premium added package >> Exception : "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public getCACertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 587
    const/4 v2, 0x0

    .line 589
    .local v2, "getCACertificateValue":Landroid/app/enterprise/CertificateInfo;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 590
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getCACertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 592
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 593
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 594
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    move-object v2, v0

    .line 608
    :cond_0
    :goto_0
    return-object v2

    .line 595
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 596
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 603
    :catch_0
    move-exception v1

    .line 604
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium CA certificate >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 598
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium CA certificate > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 601
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium CA certificate > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getEnterprisePremiumVpnConnection(Ljava/lang/String;Ljava/lang/String;)Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 160
    const/4 v2, 0x0

    .line 162
    .local v2, "getEnterpriseVpnConnectionValue":Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 163
    new-instance v3, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v4

    invoke-direct {v3, v4}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v4, "EnterprisePremiumVpnPolicy.getEnterprisePremiumVpnConnection"

    invoke-static {v3, v4}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 164
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 166
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 167
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 168
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;

    move-object v2, v0

    .line 184
    :cond_0
    :goto_0
    return-object v2

    .line 170
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 171
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :catch_0
    move-exception v1

    .line 182
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium vpn connection >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 174
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn connection > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 178
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn connection  > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getErrorString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 710
    const/4 v2, 0x0

    .line 712
    .local v2, "getErrorValue":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 713
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getErrorString(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 714
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 715
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 716
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 731
    :cond_0
    :goto_0
    return-object v2

    .line 717
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 718
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 726
    :catch_0
    move-exception v1

    .line 727
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium vpn error string >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 720
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn error string > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 723
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn error string > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getForwardRoutes(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 774
    const/4 v2, 0x0

    .line 776
    .local v2, "getForwardRoutesValue":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 777
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getForwardRoutes(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 779
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 780
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 781
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/List;

    move-object v2, v0

    .line 796
    :cond_0
    :goto_0
    return-object v2

    .line 782
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 783
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 791
    :catch_0
    move-exception v1

    .line 792
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium forward route >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 785
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium forward route > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 788
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium forward route > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getState(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 646
    const/4 v2, 0x0

    .line 648
    .local v2, "getStateValue":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 649
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getState(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 651
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 653
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 654
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 671
    :cond_0
    :goto_0
    return-object v2

    .line 656
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 657
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 666
    :catch_0
    move-exception v1

    .line 667
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium vpn state >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 660
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn state >> reponse data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 663
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium vpn state >> service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getUserCertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/CertificateInfo;
    .locals 6
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 449
    const/4 v2, 0x0

    .line 451
    .local v2, "getUserCertificateValue":Landroid/app/enterprise/CertificateInfo;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 452
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v3, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getUserCertificate(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 454
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v3, :cond_2

    .line 455
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    if-nez v3, :cond_1

    .line 456
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Landroid/app/enterprise/CertificateInfo;

    move-object v2, v0

    .line 471
    :cond_0
    :goto_0
    return-object v2

    .line 457
    :cond_1
    iget-object v3, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v3}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 458
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v3}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 466
    :catch_0
    move-exception v1

    .line 467
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getting premium user certificate >> Exception"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 460
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium user certificate > response data == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 463
    :cond_3
    sget-object v3, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v4, "getting premium user certificate > service == null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public getVpnModeOfOperation()I
    .locals 5

    .prologue
    .line 1319
    const/4 v1, -0x1

    .line 1321
    .local v1, "getVpnModeOfOperationValue":I
    :try_start_0
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    if-eqz v2, :cond_2

    .line 1322
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->getVpnModeOfOperation()Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1323
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_1

    .line 1324
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_0

    .line 1325
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1337
    :cond_0
    :goto_0
    return v1

    .line 1327
    :cond_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getting premium vpn mode > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1332
    :catch_0
    move-exception v0

    .line 1333
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getting premium vpn mode >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1330
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "getting premium vpn mode > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public isDefaultRouteEnabled(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 840
    const/4 v1, 0x0

    .line 842
    .local v1, "isDefaultRouteEnabledValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 843
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->isDefaultRouteEnabled(Ljava/lang/String;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 845
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 846
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 847
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 861
    :cond_0
    :goto_0
    return v1

    .line 848
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 849
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 856
    :catch_0
    move-exception v0

    .line 857
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "check to premium default route >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 851
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "check to premium default route > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 854
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "check to premium default route > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public removeEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseVpnStatusCallback;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "vpnStatusCallback"    # Lcom/sec/enterprise/knox/EnterpriseVpnStatusCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 308
    const/4 v1, 0x0

    .line 310
    .local v1, "removeEnterpriseVpnConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 311
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "EnterprisePremiumVpnPolicy.removeEnterpriseVpnConnection"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 312
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->removeEnterpriseVpnConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 314
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 315
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 316
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 331
    :cond_0
    :goto_0
    return v1

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 319
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :catch_0
    move-exception v0

    .line 329
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove premium vpn connection >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 322
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "remove premium vpn connection > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 325
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "remove premium vpn connection > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public removeVpnForApplication(Ljava/lang/String;)Z
    .locals 5
    .param p1, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 955
    const/4 v1, 0x0

    .line 957
    .local v1, "returnValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 958
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    const/4 v3, 0x0

    invoke-interface {v2, v3, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->removeVpnForApplication(ILjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 961
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 962
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 963
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 977
    :cond_0
    :goto_0
    return v1

    .line 964
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 965
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 972
    :catch_0
    move-exception v0

    .line 973
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "remove to premium package >>Exception: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 967
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "remove to premium package > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 970
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "remove to premium package > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setCACertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .param p3, "certificateBlob"    # [B
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 524
    const/4 v1, 0x0

    .line 526
    .local v1, "setCACertificateValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 527
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setCACertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 530
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 531
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 532
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 547
    :cond_0
    :goto_0
    return v1

    .line 533
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 534
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    :catch_0
    move-exception v0

    .line 543
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting premium CA certificate >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 536
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium CA certificate > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 540
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium CA certificate > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setEnterpriseVpnConnection(Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;Ljava/lang/String;)Z
    .locals 5
    .param p1, "vpn"    # Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;
    .param p2, "oldName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 235
    const/4 v1, 0x0

    .line 237
    .local v1, "setEnterpriseVpnConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 238
    new-instance v2, Landroid/app/enterprise/ContextInfo;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/app/enterprise/ContextInfo;-><init>(I)V

    const-string v3, "EnterprisePremiumVpnPolicy.setEnterpriseVpnConnection(com.mocana.vpn.android)"

    invoke-static {v2, v3}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 239
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setEnterprisePremiumVpnConnection(Lcom/sec/enterprise/knox/EnterprisePremiumVpnConnection;Ljava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 241
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 242
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 243
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 257
    :cond_0
    :goto_0
    return v1

    .line 245
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 246
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting premium vpn connction >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 248
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium vpn connction > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 251
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium vpn connction > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setForwardRoutes(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 741
    .local p3, "networks":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 743
    .local v1, "setForwardRouteValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 744
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setForwardRoutes(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 746
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 747
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 748
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 762
    :cond_0
    :goto_0
    return v1

    .line 749
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 750
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 757
    :catch_0
    move-exception v0

    .line 758
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting premium forward route >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 752
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium forward route > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 755
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium forward route > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public setUserCertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .param p3, "pkcs12Blob"    # [B
    .param p4, "password"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 385
    const/4 v1, 0x0

    .line 387
    .local v1, "setUserCertificateValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 388
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3, p4}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setUserCertificate(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 392
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 393
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 394
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 409
    :cond_0
    :goto_0
    return v1

    .line 395
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 396
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :catch_0
    move-exception v0

    .line 405
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting premium user certificate >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 398
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium user certificate  > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 401
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium user certificate > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    .locals 4
    .param p1, "currentFramework"    # Ljava/lang/String;

    .prologue
    .line 101
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setVpnFrameworkSystemProperty(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_0
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 103
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception at setVpnFrameworkProperty"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setVpnModeOfOperation(Z)Z
    .locals 5
    .param p1, "vpnMode"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 1267
    const/4 v1, 0x0

    .line 1269
    .local v1, "setVpnModeOfOperationValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1270
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->setVpnModeOfOperation(Z)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1272
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 1273
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 1274
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1289
    :cond_0
    :goto_0
    return v1

    .line 1275
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1276
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1284
    :catch_0
    move-exception v0

    .line 1285
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setting premium vpn mode >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1278
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium vpn mode > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1281
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "setting premium vpn mode > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public startConnection(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseVpnCallback;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .param p3, "statusCb"    # Lcom/sec/enterprise/knox/EnterpriseVpnCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 1074
    const/4 v1, 0x0

    .line 1076
    .local v1, "startConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1077
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->startConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1079
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 1080
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 1081
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1096
    :cond_0
    :goto_0
    return v1

    .line 1082
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1083
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1091
    :catch_0
    move-exception v0

    .line 1092
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "start premium connection >> Exception"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1085
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "start premium connection > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1088
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "start premium connection > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public startMocanaService()V
    .locals 4

    .prologue
    .line 113
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->bindMocanaVpnInterface()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "startMocanaService: Exception at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stopConnection(Ljava/lang/String;Ljava/lang/String;Lcom/sec/enterprise/knox/EnterpriseVpnCallback;)Z
    .locals 5
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "profileName"    # Ljava/lang/String;
    .param p3, "statusCb"    # Lcom/sec/enterprise/knox/EnterpriseVpnCallback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/UnsupportedOperationException;
        }
    .end annotation

    .prologue
    .line 1143
    const/4 v1, 0x0

    .line 1145
    .local v1, "stopConnectionValue":Z
    :try_start_0
    invoke-direct {p0}, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->getService()Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1146
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mService:Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;

    invoke-interface {v2, p1, p2, p3}, Lcom/sec/enterprise/knox/vpn/IEnterprisePremiumVpnPolicy;->stopConnection(Ljava/lang/String;Ljava/lang/String;Lcom/mocana/vpn/android/velo/IEnterpriseStatusCallback;)Landroid/app/enterprise/EnterpriseResponseData;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    .line 1148
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    if-eqz v2, :cond_2

    .line 1149
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    if-nez v2, :cond_1

    .line 1150
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1165
    :cond_0
    :goto_0
    return v1

    .line 1151
    :cond_1
    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->mEnterpriseResponseData:Landroid/app/enterprise/EnterpriseResponseData;

    invoke-virtual {v2}, Landroid/app/enterprise/EnterpriseResponseData;->getFailureState()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1152
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v2}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1160
    :catch_0
    move-exception v0

    .line 1161
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stop premium connection >> Exception : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1154
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "stop premium connection > response data == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1157
    :cond_3
    sget-object v2, Lcom/sec/enterprise/knox/EnterprisePremiumVpnPolicy;->TAG:Ljava/lang/String;

    const-string v3, "stop premium connection > service == null"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
