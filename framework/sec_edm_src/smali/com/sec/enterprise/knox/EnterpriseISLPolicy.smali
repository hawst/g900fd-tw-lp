.class public Lcom/sec/enterprise/knox/EnterpriseISLPolicy;
.super Ljava/lang/Object;
.source "EnterpriseISLPolicy.java"


# static fields
.field public static final OPERATION_CLEAR_BASELINE:I = 0x4

.field public static final OPERATION_ESTABLISH_BASELINE:I = 0x2

.field public static final OPERATION_PRE_BASELINE_SCAN:I = 0x1

.field public static final OPERATION_PUT_PACKAGE_TO_BASELINE:I = 0x5

.field public static final OPERATION_REMOVE_PACKAGE_FROM_BASELINE:I = 0x6

.field public static final OPERATION_SCAN:I = 0x3

.field public static final OPERATION_UPDATE_PLATFORM_BASELINE:I = 0x7

.field public static final SCAN_3RD_PARTY_PACKAGES:I = 0xc

.field public static final SCAN_PLATFORM_AND_3RD_PARTY_PACKAGES:I = 0xd

.field public static final SCAN_PLATFORM_BINARIES:I = 0xb

.field private static TAG:Ljava/lang/String;

.field private static mISLService:Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;


# instance fields
.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    const-string v0, "EnterpriseISLPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;

    .prologue
    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 210
    iput-object p1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 211
    return-void
.end method

.method static declared-synchronized getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;
    .locals 2

    .prologue
    .line 199
    const-class v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mISLService:Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    if-nez v0, :cond_0

    .line 200
    const-string v0, "enterprise_isl_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mISLService:Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    .line 203
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mISLService:Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public clearBaseline()Z
    .locals 3

    .prologue
    .line 425
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.clearBaseline"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 428
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->clearBaseline(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 433
    :goto_0
    return v1

    .line 429
    :catch_0
    move-exception v0

    .line 430
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API clearBaseline-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 433
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public establishBaselineScan(Z)Z
    .locals 3
    .param p1, "compareWithPreBaseline"    # Z

    .prologue
    .line 313
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 314
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.establishBaselineScan"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 316
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->establishBaselineScan(Landroid/app/enterprise/ContextInfo;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 321
    :goto_0
    return v1

    .line 317
    :catch_0
    move-exception v0

    .line 318
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API establishBaselineScan-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 321
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getISAList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 766
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 767
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.getISAList"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 769
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->getISAList(Landroid/app/enterprise/ContextInfo;)Ljava/util/List;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 774
    :goto_0
    return-object v1

    .line 770
    :catch_0
    move-exception v0

    .line 771
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API getISAList-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 774
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isISAReady()Z
    .locals 3

    .prologue
    .line 728
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.isISAReady"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 731
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->isISAReady(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 736
    :goto_0
    return v1

    .line 732
    :catch_0
    move-exception v0

    .line 733
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API isISAReady-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 736
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performPreBaselineScan()Z
    .locals 3

    .prologue
    .line 257
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.performPreBaselineScan"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 260
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->performPreBaselineScan(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 265
    :goto_0
    return v1

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API performPreBaselineScan-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 265
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public performScanNow(ILjava/util/List;)Z
    .locals 3
    .param p1, "scanType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 382
    .local p2, "packageNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 383
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.performScanNow"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 385
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1, p2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->performScanNow(Landroid/app/enterprise/ContextInfo;ILjava/util/List;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 390
    :goto_0
    return v1

    .line 386
    :catch_0
    move-exception v0

    .line 387
    .local v0, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API performScanNow-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 390
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public putPackageToBaseline(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 475
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 476
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.putPackageToBaseline"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 478
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->putPackageToBaseline(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 483
    :goto_0
    return v1

    .line 479
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API putPackageToBaseline-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 483
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public removePackageFromBaseline(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 522
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 523
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.removePackageFromBaseline"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 525
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->removePackageFromBaseline(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 531
    :goto_0
    return v1

    .line 526
    :catch_0
    move-exception v0

    .line 527
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API removePackageFromBaseline-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 531
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestBindISA(Ljava/lang/String;)Z
    .locals 3
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 689
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 690
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.requestBindISA"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 692
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->requestBindISA(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 697
    :goto_0
    return v1

    .line 693
    :catch_0
    move-exception v0

    .line 694
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API requestBindISA-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 697
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setIntegrityResultSubscriber(Lcom/sec/enterprise/knox/IntegrityResultSubscriber;)V
    .locals 3
    .param p1, "subscriber"    # Lcom/sec/enterprise/knox/IntegrityResultSubscriber;

    .prologue
    .line 629
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 630
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.setIntegrityResultSubscriber"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 632
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2, p1}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->setIntegrityResultSubscriber(Landroid/app/enterprise/ContextInfo;Lcom/sec/enterprise/knox/IIntegrityResultSubscriber;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 639
    :cond_0
    :goto_0
    return-void

    .line 633
    :catch_0
    move-exception v0

    .line 634
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API setIntegrityResultSubscriber-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public startRuntimeWatch()Z
    .locals 3

    .prologue
    .line 804
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 806
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->startRuntimeWatch(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 811
    :goto_0
    return v1

    .line 807
    :catch_0
    move-exception v0

    .line 808
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API startWatching-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 811
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public stopRuntimeWatch()Z
    .locals 3

    .prologue
    .line 841
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 843
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->stopRuntimeWatch(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 848
    :goto_0
    return v1

    .line 844
    :catch_0
    move-exception v0

    .line 845
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API stopWatching-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 848
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updatePlatformBaseline()Z
    .locals 3

    .prologue
    .line 571
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 572
    iget-object v1, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseISLPolicy.updatePlatformBaseline"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 574
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->getISLService()Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    invoke-interface {v1, v2}, Lcom/sec/enterprise/knox/IEnterpriseISLPolicy;->updatePlatformBaseline(Landroid/app/enterprise/ContextInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 579
    :goto_0
    return v1

    .line 575
    :catch_0
    move-exception v0

    .line 576
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/EnterpriseISLPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseISLPolicy API updatePlatformBaseline-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 579
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
