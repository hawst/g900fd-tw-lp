.class public abstract Lcom/sec/enterprise/knox/IntegrityResultSubscriber;
.super Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;
.source "IntegrityResultSubscriber.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/sec/enterprise/knox/IIntegrityResultSubscriber$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract onError(IILjava/lang/String;I)V
.end method

.method public abstract onProgress(II)V
.end method

.method public abstract onReady()V
.end method

.method public abstract onRuntimeViolation(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract onSuccess(I)V
.end method

.method public abstract onTimaViolation(Ljava/lang/String;)V
.end method

.method public abstract onViolation(IILjava/lang/String;[B[BI)V
.end method
