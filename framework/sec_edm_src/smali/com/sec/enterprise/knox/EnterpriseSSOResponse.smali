.class public Lcom/sec/enterprise/knox/EnterpriseSSOResponse;
.super Ljava/lang/Object;
.source "EnterpriseSSOResponse.java"


# static fields
.field public static final SSO_BIND_FAIL:I = -0x1

.field public static final SSO_PACKAGE_NOT_FOUND:I = -0x2

.field public static final SUCCESS:I


# instance fields
.field private mErrorCode:I

.field private mPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->mErrorCode:I

    .line 90
    return-void
.end method


# virtual methods
.method public GetError()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->mErrorCode:I

    return v0
.end method

.method public GetSSOPolicy()Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->mPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    return-object v0
.end method

.method public SetError(I)V
    .locals 0
    .param p1, "errorCode"    # I

    .prologue
    .line 96
    iput p1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->mErrorCode:I

    .line 97
    return-void
.end method

.method public SetSSOPolicy(Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;)V
    .locals 0
    .param p1, "ssoPolicy"    # Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/enterprise/knox/EnterpriseSSOResponse;->mPolicy:Lcom/sec/enterprise/knox/EnterpriseSSOPolicy;

    .line 104
    return-void
.end method
