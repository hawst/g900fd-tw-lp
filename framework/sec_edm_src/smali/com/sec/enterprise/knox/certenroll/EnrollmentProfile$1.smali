.class final Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile$1;
.super Ljava/lang/Object;
.source "EnrollmentProfile.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
    .locals 2
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    .line 158
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 159
    .local v0, "subClassName":Ljava/lang/String;
    const-class v1, Lcom/sec/enterprise/knox/certenroll/SCEPProfile;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 160
    new-instance v1, Lcom/sec/enterprise/knox/certenroll/SCEPProfile;

    invoke-direct {v1, p1}, Lcom/sec/enterprise/knox/certenroll/SCEPProfile;-><init>(Landroid/os/Parcel;)V

    .line 166
    :goto_0
    return-object v1

    .line 161
    :cond_0
    const-class v1, Lcom/sec/enterprise/knox/certenroll/CMCProfile;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    new-instance v1, Lcom/sec/enterprise/knox/certenroll/CMCProfile;

    invoke-direct {v1, p1}, Lcom/sec/enterprise/knox/certenroll/CMCProfile;-><init>(Landroid/os/Parcel;)V

    goto :goto_0

    .line 163
    :cond_1
    const-class v1, Lcom/sec/enterprise/knox/certenroll/CMPProfile;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 164
    new-instance v1, Lcom/sec/enterprise/knox/certenroll/CMPProfile;

    invoke-direct {v1, p1}, Lcom/sec/enterprise/knox/certenroll/CMPProfile;-><init>(Landroid/os/Parcel;)V

    goto :goto_0

    .line 166
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Landroid/os/Parcel;

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile$1;->createFromParcel(Landroid/os/Parcel;)Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 171
    new-array v0, p1, [Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 155
    invoke-virtual {p0, p1}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile$1;->newArray(I)[Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;

    move-result-object v0

    return-object v0
.end method
