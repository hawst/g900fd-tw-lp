.class public abstract Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
.super Ljava/lang/Object;
.source "EnrollmentProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public certificateAlias:Ljava/lang/String;

.field public keyPairAlgorithm:Ljava/lang/String;

.field public keySize:I

.field public keystoreType:Ljava/lang/String;

.field public profileType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 154
    new-instance v0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile$1;

    invoke-direct {v0}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile$1;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCertificateAlias()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->certificateAlias:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyPairAlgorithm()Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keyPairAlgorithm:Ljava/lang/String;

    return-object v0
.end method

.method public getKeySize()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keySize:I

    return v0
.end method

.method public getKeystoreType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keystoreType:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getProfileType()Ljava/lang/String;
.end method

.method public setCertificateAlias(Ljava/lang/String;)V
    .locals 0
    .param p1, "certificateAlias"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->certificateAlias:Ljava/lang/String;

    .line 121
    return-void
.end method

.method public setKeyPairAlgorithm(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyPairAlgorithm"    # Ljava/lang/String;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keyPairAlgorithm:Ljava/lang/String;

    .line 99
    return-void
.end method

.method public setKeySize(I)V
    .locals 0
    .param p1, "keySize"    # I

    .prologue
    .line 74
    iput p1, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keySize:I

    .line 75
    return-void
.end method

.method public setKeystoreType(Ljava/lang/String;)V
    .locals 0
    .param p1, "keystoreType"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;->keystoreType:Ljava/lang/String;

    .line 147
    return-void
.end method
