.class public Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
.super Ljava/lang/Object;
.source "EnterpriseCertEnrollPolicy.java"


# static fields
.field private static final KNOX_SCEP_POLICY_SERVICE:Ljava/lang/String; = "knox_scep_policy"

.field private static TAG:Ljava/lang/String;

.field private static certEnrolPolicyMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;",
            ">;>;"
        }
    .end annotation
.end field

.field private static volatile gSCEPService:Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;


# instance fields
.field private cepProtocol:Ljava/lang/String;

.field private mContainerId:I

.field private mContext:Landroid/content/Context;

.field private mContextInfo:Landroid/app/enterprise/ContextInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "EnterpriseCertEnrollPolicy"

    sput-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p1, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cepProtocol"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    .line 109
    iput-object p2, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContext:Landroid/content/Context;

    .line 110
    iput-object p3, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->cepProtocol:Ljava/lang/String;

    .line 112
    return-void
.end method

.method public static getInstance(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .locals 10
    .param p0, "cxtInfo"    # Landroid/app/enterprise/ContextInfo;
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cepProtocol"    # Ljava/lang/String;

    .prologue
    .line 119
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EnterpriseCertEnrollPolicy getInstance : cepProtocol = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    const/4 v3, 0x0

    .line 122
    .local v3, "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    if-eqz p2, :cond_2

    if-eqz p1, :cond_2

    .line 123
    :try_start_0
    const-class v7, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    :try_start_1
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    iget v8, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 127
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    iget v8, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/HashMap;

    .line 128
    .local v5, "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    if-eqz v5, :cond_0

    .line 129
    invoke-virtual {v5, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    move-object v0, v6

    check-cast v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    move-object v3, v0

    .line 130
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v8, "EnterpriseCertEnrollPolicy getInstance : Already Bind"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    move-object v4, v3

    .line 132
    .end local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .local v4, "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    if-nez v4, :cond_4

    .line 133
    :try_start_2
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EnterpriseCertEnrollPolicy getInstance : but not found for package ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    new-instance v3, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    invoke-direct {v3, p0, p1, p2}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 135
    .end local v4    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .restart local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    :try_start_3
    new-instance v5, Ljava/util/HashMap;

    .end local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 136
    .restart local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    invoke-virtual {v5, p2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    iget v8, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :goto_0
    if-eqz v3, :cond_1

    .line 149
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v6

    invoke-interface {v6, p0, p2}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->isEnrollCertServiceActivated(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_5

    .line 150
    const/4 v1, -0x1

    .line 151
    .local v1, "bindSuccess":I
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v6

    invoke-interface {v6, p0, p2}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->activateEnrollCertService(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)I

    move-result v1

    .line 152
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EnterpriseCertEnrollPolicy getInstance : bindSuccess = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "-->containerid["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "] protocol["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const/16 v6, -0x258

    if-eq v1, v6, :cond_1

    .line 154
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    iget v8, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    check-cast v5, Ljava/util/HashMap;

    .line 155
    .restart local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    if-eqz v5, :cond_1

    .line 156
    invoke-virtual {v5, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    const/4 v3, 0x0

    .line 158
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v8, "EnterpriseCertEnrollPolicy getInstance : Binding Failed; CEP object removed"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    .end local v1    # "bindSuccess":I
    :cond_1
    :goto_1
    monitor-exit v7

    .line 172
    .end local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    :cond_2
    :goto_2
    return-object v3

    .line 141
    :cond_3
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "EnterpriseCertEnrollPolicy getInstance : but not found entries for container ["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    new-instance v4, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;

    invoke-direct {v4, p0, p1, p2}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;-><init>(Landroid/app/enterprise/ContextInfo;Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    .end local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .restart local v4    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    :try_start_4
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 144
    .restart local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    invoke-virtual {v5, p2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->certEnrolPolicyMap:Ljava/util/Map;

    iget v8, p0, Landroid/app/enterprise/ContextInfo;->mContainerId:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_4
    move-object v3, v4

    .end local v4    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .restart local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    goto/16 :goto_0

    .line 162
    :cond_5
    :try_start_5
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v8, "EnterpriseCertEnrollPolicy getInstance :Already bind to Service"

    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 165
    .end local v5    # "policy":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;>;"
    :catchall_0
    move-exception v6

    :goto_3
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    throw v6
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    .line 167
    :catch_0
    move-exception v2

    .line 168
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    .line 169
    const/4 v3, 0x0

    .line 170
    sget-object v6, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "EnterpriseCertEnrollPolicy getInstance : returning null for cepProtocol = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 165
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .restart local v4    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    .restart local v3    # "eCertEnrolPolicy":Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;
    goto :goto_3
.end method

.method static getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->gSCEPService:Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    if-nez v0, :cond_0

    .line 97
    const-string v0, "knox_scep_policy"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v0

    sput-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->gSCEPService:Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    .line 101
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->gSCEPService:Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    return-object v0
.end method


# virtual methods
.method public deleteUserCertificate(Ljava/lang/String;)I
    .locals 4
    .param p1, "certificateHash"    # Ljava/lang/String;

    .prologue
    .line 393
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 394
    iget-object v1, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseCertEnrollPolicy.deleteUserCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 396
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->cepProtocol:Ljava/lang/String;

    invoke-interface {v1, v2, v3, p1}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->deleteUserCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 401
    :goto_0
    return v1

    .line 397
    :catch_0
    move-exception v0

    .line 398
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseCertEnrollPolicy API deleteUserCertificate-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 401
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public enrollUserCertificate(Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "enrollmentProfile"    # Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
    .param p3, "caCertHash"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 265
    .local p2, "allowedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v1, "EnterpriseCertEnrollPolicy.enrollUserCertificate"

    invoke-static {v0, v1}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 268
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v2, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->cepProtocol:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->enrollUserCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;Ljava/util/List;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 275
    :goto_0
    return-object v0

    .line 269
    :catch_0
    move-exception v6

    .line 270
    .local v6, "e":Landroid/os/RemoteException;
    sget-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v1, "Failed at EnterpriseCertEnrollPolicy API enrollUserCertificate-Exception"

    invoke-static {v0, v1, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 275
    .end local v6    # "e":Landroid/os/RemoteException;
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 273
    :cond_0
    sget-object v0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v1, "enrollUserCertificate - calling.....service is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public getCertEnrollmentStatus(Ljava/lang/String;)I
    .locals 4
    .param p1, "transactionId"    # Ljava/lang/String;

    .prologue
    .line 449
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseCertEnrollPolicy.getCertEnrollmentStatus"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 452
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->cepProtocol:Ljava/lang/String;

    invoke-interface {v1, v2, v3, p1}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->getCertEnrollmentStatus(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 458
    :goto_0
    return v1

    .line 453
    :catch_0
    move-exception v0

    .line 454
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseCertEnrollPolicy API getCertEnrollmentStatus-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 458
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public renewUserCertificate(Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .param p1, "certificateHash"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 333
    .local p2, "allowedPackages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 334
    iget-object v1, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    const-string v2, "EnterpriseCertEnrollPolicy.renewUserCertificate"

    invoke-static {v1, v2}, Landroid/app/enterprise/license/EnterpriseLicenseManager;->log(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;)V

    .line 336
    :try_start_0
    invoke-static {}, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->getService()Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->mContextInfo:Landroid/app/enterprise/ContextInfo;

    iget-object v3, p0, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->cepProtocol:Ljava/lang/String;

    invoke-interface {v1, v2, v3, p1, p2}, Lcom/sec/enterprise/knox/certenroll/IEnterpriseCertEnrollPolicy;->renewUserCertificate(Landroid/app/enterprise/ContextInfo;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 342
    :goto_0
    return-object v1

    .line 337
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/sec/enterprise/knox/certenroll/EnterpriseCertEnrollPolicy;->TAG:Ljava/lang/String;

    const-string v2, "Failed at EnterpriseCertEnrollPolicy API renewUserCertificate-Exception"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 342
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_0
    const-string v1, ""

    goto :goto_0
.end method
