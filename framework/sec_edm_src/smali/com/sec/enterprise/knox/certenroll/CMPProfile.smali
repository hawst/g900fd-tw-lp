.class public Lcom/sec/enterprise/knox/certenroll/CMPProfile;
.super Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;
.source "CMPProfile.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CMP_HTTP:I = 0x0

.field public static final CMP_POP_CHALLENGERESP:I = 0x1

.field public static final CMP_POP_INDIRECTENCRYPTCERT:I = 0x2

.field public static final CMP_POP_SIGNATURE:I = 0x0

.field public static final CMP_TCP:I = 0x1


# instance fields
.field public cmpServerURL:Ljava/lang/String;

.field public iakLength:J

.field public initialAuthenticationKey:[B

.field public issuerDN:Ljava/lang/String;

.field public keyUsage:I

.field public notAfterDate:J

.field public notBeforeDate:J

.field public popType:I

.field public subjectDN:Ljava/lang/String;

.field public transport:I

.field public userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 219
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;-><init>()V

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->profileType:Ljava/lang/String;

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->cmpServerURL:Ljava/lang/String;

    .line 222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->userName:Ljava/lang/String;

    .line 223
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->iakLength:J

    .line 224
    iget-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->iakLength:J

    long-to-int v0, v0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->initialAuthenticationKey:[B

    .line 225
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->initialAuthenticationKey:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    .line 226
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->subjectDN:Ljava/lang/String;

    .line 227
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->issuerDN:Ljava/lang/String;

    .line 228
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keySize:I

    .line 229
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyPairAlgorithm:Ljava/lang/String;

    .line 230
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->popType:I

    .line 231
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyUsage:I

    .line 232
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->transport:I

    .line 233
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notBeforeDate:J

    .line 234
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notAfterDate:J

    .line 235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->certificateAlias:Ljava/lang/String;

    .line 236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keystoreType:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;[BJLjava/lang/String;Ljava/lang/String;ILjava/lang/String;IIIJJLjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "cmpServerUrl"    # Ljava/lang/String;
    .param p2, "userName"    # Ljava/lang/String;
    .param p3, "initializationAuthentionKey"    # [B
    .param p4, "iakLength"    # J
    .param p6, "subjectDN"    # Ljava/lang/String;
    .param p7, "issuerDN"    # Ljava/lang/String;
    .param p8, "keySize"    # I
    .param p9, "keyPairAlgorithm"    # Ljava/lang/String;
    .param p10, "popType"    # I
    .param p11, "keyUsage"    # I
    .param p12, "transport"    # I
    .param p13, "notBeforeDate"    # J
    .param p15, "notAfterDate"    # J
    .param p17, "certAlias"    # Ljava/lang/String;
    .param p18, "keyStoreType"    # Ljava/lang/String;

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/sec/enterprise/knox/certenroll/EnrollmentProfile;-><init>()V

    .line 198
    const-string v2, "CMP"

    iput-object v2, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->profileType:Ljava/lang/String;

    .line 199
    iput-object p1, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->cmpServerURL:Ljava/lang/String;

    .line 200
    iput-object p2, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->userName:Ljava/lang/String;

    .line 201
    iput-object p3, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->initialAuthenticationKey:[B

    .line 202
    iput-wide p4, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->iakLength:J

    .line 203
    iput-object p6, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->subjectDN:Ljava/lang/String;

    .line 204
    iput-object p7, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->issuerDN:Ljava/lang/String;

    .line 205
    iput p8, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keySize:I

    .line 206
    iput-object p9, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyPairAlgorithm:Ljava/lang/String;

    .line 207
    iput p10, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->popType:I

    .line 208
    iput p11, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyUsage:I

    .line 209
    iput p12, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->transport:I

    .line 210
    move-wide/from16 v0, p13

    iput-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notBeforeDate:J

    .line 211
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notAfterDate:J

    .line 212
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->certificateAlias:Ljava/lang/String;

    .line 213
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keystoreType:Ljava/lang/String;

    .line 214
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 246
    const/4 v0, 0x0

    return v0
.end method

.method public getProfileType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->profileType:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "arg1"    # I

    .prologue
    .line 255
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 256
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->profileType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 257
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->cmpServerURL:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 258
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->userName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 259
    iget-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->iakLength:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 260
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->initialAuthenticationKey:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 261
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->subjectDN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->issuerDN:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 263
    iget v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keySize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 264
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyPairAlgorithm:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 265
    iget v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->popType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 266
    iget v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keyUsage:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 267
    iget v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->transport:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 268
    iget-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notBeforeDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 269
    iget-wide v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->notAfterDate:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 270
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->certificateAlias:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 271
    iget-object v0, p0, Lcom/sec/enterprise/knox/certenroll/CMPProfile;->keystoreType:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 273
    return-void
.end method
